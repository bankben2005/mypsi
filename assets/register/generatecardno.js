var clipboard = new Clipboard('.clipboard-btn');

clipboard.on('success', function(e) {
        
    });

    clipboard.on('error', function(e) {
        
    });
$('#btn-generate').click(function(e){
				var base_url = $('input[name="base_url"]').val();
				var btn_txt = $(this).html();
				var btn_generate = $(this);

				/* disabled button and add spinner */
				$(this).attr('disabled','disabled');
				$(this).html('').html('<i class="fa fa-spin fa-spinner"></i>');


				var form_data = {};
				$.ajax({
				        url: base_url+"register/RegisterAgent/ajaxGenerateAgentCardNumber",
				        type: "post",
				        data: form_data ,
				        async:true,
		                dataType:'json',
		                beforeSend: function() {
					        
					    },	
				        success: function (response) {
				        	if(response.status){
				        		$('input[name="cardno"]').val(response.code);
				        		btn_generate.removeAttr('disabled');
				        		btn_generate.html('').html(btn_txt);

				        		/* enable copy to clipboard */
				        		$('#btn-clipboard').removeAttr('disabled');



				        	}
						},
						error: function (request, status, error) {
		                		console.log(request.responseText);
		            	}
				});

});

$('.unlock-btn').click(function(){
	var base_url = $('input[name="base_url"]').val();
		var cardnogenerated = $('input[name="cardnogenerated"]').val();

				$.ajax({
				        url: base_url+"register/RegisterAgent/ajaxGetCardnoDateTime",
				        type: "post",
				        data: {'cardno':cardnogenerated} ,
				        async:true,
		                dataType:'json',
		                beforeSend: function() {
					        
					    },	
				        success: function (response) {

				        	if(response.status){
				        		var txt_show = '<div class="alert alert-info mt-10">';
				        		txt_show += '<p class="text-center">'+response.datetime+'</p>';
				        		txt_show += '</div>';

				        		$('#show-datetime').html('').html(txt_show);

				        	}
						},
						error: function (request, status, error) {
		                		console.log(request.responseText);
		            	}
				});

});