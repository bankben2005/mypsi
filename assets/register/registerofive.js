$("#frm-register").submit(function(e){
		//e.preventDefault();
		var checkpolicy = $(this).find('input[name="accept_policy"]');

		
		var sn_val = $(this).find('input[name="product_sn"]').val();

		var submit_btn = $(this).find('button[type="submit"]');

		var txt_submit = submit_btn.html();
		txt_submit += ' <i class="fa fa-spin fa-spinner"></i>';
		//submit_btn.attr('disabled','disabled');
		

		if($('input[name="product_sn"]').val() == "" || 
			$('input[name="firstname"]').val() == "" || 
			$('input[name="lastname"]').val() == "" || 
			$('input[name="telephone"]').val() == ""){
			e.preventDefault();
						swal({
							title: "แจ้งเตือน!",
							text: "กรุณากรอกข้อมูลให้ครบถ้วน",
							icon: "warning",
							type: "warning"
							}).then(function() {
							});

		}else if(sn_val.length !== 16){
			e.preventDefault();
						swal({
							title: "แจ้งเตือน!",
							text: "กรุณากรอก Serial number ให้ครบตามจำนวน 16 หลัก",
							icon: "warning",
							type: "warning"
							}).then(function() {
							});

		}else if(!checkpolicy.is(':checked')){
							e.preventDefault();
							swal({
							title: "แจ้งเตือน!",
							text: "กรุณาตกลงเงื่อนไขการลงทะเบียน",
							icon: "warning",
							type: "warning"
							}).then(function() {

							});

		}else{
			//e.preventDefault();
			submit_btn.attr('disabled','disabled');
			submit_btn.html('').html(txt_submit);
		}

		
});

