function checkAlreadyOrderProduct(){
         var agentcode = $('input[name="agentcode"]').val();

          var post_data = {
            'agentcode':agentcode
          };
          $.ajax({
            url: 'OrderProduct/ajaxCheckAlreadyOrderProduct',
            type: "post",
            data: post_data ,
            async:true,
                dataType:'json',
                success: function (response) {
                  console.log('==check already orderproduct ==');
                  console.log(response);
                  if(response.status){


                    $('button[type="submit"]').attr('disabled','disabled');
                     // location.reload();
                    swal({
                    title: "แจ้งเตือน!",
                    text: "คุณได้ทำการจองสินค้าแล้ว กรุณาติดต่อ \n Call Center 1247 \n Call ARM 0971370642",
                    icon: "warning",
                    type: "warning",
                    html:true,
                    //closeOnConfirm: false, //It does close the popup when I click on close button
                    //closeOnCancel: false,
                    //allowOutsideClick: false,
                    closeOnClickOutside: false,
                    showCancelButton: false,
                    button: {
                      text: "ดูรายการสินค้าที่สั่งจอง",
                    },
                    }).then(function() {
                      window.location.href = "OrderProduct/listorderproduct?agentcode="+$('input[name="agentcode"]').val();
                      //return false;
                    // Redirect the user
                    //window.location.href = "OrderProduct";
                    //console.log('The Ok Button was clicked.');
                    });
                  }
                   // you will get response from your php page (what you echo or print)                 

                },
            error: function (request, status, error) {
                console.log(request.responseText);
            }


      });
}

$(document).ready(function(){

    

    $('#form-orderproduct').submit(function(e){
      // e.preventDefault();
      var submit_btn = $('button[type="submit"]');
      submit_btn.attr('disabled','disabled');
      var txt_submit = submit_btn.html();

      txt_submit += ' <i class="fa fa-spin fa-spinner"></i>';

      submit_btn.html(txt_submit);
      


      var submit_available = true;
      var count_element = 0;
      var count_null = 0;
      $("input[type='number']").each(function(index,element){
      		var element = $(element);
      		// console.log(element.val());
      		if(element.val() == '' || element.val() == 0){
      			count_null += 1;
      		}
      		count_element += 1;
      });

      if(count_null === count_element){

      					swal({
						title: "แจ้งเตือน!",
						text: "กรุณาสั่งจองอย่างน้อย 1 ผลิตภัณฑ์",
						icon: "warning",
						type: "warning"
						}).then(function() {
						// Redirect the user
						window.location.href = "OrderProduct";
						console.log('The Ok Button was clicked.');
						});
		return false;
      }else{
      	return true;
      }


    });

    $('#form-ordersummary').submit(function(e){
       e.preventDefault();

      var form_data = $(this).serialize();



      //console.log(form_data); return false;
      var submit_btn = $('button[type="submit"]');
      submit_btn.attr('disabled','disabled');
      var txt_submit = submit_btn.html();

      txt_submit += ' <i class="fa fa-spin fa-spinner"></i>';

      submit_btn.html(txt_submit);
      
      	$.ajax({
		        url: 'OrderProduct/ajaxSaveProductOrder',
		        type: "post",
		        data: form_data ,
		        async:true,
                dataType:'json',
		        success: function (response) {
		        	console.log('==response==');
		        	console.log(response);
		        	if(response.status){
		        		 // location.reload();
		        		swal({
						title: "สำเร็จ!",
						text: "สั่งจองสินค้าสำเร็จ",
						icon: "success",
						type: "success"
						}).then(function() {
						// Redirect the user
						window.location.href = "OrderProduct";
						console.log('The Ok Button was clicked.');
						});
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});


  	});





  });



  function setagentcode(agentcode){
              $('input[name="agentcode"]').val(agentcode);
                checkAlreadyOrderProduct();
              /* check already order product */

              // swal({
              //   title: "Already received agentcode!",
              //   text: "agent code is "+agentcode,
              //   icon: "success",
              //   type: "success"
              //   }).then(function() {
                  
              //   });
              
  }
  