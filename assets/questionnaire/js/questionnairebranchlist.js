function renderBranchAndPTList(){

	var base_url = $('input[name="base_url"]').val();

	//console.log('base_url = '+base_url);
	var post_data = {
		'agentcode':$('input[name="agentcode"]').val()

	};

		$.ajax({
		        url: base_url+'questionnaire/Questionnaire/ajaxGetSUNAMEByAgentCode',
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			        $('#list-branch-pt').html("");
			    },
		        success: function (response) {
		        	console.log('==response==');
		        	console.log(response);
		        	if(response.status){
		        		$("#list-branch-pt").html(response.view);
		        	}else{
		        		var txt_alert = "";
		        		txt_alert += '<div class="alert alert-danger text-center">';
		        		txt_alert += '<strong>ไม่พบรายการสั่งซื้อ !</strong>';
		        		txt_alert += '<p>ไม่พบข้อมูลการสั่งซื้อกับสาขา</p>';
		        		txt_alert += '</div>';
		        		$("#list-branch-pt").html(txt_alert);
		        	}
		        	$('.loading').hide();

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});



}



function setagentcode(agentcode){
			  
              $('input[name="agentcode"]').val(agentcode);
              renderBranchAndPTList();

              /* check already order product */
}

function gotoQuestionnaire(element){
	var element = $(element);
	var base_url = $('input[name="base_url"]').val();

	var is_answer = element.find('input[name="is_answer"]').val();
	// console.log(is_answer);return false;
	if(is_answer == 1){
								swal({
								title: "แจ้งเตือน!",
								text: "คุณได้ทำการประเมินรายการดังกล่าวแล้ว",
								icon: "warning",
								type: "warning"
								}).then(function() {
										return false;
								});
	}else{

	serialize = function(obj, prefix) {
	  var str = [], p;
	  for(p in obj) {
	    if (obj.hasOwnProperty(p)) {
	      var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
	      str.push((v !== null && typeof v === "object") ?
	        serialize(v, k) :
	        encodeURIComponent(k) + "=" + encodeURIComponent(v));
	    }
	  }
	  return str.join("&");
	}
	var dataObj = {
		'agentcode':$('input[name="agentcode"]').val(),
		'suname':element.find('input[name="SUNAME"]').val()

	};

	window.location.href = base_url+'questionnaire/Questionnaire/DoQuestionnaire?'+serialize(dataObj);

	// console.log(serialize(dataObj));
	}


}



$(document).ready(function(){
		var url = window.location.href,
	    retObject = {},
	    parameters;

	    if (url.indexOf('?') === -1) {
	        return null;
	    }

	    url = url.split('?')[1];

	    parameters = url.split('&');

	    for (var i = 0; i < parameters.length; i++) {
	        retObject[parameters[i].split('=')[0]] = parameters[i].split('=')[1];
	    }

	    
	    if(Object.getOwnPropertyNames(retObject).length !== 0){
		    $('input[name="agentcode"]').val(retObject.agentcode);
	        renderBranchAndPTList();
    	}else{
    		console.log('no query string');
    	}


});