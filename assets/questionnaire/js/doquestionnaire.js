$('.rating').rating({
		clearButton:"",
		clearCaption:"ให้คะแนน",
		starCaptions: function(val) {
		    if (val === 1) {
		        return 'แย่';
		    }else if(val === 2){
		    	return 'ควรปรับปรุง';
		    }else if(val === 3){
		    	return 'พอใช้';
		    }else if(val === 4){
		    	return 'ดี';
		    }else if(val === 5){
		    	return 'ดีมาก';
		    }

		}
});


function submitQuestionnaire(form_data){
			var base_url = $('input[name="base_url"]').val();


			$.ajax({
				        url: base_url+"questionnaire/Questionnaire/ajaxSubmitQuestionnaire",
				        type: "post",
				        data: form_data ,
				        async:true,
		                dataType:'json',
		                beforeSend: function() {
					        
					    },
				        success: function (response) {
				        	console.log('response');
				        	console.log(response);

				        	if(response.status){
				        		if(response.has_coupon && response.get_coupon){
				        			var txt_discount = "";
				        			txt_discount += 'จำนวน '+response.coupon_data.DiscountRate+' ';
				        			txt_discount += (response.coupon_data.DiscountType == 'amount')?'บาท':'เปอร์เซ็น';
				        			txt_discount += ' ท่านสามารถใช้โค๊ด '+response.coupon_data.Code+' เพื่อรับส่วนลดที่จุดจำหน่ายทั่วประเทศ';
				        			swal({
										title: "ยินดีด้วย!",
										text: "คุณได้ทำการประเมินเสร็จสิ้น \ ได้รับคูปองส่วนลด "+txt_discount,
										html:true,
										icon: "success",
										type: "success"
										}).then(function() {
												window.location.href = base_url+"questionnaire/Questionnaire?agentcode="+response.agentcode;
										});
					        		

				        		}else{

				        			var txt_alert = "ทำการประเมินเสร็จเรียบร้อย \n ";
				        			if(response.has_coupon){
				        				txt_alert += "กรุณาประเมินให้ครบทุกรายการเพื่อรับคูปองส่วนลด";
				        			}
				        			swal({
										title: "เสร็จสิ้น!",
										text: txt_alert,
										html:true,
										icon: "success",
										type: "success"
										}).then(function() {
												window.location.href = base_url+"questionnaire/Questionnaire?agentcode="+response.agentcode;
										});
					        		}

				        	}
						},
						error: function (request, status, error) {
		                		console.log(request.responseText);
		            	}
				});


}

$(document).ready(function(){
	    $('.loading').hide();
		$('form[name="frm-questionnaire"]').submit(function(e){
				e.preventDefault();


				/* check all rating */
					var status = true;
					$('.rating').each(function(index,element){
						var element = $(element);
						
						if(element.val() == "" || element.val() == 0){
							status = false;
						}

					});
				/* eof check all rating */

				if(!status){
								swal({
								title: "แจ้งเตือน!",
								text: "กรุณาให้คะแนนให้ครบทุกข้อ",
								icon: "warning",
								type: "warning"
								}).then(function() {
										// location.reload();
								});
				}else{
						var form_data = $(this).serialize();
						console.log(form_data);
						submitQuestionnaire(form_data);
				}

		});

});