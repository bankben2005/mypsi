function searchMPCODE(element){
	//console.log(element);
	var base_url = $('input[name="base_url"]').val();

	var element = $(element);
	var mpcode_textbox = element.parent().parent().find('input[name="search_mpcode"]');
	var mpcode = mpcode_textbox.val();
	//console.log(mpcode);
	if(mpcode === ''){
		mpcode_textbox.focus();
		return false;
	}


	var post_data = {
		'mpcode':mpcode
	};
	console.log(post_data);

					$.ajax({
                        url: base_url+"backend/admin_psiarm/ajaxGetMPCODEData",
                        type: "post",
                        data: post_data ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                            
                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	var search_result_html = '<ul class="list-group">';
                            	search_result_html += '<li class="list-group-item">';
                            	search_result_html += response.data.MPNAME+'('+response.data.MPDESCRIPTION+')';
                            	search_result_html += '<a href="javascript:void(0)" class="btn btn-primary pull-right btn-xs" data-mpcode="'+response.data.MPCODE+'" data-name="'+response.data.MPNAME+'('+response.data.MPDESCRIPTION+'" onclick="chooseProduct(this)">เลือก</a>';
                            	search_result_html += '</li>';

                            	search_result_html += '</ul>';

                            	$('div#search_result').html('').html(search_result_html);
                                
                            }else{
                            	var notfound_div = '<div class="alert alert-danger">';
                            	notfound_div += '<strong>ไม่พบข้อมูล</strong>';
                            	notfound_div += '</div>';
                            	$('div#search_result').html('').html(notfound_div);
                            }
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });
}

function searchMPNAME(element){

	var base_url = $('input[name="base_url"]').val();

	var element = $(element);
	var mpname_textbox = element.parent().parent().find('input[name="search_mpname"]');
	var mpname = mpname_textbox.val();

	if(mpname === ''){
		mpname_textbox.focus();
		return false;
	}

	var post_data = {
		'mpname':mpname
	};

					$.ajax({
                        url: base_url+"backend/admin_psiarm/ajaxGetMPNAMEData",
                        type: "post",
                        data: post_data ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                            
                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	var search_result_html = '<ul class="list-group">';

                            	$.each(response.data,function(index,value){
                            		search_result_html += '<li class="list-group-item">';
	                            	search_result_html += value.MPNAME+'('+value.MPDESCRIPTION+')';
	                            	search_result_html += '<a href="javascript:void(0)" class="btn btn-primary pull-right btn-xs" data-mpcode="'+value.MPCODE+'" data-name="'+value.MPNAME+'('+value.MPDESCRIPTION+'" onclick="chooseProduct(this)">เลือก</a>';
	                            	search_result_html += '</li>';
                            	});

                            	

                            	search_result_html += '</ul>';

                            	$('div#search_result').html('').html(search_result_html);
                                
                            }else{
                            	var notfound_div = '<div class="alert alert-danger">';
                            	notfound_div += '<strong>ไม่พบข้อมูล</strong>';
                            	notfound_div += '</div>';
                            	$('div#search_result').html('').html(notfound_div);
                            }
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });


}


function chooseProduct(element){
	var element = $(element);
	var product_name = element.data('name');
	var mpcode = element.data('mpcode');


	/* check table row already exist */
	//var already_row = false;
	var available_status = true;
	$('table > tbody > tr').each(function(index,value){
		//console.log(value);
		var quantity_elm = $(value).find('input[name="quantity[]"][data-mpcode="'+mpcode+'"]');
		console.log(quantity_elm);
		if($(value).find('input[name="quantity[]"][data-mpcode="'+mpcode+'"]').length === 1){
			available_status =  false;
		}
	});
	/* eof check table row already exist */
	
	if(!available_status){
		return false;
	}

	var tr_row = '<tr>';
		tr_row += '<td>'+mpcode+'</td>';
		tr_row += '<td>'+product_name+'</td>';
		tr_row += '<td><input name="quantity[]" data-mpcode="'+mpcode+'" class="form-control text-center" onkeypress="validate(event)"></td>';
		tr_row += '<td><a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="deleteRow(this)">ลบ</a></td>';
	tr_row += '</tr>';
	$('table > tbody').append(tr_row);
}

function deleteRow(element){
	var element = $(element);
	var tr_row = element.parent().parent();
	tr_row.remove();
	console.log(tr_row);
}

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}


$(document).ready(function(){
  $('input[name="search_mpcode"]').keyup(function(e){
    if(e.keyCode == 13){
      e.preventDefault();
      //searchSupplier($('a.anchor-search-supplier'));
      searchMPCODE($('a.anchor-search-mpcode'));
    }
  });

  $('input[name="search_mpname"]').keyup(function(e){
    if(e.keyCode == 13){
      e.preventDefault();
      //searchSupplier($('a.anchor-search-supplier'));
      searchMPNAME($('a.anchor-search-mpname'));
    }
  });


  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

  $('input[name="search_type"]').click(function(){
  		var check_type= $(this).val();
  		switch(check_type){
  			case 'mpcode':
  				$('div#search-type-mpcode').css({
  					'display':''
  				});
  				$('div#search-type-mpname').css({
  					'display':'none'
  				});

  				$('div#search_result').html('');
  			break;
  			case 'product_name':
  				$('div#search-type-mpcode').css({
  					'display':'none'
  				});
  				$('div#search-type-mpname').css({
  					'display':''
  				});

  				$('div#search_result').html('');
  			break;
  		}
  });

  $('form[name="create-flashsalesteptwo-frm"]').submit(function(e){
  		e.preventDefault();
  		var tr_length = $('table > tbody >tr').length;
  		if(tr_length === 0){
  			e.preventDefault();
  			swal({
			  title: "แจ้งเตือน!!",
			  text: "กรุณาเพิ่มสินค้าอย่างน้อย 1 ชนิด",
			  icon: "warning",
			  // buttons: true,
			  // dangerMode: true,
			})
			.then(function(){

			});
  		}
  		//console.log('tr_length = '+tr_length);
  		var check_table_null = false;
  		/* check every row fill already */
  		$('table > tbody > tr').each(function(index,value){
  			var quantity_textbox = $(value).find('input[name="quantity[]"]');
  			if(quantity_textbox.val() === ''){
  				check_table_null = true;
  				e.preventDefault();
  				quantity_textbox.focus();
  				
  			}
  		});

  		/* eof check every row fill already*/

  		if(check_table_null){
  			e.preventDefault();
  			return false;
  		}
  		
  		var submit_data = [];

  		$('table > tbody > tr').each(function(index,value){
  			var quantity_textbox = $(value).find('input[name="quantity[]"]');
  			var mpcode = quantity_textbox.data('mpcode');
  			submit_data.push(
  			{
  				'mpcode':mpcode,
  				'quantity':quantity_textbox.val()
  			}
  			);
  		});

  		//console.log(submit_data);
  		submitStepTwo(submit_data);
  });	
});

function submitStepTwo(arrData){
	var base_url = $('input[name="base_url"]').val();

					$.ajax({
                        url: base_url+"backend/admin_psiarm/ajaxConfirmStepTwo",
                        type: "post",
                        data: {'steptwo_data':arrData} ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                            
                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	window.location.href=base_url+'backend/admin_psiarm/createFlashSale_stepthree';
                            }else{
                            	
                            }
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });

}
