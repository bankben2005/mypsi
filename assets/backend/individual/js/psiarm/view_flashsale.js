 $("#getting-started")
  .countdown($('input[name="hide_end_datetime"]').val(), function(event) {
    $(this).text(
      event.strftime('%D days %H:%M:%S')
    );
  });

  function clickEditSetProduct(element){
  	var base_url = $('input[name="base_url"]').val();
  	var element = $(element);
  	var id = element.data('id');
  	var name = element.data('name');
  	var description = element.data('description');
  	var image_landscape  = element.data('imagelandscape');
  	var image_portait = element.data('imageportait');
    var total_available = element.data('totalavailable');

  	var form_editset_product = $('form[name="edit-setproduct-form"]');

  	form_editset_product.find('input[name="hide_setproduct_id"]').val('').val(id);
  	form_editset_product.find('input[name="name"]').val('').val(name);
  	form_editset_product.find('textarea[name="description"]').val('').val(description);
    form_editset_product.find('input[name="total_available"]').val('').val(total_available);


  	if(image_landscape === ''){
  		form_editset_product.find('div#view_image_landscape').hide();
  	}else{
  		form_editset_product.find('div#view_image_landscape').show();
  		form_editset_product.find('img.image_landscape').attr('src','');
  		form_editset_product.find('img.image_landscape').attr('src',base_url+'uploaded/flashsalesetproduct/landscape/'+id+'/'+image_landscape)
  	}

  	if(image_portait === ''){
  		form_editset_product.find('div#view_image_portait').hide();
  	}else{
  		form_editset_product.find('div#view_image_portait').show();
  		form_editset_product.find('img.image_portait').attr('str','');
  		form_editset_product.find('img.image_portait').attr('src',base_url+'uploaded/flashsalesetproduct/portait/'+id+'/'+image_portait);
  	}
  }

$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });


});


function changeFlashSaleStatus(element){
  var base_url = $('input[name="base_url"]').val();
  var element = $(element);
  var flashsaleset_id = element.data('flashsalesetid');
  var changed_status = element.val();

  var post_data = {
      'flashsaleset_id':flashsaleset_id,
      'changed_status':changed_status
  };

        $.ajax({
                url: base_url+"backend/admin_psiarm/ajaxSetFlashSaleStatus",
                type: "post",
                data: post_data ,
                async:true,
                dataType:'json',
                beforeSend: function() {
                            
                },
                success: function (response) {
                    console.log('====== response =======');
                    console.log(response);
                    if(response.status){
                        // window.location.href=base_url+'backend/admin_psiarm/createFlashSale';
                        swal({
                          title: "สำเร็จ!!",
                          text: "แก้ไขสถานะ Flashsale สำเร็จ",
                          icon: "success",
                          // buttons: true,
                          // dangerMode: true,
                        })
                        .then(function(){

                        });
                    }else{
                        swal({
                          title: "ไม่สำเร็จ !!",
                          text: "เกิดข้อผิดพลาด ",
                          icon: "danger",
                          // buttons: true,
                          // dangerMode: true,
                        })
                        .then(function(){

                        });
                    }
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }

        });



  console.log(post_data);

}

$('input[name="duration"]').daterangepicker({
      autoApply:true,
          timePicker: true,
          timePicker24Hour: true,
          // timePickerIncrement: 30,
          //minDate:moment(new Date()),
          locale: {
              format: 'DD-MM-YYYY HH:mm'
          }
  }, function(start, end, label) {
    //console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    console.log(start);
    console.log(end);
    var post_data = {
      'start_datetime':start.format('DD-MM-YYYY HH:mm'),
      'end_datetime':end.format('DD-MM-YYYY HH:mm'),
      'flashsaleset_id':$('input[name="hide_flashsaleset_id"]').val()
    };

    updateDuration(post_data);

    console.log(post_data);
});

// $('input[name="duration"]').data('daterangepicker').setStartDate($('input[name="hide_startdate"]').val());
// $('input[name="duration"]').data('daterangepicker').setEndDate($('input[name="hide_enddate"]').val());

function updateDuration(post_data){
  var base_url = $('input[name="base_url"]').val();

        $.ajax({
                url: base_url+"backend/admin_psiarm/ajaxUpdateDuration",
                type: "post",
                data: post_data ,
                async:true,
                dataType:'json',
                beforeSend: function() {
                            
                },
                success: function (response) {
                    console.log('====== response =======');
                    console.log(response);
                    if(response.status){
                        // window.location.href=base_url+'backend/admin_psiarm/createFlashSale';
                        swal({
                          title: "สำเร็จ!!",
                          text: "แก้ไขวันเริ่มต้นและสิ้นสุด Flashsale สำเร็จ",
                          icon: "success",
                          // buttons: true,
                          // dangerMode: true,
                        })
                        .then(function(){

                        });
                    }else{
                        swal({
                          title: "ไม่สำเร็จ !!",
                          text: "เกิดข้อผิดพลาด ",
                          icon: "danger",
                          // buttons: true,
                          // dangerMode: true,
                        })
                        .then(function(){

                        });
                    }
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }

        });

}

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

function exportOrderToExcel(element){
  var base_url = $('input[name="base_url"]').val();
  var element = $(element);
  var branch_code = element.parent().parent().find('select[name="select_branch"]').val();
  var flashsaleset_id = $('input[name="hide_flashsaleset_id"]').val();
  //console.log(branch_code);
  var form_text = '<form action="'+base_url+'backend/admin_psiarm/exportOrderToExcel" method="post">';
  form_text += '<input type="hidden" name="branch_code" value="'+branch_code+'">';
  form_text += '<input type="hidden" name="flashsale_set_id" value="'+flashsaleset_id+'">';
  form_text += '</form>';
  $(form_text).appendTo('body').submit();
  //console.log(form_text);
  //$('<form action="form2.html"></form>').appendTo('body').submit();
}