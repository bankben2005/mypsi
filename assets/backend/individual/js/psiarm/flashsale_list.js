function checkCurrentFlashSaleExist(){
	var base_url = $('input[name="base_url"]').val();
				$.ajax({
                        url: base_url+"backend/admin_psiarm/ajaxCheckCurrentFlashSaleExist",
                        type: "post",
                        data: {} ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                            
                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	window.location.href=base_url+'backend/admin_psiarm/createFlashSale';
                            }else{
                            	swal({
								  title: "แจ้งเตือน!!",
								  text: "ไม่สามารถเพิ่ม FlashSale ได้เนื่องจากมี FlashSale ที่เปิดใช้งานอยู่อยู่",
								  icon: "warning",
								  // buttons: true,
								  // dangerMode: true,
								})
								.then(function(){

								});
                            }
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });
}



$(document).ready(function(){
	$('table > tbody > tr').each(function(index,value){
		var tr_element = $(value);
		var hide_end_datetime = tr_element.find('input[name="hide_end_datetime[]"]');
		var flash_id = hide_end_datetime.data('flashid');
		
		$("#flashsaleset_"+flash_id)
		  .countdown(hide_end_datetime.val(), function(event) {
		    $(this).text(
		      event.strftime('%D days %H:%M:%S')
		    );
		});


	});
});