$(document).ready(function(){
		var base_url = $('input[name="base_url"]').val();
	    jQuery('form[name="create-flashsale-frm"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: ''
                        }
                    }
                },
                description:{
                    validators:{
                        notEmpty:{
                            message: ''
                        }
                    }
                }

            },
            onSuccess: function(e, data) {
              //e.preventDefault();

              

            }

        });
});

$('input[name="startend_datetime"]').daterangepicker({
			autoApply:true,
	        timePicker: true,
	        timePicker24Hour: true,
	        // timePickerIncrement: 30,
	        minDate:moment(new Date()),
	        locale: {
	            format: 'DD-MM-YYYY HH:mm'
	        }
	}, function(start, end, label) {
	  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
