    $("#createcoupon-frm").validate({
        errorPlacement: function(error, element) {
            // Append error within linked label
            $( element )
                .closest( "form" )
                    .find( "label[for='" + element.attr( "id" ) + "']" )
                        .append( error );
        },
        errorElement: "span",
                                     rules: {
                                        
                                        Name: {
                                            required:true
                                          
                                        },
                                        Description: {
                                            required:true
                                            
                                        },
                                        DiscountRate:{
                                            required:true,
                                            number:true
                                        }
                                    },
        messages: {
                                       
                                        Name: {
                                            required:" *กรุณากรอกชื่อคูปอง"
                                          
                                        },
                                        Description: {
                                            required:" *กรุณากรอกรายละเอียดคูปอง"
                                            
                                        },
                                        DiscountRate:{
                                            required:" *กรุณากรอกส่วนลด",
                                            number:" *กรุณากรอกเฉพาะตัวเลขเท่านั้น"
                                        }
        },
                                    submitHandler: function(form) {
                                        form.submit();
                                    }
    });




$('input[name="Duration"]').daterangepicker({
    	"autoApply": true,
        timePicker: false,
        timePickerIncrement: 30,
        minDate:'today',
        locale: {
            format: 'DD-MM-YYYY'
        }
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});



$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });


});
$(function() {
    $('input[name="duration"]').daterangepicker({
        "autoApply": true,
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'DD-MM-YYYY h:mm:ss'
        }
    });
});

$('.pop').click(function(){
    var imgurl = $(this).closest('.view').find('img').attr('src');
    // console.log(imgurl.attr('src'));
    $('#imagepreview').attr('src', imgurl); // here asign the image to the modal when the user click the enlarge link
    $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function


});


function enabledisableCode(coupontype_id){
        var base_url = $('input[name="base_url"]').val()
        var post_data = {
            'coupontype_id':coupontype_id
        };


                    $.ajax({
                        url: base_url+"backend/admin_promotion/ajaxGetCouponGenerateStatus",
                        type: "post",
                        data: post_data ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                            
                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                                if(response.isgenerate){
                                    $('input[name="Code"]').parent().hide();
                                }else{
                                    $('input[name="Code"]').parent().show();
                                }
                            }
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });
        // console.log(post_data);

}
$(document).ready(function(){
        var coupontype_id = $('select[name="CouponType_id"]').val();
        enabledisableCode(coupontype_id);
        $('select[name="CouponType_id"]').change(function(){
                enabledisableCode($(this).val());
        });
});