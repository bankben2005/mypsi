$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });


});

$(document).ready(function(){
 	var table = $('.table').DataTable({

 	});

 	var frm_member_id = $('input[name="frm_member_id"]').val();
 	if(frm_member_id){
 		$('input[name="email"]').attr('readonly','readonly');
 	}

 });


$(".btn-act-active").click(function(){

	var btn = $(this);
	var btn_txt = btn.html();

	var btn_with_load = btn_txt+' <i class="fa fa-spin fa-spinner"></i>';

	var base_url = $('input[name="base_url"]').val();
	
	var post_data = {
		'data_active':$(this).data('active'),
		'data_member':$(this).data('user'),
		'data_menu':$(this).data('menu')
	};

		$.ajax({
		        url: base_url+"backend/admin_user/ajaxMemberMenuPermissions",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
                	  btn.attr('disabled','disabled');
					  btn.html('').html(btn_with_load);
				},
		        success: function (response) {
		        	console.log('====== response =======');
		        	console.log(response);
		        	if(response.status){
		        		location.reload();
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});




 
	console.log(post_data);

});

$('.event-checkbox').click(function(){
	var base_url = $('input[name="base_url"]').val();
		var checkbox = $(this);
		var checked_status = "unchecked";

		if(checkbox.is(':checked')){
			checked_status = "checked";
		}else{
			checked_status = "unchecked";
		}

		var post_data = {
			'data_check':checked_status,
			'data_member':checkbox.data('user'),
			'data_menu':checkbox.data('menu'),
			'data_event':checkbox.data('event') 

		};


		$.ajax({
		        url: base_url+"backend/admin_user/ajaxMemberEventPermissions",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
				},
		        success: function (response) {
		        	console.log('====== response =======');
		        	console.log(response);
		        	if(response.status){
		        		// location.reload();
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});
});



    $("form[name='reset-frm']").validate({
        errorPlacement: function(error, element) {
            // Append error within linked label
            $( element )
                .closest( "form" )
                    .find( "label[for='" + element.attr( "id" ) + "']" )
                        .append( error );
        },
        errorElement: "span",
                                     rules: {
                                         password: {
                                            required: true
                                          },
                                        confirm_password: {
                                            required:true,
                                            equalTo: "#password",
                                            minlength:6
                                          
                                        }
                                    },
        							messages: {
                                        password: {
                                            required: ' *กรุณากรอกรหัสผ่านใหม่'
                                          },
                                        confirm_password: {
                                            required:" *กรุณายืนยันรหัสผ่านใหม่",
                                            equalTo: " *รหัสผ่านไม่ตรงกัน",
                                            minlength: " *กรุณากรอกอย่างน้อย 6 หลัก"
                                          
                                        }
        							},
                                    submitHandler: function(form) {



                                    	var base_url = $('input[name="base_url"]').val();
                                    	var form_element = $(form);
                                    	var new_password = form_element.find('input[name="password"]').val();
                                    	var member_id = form_element.find('input[name="members_id"]').val();
                                    	
                                    	var save_btn = form_element.find('button[type="submit"]');
                                    	var save_btn_txt = form_element.find('button[type="submit"]').html();

                                    	//console.log(save_btn_txt); return false;
                                    	var post_data = {
                                    		'new_password':new_password,
                                    		'members_id':member_id
                                    	};

                                    	// console.log(post_data); return false;

                                    	$.ajax({
										        url: base_url+"backend/admin_user/ajaxMemberResetPassword",
										        type: "post",
										        data: post_data ,
										        async:true,
								                dataType:'json',
								                beforeSend: function() {
								                	save_btn.attr('disabled','disabled');
								                	save_btn.html('').html(save_btn_txt+' <i class="fa fa-spin fa-spinner"></i>');
												},
										        success: function (response) {
										        	console.log('====== response =======');
										        	console.log(response);
										        	if(response.status){
										        		// location.reload();
										        		swal({
														title: "สำเร็จ!",
														text: "ทำการรีเซ็ตรหัสผ่านเรียบร้อย!",
														icon: "success",
														type: "success"
														}).then(function() {
																location.reload();
														});

										        	}
										           // you will get response from your php page (what you echo or print)                 

										        },
										        error: function (request, status, error) {
								                console.log(request.responseText);
								            }


								    	});

                                    	// console.log(post_data); return false;
                                    	//console.log(form);
                                    	//return false;
                                        //form.submit();
                                    }
    });

    $('.btn-resetpassword').click(function(){
    	var member_id = $(this).data('member');

    	$('input[name="members_id"]').val(member_id);

    });

    $("form[name='createuser-frm']").validate({
        errorPlacement: function(error, element) {
            // Append error within linked label
            $( element )
                .closest( "form" )
                    .find( "label[for='" + element.attr( "id" ) + "']" )
                        .append( error );
        },
        errorElement: "span",
                                     rules: {
                                        name: {
                                            required: true
                                        },
                                        email:{
                                        	required: true,
                                        	// email:true
                                        },
                                        lastname: {
                                            required:true
                                            
                                        },
                                        member_password:{
                                        	required:true,
                                        	minlength:6
                                        },
                                        member_confirm_password:{
                                        	required:true,
                                        	equalTo: "#member_password",
                                            minlength:6

                                        }
                                    },
        							messages: {
                                        name: {
                                            required: ' *กรุณากรอกชื่อ'
                                        },
                                        email:{
                                        	required: ' *กรุณากรอกอีเมล์',
                                        	// email: ' รูปแบบของอีเมล์ไม่ถูกต้อง'
                                        },
                                        lastname: {
                                            required:" *กรุณากรอกนามสกุล"                                          
                                        },
                                        member_password:{
                                        	required:" *กรุณากรอกรหัสผ่าน",
                                        	minlength:" กรุณากรอกอย่างน้อย 6 ตัวอักษร"

                                        },
                                        member_confirm_password:{
                                        	required:" *กรุณายืนยันรหัสผ่าน",
                                        	equalTo:" รหัสผ่านไม่ตรงกัน",
                                        	minlength:" รุณากรอกอย่างน้อย 6 ตัวอักษร"

                                        }
        							},
        							submitHandler: function(form) {
        								form.submit();
        							}

    });


