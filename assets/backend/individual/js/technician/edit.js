function changeProvince(province_id){
				if(province_id != '0'){

          // console.log('province_id = '+province_id);return false;
				var base_url = $("input[name='base_url']").val();

        // console.log('base_url = '+base_url); return false;

					$.ajax({
                          type: "POST",
                          url: base_url+"backend/admin_technician/ajaxGetAmphurByProvince",
                          async:true,
                          dataType:'json',
                          data:{'province_id':province_id},
                          beforeSend: function( xhr ) {
            							    $('select[name="amphurs"]').html('');
            							    $('select[name="districts"]').html('');
            							    $('input[name="zipcode"]').val('');
						              },
                          success: function(result){
                            
                            if(result.status){
                                $.each(result.data, function(key, value) {   
								                     $('select[name="amphurs"]')
								                    .append($("<option></option>")
								                    .attr("value",key)
								                    .text(value)); 
								            });
								            $('select[name="amphur"]').focus();
								            //changeAmphur($('select[name="amphur"]').val());
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
				}

}
function changeAmphur(amphur_id){

				if(amphur_id != '0'){
				var base_url = $("input[name='base_url']").val();
        // console.log('amphur_id = '+amphur_id);return false;
					     $.ajax({
                          type: "POST",
                          url: base_url+"backend/admin_technician/ajaxGetDistrictByAmphur",
                          async:true,
                          dataType:'json',
                          data:{'amphur_id':amphur_id,'province_id':$("select[name='province']").val()},
                          beforeSend: function( xhr ) {
            							    $('select[name="districts"]').html('');
            							    $('input[name="zipcode"]').val('');
						              },
                          success: function(result){
                            console.log('result');
                            console.log(result);
                            if(result.status){
                                $.each(result.data, function(key, value) {   
            								          $('select[name="districts"]')
            								         .append($("<option></option>")
            								                    .attr("value",key)
            								                    .text(value)); 
								                });
								            $('select[name="districts"]').focus();
								            //changeDistrict($('select[name="district"]').val());
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
				}


}
function changeDistrict(district_id){

			if(district_id != '0'){
				var province_id = $("select[name='provinces']").val();
				var amphur_id = $("select[name='amphurs']").val();

				var post_data = {
					'province_id':province_id,
					'amphur_id':amphur_id,
					'district_id':district_id

				}
				var base_url = $("input[name='base_url']").val();

        // console.log('post_data');
        // console.log(post_data); return false;

					$.ajax({
                          type: "POST",
                          url: base_url+"backend/admin_technician/ajaxGetZipcode",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
							               $('input[name="zipcode"]').val('');
						              },
                          success: function(result){
                            console.log('result');
                            console.log(result);
                            return false;
                            if(result.status){
                                  $('input[name="zipcode"]').val(result.data.zipcode);
								                  $('input[name="zipcode"]').focus();
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

				}


}