
$('.table').DataTable();


function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}


$(document).ready(function(){
	
 	var table = $('.table').DataTable();
    $('button.submit-btn').click( function() {
    	var base_url = $('input[name="base_url"]').val();

        var data = table.$('input, select').serialize();


         // console.log(data);
        // alert(
        //     "The following data would have been submitted to the server: \n\n"+
        //     data.substr( 0, 120 )+'...'
        // );
        // return false;
         $.ajax({
		        url: base_url+"backend/admin_staff/ajaxSaveOrderProductAvailable",
		        type: "post",
		        data: data ,
		        async:true,
                dataType:'json',
		        success: function (response) {
		        	
		        	if(response.status){

		        		 location.reload();
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});
    } );


});
