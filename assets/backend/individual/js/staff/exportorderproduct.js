$(document).ready(function(){
	 $('select[name="select_year"]').change(function(){

 			var base_url = $('input[name="base_url"]').val();
 			// console.log($(this).val());
 			var post_data = {
 				'select_year':$(this).val()
 			}

 			$.ajax({
		        url: base_url+"backend/admin_staff/ajaxGetSeminarGroupByYear",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			        $('body').append('<div class="loading">Loading&#8230;</div>');
			        $('select[name="select_branch"]').attr('disabled','disabled');
			        $('select[name="select_group"]').html('').html('<option value="">-- เลือกกรุ๊ปสมนา --</option>');
			    },
		        success: function (response) {
		        	console.log('response');
		        	console.log(response);

		        	if(response.status){
		        		$.each(response.data, function(key, value) {   
						     $('select[name="select_group"]')
						         .append($("<option></option>")
						                    .attr("value",value)
						                    .text(value)); 
						});
						$('select[name="select_group"]').removeAttr('disabled');

		        	}else{
		        		// ไม่พบงานสัมนาในปีนั้นๆ

		        	}

		        	$('.loading').hide();


		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            	}
            });
 	});

 	$('select[name="select_group"]').change(function(){
 			if($(this).val() != ''){
 				$('select[name="select_branch"]').removeAttr('disabled');
 			}
 	});
 	$('select[name="select_branch"]').change(function(){
 			if($(this).val() != ''){
 				$('button[id="search_btn"]').removeAttr('disabled');
 			}
 	});

 	$('button#search_btn').click(function(){
 			var base_url = $('input[name="base_url"]').val();


 			var select_year = $('select[name="select_year"]').val();
 			var select_group = $('select[name="select_group"]').val();
 			var select_branch = $('select[name="select_branch"]').val();


 			var form_txt = "";

 			form_txt += '<form action="'+base_url+'backend/admin_staff/exportDataToExcel" name="export-frm" method="POST">';
 			form_txt += '<input type="hidden" name="select_year" value="'+select_year+'" />';
 			form_txt += '<input type="hidden" name="select_group" value="'+select_group+'" />';
 			form_txt += '<input type="hidden" name="select_branch" value="'+select_branch+'" />';
 			form_txt += '</form>';

 			$('body').append(form_txt);
 			$('form[name="export-frm"]').submit();

 			console.log(form_txt);return false;
 	});
});