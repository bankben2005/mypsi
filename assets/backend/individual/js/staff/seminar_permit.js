function checkPermitByCardNo(element){
	var element = $(element);
	var card_no_element = element.closest('form').find('input[name="card_no"]');
	var card_no = card_no_element.val();

	if(card_no === ''){
		card_no_element.focus();
	}else{

		$('div#agent-content').parent().append('<div class="col-lg-12 text-center"><i class="fa fa-spin fa-spinner"></i></div>');
		var card_no_data = checkCardNumber(card_no);
		if(card_no_data.status){
			console.log('found card number');
			console.log(card_no_data);


			/* check data and return agent data and pos data found bought ticket */
			$('div#agent-content > .panel > .panel-body').html('').html(card_no_data.agent_view);
			/* eof check data */
			$('div#ticket-history-content > .panel-ticket-history > .panel-body').html('').html(card_no_data.ticket_view); 

			$('div#ticket-history-content > .panel-quota-history > .panel-body').html('').html(card_no_data.quota_view);

			$('div#agent-content').parent().find('i').remove();

			$('div#agent-content').css({'display':''});

			$('div#ticket-history-content').css({'display':''});

			renderAgentImageProfile({'card_no':card_no});
		}else{
			/* alert somthing tell not found card number */
			console.log('not found card number');
			$('div#result-data').html('').html(
				'<div class="alert alert-danger"><strong>ไม่พบช่างดังกล่าว</strong></div>'
			);
			/* eof alert something tell not found card number */


		}


	}
}

function checkCardNumber(card_no){
		var base_url = $('input[name="base_url"]').val();
		
		var ajax_return = $.ajax({
		        url: base_url+"backend/admin_staff/ajaxCheckCardNoExist",
		        type: "post",
		        data: {'card_no':card_no} ,
		        async:false,
                dataType:'json',
                beforeSend: function() {
			        
			    },
		        success: function (response) {

		        	return response;
		        },
		        error: function (request, status, error) {
                	console.log(request.responseText);
            	}


    	}).responseJSON;


		if(ajax_return.status){
			return ajax_return;
		}else{
			return false;
		}


}


function renderAgentImageProfile(data = {}){
	var card_no = data.card_no;
	// console.log('card no');
	// console.log(card_no);

		var base_url = $('input[name="base_url"]').val();
		
		$.ajax({
		        url: base_url+"backend/admin_staff/ajaxGetAgentImageProfileByCardNo",
		        type: "post",
		        data: {'card_no':card_no} ,
		        async:false,
                dataType:'json',
                beforeSend: function() {
			        
			    },
		        success: function (response) {

		        	console.log('response');
		        	console.log(response);	
		        	$("#AgentImage").removeAttr('src');
		        	$("#AgentImage").attr('src','data:image/png;base64,'+response.image_profile);
		        },
		        error: function (request, status, error) {
                	console.log(request.responseText);
            	}


    	});




}