
	$('input[name="CardNo"]').keyup(function(e){
		if($(this).val().length === 10){
			var cardno = $(this).val();
        	// alert(agentcode);return false;
        	getAgentDataByAgentCode(cardno);
		}else{
			
			if(e.which == 13) {
				var cardno = $(this).val();
				getAgentDataByAgentCode(cardno);
			}
		}
	});


function getAgentDataByAgentCode(cardno){
		var base_url = $('input[name="base_url"]').val();
			// console.log('base_url = '+base_url); return false;
			var parameter = urlParameter();

			//console.log(parameter); return false;
			var post_data = {
				'cardno':cardno,
				'seminar_no':parameter.seminarno,
				's3deal':'yes'
			};

			console.log('post get agent data');
			console.log(post_data);

			$.ajax({
		        url: base_url+"backend/admin_staff/ajaxGetAgentDataByAgentCode",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			        $('body').append('<div class="loading">Loading&#8230;</div>');
			    },
		        success: function (response) {
		        	
		        	console.log('===response===');
		        	console.log(response);
		        	if(response.status){
		        			$('form[name="scanqr-form"]').find('input,textarea').attr('readonly','readonly');

		        			setAgentForm(response);
		        			setS3Data({
		        				'data_ticket':response.data_ticket,
		        				'already_checkin':response.already_checkin,
		        				'status_ticket':response.status_ticket,
		        				'already_s3deal':response.already_s3deal
		        			});
		        			// setSeminarData(response.data_ticket,response.already_deal,response.already_checkin,response.status_ticket);

		        			// $('input[name="quantity"]').val(response.data_ticket.POSIQUANTITY);
		        			$('.loading').hide();
		        		 
		        	}else{
		        			$('.loading').hide();
			        		swal({
							title: "ผิดพลาด!",
							text: "ไม่พบข้อมูลช่างจากข้อมูลบัตรข้างต้น",
							icon: "error",
							type: "error"
							}).then(function() {
							// Redirect the user
							location.reload();
							// console.log('The Ok Button was clicked.');
							});
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});


}


function setAgentForm(response){

							$('input[name="AgentCode"]').val(response.data.AgentCode);
		        			$('input[name="AgentName"]').val(response.data.AgentName);
		        			$('input[name="AgentSurName"]').val(response.data.AgentSurName);
		        			$('input[name="emailaddress"]').val(response.data.emailaddress);
		        			$('input[name="Telephone"]').val(response.data.Telephone);
		        			$('textarea[name="R_Addr1"]').val(response.data.R_Addr1);
		        			$('input[name="R_District"]').val(response.data.R_District);
		        			$('input[name="R_Province"]').val(response.data.R_Province);
		        			$('input[name="Branch"]').val(response.data.Branch);

		        			$("#AgentImage").removeAttr('src');
		        			$("#AgentImage").attr('src','data:image/png;base64,'+response.data.Image);
}


function setS3Data(data = {}){
	var already_checkin = data.already_checkin;
	var status_ticket = data.status_ticket;


	if(data.already_s3deal){

						swal({
							title: "แจ้งเตือน!!!",
							text: "คุณได้รับอุปกรณ์ S3 แล้ว",
							icon: "warning",
							type: "warning"
							}).then(function() {
							// Redirect the user
							//location.reload();
							$('input[name="CardNo"]').removeAttr('readonly');
							// console.log('The Ok Button was clicked.');
						});

						return false;

	}


	if(status_ticket){

		if(already_checkin){
			/* send data to save into database */

			setTechnicianGotS3Device(data);
		}else{
			/* sweet alert for remind no checkin */

						swal({
							title: "เกิดข้อผิดพลาด!",
							text: "กรุณา checkin ก่อนทำการรับอุปกรณ์ S3",
							icon: "error",
							type: "error"
							}).then(function() {
							// Redirect the user
							//location.reload();
							$('input[name="CardNo"]').removeAttr('readonly');
							// console.log('The Ok Button was clicked.');
						});


		}

	}else{
		
		/* sweet alert for warning no ticket */
						swal({
							title: "เกิดข้อผิดพลาด!",
							text: "ไม่พบข้อมูลการซื้อบัตรสัมนา",
							icon: "error",
							type: "error"
							}).then(function() {
							// Redirect the user
							//location.reload();
							// console.log('The Ok Button was clicked.');
							$('input[name="CardNo"]').removeAttr('readonly');

						});

		/* eof sweet alert for warning no ticket */
	}
	


}

function setTechnicianGotS3Device(data = {}){
	var base_url = $('input[name="base_url"]').val();
	var parameter = urlParameter();

	console.log('setTechnicianGotS3Device');


	var post_data = {
		'seminarno':parameter.seminarno,
		'agentcode':data.data_ticket.AgentCode
	};

	// var post_data = {
	// 	'seminarno':'PSI_TRANSFORM_001',
	// 	'agentcode':'5620400'
	// }

	//console.log(post_data);
			$.ajax({
		        url: base_url+"backend/admin_staff/ajaxSetTechnicianGotS3Device",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			        $('body').append('<div class="loading">Loading&#8230;</div>');
			    },
		        success: function (response) {
		        	console.log('response');
		        	console.log(response);


		        	if(response.status){
		        		
			        	/* eof modal set */
			        	swal({
							title: "สำเร็จ!!",
							text: "ทำการส่งมอบอุปกรณ์ S3 สำเร็จ",
							icon: "success",
							type: "success"
							}).then(function() {
							// Redirect the user
								//location.reload();
							// console.log('The Ok Button was clicked.');
							//$('input[name="CardNo"]').removeAttr('readonly');

						});


		        	}else{

		        		swal({
							title: "เกิดข้อผิดพลาด!",
							text: "เกิดข้อผิดพลาดขณะบันทึกข้อมูล กรุณาติดต่อเจ้าหน้าที่",
							icon: "error",
							type: "error"
							}).then(function() {
							// Redirect the user
							//location.reload();
							// console.log('The Ok Button was clicked.');
							$('input[name="CardNo"]').removeAttr('readonly');

						});


		        	}

		        	$('.loading').hide();


		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            	}
            });



}