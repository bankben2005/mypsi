function getAgentDataByAgentCode(cardno){
		var base_url = $('input[name="base_url"]').val();
			// console.log('base_url = '+base_url); return false;
			var parameter = urlParameter();

			//console.log(parameter); return false;
			var post_data = {
				'cardno':cardno,
				'seminar_no':parameter.seminarno
			};

			$.ajax({
		        url: base_url+"backend/admin_staff/ajaxGetAgentDataByAgentCode",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			        $('body').append('<div class="loading">Loading&#8230;</div>');
			    },
		        success: function (response) {
		        	
		        	console.log('===response===');
		        	console.log(response);
		        	if(response.status){
		        			$('form[name="scanqr-form"]').find('input,textarea').attr('readonly','readonly');

		        			setAgentForm(response);

		        			var setSeminarObj = {
		        				'seminarData':response.data_ticket,
		        				'already_deal':response.already_deal,
		        				'already_checkin':response.already_checkin,
		        				'status_ticket':response.status_ticket,
		        				'seminar_otherplace':response.seminar_otherplace,
		        				'seat_and_room':response.seat_and_room
		        			};
		        			setSeminarData(setSeminarObj);

		        			$('input[name="quantity"]').val(response.data_ticket.POSIQUANTITY);
		        			$('.loading').hide();
		        		 
		        	}else{
		        			$('.loading').hide();
			        		swal({
							title: "ผิดพลาด!",
							text: "ไม่พบข้อมูลช่างจากข้อมูลบัตรข้างต้น",
							icon: "error",
							type: "error"
							}).then(function() {
							// Redirect the user
							location.reload();
							// console.log('The Ok Button was clicked.');
							});
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});


}

function getManualAgentDataByAgentCode(cardno,input_element){
			var closest_row = input_element.closest('.rowAgentClothDeal');
			
			var base_url = $('input[name="base_url"]').val();
			// console.log('base_url = '+base_url); return false;
			var parameter = urlParameter();

			//console.log(parameter); return false;
			var post_data = {
				'cardno':cardno,
				'seminar_no':parameter.seminarno
			};

			$.ajax({
		        url: base_url+"backend/admin_staff/ajaxGetAgentDataByAgentCode",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			        $('body').append('<div class="loading">Loading&#8230;</div>');
			    },
		        success: function (response) {
		        	console.log('response');
		        	console.log(response);


		        	if(response.data_ticket.POSIQUANTITY >= 1){
		        		closest_row.find('button.btn-select-size').removeAttr('disabled');

		        		closest_row.find('input[name="AgentNameAndSurName"]').val(response.data.AgentName+' '+response.data.AgentSurName);
			        	closest_row.find('input[name="TotalTickets"]').val(response.data_ticket.POSIQUANTITY);


			        	/* function for data modal */
			        	$('form[name="manualChooseClothSize"]').find('input[name="data_card_no"]').val(response.data.CardNo);
			        	$('form[name="manualChooseClothSize"]').find('input[name="manual_quantity"]').val(response.data_ticket.POSIQUANTITY);
			        	$('form[name="manualChooseClothSize"]').find('input[name="manual_agentcode"]').val(response.data.AgentCode);
			        	$('#manualChooseSizeModal').modal({backdrop: 'static', keyboard: false});
			        	/* eof modal set */
		        	}else{


		        	}

		        	$('.loading').hide();


		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            	}
            });



}

function setAgentForm(response){

							$('input[name="AgentCode"]').val(response.data.AgentCode);
		        			$('input[name="AgentName"]').val(response.data.AgentName);
		        			$('input[name="AgentSurName"]').val(response.data.AgentSurName);
		        			$('input[name="emailaddress"]').val(response.data.emailaddress);
		        			$('input[name="Telephone"]').val(response.data.Telephone);
		        			$('textarea[name="R_Addr1"]').val(response.data.R_Addr1);
		        			$('input[name="R_District"]').val(response.data.R_District);
		        			$('input[name="R_Province"]').val(response.data.R_Province);
		        			$('input[name="Branch"]').val(response.data.Branch);

		        			$("#AgentImage").removeAttr('src');
		        			$("#AgentImage").attr('src','data:image/png;base64,'+response.data.Image);
}

function setSeminarData(seminarObj){
	console.log('seminarData');
	console.log(seminarData);

	var seminarData  = seminarObj.seminarData;
	var already_deal  = seminarObj.seminarData;
	var already_checkin = seminarObj.already_checkin;
	var status_ticket = seminarObj.status_ticket;
	var seminar_otherplace = seminarObj.seminar_otherplace;
	var seat_and_room = seminarObj.seat_and_room;



	if(status_ticket){

	var txt_already_deal = "";
	var txt_already_checkin = "";

	if(already_deal){
		txt_already_deal = '<label class="label label-success">รับเสื้อแล้ว</label>';

	}else{
		txt_already_deal = '<label class="label label-warning">ยังไม่ได้รับเสื้อ</label>';
	}

	if(already_checkin){
		txt_already_checkin = '<label class="label label-success">ลงทะเบียนเข้างานแล้ว</label>';
	}else{
		txt_already_checkin = '<label class="label label-warning">ยังไม่ได้ลงทะเบียนเข้างาน </label> <span style="color:red;">(กรุณาลงทะเบียนเข้างานก่อนรับเสื้อ)</span>';
	}

	var txt_append = '<div class="panel panel-default">'
	txt_append += '<div class="panel-heading"><h4>ข้อมูลบัตรสัมนา</h4></div>';
		txt_append += '<div class="panel-body">';
			txt_append += '<div class="col-md-6">';
								txt_append += '<p><strong>ชื่อบัตร : </strong>'+seminarData.MPNAME+'</p>';
								txt_append += '<p><strong>จำนวนบัตร : </strong>'+seminarData.POSIQUANTITY+' ใบ</p>';
								txt_append += '<input type="hidden" name="hide_quantity" value="'+seminarData.POSIQUANTITY+'" />';
								txt_append += '<p><strong>สถานะการรับเสื้อ : </strong> '+txt_already_deal+'</p>';
								txt_append += '<p><strong>สถานะการลงทะเบียนเข้างาน (Checkin) : </strong>'+txt_already_checkin+'</p>';


								var obj_seat_and_room = JSON.stringify(seat_and_room); 
								var seat_no = (seat_and_room.has_seat_and_room)?seat_and_room.data.SeatNo:'';
								var room_no = (seat_and_room.has_seat_and_room)?seat_and_room.data.RoomNo:'';

								txt_append += '<div class="panel panel-info">';
									txt_append += '<div class="panel-body">';
											txt_append += '<p><strong>หมายเลขที่นั่ง : </strong><input name="SeatNo" value="'+seat_no+'"   class="form-control"></p>';
											txt_append += '<p><strong>หมายเลขห้อง : </strong><input name="RoomNo" value="'+room_no+'"   class="form-control"></p>';
											txt_append += '<a class="btn btn-block btn-success" onclick="submitSeatAndRoomForm(this)" data-seminar="'+seat_and_room.seminarno+'" data-agentcode="'+seat_and_room.agentcode+'"><i class="fa fa-floppy-o"></i> บันทึกข้อมูลที่นั่งและเลขห้อง</a>';		
									txt_append += '</div>';
								txt_append += '</div>';
					
			txt_append += '</div>';
			// txt_append += '<div class="col-md-6">';
			// 					txt_append += '<p><strong>วันที่ซื้อบัตร : </strong>'+seminarData.POSTDATE+'</p>';
			// 					txt_append += '<p><strong>ราคาบัตร : </strong>'+seminarData.POSIPRICE+' บาท</p>';
								
			// txt_append += '</div>';
			txt_append += '<div class="clearfix"></div>';

			txt_append += '<div class="col-md-4 pull-right">';
			txt_append += (!already_checkin)?'<button type="button" class="btn btn-success pull-right" id="btn-checkin" onclick="checkinWithButton(this)"><i class="fa fa-map-marker"></i> เช็คอิน</button>':'';
			txt_append += (!already_deal && already_checkin)?'<button type="button" class="btn btn-success btn-block" onclick="confirmClothDeals(this)"><i class="fa fa-user-plus"></i> ยืนยันการรับเสื้อ</button>':'';
			txt_append += (already_checkin && already_deal)?'<button type="button" class="btn btn-default btn-block"  onclick="location.reload();">ลงข้อมูลท่านถัดไป <i class="fa fa-arrow-right"></i></button>':'';
			txt_append += '</div>';
			txt_append += '<div class="clearfix"></div>';

			// txt_append += '<div class="col-md-6">';
			// txt_append += '<button class="btn btn-success btn-block">ยืนยันการรับเสื้อ</button>';
			// txt_append += '</div>';
			
	txt_append += '</div>';
	//txt_append += '</div>';
	txt_append += '<div>';

	}else{
		var txt_append = "";

		txt_append += '<div class="alert alert-warning">';
			txt_append += '<strong>แจ้งเตือน !</strong>';
			txt_append += '<p>ไม่พบข้อมูลสิทธิการเข้างาน หรือข้อมูลการซื้อบัตร</p>';
		txt_append += '</div>';

		if(seminar_otherplace.length > 0){
			txt_append += '<div class="alert alert-danger">';
				txt_append += '<strong>พบข้อมูลการซื้อบัตรสมนาสถานที่อื่นดังนี้!!</strong>';

				txt_append += '<ul>';
				$.each(seminar_otherplace,function(index,value){
					var bought_date = value['POSTDATE'].split(" ");
					txt_append += '<li>'+value['MPDESCRIPTION']+' / จำนวน '+value['POSIQUANTITY']+' ใบ / ซื้อเมื่อ '+bought_date[0]+'</li>';
				});
				txt_append += '</ul>';

			txt_append += '</div>';
		}


	}

	$('#seminarinfo').html('').html(txt_append);


}

function saveClothDeals(post_data){
		var base_url = $('input[name="base_url"]').val();
		$.ajax({
		        url: base_url+"backend/admin_staff/ajaxSaveClothDeals",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			       
			    },
		        success: function (response) {
		        	
		        	console.log('===response===');
		        	console.log(response);
		        	if(response.status){

		        			if(!post_data.manual){
		        				$('#chooseSizeModal').modal('toggle');
		        			}else{
		        				$('#manualChooseClothSize').modal('toggle');
		        			}

		        		 	swal({
								title: "สำเร็จ!",
								text: "บันทึกข้อมูลการรับเสื้อเข้าสัมนาสำเร็จ",
								icon: "success",
								type: "success"
								}).then(function() {
								// Redirect the user
								if($('input[name="CardNo"]').val() != ""){
									location.reload();
								}else{
									$('.modal').modal('hide');

									$('.rowAgentClothDeal').each(function(index,element){
										if($(element).find('input[name="CardNoManual"]').val() != ""){
											$(element).find('input[name="CardNoManual"]').val('');
											$(element).find('input[name="AgentNameAndSurName"]').val('');
											$(element).find('input[name="TotalTickets"]').val('');
										}

									});
									// $('form[name="manual-form"]').find('.rowAgentClothDeal:first-child').remove();
								}
								// console.log('The Ok Button was clicked.');
							});
		        	}else{
		        			
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                		console.log(request.responseText);
            	}


    			});


}


function checkinWithButton(element){
		var element = $(element);

		var txt_button = element.html();
		txt_button += ' <i class="fa fa-spin fa-spinner></i>';

		// console.log(txt_button);
		element.attr('disabled','disabled');
		element.html('').html(txt_button);

		var post_data = {
				'agentcode':$('input[name="AgentCode"]').val(),
				'cardno':$('input[name="CardNo"]').val()

		};

		
			var base_url = $('input[name="base_url"]').val();
			$.ajax({
		        url: base_url+"backend/admin_staff/ajaxCheckinOnline",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			       
			    },
		        success: function (response) {
		        	console.log('--- response ---');
		        	console.log(response);

		        	if(response.status){

		        			getAgentDataByAgentCode(response.cardno);


		        	}

		        },
		        error: function (request, status, error) {
                		console.log(request.responseText);
            	}
            });


}



$(document).ready(function(){
	$('input[name="CardNo"]').focus();


	// $('input[name="agentcode"]').on('keypress',function(){

	// 	alert($(this).vak());
	// });
	// $('input[name="CardNo"]').on({
 //    keyup: function() { typed_into = true; },
 //    change: function() {
 //        if (typed_into) {
 //        	var cardno = $(this).val();
 //        	// alert(agentcode);return false;
 //        	getAgentDataByAgentCode(cardno);
 //            //alert('type');
 //            typed_into = false; //reset type listener
 //        } else {
 //            alert('not type');
 //        }
 //    }
	// });

	$('input[name="CardNo"]').keyup(function(e){
		if($(this).val().length === 10){
			var cardno = $(this).val();
        	// alert(agentcode);return false;
        	getAgentDataByAgentCode(cardno);
		}else{
			if(e.which == 13) {
				var cardno = $(this).val();
				getAgentDataByAgentCode(cardno);
			}
		}
	});




	$('button.plus-btn').click(function(e){
			var limit_cloth = parseInt($('input[name="quantity"]').val());

			var summary_all = 0;
			/* get all quantity */
			$('input.quantity').each(function(index,element){
					summary_all += parseInt($(element).val());
			});
			/* eof get all quantity*/

			// console.log('summary_all');
			// console.log(summary_all);
			var input_val = parseInt($(this).parent().find('input').val());

			//input_val += 1;

			if((summary_all+1) <= limit_cloth){
					$(this).parent().find('input').val('').val(input_val+1);
			}

	});

	$('button.minus-btn').click(function(e){
			var limit_cloth = parseInt($('input[name="quantity"]').val());


			/* get all quantity */
			$('input.quantity').each(function(index,element){

			});
			/* eof get all quantity */

			var input_val = parseInt($(this).parent().find('input').val());

			input_val -= 1;

			if(input_val >= 0){
					$(this).parent().find('input').val('').val(input_val);
			}

	});

	$('button.btn-submit-clothdeal').click(function(e){

			var origin_txt = $(this).html();

			origin_txt += ' <i class="fa fa-spin fa-spinner"></i>';
			$(this).html('').html(origin_txt);
			$(this).attr('disabled','disabled');
			var limit_cloth = parseInt($('input[name="quantity"]').val());

			// var limit_cloth = parseInt($('input[name="quantity"]').val());

			var summary_all = 0;
			/* get all quantity */
			$('input.quantity').each(function(index,element){
					summary_all += parseInt($(element).val());
			});

			if(summary_all != limit_cloth){
				var alert_txt = '<div class="alert alert-error">';
					alert_txt += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
					alert_txt += '<span aria-hidden="true">×</span>';
					alert_txt += '</button>';
					alert_txt += '<p>กรุณาเลือกไซต์เสื้อให้ครบจำนวน</p>';
					alert_txt += '</div>';

				$('#modal-alert').html('').html(alert_txt);

				$(this).removeAttr('disabled');
				$(this).find('i.fa').remove();
			}else{

						var post_data = {};
						var clothSize = [];
						var parameter = urlParameter();
						/* this case go to save */
						$('input').each(function(index,element){
								var data = {};
							 	var element = $(element);
								if(element.hasClass('quantity') && element.val() != 0){
									data = {
										'size':element.attr('name'),
										'amount':element.val()
									};
									clothSize.push(data);
								}
						});

						post_data.clothSize = clothSize;
						post_data.agentcode = $('input[name="AgentCode"]').val();
						post_data.seminarno = parameter.seminarno;
						post_data.manual = false;

						saveClothDeals(post_data);
					/* eof save seminar dealcloth*/


			}

	});


	/* for manual form */

			$('button.manual-plus-btn').click(function(e){
				var limit_cloth = parseInt($('input[name="manual_quantity"]').val());

				var summary_all = 0;
				/* get all quantity */
				$('input.m_quantity').each(function(index,element){
						summary_all += parseInt($(element).val());
				});
				/* eof get all quantity*/

				// console.log('summary_all');
				// console.log(summary_all);
				var input_val = parseInt($(this).parent().find('input').val());

				//input_val += 1;

				if((summary_all+1) <= limit_cloth){
						$(this).parent().find('input').val('').val(input_val+1);
				}

			});

			$('button.manual-minus-btn').click(function(e){
				var limit_cloth = parseInt($('input[name="manual_quantity"]').val());


				/* get all quantity */
				$('input.m_quantity').each(function(index,element){

				});
				/* eof get all quantity */

				var input_val = parseInt($(this).parent().find('input').val());

				input_val -= 1;

				if(input_val >= 0){
						$(this).parent().find('input').val('').val(input_val);
				}

			});

			$('button.btn-submitmanual-clothdeal').click(function(e){

				//console.log('aaaa');
				var origin_txt = $(this).html();

				origin_txt += ' <i class="fa fa-spin fa-spinner"></i>';
				$(this).html('').html(origin_txt);
				$(this).attr('disabled','disabled');
				var limit_cloth = parseInt($('input[name="manual_quantity"]').val());

				// var limit_cloth = parseInt($('input[name="quantity"]').val());

				//console.log(limit_cloth);return false;
				/* get all quantity */
				var summary_all = 0;
				$('input.m_quantity').each(function(index,element){
						summary_all += parseInt($(element).val());
				});

				if(summary_all !== limit_cloth){
					var alert_txt = '<div class="alert alert-error">';
						alert_txt += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
						alert_txt += '<span aria-hidden="true">×</span>';
						alert_txt += '</button>';
						alert_txt += '<p>กรุณาเลือกไซต์เสื้อให้ครบจำนวน</p>';
						alert_txt += '</div>';

					$('#modal-manual-alert').html('').html(alert_txt);

					$(this).removeAttr('disabled');
					$(this).find('i.fa').remove();
				}else{

						var post_data = {};
						var clothSize = [];
						var parameter = urlParameter();
						/* this case go to save */
						$('input').each(function(index,element){
								var data = {};
							 	var element = $(element);
								if(element.hasClass('m_quantity') && element.val() != 0){
									data = {
										'size':element.attr('name'),
										'amount':element.val()
									};
									clothSize.push(data);
								}
						});

						post_data.clothSize = clothSize;
						post_data.agentcode = $('input[name="manual_agentcode"]').val();
						post_data.seminarno = parameter.seminarno;
						post_data.manual = true;

						saveClothDeals(post_data);
						// console.log(post_data); return false;

				}

			});

		$('.btn-add-rows').click(function(e){
				var txt_form = "";
				txt_form += '<div class="rowAgentClothDeal">';
				txt_form += $('.rowAgentClothDeal').html();
				txt_form += '</div>';

                $('form[name="manual-form"]').append(txt_form);



		});

		$('input[name="CardNoManual"]').keyup(function(){
			var cardno_val = $(this).val();
			if(cardno_val.length === 10){
				var input_element = $(this);

				$('button.btn-submitmanual-clothdeal').removeAttr('disabled');
				$('button.btn-submitmanual-clothdeal').find('.fa').remove();

				getManualAgentDataByAgentCode(cardno_val,input_element);
			}
		});


		//$('')


	/* eof for manual form*/




});

function cardNoManualKeyup(element){
	var element = $(element);
			var cardno_val = element.val();
			if(cardno_val.length === 10){
				var input_element = element;
				getManualAgentDataByAgentCode(cardno_val,input_element);
			}

}


function urlParameter(){
    var url = window.location.href,
    retObject = {},
    parameters;

    if (url.indexOf('?') === -1) {
        return null;
    }

    url = url.split('?')[1];

    parameters = url.split('&');

    for (var i = 0; i < parameters.length; i++) {
        retObject[parameters[i].split('=')[0]] = parameters[i].split('=')[1];
    }

    return retObject;
}


function confirmClothDeals(element){
	var element = $(element);


	var url = window.location.href,
    retObject = {},
    parameters;

    if (url.indexOf('?') === -1) {
        return null;
    }

    url = url.split('?')[1];

    parameters = url.split('&');

    for (var i = 0; i < parameters.length; i++) {
        retObject[parameters[i].split('=')[0]] = parameters[i].split('=')[1];
    }

    //console.log(retObject);

	var post_data = {
		'CardNo':$('input[name="CardNo"]').val(),
		'AgentCode':$('input[name="AgentCode"]').val(),
		'AgentName':$('input[name="AgentName"]').val(),
		'AgentSurName':$('input[name="AgentSurName"]').val(),
		'emailaddress':$('input[name="emailaddress"]').val(),
		'Telephone':$('input[name="Telephone"]').val(),
		'R_Addr1':$('textarea[name="R_Addr1"]').val(),
		'R_District':$('input[name="R_District"]').val(),
		'R_Province':$('input[name="R_Province"]').val(),
		'Branch':$('input[name="Branch"]').val(),
		'SeminarNo':retObject.seminarno,
		'quantity':$('input[name="hide_quantity"]').val()
	};

			var base_url = $('input[name="base_url"]').val();
			$.ajax({
		        url: base_url+"backend/admin_staff/ajaxConfirmClothDeals",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			       
			    },
		        success: function (response) {
		        	console.log('--- response ---');
		        	console.log(response);

		        	if(response.status){
		        		getAgentDataByAgentCode(response.post_data.CardNo);
		        	}

		        },
		        error: function (request, status, error) {
                		console.log(request.responseText);
            	}
            });





	//console.log(post_data);
}


function setSeatAndRoomNoModal(element){
	var element = $(element);

	var json_data = element.data('json');
	$('#modalSetSeatAndRoomNo').modal('toggle');

	if(json_data.has_seat_and_room){


	}


}

function setSeatNoAndRoomNo(element){
	var element = $(element);

	var seat_no = $('input[name="SeatNo"]').val();
	var room_no = $('input[name="RoomNo"]').val();

	var post_data = {
		'seat_no':seat_no,
		'room_no':room_no,
		'seminar_no':element.data('seminar'),
		'agentcode':element.data('agentcode')
	}

	console.log(post_data);

		var base_url = $('input[name="base_url"]').val();
			$.ajax({
		        url: base_url+"backend/admin_staff/ajaxSetSeminarSeats",
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			       
			    },
		        success: function (response) {
		        	console.log('--- response ---');
		        	console.log(response);

		        	if(response.status){ 
		        		swal({
							title: "สำเร็จ!",
							text: "บันทึกข้อมูลที่นั่งและหมายเลขห้องสำเร็จ",
							icon: "success",
							type: "success"
						}).then(function() {

						});
		        	}else{
		        		swal({
							title: "แจ้งเตือน!",
							text: response.result_desc,
							icon: "warning",
							type: "warning"
						}).then(function() {

						});
		        	}

		        },
		        error: function (request, status, error) {
                		console.log(request.responseText);
            	}
            });

	console.log(post_data);
}


function submitSeatAndRoomForm(element){
	var el = $(element);

	var post_data = {
		'seat_no':$('input[name="SeatNo"]').val(),
		'room_no':$('input[name="RoomNo"]').val()
	}

	if(post_data.seat_no === ''){
		$('input[name="SeatNo"]').focus();
		return false;
	}

	if(post_data.room_no === ''){
		$('input[name="RoomNo"]').focus();
		return false;
	}

	setSeatNoAndRoomNo(el);

	console.log(post_data);
}


