$(document).ready(function(){
	console.log('abcd');

	validatorForm();
	checkGetCampaignFrom();
});

function checkGetCampaignFrom(){
	jQuery('input[name="get_campaign_from[]"]').click(function(e){
		var get_value = jQuery(this).val();
		if(get_value === 'other'){
			if(jQuery(this).is(':checked')){
				jQuery('#type-other-desc').show();
			}else{
				jQuery('#type-other-desc').hide();
			}
			
		}
	});
}



function validatorForm(){
	var base_url = jQuery('input[name="base_url"]').val();
	jQuery('form[name="register-free-purifier"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			firstname: {
				validators: {
					notEmpty: {
						message: ''
					}
				}
			},
			lastname:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			telephone:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			place_name:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			place_telephone:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			place_address:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			select_province:{
				validators:{
					callback:{
						message:'กรุณาเลือกจังหวัด',
						callback:function(value,validator,$field){
							if(value == null || value == '0' || value == ''){
								return false;
							}else{
								return true;
							}
						}
					}
				}
			},
			select_amphur:{
				validators:{
					callback:{
						message:'กรุณาเลือกอำเภอ',
						callback:function(value,validator,$field){
							if(value == null || value == '0'){
								return false;
							}else{
								return true;
							}
						}
					}
				}
			},
			select_district:{
				validators:{
					callback:{
						message:'กรุณาเลือกตำบล',
						callback:function(value,validator,$field){
							if(value == null || value == '0'){
								return false;
							}else{
								return true;
							}
						}
					}
				}
			},
			zipcode:{
				trigger: 'change keyup',
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			place_coordinator:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			place_coordinator_telephone:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			}
		},
		onSuccess: function(e, data) {

			e.preventDefault();
		// 	$('#submitButton').prop('disabled', true);
  // $('#submitButton').attr("disabled", "disabled");
  // jQuery('button[type="submit"]').prop('disabled',true);
  // jQuery('button[type="submit"]').attr('disabled','disabled');

			var register_form = jQuery('form[name="register-free-purifier"]');

			var form_data = register_form.serialize();

			saveSubmitPurifierRegister(form_data);
			// return false;

			// console.log('form data here');
			// console.log(form_data);


		}
	});

}

function saveSubmitPurifierRegister(form_data){

	var base_url = jQuery('input[name="base_url"]').val();
	// console.log(form_data);
	jQuery.ajax({
		type: "POST",
		url: base_url+"campaign/FreeWaterPurifier/ajaxSaveRegisterPurifier",
		async:true,
		dataType:'json',
		data:form_data,
		beforeSend: function( xhr ) {
		},
		success: function(response){
			console.log('response');
			console.log(response);
			if(response.status){

				

				// swal.fire({
				//   title: "สำเร็จ!", 
				//   html: "ยืนเรื่องสมัครเข้าร่วมโครงการสำเร็จ "+success_line_two,
				//   confirmButtonText: "V <u>redu</u>", 
				// });
				const wrapper = document.createElement('p');
                wrapper.innerHTML = "ทางโครงการจะติดต่อกลับไปโดยเร็วที่สุด";

					
					swal({
						html:true,
	                    title: "สำเร็จ!",
	                    text: "ยืนเรื่องสมัครเข้าร่วมโครงการสำเร็จ ",
	                    content:wrapper,
	                    icon: "success"
                    }).then(function() {
                      // location.reload();
                    });
			}

		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});
}


function changeProvince(province_id){
	var base_url = jQuery('input[name="base_url"]').val();

	jQuery.ajax({
		type: "POST",
		url: base_url+"campaign/FreeWaterPurifier/getAmphurByProvinceId",
		async:true,
		dataType:'json',
		data:{'province_id':province_id},
		beforeSend: function( xhr ) {
			jQuery('select[name="select_amphur"]').html('');
			jQuery('select[name="select_district"]').html('');
			jQuery('input[name="zipcode"]').val('');

			jQuery('select[name="select_district"]').attr('readonly','readonly');
			jQuery('input[name="zipcode"]').attr('readonly','readonly');
		},
		success: function(response){
			console.log('response');
			console.log(response);
			if(response.status){
				jQuery.each(response.data, function(key, value) {   
					jQuery('select[name="select_amphur"]')
					.append(jQuery("<option></option>")
						.attr("value",key)
						.text(value)); 
				});
				jQuery('select[name="select_amphur"]').removeAttr('readonly');
				jQuery('select[name="select_amphur"]').focus();

				if(jQuery('input[name="hide_amphur"]').val()){
					jQuery('select[name="select_amphur"]').val(jQuery('input[name="hide_amphur"]').val()).change();
				}
			}

		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}
	});


}

function changeAmphur(amphur_id){

                if(amphur_id != '0'){
                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"campaign/FreeWaterPurifier/getDistrictByAmphurId",
                          async:true,
                          dataType:'json',
                          data:{'amphur_id':amphur_id,'province_id':jQuery("select[name='select_province']").val()},
                          beforeSend: function( xhr ) {
                                jQuery('select[name="select_district"]').html('');
                                jQuery('input[name="zipcode"]').val('');
                                jQuery('input[name="zipcode"]').attr('readonly','readonly');
                          },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                                jQuery.each(response.data, function(key, value) {   
                                                     jQuery('select[name="select_district"]')
                                                         .append(jQuery("<option></option>")
                                                                    .attr("value",key)
                                                                    .text(value)); 
                                                });
                                                jQuery('select[name="select_district"]').removeAttr('readonly');
                                                jQuery('select[name="select_district"]').focus();
                                                //changeDistrict(jQuery('select[name="district"]').val());
                                  if(jQuery('input[name="hide_district"]').val()){
                                    jQuery('select[name="select_district"]').val(jQuery('input[name="hide_district"]').val()).change();
                                }
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
                }


}

function changeDistrict(district_id){

            if(district_id != '0'){
                var province_id = jQuery("select[name='select_province']").val();
                var amphur_id = jQuery("select[name='select_amphur']").val();

                var post_data = {
                    'province_id':province_id,
                    'amphur_id':amphur_id,
                    'district_id':district_id

                }
                var base_url = jQuery("input[name='base_url']").val();

                    jQuery.ajax({
                          type: "POST",
                          url: base_url+"campaign/FreeWaterPurifier/getZipcodeByDistrictId",
                          async:true,
                          dataType:'json',
                          data:post_data,
                          beforeSend: function( xhr ) {
                                           jQuery('input[name="zipcode"]').val('');
                                      },
                          success: function(response){
                            console.log('response');
                            console.log(response);
                            if(response.status){
                            	jQuery('input[name="zipcode"]').removeAttr('readonly');
                                jQuery('input[name="zipcode"]').val(response.zipcode).change();
                                
                                // jQuery('input[name="zipcode"]').focus();
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });

                }


}