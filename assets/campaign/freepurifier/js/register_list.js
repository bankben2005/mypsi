$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
function clickAddRemark(element){
		
		var element = $(element);

		$('input[name="hide_register_id"]').val(element.data('registerid'));
		$('textarea[name="remark"]').val(element.data('remark'));

		$('#remarkModal').modal('toggle');
	}


	function saveRemark(element){
		var element = $(element);
		var base_url = $('input[name="base_url"]').val();

		var post_data = {
			'register_id':$('input[name="hide_register_id"]').val(),
			'remark':$('textarea[name="remark"]').val()
		};

		// console.log(post_data); return false;

		$.ajax({
            url: base_url+'campaign/FreeWaterPurifier/ajaxSaveRemark',
            type: "post",
            data: post_data ,
            async:true,
            dataType:'json',
            beforeSend:function(){
            	$('#remarkModal').modal('toggle');
            },
            success: function (response) {
                  console.log('== response ==');
                  console.log(response);
                  if(response.status){

                  $('span.show-remark[data-registerid="'+post_data.register_id+'"]').html('').html(post_data.remark);
                  $('a[data-registerid="'+post_data.register_id+'"]').data('remark',post_data.remark);

                    swal({
	                    title: "สำเร็จ!",
	                    text: "อัพเดทข้อมูลรายละเอียดเพิ่มเติมสำเร็จ",
	                    icon: "success"
                    }).then(function() {
                      
                    });
                  }
                   // you will get response from your php page (what you echo or print)                 

            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }


      	});

		console.log(element);
	}