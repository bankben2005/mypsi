	
$(document).ready(function(){
	$.fn.bootstrapValidator.validators.checkzero = {
        validate: function(validator, $field, options) {
            var value = $field.val();

            if(value != '0') {
                return {
                    valid: true,    // or false
                }
            }

            return {
                valid: false,
                message : ''
            }
        }
    };


	var base_url = $('input[name="base_url"]').val();
	$('form[name="insurance-register-form"]').bootstrapValidator({
			message: 'This value is not valid',
	        // feedbackIcons: {
	        //     valid: 'glyphicon glyphicon-ok',
	        //     invalid: 'glyphicon glyphicon-remove',
	        //     validating: 'glyphicon glyphicon-refresh'
	        // },
	        fields: {
	        	firstname: {
	                validators: {
	                    notEmpty: {
	                        message: ''
	                    }
	                }
	            },
	            lastname: {
	                validators: {
	                    notEmpty: {
	                        message: ''
	                    }
	                }
	            },
	            telephone:{
	            	validators:{
	            		notEmpty:{
	            			message:''
	            		}
	            	}
	            },
	            address_house_no:{
	            	validators:{
	            		notEmpty:{
	            			message:''
	            		}
	            	}
	            },
	            // id_card:{
	            // 	validators:{
	            // 		notEmpty:{
	            // 			message:''
	            // 		},
	            // 		stringLength: {
	            // 			min:13,
	            // 			max:13,
	            // 			message:'โปรดระบุค่าตัวอักษร 13 ตัวอักษร'
	            // 		},
	            // 		integer:{
	            // 			message:''
	            // 		}
	            // 	}
	            // },
	            email:{
	            	validators:{
	            		notEmpty:{
	            			message:''
	            		},
	            		emailAddress:{
	            			message:''
	            		}
	            	}
	            },
	            province_id:{
	            	validators:{
	            		notEmpty:{
	            			message:''
	            		}
	            	}
	            },
	            amphur_id:{
	            	validators:{
	            		notEmpty:{
	            			message:''
	            		}
	            	}
	            },
	            district_id:{
	            	validators:{
	            		notEmpty:{
	            			message:''
	            		}
	            	}
	            },
	            zipcode:{
	            	validators:{
	            		notEmpty:{
	            			message:''
	            		}
	            	}
	            },
	            accept_condition:{
	            	validators:{
	            		notEmpty:{
	            			message:''
	            		}
	            	}
	            }
	        }


	});

	$('input[name="dob"]').daterangepicker({
        autoApply: true,
        showDropdowns: true,
        timePicker: false,
        singleDatePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'DD-MM-YYYY'
        }
    });

    	/* check */
    	

});


function checkUsernameClick(element){
	var element = $(element);
	var customer_username = $('input[name="customer_username"]');
	if(customer_username.val() == ''){
		customer_username.focus();
		return false;
	}
	var btn_text = element.html();
	btn_text += ' <i class="fa fa-spin fa-spinner"></i>';
	element.attr('disabled','disabled');
	element.html('').html(btn_text);

	var post_data = {
		customer_username:customer_username.val()
	};
	setConsumerIdByUsername(post_data);

}

function setConsumerIdByUsername(post_data){
		var base_url = $('input[name="base_url"]').val();
					$.ajax({
                        url: base_url+"campaign/tip/ajaxGetConsumerDataByUsername",
                        type: "post",
                        data: post_data ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {

                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);

                            if(response.status && response.already_register){
                            	window.location.href =base_url+'campaign/tip/already_register';
                            }else if(response.status && !response.already_register){
                           
                            	$('input[name="ConsumerID"]').val(response.ConsumerID);
                            	$('div#panel-register').css({'display':''});
                            	$('div#panel-check').fadeOut('slow');
                            			swal({
										title: "ยินดีด้วย!",
										text: "คุณมีสิทธิ์รับสิทธิ์จากโครงการ \r\n กรุณากรอกข้อมูลส่วนตัวของคุณ",
										html:true,
										icon: "success",
										type: "success"
										}).then(function() {
												
										});
                            	
                            	$('input[name="telephone"]').val(response.telephone);
                            	$('input[name="email"]').val(response.email);
                            }else if(!response.status && response.flag == 'not_found_customer'){
                            			swal({
										title: "เกิดข้อผิดพลาด!",
										text: "ไม่พบผู้ใช้งานระบบ, กรุณาตรวจสอบและลองใหม่อีกครั้ง",
										html:true,
										icon: "error",
										type: "error"
										}).then(function() {
												$('button#btn-check').removeAttr('disabled');
												$('button#btn-check').find('i').remove();

										});
                            }else if(!response.status && response.flag == 'not_found_registerproduct'){
                            			swal({
										title: "ไม่พบข้อมูลการลงทะเบียนสินค้า !",
										text: "โครงการนี้คุ้มครองเฉพาะลูกค้าที่ลงทะเบียนสินค้าตั้งแต่วันที่ 16 พฤษภาคม 2561 เป็นต้นไป.",
										html:true,
										icon: "error",
										type: "error"
										}).then(function() {
												$('button#btn-check').removeAttr('disabled');
												$('button#btn-check').find('i').remove();
										});
                            }
                            
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });
}

function setConsumerId(consumer_id){
	var base_url = $('input[name="base_url"]').val();
	if(consumer_id){
		$('div#panel-register').css({'display':''});
		$('input[name="ConsumerID"]').val(consumer_id);

					$.ajax({
                        url: base_url+"campaign/tip/ajaxGetConsumerDataByConsumerID",
                        type: "post",
                        data: {'ConsumerID':consumer_id} ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                        	$('div.loading_before_register').show();
                        },	
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	$('div.loading_before_register').hide();
                            	if(response.already_register){
                            		window.location.href =base_url+'campaign/tip/already_register';
                            	}
                            	//$('input[name="firstname"]').val(response.firstname);
                            	//$('input[name="lastname"]').val(response.lastname);
                            	$('input[name="telephone"]').val(response.telephone);
                            	$('input[name="email"]').val(response.email);
                            }
                            
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });


	}else{

		$('div#panel-register').css({'display':'none'});
	}
}


function setConsumerIdForCheck(consumer_id){
	var base_url = $('input[name="base_url"]').val();
	if(consumer_id){
		$('input[name="ConsumerID"]').val(consumer_id);
					$.ajax({
                        url: base_url+"campaign/tip/ajaxGetConsumerDataForCheck",
                        type: "post",
                        data: {'ConsumerID':consumer_id} ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                        	
                        },	
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	$('input[name="customer_username"]').val(response.customer_data.UserName);
                            	$('button#btn-check').trigger('click');
                            }
                            
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });

	}
}


function changeProvince(province_id){

		var base_url = $('input[name="base_url"]').val();

					$.ajax({
                        url: base_url+"campaign/tip/ajaxGetAmphurByProvince",
                        type: "post",
                        data: {'province_id':province_id} ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                        		$('select[name="amphur_id"]').html('');
							    $('select[name="district_id"]').html('');
							    $('input[name="zipcode"]').val('');

							    $('select[name="district_id"]').attr('readonly','readonly');
							    $('input[name="zipcode"]').attr('readonly','readonly');
                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	$.each(response.data, function(key, value) {   
								     $('select[name="amphur_id"]')
								         .append($("<option></option>")
								                    .attr("value",key)
								                    .text(value)); 
								});
								$('select[name="amphur_id"]').removeAttr('readonly');
								$('select[name="amphur_id"]').focus();
                            }
                            
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });


}

function changeAmphur(amphur_id){
	var base_url = $('input[name="base_url"]').val();

					$.ajax({
                        url: base_url+"campaign/tip/ajaxGetDristrictByAmphur",
                        type: "post",
                        data: {'amphur_id':amphur_id} ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                        		$('select[name="district_id"]').html('');
							    $('input[name="zipcode"]').val('');
							    $('input[name="zipcode"]').attr('readonly','readonly');
                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	$.each(response.data, function(key, value) {   
								     $('select[name="district_id"]')
								         .append($("<option></option>")
								                    .attr("value",key)
								                    .text(value)); 
								});
								$('select[name="district_id"]').removeAttr('readonly');
								$('select[name="district_id"]').focus();
                            }
                            
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });

}

function changeDistrict(district_id){

	var base_url = $('input[name="base_url"]').val();

	var post_data = {
		'province_id':$('select[name="province_id"]').val(),
		'amphur_id':$('select[name="amphur_id"]').val(),
		'district_id':district_id

	};
					$.ajax({
                        url: base_url+"campaign/tip/ajaxGetZipcodeByDistrict",
                        type: "post",
                        data: post_data ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                        		$('input[name="zipcode"]').val('');
                        },
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	$('input[name="zipcode"]').val(response.zipcode).change();
                                $('input[name="zipcode"]').removeAttr('readonly');
                            }
                            
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });
}