/**
 * @author Kridsada Phudpetkaew <kridsada@psisat.com>
 */
      $(document).ready(function() {

        // var pathArray = window.location.pathname.split( '/' );
        // console.log('pathArray');
        // console.log(pathArray);

        
      			$("#search-agent").submit(function(e){
      					 

                  /* Check if has province and doesn't has amphur */
                  if(!(checkIsProvinceNoAmphur())){
                    //return false;
                  }
                  /* eof check province and amphur*/

      					  /* Check if filter has data should also insert filter to form */
      					  var countFilter = $('input[name="agent_type"]:checked').length;
      					  var form_text = '';
      					  if(parseInt(countFilter) > 0){
      					  		 $('input[name="agent_type"]:checked').each(function(index,el){
	      							var agent_val = $(el).val();
	      							form_text += '<input type="hidden" name="'+agent_val+'" value="true" />';
      							});
      					  }
      					  $(this).append(form_text);
      					  /* eof check if filter */


      					  // e.preventDefault();
      					  /* disable button and add spinner to button */
      					  var submit_btn = $(this).find('button[type="submit"]');

      					  submit_btn.find('.fa').remove();
      					  var btn_txt = submit_btn.html();
      					  btn_txt += ' <i class="fa fa-spinner fa-spin"></i>';
      					   submit_btn.html('').html(btn_txt);

      					  /* eof disable button */
      					
						  var checkBlank = false;
						  var countInput = $('#search-agent :input').length;
						  var countNull = 0;
						  var countNotNull = 0;
						  // console.log('countInput = '+countInput); return false; 
						  $('#search-agent :input').each(function(index, el)
						  {
						  	//console.log($(el).val());
						    if ($(el).val() == ""){
						    	checkBlank = true; //they're not all blank anymore
						    	countNull++;
							}else{
								countNotNull++;
							}
						  });
						 submit_btn.attr('disabled','disabled');
						  
						  if(countNull == countInput){
						  	submit_btn.removeAttr('disabled');
						  	submit_btn.find('.fa').remove();
						  	var btn_txt = submit_btn.html();
						  	btn_txt = ' <i class="fa fa-search"></i> '+btn_txt;
						  	submit_btn.html('').html(btn_txt);
						  	$('input[name="search_province"]').focus();
						  	return false;
						  }else{
						  	return true;
						  }


						
      			});

            $("#mobile_filter").click(function(){





              var filter_technician = $("#filter_technician");
                if(filter_technician.hasClass('hidden-sm') || filter_technician.hasClass('hidden-xs')){
                    filter_technician.removeClass('hidden-sm');
                    filter_technician.removeClass('hidden-xs');
                }else{
                    filter_technician.addClass('hidden-sm');
                    filter_technician.addClass('hidden-xs');
                }
            });

            if(!$.isEmptyObject(urlParameter())){
              if (window.matchMedia('(max-width: 767px)').matches) {
                  //...
                  $('html, body').animate({scrollTop: $("#search-agent").offset().top+350},2000);
              } else {
                  //...
                  // $('html, body').animate({scrollTop: $("#search-agent").offset().top}, 2000);
              }
              
                $("#filter_technician").removeClass('hidden-xs');
                $("#filter_technician").removeClass('hidden-sm');
            }


      			$('input[name="agent_type"]').click(function(){

      					$('.loading').show();
      					
      					if($(this).is(':checked')){
      					var OldQueryString = urlParameter();

      					var form_text = '<form name="FilterForm" accept-charset="utf-8" type="GET" action="">';

      					if(!$.isEmptyObject(OldQueryString)){

      							$.each(OldQueryString,function(index,value){

	      							form_text += '<input type="hidden" name="'+index+'" value="'+value+'" />';

      							});
      							$('input[name="agent_type"]:checked').each(function(index,el){
      								// console.log('index = '+index);
	      							if(!OldQueryString.hasOwnProperty($(el).val())){
	      								// console.log(el);
		      							var agent_val = $(el).val();
		      							form_text += '<input type="hidden" name="'+agent_val+'" value="true" />';
	      							}
      							});

      					}else{
      							$('input[name="agent_type"]:checked').each(function(index,el){
	      							var agent_val = $(el).val();
	      							form_text += '<input type="hidden" name="'+agent_val+'" value="true" />';
      							});
      					}

      					form_text += '</form>';

      					$('body').append(form_text);

      					$('form[name="FilterForm"]').submit();
      					}else{

      						var this_val = $(this).val();
      						console.log('this val = '+this_val);
      						/* uncheck object */
      						var OldQueryString = urlParameter();
      						var form_text = '<form name="FilterForm" accept-charset="utf-8" type="GET" action="">';
      						// console.log(OldQueryString);

      						if(!$.isEmptyObject(OldQueryString)){

      							$.each(OldQueryString,function(index,value){
      									if(OldQueryString.hasOwnProperty(index)){
      											// console.log('index = '+index);
      											delete OldQueryString[this_val];
      									}
      							});

      							$.each(OldQueryString,function(index,value){

	      							form_text += '<input type="hidden" name="'+index+'" value="'+value+'" />';

      							});
      						}else{


      						}

      						form_text += '</form>';
							$('body').append(form_text);

      					    $('form[name="FilterForm"]').submit();
      						/* eof uncheck object */
      					}



      			});

            checkExpandedSearchMore();
            checkIsProvince();

            
        });

function checkExpandedSearchMore(){

      var parameters = urlParameter();

      if(parameters.search_address || parameters.search_code || parameters.search_name || parameters.search_shopname || parameters.search_surname || parameters.telephone){
        $("#search_advance").collapse('show');
      }

}
function checkIsProvinceNoAmphur(){
      // e.preventDefault();
      var search_province = $('select[name="search_province_select"]').val();
      var search_amphur = $('input[name="search_amphur"]').val();
      if((search_province != "" || search_province != "0") && search_amphur == ""){
          var btn_submit = $("#search-agent").find('button[type="submit"]');
          // btn_submit
          console.log(btn_submit);
          btn_submit.find('i.fa-spinner').hide();
          btn_submit.attr('disabled');
          $('input[name="search_amphur"]').focus();
          // e.preventDefault(); 
          return false;
      }else{
          return true;
      }
}
function checkIsProvince(){
      var parameters = urlParameter();
      if(parameters.search_province){
          var province_data_select = $('select[name="search_province_select"]').val();


          var res = province_data_select.split("|");
          var province_id = res[0];
          var province_name = res[1];

          var amphur_selected = (parameters.search_amphur)?parameters.search_amphur:"";

          $('input[name="ProvinceID"]').val(province_id);
          
          $.ajax({
                          type: "POST",
                          url: base_url+"Welcome/ajaxGetAmphurByProvince",
                          async:true,
                          dataType:'json',
                          data:{'province_id':province_id},
                          beforeSend: function( xhr ) {
                              $('input[name="search_amphur"]').html('');
                              // $('select[name="districts"]').html('');
                              // $('input[name="zipcode"]').val('');
                          },
                          success: function(result){
                            
                            if(result.status){
                                $.each(result.data, function(key, value) {   
                                     $('select[name="search_amphur"]')
                                    .append($("<option></option>")
                                    .attr("value",key)
                                    .text(value)); 
                            });
                            //$('select[name="amphur"]').focus();
                            //changeAmphur($('select[name="amphur"]').val());
                            $('input[name="search_amphur"]').removeAttr('disabled');
                            $('input[name="search_amphur"]').val(amphur_selected);
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
        }


      
}

function urlParameter() {
    var url = decodeURIComponent(window.location.href),
    retObject = {},
    parameters;


    if (url.indexOf('?') === -1) {
        return null;
    }

    url = url.split('?')[1];

    parameters = url.split('&');

    for (var i = 0; i < parameters.length; i++) {
        retObject[parameters[i].split('=')[0]] = parameters[i].split('=')[1];
    }

    return retObject;
}


var base_url = $('input[name="base_url"]').val();

function changeProvince(province_id){
        var res = province_id.split("|");

        province_id = res[0];
        var province_name = res[1];

        $('input[name="search_province"]').val(province_name);



        if(province_id != '0'){
          $('input[name="ProvinceID"]').val(province_id);
          // console.log('province_id = '+province_id);

          // console.log('province_id = '+province_id);return false;
        var base_url = $("input[name='base_url']").val();

        // console.log('base_url = '+base_url); return false;

          $.ajax({
                          type: "POST",
                          url: base_url+"Welcome/ajaxGetAmphurByProvince",
                          async:true,
                          dataType:'json',
                          data:{'province_id':province_id},
                          beforeSend: function( xhr ) {
                              $('select[name="search_amphur"]').html('');
                              // $('select[name="districts"]').html('');
                              // $('input[name="zipcode"]').val('');
                          },
                          success: function(result){
                            
                            if(result.status){
                                $.each(result.data, function(key, value) {   
                                     $('select[name="search_amphur"]')
                                    .append($("<option></option>")
                                    .attr("value",key)
                                    .text(value)); 
                            });
                            $('input[name="search_amphur"]').removeAttr('disabled');
                            $('input[name="search_amphur"]').focus();
                            $('.selectpicker').selectpicker('refresh');
                            //changeAmphur($('select[name="amphur"]').val());
                            }
                            
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                      });
        }

}

$('.selectpicker').on('changed.bs.select',function(e){
  // $('.selectpicker').selectpicker('refresh');
  // $('.selectpicker').selectpicker('hide');
  $('.dropdown-menu.open').hide();


});

// var $input_province = $("input[name='search_province']");
// $input_province.typeahead({
//   source:  function (query, process) {
//         var $this = this //get a reference to the typeahead object
        
//         return $.get(base_url+'Welcome/getAllProvince', { query: query }, function (data) {

//               data = $.parseJSON(data);

//                     var options = [];
//                     $this["map"] = {}; //replace any existing map attr with an empty object
//                     $.each(data,function (i,val){
//                         options.push(val.name);
//                         $this.map[val.name] = val.id; //keep reference from name -> id
//                     });
//                     return process(options);
             
//           });
//   },
//   updater: function(item){
//     $('input[name="ProvinceID"]').val(this.map[item]);
//     $('input[name="search_amphur"]').removeAttr('disabled');

   
//     return $.trim(item);
//   },
//   afterSelect: function(item) {

//     $('input[name="search_amphur"]').val('').focus();

//   },
//   autoSelect: true
// });



var $input_amphur = $("input[name='search_amphur']");
$input_amphur.typeahead({
  source:  function (query, process) {
        return $.get(base_url+'Welcome/getAmphurByProvince', { query: query,ProvinceID:$('input[name="ProvinceID"]').val() }, function (data) {
            console.log(data);
              data = $.parseJSON(data);
              return process(data);
          });


  },
  updater: function(item){
   
    return $.trim(item);
  },
  autoSelect: true
});






