function renderTechnicialExceptSeminar(){ 

	var base_url = $('input[name="base_url"]').val();

	//console.log('base_url = '+base_url);
	var post_data = {
		'agentcode':$('input[name="agentcode"]').val()

	};

	// console.log(post_data);return false;

	$.ajax({
		        url: base_url+'activity/TechnicialAcceptInviteSeminar/ajaxGetSeminarAndAccept',
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			        $('div#show_data_panel').html('').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
			    },
		        success: function (response) {
		        	console.log('==response==');
		        	console.log(response);
		        	if(response.status){
		        		$('div#show_data_panel').html('').html(response.view);
		        	}else{
		        		
		        	}

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    });
}

function handleClickAcceptSeminar(element){
	swal({
	  title: "ยืนยันการเข้าร่วมสัมนา?",
	  text: "กรุณากดปุ่ม OK เพื่อยืนยันการเข้าร่วมสัมนา",
	  icon: "info",
	  buttons: true,
	  dangerMode: false,
	})
	.then((willAccept) => {
	  if (willAccept) {
	    clickAcceptSeminar(element);
	  } else {
	    swal("คุณยังไม่ได้ทำการยืนยันการเข้าร่วมสัมนา!");
	  }
	});
}

function clickAcceptSeminar(element){


	var element = $(element); 
	var base_url = $('input[name="base_url"]').val();


	var post_data = {
		'seminarno':element.data('seminarno'),
		'agentcode':element.data('agentcode')
	};


	$.ajax({
		        url: base_url+'activity/TechnicialAcceptInviteSeminar/ajaxSetAcceptJoinSeminar',
		        type: "post",
		        data: post_data ,
		        async:true,
                dataType:'json',
                beforeSend: function() {
			        $('div#show_data_panel').html('').html('<center><i class="fa fa-spin fa-spinner"></i></center>');
			    },
		        success: function (response) {
		        	console.log('==response==');
		        	console.log(response);
		        	if(response.status){
		        		// $('div#show_data_panel').html('').html(response.view);
		        		renderTechnicialExceptSeminar();
		        	}else{
		        		
		        	}

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    });


	// console.log(post_data); return false;
}
function setagentcode(agentcode){
			  
              $('input[name="agentcode"]').val(agentcode);
             // $('span.show_agent_code').html(agentcode);
              renderTechnicialExceptSeminar();

              /* check already order product */
}