function exportChannelClick(){
	var base_url = $('input[name="base_url"]').val();

	 var newForm = $('<form>', {
        'action': base_url+'backend/admin_channel/exportChannelClick',
        'target': '_top',
        'method':'POST'
    }).append($('<input>', {
        'name': 'channel_id',
        'value': $('input[name="channel_id"]').val(),
        'type': 'hidden'
    }));

    newForm.appendTo('body').submit();
}