	  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url('backend/'.$this->controller.'/ads_zone_list')?>"><?php echo __('Advertisment Zone List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo (@$ads_zone_data->id)?__('Edit'):__('Create').__('Advertisment Zone')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
     	<div class="col-12">
     			<?php echo message_warning($this)?>
     	</div>
     	<div class="col-6">
     		<div class="card">
			      <div class="card-header"><?php echo (@$ads_zone_data->id)?__('Edit'):__('Create').__('Advertisment Zone')?></div>
			      <div class="card-body">
			       	<?php echo form_open('',array('name'=>'create-adszone-form'))?>

			       	  <div class="form-group">
			       	  	<label for=""><?php echo __('Advertisment')?></label>
			       	  	<?php echo form_dropdown('TVAdvertisments_id',$ads_list,@$ads_zone_data->TVAdvertisments_id,'class="form-control"')?>
			       	  </div>
			          <div class="form-group">
			            <label for=""><?php echo __('Advertisment Zone Name')?> : </label>	    
			            <?php echo form_input(array('name'=>'name','value'=>@$ads_zone_data->name,'class'=>'form-control','required'=>'required'))?>
			          </div>
			          <div class="form-group">
			            <label for=""><?php echo __('Advertisment Zone Description')?> : </label>
			            <?php echo form_textarea(array('name'=>'description','value'=>@$ads_zone_data->description,'rows'=>2,'class'=>'form-control','required'=>'required'))?>
			          </div>

			          <div class="form-group">
			            <label for=""><?php echo __('Width Spec')?> : </label>	    
			            <?php echo form_input(array('name'=>'width_spec','value'=>@$ads_zone_data->width_spec,'class'=>'form-control','required'=>'required'))?>
			          </div>

			          <div class="form-group">
			            <label for=""><?php echo __('Height Spec')?> : </label>	    
			            <?php echo form_input(array('name'=>'height_spec','value'=>@$ads_zone_data->height_spec,'class'=>'form-control','required'=>'required'))?>
			          </div>

			          <div class="form-group">
			          	<label for=""><?php echo __('Status')?> : </label>
			          	<?php echo form_dropdown('active',array('0'=>__('Unactive','backend/default'),'1'=>__('Active','backend/default')),@$ads_zone_data->active,'class="form-control"')?>
			          </div>

			       
			          
			          <?php echo form_button(array('type'=>'submit','content'=>__('Submit','backend/default'),'class'=>'btn btn-primary pull-right'))?>
			          <!-- <a class="btn btn-primary btn-block" href="index.html">Login</a> -->
			        <?php echo form_close();?>
			        
			      </div>
    		</div>
     	</div>
     </div>