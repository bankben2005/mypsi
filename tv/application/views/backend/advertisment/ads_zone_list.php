<style type="text/css">
	.modal-dialog  {width:900px;}

</style>
  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Advertisment Zone List')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
        <div class="col-12">
        	<?php echo message_warning($this)?>
          	<!-- Example DataTables Card-->
		      <div class="card mb-3">
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo __('Advertisment Zone List')?>
		          <a href="<?php echo base_url('backend/'.$this->controller.'/createAdvertismentZone')?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> <?php echo __('Add Advertisment Zone')?></a>
		      	</div>
		        <div class="card-body">
		          <div class="table-responsive">
		            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		              <thead>
		                <tr>
		                  <th><?php echo __('Name')?></th>
		                  <th><?php echo __('Advertisment')?></th>
		                  <th><?php echo __('Width')?> <small>(pixels)</small></th>
		                  <th><?php echo __('Height')?> <small>(pixels)</small></th>
		                  <th><?php echo __('Created')?></th>
		                  <th><?php echo __('Status')?></th>
		                  <th></th>
		                </tr>
		              </thead>
		              <tbody>
		              		<?php foreach($ads_zone_list as $key => $row){?>
		              			<tr>
		              				
		              				<td><?php echo $row->zone_name?></td>
		              				<td><?php echo $row->ads_name;?></td>
		              				<td><?php echo $row->width_spec;?></td>
		              				<td><?php echo $row->height_spec;?></td>
		              				<td><?php echo date('Y-m-d H:i:s',strtotime($row->created))?></td>
		              				<td>
		              					<?php if($row->active){?>
		              						<span class="badge badge-success"><?php echo __('Active','backend/default')?></span>
		              					<?php }else{?>
		              						<span class="badge badge-danger"><?php echo __('Unactive','backend/default')?></span>
		              					<?php }?>
		              				</td>
		              				<td>
		              					
		              					<a href="<?php echo base_url('backend/'.$this->controller.'/editAdvertismentZone/'.$row->zone_id)?>" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
		              					<a href="javascript:void(0);" onclick="if(confirm('ต้องการลบใช่หรือไม่') == true){window.location.href='<?php echo base_url('backend/'.$this->controller.'/deleteAdvertismentZone/'.$row->zone_id);?>'}"  class="btn btn-danger btn-sm">
		              						<i class="fa fa-trash"></i>
		              					</a>
		              				</td>
		              			</tr>

		              		<?php }?>
		              </tbody>

		            </table>
		           </div>
		        </div>
		      </div>

        </div>
      </div>

