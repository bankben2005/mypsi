	  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url('backend/'.$this->controller.'/ads_banner_list')?>"><?php echo __('Advertisment Banner List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo (@$ads_banner_data->id)?__('Edit'):__('Create').__('Advertisment Banner')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
     	<div class="col-12">
     			<?php echo message_warning($this)?>
     	</div>
        
        <div class="col-12">
            <?php echo form_open_multipart('',array('name'=>'create-adszone-form'))?>
        <div class="row">
     	<div class="col-6">
     		<div class="card">
			      <div class="card-header"><?php echo (@$ads_banner_data->id)?__('Edit'):__('Create').__('Advertisment Banner')?></div>
			      <div class="card-body">
			       

			       	  <div class="form-group">
			       	  	<label for=""><?php echo __('Advertisment Zone')?></label>
			       	  	<?php echo form_dropdown('TVAdvertismentZones_id',$ads_zone_list,@$ads_banner_data->TVAdvertismentZones_id,'class="form-control"')?>
			       	  </div>
			          <div class="form-group">
			            <label for=""><?php echo __('Advertisment Banner Name')?> : </label>	    
			            <?php echo form_input(array('name'=>'name','value'=>@$ads_banner_data->name,'class'=>'form-control','required'=>'required'))?>
			          </div>
			          <div class="form-group">
			            <label for=""><?php echo __('Advertisment Banner Description')?> : </label>
			            <?php echo form_textarea(array('name'=>'description','value'=>@$ads_banner_data->description,'rows'=>2,'class'=>'form-control','required'=>'required'))?>
			          </div>

			          
			          <div class="form-group">
			          		<label><?php echo __('Url')?></label>
			          		<?php echo form_input(array('name'=>'url','value'=>@$ads_banner_data->url,'class'=>'form-control'))?>
			          </div>

			          <div class="form-group">
			          	<label for=""><?php echo __('Status')?> : </label>
			          	<?php echo form_dropdown('active',array('0'=>__('Unactive','backend/default'),'1'=>__('Active','backend/default')),@$ads_banner_data->active,'class="form-control"')?>
			          </div>

			       
			          
			          <?php echo form_button(array('type'=>'submit','content'=>__('Submit','backend/default'),'class'=>'btn btn-primary pull-right'))?>
			          <!-- <a class="btn btn-primary btn-block" href="index.html">Login</a> -->
			        
			        
			      </div>
    		</div>
     	</div>
     	<div class="col-6">
     			<div class="form-group">
                                        <label>รูปภาพแบนเนอร์ : gif|jpg|png</label>
                                        <div class="clearfix"></div>
                                        <?php if(@$ads_banner_data->banner && file_exists('uploaded/banner/'.@$ads_banner_data->id.'/'.@$ads_banner_data->banner)){?>
                                        
                                        <div class="view view-first" style="margin:5px 0px;">
                                                    <img src="<?php echo base_url().'uploaded/banner/'.@$ads_banner_data->id.'/'.@$ads_banner_data->banner;?>" style="width:150px;height: 100px;">
                                                     <div class="mask">
                                                      <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                         <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('backend/').$this->controller.'/delete_image/'.$ads_banner_data->id.'/image';?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                     </div>
                                        </div>
                                   
                                        
                                        <?php }?>
                                    
                                    <div class="clearfix"></div>
                                    
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('banner_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                                         </div>
                                    </div>

     	</div>
        </div>
        <?php echo form_close();?>
        </div>
        
     </div>