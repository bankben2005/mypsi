	  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url('backend/admin_channel/channel_list')?>"><?php echo __('Channel List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo (@$channel_data->id)?__('Edit'):__('Create').__('Channel')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
     	<div class="col-12">
     			<?php echo message_warning($this)?>
     	</div>
        
        <div class="col-12">
          <?php if($channel_data->id){?>
          <?php echo form_button(array(
              'type'=>'button',
              'class'=>'btn btn-success pull-right mb-2',
              'content'=>__('Export Rating'),
              'onclick'=>'exportChannelClick()'
            ))?>
            <div class="clearfix"></div>
          <?php }?>
        <?php echo form_open('',array('name'=>'create-channel-form'))?>
        <?php echo form_input(array(
          'type'=>'hidden',
          'name'=>'channel_id',
          'value'=>@$channel_data->id
        ))?>
        <div class="row">
     	<div class="col-6">
     		<div class="card">
            <center><img src="<?php echo getTVChannelLogo($channel_data->logo)?>" style="width:80px;height: 80px;"></center>
			      <div class="card-header"><?php echo (@$channel_data->id)?__('Edit'):__('Create').__('Channel')?></div>
			      <div class="card-body">
                        <div class="form-group">
                            <!-- <label><?php ?></label> -->
                            <label><?php echo __('Channel Name')?> :</label>
                            <?php echo form_input(array('name'=>'name','value'=>@$channel_data->name,'class'=>'form-control'))?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Channel Description')?> :</label>
                            <?php echo form_textarea(array('name'=>'description','value'=>@$channel_data->description,'class'=>'form-control','rows'=>2))?>
                        </div>

                        <div class="form-group">
                            <label><strong><?php echo __('MV Request Streaming')?> : </strong></label>
                            <?php echo form_input(array(
                              'name'=>'mv_request_streaming',
                              'value'=>@$channel_data->mv_request_streaming,
                              'class'=>'form-control'
                            ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('MV Streaming Temp')?> : </strong></label>
                          <?php echo form_textarea(array(
                            'name'=>'mv_streaming_temp',
                            'value'=>@$channel_data->mv_streaming_temp,
                            'class'=>'form-control',
                            'rows'=>3
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('MV Status')?> : </strong></label>
                          <?php echo form_dropdown('mv_status',array(
                            '0'=>__('Unactive','backend/default'),
                            '1'=>__('Active','backend/default')
                          ),@$channel_data->mv_status,'class="form-control"')?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Direct Streaming Link')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'direct_streaming_link',
                            'class'=>'form-control',
                            'value'=>@$channel_data->direct_streaming_link
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Direct Streaming Status')?> : </strong></label>
                          <?php echo form_dropdown('direct_streaming_status',array(
                            '0'=>__('Unactive','backend/default'),
                            '1'=>__('Active','backend/default')
                          ),@$channel_data->direct_streaming_status,'class="form-control"')?>
                        </div>  

                        <div class="form-group">
                          <label><?php echo __('Status','backend/default')?> : </label>
                          <?php echo form_dropdown('active',array('0'=>__('Unactive','backend/default'),'1'=>__('Active','backend/default')),@$channel_data->active,'class="form-control"')?>
                        </div>
                  </div>
            </div>
        </div>
        <div class="col-6">

            <div class="form-group">
                <label><strong><?php echo __('Map update channel (O5)')?></strong></label>
                <?php echo form_dropdown('ofive_map_channel',@$select_map_channel,@$channel_data->ofive_map_channel,'class="form-control"')?>
            </div>


            <div class="card">
                <div class="card-header"><?php echo __('Channel Categories')?></div>
                <div class="card-body">




                        <ul class="list-group">
                          <?php foreach($channel_categories as $key => $value){?>
                            <li class="list-group-item"><?php echo $value->name;?>
                                
                                <?php echo form_checkbox(array('name'=>'check_cat[]','value'=>$value->id,'class'=>'pull-right','checked'=>(in_array($value->id, $channel_categories_list))?TRUE:FALSE))?>
                            </li>
                          <?php }?>
                        </ul>
                </div>
            </div>
        </div>
        <div class="col-12">
            <?php echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
        </div>
        </div>
        <?php echo form_close();?>
        </div>
        
    </div>