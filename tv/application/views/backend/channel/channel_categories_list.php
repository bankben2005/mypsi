	  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Channel Categories List')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
        <div class="col-12">
          	<!-- Example DataTables Card-->
          	  <?php echo message_warning($this)?>
		      <div class="card mb-3">
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo __('Channel Categories List')?>
		          <a href="<?php echo base_url('backend/'.$this->controller.'/createCategory')?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> <?php echo __('Add Category')?></a>
		          <div class="clearfix"></div>
		      	</div>
		        <div class="card-body">
		          <div class="table-responsive">
		            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		              <thead>
		                <tr>
		                  <th><?php echo __('Name')?></th>
		                  <th><?php echo __('Description')?></th>
		                  <th><?php echo __('Status')?></th>
		                  <th></th>
		                </tr>
		              </thead>
		              <tbody>
		              		<?php foreach($channel_categories as $key => $row){?>
		              			<tr>
		              				
		              				<td><?php echo $row->name;?></td>
		              				<td><?php echo $row->description?></td>
		              				<td>
		              					<?php if($row->active){?>
		              						<span class="badge badge-success"><?php echo __('Active','backend/default')?></span>
		              					<?php }else{?>
		              						<span class="badge badge-danger"><?php echo __('Unactive','backend/default')?></span>
		              					<?php }?>
		              				</td>
		              				<td>
		              					<a href="<?php echo base_url('backend/admin_channel/editCategory/'.$row->id)?>" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
		              					<a href="javascript:void(0);" onclick="if(confirm('ต้องการลบใช่หรือไม่') == true){window.location.href='<?php echo base_url('backend/'.$this->controller.'/deleteCategory/'.$row->id);?>'}" class="btn btn-danger btn-sm">
		              						<i class="fa fa-trash"></i>
		              					</a>
		              				</td>
		              			</tr>

		              		<?php }?>
		              </tbody>

		            </table>
		           </div>
		        </div>
		      </div>

        </div>
      </div>