<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/signin.css')?>">
	
</head>
<body>

<div class="container">
        <div class="card card-container">

            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <?php echo message_warning($this)?>
            <p id="profile-name" class="profile-name-card"></p>
            	<?php echo form_open('',array('signin-form'))?>
                <span id="reauth-email" class="reauth-email"></span>
                <?php echo form_input(array('type'=>'email','name'=>'email','id'=>'inputEmail','class'=>'form-control','placeholder'=>'Email address','required'=>'required','autofocus'=>true))?>
                <!-- <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus> -->
                <?php echo form_input(array('type'=>'password','name'=>'password','id'=>'inputPassword','class'=>'form-control','placeholder'=>'Password','required'=>'required'))?>
                <!-- <input type="password" id="inputPassword" class="form-control" placeholder="Password" required> -->

                <!-- <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div> -->
                <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
            	<?php echo form_close();?>
            <!-- <a href="#" class="forgot-password">
                Forgot the password?
            </a> -->
        </div><!-- /card-container -->
    </div><!-- /container -->


    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

	<script src="<?php echo base_url('assets/js/signin.js')?>"></script>
</body>
</html>