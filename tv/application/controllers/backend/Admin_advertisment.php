<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_advertisment extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){
    	$this->ads_list();
    }
    public function ads_list(){

    	$this->loadDataTableCSS();
    	$this->loadDataTableJS();

    	$this->template->javascript->add(base_url('assets/js/backend/advertisment/ads_list.js'));


    	$data = array(
    		'ads_list' => $this->getAllAdvertisment()
    	);

    	// print_r($data);exit;
    	$this->template->content->view('backend/advertisment/ads_list',$data);
        $this->template->publish();

    }

    public function createAdvertisment(){
    	$this->__createAdvertisment();
    }
    public function editAdvertisment($id=null){
    	if(!$this->getAdvertismentData($id)){
    		redirect(base_url('backend/'.$this->controller.'/ads_list'));
    	}else{
    		$this->__createAdvertisment($id);
    	}

    }
    public function deleteAdvertisment($id=null){
    	if(!$this->getAdvertismentData($id)){
    		redirect(base_url('backend/'.$this->controller.'/ads_list'));
    	}else{
    		if($this->db->delete('TVAdvertisments',array('id'=>$id))){

    			$this->msg->add(__('Delete advertisment successful!!','backend/advertisment/ads_list'),'success');
    			redirect(base_url('backend/'.$this->controller.'/ads_list'));

    		}

    	}

    }

    private function __createAdvertisment($id=null){

    		$this->template->stylesheet->add(base_url('assets/css/bootstrap-daterangepicker/daterangepicker.css'));

    		$this->template->javascript->add(base_url('assets/js/bootstrap-daterangepicker/moment.min.js'));
			$this->template->javascript->add(base_url('assets/js/bootstrap-daterangepicker/daterangepicker.js'));
			$this->template->javascript->add(base_url('assets/js/backend/advertisment/create_ads.js'));

    		$ads_data = new StdClass();
            $ads_data = $this->getAdvertismentData($id);

            //print_r($ads_data);

    		if($this->input->post(null,false)){
                // print_r($this->input->post());exit;
                /* Duration Data */
                $startDate = "";
                $endDate = "";
                $exDuration = explode(' - ', $this->input->post('duration'));
                $startDate = new DateTime($exDuration[0]);
                $endDate = new DateTime($exDuration[1]);
                /* Eof Duration Data */

    			if($id){
                    $dataUpdate = array(
                        'name' => $this->input->post('name'),
                        'description'=>$this->input->post('description'),
                        'startdate'=>$startDate->format('Y-m-d H:i:s'),
                        'enddate'=>$endDate->format('Y-m-d H:i:s'),
                        'updated'=>date('Y-m-d H:i:s'),
                        'active'=>$this->input->post('active')
                    );
                    if($this->db->update('TVAdvertisments',$dataUpdate,array('id'=>$id))){
                        $this->msg->add(__('Update advertisment success','backend/advertisment/create_ads'),'success');
                        redirect(base_url('backend/'.$this->controller.'/editAdvertisment/'.$id));
                    }

    			}else{
                    $dataInsert = array(
                        'name' => $this->input->post('name'),
                        'description'=>$this->input->post('description'),
                        'startdate'=>$startDate->format('Y-m-d H:i:s'),
                        'enddate'=>$endDate->format('Y-m-d H:i:s'),
                        'created'=>date('Y-m-d H:i:s'),
                        'active'=>$this->input->post('active')
                    );
                    if($this->db->insert('TVAdvertisments',$dataInsert)){
                        $this->msg->add(__('Create advertisment success','backend/advertisment/create_ads'),'success');
                        redirect(base_url('backend/'.$this->controller.'/editAdvertisment/'.$id));

                    }

    			}

    		}

            $duration = "";
            if($ads_data){
                $duration = date('d-m-Y',strtotime($ads_data->startdate)).' - '.date('d-m-Y',strtotime($ads_data->enddate));

            }



    		$data = array(
    			'ads_data' => $ads_data,
                'duration'=> $duration
    		);

    		$this->template->content->view('backend/advertisment/create_ads',$data);
        	$this->template->publish();

    }

    public function ads_zone_list(){
    	$data = array(
    		'ads_zone_list' => $this->getAllAdvertismentZone()
    	);

    	//print_r($data);exit;
    	$this->template->content->view('backend/advertisment/ads_zone_list',$data);
        $this->template->publish();
    }

    public function createAdvertismentZone(){
        $this->__createAdvertismentZone();
    }
    public function editAdvertismentZone($id=null){
        if(!$this->getAdvertismentZoneData($id)){
            redirect(base_url('backend/'.$this->controller.'/ads_zone_list'));
        }else{
            $this->__createAdvertismentZone($id);
        }
    }
    public function deleteAdvertismentZone($id=null){
        if(!$this->getAdvertismentZoneData($id)){
            redirect(base_url('backend/'.$this->controller.'/ads_zone_list'));
        }else{
            if($this->db->delete('TVAdvertismentZones',array('id'=>$id))){
                $this->msg->add(__('Delete advertisment zone successful!!','backend/advertisment/ads_zone_list'),'success');
                redirect(base_url('backend/'.$this->controller.'/ads_zone_list'));
            }
        }
    }
    private function __createAdvertismentZone($id=null){

            $ads_zone_data = $this->getAdvertismentZoneData($id);
            if($this->input->post(null,false)){
                $post_data = $this->input->post();
                //print_r($post_data);exit;

                if($id){
                    $post_data['updated'] = date('Y-m-d H:i:s');
                    if($this->db->update('TVAdvertismentZones',$post_data,array('id'=>$id))){

                        $this->msg->add(__('Update advertisment zone success','backend/advertisment/create_ads_zone'),'success');
                        redirect(base_url('backend/'.$this->controller.'/editAdvertismentZone/'.$id));
                    }
                }else{
                    $post_data['created'] = date('Y-m-d H:i:s');
                    if($this->db->insert('TVAdvertismentZones',$post_data)){
                        $this->msg->add(__('Create advertisment zone success','backend/advertisment/create_ads_zone'),'success');
                        redirect(base_url('backend/'.$this->controller.'/editAdvertismentZone/'.$id));
                    }
                }
            }

            $data = array(
                'ads_zone_data' => $ads_zone_data,
                'ads_list' => $this->getAllAdvertismentAvailable()
            );

            $this->template->content->view('backend/advertisment/create_ads_zone',$data);
            $this->template->publish();

    }
    public function ads_banner_list(){
    	$data = array(
    		'ads_banner_list' => $this->getAllAdvertismentBanner()
    	);

    	// print_r($data);exit;
    	$this->template->content->view('backend/advertisment/ads_banner_list',$data);
        $this->template->publish();
    }
    public function createAdvertismentBanner(){
        $this->__createAdvertismentBanner();
    }
    public function editAdvertismentBanner($id=null){
            if(!$this->getAdvertismentBannerData($id)){
                redirect(base_url('backend/'.$this->controller.'/ads_banner_list'));
            }else{
                $this->__createAdvertismentBanner($id);
            }

    }
    public function deleteAdvertismentBanner($id=null){
            if(!$this->getAdvertismentBannerData($id)){
                redirect(base_url('backend/'.$this->controller.'/ads_banner_list'));
            }else{
                $row = $this->getAdvertismentBannerData($id);
                if(file_exists('./uploaded/banner/'.$id.'/'.$row->banner)){
                    unlink('./uploaded/banner/'.$id.'/'.$row->banner);
                    rmdir('./uploaded/banner/'.$id);
                }
                if($this->db->delete('TVAdvertismentBanners',array('id'=>$id))){
                    $this->msg->add(__('Delete advertisment banner successful!!','backend/advertisment/ads_banner_list'),'success');
                    redirect(base_url('backend/'.$this->controller.'/ads_banner_list'));
                }
            }

    }
    private function __createAdvertismentBanner($id=null){

            $this->load->library(array('upload'));

            $this->template->stylesheet->add(base_url('assets/css/imghover/img_common.css'));
            $this->template->stylesheet->add(base_url('assets/css/imghover/img_hover.css'));

            $this->template->javascript->add(base_url('assets/js/backend/advertisment/create_ads_banner.js'));
            $ads_banner_data = $this->getAdvertismentBannerData($id);
            if($this->input->post(null,false)){
                    $post_data = $this->input->post();

                    $zone_data = $this->getAdvertismentZoneData($post_data['TVAdvertismentZones_id']);

                    //print_r($zone_data);exit;

                    if($id){
                        $data_update = $post_data;
                        $data_update['updated'] = date('Y-m-d H:i:s');

                        if($this->db->update('TVAdvertismentBanners',$data_update,array('id'=>$id))){
                            $banner_data = $this->getAdvertismentBannerData($id);
                            $banner_data->{'zone_width_spec'} = $zone_data->width_spec;
                            $banner_data->{'zone_height_spec'} = $zone_data->height_spec;
                            /* save banner image */
                            if($_FILES['banner_image']['error'] == 0){
                                $this->uploadBannerImage($banner_data,'update');
                            }

                            $this->msg->add(__('Update advertisment banner success !!','backend/advertisment/create_ads_banner'),'success');
                            redirect(base_url('backend/'.$this->controller.'/editAdvertismentBanner/'.$id));

                        }


                    }else{
                        $data_insert = $post_data;
                        $data_insert['created'] = date('Y-m-d H:i:s');

                        if($this->db->insert('TVAdvertismentBanners',$data_insert)){
                            $last_insert_id = $this->db->insert_id();
                            $banner_data = $this->getAdvertismentBannerData($last_insert_id);
                            $banner_data->{'zone_width_spec'} = $zone_data->width_spec;
                            $banner_data->{'zone_height_spec'} = $zone_data->height_spec;

                            if($_FILES['banner_image']['error'] == 0){
                                $this->uploadBannerImage($banner_data,'create');
                            }

                            $this->msg->add(__('Create advertisment banner success !!','backend/advertisment/create_ads_banner'),'success');
                            redirect(base_url('backend/'.$this->controller.'/createAdvertismentBanner/'.$id));

                        }


                    }
                    



            }

            $data = array(
                'ads_banner_data' => $ads_banner_data,
                'ads_zone_list' => $this->getAllAdvertismentZoneAvailable()
            );

            // print_r($data);exit;

            $this->template->content->view('backend/advertisment/create_ads_banner',$data);
            $this->template->publish();




    }

    private function getAllAdvertisment(){
    	$query = $this->db->select('*')
    	->from('TVAdvertisments')
    	->get();

    	if($query->num_rows() > 0){
    		return $query->result();
    	}else{
    		return array();
    	}

    	// return $query->row();
    }
    private function getAllAdvertismentAvailable(){
        $arReturn = array();
        $query = $this->db->select('*')
        ->from('TVAdvertisments')
        ->where('active',1)
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $arReturn[$value->id] = $value->name;
            }
            return $arReturn;
        }else{
            return array();
        }

        // return $query->row();
    }

    private function getAllAdvertismentZoneAvailable(){
        $arReturn = array();
        $query = $this->db->select('*')
        ->from('TVAdvertismentZones')
        ->where('active',1)
        ->get();

        // print_r($query->result());exit;

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $arReturn[$value->id] = $value->name;
            }
            return $arReturn;
        }else{
            return array();
        }

        // return $query->row();
    }
    private function getAdvertismentData($ads_id){
    	$query = $this->db->select('*')
    	->from('TVAdvertisments')
    	->where('id',$ads_id)
    	->get();

    	if($query->num_rows() > 0){
    		return $query->row();
    	}else{
    		return false;
    	}
    }

    private function getAdvertismentZoneData($ads_zone_id = null){
        $query = $this->db->select('*')
        ->from('TVAdvertismentZones')
        ->where('id',$ads_zone_id)
        ->get();

        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }
    private function getAllAdvertismentZone(){
        $query =$this->db->select('*,TVAdvertisments.name as ads_name,TVAdvertismentZones.name as zone_name,TVAdvertismentZones.id as zone_id')
        ->from('TVAdvertismentZones')
        ->join('TVAdvertisments','TVAdvertismentZones.TVAdvertisments_id = TVAdvertisments.id')
        ->get();

        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return array();
        }

    }
    private function getAllAdvertismentBanner(){
        $query = $this->db->select('*,TVAdvertismentBanners.id as banner_id,TVAdvertismentZones.name as zone_name,TVAdvertismentBanners.updated as banner_updated,TVAdvertismentBanners.active as banner_active,TVAdvertismentBanners.name as banner_name')
        ->from('TVAdvertismentBanners')
        ->join('TVAdvertismentZones','TVAdvertismentBanners.TVAdvertismentZones_id = TVAdvertismentZones.id')
        ->get();

        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return array();
        }

    }
    private function getAdvertismentBannerData($ads_banner_id){

        $query = $this->db->select('*')
        ->from('TVAdvertismentBanners')
        ->where('id',$ads_banner_id)
        ->get();

        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }

    }
    private function uploadBannerImage($banner,$type){

        //print_r($banner);exit;

                            $config = array();
                            if(!is_dir('uploaded/banner/'.$banner->id.'')){
                                mkdir('uploaded/banner/'.$banner->id.'',0777,true);
                            }
                            clearDirectory('uploaded/banner/'.$banner->id.'/');
                                                     
                            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                            $config['upload_path'] = 'uploaded/banner/'.$banner->id.'/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '2000';


                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload('banner_image')){
                                $error = array('error' => $this->upload->display_errors());
                                $this->msg->add(__($error['error'],'default'), 'error');
                                if(file_exists('uploaded/banner/'.$banner->id)){
                                        rmdir('uploaded/banner/'.$banner->id);
                                }
                                
                                if($type == 'update'){
                                    redirect(base_url('backend/').$this->controller.'/editAdvertismentBanner/'.$banner->id);   
                                }else if($type == 'create'){
                                    $this->db->delete('TVAdvertismentBanners',array('id'=>$banner->id));
                                    redirect(base_url('bakcend/').$this->controller.'/createAdvertismentBanner');
                                }
                                
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());

                                $this->resizeImageWithZoneSpec(array('banner'=>$banner,'img_name'=>$data_upload['upload_data']['file_name']));

                                if($this->db->update('TVAdvertismentBanners',array('banner'=>$data_upload['upload_data']['file_name']),array('id'=>$banner->id))){
                                    return true;
                                }
                                
                            }
                            return false;

    }


    private function resizeImageWithZoneSpec($data = array()){

                                $config['image_library'] = 'gd2';
                                $config['source_image'] = './uploaded/banner/'.$data['banner']->id.'/'.$data['img_name'];
                                $config['create_thumb'] = FALSE;
                                $config['maintain_ratio'] = TRUE;
                                $config['width'] = $data['banner']->zone_width_spec;
                                $config['height'] = $data['banner']->zone_height_spec;

                                //print_r($config);exit;
                                $this->load->library('image_lib', $config);

                                $this->image_lib->resize();

    }


}
