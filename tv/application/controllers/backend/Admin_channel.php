<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_channel extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
                        $this->load->helper(array(
                            'our'
                        ));
    }

    public function index(){
    	$this->channel_list();
    }
    public function channel_list(){
        //echo 'aaaa';exit;
    	$this->loadDataTableCSS();
    	$this->loadDataTableJS();

    	$this->template->javascript->add(base_url('assets/js/backend/channel/channel_list.js'));


    	$data = array(
    		'channels' => $this->getAllChannels()
    	);

    	//print_r($data);exit;
    	$this->template->content->view('backend/channel/channel_list',$data);
        $this->template->publish();
    }
    public function editChannel($id=null){
            if(!$this->getChannelData($id)){
                redirect(base_url('backend/'.$this->controller.'/channel_list'));
            }else{
                $this->__createChannel($id);
            }
    }
    public function deleteChannel($id=0){
        if(!$this->getChannelData($id)){
            redirect(base_url('backend/'.$this->controller.'/channel_list'));
        }else{
            $this->db->delete('TVChannels',array('id'=>$id));
            $this->msg->add(__('Delete channel success','backend/channel/create_channel'),'success');
            redirect(base_url('backend/'.$this->controller.'/channel_list'));
        }
    }
    private function __createChannel($id=null){
            $row = $this->getChannelData($id);

            $center_channel_json = $this->getSelectOfiveChannelList();

            //print_r($center_channel_json);exit;
            

            if($id){
                $this->template->javascript->add(base_url('assets/js/backend/channel/create_channel.js'));
            }

            if($this->input->post(NULL,FALSE)){
                /* update channel */
                if($id){
                    $query = $this->db->update('TVChannels',array(
                        'name'=>$this->input->post('name'),
                        'description'=>$this->input->post('description'),
                        'active'=>$this->input->post('active'),
                        'mv_request_streaming'=>$this->input->post('mv_request_streaming'),
                        'mv_streaming_temp'=>$this->input->post('mv_streaming_temp'),
                        'mv_status'=>$this->input->post('mv_status'),
                        'direct_streaming_link'=>$this->input->post('direct_streaming_link'),
                        'direct_streaming_status'=>$this->input->post('direct_streaming_status'),
                        'ofive_map_channel'=>$this->input->post('ofive_map_channel')
                    ),array('id'=>$id));
                    if($query){
                        /* update tv channels category list */
                            // 1. remove old record from fv channels category list
                            $this->db->delete('TvChannelCategoryLists',array('TVChannels_id'=>$id));

                            // 2. insert new config
                            foreach ($this->input->post('check_cat') as $key => $value) {
                                # code...
                                    $this->db->insert('TvChannelCategoryLists',array('TVChannels_id'=>$id,'TVChannelCategories_id'=>$value));
                            }
                            $this->msg->add(__('Edit channel successful!!','backend/channel/create_channel'),'success');
                            redirect(base_url('backend/'.$this->controller.'/editChannel/'.$id));
                    }

                }else{
                    $query = $this->db->insert('TVChannels',array('name'=>$this->input->post('name'),'description'=>$this->input->post('description')));
                    if($query){
                        /* insert tv channel category list */
                        foreach ($this->input->post('check_cat') as $key => $value) {
                            # code...
                            $this->db->insert('TvChannelCategoryLists',array('TVChannels_id'=>$id,'TVChannelCategories_id'=>$value));
                        }
                        $this->msg->add(__('Create channel successful!!','backend/channel/create_channel'),'success');
                            redirect(base_url('backend/'.$this->controller.'/channel_list'));

                    }

                }
            }

            $data = array(
                'channel_data' => $row,
                'channel_categories'=>$this->getAllChannelCategoriesAvailable(),
                'channel_categories_list'=>$this->getChannelCategoriesList($row->id),
                'select_map_channel'=>$center_channel_json
            );

            // print_r($data);exit;

            $this->template->content->view('backend/channel/create_channel',$data);
            $this->template->publish();

    }

    public function channel_categories_list(){
    	$this->loadDataTableCSS();
    	$this->loadDataTableJS();

    	$data = array(
    		'channel_categories' => $this->getAllChannelCategories()
    	);

    	$this->template->content->view('backend/channel/channel_categories_list',$data);
    	$this->template->publish();
    }

    public function createCategory(){
    		$this->__createCategory();
    }
    public function editCategory($id=0){
        if(!$this->getCategoryData($id)){
            redirect(base_url('backend/'.$this->controller.'/channel_categories_list'));
        }else{
            $this->__createCategory($id);
        }
    }
    public function deleteCategory($id=0){
        if(!$this->getCategoryData($id)){
            redirect(base_url('backend/'.$this->controller.'/channel_categories_list'));
        }else{
            $this->db->delete('TvChannelCategories',array('id'=>$id));
            $this->msg->add(__('Delete channel category success','backend/channel/create_category'),'success');
            redirect(base_url('backend/'.$this->controller.'/channel_categories_list'));
        }
    }

    public function ajaxGetChannelData(){
        $data_receive = array(
            'channel_id' => $this->input->post('channel_id')
        );

        $channel_data = $this->getChannelData($data_receive['channel_id']);

        echo json_encode(array('status'=>true,'channel_data'=>$channel_data));
        // print_r($channel_data);
    }

    private function __createCategory($id=null){
    	
    	$row = $this->getCategoryData($id);

    	if($this->input->post(NULL,FALSE)){
                $data = array(
                    'name'=>$this->input->post('name'),
                    'description'=>$this->input->post('description')
                );
            if($row){
                /* edit */
                $this->db->update('TvChannelCategories',$data,array('id'=>$row->id));
                $this->msg->add(__('Edit category successful!!','backend/channel/create_category'),'success');

            }else{
                $this->db->insert('TvChannelCategories',$data);
                $this->msg->add(__('Create category successful!!','backend/channel/create_category'),'success');

                /* create*/
            }

    	}

    	$data = array(
    		'category_data' => $row
    	);

    	// print_r($data);exit;

    	$this->template->content->view('backend/channel/create_category',$data);
    	$this->template->publish();

    }

    private function getCategoryData($category_id){
    		$query = $this->db->select('*')
    		->from('TvChannelCategories')
    		->where('id',$category_id)
    		->get();

    		if($query->num_rows() > 0){
    			return $query->row();
    		}else{
    			return false;
    		}
    }

    private function getChannelData($channel_id){
            $query = $this->db->select('*')
            ->from('TVChannels')
            ->where('id',$channel_id)
            ->get();
            if($query->num_rows() > 0){
                return $query->row();
            }else{
                return false;
            }
    }

    private function getAllChannels(){
    	$query = $this->db->select('*')
    	->from('TVChannels')
        ->order_by('channel_number')
    	->get();

    	return $query->result();

    }
    private function getAllChannelCategories(){
    	$query = $this->db->select('*')
    	->from('TvChannelCategories')
    	->get();

    	return $query->result();
    }
    private function getAllChannelCategoriesAvailable(){
        $query = $this->db->select('*')
        ->from('TvChannelCategories')
        ->where('active',1)
        ->get();
        return $query->result();
    }

    private function getChannelCategoriesList($channel_id=0){
        //print_r($channel_id);exit;
        $arReturn = array();
        $query = $this->db->select('*')
        ->from('TvChannelCategoryLists')
        ->where('TVChannels_id',$channel_id)
        ->get();

        foreach ($query->result() as $key => $value) {
            # code...
            array_push($arReturn, $value->TVChannelCategories_id);
        }
        return $arReturn;

        // print_r($arReturn);exit;


    }

    public function exportChannelClick(){
        $post_data = $this->input->post();

        $query = $this->db->select('*,TVChannels.name,TVChannelViews.created as viewtime')
        ->from('TVChannelViews')
        ->join('TVChannels','TVChannelViews.TVChannels_id = TVChannels.id')
        ->where('TVChannelViews.TVChannels_id',$post_data['channel_id'])
        ->order_by('TVChannelViews.created','desc')
        ->get();

        //print_r($query->result());exit;

        // print_r($questionnaireanswer);exit;
                $filename = "TVChannelViewReport(".$query->result()[0]->name.').xlsx';

                require_once 'assets/plugin/PHPExcel/Classes/PHPExcel.php';


                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();

                //exit;

                // Set document properties
                $objPHPExcel->getProperties()->setCreator("PSIFIXIT")
                                             ->setLastModifiedBy("PSIFIXIT Admin")
                                             ->setTitle($filename)
                                             ->setSubject("Excel file tv channel view")
                                             ->setDescription("Excel file tv")
                                             ->setKeywords("TvChannelView Report")
                                             ->setCategory("Report");

                //print_r($arDataOrder);


                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1')->setCellValue('A1',$filename);
                $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A2','#')
                            ->setCellValue('B2','TVChannel')
                            ->setCellValue('C2','Customer IP')
                            ->setCellValue('D2','ViewTime');

                $count = 1;
                $table_row = 3;
                foreach ($query->result() as $key => $value) {
                   
                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$table_row.'',@$count)
                                ->setCellValue('B'.$table_row.'',@$value->name)
                                ->setCellValue('C'.$table_row.'',@$value->client_ip)
                                ->setCellValue('D'.$table_row.'',@date('d/m/Y H:i:s',strtotime($value->viewtime)));
                                $count++;
                                $table_row++;
                }


                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('A')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('B')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('C')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('D')
                ->setAutoSize(true);

                // Rename worksheet
                $objPHPExcel->getActiveSheet()->setTitle('TVChannelViewReport');


                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);


                // Redirect output to a client’s web browser (Excel2007)
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$filename.'"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');

                // If you're serving to IE over SSL, then the following may be needed
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save('php://output');
                exit;

    }

    private function getSelectOfiveChannelList(){

        $ofive_url = 'http://ofivetv.psisat.com/welcome/getAllOfiveChannel';

        // create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $ofive_url); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch);      


        $response = json_decode($output);

        $arrSelect = [];
        $arrSelect[""] = "-- Select Channel --";
        foreach ($response->channels as $key => $value) {
            # code...
            $arrSelect[$value->channel_id] = $value->channel_name;

        }

        return $arrSelect;


    }

}