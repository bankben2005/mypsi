<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	private $conn;

	public function __construct(){
					parent::__construct();
					 /* connect mysql */
                     $this->connect_mysql();
                     /* eof connect mysql */

                     $this->load->library(array('Msg','Lang_controller'));
                     $this->load->helper(array('lang','our'));
	}
	public function index()
	{
		if($this->session->userdata('user_id')){
    		redirect(base_url('backend/admin_dashboard'));
    	}
		if($this->input->post(NULL,FALSE)){
				$sql = "SELECT * FROM members WHERE email = '".$this->input->post('email')."' and password = '".sha1($this->input->post('password'))."'";
        		$result = $this->conn->query($sql);

		        if($result->num_rows > 0){
		          	$row = $result->fetch_assoc();
		          	if($row['active']){
				            /* get user permission menu */
				            $strQuery = "select * from member_permissions join member_menus on member_permissions.member_menus_id = member_menus.id where member_permissions.members_id = '".$row['id']."'";
				            //echo $strQuery;exit;
				            $resultPermission = $this->conn->query($strQuery);

				            //print_r($resultPermission);exit;
				            $controller_ability = array();
				            $member_permissions = array();
				            if($resultPermission->num_rows > 0){
				                while ($row_permission = $resultPermission->fetch_assoc()) {
				                  # code...
				                  array_push($controller_ability, $row_permission['controller_name']);
				                  array_push($member_permissions, $row_permission);
				                }
				                $controller_ability = array_unique($controller_ability);

				                ($row['member_access_types_id'] == '1')?array_push($controller_ability, 'SuperAdmin'):"";

				                ($row['member_access_types_id'] == '2')?array_push($controller_ability, "Administrator"):"";
				                /* eof get user permission menu */
				                $member_data = array(
				                  'data' => $row,
				                  'member_permissions' => $member_permissions,
				                  'member_controller_available' => $controller_ability
				                );

				                $this->session->set_userdata('user_id',$row['id']);
				                $this->session->set_userdata('member_data',$member_data);

				                redirect(base_url('backend/admin_dashboard'));
				            
				            }else{

				            	if($row['member_access_types_id'] == '2'){
				            		array_push($controller_ability, "Administrator");
				            		$member_data = array(
				            			'data'=>$row,
				            			'member_permissions'=>$member_permissions,
				            			'member_controller_available'=>$controller_ability
				            		);
				            		$this->session->set_userdata('user_id',$row['id']);
					                $this->session->set_userdata('member_data',$member_data);

					                redirect(base_url('backend/admin_dashboard'));
				            	}else{
					                $this->msg->add(__('Please set permission for this user,before access to backend','backend/login'),'error');
					                redirect($this->uri->uri_string());
				            	}
				            }
				            
				          }else{
				            $this->msg->add(__('Account has been blocked,Please contact administrator.','default'),'error');
				            redirect($this->uri->uri_string());
				          }
				}else{
					$this->msg->add('Authentication fail,Please try again.','error');
            		redirect($this->uri->uri_string());
				}
		}
		$this->load->view('welcome_message');
	}
	public function logout(){
    	$this->session->unset_userdata('user_id');
      	$this->session->unset_userdata('member_data');
      	redirect(base_url());
    }
	private function connect_mysql(){

        if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "psitv";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psitv";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psisat@2020";
            $dbname = "psitv";

        }

        // Create connection
        $this->conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        $this->conn->set_charset("utf8");
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }
}
