<?php
     class Backend_controller extends CI_Controller{
            
             public $meta_title = '';
             public $meta_keywords = '';
             public $meta_description = '';
             public $data = array();
             public $page_num = 10;
             public $controller = '';
             public $method = '';
             public $language = 'th';
             public $default_language;
             public $site_setting = '';
             public $account_data = array();
             private $conn_mysql;
             public $event_ability;
         public function __construct() {
                    parent::__construct();
                    $CI = & get_instance();
                    $this->setController($CI->router->class);
                    $this->setMethod($CI->router->method);

                    $this->load->library(array('Template','Msg', 'Lang_controller'));
                    $this->load->helper(array('lang', 'our', 'inflector','html','cookie','url'));
                    
                    $this->template->set_template('backend/template');

                    $this->_checkAlready_signin();
                    $this->_getAccountData();

                    //$this->checkMenuPermission();

                    // print_r($this->session->userdata('member_data'));
                    $this->_load_js();
                    $this->_load_css();

                    //print_r($this->session->userdata());

                    //print_r($this->session->userdata('member_data'));
        }

        /**
         * load javascript
         */
        public function _load_js() {
            $this->template->javascript->add(base_url('assets/vendor/jquery/jquery.min.js'));
            $this->template->javascript->add(base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'));
            $this->template->javascript->add(base_url('assets/js/sb-admin.min.js'));
        }
        /**
         * load style sheet
         */
        public function _load_css() {            
            $this->template->stylesheet->add(base_url('assets/vendor/bootstrap/css/bootstrap.min.css'));
            $this->template->stylesheet->add(base_url('assets/vendor/font-awesome/css/font-awesome.min.css'));
            
            $this->template->stylesheet->add(base_url('assets/css/sb-admin.css'));
        }

        public function loadDataTableCSS(){
            $this->template->stylesheet->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.css'));
        }
        public function loadDataTableJS(){
            $this->template->javascript->add(base_url('assets/vendor/datatables/jquery.dataTables.js'));
            $this->template->javascript->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.js'));
            $this->template->javascript->add(base_url('assets/js/sb-admin-datatables.min.js'));
        }
        public function _checkAlready_signin(){
            if(!$this->session->userdata('user_id')){
                redirect(base_url());
            }
        }
        public function _getAccountData(){
            if($this->session->userdata('member_data') && empty($this->account_data)){
                $this->account_data = $this->session->userdata('member_data');
            }

        }
        public function getData($key = null) {
            if (is_null($key) || empty($key))
                return $this->data;
            else
                return $this->data[$key];
        }

        public function setData($key, $data) {
            $this->data[$key] = $data;
        }
        public function getController() {
            return $this->controller;
        }

        public function setController($controller) {
            $this->setData('controller', $controller);
            $this->controller = $controller;
        }

        public function getMethod() {
            return $this->method;
        }

        public function setMethod($method) {
            $this->setData('method', $method);
            $this->method = $method;
        }



}