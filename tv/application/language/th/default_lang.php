<?php

$lang['Please set permission for this user,before access to backend'] = "กรุณาตั้งค่าสิทธิการเข้าถึงเมนูให้ User ก่อนเข้าใช้งานระบบ";
$lang['Authentication fail,Please try again.'] = "อีเมล์หรือหรัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง";
$lang['Success'] = "สำเร็จ";
