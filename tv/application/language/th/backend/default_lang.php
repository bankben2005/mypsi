<?php

$lang['Cancel'] = "ยกเลิก";
$lang['Logout'] = "ออกจากระบบ";
$lang['Dashboard'] = "แผงควบคุม";
$lang['Active'] = "เปิดใช้งาน";
$lang['Submit'] = "ตกลง";
$lang['OK'] = "ตกลง";
$lang['Close'] = "ปิด";
$lang['Unactive'] = "ไม่เปิดใช้งาน";
$lang['Status'] = "สถานะ";
