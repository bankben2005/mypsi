<?php

$lang['Dashboard'] = "แผงควบคุม";
$lang['Channel Management'] = "จัดการรายการ";
$lang['Channels'] = "ช่องรายการ";
$lang['Channel Categories'] = "หมวดหมู่รายการ";
$lang['Advertisment'] = "รายการโฆษณา";
$lang['Advertisment Management'] = "จัดการโฆษณา";
$lang['Advertisment Zone'] = "รายการโซนโฆษณา";
$lang['Advertisment Banner'] = "รายการแบนเนอร์โฆษณา";
