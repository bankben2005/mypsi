<?php

$lang['Advertisment Banner List'] = "รายการแบนเนอร์โฆษณา";
$lang['Create'] = "เพิ่ม";
$lang['Advertisment Banner'] = "แบนเนอร์โฆษณา";
$lang['Advertisment'] = "Advertisment";
$lang['Advertisment Zone Name'] = "Advertisment Zone Name";
$lang['Advertisment Zone Description'] = "Advertisment Zone Description";
$lang['Width Spec'] = "Width Spec";
$lang['Height Spec'] = "Height Spec";
$lang['Status'] = "สถานะ";
$lang['Advertisment Zone'] = "โซนโฆษณา";
$lang['Advertisment Banner Name'] = "ชื่อ";
$lang['Advertisment Banner Description'] = "รายละเอียด";
$lang['Url'] = "Url";
$lang['pls_select_file'] = "เลือกไฟล์";
$lang['Create advertisment banner success !!'] = "เพิ่มแบนเนอร์โฆษณาสำเร็จ !!";
$lang['Edit'] = "แก้ไข";
$lang['confirm_delete_image'] = "ต้องการลบรูปภาพใช่หรือไม่";
$lang['Update advertisment banner success !!'] = "แก้ไขแบนเนอร์โฆษณาสำเร็จ !!";
