<?php

$lang['Channel List'] = "รายการช่องรายการ";
$lang['Edit'] = "แก้ไข";
$lang['Channel Name'] = "ชื่อ";
$lang['Channel Description'] = "รายละเอียด";
$lang['Channel Categories'] = "หมวดหมู่ช่องรายการ";
$lang['Edit channel successful!!'] = "แก้ไขช่องรายการสำเร็จ!!";
$lang['Export Rating'] = "Export Rating";
$lang['MV Request Streaming'] = "MV Request Streaming";
$lang['MV Streaming Temp'] = "MV Streaming Temp";
$lang['MV Status'] = "MV Status";
$lang['Unactive'] = "Unactive";
$lang['Active'] = "Active";
$lang['Direct Streaming Link'] = "Direct Streaming Link";
$lang['Direct Streaming Status'] = "Direct Streaming Status";
$lang['Map update channel (O5)'] = "Map update channel (O5)";
