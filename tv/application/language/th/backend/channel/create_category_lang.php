<?php

$lang['Categories List'] = "หมวดหม่รายการทั้งหมด";
$lang['Create'] = "สร้าง";
$lang['Channel Category'] = "หมวดหมู่รายการ";
$lang['Form'] = "แบบฟอร์ม";
$lang['Edit'] = "แก้ไข";
$lang['Category Name'] = "ชื่อหมวดหมู่";
$lang['Category Description'] = "รายละเอียด";
$lang['Create category successful!!'] = "สร้างหมวดหมู่ช่องรายการสำเร็จ";
$lang['Delete channel category success'] = "ลบหมวดหมู่ช่องรายการเรียบร้อย";
$lang['Edit category successful!!'] = "แก้ไขหมวดหมู่ช่องรายการสำเร็จ!!";
