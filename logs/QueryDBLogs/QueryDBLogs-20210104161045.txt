2021-01-04 16:10:45 query string : <pre>SELECT *
FROM "agent"
WHERE "agent"."FixIt01" = 'T'
AND "agent"."AgentName" NOT LIKE  '%TEST%'
AND
	("FixIt01" = 'T' or "FixIt02" = 'T' or "FixIt03" = 'T' or "FixIt04" = 'T' or "FixIt05" = 'T')
AND
	"agent"."AgentType" NOT IN('AP', 'GN', '')
AND "status" = 'O'
ORDER BY "agent"."BranchCode" ASC,
	"agent"."AgentCode" ASC
 OFFSET 0 ROWS FETCH NEXT 20 ROWS ONLY</pre>
