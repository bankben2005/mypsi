<?php
date_default_timezone_set('Asia/Bangkok');
define('ENV_PRODUCT','production');
define('secret_key',':}E6Uc)>FD*[q[>T');
class CronProductNotification{
// Create connection
	private $conn;
	private $last_id;
	private $curl_authentication_header = array();
	private $notification_url = '';
	function __construct(){
			// $this->connect_db();
		$this->curl_authentication_header = array(
			'Content-Type: application/json; charset=utf-8',
			'username:PsiCare',
			'password:dq3JTzXfz9eMYvPW',
			'secretkey:4jeMW5 ',
			'function:SetProductPartNotification'
		);
		$this->setUrl();

	}

	private function connect_db(){
		
		// } 
		$serverName = "DESKTOP-ONSAO6M\SQLEXPRESS"; //serverName\instanceName
		$connectionInfo = array( "Database"=>"PsiCRM", "UID"=>"", "PWD"=>"");
		$conn = sqlsrv_connect( $serverName, $connectionInfo);

		if( $conn ) {
			$this->conn = $conn;
		}else{
		     echo "Connection could not be established.<br />";
		     die( print_r( sqlsrv_errors(), true));
		}

	}

	private function setUrl(){
		switch (ENV_PRODUCT) {
			case 'development':
				# code...
				$this->notification_url = "http://psi.dev/api/PsiCare/SetProductPartNotification";
			break;
			case 'testing':
				$this->notification_url = 'http://armservice.psisat.com/api/PsiCare/SetProductPartNotification';
			break;
			
			case 'production':
				$this->notification_url = 'http://apiservice.psisat.com/api/PsiCare/SetProductPartNotification';
			break;
			default:
				# code...
			break;
		}
	}

	public function setNotificationToCustomer(){

				// echo $this->notification_url;exit;

				// $sql = "select * from ProductPartNotification where product_part_next_notification_date = '".date('Y-m-d')."' and product_part_status_id != '3'";
				// $stmt = sqlsrv_query( $this->conn, $sql);

				// $row_count = sqlsrv_num_rows($stmt);

				// while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
				// 	print_r($row);
				// }
				// exit;

				// print_r($row);exit;

				// $webhook_url = "";
				// switch (ENVIRONMENT) {
				// 	case 'development':
				// 		$webhook_url = 'http://roampass.front/webhooks/subscriptionV2';
				// 	break;
				// 	case 'testing':

				// 	break;

				// 	case 'production':
				// 		$webhook_url = 'https://roampass.sg/webhooks/subscriptionV2';
				// 	break;
					
				// 	default:
						
				// 		break;
				// }
				$fields = array(
					'test' => 'testtest'
				);

				$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->notification_url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->curl_authentication_header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                //print_r($response);exit;
                if($response){
                		$log_file_path = $this->createLogFilePath('Notification Cron');

			            $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($response) . "\n";
			            file_put_contents($log_file_path, $file_content, FILE_APPEND);
			            unset($file_content);
                }

				

	}
	
	public function connClose(){
		// $this->conn->close();
		sqlsrv_close( $this->conn );

	}
	private function createLogFilePath($filename = '') {
        
        //$document_root = $_SERVER['DOCUMENT_ROOT'];

        $log_path = '../../logs/cron';
        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }
}

$cron = new CronProductNotification();
$cron->setNotificationToCustomer();
//$cron->setSubscriptionSchedule();
//$cron->checkTransactionSubscription();
//$cron->connClose();

?>