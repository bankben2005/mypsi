<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class TestCron{

    function __construct(){

        // echo "abcd";
    }

    public function runCron(){

        //echo $_SERVER['DOCUMENT_ROOT'];exit;

            $data = array(
                'module' => 'testCron'
            );

            $log_file_path = $this->createLogFilePath('Run Cron');

            $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($data) . "\n";
            file_put_contents($log_file_path, $file_content, FILE_APPEND);
            unset($file_content);


    }

    private function createLogFilePath($filename = '') {
        
        //$document_root = $_SERVER['DOCUMENT_ROOT'];

        $log_path = '../../logs/cron';
        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }




}

$testCron = new TestCron();
$testCron->runCron();


?>