<?php


if (!function_exists('message_warning')) {

    /**
     *
     * @param <type> $obj ให้ใช้เป็น $this นะค่ะ
     * @param <type> $return ต้องการให้บรรทัดล่างแสดงหรือไม่
     */
    function message_warning($obj) {
        $text = '';
        $messages = $obj->msg->get();
        //var_dump($messages);
       if (is_array($messages) && count($messages['success']) > 0){
            foreach($messages['success'] as $row){
                $text .= '<span class="success">' .$row. '</span>';
            }
            echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>สำเร็จ!!</strong><br> '.$text."</div>";
       }else  if (is_array($messages) && count($messages['error']) > 0){
            foreach($messages['error'] as $row){
            
                $text .= '<span class="error">' .$row. '</span>';
            }
            echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>×</button><strong>เกิดข้อผิดพลาด!!</strong><br> ".$text."</div>";
       }
        return FALSE;
        
        
    }

}