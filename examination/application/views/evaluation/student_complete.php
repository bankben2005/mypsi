<style type="text/css">
	.breadcrumb{
		display: none;
	}

</style>
<div class="row">
<div class="col-md-12">
	<div class="col-md-12">
	<div class="alert alert-success">
	<strong>สำเร็จ!</strong> ขอบคุณสำหรับการประเมิน

	</div>
	</div>
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<p>
			<div class="text-center mb20">
			<?php 
				if($total_percent >= 70 && $total_percent <= 100){
					echo '<img src="'.base_url('assets/images/emotion-icon/good.png').'" class="mb20" width="100px" height="100px"';
				}else if($total_percent >= 50 && $total_percent < 70){
					echo '<img src="'.base_url('assets/images/emotion-icon/well.png').'" class="mb20" width="100px" height="100px"';
				}else if($total_percent >= 30 && $total_percent < 50){
					echo '<img src="'.base_url('assets/images/emotion-icon/not-bad.png').'" class="mb20" width="100px" height="100px"';
				}else if($total_percent < 30) {
					echo '<img src="'.base_url('assets/images/emotion-icon/not-good.png').'" class="mb20" width="100px" height="100px"';
				}

			?>
			</div>
			<span>
				<?php
				if($total_percent >= 70 && $total_percent <= 100){
				?>
				<div class="alert alert-success"><strong>ผลการประเมิน: </strong>ผลประเมินอยู่ในเกณฑ์ดี - ดีมาก ให้นักศึกษารักษาความดีต่อไป</div>
				<?php }else if($total_percent >= 50 && $total_percent < 70){?>

				<div class="alert alert-info"><strong>ผลการประเมิน: </strong>ผลประเมินอยู่ในเกณฑ์ดี ให้รักษาความดี และและยังสามารถทำดีได้มากขึ้นอีก</div>
				<?php }else if($total_percent >= 30 && $total_percent < 50){?>
				<div class="alert alert-warning"><strong>ผลการประเมิน: </strong>ผลการประเมินอยู่ในระดับที่ต้องปรับปรุง ให้นักศึกษาพัฒนาตัวเองให้มีจิตสาธารณะให้มากขึ้น</label>

				<?php }else if($total_percent < 30){?>
				<div class="alert alert-danger"><strong>ผลการประเมิน: </strong>ผลการประเมินอยู่ในระดับไม่ผ่านการประเมิน ให้นักศึกษาปรับปรุงตัวให้มากขึ้น</div>

				<?php }?>
			</span>
			</p>
			<p>
			<label>คิดเป็นเปอเซนต์: </label>
			<span> 
				<?php if($summary_grade == "pass"){?>
				<label class="label label-success"> <?php echo number_format($total_percent,2).' %';?></label>
			<?php }else{?>
				<label class="label label-danger"> <?php echo number_format($total_percent,2).' %';?></label>

			<?php }?>
			</span>
			</p>
			</div>
		</div>
		<div class="row text-center">
			<a href="<?php echo base_url()?>" class="btn btn-primary">กลับหน้าแรก</a>
		</div>

	</div>
</div>
</div>