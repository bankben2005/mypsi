 <div class="container">
        <div class="row">
            <div class="col-md-12">
            		<ol class="breadcrumb">
					  <li class="breadcrumb-item"><a href="<?php echo base_url();?>">หน้าแรก</a></li>
					  <li class="breadcrumb-item active">แบบประเมินสำหรับอาจารย์ (รายชื่อนักศึกษาทั้งหมด)</li>
					</ol>

					<!-- Button trigger modal -->
					<button type="button" class="btn btn-warning mb20 pull-right" data-toggle="modal" data-target="#myModal">
					 <i class="fa fa-info-circle" aria-hidden="true"></i> ดูคำชี้แจง
					</button>
					<div class="clearfix"></div>
					<div class="col-md-12">
						<div class="row">
						<div class="table-responsive">
						  <table class="table table-striped">
						  <thead>
						  	<th>#</th>
						  	<th>รหัสนักศึกษา</th>
						  	<th>ชื่อ-นามสกุล</th>
						  	<th>ชั้น(ชั้นปีที่)</th>
						  	<th>ห้อง</th>
						  	<th>เลขที่</th>
						  	<th>สถานะ</th>
						  	<th></th>
						  </thead>
						  <tbody>
						  		<?php $count=1; foreach($student_list as $row){
						  			$query = $this->db->select('*')
						  				->from('evaluation_form')
						  				->where('related_student_id',$row['id'])
						  				->get();
						  			$rowForm = $query->row();

						  		?>
						  			<tr>
						  				<td><?php echo $count++;?></td>
						  				<td><?php echo $row['code']?></td>
						  				<td>
						  				<?php 
						  					$title = "";
						  					if(isset($row['title'])){
						  						switch ($row['title']) {
							  						case 'Master':
	                                                    # code...
	                                                $title  = "เด็กชาย";
	                                                    break;
	                                                case "Miss":
	                                                $title = "เด็กหญิง/นางสาว";
	                                                break;
	                                                case "Mr":
	                                                $title = "นาย";
	                                                break;
	                                                case "Mrs":
	                                                $title = "นาง";
	                                                break;
	                                                
	                                                default:
	                                                    # code...
	                                                    break;
						  						}
						  					}
						  				?>

						  				<?php echo $title.' '.$row['firstname'].' - '.$row['lastname']?>
						  					

						  				</td>
						  				<td><?php echo $row['level'];?></td>
						  				<td><?php echo $row['room'];?></td>
						  				<td><?php echo $row['number'];?></td>
						  				<td><?php echo (!empty($rowForm))?"ประเมินแล้ว":"ยังไม่ได้ประเมิน"?></td>
						  				<td>
						  					<?php if(!empty($rowForm)){?>
						  					
						  					<?php }else{?>
						  						<a href="<?php echo base_url('evaluation/teacherEvaluationForm/'.$row['id'])?>" class="btn btn-primary">เริ่มประเมิน</a>
						  					<?php }?>
						  				</td>
						  			</tr>

						  		<?php }?>

						  </tbody>
						  </table>
						</div>
						</div>
					</div>
      		</div>

        </div>

        		 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">คำชี้แจง</h4>
        </div>
        <div class="modal-body">
          <p>
          1. ข้อมูลนี้จะถูกเก็บเป็นความลับเฉพาะ กรุณาตอบให้ตรงกับความจริงที่สุด
          </p>
          <p>
          2. โปรดอ่านข้อมูลอย่างละเอียด และกดปุ่มเลือกในช่องระดับที่เป็นความจริงหรือใกล้เคียงกับความคิดเห็นของท่านมากที่สุดในแต่ละหัวข้อ โดยพิจารณาเกณฑ์ของแต่ละระดับ ดังนี้
          </p>
          <table class="table table-striped">
          	<tbody>
          		<tr>
          			<td> 5 ตรงกับระดับความพึงพอใจ</td>
          			<td>มากที่สุด</td>
          		</tr>	
          		<tr>
          			<td> 4 ตรงกับระดับความพึงพอใจ</td>
          			<td>มาก</td>
          		</tr>	
          		<tr>
          			<td> 3 ตรงกับระดับความพึงพอใจ</td>
          			<td>ปานกลาง</td>
          		</tr>	
          		<tr>
          			<td> 2 ตรงกับระดับความพึงพอใจ</td>
          			<td>น้อย</td>
          		</tr>	
          		<tr>
          			<td> 1 ตรงกับระดับความพึงพอใจ</td>
          			<td>น้อยที่สุด/ควรปรับปรุง</td>
          		</tr>	

          	</tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
      </div>
      
    </div>
  </div>

</div>
