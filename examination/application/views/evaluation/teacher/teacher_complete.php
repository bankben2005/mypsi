<div class="row">
<div class="col-md-12">
	<div class="alert alert-success">
	<strong>สำเร็จ!</strong> ขอบคุณสำหรับการประเมิน

	</div>
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<p>
			<label>ผลการประเมิน: </label>
			<span>
			<?php if($summary_grade == "pass"){?>
				<label class="label label-success"> ผ่าน</label>
			<?php }else{?>
				<label class="label label-danger"> ไม่ผ่าน</label>

			<?php }?>
			</span>
			</p>
			<p>
			<label>คิดเป็นเปอเซนต์: </label>
			<span> 
				<?php if($summary_grade == "pass"){?>
				<label class="label label-success"> <?php echo number_format($total_percent,2).' %';?></label>
			<?php }else{?>
				<label class="label label-danger"> <?php echo number_format($total_percent,2).' %';?></label>

			<?php }?>
			</span>
			</p>
			</div>
		</div>
		<div class="row text-center">
			<a href="<?php echo base_url('evaluation/teacher')?>" class="btn btn-primary">กลับหน้าหลัก</a>
		</div>

	</div>
</div>
</div>