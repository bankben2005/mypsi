 <?php //var_dump($_SESSION)?>
 <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="row">
            	<div class="col-md-6 col-md-offset-3">
            		<ol class="breadcrumb">
					  <li class="breadcrumb-item"><a href="<?php echo base_url();?>">หน้าแรก</a></li>
					  <li class="breadcrumb-item active">กรอกแบบประเมินสำหรับนักศึกษา</li>
					</ol>
				</div>
            	<div class="col-md-6 col-md-offset-3 pd20" id="evaluation_choice">
	            	
						<div class="choice-detail pd10">
							<h4><?php echo $choice_number;?>. <?php echo $evaluation_detail;?></h4>
						</div>
						<div class="choice-chooser pd10">
								<?php foreach($choice_rating as $key => $val){?>
								<p>
									<input type="radio" name="choice_rating" value="<?php echo $val['id']?>" id="rating_<?php echo $val['id']?>">
									<label for="rating_<?php echo $val['id']?>" class="cursor-pointer"><?php echo $val['detail']?></label> 
									</p>
								<?php }?>

								<input type="hidden" name="evaluation_choice_id" value="<?php echo $evaluation_choice_id?>">
								<input type="hidden" name="choice_number" value="<?php echo $choice_number?>">
								<div class="text-center">
								<a href="javascript:void(0)" onclick="nextChoice()" class="btn btn-primary">ถัดไป</a>
								</div>
						</div>

            			
            	</div>

            	</div>
            
            </div>

        </div>


</div>