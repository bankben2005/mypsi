<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            รายการทำข้อสอบ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> หน้าหลัก
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <?php //var_dump($examination_form);?>
                    <div class="col-md-12">
                    <?php //var_dump($examination_form->result_array())?>
                    <?php message_warning($this);?>
                    <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>รหัส</th>
                                        <th>ชื่อ-สกุล</th>
                                        <th>Level (ความยากง่าย)</th>
                                        <th>จำนวน (ทำถูก)</th>
                                        <th>จำนวน (ทำผิด)</th>
                                        <th>จำนวน (ไม่ได้ทำ)</th>
                                        <th>ส่งข้อสอบเมื่อ</th>
                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $count = 1;
                                    foreach ($examination_form->result() as $key => $value) { ?>
                                    <tr>
                                    <td><?php echo $count++;?></td>
                                    <td><?php echo $value->code?></td>
                                    <td><?php echo $value->firstname.' '.$value->lastname;?></td>
                                    <td><?php echo $value->level_txt;?></td>
                                    
                                    <td><?php echo $value->total_correct;?></td>
                                    <td><?php echo $value->total_incorrect;?></td>
                                    <td><?php echo $value->total_did_not;?></td>
                                    <td><?php echo $value->created_form?></td>
                                    <td></td>
                                    </tr>
                                  <?php     
                                    }

                                ?>
                                    
                                   
                                </tbody>
                                
                            </table>
                    </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>