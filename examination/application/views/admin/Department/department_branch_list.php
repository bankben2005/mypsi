<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            แขนงวิชา <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-book"></i> จัดการแขนงวิชา
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this);?>

                    <span class="pull-right">
                        <a href="<?php echo base_url('department/DepartmentBranchAdd')?>" class="btn btn-primary mb20">
                        เพิ่มแขนงวิชา
                        </a>
                        </span>
                        <div class="clearfix"></div>
                    <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ชื่อแขนงวิชา</th>
                                        <th>สังกัดสาขาวิชา</th>
                                        <th>จำนวนบท</th>
                                        <th>แก้ไขล่าสุด</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $count = 1;
                                    foreach ($department_branch->result() as $key => $value) {?>
                                        <tr>
                                        <td><?php echo $count++;?></td>
                                        <td><?php echo $value->department_branch_name;?></td>
                                        <td><?php echo $value->department_name;?></td>
                                        <td><?php echo $value->total_lesson;?></td>
                                        <td><?php echo $value->updated?></td>
                                        <td>
                                            <a href="<?php echo base_url('department/DepartmentBranchEdit/'.$value->branch_id);?>" class="btn btn-default">
                                            แก้ไข
                                            </a>
                                            <a href="<?php echo base_url('department/DepartmentBranchDelete/'.$value->branch_id);?>" class="btn btn-danger">
                                            ลบ
                                            </a>
                                        </td>

                                        </tr>
                                    <?php
                                    }
                                    ?>
                                   
                                </tbody>
                                
                            </table>
                    </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>