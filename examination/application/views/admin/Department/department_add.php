<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            เพิ่มรายวิชา <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('department')?>"><i class="fa fa-book"></i> จัดการรายวิชา</a>
                            </li>
                            <li class="active">
                                เพิ่มรายวิชา
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                            <?php echo form_open('',array())?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ชื่อรายวิชา : </label>
                                        <?php echo form_input(array('name'=>'name','class'=>'form-control','required'=>'required'))?>
                                    </div>
                                    

                                    <div class="form-group">
                                        <label>รายละเอียด</label>
                                        <?php echo form_textarea('detail','',array('class'=>'form-control'))?>
                                    </div>


                                    <span class="pull-right">
                                    <?php echo form_submit('submit_form','บันทึก',array('class'=>'btn btn-primary'))?>
                                    </span>
                                </div>

                           

                            <?php echo form_close();?>
                        </div>

                    </div>

                    </div>
                </div>

                

            </div>
</div>
</div>



