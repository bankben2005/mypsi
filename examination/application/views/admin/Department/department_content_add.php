<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            เพิ่มเนื้อหาวิชา <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('department/DepartmentContent')?>"><i class="fa fa-book"></i> จัดการเนื้อหาวิชา</a>
                            </li>
                            <li class="active">
                                เพิ่มเนื้อหาวิชา
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                            <?php echo form_open('',array())?>
                                <div class="col-md-4 col-md-offset-4">
                                    <div class="form-group">
                                    <label>สังกัดรายวิชา</label>
                                    <?php echo form_dropdown('examination_department_id',$department,'',array('class'=>'form-control','onchange'=>'getDepartmentBranchByDepartment(\''.base_url().'\',this.value)'))?>
                                    </div>

                                    <div class="form-group" id="box-branch">
                                        <label>แขนงวิชา</label>
                                        <?php echo form_dropdown('examination_department_branch_id',array(),'',array('class'=>'form-control','id'=>'examination_department_branch_id','onchange'=>'getTotalLessonByDepartmentBranch(\''.base_url().'\',this.value)'));?>
                                    </div>

                                    <div class="form-group" id="box-level">
                                        <label>บทที่</label>
                                        <?php echo form_dropdown('lesson_level',array(),'',array('class'=>'form-control','id'=>'lesson_level'));?>
                                    </div>

                                    <div class="form-group">
                                     <label>วิดีโอ (Youtube,..etc)</label>
                                     <?php echo form_input(array('type'=>'url','name'=>'vdo_url','class'=>'form-control'))?>
                                    </div>

                                    
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>เนื้อหาวิชา</label>
                                        <?php echo form_textarea('content','',array('class'=>'form-control','required'=>'required'))?>
                                    </div>

                                    <span class="pull-right">
                                    <?php echo form_submit('submit_form','บันทึก',array('class'=>'btn btn-primary'))?>
                                    </span>
                                </div>

                           

                            <?php echo form_close();?>
                        </div>

                    </div>

                    </div>
                </div>

                

            </div>
</div>
</div>
