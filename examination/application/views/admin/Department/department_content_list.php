<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            เนื้อหาวิชา <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-book"></i> เนื้อหาวิชา
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this);?>

                    <span class="pull-right">
                        <a href="<?php echo base_url('department/DepartmentContentAdd')?>" class="btn btn-primary mb20">
                        เพิ่มเนื้อหาวิชา
                        </a>
                        </span>
                        <div class="clearfix"></div>
                    <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>เนื้อหาวิชา</th>
                                        <th>สังกัดสาขาวิชา</th>
                                        <th>สังกัดแขนงวิชา</th>
                                        <th>บทที่</th>
                                        <th>วิดีโอ (Youtube,..etc)</th>
                                        <th>แก้ไขล่าสุด</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $count = 1;
                                    foreach ($department_content->result() as $key => $value) {?>
                                        <tr>
                                        <td><?php echo $count++;?></td>
                                        <td><?php echo mb_strimwidth($value->content, 0, 30, "...");?></td>
                                        <td><?php echo $value->department_name;?></td>
                                        <td><?php echo $value->branch_name;?></td>
                                        <td><?php echo $value->lesson_level;?></td>
                                        <td>
                                                <?php if($value->vdo_url != ''){?>
                                                <a href="<?php echo $value->vdo_url?>" target="_blank">คลิกเพื่อดู</a>
                                                <?php }else{echo '-';}?>
                                        </td>
                                        <td><?php echo $value->updated?></td>
                                        <td>
                                            <a href="<?php echo base_url('department/DepartmentContentEdit/'.$value->content_id);?>" class="btn btn-default">
                                            แก้ไข
                                            </a>
                                            <a href="<?php echo base_url('department/DepartmentContentDelete/'.$value->content_id);?>" class="btn btn-danger">
                                            ลบ
                                            </a>
                                        </td>

                                        </tr>
                                    <?php
                                    }
                                    ?>
                                   
                                </tbody>
                                
                            </table>
                    </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>