<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            ประเภทช่าง <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-book"></i> จัดการประเภทช่าง
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this);?>

                    <span class="pull-right">
                        <a href="<?php echo base_url('department/department_add')?>" class="btn btn-primary mb20">
                        เพิ่มรายวิชา
                        </a>
                        </span>
                        <div class="clearfix"></div>
                    <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ชื่อสาขาวิชา</th>
                                        <th>แก้ไขล่าสุด</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $count = 1;
                                    foreach ($examination_department->result() as $key => $value) {?>
                                        <tr>
                                        <td><?php echo $count++;?></td>
                                        <td><?php echo $value->name;?></td>
                                        <td><?php echo $value->updated?></td>
                                        <td>
                                            <a href="<?php echo base_url('department/department_edit/'.$value->id);?>" class="btn btn-default">
                                            แก้ไข
                                            </a>
                                           <!--  <a href="<?php echo base_url('department/department_delete/'.$value->id);?>" class="btn btn-danger">
                                            ลบ
                                            </a> -->
                                        </td>

                                        </tr>
                                    <?php
                                    }
                                    ?>
                                   
                                </tbody>
                                
                            </table>
                    </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>