<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            เพิ่มแขนงวิชา <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('department/DepartmentBranch')?>"><i class="fa fa-book"></i> จัดการแขนงวิชา</a>
                            </li>
                            <li class="active">
                                เพิ่มแขนงวิชา
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                            <?php echo form_open('',array())?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>สังกัดรายวิชา</label>
                                    <?php echo form_dropdown('examination_department_id',$department,'',array('class'=>'form-control'))?>
                                    </div>
                                    <div class="form-group">
                                        <label>ชื่อแขนงวิชา : </label>
                                        <?php echo form_input(array('name'=>'name','class'=>'form-control','required'=>'required'))?>
                                    </div>
                                    <div class="form-group">
                                    <label>จำนวนบท</label>
                                    <?php echo form_input(array('type'=>'number','name'=>'total_lesson','value'=>'1','class'=>'form-control','min'=>'1','onkeypress'=>'validate_number(event)'))?>
                                    </div>
                                    
                                    <span class="pull-right">
                                    <?php echo form_submit('submit_form','บันทึก',array('class'=>'btn btn-primary'))?>
                                    </span>
                                </div>

                           

                            <?php echo form_close();?>
                        </div>

                    </div>

                    </div>
                </div>

                

            </div>
</div>
</div>

<script type="text/javascript">
        function validate_number(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode( key );
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
          }
        }

</script>