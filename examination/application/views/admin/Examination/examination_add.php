<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            เพิ่มข้อสอบ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('examination')?>"><i class="fa fa-edit"></i> จัดการข้อสอบ</a>
                            </li>
                            <li class="active">
                                เพิ่มข้อสอบ
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                            <?php echo form_open_multipart('',array())?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>วิชา : </label>
                                    <?php echo form_dropdown('examination_department_id',$department,'',array('class'=>'form-control','id'=>'examination_department_id'));?>
                                    </div>
                                    
                                    <div class="form-group">
                                    <label>คำถาม : </label>
                                    <?php echo form_input(array('name'=>'question','class'=>'form-control','required'=>'required'))?>
                                    </div>
                                    <div class="form-group">
                                    

                                    <div class="form-group mt10">
                                    <label>รูปภาพ : </label>
                                    <?php echo form_input(array('name'=>'image','type'=>'file'))?>
                                    </div>

                                    <div class="form-group mt10">
                                    <label>ระดับความยาก : </label>
                                    
                                    <?php echo form_dropdown('examination_level_id',$level,'','class="form-control"')?>
                                    </div>
                                </div>

                                
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                    <label>กรอกคำตอบ และ เลือกข้อที่ถูกต้อง : </label>
                                    <div class="input-group">
                                      <input type="text" name="answer[]" class="form-control" placeholder="คำตอบข้อที่ 1" aria-describedby="basic-addon2" required="required">
                                      <span class="input-group-addon" id="basic-addon2">
                                      
                                      <?php echo form_radio('correct_choice','0',true)?>
                                                                              
                                      </span>
                                    </div>
                                    <div class="input-group mt10">
                                      <input type="text" name="answer[]" class="form-control" placeholder="คำตอบข้อที่ 2" aria-describedby="basic-addon2" required="required">
                                      <span class="input-group-addon" id="basic-addon2">
                                      <?php echo form_radio('correct_choice','1')?>                                          
                                      </span>
                                    </div>
                                    <div class="input-group mt10">
                                      <input type="text" name="answer[]" class="form-control" placeholder="คำตอบข้อที่ 3" aria-describedby="basic-addon2" required="required">
                                      <span class="input-group-addon" id="basic-addon2">
                                      <?php echo form_radio('correct_choice','2')?>                                          
                                      </span>
                                    </div>
                                    <div class="input-group mt10">
                                      <input type="text" name="answer[]" class="form-control" placeholder="คำตอบข้อที่ 4" aria-describedby="basic-addon2" required="required">
                                      <span class="input-group-addon" id="basic-addon2">
                                      <?php echo form_radio('correct_choice','3')?>                                          
                                      </span>
                                    </div>
                                    

                                    </div>

                                    </div>

                            </div>
                            <div class="col-md-12">
                                <span class="pull-right">
                                    <?php echo form_input(array('type'=>'submit','value'=>'บันทึก','class'=>'btn btn-primary'))?>
                                </span>
                            </div>
                           

                            <?php echo form_close();?>
                        </div>

                    </div>

                    </div>
          

                

            </div>
</div>
</div>
</div>

<script type="text/javascript">
    function changeState(el) {
        if (el.readOnly) el.checked=el.readOnly=false;
        else if (!el.checked) el.readOnly=el.indeterminate=true;
    }
</script>