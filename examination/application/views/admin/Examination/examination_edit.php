<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            แก้ไขข้อสอบ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('examination')?>"><i class="fa fa-edit"></i> จัดการข้อสอบ</a>
                            </li>
                            <li class="active">
                                แก้ไขข้อสอบ
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                            <?php echo form_open_multipart('',array())?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>วิชา : </label>
                                    <?php echo form_dropdown('examination_department_id',$department,$examination->examination_department_id,array('class'=>'form-control','id'=>'examination_department_id'));?>
                                    </div>

                                    
                                    <div class="form-group">
                                    <label>คำถาม : </label>
                                    <?php echo form_input(array('name'=>'question','value'=>$examination->question,'class'=>'form-control','required'=>'required'))?>
                                    </div>
                                    <div class="form-group">
                                    

                                    <div class="form-group mt10">
                                    <label>รูปภาพ : </label>

                                    <?php 
                                        if($examination->image){
                                            echo "<p><img src='".base_url('uploaded/examination/'.$examination->id.'/'.$examination->image)."' width='70' height='70'></p>";
                                        }
                                    ?>
                                    <?php echo form_input(array('name'=>'image','type'=>'file'))?>
                                    </div>

                                    <div class="form-group mt10">
                                    <label>ระดับความยาก : </label>
                                    
                                    <?php echo form_dropdown('examination_level_id',$level,$examination->examination_level_id,'class="form-control"')?>
                                    </div>
                                </div>

                                
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                    <label>กรอกคำตอบ และ เลือกข้อที่ถูกต้อง : </label>

                                    <?php 

                                        foreach ($examination_answer->result() as $key => $value) {?>
                                            <div class="input-group mt10">
                                              <input type="text" name="answer[<?php echo $value->id?>]" value="<?php echo $value->answer;?>" class="form-control"  aria-describedby="basic-addon2" required="required">
                                              <span class="input-group-addon" id="basic-addon2">
                                              
                                              <?php //echo form_radio('correct_choice',''.$key.'',)?>
                                                <input type="radio" name="correct_choice" value="<?php echo $value->id;?>" <?php echo ($value->is_correct)?"Checked":""?>>                                      
                                              </span>
                                            </div>
                                    <?php    }
                                    ?>
                                    

                                    </div>

                                    </div>

                            </div>
                            <div class="col-md-12">
                                <span class="pull-right">
                                    <?php echo form_input(array('type'=>'submit','value'=>'บันทึก','class'=>'btn btn-primary'))?>
                                </span>
                            </div>
                           

                            <?php echo form_close();?>
                        </div>

                    </div>

                    </div>
          

                

            </div>
</div>
</div>
</div>

<script type="text/javascript">
    function changeState(el) {
        if (el.readOnly) el.checked=el.readOnly=false;
        else if (!el.checked) el.readOnly=el.indeterminate=true;
    }
</script>