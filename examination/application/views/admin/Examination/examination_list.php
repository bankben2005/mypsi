<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            จัดการข้อสอบ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator');?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li class="active">
                            <i class="fa fa-edit"></i> จัดการข้อสอบ
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this);?>

                    <span class="pull-right">
                        <a href="<?php echo base_url('examination/examination_add')?>" class="btn btn-primary mb20">
                        เพิ่มข้อสอบ
                        </a>
                        </span>
                        <div class="clearfix"></div>
                    <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>คำถาม</th>
                                        <th>รูปภาพ</th>
                                        <!-- <th>นักศึกษา/อาจารย์</th> -->
                                        <th>วิชา</th>
                                        <th>ความยากง่าย</th>
                                       
                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $count = 1;
                                foreach ($examination->result() as $key => $value) { 
                                    # code...
                                //var_dump($value);
                                ?>
                                    <tr>
                                    <td><?php echo $count++;?></td>
                                    <td><?php echo $value->question;?></td>
                                    <td>
                                        <?php if($value->image){?>
                                            <img src="<?php echo base_url('uploaded/examination/'.$value->ex_id.'/'.$value->image)?>" width="70" height="70">

                                        <?php }?>
                                    </td>
                                    <td>
                                        <?php echo $value->department_name; ?>
                                    </td>
                                    
                                    <td>
                                        <?php echo $value->level_txt;?>
                                    </td>

                                    <td>
                                    <a href="<?php echo base_url('examination/examination_edit/'.$value->ex_id);?>" class="btn btn-default">แก้ไข</a>
                                    <a href="<?php echo base_url('examination/examination_delete/'.$value->ex_id);?>" class="btn btn-danger">ลบ</a>
                                    </td>
                                    </tr>
                                   
                                <?php } ?>
                                </tbody>
                                
                            </table>
                    </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>