<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            แก้ไขอาจารย์ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>">หนัาหลัก</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('administrator/teacher')?>"><i class="fa fa-user"></i> อาจารย์ </a>
                            </li>
                            <li class="active">
                                 แก้ไขข้อมูล - <?php echo $teacherData->firstname.' '.$teacherData->lastname;?>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-4">
                        <?php echo form_open('',array())?>
                        <div class="form-group">
                        <label>รหัสอาจารย์:</label>
                        <?php echo form_input(array('name'=>'code','value'=>$teacherData->code,'class'=>'form-control','required' =>'required'))?>

                        </div>
                        <div class="form-group">
                        <label>คำนำหน้า:</label>
                         <?php
                            $arrayTitle = array();
                            $arrayTitle = array(
                                'Mr' => 'นาย',
                                'Miss' => 'นางสาว',
                                'Mrs' => 'นาง'
                            );
                            ?>
                            <?php echo form_dropdown('title',$arrayTitle,$teacherData->title,'class="form-control"')?>
                        </div>
                        <div class="form-group">
                        <label>ชื่อ:</label>
                        <?php echo form_input(array('name'=>'firstname','value'=>$teacherData->firstname,'class'=>'form-control','required'=>'required'))?>

                        </div>
                        <div class="form-group">
                        <label>สกุล:</label>
                        <?php echo form_input(array('name'=>'lastname','value'=>$teacherData->lastname,'class'=>'form-control','required'=>'required'))?>

                        </div>
                        <div class="form-group">
                       
                        <?php echo form_submit('','บันทึก','class="btn btn-primary pull-right"')?>

                        </div>

                        
                        </div>
                        
                        <?php echo form_close();?>
                    </div>
                    </div>

                </div>

            </div>
</div>