<style type="text/css">
    .dataTables_filter{
    text-align: right;
}
</style>
<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            นักศึกษา <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-user"></i> นักศึกษา
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                        <span class="pull-right">
                        <a href="<?php echo base_url('administrator/addStudent')?>" class="btn btn-primary mb20">
                        เพิ่มนักศึกษา
                        </a>
                        </span>
                
                <div class="clearfix"></div>
                
                    <div class="table-responsive mt10">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>รหัส</th>
                                        <th>ชื่อ-สกุล</th>
                                        <th>ชั้น(ชั้นปีที่)</th>
                                        <th>ห้อง</th>
                                        <th>เลขที่</th>
                                        <th>สถานะ</th>
                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                        $count = 1;
                                        foreach($studentRow->result() as $row){
                                            $eva_form = $this->db->select('*')
                                                ->from('evaluation_form')
                                                ->where('code',$row->code)
                                                ->get();
                                        ?>
                                        <tr>
                                        <td><?php echo $count++;?></td>
                                        <td><?php echo $row->code;?></td>
                                        <td>
                                        <?php 
                                            $title = "";
                                            switch ($row->title) {
                                                case 'Master':
                                                    # code...
                                                $title  = "เด็กชาย";
                                                    break;
                                                case "Miss":
                                                $title = "เด็กหญิง/นางสาว";
                                                break;
                                                case "Mr":
                                                $title = "นาย";
                                                break;
                                                case "Mrs":
                                                $title = "นาง";
                                                break;
                                                
                                                default:
                                                    # code...
                                                    break;
                                            }
                                            echo $title.' '.$row->firstname.' '.$row->lastname;?>
                                            
                                        </td>
                                        <td><?php echo ($row->level)?$row->level:"-";?></td>
                                        <td><?php echo ($row->room)?$row->room:"-";?></td>
                                        <td><?php echo ($row->number)?$row->number:"-";?></td>
                                        <td><?php echo ($eva_form->num_rows() > 0)?"ทำข้อสอบแล้ว":"ยังไม่ได้ทำข้อสอบ"?></td>
                                        <td>
                                        <a href="<?php echo base_url('administrator/editStudent/'.$row->id)?>" class="btn btn-default">
                                        แก้ไข
                                        </a>
                                        <a href="<?php echo base_url('administrator/deleteStudent/'.$row->id)?>" class="btn btn-danger">
                                        ลบ
                                        </a>
                                        </td>
                                        </tr>

                                        <?php }?>
                                </tbody>
                                
                            </table>
                    </div>
                    
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>