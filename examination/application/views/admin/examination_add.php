<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            เพิ่มข้อสอบ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('examination')?>"><i class="fa fa-user"></i> จัดการข้อสอบ</a>
                            </li>
                            <li class="active">
                                เพิ่มนักศึกษา
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <?php //var_dump($department);exit;?>
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                            <?php echo form_open('',array())?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>วิชา : </label>


                                    <?php echo form_dropdown('departure',$department,'',array('class'=>'form-control'));?>
                                    </div>
                                    <div class="form-group">
                                    <label>คำถาม : </label>
                                    <?php echo form_input(array('name'=>'question','class'=>'form-control'))?>
                                    </div>
                                    <div class="form-group">
                                    <div class="row">
                                    <div class="col-md-12">
                                    <label>เขียนคำตอบ : </label>
                                    <div class="input-group">
                                      <input type="text" name="answer_1" class="form-control" placeholder="คำตอบข้อที่ 1" aria-describedby="basic-addon2">
                                      <span class="input-group-addon" id="basic-addon2">
                                      
                                      <?php echo form_radio('correct_choice')?>
                                                                              
                                      </span>
                                    </div>
                                    <div class="input-group mt10">
                                      <input type="text" name="answer_2" class="form-control" placeholder="คำตอบข้อที่ 2" aria-describedby="basic-addon2">
                                      <span class="input-group-addon" id="basic-addon2">
                                      <?php echo form_radio('correct_choice')?>                                          
                                      </span>
                                    </div>
                                    <div class="input-group mt10">
                                      <input type="text" name="answer_3" class="form-control" placeholder="คำตอบข้อที่ 3" aria-describedby="basic-addon2">
                                      <span class="input-group-addon" id="basic-addon2">
                                      <?php echo form_radio('correct_choice')?>                                          
                                      </span>
                                    </div>
                                    <div class="input-group mt10">
                                      <input type="text" name="answer_4" class="form-control" placeholder="คำตอบข้อที่ 4" aria-describedby="basic-addon2">
                                      <span class="input-group-addon" id="basic-addon2">
                                      <?php echo form_radio('correct_choice')?>                                          
                                      </span>
                                    </div>
                                    

                                    </div>

                                    </div>
                                    <div class="form-group mt10">
                                    <label>Level : </label>
                                    <?php 
                                        $arLevel = array(
                                            '1' => '1',
                                            '2' => '2',
                                            '3' => '3'
                                        );
                                    ?>
                                    <?php echo form_dropdown('level',$arLevel,'','class="form-control"')?>
                                    </div>
                                </div>

                                <span class="pull-right">
                            <?php echo form_input(array('type'=>'submit','value'=>'บันทึก','class'=>'btn btn-primary'))?>
                            </span>
                            </div>
                           

                            <?php echo form_close();?>
                        </div>

                    </div>

                    </div>
                </div>

                

            </div>
</div>

<script type="text/javascript">
    function changeState(el) {
        if (el.readOnly) el.checked=el.readOnly=false;
        else if (!el.checked) el.readOnly=el.indeterminate=true;
    }
</script>