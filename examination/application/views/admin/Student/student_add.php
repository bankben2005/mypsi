<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            เพิ่มนักศึกษา <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('administrator/student')?>"><i class="fa fa-user"></i> นักศึกษา</a>
                            </li>
                            <li class="active">
                                เพิ่มนักศึกษา
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-4">
                        <?php echo form_open('',array())?>
                        <div class="form-group">
                        <label>รหัสนักศึกษา:</label>
                        <?php echo form_input(array('name'=>'code','class'=>'form-control','required' =>'required'))?>

                        </div>
                        <div class="form-group">
                            <label>คำนำหน้าชื่อ</label>
                            <select name="title" class="form-control">
                                <option value="Master">เด็กชาย</option>
                                <option value="Miss">เด็กหญิง</option>
                                <option value="Mr">นาย</option>
                                <option value="Mrs">นาง</option>
                                <option value="Miss">นางสาว</option>
                            </select>

                        </div>
                        <div class="form-group">
                        <label>ชื่อ:</label>
                        <?php echo form_input(array('name'=>'firstname','class'=>'form-control','required'=>'required'))?>

                        </div>
                        <div class="form-group">
                        <label>สกุล:</label>
                        <?php echo form_input(array('name'=>'lastname','class'=>'form-control','required'=>'required'))?>

                        </div>
                        <div class="form-group">
                        <label>ชั้น(ชั้นปีที่)</label>
                        <?php echo form_input(array('name'=>'level','value'=>'1','type'=>'number','max'=>'6','min'=>'1','class'=>'form-control'))?>
                        </div>
                        <div class="form-group">
                        <label>ห้อง</label>
                        <?php echo form_input(array('name'=>'room','value'=>'1','type'=>'number','max'=>'6','min'=>'1','class'=>'form-control'))?>
                        </div>
                        <div class="form-group">
                        <label>เลขที่</label>
                        <?php echo form_input(array('name'=>'number','value'=>'1','type'=>'number','max'=>'100','min'=>'1','class'=>'form-control'))?>
                        </div>
                        <div class="form-group">
                       
                        <?php echo form_submit('','บันทึก','class="btn btn-primary pull-right"')?>

                        </div>

                        
                        </div>
                        
                        <?php echo form_close();?>
                    </div>
                    </div>

                </div>

            </div>
</div>