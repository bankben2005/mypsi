<!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url('administrator')?>">Administrator</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Administrator <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <!-- <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li> -->
                        <li>
                            <a href="<?php echo base_url('administrator/signout')?>"><i class="fa fa-fw fa-power-off"></i> ออกจากระบบ</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php //echo $this->router->fetch_class();exit;?>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li <?php if(($this->router->fetch_class() == 'administrator' && $this->router->fetch_method() == "index") || $this->router->fetch_method() == "evaluationDetail"){
                        echo "class='active'";
                    }
                        ?>>
                        <a href="<?php echo base_url('administrator')?>"><i class="fa fa-fw fa-dashboard"></i> รายการทำข้อสอบ</a>
                    </li>
                    <li <?php if(($this->router->fetch_class() == 'examination' && $this->router->fetch_method() == "index") || $this->router->fetch_method() == "examination_add" || $this->router->fetch_method() == "examination_edit"){
                        echo "class='active'";
                    }
                        ?>>
                        <a href="<?php echo base_url('examination')?>"><i class="fa fa-fw fa-edit"></i> จัดการข้อสอบ</a>
                    </li>
                    <li <?php if(($this->router->fetch_class() == 'department' && $this->router->fetch_method() == "index") || $this->router->fetch_method() == "department_add" || $this->router->fetch_method() == "department_edit" ){
                        echo "class='active'";
                    }
                        ?>>
                        <!-- <a href="<?php echo base_url('department')?>"><i class="fa fa-fw fa-book"></i> จัดการรายวิชา</a> -->

                        <a href="javascript:;" data-toggle="collapse" data-target="#department"><i class="fa fa-fw fa-book"></i> จัดการหมวดหมู่ <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="department" class="collapse">
                            <li>
                                <a href="<?php echo base_url('department')?>"><i class="fa fa-fw fa-book"></i> ประเภทช่าง</a>
                            </li>
                            <li style="display: none;">
                                <a href="<?php echo base_url('department/DepartmentBranch')?>"><i class="fa fa-fw fa-book"></i> แขนงวิชา</a>
                            </li>
                            <li style="display: none;">
                                <a href="<?php echo base_url('department/DepartmentContent')?>">
                                <i class="fa fa-fw fa-book"></i> เนื้อหาวิชา
                                </a>
                            </li>
                        </ul>



                    </li>
                    <?php 
                        $studentMethod = array('addStudent','editStudent','student');
                    ?>
                    <li <?php if($this->router->fetch_class() == "administrator" && in_array($this->router->fetch_method(), $studentMethod)){
                        echo "class='active'";
                    }
                        ?> style="display: none;">
                        <a href="<?php echo base_url('administrator/student')?>"><i class="fa fa-fw fa-user"></i> นักศึกษา</a>
                    </li>

                    <?php
                        $teacherMethod = array('addTeacher','editTeacher');
                    ?>
                    <!-- <li <?php if($this->router->fetch_class() == "administrator" && in_array($this->router->fetch_method(), $teacherMethod)){
                        echo "class='active'";
                    }
                        ?>>
                        <a href="<?php echo base_url('administrator/teacher')?>"><i class="fa fa-fw fa-user"></i> อาจารย์</a>
                    </li> -->
                   
                    <?php 
                        $reportMethod = array(
                            'student','teacher'
                        );
                    ?>
                    <!-- <li <?php if($this->router->fetch_class() == "report" || ($this->router->fetch_class() == "report" && in_array($this->router->fetch_method(), $reportMethod)) ){echo "class='active'";}?>>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-bar-chart-o"></i> รายงาน <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="<?php echo base_url('report/student')?>"><i class="fa fa-fw fa-user"></i> นักศึกษา</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('report/teacher')?>"><i class="fa fa-fw fa-user"></i> อาจารย์</a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- <li>
                        <a href="forms.html"><i class="fa fa-fw fa-edit"></i> Forms</a>
                    </li>
                    <li>
                        <a href="bootstrap-elements.html"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>
                    </li>
                    <li>
                        <a href="bootstrap-grid.html"><i class="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="blank-page.html"><i class="fa fa-fw fa-file"></i> Blank Page</a>
                    </li>
                    <li>
                        <a href="index-rtl.html"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>
                    </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
