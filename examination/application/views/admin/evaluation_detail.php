<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            รายละเอียดแบบประเมินพฤติกรรม <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="">
                               <a href="<?php echo base_url('administrator')?>"> <i class="fa fa-dashboard"></i> หน้าหลัก </a>
                            </li>
                            <li class="active">
                                รายละเอียดแบบประเมินพฤติกรรม
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <p>
                        <label>ผู้ประเมิน:</label><span> <?php echo $evaluationDetail->firstname.' '.$evaluationDetail->lastname;?></span>
                    </p>
                    <?php if($evaluationDetail->access_type == "teacher"){
                        $query = $this->db->select('*')
                            ->from('evaluation_form')
                            ->join('account','account.id = evaluation_form.related_student_id')
                            ->where('evaluation_form.id',$evaluationDetail->eva_id)
                            ->get();
                            $rowStudent = $query->row();
                            //var_dump($rowStudent);
                    ?>
                    <p>
                        <label>ผู้ถูกประเมิน:</label><span> <?php echo (!empty($rowStudent))?$rowStudent->firstname.' '.$rowStudent->lastname:""; ?></span>
                    </p>
                    <p>
                    <label>ผลการประเมิน:</label>
                    <span>
                    <?php if($gradeData['status'] == "pass"){?>
                        <label class="label label-success">ผ่าน</label>
                    <?php }else{?>
                        <label class="label label-danger">ไม่ผ่าน</label>
                    <?php }?>    
                    </span>
                    </p>
                    <p>
                    <label>คิดเป็นเปอเซนต์:</label>
                    <span>
                    
                    <?php if($gradeData['status'] == "pass"){?>
                        <label class="label label-success"><?php echo number_format($gradeData['total_percent'],2).' %'?></label>
                    <?php }else{?>
                        <label class="label label-danger"><?php echo number_format($gradeData['total_percent'],2).' %'?></label>
                    <?php }?>
                        
                    </span>
                    </p>
                    <?php }?>


                    <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>หัวข้อ</th>
                                        <th>เกณฑ์การประเมิน</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $count=1; foreach($evaluationRow->result() as $row){

                                    ?>
                                    <tr>
                                        <td><?php echo $count++;?></td>
                                        <td><?php echo $row->choice_detail?></td>
                                        <td>
                                            <?php
                                            if($row->rating_type == "teacher"){
                                                switch ($row->detail) {
                                                   case '5':
                                                    # code...
                                                echo "มากที่สุด";
                                                    
                                                    case '4':
                                                echo "มาก";
                                                    break;
                                                    case '3':
                                                echo "ปานกลาง";
                                                    break;
                                                    case '2':
                                                echo "น้อย";
                                                    break;
                                                    case '1':
                                                echo "น้อยที่สุด";                                                  break;
                                                default:
                                                    # code...
                                                    break;
                                                }


                                            }else{
                                                echo $row->detail;
                                            }
                                            ?>

                                        </td>
                                        </tr>
                                    <?php }?>
                                  
                                   
                                </tbody>
                                
                            </table>
                    </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>