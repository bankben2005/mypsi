<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            อาจารย์ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-user"></i> อาจารย์
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                        <span class="pull-right">
                        <a href="<?php echo base_url('administrator/addTeacher')?>" class="btn btn-primary">
                        เพิ่มอาจารย์
                        </a>
                        </span>
                        <div class="clearfix"></div>
                    <div class="table-responsive mt10">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>รหัส</th>
                                        <th>ชื่อ-สกุล</th>
                                        <th>สถานะ</th>
                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                        $count = 1;
                                        foreach($teacherRow->result() as $row){
                                            $eva_form = $this->db->select('*')
                                                ->from('evaluation_form')
                                                ->where('account_id',$row->id)
                                                ->get();
                                        ?>
                                        <tr>
                                        <td><?php echo $count++;?></td>
                                        <td><?php echo $row->code;?></td>
                                        <td>
                                            <?php
                                            $title = '';
                                            switch ($row->title) {
                                                case 'Mr':
                                                    # code...
                                                $title = "นาย";
                                                    break;
                                                case "Mrs":
                                                $title = "นาง";
                                                break;  
                                                case "Miss":
                                                $title = "นางสาว";
                                                break;
                                                
                                                default:
                                                    # code...
                                                    break;
                                            }
                                            ?>
                                            <?php echo $title.' '.$row->firstname.' '.$row->lastname;?>
                                                
                                            </td>
                                        <td><?php echo ($eva_form->num_rows() > 0)?"กรอกแบบประเมินแล้ว":"ยังไม่ได้กรอกแบบประเมิน"?></td>
                                        <td>

                                        <a href="<?php echo base_url('administrator/editTeacher/'.$row->id)?>" class="btn btn-default">แก้ไข</a>
                                        <a href="<?php echo base_url('administrator/deleteTeacher/'.$row->id)?>" class="btn btn-danger">
                                        ลบ
                                        </a>
                                        </td>
                                        </tr>

                                        <?php }?>
                                </tbody>
                                
                            </table>
                    </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>