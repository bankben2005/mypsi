<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            เพิ่มอาจารย์ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('administrator/teacher')?>"><i class="fa fa-user"></i> อาจารย์ </a>
                            </li>
                            <li class="active">
                             เพิ่มอาจารย์
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this)?>
                    <div class="row">
                        <div class="col-md-4">
                        <?php echo form_open('',array())?>
                        <div class="form-group">
                        <label>รหัสอาจารย์:</label>
                        <?php echo form_input(array('name'=>'code','class'=>'form-control','required' =>'required'))?>

                        </div>
                        <div class="form-group">
                        <label>คำนำหน้า:</label>
                         <?php
                            $arrayTitle = array();
                            $arrayTitle = array(
                                'Mr' => 'นาย',
                                'Miss' => 'นางสาว',
                                'Mrs' => 'นาง'
                            );
                            ?>
                            <?php echo form_dropdown('title',$arrayTitle,'','class="form-control"')?>
                        </div>
                        <div class="form-group">
                        <label>ชื่อ:</label>
                        <?php echo form_input(array('name'=>'firstname','class'=>'form-control','required'=>'required'))?>

                        </div>
                        <div class="form-group">
                        <label>สกุล:</label>
                        <?php echo form_input(array('name'=>'lastname','class'=>'form-control','required'=>'required'))?>

                        </div>
                        <div class="form-group">
                       
                        <?php echo form_submit('','บันทึก','class="btn btn-primary pull-right"')?>

                        </div>

                        
                        </div>
                        
                        <?php echo form_close();?>
                    </div>
                    </div>

                </div>

            </div>
</div>