<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            จัดการข้อสอบ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> หน้าหลัก
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this);?>

                    <span class="pull-right">
                        <a href="<?php echo base_url('examination/examination_add')?>" class="btn btn-primary mb20">
                        เพิ่มข้อสอบ
                        </a>
                        </span>
                        <div class="clearfix"></div>
                    <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>คำถาม</th>
                                        <th>รูปภาพ</th>
                                        <!-- <th>นักศึกษา/อาจารย์</th> -->
                                        <th>วิชา</th>
                                        <th>สถานะ</th>
                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                   
                                </tbody>
                                
                            </table>
                    </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>