 <div class="container">
        <div class="row">
            <div class="col-lg-12">
            	<div class="col-md-6 col-md-offset-3">
            			<?php message_warning($this);?>
            			<div class="panel panel-default">
	            			<div class="panel-heading"><h3>เข้าสู่ระบบ (ผู้ดูแลระบบ)</h3></div>

	            			<div class="panel-body">
	            				<?php echo form_open('',array())?>
	            					<div class="form-group">
	            					<label>Email:</label>
	            					<?php echo form_input(array('name'=>'email','type'=>'email','class'=>'form-control','required'=>'required'))?>
	            					</div>
	            					<div class="form-group">
	            					<label>Password:</label>
	            					<?php echo form_input(array('name'=>'password','type'=>'password','class'=>'form-control','required'=>'required'))?>
	            					</div>
	            					<div class="form-group">
	            					<?php echo form_submit('','เข้าสู่ระบบ','class="btn btn-primary pull-right"')?>
	            					</div>


	            				<?php echo form_close();?>
	            			</div>

            			</div>
            	</div>

            </div>

         </div>

</div>