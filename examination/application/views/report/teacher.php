
<div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            รายการสรุปแบบประเมินพฤติกรรมสำหรับอาจารย์ <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-bar-chart-o"> รายงานสรุปนักศึกษา</i>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this);?>
                        <?php if($teacher_number > 0){?>
                        <h4>รายงานการประเมินนักศึกษาสำหรับอาจารย์ จากนักศึกษาทั้งหมด <?php echo $teacher_number;?> คน</h4>
                        
                        <?php }?>
                        <div id="all-teacher-chart" class="loading"></div>
                    </div>


                </div>


            </div>
</div>


