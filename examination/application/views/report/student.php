
<div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            รายการสรุปแบบประเมินพฤติกรรมสำหรับนักศึกษา <small></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url('administrator')?>"><i class="fa fa-dashboard"></i> หน้าหลัก</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-bar-chart-o"> รายงานสรุปนักศึกษา</i>
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    
                    <div class="col-md-12">
                    <?php message_warning($this);?>
                        <?php if($student_number > 0){?>
                        <h4>รายงานสรุปจากนักศึกษาที่กรอกแบบสอบถามทั้งหมด <?php echo $student_number;?> คน</h4>
                        <div id="all-student-chart" class="loading"></div    >
                        <?php }?>
                        
                        
                    </div>

                    <div class="col-md-12">
                        <?php if($student_number > 0){?>
                        <h4>รายงานสรุปตามระดับชั้นจากนักศึกษาที่กรอกแบบสอบถามทั้งหมด <?php echo $student_number;?> คน</h4>
                        <div id="student-level-chart" class="loading"></div    >
                        <?php }?>
                    </div>

                </div>


            </div>
</div>


