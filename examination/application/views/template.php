<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>เข้าสู่ระบบ</title>

    <?php echo $this->template->stylesheet;?>
    <!-- Bootstrap Core CSS -->
    <!-- <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom CSS -->
   <!--  <link href="<?php echo base_url()?>assets/css/logo-nav.css" rel="stylesheet"> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url();?>">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="<?php echo base_url();?>">
                   
                    <img src="<?php echo base_url('assets/images/logo/Setthabut-Bamphen-Logo-Color1.png');?>" width="50px" height="50px"> 
                </a>
 -->
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo base_url();?>">หน้าแรก</a>
                    </li>
                    
                    
                </ul>
                <ul class="nav navbar-nav navbar-right">

                <?php if($this->session->userdata('administrator')){?>
                    <li><a href="<?php echo base_url('main/signout');?>">ออกจากระบบ</a></li>
                <?php }else{?>
                    <li><a href="<?php echo base_url('main/signin')?>">ผู้ดูแลระบบ</a></li>
                <?php }?>

                <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">กรอกแบบประเมิน <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">นักศึกษา</a></li>
                            <li><a href="#">อาจารย์</a></li>
                            <li><a href="#">Dropdown Link 3</a></li>
                            
                        </ul>
                    </li> -->
                </ul>


            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
 <?php echo $this->template->content;?>

    <!-- Page Content -->

   <!--  <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Logo Nav by Start Bootstrap</h1>
                <p>Note: You may need to adjust some CSS based on the size of your logo. The default logo size is 150x50 pixels.</p>
            </div>
        </div>
    </div> -->

    <!-- /.container -->
    <script type="text/javascript">
        window.base_url = document.getElementById('base_url').value;
        //console.log(base_url);

    </script>
    <?php echo $this->template->javascript;?>
    <script type="text/javascript">
       
       $(document).ready(function() {
                $('.table').DataTable({responsive:true});
            } );
   </script>

</body>

</html>
