<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EXAMINATION ADMIN</title>

    <?php echo $this->template->stylesheet;?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url();?>">
    <div id="wrapper">

        <?php $this->load->view('admin/navbar_left');?>
        <?php echo $this->template->content;?>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<script type="text/javascript">
        window.base_url = document.getElementById('base_url').value;
        //console.log(base_url);

    </script>
   <?php echo $this->template->javascript;?>
   <script type="text/javascript">
       
       $(document).ready(function() {
                $('.table').DataTable({responsive:true});
            } );
   </script>

</body>

</html>
