 <div class="container">
        <div class="row">
            <div class="col-lg-12">
            
            				<div class="col-md-12 text-center mb20">
               				<img src="<?php echo base_url('assets/images/logo/Setthabut-Bamphen-Logo-Color1.png');?>" >
                           <h5 class="text-green">แบบสอบถามพฤติกรรมจิตสาธารณะโรงเรียนเศรษฐบุตรบำเพ็ญ</h5>
               				</div>
               <div class="col-md-6">
               		<div class="row">

               			<div class="col-md-12">
               			<div class="panel panel-default">
               			<div class="panel-heading text-center">
               			<h4>เริ่มทำแบบสอบถาม</h4>
               			

               			</div>

               			<div class="panel-body">
               				<div class="col-md-12">
                           <div class="col-md-12">
               				<?php message_warning($this);?>
                           </div>
               				</div>
               			<div class="col-md-12 mb20">
               				<div class="row">
               				<div class="col-md-6 col-sm-6 col-xs-6 text-center">
               					<a href="javascript:void(0);" class="col_toggle" data-toggle="collapse" data-target="#student_signin">
               					<div class="box-avatar-home box-home-student">
	               					<img src="<?php echo base_url('assets/images/avatar/student.png')?>" class="panel-avatar">
	               					<p>นักศึกษา</p>
	               					
               					</div>
               					</a>
               				
               				<div class="clearfix"></div>
               				</div>
               				<div class="col-md-6 col-sm-6 col-xs-6 text-center">
               					<a href="javascript:void(0);" class="col_toggle" data-toggle="collapse" data-target="#teacher_signin">
               					<div class="box-avatar-home box-home-teacher">
	               					
									<img src="<?php echo base_url('assets/images/avatar/teacher.png')?>" class="panel-avatar">
									
									<p>อาจารย์</p>
								</div>
								</a>
								<div class="clearfix"></div>
               				</div>
               				</div>
               				</div>
               				<div class="col-md-12">

               					<div id="student_signin" class="collapse col-md-12 in">
               						 <?php echo form_open('',array());?>
               						 <input type="hidden" name="access_type" value="student">
			                              <div class="form-group">
			                                  <!-- <input type="text" class="form-control" id="student_code" placeholder="รหัสนักศึกษา"> -->
			                                  <?php echo form_input(array('name'=>'code','id'=>'student_code','class'=>'form-control','placeholder'=>'กรอกรหัสนักศึกษา'))?>
			                              </div>
			                              <div class="form-group text-center">
			                                 <?php echo form_submit('','เริ่มประเมิน','class="btn btn-primary btn-block"')?>
			                              </div>

                           			<?php echo form_close();?>

               					</div>
               					<div id="teacher_signin" class="collapse col-md-12">
               						 <?php echo form_open('',array());?>
               						 <input type="hidden" name="access_type" value="teacher">
			                              <div class="form-group">
			                                  <!-- <input type="text" class="form-control" id="student_code" placeholder="รหัสนักศึกษา"> -->
			                                  <?php echo form_input(array('name'=>'code','id'=>'student_code','class'=>'form-control','placeholder'=>'กรอกรหัสอาจารย์'))?>
			                              </div>
			                              <div class="form-group text-center">
			                                 <?php echo form_submit('','เริ่มประเมิน','class="btn btn-primary btn-block"')?>
			                              </div>

                           			<?php echo form_close();?>

               					</div>

               				</div>

               			</div>

               			<div class="panel-footer">
               					website : <a href="http://imsbp.com" target="_blank">http://imsbp.com</a>
               			</div>
               			</div>

               		</div>

               		</div>
               </div>
               <div class="col-md-6">
               		<div class="row">
               		<div class="col-md-12">
               			<div class="panel panel-default">
                           <div class="panel-heading text-center"><h4>เข้าถึงแบบสอบถามผ่านอุปกรณ์</h4></div>
               					<div class="panel-body text-center">
               					<img src='<?php echo base_url('assets/images/qrcode/qrCode.png')?>'  class="img-responsive qrcode" />
               					</div>
               			</div>
               		</div>

               		</div>
               </div>
            </div>
        </div>
    </div>