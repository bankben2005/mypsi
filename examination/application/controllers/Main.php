﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Frontend_controller.php';
class Main extends Frontend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		//var_dump($_SESSION);
		error_reporting(-1);
		ini_set('display_errors', 1);


	}
	public function index()
	{

		$this->signin();

		// if($this->input->post(null,false)){

		// 	$query = $this->db->select('*')
		// 		->from('account')
		// 		->where('code',$this->input->post('code'))
		// 		->where('access_type',$this->input->post('access_type'))
		// 		->where('status',1)
		// 		->get();

		// 	if($query->num_rows() > 0){
		// 		$row = $query->row();
		// 		$queryForm = $this->db->select('id,account_id')
		// 			->from('evaluation_form')
		// 			->where('account_id',$row->id)
		// 			->get();

		// 		if($queryForm->num_rows() <= 0){
		// 			$this->session->set_userdata('account_id',md5($row->id));
					
		// 			//var_dump($row->access_type);exit;
		// 			switch ($row->access_type) {
		// 				case 'student':
		// 					# code...
		// 				redirect(base_url('evaluation/student'));
		// 					break;

		// 				case 'teacher':
		// 				echo "aaaa";
		// 				redirect(base_url('evaluation/teacher'));
		// 				break;
						
		// 				default:
		// 					# code...
		// 					break;
		// 			}

		// 		}else{
		// 			if($row->access_type == 'student'){
		// 				$this->msg->add('นักเรียนได้ทำการประเมินแล้ว','error');
		// 				redirect($this->uri->uri_string());
		// 			}else{
		// 				$this->session->set_userdata('account_id',md5($row->id));
		// 				//echo "aaaa";exit;
		// 				redirect(base_url('evaluation/teacher'));

		// 			}
		// 		}


		// 	}else{
		// 		$this->msg->add('กรุณาตรวจรหัสนักเรียนหรืออาจารย์ให้ถูกต้อง','error');
		// 		redirect($this->uri->uri_string());
		// 	}


		// }

		// $this->template->javascript->add(base_url('assets/js/main.js'));
		// $this->template->content->view('main');
		// $this->template->publish();
	}
	public function signin(){

			//echo "abcd";exit;
			if($this->input->post(null,false)){

				$query = $this->db->select('*')
				->from('account')
				->where('email',$this->input->post('email'))
				->where('password',$this->input->post('password'))
				->where('access_type','admin')
				->get();
				$row = $query->row();
				if($query->num_rows() > 0){
					if($this->session->userdata('account_id')){
						$this->session->unset_userdata('account_id');
					}
					$this->session->set_userdata('administrator_id',md5($row->id));
					//var_dump($_SESSION);exit;
					redirect('administrator');
				}else{

					$this->msg->add('อีเมล์หรือรหัสผ่านไม่ถูกต้อง','error');
					redirect($this->uri->uri_string());
				}

			}
			$this->template->content->view('signin');
			$this->template->publish();

	}
	public function signout(){
		//var_dump($this->session->userdata('account_id'));exit;
		if($this->session->userdata('account_id')){
			$this->session->unset_userdata('account_id');
			if($this->session->userdata('evaluation_form')){
				$this->session->unset_userdata('evaluation_form');
			}
			if($this->session->userdata('evaluation_teacher_form')){
				$this->session->unset_userdata('evaluation_teacher_form');
			}
			redirect(base_url());
		}else if($this->session->userdata('administrator_id')){
			$this->session->unset_userdata('administrator_id');
			redirect(base_url());
		}
	}
		
}
