<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Admin_controller.php';
class Administrator extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		
	}
	public function index(){
		

		$query = $this->db->select('*,examination_form.id as ex_id,examination_department.name as dep_name,examination_form.created as created_form')
		->from('examination_form')
		->join('account','account.code = examination_form.code')
		->join('examination_level','examination_form.examination_level_id = examination_level.id')
		->join('examination_department','examination_form.examination_department_id = examination_department.id')
		->get();


		//echo $this->db->last_query();

		$data = array(
			'examination_form' => $query
		);

		$this->template->content->view('admin/main',$data);
		$this->template->publish();

	}

	
	
	public function student(){

		$query = $this->db->select('*')
			->from('account')
			->where('access_type','student')
			->get();

			//exit;

		$data = array(
			'studentRow' => $query
		);

		$this->template->content->view('admin/Student/student',$data);
		$this->template->publish();
	}
	public function addStudent(){
		if($this->input->post(null,false)){

				$data_insert = array(
					'room' => $this->input->post('room'),
					'title' => $this->input->post('title'),
					'code' => $this->input->post('code'),
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'level' => $this->input->post('level'),
					'number' => $this->input->post('number')
					);
				$this->db->insert('account',$data_insert);
				unset($_POST);
				$this->msg->add('เพิ่มรายชื่อนักเรียนสำเร้จ','success');
				redirect($this->uri->uri_string());


		}

		$this->template->content->view('admin/student_add');
		$this->template->publish();
	}
	public function editStudent($student_id){
		$query = $this->db->select('*')
		->from('account')
		->where('id',$student_id)
		->where('access_type','student')
		->get();

		if($query->num_rows() <= 0){
			redirect(base_url('administrator/student'));
		}else{
			$studentData = $query->row();

			if($this->input->post(null,false)){
				$dataUpdate = array(
					'room' => $this->input->post('room'),
					'code' => $this->input->post('code'),
					'title' => $this->input->post('title'),
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'level' => $this->input->post('level'),
					'number' => $this->input->post('number')
				);

				$this->db->update('account',$dataUpdate,array('id'=>$studentData->id));
				$this->msg->add('อัพเดทข้อมูลนักเรียนเรียบร้อย','success');
				redirect($this->uri->uri_string());

			}

			$data = array();
			$data = array(
				'studentData' => $studentData
			);
			$this->template->content->view('admin/student_edit',$data);
			$this->template->publish();

		}


	}
	public function deleteStudent($student_id){

		$query = $this->db->select('*')
		->from('account')
		->where('id',$student_id)
		->get();

		if($query->num_rows() > 0){
			$this->db->delete('account',array('id'=>$query->row()->id));
			$this->msg->add('ลบบัญชีผู้ใช้นักเรียนเรียบร้อย','success');
			redirect('administrator/student');
		}else{
			redirect('administrator/student');
		}
	}
	public function teacher(){

		$query = $this->db->select('*')
			->from('account')
			->where('access_type','teacher')
			->get();

		$data = array(
			'teacherRow' => $query
		);

		$this->template->content->view('admin/teacher',$data);
		$this->template->publish();
	}
	public function addTeacher(){
		if($this->input->post(null,false)){

				$data_insert = array(
					'title' => $this->input->post('title'),
					'code' => $this->input->post('code'),
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'access_type' => 'teacher'
					);
				$this->db->insert('account',$data_insert);
				unset($_POST);
				$this->msg->add('เพิ่มรายชื่ออาจารย์สำเร็จ','success');
				redirect($this->uri->uri_string());


		}

		$this->template->content->view('admin/teacher_add');
		$this->template->publish();
	}
	public function editTeacher($teacher_id){

		$query = $this->db->select('*')
		->from('account')
		->where('id',$teacher_id)
		->where('access_type','teacher')
		->get();

		if($query->num_rows() <= 0){
			redirect(base_url('administrator/teacher'));
		}else{
			$teacherData = $query->row();


			if($this->input->post(null,false)){

				$dataUpdate = array(
					'code' => $this->input->post('code'),
					'title' => $this->input->post('title'),
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname')
				);

				$this->db->update('account',$dataUpdate,array('id'=>$teacherData->id));
				$this->msg->add('อัพเดทข้อมูลอาจารย์เรียบร้อย','success');
				redirect($this->uri->uri_string());
			}

			$this->template->content->view('admin/teacher_edit',array('teacherData'=>$teacherData));
			$this->template->publish();


		}

	}
	public function deleteTeacher($teacher_id){

		$query = $this->db->select('*')
		->from('account')
		->where('id',$teacher_id)
		->get();

		//var_dump($query->row());exit;
		if($query->num_rows() > 0){
			$this->db->delete('account',array('id'=>$query->row()->id));
			$this->msg->add('ลบบัญชีผู้ใช้อาจารย์','success');
			redirect('administrator/teacher');
		}else{
			redirect('administrator/teacher');
		}
	}
	public function signout(){
		if($this->session->userdata('administrator_id')){
			$this->session->unset_userdata('administrator_id');
			
		}
		redirect(base_url());
	}

	public function getSummaryGrade($evaluation_data){
		$statusTxt = "";
		$arEvaluation  = array();
		$totalPointAverage = $evaluation_data->num_rows()*5;
		foreach ($evaluation_data->result() as $key => $value) {
			# code...
			$query = $this->db->select('*')
				->from('rating')
				->where('id',$value->rating_id)
				->get();
			$row = $query->row();

			array_push($arEvaluation, (int)$row->detail);
		}


		

		$sumArEvaluation = array_sum($arEvaluation);

		$totalPercent = (($sumArEvaluation/$totalPointAverage)*100);


		
		//echo $totalPercent;
		if($totalPercent >= 50){
			//echo "sssssss"
			$statusTxt = "pass";
		}else{
			$statusTxt = "notpass";
		}
		return array('status'=>$statusTxt,'total_percent' => $totalPercent);


	}



	
		
}
