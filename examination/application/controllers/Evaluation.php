<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Evaluation_controller.php';
class Evaluation extends Evaluation_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		//$this->session->unset_userdata('evaluation_form');
		//var_dump($this->student_id);

	}
	public function index()
	{



		$this->template->content->view('student/signin');
		$this->template->publish();

		//$this->load->view('welcome_message');
	}
	public function student(){

		/* Check already form */



		$this->template->javascript->add(base_url('assets/js/student.js'));
		$studentChoice = $this->getStudentChoiceInformation();
		$ChoiceRating = $this->getStudentChoiceRating();
		$dataChoice = array();

		if($this->session->userdata('evaluation_form')){
			$evaluation_form = $this->session->userdata('evaluation_form');

				$arEvaluationSes = array();
				$arEvaluation = array();
				foreach ($evaluation_form as $key => $value) {
					$arEvaluationSes[$key] = $value['evaluation_choice_id'];
				}
				foreach ($studentChoice['student_choice'] as $key => $value) {
					$arEvaluation[$key] = $value['id'];
				}

				$stillLeft = array_diff($arEvaluation, $arEvaluationSes);

				$thisChoiceKey = min(array_keys($stillLeft));
				$choice_number = (count($arEvaluationSes)+1);

				$dataChoice = array(
					'evaluation_choice_id' => $studentChoice['student_choice'][$thisChoiceKey]['id'],
					'evaluation_detail' => $studentChoice['student_choice'][$thisChoiceKey]['detail'],
					'choice_number' => $choice_number,
					'choice_rating' => $ChoiceRating,
				);
		}else{

			
			$dataChoice = array(
				'evaluation_choice_id' => $studentChoice['student_choice'][0]['id'],
				'evaluation_detail' => $studentChoice['student_choice'][0]['detail'],
				'choice_number' => 1,
				'choice_rating' => $ChoiceRating
			);

		}

		
		
		if($this->checkAlreadyFormStudent()){
		$this->template->content->view('evaluation/student_complete');

		}else{
		$this->template->content->view('evaluation/student',$dataChoice);
		}
		$this->template->publish();


	}

	public function studentEvaluation(){
		$evaluation_form = array();
		$evaluation_form = $this->session->userdata('evaluation_form');

		$nextChoiceRequest = $this->input->post('next_choice_request');
		$evaluation_choice_id = $this->input->post('evaluation_choice_id');
		$rating_id = $this->input->post('rating_id');

		
		$totalStudentChoice = $this->getStudentChoiceInformation();
		$student_choice = $totalStudentChoice;
		$totalStudentChoice = $totalStudentChoice['totalStudentChoice'];
		//var_dump($totalStudentChoice);exit;

		if(count($evaluation_form) == $totalStudentChoice){


		}else if(count($evaluation_form) == ($totalStudentChoice-1)){
			
			array_push($evaluation_form, array('evaluation_choice_id'=>$evaluation_choice_id,'rating_id'=>$rating_id));
			
			$this->session->set_userdata('evaluation_form',$evaluation_form);

			$summary_grade = $this->getSummaryGradeStudent();

			$dataInsert = array(
				'account_id' => $this->student_id,
				'created' => date('Y-m-d H:i:s'),
				'updated' => date('Y-m-d H:i:s'),
			);

			$query = $this->db->insert('evaluation_form',$dataInsert);
			$evaluation_form_id = $this->db->insert_id();


			if($query){
				foreach($evaluation_form as $key => $val){
					$dataEvaluationForm = array(
						'evaluation_form_id' => $evaluation_form_id,
						'evaluation_choice_id' => $val['evaluation_choice_id'],
						'rating_id' => $val['rating_id']
					);
					$this->db->insert('evaluation_form_choice',$dataEvaluationForm);

				}
					$this->session->unset_userdata('evaluation_form');
					$view = $this->load->view('evaluation/student_complete',array('summary_grade'=>$summary_grade['summary_grade'],'total_percent'=>$summary_grade['total_percent']),true);
					$json_return  = array(
						'view' => $view
					);

					echo json_encode($json_return);

			}


		}else{

			//$evaluation_form = $this->session->userdata('evaluation_form');
			/* check already push*/




			if($evaluation_choice_id && $rating_id){
				if($evaluation_form){
					array_push($evaluation_form, array('evaluation_choice_id'=>$evaluation_choice_id,'rating_id'=>$rating_id));
				}else{
					$evaluation_form = array();

					array_push($evaluation_form, array('evaluation_choice_id'=>$evaluation_choice_id,'rating_id'=>$rating_id));
				}

				$choice_number = array();
				if($evaluation_form){
					$arEvaluationSes = array();
					$arEvaluation = array();
					foreach ($evaluation_form as $key => $value) {
						$arEvaluationSes[$key] = $value['evaluation_choice_id'];
					}
					foreach ($student_choice['student_choice'] as $key => $value) {
						$arEvaluation[$key] = $value['id'];
					}

					$stillLeft = array_diff($arEvaluation, $arEvaluationSes);

					$thisChoiceKey = min(array_keys($stillLeft));
					$choice_number = (count($arEvaluationSes)+1);

				}else{
					$choice_number = $nextChoiceRequest;
				}

				//echo json_encode(array('nex_choice_request' => $nextChoiceRequest));exit;
				$this->session->set_userdata('evaluation_form',$evaluation_form);
				//$nextChoice = $student_choice[$nextChoiceRequest-1];

				$dataNextChoice = array(
					'evaluation_choice_id' => $student_choice['student_choice'][$thisChoiceKey]['id'],
					'evaluation_detail' => $student_choice['student_choice'][$thisChoiceKey]['detail'],
					'choice_number' => $choice_number,
					'choice_rating' => $this->getStudentChoiceRating(),
				);
				//echo json_encode($dataNextChoice);exit;

				$view  = $this->load->view('evaluation/student_choice',$dataNextChoice,true);

				$json_return = array(
					'view' => $view 
				);

				echo json_encode($json_return);

			}


		}
		//var_dump($_SESSION);


		//var_dump($qTotalChoiceStudent);






	}
	public function getStudentChoiceRating(){
		$qChoiceRating = $this->db->select('*')->from('rating')->where('rating_type','student')->get();
		$ChoiceRating = $qChoiceRating->result_array();
		return $ChoiceRating;


	}
	public function getTeacherChoiceRating(){
		$qChoiceRating = $this->db->select('*')->from('rating')->where('rating_type','teacher')->get();
		$ChoiceRating = $qChoiceRating->result_array();
		return $ChoiceRating;
	}

	public function getStudentChoiceInformation(){
		/* Calculate The last choice */
		$qTotalChoiceStudent = $this->db->select('*')->from('evaluation_choice')->where('evaluation_type','student')->get();
		$student_choice = $qTotalChoiceStudent->result_array();
		$qTotalChoiceStudent = (int)$qTotalChoiceStudent->num_rows();
		return array('totalStudentChoice' => $qTotalChoiceStudent,'student_choice' => $student_choice);


	}
	public function getTeacherChoiceInformation(){
		$qTotalChoiceTeacher = $this->db->select('*')->from('evaluation_choice')->where('evaluation_type','teacher')->get();
		$teacher_choice = $qTotalChoiceTeacher->result_array();
		$qTotalChoiceTeacher = (int)$qTotalChoiceTeacher->num_rows();
		return array('totalTeacherChoice' => $qTotalChoiceTeacher,'teacher_choice' => $teacher_choice);
	}
	
	public function unsetEvaluationFormSession(){
		$this->session->unset_userdata('evaluation_form');
		$this->session->unset_userdata('evaluation_teacher_form');
	}
	public function checkAlreadyFormStudent(){
		$query = $this->db->select('*')
				->from('evaluation_form')
				->where('md5(account_id)',$this->session->userdata('account_id'))
				->get();


		$row = $query->row();
		if(!empty($row)){
			return true;
		}else{
			return false;
		}

	}




	public function teacher(){

		$this->template->javascript->add(base_url('assets/js/modal.js'));

		$student_list = $this->getStudentList();

		$data = array(
			'student_list' => $student_list
		);

		$this->template->content->view('evaluation/teacher/student_list',$data);
		$this->template->publish();



	}
	public function getStudentList(){
		$query = $this->db->select('*')
			->from('account')
			->where('access_type','student')
			->get();

		return $query->result_array();



	}
	public function teacherEvaluationForm($student_id){

		$qStudent = $this->db->select('*')
			->from('account')
			->where('id',$student_id)
			->where('access_type','student')
			->get();
		$rStudent = $qStudent->row();

		if(!empty($rStudent)){

			$studentData = $rStudent;

		}else{
			redirect(base_url('evaluation/teacher'));
		}

		$this->template->javascript->add(base_url('assets/js/teacher.js'));
		$teacherChoice = $this->getTeacherChoiceInformation();
		$ChoiceRating = $this->getTeacherChoiceRating();
		$dataChoice = array();


		//var_dump($teacherChoice);
		

		if($this->session->userdata('evaluation_teacher_form')){
			$evaluation_form = $this->session->userdata('evaluation_teacher_form');

				$arEvaluationSes = array();
				$arEvaluation = array();
				foreach ($evaluation_form as $key => $value) {
					$arEvaluationSes[$key] = $value['evaluation_choice_id'];
				}
				foreach ($teacherChoice['teacher_choice'] as $key => $value) {
					$arEvaluation[$key] = $value['id'];
				}

				$stillLeft = array_diff($arEvaluation, $arEvaluationSes);

				$thisChoiceKey = min(array_keys($stillLeft));
				$choice_number = (count($arEvaluationSes)+1);

				$dataChoice = array(
					'studentData' => $studentData,
					'evaluation_choice_id' => $teacherChoice['teacher_choice'][$thisChoiceKey]['id'],
					'evaluation_detail' => $teacherChoice['teacher_choice'][$thisChoiceKey]['detail'],
					'choice_number' => $choice_number,
					'choice_rating' => $ChoiceRating,
				);
		}else{
			$dataChoice = array(
				'studentData' => $studentData,
				'evaluation_choice_id' => $teacherChoice['teacher_choice'][0]['id'],
				'evaluation_detail' => $teacherChoice['teacher_choice'][0]['detail'],
				'choice_number' => 1,
				'choice_rating' => $ChoiceRating
			);
		}


		$this->template->content->view('evaluation/teacher/teacherEvaluationForm',$dataChoice);
		$this->template->publish();

	}
	public function teacherEvaluation(){
		$evaluation_form = array();
		$evaluation_form = $this->session->userdata('evaluation_teacher_form');

		$nextChoiceRequest = $this->input->post('next_choice_request');
		$evaluation_choice_id = $this->input->post('evaluation_choice_id');
		$rating_id = $this->input->post('rating_id');
		$student_id = $this->input->post('student_id');

		
		$totalTeacherChoice = $this->getTeacherChoiceInformation();
		$teacher_choice = $totalTeacherChoice;
		$totalTeacherChoice = $totalTeacherChoice['totalTeacherChoice'];

		if(count($evaluation_form) == $totalTeacherChoice){


		}else if(count($evaluation_form) == ($totalTeacherChoice-1)){
			
			array_push($evaluation_form, array('evaluation_choice_id'=>$evaluation_choice_id,'rating_id'=>$rating_id));
			
			$this->session->set_userdata('evaluation_teacher_form',$evaluation_form);

			$summary_grade = $this->getSummaryGrade();

			$summary_percent = $summary_grade['total_percent'];

			$dataInsert = array(
				'summarize' => $summary_grade['summary_grade'],
				'related_student_id' => $student_id,
				'account_id' => $this->teacher_id,
				'created' => date('Y-m-d H:i:s'),
				'updated' => date('Y-m-d H:i:s'),
			);

			$query = $this->db->insert('evaluation_form',$dataInsert);
			$evaluation_form_id = $this->db->insert_id();


			if($query){
				foreach($evaluation_form as $key => $val){
					$dataEvaluationForm = array(
						'evaluation_form_id' => $evaluation_form_id,
						'evaluation_choice_id' => $val['evaluation_choice_id'],
						'rating_id' => $val['rating_id']
					);
					$this->db->insert('evaluation_form_choice',$dataEvaluationForm);

				}
					$this->session->unset_userdata('evaluation_teacher_form');
					$view = $this->load->view('evaluation/teacher/teacher_complete',array('summary_grade'=>$summary_grade['summary_grade'],'total_percent'=>$summary_grade['total_percent']),true);
					$json_return  = array(
						'view' => $view
					);

					echo json_encode($json_return);

			}


		}else{

			if($evaluation_choice_id && $rating_id){
				if($evaluation_form){
					array_push($evaluation_form, array('evaluation_choice_id'=>$evaluation_choice_id,'rating_id'=>$rating_id));
				}else{
					$evaluation_form = array();

					array_push($evaluation_form, array('evaluation_choice_id'=>$evaluation_choice_id,'rating_id'=>$rating_id));
				}

				$choice_number = array();
				if($evaluation_form){
					$arEvaluationSes = array();
					$arEvaluation = array();
					foreach ($evaluation_form as $key => $value) {
						$arEvaluationSes[$key] = $value['evaluation_choice_id'];
					}
					foreach ($teacher_choice['teacher_choice'] as $key => $value) {
						$arEvaluation[$key] = $value['id'];
					}

					$stillLeft = array_diff($arEvaluation, $arEvaluationSes);

					$thisChoiceKey = min(array_keys($stillLeft));
					$choice_number = (count($arEvaluationSes)+1);

				}else{
					$choice_number = $nextChoiceRequest;
				}

				//echo json_encode(array('nex_choice_request' => $nextChoiceRequest));exit;
				$this->session->set_userdata('evaluation_teacher_form',$evaluation_form);
				//$nextChoice = $student_choice[$nextChoiceRequest-1];

				$dataNextChoice = array(
					'evaluation_choice_id' => $teacher_choice['teacher_choice'][$thisChoiceKey]['id'],
					'evaluation_detail' => $teacher_choice['teacher_choice'][$thisChoiceKey]['detail'],
					'choice_number' => $choice_number,
					'choice_rating' => $this->getTeacherChoiceRating(),
				);
				//echo json_encode($dataNextChoice);exit;

				$view  = $this->load->view('evaluation/teacher/teacher_choice',$dataNextChoice,true);

				$json_return = array(
					'view' => $view 
				);

				echo json_encode($json_return);

			}
		}



	}
	public function getSummaryGrade(){
		$evaluation_session = $this->session->userdata('evaluation_teacher_form');
		$arEvaluation  = array();
		$totalPointAverage = count($evaluation_session)*5;
		foreach ($evaluation_session as $key => $value) {
			# code...
			$query = $this->db->select('*')
				->from('rating')
				->where('id',$value['rating_id'])
				->get();
			$row = $query->row();

			array_push($arEvaluation, (int)$row->detail);
		}



		$sumArEvaluation = array_sum($arEvaluation);

		$totalPercent = (($sumArEvaluation/$totalPointAverage)*100);

		//echo $totalPercent;
		$grade = "";
		if($totalPercent >= 50){
			$grade =  "pass";
		}else{
			$grade = "notpass";
		}

		return array('summary_grade' => $grade,'total_percent'=>$totalPercent);


	}
	public function getSummaryGradeStudent(){
		$evaluation_session = $this->session->userdata('evaluation_form');
		$arEvaluation  = array();
		$totalPointAverage = count($evaluation_session)*5;
		foreach ($evaluation_session as $key => $value) {
			# code...
			$query = $this->db->select('*')
				->from('rating')
				->where('id',$value['rating_id'])
				->get();
			$row = $query->row();
			$rate_number = 0;
			switch ($row->id) {
				case "1":
					$rate_number = 5;
					break;
				case "2":
					$rate_number = 4;
				break;
				case "3":
					$rate_number = 3;
				break;
				case "4":
					$rate_number = 2;
				break;
				case "5":
					$rate_number = 1;
				break;
				
				default:
					# code...
					break;
			}

			array_push($arEvaluation, (int)$rate_number);
		}

		$sumArEvaluation = array_sum($arEvaluation);

		$totalPercent = (($sumArEvaluation/$totalPointAverage)*100);

		//echo $totalPercent;
		$grade = "";
		if($totalPercent >= 50){
			$grade =  "pass";
		}else{
			$grade = "notpass";
		}
		//var_dump($grade);
		//var_dump($totalPercent);

		return array('summary_grade' => $grade,'total_percent'=>$totalPercent);



	}




}
