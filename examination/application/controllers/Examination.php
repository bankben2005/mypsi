<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Admin_controller.php';
class Examination extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		//var_dump($_SESSION);
		//$this->load->library('upload');


	}

	public function index(){

			

			$query = $this->db->select('*,examination.id as ex_id,examination_department.name as department_name')
			->from('examination')
			->join('examination_department','examination_department.id = examination.examination_department_id')
			->join('examination_level','examination_level.id = examination.examination_level_id')
			->get();


			$data = array(
				'examination' => $query
			);

		    $this->template->content->view('admin/Examination/examination_list',$data);
			$this->template->publish();
	}

	public function examination_add(){

			$this->template->javascript->add(base_url('assets/js/examination_add.js'));

			if($this->input->post(NULL,FALSE)){
				
				$data_insert = array(
					'question' => $this->input->post('question'),
					'examination_level_id' => $this->input->post('examination_level_id'),
					'examination_department_id' => $this->input->post('examination_department_id'),
					'created' => date('Y-m-d H:i:s'),
					'updated' => date('Y-m-d H:i:s')
				);
				//var_dump($_FILES);exit;
				if($this->db->insert('examination',$data_insert)){

					$insert_id = $this->db->insert_id();


					/* Fill answer to  examination_answer */
					foreach ($this->input->post('answer') as $key => $value) {
						# code...
						$data_answer = array(
							'examination_id' => $insert_id,
							'answer' => $value,
							'is_correct' => ($this->input->post('correct_choice') == $key)?1:0
						);
						$this->db->insert('examination_answer',$data_answer);

					}


					if($_FILES['image']['error'] != 4){
						/* Create Folder For article */
		                if(!is_dir('./uploaded/examination/'.$insert_id)){
		                    mkdir('./uploaded/examination/'.$insert_id,0777);
		                }



						$config = array(
						'upload_path' => "./uploaded/examination/".$insert_id,
						'allowed_types' => "gif|jpg|png|jpeg",
						'overwrite' => TRUE,
						'file_name' => md5(strtotime(date('Y-m-d H:i:s'))),
						'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
						'max_height' => "768",
						'max_width' => "1024"
						);

						$this->load->library('upload', $config);
						if($this->upload->do_upload('image')){
							$data = array('upload_data' => $this->upload->data());
							$query_update = $this->db->update('examination',array('image'=>$data['upload_data']['file_name']),array('id'=>$insert_id));
						}else{
							$error = array('error' => $this->upload->display_errors());

							$this->msg->add($error['error'],'error');
							redirect($this->uri->uri_string());
						}

					}

					$this->msg->add('สร้างข้อสอบสำเร็จ','success');
					redirect($this->uri->uri_string());





				}



			}


			/* Get All of  department */
			$query = $this->db->select('*')
			->from('examination_department')
			->get();

			$department = array();

			foreach ($query->result_array() as $key => $value) {
				# code...
				$department[$value['id']] = $value['name'];
			}


			/* Get All of Level*/
			$query = $this->db->select('*')
			->from('examination_level')
			->get();

			$level = array();

			foreach ($query->result_array() as $key => $value) {
				# code...
				$level[$value['id']] = $value['level_txt'];
			}





			$data = array(
				'department' => $department,
				'level' => $level
			);


			$this->template->stylesheet->add(base_url('assets/admin/css/build.css'));

			$this->template->content->view('admin/Examination/examination_add',$data);
			$this->template->publish();

	}
	public function examination_edit($examination_id){


			$this->template->javascript->add(base_url('assets/js/examination_edit.js'));

			$queryExam = $this->db->select('*')
			->from('examination')
			->where('id',$examination_id)
			->get();

			$queryExaminationAnswer = $this->db->select('*')
			->from('examination_answer')
			->where('examination_id',$queryExam->row()->id)
			->get();

			if($queryExam->num_rows() > 0){
				if($this->input->post(NULL,FALSE)){

						//var_dump($this->input->post());
					$data_update =array(
						'question' => $this->input->post('question'),
						'examination_level_id' => $this->input->post('examination_level_id'),
						'examination_department_id' => $this->input->post('examination_department_id'),
						'updated' => date('Y-m-d H:i:s')
					);

					if($this->db->update('examination',$data_update,array('id'=>$examination_id))){

							foreach ($this->input->post('answer') as $key => $value) {
								
								$arUpdateAnswer = array(
									'answer' => $value,
									'is_correct' => ($this->input->post('correct_choice') == $key)?1:0
								);
								$this->db->update('examination_answer',$arUpdateAnswer,array('id'=>$key));
							}

							if($_FILES['image']['error'] != 4){
							/* Create Folder For article */
			                if(!is_dir('./uploaded/examination/'.$examination_id)){
			                    mkdir('./uploaded/examination/'.$examination_id,0777);
			                }



							$config = array(
							'upload_path' => "./uploaded/examination/".$examination_id,
							'allowed_types' => "gif|jpg|png|jpeg",
							'overwrite' => TRUE,
							'file_name' => md5(strtotime(date('Y-m-d H:i:s'))),
							'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
							'max_height' => "768",
							'max_width' => "1024"
							);

							$this->load->library('upload', $config);
							if($this->upload->do_upload('image')){
								$data = array('upload_data' => $this->upload->data());
								$query_update = $this->db->update('examination',array('image'=>$data['upload_data']['file_name']),array('id'=>$examination_id));
							}else{
								$error = array('error' => $this->upload->display_errors());
								//var_dump($error);exit;
								$this->msg->add($error['error'],'error');
								redirect($this->uri->uri_string());

							}

						}


						/* Update answer */



						$this->msg->add('แก้ไขข้อสอบสำเร็จ','success');
						redirect($this->uri->uri_string());


					}





				}

			}else{
				redirect(base_url('examination'));

			}

			/* Get All of  department */
			$query = $this->db->select('*')
			->from('examination_department')
			->get();

			$department = array();

			foreach ($query->result_array() as $key => $value) {
				# code...
				$department[$value['id']] = $value['name'];
			}


			/* Get All of Level*/
			$query = $this->db->select('*')
			->from('examination_level')
			->get();

			$level = array();

			foreach ($query->result_array() as $key => $value) {
				# code...
				$level[$value['id']] = $value['level_txt'];
			}






			$this->template->content->view('admin/Examination/examination_edit',array('examination'=>$queryExam->row(),'department'=>$department,'level'=>$level,'examination_answer'=>$queryExaminationAnswer));
			$this->template->publish();

	}
	public function examination_delete($examination_id){

		$query = $this->db->select('*')
		->from('examination')
		->where('id',$examination_id)
		->get();

		if($query->num_rows() > 0){


			/* Delete answer from examination_answer */
			$queryAnswer = $this->db->select('*')
			->from('examination_answer')
			->where('examination_id',$query->row()->id)
			->get();

			foreach ($queryAnswer->result() as $key => $value) {
				# code...
				$this->db->delete('examination_answer',array('id'=>$value->id));
			}


			/* Delete image and remove folder */
			if($query->row()->image){
				unlink('./uploaded/examination/'.$query->row()->id.'/'.$query->row()->image);
                rmdir('./uploaded/examination/'.$query->row()->id);
			}

			/* Finally Delete examination */

			if($this->db->delete('examination',array('id'=>$query->row()->id))){
				$this->msg->add('ลบข้อสอบสำเร็จ','success');
				redirect(base_url('examination'));
			}




		}

	}

	public function getDepartmentBranch(){

		$arReturn = array();
		$examination_department_id = $this->input->post('examination_department_id');


		$query = $this->db->select('*')
		->from('examination_department_branch')
		->where('examination_department_id',$examination_department_id)
		->get();

		if($query->num_rows() > 0){

			$arReturn['DataReceive'] = array(
				'examination_department_id' => $examination_department_id

			);

			$arReturn['DataReturn'] = array(
				'examination_department_branch' => $query->result_array()
			);

			$arReturn['status'] = TRUE;
		}else{
			$arReturn['status'] = FALSE;
		}



		echo json_encode($arReturn);

	}

}