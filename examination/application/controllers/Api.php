<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
// require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends CI_Controller{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        // $this->methods['user']['limit'] = 100; // 100 requests per hour per user/key
        // $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function getExamination(){

    	if($this->input->post('request')){
            $request = $this->input->post('request');
        }else{
            echo json_encode(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')));
            // $this->set_response(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')),  REST_Controller::HTTP_BAD_REQUEST);
            //exit;
        }

        			$this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);
                    //var_dump($requestCriteria);

                    /* Check Student Code */
                    $student_query = $this->db->select('*')
                    ->from('account')
                    ->where('code',$requestCriteria->StudentCode)
                    ->get();

                    if($student_query->num_rows() > 0){
                    	/* Check Student already ex*/
                    	
                    		$example_return = array();
                            //$example_return['student'] = $student_query->row_array();

                            
                    		$example_return = $this->getReturnExamination($requestCriteria);

                            //var_dump($example_return);
                            echo json_encode(array('output'=>$example_return));
                    		// $this->set_response(array('output'=>$example_return),REST_Controller::HTTP_OK);
                    	

                    }else{
                    	 $arReturn = array(
                                'error' => array(
                                    'title' => 'not-found-exception',
                                    'detail' => 'student not found'
                                )
                            );
                         echo json_encode(array('output'=>array($arReturn)));
                        // $this->set_response(array('output'=>array($arReturn)),REST_Controller::HTTP_BAD_REQUEST);
                    }
                   

    }

    
    private function getReturnExamination($requestCriteria){

    		$queryExam = $this->db->select('*,examination.id as ex_id,examination_department_branch.name as branch_name,examination_department.name as dep_name')
			->from('examination')
			->join('examination_department','examination_department.id = examination.examination_department_id')
            ->join('examination_department_branch','examination_department_branch.id = examination.examination_department_branch_id')
			->join('examination_level','examination_level.id = examination.examination_level_id')
			// ->where('examination.examination_level_id',$requestCriteria->ExaminationLevel)
            ->where('examination.examination_department_id',$requestCriteria->DepartmentId)
            ->where('examination.examination_department_branch_id',$requestCriteria->DepartmentBranchId)
			->get();

            //echo $this->db->last_query();exit;

    		$arData = array();
            

    		//var_dump($queryExam->result_array());
    		foreach ($queryExam->result() as $key => $value) {
    			# code...
    			$answerQuery = $this->db->select('*')
    			->from('examination_answer')
    			->where('examination_id',$value->ex_id)
    			->get();
    			$ar_answer = array();
    			foreach ($answerQuery->result() as $k => $v) {
    				# code...
    				$ar_answer[$k] = array(
    					'answer' => $v->answer,
    					'is_correct' => $v->is_correct
    				);
    			}
    			$arData[$key] = array(
    				'id' => $value->ex_id,
    				'question' => $value->question,
    				'image' => ($value->image)?base_url('uploaded/examination/'.$value->ex_id.'/'.$value->image):"",
    				'department' => $value->dep_name,
                    'department_branch' => $value->branch_name,
    				'level_txt' => $value->level_txt,
    				'answer' => $ar_answer

    			);
    		}
            //array_push($arData, $student_row);

    		return $arData;

    		//echo "<pre>".print_r($arData,true)."</pre>";




    }

    
    public function getDepartment(){

        $arReturn = array();
        $query = $this->db->select('*')
        ->from('examination_department')
        ->get();

        if($query->num_rows() > 0){

            foreach ($query->result() as $key => $value) {
                # code...

                $arBranch = array();
                $queryBranch = $this->db->select('*')
                ->from('examination_department_branch')
                ->where('examination_department_id',$value->id)
                ->get();

                foreach ($queryBranch->result() as $k => $v) {
                    # code...
                    $arBranch[$k] = array(
                        'branch_id' => $v->id,
                        'branch_name' => $v->name,
                        'total_lesson' => (int)$v->total_lesson
                    );
                }


                $arReturn[$key] = array(
                    'department_id' => $value->id,
                    'department_name' => $value->name,
                    // 'total_lesson' => $value->total_lesson,
                    'department_branch' => $arBranch

                );
            }
        }
        echo json_encode(array('output'=>$arReturn));


        // $this->set_response(array('output'=>$arReturn),REST_Controller::HTTP_OK);


    }
    
    public function getDepartmentContentByDepartmentAndLesson(){
        if($this->input->post('request')){
            $request = $this->input->post('request');
        }else{
            $this->set_response(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')),  REST_Controller::HTTP_BAD_REQUEST);
            //exit;
        }
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    $query = $this->db->select('*')
                    ->from('examination_department_content')
                    ->where('examination_department_id',$requestCriteria->DepartmentId)
                    ->where('lesson_level',$requestCriteria->LessonLevel)
                    ->get();

                    if($query->num_rows() > 0){

                            $department_return = $query->row_array();

                            echo json_encode(array('output'=>array($department_return)));
                            // $this->set_response(array('output'=>array($department_return)),REST_Controller::HTTP_OK);
                    }else{

                        $arReturn = array(
                                'error' => array(
                                    'title' => 'not-found-exception',
                                    'detail' => 'department not found'
                                )
                            );
                        echo json_encode(array('output'=>array($arReturn)));
                        // $this->set_response(array('output'=>array($arReturn)),REST_Controller::HTTP_BAD_REQUEST);
                    }
                    // var_dump($requestCriteria);exit;
    }

    public function getDepartmentContentByDepartment(){
        if($this->input->post('request')){
            $request = $this->input->post('request');
        }else{
            echo json_encode(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')));
            // $this->set_response(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')),  REST_Controller::HTTP_BAD_REQUEST);
            //exit;
        }
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    $query = $this->db->select('*,examination_department.name as department_name')
                    ->from('examination_department_content')
                    ->join('examination_department','examination_department.id = examination_department_content.examination_department_id')
                    ->where('examination_department_id',$requestCriteria->DepartmentId)
                    // ->where('lesson_level',$requestCriteria->LessonLevel)
                    ->get();

                    if($query->num_rows() > 0){
                            $department_return = array();
                            foreach ($query->result_array() as $key => $value) {
                                # code...
                                $department_return[$key] = $value;

                                unset($department_return[$key]['name']);
                                unset($department_return[$key]['detail']);
                                unset($department_return[$key]['total_lesson']);
                            }
                            // $department_return = $query->row_array();
                            echo json_encode(array('output'=>$department_return));

                            // $this->set_response(array('output'=>$department_return),REST_Controller::HTTP_OK);
                    }else{

                        $arReturn = array(
                                'error' => array(
                                    'title' => 'not-found-exception',
                                    'detail' => 'department not found'
                                )
                            );
                            echo json_encode(array('output'=>array($arReturn)));
                        // $this->set_response(array('output'=>array($arReturn)),REST_Controller::HTTP_BAD_REQUEST);
                    }

    }

    public function getDepartmentContent(){
        if($this->input->post('request')){
            $request = $this->input->post('request');
        }else{
            echo json_encode(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')));
            // $this->set_response(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')),  REST_Controller::HTTP_BAD_REQUEST);
            //exit;
        }
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    $query = $this->db->select('*,examination_department.name as department_name,examination_department_branch.name as branch_name')
                    ->from('examination_department_content')
                    ->join('examination_department','examination_department_content.examination_department_id = examination_department.id')
                    ->join('examination_department_branch','examination_department_branch.id = examination_department_content.examination_department_branch_id')
                    ->where('examination_department_content.examination_department_branch_id',$requestCriteria->DepartmentBranchId)
                    ->where('examination_department_content.lesson_level',$requestCriteria->DepartmentBranchLesson)
                    ->get();

                    if($query->num_rows() > 0){
                        /* Set new array */
                        $result = $query->row_array();
                        unset($result['created']);
                        unset($result['updated']);
                        unset($result['name']);
                        unset($result['detail']);
                        unset($result['total_lesson']);
                        echo json_encode(array('output'=>array($result)));
                    }else{
                        $arReturn = array(
                                'error' => array(
                                    'title' => 'not-found-exception',
                                    'detail' => 'department not found'
                                )
                            );
                            echo json_encode(array('output'=>array($arReturn)));
                    }

    }
    public function getAllDepartmentContent(){
        if($this->input->post('request')){
            $request = $this->input->post('request');
        }else{
            echo json_encode(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')));
            // $this->set_response(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')),  REST_Controller::HTTP_BAD_REQUEST);
            //exit;
        }
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    $query = $this->db->select('*,examination_department.name as department_name,examination_department_branch.name as branch_name')
                    ->from('examination_department_content')
                    ->join('examination_department_branch','examination_department_content.examination_department_branch_id = examination_department_branch.id')
                    ->join('examination_department','examination_department_content.examination_department_id = examination_department.id')
                    ->where('examination_department_content.examination_department_id',$requestCriteria->DepartmentId)
                    ->get();

                    if($query->num_rows() > 0){
                    $result =  $query->result_array();
                    foreach ($query->result_array() as $key => $value) {
                        # code...
                        unset($result[$key]['id']);
                        unset($result[$key]['created']);
                        unset($result[$key]['updated']);
                        unset($result[$key]['name']);
                        unset($result[$key]['detail']);
                        unset($result[$key]['total_lesson']);
                    }
                    echo json_encode(array('output'=>$result));
                    }else{
                        $arReturn = array(
                                'error' => array(
                                    'title' => 'not-found-exception',
                                    'detail' => 'result not found'
                                )
                            );
                            echo json_encode(array('output'=>$arReturn));
                    }

                    // echo "<pre>".print_r($result,true)."</pre>";


    }



    public function sendExamination(){

        if($this->input->post('request')){
            $request = $this->input->post('request');
        }else{
            echo json_encode(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')));
            // $this->set_response(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')),  REST_Controller::HTTP_BAD_REQUEST);
            //exit;
        }


                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);



                    $queryStudent = $this->db->select('*')
                    ->from('account')
                    ->where('code',$requestCriteria->StudentCode)
                    ->get();

                    if($queryStudent->num_rows() > 0){

                        $data_insert = array(
                            'code' => $requestCriteria->StudentCode,
                            'examination_level_id' => $requestCriteria->ExaminationLevel,
                            'examination_department_id' => $requestCriteria->DepartmentId,
                            'examination_department_branch_id' => $requestCriteria->DepartmentBranchId,
                            'total_correct' => $requestCriteria->TotalCorrect,
                            'total_incorrect' => $requestCriteria->TotalIncorrect,
                            'total_did_not' => $requestCriteria->TotalDidnot,
                            'created' => date('Y-m-d H:i:s')

                        );

                        if($this->db->insert('examination_form',$data_insert)){
                            $arReturn = array(
                                'status'=>true
                            );
                            echo json_encode(array('output'=>array($arReturn)));
                             // $this->set_response(array('output'=>array($arReturn)),REST_Controller::HTTP_OK);

                        }




                    }else{
                            $arReturn = array(
                                'error' => array(
                                    'title' => 'not-found-exception',
                                    'detail' => 'student not found'
                                )
                            );
                            echo json_encode(array('output'=>array($arReturn)));
                        // $this->set_response(array('output'=>array($arReturn)),REST_Controller::HTTP_BAD_REQUEST);

                    }

                    //var_dump($requestCriteria);





    }

    

    public function getStudentData(){
        if($this->input->post('request')){
            $request = $this->input->post('request');
        }else{
            echo json_encode(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')));
            // $this->set_response(array('error'=>array('title'=>'request-not-found','message'=>'request not found, Please send request')),  REST_Controller::HTTP_BAD_REQUEST);
            //exit;
        }


                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    $query = $this->db->select('*')
                    ->from('account')
                    ->where('code',$requestCriteria->StudentCode)
                    ->get();

                    if($query->num_rows() > 0){
                        $account_row = $query->row_array();
                        echo json_encode(array('output'=>array($account_row)));
                         // $this->set_response(array('output'=>array($account_row)),REST_Controller::HTTP_OK);


                    }else{
                         $arReturn = array(
                                'error' => array(
                                    'title' => 'not-found-exception',
                                    'detail' => 'student not found'
                                )
                            );
                         echo json_encode(array('output'=>array($arReturn)));
                        // $this->set_response(array('output'=>array($arReturn)),REST_Controller::HTTP_BAD_REQUEST);
                    }

                    //var_dump($requestCriteria);


    }
    public function getAllExamination(){

            $queryExam = $this->db->select('*,examination.id as ex_id,examination_department_branch.name as branch_name,examination_department.name as dep_name')
            ->from('examination')
            ->join('examination_department','examination_department.id = examination.examination_department_id')
            ->join('examination_department_branch','examination_department_branch.id = examination.examination_department_branch_id')
            ->join('examination_level','examination_level.id = examination.examination_level_id')
            ->get();


            $arData = array();
            

            foreach ($queryExam->result() as $key => $value) {
                # code...
                $answerQuery = $this->db->select('*')
                ->from('examination_answer')
                ->where('examination_id',$value->ex_id)
                ->get();
                $ar_answer = array();
                foreach ($answerQuery->result() as $k => $v) {
                    # code...
                    $ar_answer[$k] = array(
                        'answer' => $v->answer,
                        'is_correct' => $v->is_correct
                    );
                }
                $arData[$key] = array(
                    'id' => $value->ex_id,
                    'question' => $value->question,
                    'image' => ($value->image)?base_url('uploaded/examination/'.$value->ex_id.'/'.$value->image):"",
                    'department' => $value->dep_name,
                    'department_branch' => $value->branch_name,
                    'level_txt' => $value->level_txt,
                    'answer' => $ar_answer

                );
            }
            echo json_encode(array('output'=>$arData));
            // $this->set_response(array('output'=>$arData),REST_Controller::HTTP_OK);


    }
    

}