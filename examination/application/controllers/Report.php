<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Admin_controller.php';
class Report extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		
	}
	public function index(){
		
		$this->student();

	}

	public function student(){

		$this->template->stylesheet->add('assets/admin/css/plugins/morris.css');

		$this->template->javascript->add('assets/admin/js/plugins/morris/raphael.min.js');
		$this->template->javascript->add('assets/admin/js/plugins/morris/morris.min.js');
		$this->template->javascript->add('assets/admin/js/report/student.js');


		$studentEvaluation = $this->db->select('*')
		->from('evaluation_form')
		->join('account','account.id = evaluation_form.account_id')
		->where('account.access_type','student')
		->get();



		$data = array('student_number'=>$studentEvaluation->num_rows());

		$this->template->content->view('report/student',$data);
		$this->template->publish();

	}

	public function teacher(){

		$this->template->stylesheet->add('assets/admin/css/plugins/morris.css');

		$this->template->javascript->add('assets/admin/js/plugins/morris/raphael.min.js');
		$this->template->javascript->add('assets/admin/js/plugins/morris/morris.min.js');
		$this->template->javascript->add('assets/admin/js/report/teacher.js');

		$teacherEvaluation = $this->db->select('*')
		->from('evaluation_form')
		->join('account','account.id = evaluation_form.account_id')
		->where('account.access_type','teacher')
		->get();

		$data = array('teacher_number'=>$teacherEvaluation->num_rows());

		$this->template->content->view('report/teacher',$data);
		$this->template->publish();

	}
	public function studentFetchGraph(){


		$queryFormStudent = $this->db->select('*,evaluation_form.id as eva_id')
		->from('evaluation_form')
		->join('account','evaluation_form.account_id = account.id')
		->where('account.access_type','student')
		->get();

		$numberOfForm = $queryFormStudent->num_rows();

		
		$arData = array();
		$choiceGroup = array();
		foreach ($queryFormStudent->result() as $key => $value) {
			# code...
			$queryFormChoice = $this->db->select('*')
			->from('evaluation_form_choice')
			->where('evaluation_form_id',$value->eva_id)
			->get();
			$arData[$key]['account_id'] = $value->account_id;
			if($queryFormChoice->num_rows() > 0){
				foreach ($queryFormChoice->result() as $k => $v) {
					$evaluatio_choice =$this->db->select('*')
					->from('evaluation_choice')
					->where('id',$v->evaluation_choice_id)->get()->row();

					# code...
					$arData[$key]['choice'][] = array(
						'evaluation_choice_id' => $v->evaluation_choice_id,
						'evaluation_rating' => $this->getRatingById($v->rating_id)
					);
					//array_push($choiceGroup[$k], $this->getRatingById($v->rating_id));
					$choiceGroup[$k]['evaluation_choice_id'] = $v->evaluation_choice_id;
					$choiceGroup[$k]['all_choice'] = array();
					$choiceGroup[$k]['choice_detail'] = $evaluatio_choice->detail;
					$choiceGroup[$k]['percent_score'] = "";
				}
			}
		}
		//var_dump($choiceGroup);
		
		foreach ($arData as $key => $value) {
			# code...
			foreach ($value['choice'] as $k => $v) {
				# code...
				array_push($choiceGroup[$k]['all_choice'], $v['evaluation_rating']);
				

			}
		}

		$FullStudentScore = $numberOfForm*5;
		//echo $FullStudentScore;
		$jsonReturn = array();
		foreach ($choiceGroup as $key => $value) {
			# code...
			$sumChoice = array_sum($value['all_choice']);
			$choiceGroup[$key]['percent_score'] = number_format(($sumChoice/$FullStudentScore)*100,2);
			//$jsonReturn[$key]['evaluation_choice_id'] = $value['evaluation_choice_id'];
			$jsonReturn[$key]['percent_score'] =  number_format(($sumChoice/$FullStudentScore)*100,2);
			$jsonReturn[$key]['choice_number'] = $key+1;
			$jsonReturn[$key]['choice_detail'] = $value['choice_detail'];
		}

		echo json_encode($jsonReturn);

	}
	public function teacherFetchGraph(){

		$queryFormTeacher = $this->db->select('*,evaluation_form.id as eva_id')
		->from('evaluation_form')
		->join('account','evaluation_form.account_id = account.id')
		->where('account.access_type','teacher')
		->get();

		$numberOfForm = $queryFormTeacher->num_rows();

		$arData = array();
		$choiceGroup = array();
		foreach ($queryFormTeacher->result() as $key => $value) {
			# code...
			$queryFormChoice = $this->db->select('*')
			->from('evaluation_form_choice')
			->where('evaluation_form_id',$value->eva_id)
			->get();
			$arData[$key]['account_id'] = $value->account_id;
			if($queryFormChoice->num_rows() > 0){
				foreach ($queryFormChoice->result() as $k => $v) {
					$evaluatio_choice =$this->db->select('*')
					->from('evaluation_choice')
					->where('id',$v->evaluation_choice_id)->get()->row();
					//var_dump($v->rating_id);
					# code...
					$arData[$key]['choice'][] = array(
						'evaluation_choice_id' => $v->evaluation_choice_id,
						'evaluation_rating' => $this->getRatingById($v->rating_id)
					);
					//array_push($choiceGroup[$k], $this->getRatingById($v->rating_id));
					$choiceGroup[$k]['evaluation_choice_id'] = $v->evaluation_choice_id;
					$choiceGroup[$k]['all_choice'] = array();
					$choiceGroup[$k]['choice_detail'] = $evaluatio_choice->detail;
					$choiceGroup[$k]['percent_score'] = "";
				}
			}
		}

		//var_dump($arData);exit;
		//echo "<pre>".print_r($arData,true)."</pre>";exit;

		foreach ($arData as $key => $value) {
			# code...
			foreach ($value['choice'] as $k => $v) {
				# code...
				array_push($choiceGroup[$k]['all_choice'], $v['evaluation_rating']);
				

			}
		}

		//var_dump($choiceGroup);

		$FullTeacherScore = $numberOfForm*5;
		//echo $FullTeacherScore;
		$jsonReturn = array();
		foreach ($choiceGroup as $key => $value) {
			# code...
			$sumChoice = array_sum($value['all_choice']);
			$choiceGroup[$key]['percent_score'] = number_format(($sumChoice/$FullTeacherScore)*100,2);
			//$jsonReturn[$key]['evaluation_choice_id'] = $value['evaluation_choice_id'];
			$jsonReturn[$key]['percent_score'] =  number_format(($sumChoice/$FullTeacherScore)*100,2);
			$jsonReturn[$key]['choice_number'] = $key+1;
			$jsonReturn[$key]['choice_detail'] = $value['choice_detail'];
		}

		echo json_encode($jsonReturn);



	}
	public function studentLevelFetchGraph(){
		$queryStudent = $this->db->select('*,evaluation_form.id as eva_id')
		->from('evaluation_form')
		->join('account','evaluation_form.account_id = account.id')
		->where('account.access_type','student')
		->get();

		$arLevel = array();
		$allScore = array();
		foreach ($queryStudent->result() as $key => $value) {
			# code...
			// var_dump($value);
			// var_dump($this->studentCalculatePercentByEvaluationForm($value->eva_id));
			// $arLevel[$key]['total_percent'] = array();
			// array_push($arLevel[$key]['total_percent'], $this->studentCalculatePercentByEvaluationForm($value->eva_id));
			//$arLevel[$key] => array();
			//array_push($arLevel, array('level'=>$value->lavel));
			$arLevel[$key] = array(
				'level' => $value->level,
				'percent_score' => $this->studentCalculatePercentByEvaluationForm($value->eva_id),

			);


		}

		$arSumlevel = array();
		$arSumlevel['6'] = array();
		$arSumlevel['5'] = array();
		$arSumlevel['4'] = array();
		$arSumlevel['3'] = array();
		$arSumlevel['2'] = array();
		$arSumlevel['1'] = array();
		foreach ($arLevel as $key => $value) {
			# code...
			$arSumlevel[$key] = array();
			switch ($value['level']) {
				case '6':

					array_push($arSumlevel['6'], $value['percent_score']);
					break;
				case '5':
					array_push($arSumlevel['5'], $value['percent_score']);
					break;
				case '4':
					array_push($arSumlevel['4'], $value['percent_score']);
					break;
				case '3':
					array_push($arSumlevel['3'], $value['percent_score']);
					break;
				case '2':
					array_push($arSumlevel['2'], $value['percent_score']);
					break;
				case '1':
					array_push($arSumlevel['1'], $value['percent_score']);
					break;

				
				default:
					# code...
					break;
			}
		}


		$dataPercent = array();
		$count=0;
		foreach ($arSumlevel as $key => $value) {
			//var_dump($key);
			//# code...
			$allPercent = count($value)*100;
			//var_dump($allPercent);
			$dataPercent[$count]['level'] = (string)$key;
			$dataPercent[$count]['sum_percent'] = (!empty($value))?array_sum($value):0;
			$dataPercent[$count]['total_percent'] = (!empty($value))?($dataPercent[$count]['sum_percent']/$allPercent)*100:0;
			$count++;

		}
		//var_dump($dataPercent);
		unset($dataPercent[6]);
		sort($dataPercent);
		
		//var_dump($dataPercent);
		echo json_encode($dataPercent);

		//var_dump($dataPercent);
		//var_dump($queryStudent->result_array());
	}

	private function studentCalculatePercentByEvaluationForm($evaluation_form_id){

			$queryEvaluationForm = $this->db->select('*')
			->from('evaluation_form_choice')
			->join('evaluation_choice','evaluation_choice.id = evaluation_form_choice.evaluation_choice_id')
			->where('evaluation_form_id',$evaluation_form_id)
			->get();
			//echo $this->db->last_query();
			$keepRating = array();
			foreach ($queryEvaluationForm->result() as $key => $value) {
				# code...
				//var_dump($value);
				switch ($value->rating_id) {
					case '1':
						# code...
					array_push($keepRating, (int)5);
						break;
					case '2':
					array_push($keepRating, (int)4);
						break;
					case '3':
					array_push($keepRating, (int)3);
						break;
					case '4':
					array_push($keepRating, (int)2);
						break;
					case '5':
					array_push($keepRating, (int)1);
						break;
					
					default:
						# code...
						break;
				}
			}
			$fullPoint = (count($keepRating))*5;

			$returnPercent = 0;

			$returnPercent = (array_sum($keepRating)/$fullPoint)*100;

			return number_format($returnPercent,2);


	}
	private function getRatingById($rating_id){

		$rating_point = 0;
		$query = $this->db->select('*')
		->from('rating')
		->where('id',$rating_id)
		//->where('rating_type','student')
		->get();

		if($query->num_rows() > 0){
			$row = $query->row();
			switch ($row->id) {
				case '1':
					# code...
				$rating_point = 5;
					break;
				case '2':
				$rating_point = 4;
					break;
				case '3':
				$rating_point = 3;
					break;
				case '4':
				$rating_point = 2;
					break;
				case '5':
				$rating_point = 1;
					break;
				case '6':
				$rating_point = 5;
					break;
				case '7':
				$rating_point = 4;
					break;
				case '8':
				$rating_point = 3;
					break;
				case '9':
				$rating_point = 2;
					break;
				case '10':
				$rating_point = 1;
					break;
				default:
					# code...
					break;
			}

		}
		return $rating_point;

	}

}
