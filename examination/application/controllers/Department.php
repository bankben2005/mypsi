<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Admin_controller.php';
class Department extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		//var_dump($_SESSION);


	}

	public function index(){

		$query = $this->db->select('*')
		->from('examination_department')
		->get();


		$data = array(
			'examination_department' => $query
		);

		$this->template->content->view('admin/Department/department_list',$data);
		$this->template->publish();
	}

	public function department_add(){


			if($this->input->post(NULL,FALSE)){
				//var_dump($this->input->post());
				//exit;

				$data_insert = array(
					'name' => $this->input->post('name'),
					'detail' => $this->input->post('detail'),
					// 'total_lesson' => $this->input->post('total_lesson'),
					'created' => date('Y-m-d H:i:s'),
					'updated' => date('Y-m-d H:i:s')
				);

				$query = $this->db->insert('examination_department',$data_insert);

				if($query){
					$this->msg->add('เพิ่มรายวิชาเรียบร้อย','success');
					redirect($this->uri->uri_string());
				}

			}


			$this->template->stylesheet->add(base_url('assets/admin/css/build.css'));

			$this->template->content->view('admin/Department/department_add');
			$this->template->publish();

	}
	public function department_edit($department_id){


			$query = $this->db->select('*')
			->from('examination_department')
			->where('id',$department_id)
			->get();

			if($query->num_rows() > 0){
				$department_data = $query->row();

				if($this->input->post(NULL,FALSE)){

					//var_dump($this->input->post());
					$data_update = array(
						'name' => $this->input->post('name'),
						'detail' => $this->input->post('detail'),
						// 'total_lesson' => $this->input->post('total_lesson'),
						'updated' => date('Y-m-d H:i:s')
					);

					if($this->db->update('examination_department',$data_update,array('id'=>$department_id))){
						$this->msg->add('แก้ไขรายวิชาสำเร็จ','success');
						redirect($this->uri->uri_string());

					}


				}
			}else{

				redirect(base_url('department'));
			}

			


			$this->template->content->view('admin/Department/department_edit',array('department'=>$department_data));
			$this->template->publish();

	}
	public function department_delete($department_id){

			$query = $this->db->select('*')
			->from('examination_department')
			->where('id',$department_id)
			->get();

			if($query->num_rows() > 0){
				$department_data = $query->row();

				if($this->db->delete('examination_department',array('id'=>$department_id))){
					$this->msg->add('ลบรายวิชาสำเร็จ','success');
					redirect('department');
				}


			}else{

				redirect(base_url('department'));
			}



	}

	public function DepartmentBranch(){

		$query = $this->db->select('*,examination_department.name as department_name,examination_department_branch.id as branch_id,examination_department_branch.name as department_branch_name')
		->from('examination_department_branch')
		->join('examination_department','examination_department.id = examination_department_branch.examination_department_id')
		->get();

		//var_dump($query->result());exit;

		$data = array(
			'department_branch' => $query
		);

		$this->template->content->view('admin/Department/department_branch_list',$data);
		$this->template->publish();
	}
	public function DepartmentBranchAdd(){


		if($this->input->post(NULL,FALSE)){
			$data_insert = array(
				'examination_department_id' => $this->input->post('examination_department_id'),
				'total_lesson' => $this->input->post('total_lesson'),
				'name' => $this->input->post('name')
			);

			if($this->db->insert('examination_department_branch',$data_insert)){

				$this->msg->add('เพิ่มแขนงวิชาสำเร็จ','success');
				redirect($this->uri->uri_string());

			}


		}

		/* get Examination all*/
			$arDepartment = array();
			$queryDepartment = $this->db->select('*')
			->from('examination_department')
			->get();
			foreach ($queryDepartment->result() as $key => $value) {
				# code...
				//var_dump($value);
				$arDepartment[$value->id] = $value->name;
			}



		$data = array(
			'department' => $arDepartment
		);








		$this->template->content->view('admin/Department/department_branch_add',$data);
		$this->template->publish();

	}
	public function DepartmentBranchEdit($department_branch_id){

		$query = $this->db->select('*')
		->from('examination_department_branch')
		->where('id',$department_branch_id)
		->get();

		if($query->num_rows() > 0){


			if($this->input->post(NULL,FALSE)){
				$data_update = array(
					'examination_department_id' => $this->input->post('examination_department_id'),
					'total_lesson' => $this->input->post('total_lesson'),
					'name' => $this->input->post('name')
				);

				if($this->db->update('examination_department_branch',$data_update,array('id'=>$department_branch_id))){
						$this->msg->add('แก้ไขแขนงวิชาสำเร็จ','success');
						redirect($this->uri->uri_string());

				}

			}

			$branch_data = $query->row();

			/* get Examination all*/
			$arDepartment = array();
			$queryDepartment = $this->db->select('*')
			->from('examination_department')
			->get();
			foreach ($queryDepartment->result() as $key => $value) {
				# code...
				//var_dump($value);
				$arDepartment[$value->id] = $value->name;
			}


			$data = array(
				'branch_data' => $branch_data,
				'department' => $arDepartment
			);

			$this->template->content->view('admin/Department/department_branch_edit',$data);
			$this->template->publish();

		}else{
			redirect(base_url('department/DepartmentBranch'));
		}


		
	}

	public function DepartmentBranchDelete($department_branch_id){

		$query = $this->db->select('*')
		->from('examination_department_branch')
		->where('id',$department_branch_id)
		->get();

		if($query->num_rows() > 0){

			if($this->db->delete('examination_department_branch',array('id'=>$department_branch_id))){
				$this->msg->add('ลบแขนงวิชาสำเร็จ','success');
				redirect(base_url('department/DepartmentBranch'));
			}

		}else{

			redirect(base_url('department/DepartmentBranch'));
		}
	}

	public function DepartmentContent(){
		$query = $this->db->select('*,examination_department_content.id as content_id,examination_department.name as department_name,examination_department_branch.name as branch_name')
		->from('examination_department_content')
		->join('examination_department','examination_department.id = examination_department_content.examination_department_id')
		->join('examination_department_branch','examination_department_branch.id = examination_department_content.examination_department_branch_id')
		->get();

		//echo "<pre>".print_r($query->result_array(),true)."</pre>";exit;


		$data = array(
			'department_content' => $query
		);

		$this->template->content->view('admin/Department/department_content_list',$data);
		$this->template->publish();

	}
	public function DepartmentContentAdd(){

			$this->template->javascript->add(base_url('assets/js/department_content_add.js'));

			if($this->input->post(NULL,FALSE)){

				// echo "<pre>".print_r($this->input->post(),true)."</pre>";exit;

				$data_insert = array(
					'examination_department_id' => $this->input->post('examination_department_id'),
					'examination_department_branch_id' => $this->input->post('examination_department_branch_id'),
					'content' => $this->input->post('content'),
					'lesson_level' => $this->input->post('lesson_level'),
					'vdo_url' => $this->input->post('vdo_url'),
					'created' => date('Y-m-d H:i:s'),
					'updated' => date('Y-m-d H:i:s')
				);

				$query = $this->db->insert('examination_department_content',$data_insert);

				if($query){
					$this->msg->add('เพิ่มเนื้อหาวิชาเรียบร้อย','success');
					redirect($this->uri->uri_string());
				}

			}

			$departmentQuery = $this->db->select('*')
			->from('examination_department')
			->get();
			$department = array();

			foreach ($departmentQuery->result() as $key => $value) {
				# code...
				$department[$value->id] = $value->name;
			}

			$data = array(
				'department' => $department
			);
			// echo "<pre>".print_r($data,true)."</pre>";exit;			


			$this->template->stylesheet->add(base_url('assets/admin/css/build.css'));

			$this->template->content->view('admin/Department/department_content_add',$data);
			$this->template->publish();

	}

	public function DepartmentContentEdit($department_content_id){
		$this->template->javascript->add(base_url('assets/js/department_content_edit.js'));
		$query = $this->db->select('*')
		->from('examination_department_content')
		->where('id',$department_content_id)
		->get();

		if($query->num_rows() > 0){


			if($this->input->post(NULL,FALSE)){
				$data_update = array(
					'examination_department_id' => $this->input->post('examination_department_id'),
					'examination_department_branch_id' => $this->input->post('examination_department_branch_id'),
					'content' => $this->input->post('content'),
					'lesson_level' => $this->input->post('lesson_level'),
					'vdo_url' => $this->input->post('vdo_url'),
					'updated' => date('Y-m-d H:i:s')
				);

				if($this->db->update('examination_department_content',$data_update,array('id'=>$department_content_id))){
						$this->msg->add('แก้ไขเนื้อหาวิชาสำเร็จ','success');
						redirect($this->uri->uri_string());

				}

			}

			$departmentQuery = $this->db->select('*')
			->from('examination_department')
			->get();
			$department = array();

			foreach ($departmentQuery->result() as $key => $value) {
				# code...
				$department[$value->id] = $value->name;
			}

			$data = array(
				'department' => $department,
				'department_content_data' => $query->row()
			);

			$this->template->content->view('admin/Department/department_content_edit',$data);
			$this->template->publish();

		}else{
			redirect(base_url('department/DepartmentContent'));
		}
	}
	public function DepartmentContentDelete($department_content_id){
		$query = $this->db->select('*')
		->from('examination_department_content')
		->where('id',$department_content_id)
		->get();

		if($query->num_rows() > 0){

			if($this->db->delete('examination_department_content',array('id'=>$department_content_id))){
				$this->msg->add('ลบเนื้อหาวิชาสำเร็จ','success');
				redirect(base_url('department/DepartmentContent'));
			}

		}else{

			redirect(base_url('department/DepartmentContent'));
		}

	}
	public function ajaxGetTotalLessonByDepartmentBranch(){
		$arData = array();
		$department_branch_id = $this->input->post('department_branch_id');
		// echo $department_id;exit;
		$query =  $this->db->select('id,total_lesson')
		->from('examination_department_branch')
		->where('id',$department_branch_id)
		->get();

		if($query->num_rows() > 0){
			$row = $query->row();
			$arData = array(
				'status' => true,
				'total_lesson' => $row->total_lesson
			);
		}else{
			$arData = array(
				'status' => false,
				'total_lesson' => ''
			);
		}

		echo json_encode($arData);	

	}

	public function ajaxGetDepartmentBranchByDepartment(){
		$arData = array();
		$department_id = $this->input->post('department_id');

		$query = $this->db->select('*')
		->from('examination_department_branch')
		->where('examination_department_id',$department_id)
		->get();

		if($query->num_rows() > 0){
			$arDepartmentBranch = array();

			foreach ($query->result() as $key => $value) {
				# code...
				$arDepartmentBranch[$value->id] = $value->name;
			}
			$arData['status'] = true;
			$arData['department_branch'] = $arDepartmentBranch;
		}else{
			$arData['status'] = false;
		}

		echo json_encode($arData);



	}

}