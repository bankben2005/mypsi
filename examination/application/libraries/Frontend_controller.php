<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		//error_reporting(E_ALL);
		//ini_set('display_errors', 1);
		$this->load->helper(array('message'));
		$this->load->library(array('Msg'));

		//$this->checkAlreadySignin();

		$this->setDefaultJS();
		$this->setDefaultCSS();

	}


	private function setDefaultJS(){
		$this->template->stylesheet->add(base_url('assets/css/bootstrap.min.css'));
		$this->template->stylesheet->add(base_url('assets/css/logo-nav.css'));
		$this->template->stylesheet->add(base_url('assets/css/style.css'));

	}
	private function setDefaultCSS(){
		$this->template->javascript->add(base_url('assets/js/jquery.js'));
		$this->template->javascript->add(base_url('assets/js/bootstrap.min.js'));

	}

	private function checkStudentUnderEvaluation(){

	}
	private function checkAlreadySignin(){
		if($this->session->userdata('account_id')){
			$query = $this->db->select('*')
				->from('account')
				->where('md5(id)',$this->session->userdata('account_id'))
				->get();

			if($query->num_rows() > 0){
				$row = $query->row();
				switch ($row->access_type) {
					case 'student':
						# code...
						redirect(base_url('evaluation/student'));
					break;
					case 'teacher':
						# code...
						redirect(base_url('evaluation/teacher'));

					break;
					default:
						# code...
						break;
				}


			}


		}else if($this->session->userdata('administrator_id')){
			redirect(base_url('administrator'));
		}

	}


}