
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

	public  $admin_data = array();

	function __construct(){
		parent::__construct();
		$this->template->set_template('template_admin');
		$this->load->helper(array('message'));
		$this->load->library(array('msg'));

		$this->checkAlreadySignin();

		$this->setDefaultJS();
		$this->setDefaultCSS();

	}


	private function setDefaultJS(){
		$this->template->stylesheet->add(base_url('assets/admin/css/bootstrap.min.css'));
		$this->template->stylesheet->add(base_url('assets/admin/css/sb-admin.css'));
		$this->template->stylesheet->add(base_url('assets/admin/font-awesome/css/font-awesome.min.css'));

	}
	private function setDefaultCSS(){
		$this->template->javascript->add(base_url('assets/admin/js/jquery.js'));
		$this->template->javascript->add(base_url('assets/admin/js/bootstrap.min.js'));
		$this->template->javascript->add('https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js');
		$this->template->javascript->add('https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js');

	}

	private function checkAlreadySignin(){
		
		if(!$this->session->userdata('administrator_id')){
			redirect(base_url());
		}else{
			$query = $this->db->select('*')
				->from('account')
				->where('md5(id)',$this->session->userdata('administrator'))
				->get();
			$row = $query->row_array();

			$this->admin_data = $row;

		}
	}


}