
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluation_controller extends CI_Controller {

	public $student_id;
	public $teacher_id;

	function __construct(){
		parent::__construct();

		$this->load->helper(array('message'));
		$this->load->library(array('msg'));

		$this->checkSignin();
		$this->setDefaultJS();
		$this->setDefaultCSS();

	}


	private function setDefaultJS(){
		$this->template->stylesheet->add(base_url('assets/css/bootstrap.min.css'));
		$this->template->stylesheet->add(base_url('assets/css/logo-nav.css'));
		$this->template->stylesheet->add(base_url('assets/css/style.css'));
		$this->template->stylesheet->add(base_url('assets/admin/font-awesome/css/font-awesome.min.css'));

	}
	private function setDefaultCSS(){
		$this->template->javascript->add(base_url('assets/js/jquery.js'));
		$this->template->javascript->add(base_url('assets/js/bootstrap.min.js'));
				$this->template->javascript->add('https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js');
		$this->template->javascript->add('https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js');
	}
	private function checkSignin(){
		
		if($this->session->userdata('account_id')){
			$query = $this->db->select('*')
				->from('account')
				->where('md5(id)',$this->session->userdata('account_id'))
				->get();

			$row = $query->row();
			switch ($row->access_type) {
				case 'student':
					$this->student_id = $row->id;
					break;
				case 'teacher':
					$this->teacher_id = $row->id;
					break;
				default:
					# code...
					break;
			}
		}else{
			redirect(base_url());
		}

	}
	private function checkUnderEvaluation(){


	}


}