$(document).ready(function(){


		var examination_department_id = $("#examination_department_id").val();
		var examination_department_branch_id = $("#examination_department_branch_id").val();




		$.ajax({
            method: "POST",
            url: base_url + "/examination/getDepartmentBranch",
            data: {
                examination_department_id: examination_department_id,
            },
            dataType:'json',
          success:function(data) {
          	if(data.status){
          		 $("#examination_department_branch_id").html('');
          		  $.each(data.DataReturn.examination_department_branch, function(index, item) { // Iterates through a collection
          		  	//console.log(item);
			        $("#examination_department_branch_id").append( // Append an object to the inside of the select box
			            $("<option></option>") // Yes you can do this.
			                .text(item.name)
			                .val(item.id)
			        );
			    });

          	}else{
          		$("#examination_department_branch_id").html('');
          	}	
          	
          },
          error: function (request, status, error) {
                console.log(request.responseText);
        	}       
        });

		$("#examination_department_id").change(function(){
				var examination_department_id = $(this).val();

				$.ajax({
		            method: "POST",
		            url: base_url + "/examination/getDepartmentBranch",
		            data: {
		                examination_department_id: examination_department_id,
		            },
		            dataType:'json',
		          success:function(data) {
		          	if(data.status){
		          		$("#examination_department_branch_id").html('');
		          		  $.each(data.DataReturn.examination_department_branch, function(index, item) { // Iterates through a collection
		          		  	//console.log(item);
					        $("#examination_department_branch_id").append( // Append an object to the inside of the select box
					            $("<option></option>") // Yes you can do this.
					                .text(item.name)
					                .val(item.id)
					        );
					    });

		          	}else{
		          		$("#examination_department_branch_id").html('');
		          	}
		          	
		          },
		          error: function (request, status, error) {
		                console.log(request.responseText);
		        	}       
		        });

		});




});