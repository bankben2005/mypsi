        function validate_number(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode( key );
          var regex = /[0-9]|\./;
          if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
          }
        }

        $(document).ready(function(){
            var base_url = window.base_url;
            var department_id = $("select[name='examination_department_id']").val();
            // console.log('base_url = '+base_url);
            getDepartmentBranchByDepartment(base_url,department_id);
        });

        function getDepartmentBranchByDepartment(base_url,department_id){
                        $.ajax({
                          type: "POST",
                          url: base_url+"department/ajaxGetDepartmentBranchByDepartment",
                          async:true,
                          dataType:'json',
                          data:{'department_id':department_id},
                          beforeSend: function(){
                                $("#box-branch").hide();
                          },
                          success: function(result){
                            // console.log(result);
                            var options = $("#examination_department_branch_id");
                            options.html('');
                            if(result.status){
                              $.each( result.department_branch, function( key, value ) {
                                options.append($("<option />").val(key).text(value));
                              });
                            }

                            var department_branch = $('#department_branch_val').val();
                            if($("#examination_department_branch_id option[value='"+department_branch+"']").length > 0){
                              options.val(department_branch);
                            }

                            $("#box-branch").fadeIn('fast');

                            // console.log('base_url = '+base_url);
                            // console.log(options.val()); return false;

                            getTotalLessonByDepartmentBranch(base_url,options.val());
                                
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                            
                        });

        }

        function getTotalLessonByDepartmentBranch(base_url,department_branch_id){
                        $.ajax({
                          type: "POST",
                          url: base_url+"department/ajaxGetTotalLessonByDepartmentBranch",
                          async:true,
                          dataType:'json',
                          data:{'department_branch_id':department_branch_id},
                          beforeSend: function(){
                                $("#box-level").hide();
                          },
                          success: function(result){
                            if(result.status){
                                var total_lesson = parseInt(result.total_lesson);
                                var options = $("#lesson_level");
                                options.html('');
                                for (var i = 1; i <= total_lesson; i++) {
                                    options.append($("<option />").val(i).text(i));
                                }

                                var lesson_val = $("#lesson_level_val").val();
                                if($("#lesson_level option[value='"+lesson_val+"']").length > 0){
                                  options.val(lesson_val);
                                }else{
                                  options.val(1);
                                }

                                $("#box-level").fadeIn('fast');
                            }
                                
                          },
                            error: function (request, status, error) {
                                    console.log(request.responseText);
                            }
                            
                        });

        }