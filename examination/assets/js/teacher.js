
function nextChoice(){

	var rating_id = $("input[name='choice_rating']:checked").val();

	var evaluation_choice_id = $("input[name='evaluation_choice_id']").val();

	var choice_number = $("input[name='choice_number']").val();
	var next_choice_request = (parseInt(choice_number)+1);

  var student_id = $("input[name='student_id']").val();
	//console.log(next_choice_request);


	if(!rating_id){
		alert('ต้องทำการเลือก เพื่อไปยังข้อถัดไป');
	}else{

		$.ajax({
            method: "POST",
            url: base_url + "/evaluation/teacherEvaluation",
            data: {
                evaluation_choice_id: evaluation_choice_id,
                rating_id: rating_id,
                next_choice_request: next_choice_request,
                student_id: student_id,
            },
            dataType:'json',
          success:function(data) {
          	console.log(data);
          	$("#evaluation_choice").html('');
          	$("#evaluation_choice").html(data.view);
          },
          error: function (request, status, error) {
                console.log(request.responseText);
        }       
        });



	}

	
	


}
