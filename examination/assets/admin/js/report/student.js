$(document).ready(function(){

   var chart = Morris.Bar({
        element: 'all-student-chart',
        data: [0, 0],
        xkey: 'choice_number',
        ykeys: ['percent_score'],
        labels: ['จำนวนเปอเซนต์'],
        postUnits:'%',
        goals: [1.0, 100.0],
        resize: true,
        hoverCallback: function(index, options, content) {
        	console.log(options)
        return("<div class='morris-hover-row-label'>"+options.data[index]['choice_number']+". "+options.data[index].choice_detail+"</div><div class='morris-hover-point' style='color: #0b62a4'>คิดเป็น "+options.data[index].percent_score+" "+options.postUnits+"</div");
    	},

    });

    // Fire off an AJAX request to load the data
  $.ajax({
      type: "GET",
      dataType: 'json',
      url: base_url+"report/studentFetchGraph", // This is the URL to the API
      data: { days: 7 } // Passing a parameter to the API to specify number of days
    })
    .done(function( data ) {
    	//console.log(data);
      // When the response to the AJAX request comes back render the chart with new data
      $("#all-student-chart").removeClass("loading");
      chart.setData(data);
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      alert( "error occured" );
    });


    var student_level_chart = Morris.Bar({
      element: 'student-level-chart',
      data:[0,0],
      xkey:'level',
      ykeys:['total_percent'],
      labels:['จำนวนเปอเซนต์'],
      postUnits: '%',
      goals:[1.0,100.0],
      resize:true,
      hoverCallback:function(index,options,content){

        var txtReturn = "";
        txtReturn = "<div class='morris-hover-row-label'>";
        txtReturn += "ชั้นมัธยมศึกษาปีที่ "+options.data[index]['level']+"";
        txtReturn += "</div>";
        txtReturn += "<div class='morris-hover-point' style='color: #0b62a4'>";
        txtReturn += "พฤติกรรมจิตสาธารณะคิดเป็น "+options.data[index].total_percent+" "+options.postUnits+"";
        txtReturn += "</div>";
        return (txtReturn);

      },
    });

    $.ajax({
      type:"GET",
      dataType:'json',
      url:base_url+"report/studentLevelFetchGraph",

    })
    .done(function(data){
      student_level_chart.setData(data);
      $("#student-level-chart").removeClass("loading");

    }).fail(function(xhr, err){
      //alert( "error occured" );
      console.log(xhr.responseText);
    });

	

});