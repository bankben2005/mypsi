$(document).ready(function(){
    var student_level_chart = Morris.Bar({
      element: 'all-teacher-chart',
      data:[0,0],
      xkey:'choice_number',
      ykeys:['percent_score'],
      labels:['จำนวนเปอเซนต์'],
      postUnits: '%',
      goals:[1.0,100.0],
      resize:true,
      hoverCallback:function(index,options,content){

        var txtReturn = "";
        txtReturn = "<div class='morris-hover-row-label'>";
        txtReturn += options.data[index]['choice_number']+". "+options.data[index].choice_detail;
        txtReturn += "</div>";
        txtReturn += "<div class='morris-hover-point' style='color: #0b62a4'>";
        txtReturn += "คิดเป็น "+options.data[index].percent_score+" "+options.postUnits+"";
        txtReturn += "</div>";
        return (txtReturn);

      },
    });

    $.ajax({
      type:"GET",
      dataType:'json',
      url:base_url+"report/teacherFetchGraph",

    })
    .done(function(data){
    	console.log(data);
    $("#all-teacher-chart").removeClass("loading");
      student_level_chart.setData(data);
      

    }).fail(function(xhr, err){
      //alert( "error occured" );
      console.log(xhr.responseText);
    });

});