<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_campaigninsuranceregister extends DataMapper {

    //put your code here
    var $table = 'CampaignInsuranceRegister';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'provinces' => array(
             'class' => 'M_provinces',
               'other_field' => 'campaigninsuranceregister',
               'join_other_as' => 'Provinces',
               'join_table' => 'Provinces'
           ),
           'amphurs'=>array(
              'class'=>'M_amphurs',
              'other_field'=>'campaigninsuranceregister',
              'join_other_as'=>'Amphurs',
              'join_table'=>'Amphurs'
           ),
           'districts'=>array(
                'class'=>'M_districts',
                'other_field'=>'campaigninsuranceregister',
                'join_other_as'=>'Districts',
                'join_table'=>'Districts'
           )
   );
    
//     var $has_many = array(
//        'gshop_transaction' => array(
//            'class' => 'm_gshop_transaction',
//            'other_field' => 'gshop_customer',
//            'join_self_as' => 'customer',
//            'join_other_as' => 'gshop_customer',
//            'join_table' => 'gshop_customer')
//   );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

}