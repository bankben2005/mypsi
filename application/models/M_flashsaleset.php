<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_flashsaleset extends DataMapper {

    //put your code here
    var $table = 'FlashSaleSet';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
     // var $has_one = array(
     //         'questionnairecategory' => array(
     //           'class' => 'M_questionnairecategory',
     //             'other_field' => 'questionnaire',
     //             'join_other_as' => 'QuestionnaireCategory',
     //             'join_table' => 'QuestionnaireCategory'
     //         ),
     //         'questionnaireset' => array(
     //            'class' => 'M_questionnaireset',
     //            'other_field' => 'questionnaire',
     //            'join_other_as' => 'QuestionnaireSet',
     //            'join_table' => 'QuestionnaireSet'
     //         )
     // );
    
    var $has_many = array(
       'flashsaletransaction' => array(
           'class' => 'M_flashsaletransaction',
           'other_field' => 'FlashSaleSet',
           'join_self_as' => 'FlashSaleSet',
           'join_other_as' => 'FlashSaleSet',
           'join_table' => 'FlashSaleSetTransaction'
         ),
       'flashsalesetproduct' => array(
           'class' => 'M_flashsalesetproduct',
           'other_field' => 'flashsaleset',
           'join_self_as' => 'FlashSaleSet',
           'join_other_as' => 'FlashSaleSet',
           'join_table' => 'FlashSaleSetProduct'
         ),
    );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}