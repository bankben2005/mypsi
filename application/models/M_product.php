<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_product extends DataMapper {

    //put your code here
    var $table = 'Product';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'warranty' => array(
             'class' => 'M_warranty',
               'other_field' => 'product',
               'join_other_as' => 'Warranty',
               'join_table' => 'warranty'
           )
   );
    
    var $has_many = array(
       'productpart' => array(
           'class' => 'M_productpart',
           'other_field' => 'Product',
           'join_self_as' => 'Product',
           'join_other_as' => 'Product',
           'join_table' => 'ProductPart')
  );
    


}