<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_flashsaletransactionproduct extends DataMapper {

    //put your code here
    var $table = 'FlashSaleTransactionProduct';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
     var $has_one = array(
             'flashsaletransaction' => array(
               'class' => 'M_flashsaletransaction',
                 'other_field' => 'flashsaletransactionproduct',
                 'join_other_as' => 'FlashSaleTransaction',
                 'join_table' => 'FlashSaleTransaction'
             ),
             'flashsalesetproduct' => array(
                'class' => 'M_flashsalesetproduct',
                'other_field' => 'flashsaletransactionproduct',
                'join_other_as' => 'FlashSaleSetProduct',
                'join_table' => 'FlashSaleSetProduct'
             )
     );
    
  //   var $has_many = array(
  //      'productpart' => array(
  //          'class' => 'M_productpart',
  //          'other_field' => 'Product',
  //          'join_self_as' => 'Product',
  //          'join_other_as' => 'Product',
  //          'join_table' => 'ProductPart')a
  // );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}