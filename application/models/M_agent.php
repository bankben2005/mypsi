<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_agent extends DataMapper {

    //put your code here
    var $table = 'agent';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
//    var $has_one = array(
//            'gshop_shop' => array(
//              'class' => 'm_gshop_shop',
//                'other_field' => 'gshop_customer',
//                'join_other_as' => 'shop',
//                'join_table' => 'gshop_shop'
//            )
//    );
    
//     var $has_many = array(
//        'gshop_transaction' => array(
//            'class' => 'm_gshop_transaction',
//            'other_field' => 'gshop_customer',
//            'join_self_as' => 'customer',
//            'join_other_as' => 'gshop_customer',
//            'join_table' => 'gshop_customer')
//   );
    

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getAgentCode(){
        return $this->AgentCode;
    }
    public function setAgentCode($AgentCode){
        $this->AgentCode = $AgentCode;
    }

    public function getAgentName(){
        return $this->AgentName;
    }
    public function setAgentName($AgentName){
        $this->AgentName = $AgentName;
    }
    public function getAgentSurName(){
        return $this->AgentSurName;
    }
    public function setAgentSurName($AgentSurName){
        $this->AgentSurName = $AgentSurName;
    }
    public function getBirthDay(){
        return $this->BirthDay;
    }
    public function setBirthDay($BirthDay){
        $this->BirthDay = $BirthDay;
    }
    public function getTelephone(){
        return $this->Telephone;
    }
    public function setTelephone($Telephone){
        $this->Telephone = $Telephone;
    }
    public function getEmailAddress(){
        return $this->emailaddress;
    }
    public function setEmailAddress($emailaddress){
        $this->emailaddress = $emailaddress;
    }
    public function getTradeName(){
        return $this->TradeName;
    }
    public function setTradeName($TradeName){
        $this->TradeName = $TradeName;
    }
    public function getRegisterNo(){
        return $this->RegisterNo;
    }
    public function setRegisterNo($RegisterNo){
        $this->RegisterNo = $RegisterNo;
    }
    public function getBranch(){
        return $this->Branch;
    }
    public function setBranch($Branch){
        $this->Branch = $Branch;
    }
    public function getStatus(){
        return $this->status;
    }
    public function setStatus($status){
        $this->status = $status;
    }
    public function getSex(){
        return $this->sex;
    }
    public function setSex($sex){
        $this->sex = $sex;
    }
    public function getRegisterDate(){
        return $this->RegisterDate;
    }
    public function setRegisterDate($RegisterDate){
        $this->RegisterDate = $RegisterDate;
    }
    public function getBranchCode(){
        return $this->BranchCode;
    }
    public function setBranchCode($BranchCode){
        $this->BranchCode = $BranchCode;
    }
    public function getPriceId(){
        return $this->PriceId;
    }
    public function setPriceId($PriceId){
        $this->PriceId = $PriceId;
    }
    public function getAgentType(){
        return $this->AgentType;
    }
    public function setAgentType($AgentType){
        $this->AgentType  = $AgentType;
    }
    public function getCardNo(){
        return $this->CardNo;
    }
    public function setCardNo($CardNo){
        $this->CardNo = $CardNo;
    }
    public function getPointUsed(){
        return $this->PointUsed;
    }
    public function setPointUsed($PointUsed){
        $this->PointUsed = $PointUsed;
    }
    public function getPointSum(){
        return $this->PointSum;
    }
    public function setPointSum($PointSum){
        $this->PointSum = $PointSum;
    }
    public function getCreditAmt(){
        return $this->CreditAmt;
    }
    public function setCreditAmt($CreditAmt){
        $this->CreditAmt = $CreditAmt;
    }
    public function getCreditRemain(){
        return $this->CreditRemain;
    }
    public function setCreditRemain($CreditRemain){
        $this->CreditRemain = $CreditRemain;
    }
    public function getCust_no(){
        return $this->Cust_no;
    }
    public function setCust_no($Cust_no){
        $this->Cust_no = $Cust_no;
    }
    public function getBankAccountNo(){
        return $this->BankAccountNo;
    }
    public function setBankAccountNo($BankAccountNo){
        $this->BankAccountNo = $BankAccountNo;
    }
    public function getBank(){
        return $this->Bank;
    }
    public function setBank($Bank){
        $this->Bank = $Bank;
    }
    public function getBankBranch(){
        return $this->BankBranch;
    }
    public function setBankBranch($BankBranch){
        $this->BankBranch = $BankBranch;
    }
    public function getImage(){
        return $this->Image;
    }
    public function setImage($Image){
        $this->Image = $Image;
    }
    public function getTradeType(){
        return $this->TradeType;
    }
    public function setTradeType($TradeType){
        $this->TradeType = $TradeType;
    }
    public function getR_Addr1(){
        return $this->R_Addr1;
    }
    public function setR_Addr1($R_Addr1){
        $this->R_Addr1 = $R_Addr1;
    }
    public function getR_District(){
        return $this->R_District;
    }
    public function setR_District($R_District){
        $this->R_District = $R_District;
    }
    public function getR_Province(){
        return $this->R_Province;
    }
    public function setR_Province($R_Province){
        $this->R_Province = $R_Province;
    }
    public function getLatitude(){
        return $this->Latitude;
    }
    public function setLatitude($Latitude){
        $this->Latitude = $Latitude;
    }
    public function getLongtitude(){
        return $this->Longtitude;
    }
    public function setLongtitude($Longtitude){
        $this->Longtitude = $Longtitude;
    }
    public function getIconImage(){
        return $this->IconImage;
    }
    public function setIconImage($IconImage){
        $this->IconImage = $IconImage;
    }
    public function getIconSize(){
        return $this->IconSize;
    }
    public function setIconSize($IconSize){
        $this->IconSize = $IconSize;
    }
    public function getFixIt01(){
        return $this->FixIt01;
    }
    public function setFixIt01($FixIt01){
        $this->FixIt01 = $FixIt01;
    }

    public function getFixIt02(){
        return $this->FixIt02;
    }
    public function setFixIt02($FixIt02){
        $this->FixIt02 = $FixIt02;
    }

    public function getFixIt03(){
        return $this->FixIt03;
    }
    public function setFixIt03($FixIt03){
        $this->FixIt03 = $FixIt03;
    }

    public function getFixIt04(){
        return $this->FixIt04;
    }
    public function setFixIt04($FixIt04){
        $this->FixIt04 = $FixIt04;
    }

    public function getFixIt05(){
        return $this->FixIt05;
    }
    public function setFixIt05($FixIt05){
        $this->FixIt05 = $FixIt05;
    }

    public function getFixIt06(){
        return $this->FixIt06;
    }
    public function setFixIt06($FixIt06){
        $this->FixIt06 = $FixIt06;
    }
    public function DiscRate_GN(){
        return $this->DiscRate_GN;
    }
    public function setDiscRate_GN($DiscRate_GN){
        $this->DiscRate_GN = $DiscRate_GN;
    }
    public function getUse_FixIt(){
        return $this->Use_FixIt;
    }
    public function setUse_FixIt($Use_FixIt){
        $this->Use_FixIt = $Use_FixIt;
    }
    public function getFixIt_Password(){
        return $this->FixIt_Password;
    }
    public function setFixIt_Password($FixIt_Password){
        $this->FixIt_Password = $FixIt_Password;
    }
    public function getFixIt_Telephone(){
        return $this->FixIt_Telephone;
    }
    public function setFixIt_Telephone($FixIt_Telephone){
        $this->FixIt_Telephone = $FixIt_Telephone;
    }
    public function getFixIt_Device_Token(){
        return $this->FixIt_Device_Token;
    }
    public function setFixIt_Device_Token($FixIt_Device_Token){
        $this->FixIt_Device_Token = $FixIt_Device_Token;
    }
    public function getFixIt_OS(){
        return $this->FixIt_OS;
    }
    public function setFixIt_OS($FixIt_OS){
        $this->FixIt_OS = $FixIt_OS;
    }
    public function getFixIt_Latitude(){
        return $this->FixIt_Latitude;
    }
    public function setFixIt_Latitude($FixIt_Latitude){
        $this->FixIt_Latitude = $FixIt_Latitude;

    }
    public function getFixIt_Longtitude(){
        return $this->FixIt_Longtitude;
    }
    public function setFixIt_Longtitude($FixIt_Longtitude){
        $this->FixIt_Longtitude = $FixIt_Longtitude;
    }

}