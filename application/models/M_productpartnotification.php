<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_productpartnotification extends DataMapper {

    //put your code here
    var $table = 'ProductPartNotification';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'productpart' => array(
             'class' => 'M_productpart',
               'other_field' => 'productpartnotification',
               'join_other_as' => 'ProductPart',
               'join_table' => 'productpart'
           ),
           'productpartstatus' => array(
              'class'=>'M_productpartstatus',
              'other_field' => 'productpartnotification',
              'join_other_as' => 'product_part_next_status',
              'join_table' => 'productpartstatus'
           )
   );
    
    var $has_many = array(
       'productpartnotificationlogs' => array(
           'class' => 'M_productpartnotificationlogs',
           'other_field' => 'productpartnotification',
           'join_self_as' => 'productpartnotification',
           'join_other_as' => 'productpartnotification',
           'join_table' => 'productpartnotificationlogs'
        ),
        'productpartnotificationresetlogs' => array(
            'class' => 'M_productpartnotificationresetlogs',
            'other_field' => 'productpartnotification',
            'join_self_as' => 'productpartnotification',
            'join_other_as' => 'productpartnotification',
            'join_table' => 'productpartnotificationresetlogs'
        )
    );


}