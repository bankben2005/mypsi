<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_flashsalesetproduct extends DataMapper {

    //put your code here
    var $table = 'FlashSaleSetProduct';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
     var $has_one = array(
             'flashsaleset' => array(
                 'class' => 'M_flashsaleset',
                 'other_field' => 'flashsalesetproduct',
                 'join_other_as' => 'FlashSaleSet',
                 'join_table' => 'FlashSaleSet'
             ),
             // 'questionnaireset' => array(
             //    'class' => 'M_questionnaireset',
             //    'other_field' => 'questionnaire',
             //    'join_other_as' => 'QuestionnaireSet',
             //    'join_table' => 'QuestionnaireSet'
             // )
     );
    
    var $has_many = array(
       'flashsaletransactionproduct' => array(
           'class' => 'M_flashsaletransactionproduct',
           'other_field' => 'flashsalesetproduct',
           'join_self_as' => 'FlashSaleSetProduct',
           'join_other_as' => 'FlashSaleSetProduct',
           'join_table' => 'FlashSaleTransactionProduct')
  );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}