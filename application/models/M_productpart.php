<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_productpart extends DataMapper {

    //put your code here
    var $table = 'ProductPart';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'product' => array(
             'class' => 'M_product',
               'other_field' => 'productpart',
               'join_other_as' => 'product',
               'join_table' => 'product'
           ),
           'warranty' => array(
           	  'class' => 'M_warranty',
           	  'other_field' => 'productpart',
           	  'join_other_as' => 'warranty',
           	  'join_table' => 'warranty'
           )
   );
    
    var $has_many = array(
       'productpartnotification' => array(
           'class' => 'M_productpartnotification',
           'other_field' => 'productpart',
           'join_self_as' => 'ProductPart',
           'join_other_as' => 'productpart',
           'join_table' => 'productpartnotification'
        ),
       'productpartreplacement'=>array(
           'class' => 'M_productpartreplacement',
           'other_field' => 'productpart',
           'join_self_as' => 'ProductPart',
           'join_other_as' => 'productpart',
           'join_table' => 'ProductPartReplacement'
       )
  );

        function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}