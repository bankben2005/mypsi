<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_districts extends DataMapper {

    //put your code here
    var $table = 'Districts';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
//    var $has_one = array(
//            'gshop_shop' => array(
//              'class' => 'm_gshop_shop',
//                'other_field' => 'gshop_customer',
//                'join_other_as' => 'shop',
//                'join_table' => 'gshop_shop'
//            )
//    );
    
    var $has_many = array(
       'campaigninsuranceregister' => array(
           'class' => 'M_campaigninsuranceregister',
           'other_field' => 'districts',
           'join_self_as' => 'Districts',
           'join_other_as' => 'Districts',
           'join_table' => 'CampaignInsuranceRegister'
        ),
       'campaignpurifierregister' => array(
           'class' => 'M_campaignpurifierregister',
           'other_field' => 'Districts',
           'join_self_as' => 'Districts',
           'join_other_as' => 'Districts',
           'join_table' => 'CampaignPurifierRegister'
        )
    );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}