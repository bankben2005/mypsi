<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_questionnaireset extends DataMapper {

    //put your code here
    var $table = 'QuestionnaireSet';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
//    var $has_one = array(
//            'gshop_shop' => array(
//              'class' => 'm_gshop_shop',
//                'other_field' => 'gshop_customer',
//                'join_other_as' => 'shop',
//                'join_table' => 'gshop_shop'
//            )
//    );
    
    var $has_many = array(
       'questionnaire' => array(
           'class' => 'M_questionnaire',
           'other_field' => 'QuestionnaireSet',
           'join_self_as' => 'QuestionnaireSet',
           'join_other_as' => 'QuestionnaireSet',
           'join_table' => 'Questionnaire')
  );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}