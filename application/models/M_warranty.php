<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_warranty extends DataMapper {

    //put your code here
    var $table = 'Warranty';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
//    var $has_one = array(
//            'gshop_shop' => array(
//              'class' => 'm_gshop_shop',
//                'other_field' => 'gshop_customer',
//                'join_other_as' => 'shop',
//                'join_table' => 'gshop_shop'
//            )
//    );
    
    var $has_many = array(
       'productpart' => array(
           'class' => 'M_productpart',
           'other_field' => 'warranty',
           'join_self_as' => 'warranty',
           'join_other_as' => 'warranty',
           'join_table' => 'productpart'
       ),
       'product'=>array(
          'class'=>'M_product',
          'other_field'=>'warranty',
          'join_self_as'=>'Warranty',
          'join_other_as'=>'warranty',
          'join_table'=>'product'
       )
  );
    


}