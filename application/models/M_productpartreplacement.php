<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_productpartreplacement extends DataMapper {

    //put your code here
    var $table = 'ProductPartReplacement';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'productpart' => array(
             'class' => 'M_productpart',
               'other_field' => 'productpartreplacement',
               'join_other_as' => 'ProductPart',
               'join_table' => 'ProductPart'
           )
   );
    
    // var $has_many = array(
    //    'campaigninsuranceregister' => array(
    //        'class' => 'M_campaigninsuranceregister',
    //        'other_field' => 'provinces',
    //        'join_self_as' => 'Provinces',
    //        'join_other_as' => 'Provinces',
    //        'join_table' => 'CampaignInsuranceRegister')
    // );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}