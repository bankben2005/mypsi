<?php

$lang['Customer Data'] = "ข้อมูลลูกค้า";
$lang['Customer'] = "Customer";
$lang['Customer list'] = "รายการลูกค้า";
$lang['Name'] = "ชื่อ-สกุล";
$lang['Address'] = "ที่อยู่";
$lang['Telephone'] = "เบอร์โทรศัพท์";
$lang['CustomerID'] = "รหัสลูกค้า";
$lang['Email'] = "อีเมล์";
$lang['Customer List'] = "รายการลูกค้า";
$lang['Search Customer'] = "ค้นหาลูกค้า";
$lang['Customer ID'] = "รหัสลูกค้า";
$lang['Customer Name'] = "ชื่อ-สกุล";
$lang['Customer Email'] = "อีเมล์";
$lang['Customer Telephone'] = "เบอร์โทรศัพท์";
$lang['Product Item(s)'] = "Product Item(s)";
