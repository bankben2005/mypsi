<?php

$lang['Name'] = "ชื่อ-สกุล";
$lang['Email'] = "อีเมล์";
$lang['Telephone'] = "เบอร์โทรศัพท์";
$lang['Address'] = "ที่อยู่";
$lang['Login Type'] = "รูปแบบการเข้าระบบ";
$lang['Device Token'] = "Device Token";
$lang['Submit'] = "ตกลง";
$lang['Edit Customer'] = "แก้ไขข้อมูลลูกค้า";
$lang['Product List'] = "รายการสินค้า";
$lang['Not found product for this customer'] = "ไม่พบสินค้าสำหรับลูกค้านี้";
$lang['Start Warranty'] = "เริ่มรับประกัน";
$lang['Expire Warranty'] = "สิ้นสุดรับประกัน";
$lang['Active'] = "รับประกัน";
$lang['Expired'] = "สิ้นสุดรับประกัน";
