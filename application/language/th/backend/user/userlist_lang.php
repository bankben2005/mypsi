<?php

$lang['UserList'] = "รายการผู้ใช้งาน";
$lang['Create User'] = "สร้างผู้ใช้งาน";
$lang['Firstname&amp;Lastname'] = "ชื่อ-สกุล";
$lang['Email'] = "อีเมล์";
$lang['AccessType'] = "สิทธิการเข้าใช้งาน";
$lang['Created'] = "สร้างเมื่อ";
$lang['Status'] = "สถานะ";
$lang['Delete user success'] = "ลบข้อมูลผู้ใช้งานสำเร็จ";
$lang['Picture'] = "รุปภาพ";
