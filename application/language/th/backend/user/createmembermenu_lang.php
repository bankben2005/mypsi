<?php

$lang['Create'] = "สร้าง";
$lang['Member Menu'] = "เมนู";
$lang['MemberMenuList'] = "รายการเมนู";
$lang['Menu Name'] = "ชื่อเมนู";
$lang['Controller Name'] = "Controller Name";
$lang['Method Name'] = "Method Name";
$lang['Status'] = "สถานะ";
$lang['Active'] = "เปิดใช้งาน";
$lang['UnActive'] = "ปิดใช้งาน";
$lang['Edit'] = "แก้ไข";
