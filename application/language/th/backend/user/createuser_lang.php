<?php

$lang['Edit'] = "แก้ไข";
$lang['UserList'] = "รายการผู้ใช้งาน";
$lang['Create'] = "สร้าง";
$lang['User'] = "ผู้ใช้งาน";
$lang['Firstname'] = "ชื่อ";
$lang['Lastname'] = "สกุล";
$lang['Email'] = "อีเมล์";
$lang['Access Type'] = "ประเภทการเข้าถึง";
$lang['User Permissions'] = "ตั้งค่าสิทธิการเข้าถึงเมนูต่างๆ";
$lang['Menu'] = "Menu";
$lang['Active'] = "เปิดใช้งาน";
$lang['Unactive'] = "ไม่เปิดใช้งาน";
$lang['Controller Name'] = "Controller Name";
$lang['Method Name'] = "Method Name";
$lang['Event Availability'] = "Event Availability";
$lang['View'] = "ดู";
$lang['Delete'] = "ลบ";
$lang['Update member data success'] = "แก้ไขข้อมูลผู้ใช้งานสำเร็จ";
$lang['Reset Password'] = "รีเซ็ตรหัสผ่าน";
$lang['New password'] = "รหัสผ่านใหม่";
$lang['Confirm new password'] = "ยืนยันรหัสผ่านใหม่";
$lang['Password'] = "รหัสผ่าน";
$lang['Confirm Password'] = "ยืนยันรหัสผ่าน";
$lang['Create member success'] = "Create member success";
$lang['pls_select_file'] = "เลือกรูป";
$lang['Can not create member data'] = "Can not create member data";
