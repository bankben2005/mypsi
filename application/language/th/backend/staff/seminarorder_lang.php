<?php

$lang['Staff'] = "Staff";
$lang['SeminarOrder'] = "สั่งจองสินค้าในวันสัมมนา";
$lang['Seminar List'] = "รายการสัมมนา";
$lang['Seminar list'] = "รายการสัมมนา";
$lang['Name'] = "ชื่อสัมมนา";
$lang['SeminarDate'] = "วันที่สัมมนา";
$lang['Product Available'] = "สินค้าที่สามารถจองได้ (ชิ้น)";
