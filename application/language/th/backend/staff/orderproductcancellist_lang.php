<?php


$lang['Seminar List'] = "รายการสัมมนา";
$lang['Order Product Cancel List'] = "รายการ";
$lang['SeminarName'] = "สัมมนา";
$lang['MPNAME'] = "ชื่อสินค้า";
$lang['AgentCode'] = "รหัสช่าง";
$lang['CardNo'] = "เลขบัตร";
$lang['CustomerName'] = "ชื่อลูกค้า";
$lang['CustomerTelephone'] = "เบอร์โทร";
$lang['Branch'] = "สาขา";
$lang['Amount'] = "จำนวน";
$lang['Created'] = "จองเมื่อ";
$lang['Delete'] = "ลบข้อมูล";
$lang['Cancel'] = "ยกเลิก";
$lang['OK'] = "ตกลง";
$lang['SeminarOrderProductCancelList'] = "รายการยกเลิกการจองสินค้า";
$lang['Restore'] = "คืนค่า";
$lang['Restore product order success!'] = "คืนค่าการจองสินค้าสำเร็จ!";
$lang['Updated'] = "แก้ไขล่าสุด";
