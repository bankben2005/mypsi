<?php

$lang['ClothDeal List'] = "ClothDeal List";
$lang['Add Seminar S3Deal'] = "ลงทะเบียนรับกล่อง S3";
$lang['Register ClothDeal'] = "Register ClothDeal";
$lang['Scan QR Code'] = "สแกน QR Code";
$lang['Enter Detail'] = "กรอกรายละเอียด";
$lang['S3Deal List'] = "รายการลงทะเบียนรับกล่อง S3";
$lang['Register S3Deal'] = "ลงทะเบียนรับกล่อง S3";

$lang['AgentCode'] = "รหัสช่าง";
$lang['Name&amp;Lastname'] = "ชื่อ-สกุล";
$lang['Email'] = "อีเมล์";
$lang['Telephone'] = "เบอร์โทรศัพท์";
$lang['Address'] = "ที่อยู่";
$lang['R_District'] = "อำเภอ";
$lang['R_Province'] = "จังหวัด";
$lang['Branch'] = "สาขา";
$lang['District'] = "อำเภอ";
$lang['Province'] = "จังหวัด";
$lang['Amphur'] = "อำเภอ";
$lang['CardNo'] = "หมายเลขบัตร";
$lang['Lastname'] = "Lastname";
$lang['Add rows'] = "Add rows";
$lang['Fill agent card number and choose size'] = "Fill agent card number and choose size";
$lang['Total Tickets'] = "Total Tickets";
$lang['Choose Size'] = "Choose Size";
