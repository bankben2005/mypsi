<?php

$lang['SeminarOrderProductList'] = "รายการจองสินค้าในงานสัมมนา";
$lang['Seminar List'] = "รายการสัมมนา";
$lang['Order Product List'] = "รายการจองสินค้า";
$lang['SeminarNo'] = "รหัสสัมมนา";
$lang['AgentCode'] = "รหัสช่าง";
$lang['MPCODE'] = "รหัสสินค้า";
$lang['Amount'] = "จำนวน";
$lang['Created'] = "จองเมื่อ";
$lang['CustomerName'] = "ชื่อลูกค้า";
$lang['CustomerTelephone'] = "เบอร์โทร";
$lang['Branch'] = "สาขา";
$lang['Select Year'] = "Select Year";
$lang['CardNo'] = "เลขบัตร";
$lang['MPNAME'] = "ชื่อสินค้า";
$lang['SeminarName'] = "สัมมนา";
$lang['Select Seminar Group'] = "-- เลือกกรุ๊ปสมนา --";
$lang['Export to excel file'] = "Export to excel file";
$lang['Delete'] = "ลบข้อมูล";
$lang['OK'] = "ตกลง";
$lang['Delete product order success!'] = "ลบข้อมูลรายการจองสินค้าสำเร็จ!";
$lang['Cancel'] = "ยกเลิก";
$lang['Cancel product order success!'] = "ยกเลิกการจองสินค้าสำเร็จ!";
