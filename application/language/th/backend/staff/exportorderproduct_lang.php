<?php

$lang['ExportOrderProduct'] = "ส่งออกรายการจองสินค้า";
$lang['Order Product List'] = "รายการจองสินค้า";
$lang['Export Order Product'] = "ส่งออกรายการจองสินค้า";
$lang['Export to excel file'] = "ส่งออกเป็นไฟล์ excel (.xlsx)";
$lang['Select Year'] = "เลือกปี";
$lang['Select Seminar Group'] = "เลือกสัมมนากรุ๊ป";
$lang['Order product not found'] = "ไม่พบรายการจองสินค้า";
