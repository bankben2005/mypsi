<?php

$lang['Seminar'] = "สัมมนา";
$lang['Check Permit'] = "ตรวจสอบสิทธิเข้าสัมมนา";
$lang['Seminar check permit'] = "ตรวจสอบสิทธิเข้าสัมมนา";
$lang['Check permit form'] = "แบบฟอร์มตรวจสอบสิทธิเข้าสัมมนา";
$lang['Card No'] = "หมายเลขบัตร";
$lang['Check Permission'] = "ตรวจสอบ";
$lang['Agent Data'] = "ข้อมูลช่าง";
$lang['Ticket History'] = "ข้อมูลประวัติการซื้อบัตร";
$lang['Quota History'] = "ประวัติการเข้าสัมนาแบบโควต้า";
