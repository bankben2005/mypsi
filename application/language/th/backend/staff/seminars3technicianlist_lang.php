<?php

$lang['SeminarS3TechnicialList'] = "รายการผู้ลงทะเบียนรับกล่องแล้ว";
$lang['Seminar List'] = "รายการสัมมนา";
$lang['Seminar S3 Technicial List'] = "รายการผู้ลงทะเบียนรับกล่องแล้ว";
$lang['Add Customer S3 Deal'] = "ลงทะเบียนรับกล่อง S3";
$lang['Seminar S3 Deal list'] = "รายการลงทะเบียนรับกล่อง S3";
$lang['Name'] = "ชื่อ";
$lang['AgentCode'] = "รหัสช่าง";
$lang['Totals'] = "จำนวน";
$lang['Telephone'] = "เบอร์โทรศัพท์";
$lang['Created'] = "รับเมื่อ";
