<?php

$lang['Staff'] = "Staff";
$lang['SeminarClothDealList'] = "รายการลงทะเบียน/ข้อมูลเข้างาน";
$lang['Seminar ClothDeal List'] = "รายการลงทะเบียนสัมนา";
$lang['Seminar list'] = "รายการสัมมนา";
$lang['Name'] = "ชื่อ สกุล";
$lang['AgentCode'] = "รหัสช่าง";
$lang['Size'] = "Size";
$lang['Telephone'] = "เบอร์โทร";
$lang['Add Customer ClothDeal'] = "ลงทะเบียน/เพิ่มข้อมูลเข้างาน";
$lang['Seminar ClothDeal list'] = "รายการลงทะเบียน/ข้อมูลเข้างาน";
$lang['Totals'] = "รับเสื้อจำนวน (ตัว)";
$lang['Created'] = "ว/ด/ป";
$lang['Seminar List'] = "รายการสัมมนา";
