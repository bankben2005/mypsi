<?php

$lang['Edit Seminar Order Product'] = "Edit Seminar Order Product";
$lang['Add Seminar ClothDeal'] = "เพิ่มรายชื่อลงทะเบียน/ข้อมูลเข้างาน";
$lang['Scan QR Code'] = "สแกน QR Code";
$lang['Enter Detail'] = "กรอกรายละเอียด";
$lang['AgentCode'] = "รหัสช่าง";
$lang['Name&amp;Lastname'] = "ชื่อ-สกุล";
$lang['Email'] = "อีเมล์";
$lang['Telephone'] = "เบอร์โทรศัพท์";
$lang['Address'] = "ที่อยู่";
$lang['R_District'] = "อำเภอ";
$lang['R_Province'] = "จังหวัด";
$lang['Branch'] = "สาขา";
$lang['District'] = "อำเภอ";
$lang['Province'] = "จังหวัด";
$lang['Amphur'] = "อำเภอ";
$lang['CardNo'] = "หมายเลขบัตร";
$lang['ClothDeal List'] = "รายการลงทะเบียน/ข้อมูลเข้างาน";
$lang['Register ClothDeal'] = "ลงทะเบียน/ข้อมูลเข้างาน";
$lang['ClothDeal Detail'] = "ClothDeal Detail";
$lang['Add rows'] = "Add rows";
$lang['Fill agent card number and choose size'] = "กรอกหมายเลขบัตรเพื่อลงทะเบียน";
$lang['Total Tickets'] = "จำนวนบัตร";
$lang['Choose Size'] = "ไซด์";
$lang['Lastname'] = "นามสกุล";
$lang['Add Seminar S3Deal'] = "Add Seminar S3Deal";
$lang['Input cardno for register/clothdeal'] = "สแกนเลขหลังบัตร เพื่อลงทะเบียน/ข้อมูลเข้างาน";
