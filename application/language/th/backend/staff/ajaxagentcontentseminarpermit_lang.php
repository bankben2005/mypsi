<?php

$lang['Card No'] = "หมายเลขบัตร";
$lang['Agent Code'] = "รหัสช่าง";
$lang['Firstname&amp;Lastname'] = "ชื่อ-สกุล";
$lang['Email'] = "อีเมล์";
$lang['Telephone'] = "เบอร์โทร";
$lang['Address'] = "จังหวัด";
$lang['Amphur'] = "อำเภอ";
$lang['Province'] = "จังหวัด";
$lang['Branch'] = "สาขา";
