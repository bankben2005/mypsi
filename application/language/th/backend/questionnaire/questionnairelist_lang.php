<?php

$lang['QuestionnaireList'] = "รายการคำถามแบบสอบถาม";
$lang['Question'] = "คำถาม";
$lang['Questionnaire Set'] = "ชุดคำถาม";
$lang['Questionnaire Category'] = "หมวดหมู่คำถาม";
$lang['Created'] = "สร้างเมื่อ";
$lang['Updated'] = "แก้ไขล่าสุด";
$lang['Active'] = "สถานะการใช้งาน";
$lang['Create questtionnaire'] = "สร้างคำถามสำหรับแบบสอบถาม";
