<?php

$lang['QuestionnaireSetList'] = "ชุดคำถามแบบสอบถาม";
$lang['Name'] = "ชื่อชุด";
$lang['Description'] = "รายละเอียด";
$lang['StartDate'] = "วันเริ่มต้น";
$lang['EndDate'] = "วันสิ้นสุด";
$lang['Created'] = "สร้างเมื่อ";
$lang['Updated'] = "แก้ไขล่าสุด";
$lang['Active'] = "เปิดใช้งาน";
$lang['Create questtionnaire set list'] = "สร้างชุดคำถามแบบสอบถาม";
$lang['Status'] = "สถานะ";
$lang['Delete questionnaire set success'] = "ลบข้อมูลแบบสอบถามเรียบร้อย";
$lang['Unactive'] = "ปิดใช้งาน";
