<?php

$lang['QuestionnaireAnswer'] = "รายการคำตอบประเมิน";
$lang['QuestionnaireSet'] = "ชุดคำถาม";
$lang['Assessor'] = "ผู้ประเมิน";
$lang['Assessment'] = "ผู้ถูกประเมิน";
$lang['Created'] = "สร้างเมื่อ";
$lang['AgentCode'] = "AgentCode";
$lang['Delete questionnaire answer success!'] = "Delete questionnaire answer success!";
$lang['Summary'] = "ภาพรวม (%)";
$lang['Recommend'] = "ความคิดเห็นเพิ่มเติม";
$lang['Select Questionnaire Set'] = "-- เลือกชุดคำถาม --";
$lang['Export to xlsx'] = "ส่งออกเป็นไฟล์ Excel";
$lang['Export'] = "ส่งออก";
