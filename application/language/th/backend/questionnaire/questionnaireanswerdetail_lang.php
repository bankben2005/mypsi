<?php

$lang['QuestionnaireAnswerDetail'] = "รายละเอียดการตอบคำถามประเมิน";
$lang['QuestionnaireAnswer'] = "รายการคำตอบประเมิน";
$lang['QuestionnaireSet'] = "ชุดคำถามประเมิน";
$lang['Assessor'] = "ผู้ประเมิน";
$lang['Name&amp;Lastname'] = "ชื่อ-สกุล";
$lang['AgentCode'] = "รหัสช่าง";
$lang['Telephone'] = "เบอร์โทร";
$lang['Province'] = "จังหวัด";
$lang['Assessment'] = "ผู้ถูกประเมิน";
$lang['QuestionnaireResult'] = "ผลประเมิน";
$lang['Question'] = "คำถาม";
$lang['AssessmentResult'] = "ผลประเมิน";
$lang['Recommend'] = "ความคิดเห็นเพิ่มเติม";
$lang['QuestionnaireCategory'] = "หมวดหมู่คำถาม";
