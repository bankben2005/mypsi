<?php

$lang['Create'] = "สร้าง";
$lang['QuestionnaireSetList'] = "รายการชุดคำถามแบบสอบถาม";
$lang['QuestionnaireSet'] = "ชุดคำถามแบบสอบถาม";
$lang['Name'] = "ชื่อชุดแบบสอบถาม";
$lang['Description'] = "รายละเอียด";
$lang['Start Date'] = "วันเริ่มต้น";
$lang['End Date'] = "วันสิ้นสุด";
$lang['Active'] = "เปิดใช้งาน";
$lang['Unactive'] = "ปิดใช้งาน";
$lang['Form'] = "แบบฟอร์ม";
$lang['Edit'] = "แก้ไข";
$lang['Duration'] = "วันเริ่มต้น - วันสิ้นสุด";
