<?php

$lang['Create'] = "สร้าง";
$lang['Questionnaire'] = "คำถามประเมิน";
$lang['QuestionnaireList'] = "รายการคำถามประเมิน";
$lang['Question'] = "คำถาม";
$lang['Active'] = "เปิดใช้งาน";
$lang['Unactive'] = "ปิดใช้งาน";
$lang['Questionnaire Set'] = "ชุดคำถาม";
$lang['Questionnaire Category'] = "หมวดหมู่คำถาม";
$lang['Complete'] = "สำเร็จ";
$lang['Edit'] = "แก้ไข";
$lang['Questionnaire Type'] = "ประเภทคำตอบ";
$lang['Fillin'] = "กรอกรายละเอียด";
$lang['Rating'] = "แบบให้ดาว";
