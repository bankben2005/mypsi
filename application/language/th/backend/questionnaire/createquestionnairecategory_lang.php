<?php

$lang['Create'] = "สร้าง";
$lang['QuestionnaireCategory'] = "หมวดหมู่คำถาม";
$lang['QuestionnaireSet'] = "ชุดคำถาม";
$lang['Name'] = "ชื่อหมวดหมู่";
$lang['Active'] = "เปิดใช้งาน";
$lang['Unactive'] = "ปิดใช้งาน";
$lang['Edit'] = "แก้ไข";
$lang['Complete'] = "สำเร็จ";
