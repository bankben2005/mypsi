<?php

$lang['Home'] = "หน้าแรก";
$lang['Customer'] = "ข้อมูลลูกค้า";
$lang['Search'] = "ค้นหา";
$lang['Close'] = "ปิด";
$lang['Submit'] = "ตกลง";
$lang['Account has been blocked,Please contact administrator.'] = "บัญชีถูกระงับการใช้งาน กรุณาติดต่อผู้ดูแลระบบ.";
$lang['Reset'] = "รีเซ็ต";
$lang['Back'] = "ย้อนกลับ";
$lang['Technician'] = "Technician";
$lang['Staff'] = "Staff";
$lang['SeminarOrder'] = "สั่งจองสินค้าในวันสัมนา";
$lang['Seminar List'] = "รายการสัมนา";
$lang['Seminar'] = "สัมนา";
$lang['Edit'] = "แก้ไข";
$lang['Delete'] = "ลบ";
$lang['Active'] = "เปิดใช้งาน";
$lang['Unactive'] = "ไม่เปิดใช้งาน";
$lang['Create'] = "สร้าง";
$lang['Complete'] = "สำเร็จ";
$lang['No Expired'] = "ยังไม่หมดอายุ";
$lang['Expired'] = "หมดอายุแล้ว";
$lang['Status'] = "สถานะ";
$lang['Save'] = "บันทึก";
