<?php

$lang['Create'] = "เพิ่ม";
$lang['Flash Sale'] = "Flash Sale";
$lang['FlashSale List'] = "FlashSale List";
$lang['FlashSale'] = "FlashSale";
$lang['Name'] = "หัวข้อ";
$lang['Description'] = "รายละเอียด";
$lang['Start Datetime'] = "วัน/เวลา เริ่มต้น";
$lang['End Datetime'] = "วัน/เวลา สิ้นสุด";
$lang['Step 1'] = "ขั้นตอนที่ 1";
$lang['Information Form'] = "กรอกข้อมูลทั่วไป";
$lang['Step 2'] = "ขั้นตอนที่ 2";
$lang['Choose Product Item(s)'] = "เลือกสินค้าที่ต้องการทำ FlashSale";
$lang['Step 3'] = "ขั้นตอนที่ 3";
$lang['Confirm PR'] = "ยืนยันการเพิ่ม FlashSale";
$lang['Start Datetime - End Datetime'] = "ระยะเวลาเริ่มต้น - สิ้นสุด";
