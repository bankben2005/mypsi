<?php

$lang['FlashSale List'] = "รายการ FlashSale";
$lang['Create FlashSale'] = "สร้าง FlashSale";
$lang['Name'] = "ชื่อ";
$lang['Description'] = "รายละเอียด";
$lang['Start'] = "เริ่มต้น";
$lang['End'] = "สิ้นสุด";
$lang['Created'] = "สร้างเมื่อ";
$lang['Updated'] = "แก้ไขล่าสุด";
$lang['Status'] = "สถานะ";
$lang['Expired Status'] = "สถานะหมดอายุ";
$lang['Active'] = "เปิดใช้งาน";
$lang['Expired'] = "หมดอายุแล้ว";
$lang['Remaining'] = "Remaining";
$lang['Product Quantity'] = "Product Quantity";
$lang['Edit flashsale product'] = "Edit flashsale product";
