<?php

$lang['Edit'] = "แก้ไข";
$lang['ActivityList'] = "รายการกิจกรรม";
$lang['Activity Name'] = "ชื่อกิจกรรม";
$lang['Duration'] = "เริ่มต้น-สิ้นสุด";
$lang['Url'] = "Url";
$lang['Status'] = "สถานะ";
$lang['Active'] = "เปิดใช้งาน";
$lang['UnActive'] = "ปิดใช้งาน";
$lang['ActivityType'] = "ประเภทกิจกรรม";
$lang['confirm_delete_image'] = "ยืนยันการลบรูปภาพ";
$lang['pls_select_file'] = "เลือกไฟล์";
$lang['Create'] = "สร้าง";
$lang['Activity'] = "กิจกรรม";
$lang['Image Preview'] = "Image Preview";
