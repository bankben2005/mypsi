<?php

$lang['Create'] = "Create";
$lang['Flash Sale'] = "Flash Sale";
$lang['FlashSale List'] = "FlashSale List";
$lang['FlashSale'] = "FlashSale";
$lang['Add More Product'] = "Add More Product";
$lang['Search By MPCODE'] = "Search By MPCODE";
$lang['Search By ProductName'] = "Search By ProductName";
$lang['Search MPCODE'] = "Search MPCODE";
$lang['Search Product'] = "Search Product";
$lang['MPCODE'] = "MPCODE";
$lang['Product Choosed'] = "Product Choosed";
$lang['Back'] = "Back";
$lang['Default'] = "Default";
$lang['Product Code'] = "Product Code";
$lang['Product Name'] = "Product Name";
$lang['Quantity'] = "Quantity";
$lang['MPCODE..'] = "MPCODE..";
$lang['MPNAME..'] = "MPNAME..";
