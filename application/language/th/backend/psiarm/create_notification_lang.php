<?php

$lang['Create'] = "Create";
$lang['Notification'] = "Notification";
$lang['Notification List'] = "Notification List";
$lang['Notification Form'] = "Notification Form";
$lang['Notification Message'] = "Notification Message";
$lang['Send Type'] = "Send Type";
$lang['Send All'] = "Send All";
$lang['Send By Device Token'] = "Send By Device Token";
$lang['Device Token'] = "Device Token";
