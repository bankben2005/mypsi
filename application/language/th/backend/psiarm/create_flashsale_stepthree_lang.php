<?php

$lang['Create'] = "Create";
$lang['Flash Sale'] = "Flash Sale";
$lang['FlashSale List'] = "FlashSale List";
$lang['FlashSale'] = "FlashSale";
$lang['Add More Product'] = "Add More Product";
$lang['General Information'] = "General Information";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['Duration'] = "Duration";
$lang['Product Information'] = "Product Information";
$lang['Product Code'] = "Product Code";
$lang['Product Name'] = "Product Name";
$lang['Product Description'] = "Product Description";
$lang['Quantity'] = "Quantity";
$lang['Confirm FlashSale'] = "Confirm FlashSale";
