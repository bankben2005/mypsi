<?php

$lang['ActivityList'] = "รายการกิจกรรม";
$lang['Activity'] = "กิจกรรม";
$lang['Create Activity'] = "สร้างกิจกรรม";
$lang['Activity Name'] = "ชื่อกิจกรรม";
$lang['Cover'] = "รูปภาพปก";
$lang['StartDate'] = "เริ่มต้น";
$lang['EndDate'] = "สิ้นสุด";
$lang['Created'] = "สร้างเมื่อ";
$lang['Status'] = "สถานะ";
$lang['Updated'] = "แก้ไขล่าสุด";
