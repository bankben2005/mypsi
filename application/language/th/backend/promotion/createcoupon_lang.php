<?php

$lang['Create'] = "สร้าง";
$lang['Coupon'] = "คูปอง";
$lang['CouponList'] = "รายการคูปอง";
$lang['Coupon Code'] = "คูปองโค๊ด";
$lang['Coupon Name'] = "ชื่อคูปอง";
$lang['Discount Rate'] = "ส่วนลด";
$lang['Duration'] = "เริ่มต้น-สิ้นสุด";
$lang['Active'] = "เปิดใช้งาน";
$lang['Unactive'] = "ปิดใช้งาน";
$lang['Edit'] = "แก้ไข";
$lang['Discount Type'] = "หน่วยที่ลด";
$lang['Amount'] = "บาท";
$lang['Percent'] = "เปอร์เซ็นต์";
$lang['CouponType'] = "ประเภทส่วนลด";
$lang['pls_select_file'] = "เลือกไฟล์";
$lang['confirm_delete_image'] = "ยืนยันการลบรูปภาพ";
$lang['delete_image'] = "ลบรูปภาพ";
$lang['Coupon Description'] = "รายละเอียดคูปอง";
$lang['Delete coupon image success'] = "Delete coupon image success";
$lang['Image Preview'] = "Image Preview";
$lang['Point'] = "คะแนน";
$lang['Point Use'] = "จำนวนแต้มสำหรับแลก";
