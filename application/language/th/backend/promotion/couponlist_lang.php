<?php

$lang['CouponList'] = "รายการคูปอง";
$lang['Create Coupon'] = "สร้างคูปอง";
$lang['Coupon Code'] = "โค๊ด";
$lang['Coupon Name'] = "ชื่อคูปอง";
$lang['Discount Rate'] = "ส่วนลด";
$lang['Discount Type'] = "ประเภทส่วนลด";
$lang['StartDate'] = "เริ่มใช้งาน";
$lang['EndDate'] = "สิ้นสุดใช้งาน";
$lang['Created'] = "สร้างเมื่อ";
$lang['Status'] = "สถานะ";
$lang['Delete coupon success'] = "Delete coupon success";
$lang['Expire Status'] = "Expire Status";
