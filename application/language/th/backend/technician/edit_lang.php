<?php

$lang['Fistname'] = "Fistname";
$lang['Lastname'] = "Lastname";
$lang['Email'] = "Email";
$lang['Telephone'] = "Telephone";
$lang['Address'] = "Address";
$lang['Province'] = "Province";
$lang['District'] = "District";
$lang['Amphur'] = "Amphur";
$lang['Select Province'] = "Select Province";
$lang['Select Amphur'] = "Select Amphur";
$lang['Zipcode'] = "Zipcode";
