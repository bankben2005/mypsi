<?php

$lang['Technician'] = "Technician";
$lang['Satellite'] = "Satellite";
$lang['Technician satellite list'] = "Technician satellite list";
$lang['Firstname&amp;Lastname'] = "Firstname&amp;Lastname";
$lang['Name'] = "Name";
$lang['Address'] = "Address";
$lang['Telephone'] = "Telephone";
$lang['Technician Skill'] = "Technician Skill";
$lang['Technician Satellite List'] = "Technician Satellite List";
$lang['Search Technician'] = "Search Technician";
$lang['Province'] = "Province";
$lang['Amphur'] = "Amphur";
