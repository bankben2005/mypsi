<?php

$lang['Firstname'] = "ชื่อ";
$lang['Lastname'] = "นามสกุล";
$lang['Telephone'] = "เบอร์โทร";
$lang['Address'] = "ที่อยู่";
$lang['Province'] = "จังหวัด";
$lang['Amphur'] = "อำเภอ";
$lang['District'] = "ตำบล";
$lang['Zipcode'] = "รหัสไปรษณีย์";
$lang['Email'] = "อีเมล์";
$lang['Select Province'] = "-- เลือกจังหวัด --";
$lang['Select Amphur'] = "-- เลือกอำเภอ --";
$lang['Select District'] = "-- เลือกตำบล --";
$lang['REGISTER'] = "ลงทะเบียน";
