<?php

$lang['Firstname'] = "ชื่อ";
$lang['Lastname'] = "นามสกุล";
$lang['Telephone'] = "เบอร์โทร";
$lang['Email'] = "อีเมล์";
$lang['Address'] = "ที่อยู่";
$lang['Province'] = "จังหวัด";
$lang['Amphur'] = "อำเภอ";
$lang['District'] = "ตำบล";
$lang['Zipcode'] = "รหัสไปรษณีย์";
$lang['REGISTER'] = "ลงทะเบียน";
$lang['Coordinator'] = "ผู้ประสานงาน";
$lang['Advisor Customer'] = "ผู้แนะนำ (ลูกค้า)";
$lang['Coordinator Telephone'] = "เบอร์โทร (ผู้ประสานงาน)";
