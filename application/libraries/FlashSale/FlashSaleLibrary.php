<?php


class FlashSaleLibrary{

    
    private $ci;
    function __construct() {
        $CI =& get_instance();
        $this->ci = $CI;
       
    }

    public function getFlashSale($requestCriteria){
        //print_r($requestCriteria);
        //print_r($requestCriteria);exit;
        $flashsaleset = $this->getFlashSaleSetAvailable();

        if($flashsaleset->id){
            $dataReturn = array();

            /* get date diff for countdown */
            $datetime_end = new DateTime($flashsaleset->end_datetime);
            $datetime_now = new DateTime();

            $interval = $datetime_end->diff($datetime_now);

            $dateend = strtotime($flashsaleset->end_datetime);
            $dataReturn = array(
                'name'=>$flashsaleset->name,
                'description'=>$flashsaleset->description,
                'start_datetime'=>date('Y-m-d H:i',strtotime($flashsaleset->start_datetime)),
                'end_datetime'=>date('Y-m-d H:i',strtotime($flashsaleset->end_datetime)),
                'remaining_seconds'=>$dateend-time(),
                'interval'=>$interval,
                'flashsale_product'=>$this->getFlashSaleProduct(array(
                    'flashsaleset_id'=>$flashsaleset->id
                ))
            );

            return array(
                'status'=>true,
                'FlashSaleData'=>$dataReturn,
                'has_flashsale'=>(count($dataReturn) > 0)?true:false,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );

        }else{
            return array(
                'status'=>true,
                'FlashSaleData'=>new stdClass(),
                'has_flashsale'=>false,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
        }

        //print_r($flashsaleset->to_array());
    }

    public function setFlashSaleOrder($requestCriteria){
        //print_r($requestCriteria);
        /* check product id */
        $flashsalesetproduct = new M_flashsalesetproduct($requestCriteria->product_id);
        if(!$flashsalesetproduct->id){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบสินค้าดังกล่าว'
            );
        }
        /* eof check product id*/

        /* check product remain */
        $product_available = $flashsalesetproduct->total_available;

        $product_ordered = $this->getProductOrderedByFlashSaleSetProductId($flashsalesetproduct->id);

        $product_remain = $product_available-$product_ordered;

        if($product_remain < $requestCriteria->order_quantity){
            return array(
                'status'=>false,
                'result_code'=>'-009',
                'result_desc'=>'จำนวนสินค้าไม่พอสำหรับสั่งซื้อ'
            );
        }
        /* eof check product remain*/


        /* get master product data*/
        $masterproductdata = new M_masterproduct();
        $masterproductdata->where('MPCODE',$flashsalesetproduct->MPCODE)->get();
        /* eof get master product data*/

        /* check agent already ordered this set */
        $check_transaction = new M_flashsaletransaction();
        $check_transaction->where('AgentCode',$requestCriteria->agent_code)
        ->where('FlashSaleSet_id',$this->getFlashSaleSetAvailable()->id)
        ->get();
        if($check_transaction->id){
            /* insert more only flashsale transaction item */

            //print_r($check_transaction->to_array());
            /* update transaction and transaction product */
            $check_transaction->sub_totals = $check_transaction->sub_totals + $this->getPurchaseSubTotals($requestCriteria);
            $check_transaction->grand_totals = $check_transaction->sub_totals;
            $check_transaction->total_amount = $check_transaction->total_amount+$requestCriteria->order_quantity;

            if($check_transaction->save()){
                /* update transaction product */
                $update_transactionproduct = new M_flashsaletransactionproduct();
                $update_transactionproduct->where('FlashSaleTransaction_id',$check_transaction->id)
                ->where('FlashSaleSetProduct_id',$requestCriteria->product_id)->get();

                if($update_transactionproduct->id){
                        /* add more product*/
                        $update_transactionproduct->amount = $update_transactionproduct->amount+$requestCriteria->order_quantity;
                        $update_transactionproduct->save();
                }else{
                        /* insert new product */
                        $transaction_product = new M_flashsaletransactionproduct();
                        $transaction_product->FlashSaleTransaction_id = $check_transaction->id;
                        $transaction_product->FlashSaleSetProduct_id = $requestCriteria->product_id;
                        $transaction_product->amount = $requestCriteria->order_quantity;
                        $transaction_product->save();

                }

                /* insert transaction logs */
                $log_criteria = json_encode($requestCriteria);
                $log_criteria = json_decode($log_criteria,true);
                $log_criteria['current_price'] = $masterproductdata->PriceB;
                unset($log_criteria['authentication']);
                $transaction_log = new M_flashsaletransactionlogs();
                $transaction_log->FlashSaleTransaction_id = $check_transaction->id;
                $transaction_log->FlashSaleSetProduct_id = $requestCriteria->product_id;
                $transaction_log->log_type = 'add';
                $transaction_log->log_message = json_encode($log_criteria);
                $transaction_log->save();


                /* eof insert transaction logs */


                /* send background notification for update */
                $this->setBackgroundNotification($requestCriteria);
                /* eof send background notification for update */

                return array(
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );

            }else{
                return array(
                    'status'=>false,
                    'result_code'=>'-004',
                    'result_desc'=>'ไม่สามารถอัพเดท transaction ได้'
                );
            }

        }else{
            /* insert new transaction and transaction item */
            $insert_transaction = new M_flashsaletransaction();
            $insert_transaction->AgentCode = $requestCriteria->agent_code;
            $insert_transaction->FlashSaleSet_id = $this->getFlashSaleSetAvailable()->id;
            $insert_transaction->sub_totals = $this->getPurchaseSubTotals($requestCriteria);
            $insert_transaction->vat_totals = 0;
            $insert_transaction->grand_totals = $insert_transaction->sub_totals;
            $insert_transaction->total_amount = $requestCriteria->order_quantity;
            $insert_transaction->status = 'ordered';



            if($insert_transaction->save()){
                /* insert into transaction product */
                $transaction_product = new M_flashsaletransactionproduct();
                $transaction_product->FlashSaleTransaction_id = $insert_transaction->id;
                $transaction_product->FlashSaleSetProduct_id = $requestCriteria->product_id;
                $transaction_product->amount = $requestCriteria->order_quantity;
                $transaction_product->save();

                /* insert transaction logs */
                $log_criteria = json_encode($requestCriteria);
                $log_criteria = json_decode($log_criteria,true);
                $log_criteria['current_price'] = $masterproductdata->PriceB;
                unset($log_criteria['authentication']);
                $transaction_log = new M_flashsaletransactionlogs();
                $transaction_log->FlashSaleTransaction_id = $insert_transaction->id;
                $transaction_log->FlashSaleSetProduct_id = $requestCriteria->product_id;
                $transaction_log->log_type = 'add';
                $transaction_log->log_message = json_encode($log_criteria);
                $transaction_log->save();
                /* eof insert transaction logs */


                /* send background notification for update */
                $this->setBackgroundNotification($requestCriteria);
                /* eof send background notification for update */

                
                return array(
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );

            }else{
                return array(
                    'status'=>false,
                    'result_code'=>'-003',
                    'result_desc'=>'ไม่สามารถเพิ่ม Transaction ได้'
                );
            }
        }
        /* eof check agent already ordered this set */

        // $flashsaletransaction = new M_flashsaletransaction();
        // $flashsaletransaction->AgentCode = $requestCriteria->agent_code;
        




    }

    public function getFlashSaleOrderHistory($requestCriteria){
        $arrReturn = array();
        //print_r($requestCriteria);exit;

        /* check agent code exist */
        if(!$this->checkAgentCodeExist($requestCriteria->agent_code)){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบ AgentCode ดังกล่าว'
            );
        }
        /* eof check agent code exist */

        $query = $this->ci->db->select('*,FlashSaleTransaction.id as transaction_id')
        ->from('FlashSaleTransaction')
        ->join('FlashSaleSet','FlashSaleSet.id = FlashSaleTransaction.FlashSaleSet_id')
        ->where('FlashSaleTransaction.AgentCode',$requestCriteria->agent_code)
        ->where('FlashSaleSet.active',1)
        ->order_by('FlashSaleTransaction.id','desc')->get();

        if($query->num_rows() > 0){
            //print_r($query->result());

            foreach ($query->result() as $key => $value) {
                # code...
                //print_r($value);
                $data_push = array(
                    'transaction_id'=>$value->transaction_id,
                    'transaction_grand_totals'=>number_format($value->grand_totals,2),
                    'transaction_amount_set'=>number_format($value->total_amount,2),
                    'transaction_product'=>$this->getFlashSaleTransactionProduct($value->transaction_id),
                    'flashsale_name'=>$value->name,
                    'flashsale_description'=>$value->description,
                    'flashsale_cancel_status'=>($this->checkIsExpiredFlashSale($value->FlashSaleSet_id))?false:true
                );
                array_push($arrReturn, $data_push);
            }


        }
        return array(
            'status'=>true,
            'FlashOrderHistory'=>$arrReturn,
            'result_code'=>'000',
            'result_desc'=>'Success'
        );

    }

    public function setFlashSaleOrderCancel($requestCriteria){
        //print_r($requestCriteria);
        /* check agent code exist */
        if(!$this->checkAgentCodeExist($requestCriteria->agent_code)){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบ AgentCode ดังกล่าว'
            );
        }
        /* eof check agent code exist */

        /* check transaction product id */
        $flashsaletransactionproduct = new M_flashsaletransactionproduct($requestCriteria->transaction_product_id);

        if(!$flashsaletransactionproduct->id){
            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบ Transaction Product'
            );
        }
        /* eof check transaction product id*/


        /* update transaction flashsale transaction amount and subtotal and grand total */
        /* then check amount if equal to zero remove this transaction */
        $flashsaletransaction = new M_flashsaletransaction($flashsaletransactionproduct->FlashSaleTransaction_id);

        if($flashsaletransaction->id){
                /* calculate price with amount */
                $masterproductdata = $this->getMasterProductData($flashsaletransactionproduct->flashsalesetproduct->get()->MPCODE);
                $total_price_cancel = ($masterproductdata['PriceB']*$flashsaletransactionproduct->amount);

                $flashsaletransaction->sub_totals = ($flashsaletransaction->sub_totals-$total_price_cancel);

                $flashsaletransaction->grand_totals = $flashsaletransaction->sub_totals;

                $flashsaletransaction->total_amount = ($flashsaletransaction->total_amount-$flashsaletransactionproduct->amount);

                //echo $total_price_cancel;
                $flashsaletransaction->save();


        }
       
        if($flashsaletransaction->total_amount <= 0){ /* if cancel then total amount equal 0*/
            
            /* delete logs first */
            $query = $this->ci->db->delete('FlashSaleTransactionLogs',array(
                'FlashSaleTransaction_id'=>$flashsaletransaction->id,
                'FlashSaleSetProduct_id'=>$flashsaletransactionproduct->FlashSaleSetProduct_id
            ));

            /* delete flashsale transaction product */
            $this->ci->db->delete('FlashSaleTransactionProduct',array(
                'id'=>$flashsaletransactionproduct->id
            ));
            //$flashsaletransactionproduct->delete();


            //$flashsaletransaction->delete();
            $this->ci->db->delete('FlashSaleTransaction',array(
                'id'=>$flashsaletransaction->id
            ));

            //echo 'aaaa';exit;

            /* send background notification for update */
            $this->setBackgroundNotification($requestCriteria);
            /* eof send background notification for update */

            return array(
                'status'=>true,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );

        }else{
            /* let set transaction product active to 0*/
            $flashsaletransactionproduct->active = 0;
            if($flashsaletransactionproduct->save()){

                /* insert log file */
                $log_criteria = json_encode($requestCriteria);
                $log_criteria = json_decode($log_criteria,true);
                unset($log_criteria['authentication']);
                $log_criteria->{'cancel_totals'} = $flashsaletransactionproduct->amount;
                $transaction_log = new M_flashsaletransactionlogs();
                $transaction_log->FlashSaleTransaction_id = $flashsaletransactionproduct->FlashSaleTransaction_id;
                $transaction_log->FlashSaleSetProduct_id = $flashsaletransactionproduct->FlashSaleSetProduct_id;
                $transaction_log->log_type = 'cancel';
                $transaction_log->log_message = json_encode($log_criteria);
                $transaction_log->save();

               $this->ci->db->delete('FlashSaleTransactionProduct',array(
                'id'=>$flashsaletransactionproduct->id
               ));
                


               /* send background notification for update */
                $this->setBackgroundNotification($requestCriteria);
                /* eof send background notification for update */
                return array(
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );
            }
            /* eof set transaction product active to 0*/
        }

    }

    public function setBackgroundNotification($requestCriteria){
        
        $this->ci->load->config('api');
        $onesignal_config = $this->ci->config->item('onesignal_psiarm');

        $content = array(
            'en' => 'PSIARM BACKGROUND NOTIFICATION'
        );

        $fields = array(
            'app_id' => $onesignal_config['app_id'],
            'include_player_ids' => array(),
            'included_segments'=> array('Active Users'),
            'data' => array("action" => "UpdateStock"),
            'content_available'=>true
            //'contents' => $content
        );

        $post_data = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $onesignal_config['url']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $onesignal_config['header']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        $response_decode = json_decode($response);


        if($response_decode->recipients){

        }

        return true;
        //print_r($response);
        //return $response;


        //print_r($onesignal_config);exit;

        // print_r($this->api_config->)
        // print_r($api_config-);
    }

    private function getFlashSaleProduct($data = array()){
        $arrReturn = array();
        $flashsalesetproduct = new M_flashsalesetproduct();
        $flashsalesetproduct->where('FlashSaleSet_id',$data['flashsaleset_id'])->where('active',1)->get();

        if($flashsalesetproduct->result_count() > 0){
            foreach ($flashsalesetproduct as $key => $value) {
                # code...
                $masterproductdata = $this->getMasterProductData($value->MPCODE);
                //array_push($arrReturn, $value->to_array());
                $data_push = array(
                    'product_id'=>$value->id,
                    'product_code'=>$value->MPCODE,
                    'product_name'=>$value->name,
                    'product_title'=>$value->title,
                    'product_description'=>$value->description,
                    'product_available'=>$value->total_available,
                    'product_image_list'=>$this->getFlashSaleSetProductImage(array(
                        'type'=>'portait',
                        'flashsalesetproduct_id'=>$value->id
                    )),
                    'product_image_detail'=>$this->getFlashSaleSetProductImage(array(
                        'type'=>'landscape',
                        'flashsalesetproduct_id'=>$value->id
                    )),
                    'product_price_before'=>$masterproductdata['PriceA'],
                    'product_price_after'=>$masterproductdata['PriceB'],
                    'product_ordered_percent'=>number_format($this->getOrderedPerCent(array(
                        'flashsalesetproduct_id'=>$value->id,
                        'total_available'=>$value->total_available
                    )),2),
                    'product_remain'=>(int)$this->getProductRemain(array(
                        'flashsalesetproduct_id'=>$value->id,
                        'total_available'=>$value->total_available
                    ))
                );
                array_push($arrReturn, $data_push);

                
                $arrReturn[$key]['product_order_available'] = ((float)$arrReturn[$key]['product_ordered_percent'] >= 100)?false:true;
            }
        }
        return $arrReturn;


    }

    private function getFlashSaleSetAvailable(){
        $flashsaleset = new M_flashsaleset();
        $flashsaleset->where('start_datetime <= ',date('Y-m-d H:i:s'))
        ->where('end_datetime >= ',date('Y-m-d H:i:s'))
        ->where('active',1)
        ->order_by('id','desc')
        ->limit(1)
        ->get();
        return $flashsaleset;
    }
    private function getMasterProductData($mpcode){
        $product = new M_masterproduct();
        $product->where('MPCODE',$mpcode)->get();

        return $product->to_array();
    }
    private function getOrderedPerCent($data = array()){
        $countQuery = $this->ci->db->select('sum(amount) as count_amount')
        ->from('FlashSaleTransactionProduct')
        ->where('FlashSaleSetProduct_id',$data['flashsalesetproduct_id'])
        ->where('active',1)
        ->get();
        $rowcountQuery  = $countQuery->row();
        //echo $rowcountQuery->count_amount;exit;
        if($rowcountQuery->count_amount == 0){
            return 0;
        }else{
            $calculate_percent = (($rowcountQuery->count_amount/$data['total_available'])*100);

            return $calculate_percent;
        }
        
    }

    private function getProductRemain($data = array()){

        $countQuery = $this->ci->db->select('sum(amount) as count_amount')
        ->from('FlashSaleTransactionProduct')
        ->where('FlashSaleSetProduct_id',$data['flashsalesetproduct_id'])
        ->where('active',1)
        ->get();

        $rowcountQuery  = $countQuery->row();

        return $data['total_available']-$rowcountQuery->count_amount;
    }

    private function getProductOrderedByFlashSaleSetProductId($id){
        $countQuery = $this->ci->db->select('sum(amount) as count_amount')
        ->from('FlashSaleTransactionProduct')
        ->where('FlashSaleSetProduct_id',$id)
        ->where('active',1)
        ->get();
        $rowcountQuery  = $countQuery->row();
        return $rowcountQuery->count_amount;
    }

    private function getFlashSaleSetProductDataById($id){
        $flashsalesetproduct = new M_flashsalesetproduct($id);
        return $flashsalesetproduct;
    }

    private function getPurchaseSubTotals($requestCriteria){
        //$masterproduct = $this->getMasterProductData($req)
        $flashsalesetproduct = $this->getFlashSaleSetProductDataById($requestCriteria->product_id);

        $masterproduct = new M_masterproduct();
        $masterproduct->where('MPCODE',$flashsalesetproduct->MPCODE)->get();
        return ($requestCriteria->order_quantity*$masterproduct->PriceB);
    }

    private function checkAgentCodeExist($agent_code = ""){
        $query_check = $this->ci->db->select('AgentCode')
        ->from('Agent')->where('AgentCode',$agent_code)->get();

        if($query_check->num_rows() <= 0){
            return false;
        }else{
            return true;
        }
    }

    private function getFlashSaleTransactionProduct($flashsaletransaction_id){
        $this->ci->load->library(array('Lang_controller'));
        $this->ci->load->helper(array('our','lang'));
        $arrReturn = array();
        $flashsaletransactionproduct = new M_flashsaletransactionproduct();
        $flashsaletransactionproduct->where('FlashSaleTransaction_id',$flashsaletransaction_id)
        ->where('active',1)
        ->get();
        if($flashsaletransactionproduct->result_count() > 0){
            foreach ($flashsaletransactionproduct as $key => $value) {
                # code...
                //echo $value->flashsaletransaction->get()->created;exit;
                
                $query_log = $this->ci->db->select('*')->from('FlashSaleTransactionLogs')
                ->where('FlashSaleTransaction_id',$value->FlashSaleTransaction_id)
                ->where('FlashSaleSetProduct_id',$value->FlashSaleSetProduct_id)->get();

                $arrLogs = array();

                foreach ($query_log->result() as $k => $v) {
                    # code...
                    array_push($arrLogs, array(
                        'transaction_id'=>$v->FlashSaleTransaction_id,
                        'product_id'=>$v->FlashSaleSetProduct_id,
                        'log_type'=>$v->log_type,
                        'log_message'=>json_decode($v->log_message),
                        'created'=>$v->created
                    ));

                }

                $masterproduct = $this->ci->db->select('MPCODE,ProductImage,PriceB')->from('MasterProduct')->where('MPCODE',$value->flashsalesetproduct->get()->MPCODE)->get();

                $rowmasterproduct = $masterproduct->row();

                $data_push = array(
                    'transaction_product_id'=>$value->id,
                    'product_id'=>$value->FlashSaleSetProduct_id,
                    'product_code'=>$value->flashsalesetproduct->get()->MPCODE,
                    'product_name'=>$value->flashsalesetproduct->get()->name,
                    'product_description'=>$value->flashsalesetproduct->get()->description,
                    'product_image_list'=>$this->getFlashSaleSetProductImage(array(
                        'type'=>'portait',
                        'flashsalesetproduct_id'=>$value->FlashSaleSetProduct_id
                    )),
                    'product_image_detail'=>$this->getFlashSaleSetProductImage(array(
                        'type'=>'landscape',
                        'flashsalesetproduct_id'=>$value->FlashSaleSetProduct_id
                    )),
                    'product_total_amount'=>number_format($value->amount,2),
                    'product_total_price'=>number_format((($value->amount)*$rowmasterproduct->PriceB),2),
                    'product_cancel_status'=>($this->checkIsExpiredFlashSale($value->flashsaletransaction->get()->FlashSaleSet_id))?false:true,
                    'product_order_date'=>datetimethai_show($value->flashsaletransaction->get()->created),
                    'product_items'=>$this->getProductStructure(array(
                        'mpcode'=>$rowmasterproduct->MPCODE,
                        'product_total_amount'=>$value->amount
                    )),
                    'product_logs'=>$arrLogs,

                );

                //print_r($data_push);exit;
                array_push($arrReturn, $data_push);

            }
        }
        return $arrReturn;

    }

    private function checkIsExpiredFlashSale($flashsaleset_id){
        $flashsaleset = new M_flashsaleset($flashsaleset_id);

        $now = new DateTime();
        $end_datetime = new DateTime($flashsaleset->end_datetime);

        if(strtotime($now->format('Y-m-d H:i:s')) > strtotime($end_datetime->format('Y-m-d H:i:s'))){
            return true;
        }else{
            return false;
        }

    }

    private function getProductStructure($data = array()){
        $arrReturn = array();
        $query  = $this->ci->db->select('*,PRODUCTSTRUCTURE.MPCODECHILD as product_code')->from('PRODUCTSTRUCTURE')
        ->join('MasterProduct','PRODUCTSTRUCTURE.MPCODECHILD = MasterProduct.MPCODE')
        ->where('PRODUCTSTRUCTURE.MPCODEPARENT',$data['mpcode'])->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $data_push = array(
                    'product_code'=>$value->product_code,
                    'product_name'=>$value->MPNAME,
                    'product_description'=>$value->MPDESCRIPTION,
                    'product_image'=>'http://apifixit.psisat.com/'.$value->ProductImage,
                    'product_total_amount'=>number_format(($value->PSQUANTITY*$data['product_total_amount']),2)
                );

                array_push($arrReturn, $data_push);
            }
        }
        return $arrReturn;


    }

    private function getFlashSaleSetProductImage($data = array()){
        $flashsalesetproduct = new M_flashsalesetproduct($data['flashsalesetproduct_id']);

        $image_url = '';
        switch ($data['type']) {
            case 'landscape':
                if($flashsalesetproduct->image_landscape){
                    $image_url = base_url('uploaded/flashsalesetproduct/landscape/'.$flashsalesetproduct->id.'/'.$flashsalesetproduct->image_landscape);
                }else{
                    $image_url = base_url('uploaded/flashsalesetproduct/no-image-landscape.png');
                }
            break;
            case 'portait':
                if($flashsalesetproduct->image_portait){
                    $image_url = base_url('uploaded/flashsalesetproduct/portait/'.$flashsalesetproduct->id.'/'.$flashsalesetproduct->image_portait);
                }else{
                    $image_url = base_url('uploaded/flashsalesetproduct/no-image-portait.png');
                }
            break;
            
            default:
                $image_url = base_url('uploaded/flashsalesetproduct/no-image-portait.png');
            break;
        }

        return $image_url;


    }

    

}
