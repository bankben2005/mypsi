<?php


class TVApiLibrary{

    private $ci;
    private $default_bitrates;
    private $apiConfig;
    private $lowest_bitrates;
    function __construct() {
        $this->ci =& get_instance();
        $this->ci->load->config('api');
        $this->apiConfig = $this->ci->config->item('tvapis');
        $this->default_bitrates = $this->apiConfig['bitrates'];
        $this->lowest_bitrates = $this->default_bitrates[0]['value'];

    }
    public function getAllChannelList($requestCriteria){

        //print_r($requestCriteria);exit;
        if(property_exists($requestCriteria, 'checkRemakeLogo') && $requestCriteria->checkRemakeLogo == true){
            $this->checkRemakeLogo();
        }

    	$dataReturn = array();
    	/* get all chanels from TVChanels table */
    	$qAllChanels = $this->ci->db->select('*,TVChannels.api_id as chanel_api_id,TVChannels.id as channel_id,TVChannels.name as channel_name,TVChannels.description as channel_description')
    	->from('TVChannels')
        ->where('TVChannels.active',1)
        ->order_by('TVChannels.channel_number','asc')
    	->get();

    	if($qAllChanels->num_rows() > 0){
    		//print_r($qAllChanels->result());
	    	foreach ($qAllChanels->result() as $key => $value) {
	    		# code...
	    		$dataPush = array(
	    			'channel_id'=>$value->channel_id,
	    			'channel_logo'=>$this->getChannelLogo($value->logo),
	    			'channel_name' => $value->channel_name,
	    			'channel_description' => $value->channel_description,
                    'channel_category'=>$this->getChannelCategory($value->channel_id)
	    			// 'channel_category' => $value->category_name,
	    			// 'channel_category_description' => $value->category_description
	    		);
	    		array_push($dataReturn, $dataPush);

	    	}

	    	return array(
	    		'status' => true,
	    		'totals'=>count($dataReturn),
	    		'channels' => $dataReturn,
	    		'result_code' => '000',
	    		'result_desc' => 'success'

	    	);
	    }else{
	    	return array(
	    		'status' => false,
	    		'result_code' => '-002',
	    		'result_desc' => 'ไม่พบข้อมูลช่อง'
	    	);
	    }



    	// print_r($qAllChanels->result());


    }
    public function getChannelDetail($requestCriteria){
        
        //$ads = $this->getTVBottomBanner();


        $bitrates = $this->default_bitrates;
        if(property_exists($requestCriteria, 'bitrates')){
            $bitrates = $requestCriteria->bitrates;
        }

        // print_r($bitrates);exit;
        /* check bitrate available */
        if(!$this->checkBitratesAvailable(array('bitrates'=>$bitrates))){
            return array(
                'status' => false,
                'result_code'=>'-002',
                'result_desc' => 'ไม่พบBitratesดังกล่าว'
            );
        }
        /* eof check bitrate available */

    	$qChannels = $this->ci->db->select('*,TVChannels.id as channel_id,TVChannels.name as channel_name,TVChannels.description as channel_description,TVChannels.active as channel_active')
    	->from('TVChannels')
        ->where('TVChannels.active',1)
    	->where('TVChannels.id',$requestCriteria->channel_id)
    	->get();

    	if($qChannels->num_rows() > 0){

            /* check if this channels has been disable by active status */
            if(!$qChannels->row()->channel_active){
                return array(
                    'status' => false,
                    'result_code'=>'-009',
                    'result_desc'=>'ช่องรายการดังกล่าวถูกยกเลิกไว้ในขณะนี้'
                );

            }
            /* eof check channels active */


    		/* before get channel detail, insert data to tvchannelviews */
    		$dataInsertTvChannelViews = array(
    			'TVChannels_id'=>$requestCriteria->channel_id,
    			'client_ip'=>$this->getUserIP(),
    			'ConsumerID'=>$requestCriteria->consumer_id,
    			'created'=>date('Y-m-d H:i:s')
    		);


    		if($this->ci->db->insert('TVChannelViews',$dataInsertTvChannelViews)){
    			$channel_data = $qChannels->row();


                //============= insert to tvaudiencerecent ==============
                $this->updateTvAudienceView(array('consumer_id'=>$requestCriteria->consumer_id,'channel_id'=>$requestCriteria->channel_id));
                //============= eof insert to tvaudience recent =========

                $prefix_tv_server = $this->getPrefixTVServer($channel_data->nodes_host);
    			// print_r($channel_data);
    			$arReturn = array(
                    'channel_id' => $channel_data->channel_id,
                    'channel_logo'=>$this->getChannelLogo($channel_data->logo),
                    //'channel_url'=> ($channel_data->channel_id == 1)?'http://96.30.124.232:1935/EDGE_T4/smil:PsiHD_ABR.smil/playlist.m3u8':$prefix_tv_server.$this->getServerLoadBalance($channel_data->nodes_host).':1935/'.$channel_data->nodes_folder.'/'.$channel_data->streamer.'_'.$bitrates.'/playlist.m3u8', 
                    'channel_url'=>$this->getChannelUrl($channel_data),
                    'channel_name' => $channel_data->channel_name,
                    'channel_description' => $channel_data->channel_description,
                    'channel_category'=>$this->getChannelCategory($channel_data->channel_id),
                    'channel_views'=>$this->getChannelView($channel_data->channel_id),
                    'is_favourite'=>$this->checkFavouriteChannel(array('consumer_id'=>$requestCriteria->consumer_id,'channel_id'=>$requestCriteria->channel_id))
                );

                return array(
                    'status'=>true,
                    'channel'=>$arReturn,
                    'ads' => $this->getTVBottomBanner(),
                    // 'ads'=>$this->getDefaultBanner(),
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );
    			
                // print_r($arReturn);

    		}else{
    			return array(
    				'status' => false,
    				'result_code' => '-003',
    				'result_desc' => 'เกิดข้อผิดพลาดขณะบันทึกข้อมูล กรุณาลองใหม่อีกครั้ง'
    			);
    		}

    		/* */
    		//print_r($qChannels->row());
    	}else{
    		return array(
    			'status' => false,
    			'result_code' => '-002',
    			'result_desc' => 'ไม่พบข้อมูลช่องนี้ในระบบ'
    		);
    	}

    }
    public function updateChannelList(){
        //print_r('aaaa');
    	$this->curlGetChannelByApi();

    }
    public function togglesFavouriteChannel($requestCriteria){
        
        /* check channel */
        $queryCheckChannel = $this->ci->db->select('*')
        ->from('TVChannels')
        ->where('id',$requestCriteria->channel_id)
        ->get();

        if($queryCheckChannel->num_rows() <= 0){
            return array(
                'status' => false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบข้อมูลช่องนี้ในระบบ'
            );
        }
        /* eof check channel*/


        /* first check is this consumer id favourite this channel */
        $queryCheck = $this->ci->db->select('*')
        ->from('TVAudienceFavourite')
        ->where('ConsumerID',$requestCriteria->consumer_id)
        ->where('TVChannels_id',$requestCriteria->channel_id)
        ->get();

        if($queryCheck->num_rows() > 0){
            $row = $queryCheck->row();
            /* if already has then delete it*/
            $queryDelete = $this->ci->db->delete('TVAudienceFavourite',array('id'=>$row->id));
            if($queryDelete){
                return array(
                    'status'=>true,
                    'result_code' => '000',
                    'result_desc' => 'success'
                );
            }else{
                return array(
                    'status' => false,
                    'result_code' => '-004',
                    'result_desc' => 'เกิดข้อผิดพลาดขณะทำการอัพเดทข้อมูล'
                );

            }
        }else{
            /* if no have insert to record*/
            $queryInsert = $this->ci->db->insert('TVAudienceFavourite',array(
                'ConsumerID' => $requestCriteria->consumer_id,
                'TVChannels_id' =>$requestCriteria->channel_id,
                'Created'=>date('Y-m-d H:i:s')
            ));

            if($queryInsert){
                return array(
                    'status'=>true,
                    'result_code' => '000',
                    'result_desc' => 'success'
                );
            }else{
                return array(
                    'status'=>false,
                    'result_code'=>'-003',
                    'result_desc'=>'เกิดข้อผิดพลาดในขณะที่บันทึกข้อมูล'
                );

            }

        }



    }
    public function getFavouriteChannel($requestCriteria){
        $arChannels = array();
        $queryFavourite = $this->ci->db->select('*')
        ->from('TVAudienceFavourite')
        ->join('TVChannels','TVAudienceFavourite.TVChannels_id = TVChannels.id')
        ->where('TVAudienceFavourite.ConsumerID',$requestCriteria->consumer_id)
        ->order_by('TVAudienceFavourite.created','desc')
        ->get();

        if($queryFavourite->num_rows() > 0){
           

            foreach ($queryFavourite->result() as $key => $value) {
                # code...
                $dataPush = array(
                    'channel_id'=>$value->id,
                    'channel_logo'=>$value->logo,
                    'channel_name'=>$value->name,
                    'channel_description'=>$value->description
                );

                array_push($arChannels, $dataPush);


            }
            return array(
                'status' => true,
                'favourite_channels'=>$arChannels,
                'result_code'=>'000',
                'result_desc'=>'Success'

            );

        }else{
            return array(
                'status' => true,
                'favourite_channels'=>$arChannels,
                'result_code'=>'000',
                'result_desc'=>'Success'

            );

        }
    }
    public function getRecentChannelView($requestCriteria){
        $arChannels = array();
        $queryRecent = $this->ci->db->select('*')
        ->from('TVAudienceRecentView')
        ->where('ConsumerID',$requestCriteria->consumer_id)
        ->get();

        if($queryRecent->num_rows() > 0){
            $row = $queryRecent->row();

            $recent_view = json_decode($row->recent_view);
            
            krsort($recent_view);
            
            foreach ($recent_view as $key => $value) {
                # code...
                $query = $this->ci->db->select('*')
                ->from('TVChannels')
                ->where('id',$value)
                ->where('active',1)
                ->get();

                //echo $this->ci->db->last_query();

                if($query->num_rows() > 0){
                    $row_channels = $query->row();

                    $arChannelPush = array(
                        'channel_id' => $row_channels->id,
                        'channel_logo'=> $this->getChannelLogo($row_channels->logo),
                        'channel_name'=>$row_channels->name,
                        'channel_description'=>$row_channels->description
                    );
                    array_push($arChannels, $arChannelPush);
                }
            }

            return array(
                'status' => true,
                'recent_channels'=>$arChannels,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );

        }else{

            return array(
                'status' => true,
                'recent_channels'=>$arChannels,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
        }
        

    }

    public function getChannelCategories($requestCriteria){
        $arCategories = array();
        $query = $this->ci->db->select('*')
        ->from('TVChannelCategories')
        ->where('active',1)
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                array_push($arCategories, array(
                    'id'=>$value->id,
                    'name'=>$value->name,
                    'description'=>$value->description
                ));
            }
            return array(
                'status' => true,
                'categories'=>$arCategories,
                'result_code' => '000',
                'result_desc' => 'Success'

            );
        }else{

            return array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบข้อมูลหมวดหมู่'
            );
        }
    }

    public function getChannelByCategory($requestCriteria){

        $arReturn = array();
        $query = $this->ci->db->select('*')
        ->from('TVChannelCategoryLists')
        ->join('TVChannels','TVChannelCategoryLists.TVChannels_id = TVChannels.id')
        ->where('TVChannelCategoryLists.TVChannelCategories_id',$requestCriteria->category_id)
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                array_push($arReturn, array(
                    'channel_id'=>$value->TVChannels_id,
                    'channel_logo'=>$value->logo,
                    'channel_name'=>$value->name,
                    'channel_description'=>$value->description,
                    'channel_category'=>$this->getChannelCategory($value->TVChannels_id)
                ));
            }
            return array(
                'status'=>true,
                'totals'=>count($arReturn),
                'channels'=>$arReturn,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );

        }else{

            return array(
                'status'=>true,
                'totals'=>count($arReturn),
                'channels'=>$arReturn,
                'result_code'=>'000',
                'result_desc'=>'Success'
            );
        }

    }

    public function setTVBannerClick($requestCriteria){
        /* save data to tv advertisment banner click */
            $dataInsert = array(
                'TVAdvertismentBanners_id'=>$requestCriteria->banner_id,
                'ConsumerID' => $requestCriteria->consumer_id,
                'client_ip' => $this->getUserIP(),
                'created' => date('Y-m-d H:i:s')
            );

            if($this->ci->db->insert('TVAdvertismentBannerClicks',$dataInsert)){
                return array(
                    'status' => true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );

            }else{
                return array(
                    'status' => false,
                    'result_code' => '-003',
                    'result_desc' => 'ไม่สามารถเพิ่มข้อมูลได้'

                );
            }
        /* eof tv advertisment banner click */
    }

    public function getS3TVChannelList($requestCriteria){
        // print_r($requestCriteria);

        $arReturn = array();

        $query = $this->ci->db->select('*')
        ->from('TVChannels')
        ->where('s3_status',1)->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                array_push($arReturn, $value);
                $arReturn[$key]->{'logo_path'} = $this->getChannelLogo($value->logo);
            }

        }
        return array(
            'status'=>true,
            'ChannelList'=>$arReturn,
        );

    }

    private function curlGetChannelByApi(){
                $count_response_channels = 0;
                $count_db_channels = 0;
    			$queryChanels = $this->ci->db->select('*')
    			->from('TVChannels')
    			->get();

    			$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->apiConfig['get_channel']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                $response = json_decode($response);

                $log_file_path = $this->createLogFilePath('RequestUpdateChannel');
                $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode(array()) . "\n";
                file_put_contents($log_file_path, $file_content, FILE_APPEND);
                unset($file_content);

                foreach ($response->result->channels as $key => $value) {
                	foreach ($queryChanels->result() as $k => $v) {
                		if($value->id == $v->api_id && strtotime($value->updated_date) != strtotime($v->updated)){
                			$dataUpdate = array(
                				'logo' => $value->logo,
                                'streamer' => $value->streamer,
                                'ordinal'=>$value->ordinal,
                                'channel_number' => $value->channel_number[0],
                				'name' => $value->name,
	    						'description' => $value->description,
                                'nodes_host'=>$value->nodes[0]->host,
                                'nodes_folder'=>$value->nodes[0]->folder,
                                'country_iso2'=>$value->country,
                                'updated'=>$value->updated_date
                			);

                            if($this->ci->db->update('TVChannels',$dataUpdate,array('id'=>$v->id))){

                                /* write some logs */
                            $log_file_path = $this->createLogFilePath('UpdateChannel');
                                $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($dataUpdate) . "\n";
                                file_put_contents($log_file_path, $file_content, FILE_APPEND);
                                unset($file_content);
                            }

                		}
                	}

                }

                $count_response_channels = count($response->result->channels);
                $count_db_channels = $this->countTVChannels();

                if($count_response_channels > $count_db_channels){
                    /* add channel */
                    $channel_toadd = $this->getChannelToAdd($response->result->channels);

                }else{
                    /* disable some channel*/
                }
              
    }

    private function createLogFilePath($filename = '') {
        $log_path = './logs/updatetvchannels';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function getUserIP(){
                       $client  = @$_SERVER['HTTP_CLIENT_IP'];
                            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                            $remote  = $_SERVER['REMOTE_ADDR'];

                            if(filter_var($client, FILTER_VALIDATE_IP))
                            {
                                $ip = $client;
                            }
                            elseif(filter_var($forward, FILTER_VALIDATE_IP))
                            {
                                $ip = $forward;
                            }
                            else
                            {
                                $ip = $remote;
                            }

                            return $ip;
    }

    private function updateTvAudienceView($data = array()){
        $query = $this->ci->db->select('*')
        ->from('TVAudienceRecentView')
        ->where('ConsumerID',$data['consumer_id'])
        ->get();

        if($query->num_rows() > 0){
            $row = $query->row();
            $recent_view = json_decode($row->recent_view);

            /* check total recent view */
            $count_recent_view = count($recent_view);

            if($count_recent_view < 12){
                /* check this channel already in recent */
                if(!(in_array($data['channel_id'], $recent_view))){
                    array_push($recent_view, $data['channel_id']);

                    $data_update = array(
                        'recent_view'=>json_encode($recent_view),
                        'updated' => date('Y-m-d H:i:s')
                    );
                    $this->ci->db->update('TVAudienceRecentView',$data_update,array('id'=>$row->id));

                    // print_r($recent_view);
                }
                
            }else if($count_recent_view == 12){
                //$row = $query->row();

                $recent_view = json_decode($row->recent_view);

                if(!(in_array($data['channel_id'], $recent_view))){
                    /* first remove first key of array */
                    unset($recent_view[0]);

                    array_push($recent_view, $data['channel_id']);

                    $recent_arrange = array();
                    /* new arrange array*/

                    // print_r($recent_view);
                    foreach($recent_view as $key => $value) {
                        # code...
                        array_push($recent_arrange, $value);
                    }
                    $data_update = array(
                        'recent_view'=>json_encode($recent_arrange),
                        'updated' => date('Y-m-d H:i:s')
                    );
                    $this->ci->db->update('TVAudienceRecentView',$data_update,array('id'=>$row->id));
                }

            }

        }else{

            /* insert new record to tvaudiencerecentview */
            $data_insert = array(
                'ConsumerID' => $data['consumer_id'],
                'recent_view' => json_encode(array($data['channel_id'])),
                'created' => date('Y-m-d H:i:s')
            );
            $this->ci->db->insert('TVAudienceRecentView',$data_insert);


        }

    }

    private function checkFavouriteChannel($data = array()){
        // print_r($data);exit;
        $querycheck = $this->ci->db->select('*')
        ->from('TVAudienceFavourite')
        ->where('ConsumerID',$data['consumer_id'])
        ->where('TVChannels_id',$data['channel_id'])
        ->get();
        if($querycheck->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }

    private function getPrefixTVServer($server_name = ""){
        if(!(preg_match("@^http://@i",$server_name)) && !(preg_match("@^https://@i",$server_name))){
            return 'http://';
        }

    }

    private function checkBitratesAvailable($data = array()){

        $key = null;
        $key = array_search($data['bitrates'], array_column($this->default_bitrates, 'value'));
        //echo 'key ='.$key;exit;
        if(is_numeric($key)){
            return true;
        }else{
            // echo 'aaa';exit;
            return false;
        }

    }

    private function getServerLoadBalance($nodes_host){
        $html = file_get_contents('http://'.$nodes_host);

        //print_r($html);exit;
        $serverName = "";
        $serverExplode = explode('=', $html);
        $serverName = $serverExplode[1];
        return $serverName;

    }

    private function getChannelCategory($channel_id = 0){
        $arReturn = array();
        $query = $this->ci->db->select('*')
        ->from('TVChannelCategoryLists')
        ->join('TVChannelCategories','TVChannelCategoryLists.TVChannelCategories_id = TVChannelCategories.id')
        ->where('TVChannelCategoryLists.TVChannels_id',$channel_id)
        ->get();
        //print_r($query->result());exit;
        foreach ($query->result() as $key => $value) {
            # code...
            array_push($arReturn, array(
                'id'=>$value->TVChannelCategories_id,
                'name'=>$value->name,
                'description'=>$value->description
            ));
        }
        return $arReturn;

    }
    private function getChannelView($channel_id = 0){
        $subdate = new DateTime();
        $subdate->sub(new DateInterval('PT15M'));

        // print_r($subdate);
        $query = $this->ci->db->select('count(ConsumerID) as count_view')
        ->from('TVChannelViews')
        ->where('TVChannels_id',$channel_id)
        ->where('CAST(TVChannelViews.created AS Datetime) >= ',$subdate->format('Y-m-d H:i:s'))
        ->group_by('ConsumerID')
        ->get();
        
        return $query->num_rows();



    }

    private function getDefaultBanner(){
        return array(
            array(
                'id'=>'1000',
                'name'=>'default1',
                'description'=>'default1',
                'image'=>base_url('tv/uploaded/banner/default1/default1.jpg'),
                'url'=>'http://psisat.com'
            ),
            // array(
            //     'id'=>'1001',
            //     'name'=>'default1',
            //     'description'=>'default2',
            //     'image'=>base_url('tv/uploaded/banner/default2/default2.jpg'),
            //     'url'=>'http://psisat.com'
            // )

        );
    }

    private function getTVBottomBanner(){
        $arBanner = array();
        /* find banner which zone has width 320x100 */
        $query  = $this->ci->db->select('*,TVAdvertismentBanners.id as banner_id,TVAdvertismentBanners.name as banner_name,TVAdvertismentBanners.description as banner_description')
        ->from('TVAdvertismentBanners')
        ->join('TVAdvertismentZones','TVAdvertismentBanners.TVAdvertismentZones_id = TVAdvertismentZones.id')
        ->where('TVAdvertismentZones.width_spec','320')
        ->where('TVAdvertismentZones.height_spec','100')
        ->where('TVAdvertismentBanners.active',1)
        ->get();
       // echo $this->ci->db->last_query();exit;

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                array_push($arBanner,array(
                    'banner_id' => $value->banner_id,
                    'name'=>$value->banner_name,
                    'description'=>$value->banner_description,
                    'image'=>base_url('tv/uploaded/banner/'.$value->banner_id.'/'.$value->banner),
                    'url'=>$value->url

                ));
            }

            $key_random = array_rand($arBanner,1);

            return array($arBanner[$key_random]);

        }else{
            return $this->getDefaultBanner();
        }

    }

    private function countTVChannels(){
        $query = $this->ci->db->select('count(id) as count_channels')
        ->from('TVChannels')
        ->get();
        $row = $query->row();
        return $row->count_channels;
    }

    private function getChannelToAdd($api_channels){
        $arApiChannels = array();
        $arDBChannels = array();
        $query = $this->ci->db->select('api_id')->from('TVChannels')->get();

        foreach ($api_channels as $key => $value) {
            # code...
            array_push($arApiChannels, $value->id);
        }

        foreach ($query->result() as $key => $value) {
            # code...
            array_push($arDBChannels, $value->api_id);
        }
            $arResultDiff = array();
            $result_diff = array_diff($arApiChannels, $arDBChannels);
            
            if(count($result_diff) > 0){
                foreach ($result_diff as $key => $value) {
                    # code...
                    array_push($arResultDiff, $value);
                }
            }
        $ar_api_channel = json_encode($api_channels);
        $ar_api_channel = json_decode($ar_api_channel,true);

        //print_r($arResultDiff);exit;

        foreach ($arResultDiff as $key => $value) {
            # code...
            $key = array_search($value, array_column($ar_api_channel, 'id'));
            $data = $ar_api_channel[$key];

                    $dataInsert = array(
                        'api_id'=>$data['id'],
                        'channel_number'=>$data['channel_number'][0],
                        'ordinal'=>$data['ordinal'],
                        'streamer'=>$data['streamer'],
                        'nodes_host'=>$data['nodes'][0]['host'],
                        'nodes_folder'=>$data['nodes'][0]['folder'],
                        'name'=>$data['name'],
                        'description'=>$data['description'],
                        'logo'=>$data['logo'],
                        'country_iso2'=>$data['country'],
                        'created'=>$data['created_date'],
                        'updated'=>$data['updated_date']
                    );

                    //print_r($dataInsert);exit;
                    $this->ci->db->insert('TVChannels',$dataInsert);
                    //echo $this->ci->db->last_query();exit;

                    $insertCategoryList =array(
                        'TVChannels_id'=>$this->ci->db->insert_id(),
                        'TVChannelCategories_id'=>'1',
                        'created'=> date('Y-m-d H:i:s')
                    );
                    $this->ci->db->insert('TVChannelCategoryLists',$insertCategoryList);

                    //print_r($dataInsert);
            
        }
        

    }

    private function getChannelLogo($image_logo){
        switch (ENVIRONMENT) {
            case 'development':
                // return 'http://psi.development/uploaded/tv/logo/'.$image_logo.'?'.time();
            return 'http://psi.development/uploaded/tv/logo/'.$image_logo;

            break;
            case 'testing':

            break;
            case 'production':
                return 'http://apiservice.psisat.com/uploaded/tv/logo/'.$image_logo.'?'.time();
            // return 'http://apiservice.psisat.com/uploaded/tv/logo/'.$image_logo;
            break;
            
            default:
                # code...
            break;
        }
    }

    private function checkRemakeLogo(){
        $query = $this->ci->db->select('*')
        ->from('TVChannels')->get();

        foreach ($query->result() as $key => $value) {
            # code...
            //print_r($value);
            $exLogo = explode('?', $value->logo);
            if(count($exLogo) > 0){
                $exLogoUrl = explode('/', $exLogo[0]);
                //print_r($exLogoUrl);
                if(count($exLogoUrl) > 0){
                    $image_name = $exLogoUrl[5];
                    $this->ci->db->update('TVChannels',array('logo'=>$image_name),array('id'=>$value->id));
                }

            }
        }

        return true;
    }

    private function getMVChannelLinkByLinkName($link_name){

        return 'http://96.30.124.109:4040/api/Product?linkname='.$link_name.'&boxuser=psifixit&hr=1';

    }

    private function getChannelUrl($channel_row){
            if($channel_row->mv_status){
                /* get from mv*/

                /* if record already has link temp then check link expired */
                //-- if expired request for new link and if not yet return  link temp
                if($channel_row->mv_streaming_temp){

                    /* check streaming expired */
                    $get_querystring = parse_url($channel_row->mv_streaming_temp, PHP_URL_QUERY);
                    parse_str($get_querystring, $parseQueryString);

                    //print_r($parseQueryString);exit;

                    if(@$parseQueryString['doostreamendtime'] > strtotime(date('Y-m-d H:i:s')) || @$parseQueryString['psiliveendtime'] > strtotime(date('Y-m-d H:i:s'))){
                        return $channel_row->mv_streaming_temp;
                    }else{

                        $link_streaming = file_get_contents($this->getMVChannelLinkByLinkName($channel_row->mv_request_streaming));

                        $link_streaming = str_replace('"', '', $link_streaming); 
                        //echo $link_streaming;exit;
                        $this->ci->db->update('tvchannels',array(
                            'mv_streaming_temp'=>$link_streaming
                        ),array('id'=>$channel_row->id));

                        return $link_streaming;

                    }
                    


                }else{
                    //--- if no mv streaming temp request and then update to record 
                    $link_streaming = file_get_contents($this->getMVChannelLinkByLinkName($channel_row->mv_request_streaming));

                    //echo '****'$link_streaming;exit;

                    //echo $link_streaming;exit;

                    $link_streaming = str_replace('"', '', $link_streaming); 
                    //echo $link_streaming;exit;
                    //echo $link_streaming;exit;
                    $this->ci->db->update('tvchannels',array(
                        'mv_streaming_temp'=>$link_streaming
                    ),array('id'=>$channel_row->id));

                    //echo $this->ci->db->last_query();exit;

                    return $link_streaming;
                }



            }else if($channel_row->direct_streaming_status){

                return $channel_row->direct_streaming_link;
            }else{
                /* get from symphony*/
                $bitrates = '300';
                $prefix_tv_server = $this->getPrefixTVServer($channel_row->nodes_host);
                return $prefix_tv_server.$this->getServerLoadBalance($channel_row->nodes_host).':1935/'.$channel_row->nodes_folder.'/'.$channel_row->streamer.'_'.$bitrates.'/playlist.m3u8';
            }

    }
}