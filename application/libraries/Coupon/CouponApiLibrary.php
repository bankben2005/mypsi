<?php


class CouponApiLibrary{

    private $ci;
    private $psiarm_config = array();
    function __construct() {
        $CI =& get_instance();
        $this->ci = $CI;
       // $CI->load->config('agent');
        //$CI->load->config('api');
        $CI->load->config('api');
        $this->psiarm_config = $this->ci->config->item('psiarm');

       // $this->apiConfigPWT = $CI->config->item('water_filter');
        $this->onesignalConfig  = $CI->config->item('onesignal_psiarm');
       
    }


    //========================== This is for pos system =======================
    //=========================================================================
    public function checkCouponAgentAvailable($requestCriteria){
        // print_r($requestCriteria);exit;
        $dataReturn = array();
        $dataCouponReturn = array();
        $query = $this->ci->db->select('*,CouponAgent.id as couponagentid,Coupon.Name as CouponName,CouponAgent.Created as createcouponagent')
        ->from('Coupon')
        ->join('CouponAgent','Coupon.id = CouponAgent.Coupon_id')
        ->join('CouponType','Coupon.CouponType_id = CouponType.id')
        ->where('CouponAgent.AgentCode',$requestCriteria->agentcode)
        ->where('CouponAgent.Code',$requestCriteria->couponcode)
        ->get();

       

        if($query->num_rows() > 0){

                $row = $query->row();
                $expireDate = strtotime($row->EndDate);
                /* Check Coupon Expired */
                if($expireDate < strtotime(date('Y-m-d'))){
                    return array(
                        'status' => false,
                        'result_code' => '-008',
                        'result_desc' => 'คูปองหมดอายุแล้ว'

                    );
                }else if(strtotime($row->StartDate) > strtotime(date('Y-m-d'))){
                    return array(
                        'status' => false,
                        'result_code' => '-007',
                        'result_desc' => 'คูปองนี้กำลังจะเปิดใช้เร็วๆนี้'

                    );
                }

                /* Check Coupon Used */
                if($row->Used){
                    return array(
                        'status' => false,
                        'result_code' => '-006',
                        'result_desc' => 'คูปองได้ถูกใช้งานแล้ว'
                    );

                }



            
                # code...
                $row->{'StartDate'} = date('Y-m-d',strtotime($row->StartDate));
                $row->{'EndDate'} = date('Y-m-d',strtotime($row->EndDate));
                $row->{'createcouponagent'} = date('Y-m-d H:i:s',strtotime($row->createcouponagent));

                $dataCouponReturn = array(
                    'CouponAgentId' => $row->couponagentid,
                    'Code' => $row->Code,
                    'CouponName' => $row->CouponName,
                    'DiscountRate' => $row->DiscountRate,
                    'DiscountType' => $row->DiscountType,
                    'DiscountCurrency' => $row->DiscountCurrency,
                    'StartDate' => $row->StartDate,
                    'EndDate' => $row->EndDate,
                    'CouponType' => $row->Name,
                    'Created' => $row->createcouponagent,
                    'Active' => $row->Active,
                    'Used' => $row->Used

                );

           

            $dataReturn = array(
                'status' => true,
                'coupon_data' => $dataCouponReturn,
                'result_code' => '000',
                'result_desc' => 'Success'

            );
            return $dataReturn;
        }else{
            return array(
                'status' => false,
                'result_code' => '-002',
                'result_desc' => 'ไม่พบข้อมูลคูปอง'

            );
        }





    }

    public function setCouponAgentUsed($requestCriteria){
        //print_r($requestCriteria);exit;
        /* check id of couponagent id */
        $query = $this->ci->db->select('*')
        ->from('CouponAgent')
        ->join('Coupon','CouponAgent.Coupon_id = Coupon.id')
        ->where('CouponAgent.id',$requestCriteria->couponagentid)
        ->get();

        if($query->num_rows() > 0){
            $row = $query->row();
            //print_r($row);exit;

            if($row->Used){
                return array(
                    'status' => false,
                    'result_code' => '-006',
                    'result_desc' => 'คูปองได้ถูกใช้งานแล้ว'
                );
            }

            /* update row to used */
            $update_query = $this->ci->db->update('CouponAgent',array('Used'=>1),array('id'=>$requestCriteria->couponagentid));
            if($update_query){

                /* Send notification */

                $systemuser_token = $this->ci->db->select('AgentCode,device_token')
                ->from('SystemUser')
                ->where('AgentCode',$row->AgentCode)
                ->get();

                if($systemuser_token->num_rows() > 0){

                            $txt_notification = $this->getTxtCouponUsedNotification(array('couponagentdata'=>$row,'suname'=>$requestCriteria->suname));

                            $content = array(
                                'en' => $txt_notification
                            );

                            $fields = array(
                                'app_id' => $this->onesignalConfig['app_id'],
                                'include_player_ids' => array($systemuser_token->row()->device_token),
                                'data' => array("jobstatus" => "CouponUsed"),
                                'contents' => $content
                            );

                            
                            $post_data = json_encode($fields);
                            
                            $response = json_decode($this->onesignalCurlNotification($post_data));

                            if($response->recipients > 0){


                            }

                }

                /* Eof Send notification */


                /* then set couponagentlog */
                $data_insert = array(
                    'CouponAgent_id' => $requestCriteria->couponagentid,
                    'LogMessage' => json_encode(array(
                        'type' => 'used coupon',
                        'use_by' => $requestCriteria->suname
                    )),
                    'Created' => date('Y-m-d H:i:s')

                );

                $this->ci->db->insert('CouponAgentLogs',$data_insert);

                /* eof set couponagentlog */
                return array(
                    'status' => true,
                    'result_code' => '000',
                    'result_desc' => 'Success'

                );
            }else{
                return array(
                    'status' => false,
                    'result_code' => '-004',
                    'result_desc' => 'เกิดข้อผิดพลาดในการ'

                );
            }


        }else{
            return array(
                'status' => false,
                'result_code' => '-002',
                'result_desc' => 'ไม่พบข้อมูลคูปองของช่างดังกล่าว'
            );
        }

    }

    public function getRedeemCoupon($requestCriteria){
            $arReturn = array();
            $query = $this->ci->db->select('*,Coupon.id as coupon_id,Coupon.name as coupon_name,CouponType.id as coupontype_id')
            ->from('Coupon')
            ->join('CouponType','Coupon.CouponType_id = CouponType.id')
            ->where('CouponType.name','Redeem')
            ->where('Coupon.PointUse !=',null)
            ->where('Coupon.active',1)
            ->order_by('Coupon.id','asc')
            ->get();

            if($query->num_rows() > 0){
                
                foreach ($query->result() as $key => $value) {
                    # code...
                    array_push($arReturn, array(
                        'CouponId'=>$value->coupon_id,
                        'CouponName'=>$value->coupon_name,
                        'CouponDescription'=>$value->Description,
                        'DiscountRate'=>$value->DiscountRate,
                        'DiscountType'=>$value->DiscountType,
                        'DiscountCurrency'=>$value->DiscountCurrency,
                        'PointUse'=>$value->PointUse,
                        'Image'=>base_url('uploaded/coupon/coupon_image/'.$value->coupon_id.'/'.$value->Image)
                    ));
                }
                return array(
                    'status'=>true,
                    'CouponLists'=>$arReturn,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );
                //return $arReturn;
                //print_r($arReturn);exit;
                
            }else{
                return array(
                    'status'=>true,
                    'result_code'=>'000',
                    'result_desc'=>'Success'
                );
            }
        

        
    }
    public function setRedeemPointToCoupon($requestCriteria){

        

        //print_r($redeem_config);exit;
        /* check point available */
        $checkPoinAvailable = $this->checkPoinAvailable($requestCriteria);
        if(!$checkPoinAvailable['status']){
            switch ($checkPoinAvailable['flag']) {
                case 'not-enough-point':
                    return array(
                        'status'=>false,
                        'result_code'=>'-009',
                        'result_desc'=>'แต้มของคุณเหลือไม่เพียงพอสำหรับแลก'
                    );

                case 'not-found-agent':
                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'ไม่พบข้อมูลช่าง'
                    );
                break;
                case 'not-found-coupon':
                    return array(
                        'status'=>false,
                        'result_code'=>'-002',
                        'result_desc'=>'ไม่พบข้อมูลคูปอง'
                    );
                break;
                
                default:
                    return array(
                        'status'=>false,
                        'result_code'=>'-009',
                        'result_desc'=>'แต้มของคุณเหลือไม่เพียงพอสำหรับแลก'
                    );
                break;
            }
            
        }

        /* create coupon agent */


        // calculate expiredate by coupon 
        //$expiredate = $this->calculateExpireDate();


        $couponCodeGenerated = $this->generateCouponCode();
        $insertCouponAgent = array(
            'Coupon_id'=>$checkPoinAvailable['coupon']->id,
            'Code'=>$couponCodeGenerated,
            'AgentCode'=>$checkPoinAvailable['agent']->AgentCode,
            'Created'=>date('Y-m-d H:i:s'),
            'ExpireDate'=>$this->calculateExpireDate()
        );
            if($this->ci->db->insert('CouponAgent',$insertCouponAgent)){
                /* update coupon point to agent table */
                $updateAgentPoint = array(
                    'Point' => $checkPoinAvailable['agent']->Point - $checkPoinAvailable['coupon']->PointUse
                );
                if($this->ci->db->update('Agent',$updateAgentPoint,array('AgentCode'=>$checkPoinAvailable['agent']->AgentCode))){
                    return array(
                        'status'=>true,
                        'UsedPoint'=>$checkPoinAvailable['coupon']->PointUse,
                        'RemainPoint'=>$updateAgentPoint['Point'],
                        'result_code'=>'000',
                        'result_desc'=>"ทำการแลกแต้มเรียบร้อย \r\n แต้มของคุณคงเหลือหลังจากแลก ".number_format($updateAgentPoint['Point'])." แต้ม"
                    );

                }else{
                    return array(
                        'status'=>false,
                        'result_code'=>'-004',
                        'result_desc'=>'ไม่สามารถอัพเดทข้อมูลแต้มของช่างได้'
                    );
                }

            }else{
                return array(
                    'status'=>false,
                    'result_code'=>'-003',
                    'result_desc'=>'ไม่สามารถบันทึกข้อมูลคูปองสำหรับช่างได้'
                );
            }
        //print_r($couponCodeGenerated);exit;

        /* eof create coupon agent */

        //print_r($checkPoinAvailable);exit;
    }


    private function getTxtCouponUsedNotification($data = array()){

        //print_r($data);exit;
        $txt_return = "คูปองของคุณโค๊ด ".$data['couponagentdata']->Code." \r\n";
        $txt_return .= "ได้ถูกใช้งานแล้วกับ ";

        /* get suname data */
        $querySuname = $this->ci->db->select('AgentCode,AgentName,AgentSurName,TradeName,AgentType')
        ->from('Agent')
        ->where('AgentCode',$data['suname'])
        ->get();

        if($querySuname->num_rows() > 0){
            $rowSuname = $querySuname->row();
            switch ($rowSuname->AgentType) {
                case 'AD':
                    $txt_return .= $rowSuname->TradeName.' '.$rowSuname->AgentName." \r\n";
                break;
                    $txt_return .= $rowSuname->AgentName.' '.$rowSuname->AgentSurName." \r\n";
                default:
                    # code...
                    break;
            }
        }else{
            $txt_return .= "ไม่ระบุ \r\n";

        }

        /* eof get suname data */

        $txt_return .= "หากคุณไม่ได้ทำการใช้งานคูปอง กรุณาติดต่อ Call Center 1247";

        return $txt_return;

    }



    private function onesignalCurlNotification($post_data){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->onesignalConfig['url']);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->onesignalConfig['header']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

               
                return $response;

    }



    //========================== This is for mobile ===========================
    //=========================================================================

    public function getCouponAgentList($requestCriteria){

       //print_r($requestCriteria);exit;
       $is_active = true;
       $active = "all";
       if(property_exists($requestCriteria, 'is_active')){
            $is_active = $requestCriteria->is_active;
            if($is_active){
                    $active = "true";
            }else{
                    $active = "false";
            }
       }
       $couponData = array();

       $query = $this->ci->db->select('*,CouponAgent.id as couponagent_id,CouponAgent.Code as couponagent_code,Coupon.name as coupon_name,CouponType.name as coupontype_name')
       ->from('CouponAgent')
       ->join('Coupon','CouponAgent.Coupon_id = Coupon.id')
       ->join('CouponType','Coupon.CouponType_id = CouponType.id')
       ->where('CouponAgent.AgentCode',$requestCriteria->agentcode)
       ->get();
       //echo $this->ci->db->last_query();

       //echo $query->num_rows();exit;

       if($query->num_rows() > 0){
            $result = $query->result();
            $coupon_code = "";
            foreach ($result as $key => $row) {
                # code...
                //print_r($row);
                $dataImageStatus = ($row->coupontype_name == 'Redeem')?$this->getCouponListImageStatusRedeem($row):$this->getCouponListImageStatus($row);

                //print_r($dataImageStatus);
                $coupon_code = ($row->IsGenerateCode)?$row->couponagent_code:$row->Code;
                if($active == "true"){
                        if($dataImageStatus['status'] == 'Available'){
                            
                            array_push($couponData, array(
                                'CouponId' => $row->Coupon_id,
                                'CouponAgentId' => $row->couponagent_id,
                                'CouponCode' => $coupon_code,
                                'CouponName' => $row->coupon_name,
                                'CouponDescription' => $row->Description,
                                'DiscountRate' => $row->DiscountRate,
                                'DiscountType' => $row->DiscountType,
                                'DiscountCurrency' => $row->DiscountCurrency,
                                'StartDate' => date('Y-m-d H:i:s',strtotime($row->StartDate)),
                                'EndDate' => date('Y-m-d H:i:s',strtotime($row->EndDate)),
                                'Status' => $dataImageStatus['status'],
                                'Image' => $dataImageStatus['image'],
                                'QrCode' => 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl='.$coupon_code.'&chld=L|1&choe=UTF-8'

                            ));
                        }

                }else if($active == "false"){
                      if($dataImageStatus['status'] == 'Expired' || $dataImageStatus['status'] == 'Used'){
                            array_push($couponData, array(
                                'CouponId' => $row->Coupon_id,
                                'CouponAgentId' => $row->couponagent_id,
                                'CouponCode' => $coupon_code,
                                'CouponName' => $row->Name,
                                'CouponDescription' => $row->Description,
                                'DiscountRate' => $row->DiscountRate,
                                'DiscountType' => $row->DiscountType,
                                'DiscountCurrency' => $row->DiscountCurrency,
                                'StartDate' => date('Y-m-d H:i:s',strtotime($row->StartDate)),
                                'EndDate' => date('Y-m-d H:i:s',strtotime($row->EndDate)),
                                'Status' => $dataImageStatus['status'],
                                'Image' => $dataImageStatus['image'],
                                'QrCode' => 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl='.$coupon_code.'&chld=L|1&choe=UTF-8'

                            ));

                      }

                }else{
                            array_push($couponData, array(
                                'CouponId' => $row->Coupon_id,
                                'CouponAgentId' => $row->couponagent_id,
                                'CouponCode' => $coupon_code,
                                'CouponName' => $row->Name,
                                'CouponDescription' => $row->Description,
                                'DiscountRate' => $row->DiscountRate,
                                'DiscountType' => $row->DiscountType,
                                'DiscountCurrency' => $row->DiscountCurrency,
                                'StartDate' => date('Y-m-d H:i:s',strtotime($row->StartDate)),
                                'EndDate' => date('Y-m-d H:i:s',strtotime($row->EndDate)),
                                'Status' => $dataImageStatus['status'],
                                'Image' => $dataImageStatus['image'],
                                'QrCode' => 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl='.$coupon_code.'&chld=L|1&choe=UTF-8'

                            ));
                }
                
                
            }

            return array(
                'status' => true,
                'coupon_data' => $couponData,
                'result_code' => '000',
                'result_desc' => 'Success'


            );
            

       }else{

        return array(
            'status' => false,
            'result_code' => '-002',
            'result_desc' => 'ไม่พบคูปอง'
        );
       }

    }


    private function getCouponListImageStatus($obj = ""){
        //print_r($obj); exit;
        $status = "Available";
        $image = $obj->Image;

        if($obj->Used){
            $status = "Used";
            $image = base_url('uploaded/coupon/coupon_imageused/'.$obj->Coupon_id.'/'.$obj->ImageUsed);
        }else if(strtotime($obj->EndDate) >= strtotime(date('Y-m-d'))){
            $status = "Available";
            $image = base_url('uploaded/coupon/coupon_image/'.$obj->Coupon_id.'/'.$obj->Image);
        }else if(strtotime(date('Y-m-d')) > strtotime($obj->EndDate)){
            $status = "Expired";
            $image = base_url('uploaded/coupon/coupon_imageexpired/'.$obj->Coupon_id.'/'.$obj->ImageExpired);
        }


        return array('status'=>$status,'image'=>$image);

    }

    private function getCouponListImageStatusRedeem($obj = ""){
        $status = "Available";
        $image = $obj->Image;

        if($obj->Used){
                $status = "Used";
                $image = base_url('uploaded/coupon/coupon_imageused/'.$obj->Coupon_id.'/'.$obj->ImageUsed);
        }else if(strtotime($obj->ExpireDate) >= strtotime(date('Y-m-d H:i:s'))){
                $status = "Available";
                $image = base_url('uploaded/coupon/coupon_image/'.$obj->Coupon_id.'/'.$obj->Image);
        }else if(strtotime(date('Y-m-d H:i:s')) > strtotime($obj->ExpireDate)){
                $status = "Expired";
                $image = base_url('uploaded/coupon/coupon_imageexpired/'.$obj->Coupon_id.'/'.$obj->ImageExpired);
        }
        return array(
                'status'=>$status,
                'image'=>$image
        );

    }


    //=============================== FOR REDEEM STEP =========================================
    public function checkPoinAvailable($requestCriteria){

            /* check coupon id first */
            $queryCoupon = $this->ci->db->select('*')
            ->from('Coupon')
            ->where('id',$requestCriteria->coupon_id)
            ->get();

            if($queryCoupon->num_rows() > 0){
                $rowCoupon = $queryCoupon->row();


                $queryAgent = $this->ci->db->select('AgentCode,Point')
                    ->from('Agent')
                    ->where('AgentCode',$requestCriteria->agentcode)
                    ->get();

                    if($queryAgent->num_rows() > 0){
                        $rowAgent = $queryAgent->row();
                        if($rowAgent->Point >= $rowCoupon->PointUse){
                            return array(
                                    'status'=>true,
                                    'coupon'=>$rowCoupon,
                                    'agent'=>$rowAgent
                            );
                        }else{
                            return array(
                                    'status'=>false,
                                    'flag'=>'not-enough-point'
                            );
                        }

                    }else{
                        return array(
                            'status'=>false,
                            'flag'=>'not-found-agent'
                        );
                    }

            }else{
                return  array(
                        'status'=>false,
                        'flag'=>'not-found-coupon'
                );
            }



    }

    private function generateCouponCode($length=10){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }

    private function calculateExpireDate($arrData = array()){
        $redeem_config = $this->psiarm_config['redeem_coupon'];
        $expire_date = new DateTime();
        $expire_date->add(new DateInterval('P'.$redeem_config['duration_use'].$redeem_config['duration_type']));
        return $expire_date->format('Y-m-d H:i:s');
        //print_r($expire_date);exit;

    }


    //=============================== EOF REDEEM STEP =========================================




}