<?php


class AgentLibrary {

    private $ci;
    private $conn;
    private $conn_mysql;
    private $apiConfigPWT = array();
    private $agentConfig = array();
    private $onesignalFixitArmConfig = array();
    private $onesignalFixitConfig = array();
    private $serverA_conn;
    private $seminar_conn;
    private $seminar_available;
    private $db_config;
    function __construct() {
        $CI =& get_instance();
        $this->ci = $CI;
        /** SERVER A DB CONNECTION */
        $CI->load->config('internaldb');
		$this->db_config = $CI->config->item('serverA_DB');
        $this->connectSeminarDB();
        /** EOF SERVER A DB CONNECTION */
        
        //$this->ci->otherdb = $CI->load->database('otherdb', TRUE);
        $CI->load->config('agent');
        $CI->load->config('api');
        /* connect mysql */
        $this->connect_mysql();
        /* eof connect mysql */
        /** connect mssql database ( multiple ) */
      

        $this->apiConfigPWT = $CI->config->item('water_filter');
        $this->agentConfig = $CI->config->item('agent');
        $this->onesignalFixitArmConfig = $CI->config->item('onesignal_fixitarm');
        $this->onesignalFixitConfig = $CI->config->item('onesignal');

       
    }

 
    // my func : mypsi service
    public function SyncWPuser($requestCriteria){
        

        /**
         * SELECT TWO SERVER DATABASE FOR COMPARE 
         * AND SYNC DATA FROM SERVER A (mysql-> wordpress psisat) TO SERVER B (mssql -> )
         */
         # TABLE DESCRIPTION FOR COMPARE
         $tables = array( 
                         "wp_users"   =>  array("key_identifier_serverB" => "wp_id"   
                                        , "key_identifier_serverA" => "ID" 
                                        , "column" => array(
                                            "wp_id" => "",
                                            "user_login" => "",
                                            "user_pass" => "",
                                            "user_nicename" => "",
                                            "user_email" => "",
                                            "user_url" => "",
                                            "user_registered" => "",
                                            "user_activation_key" => "",
                                            "display_name" => "",
                                            "sync_check" => "",
                                        )
                        )
                        , "wp_usermeta" => array("key_identifier_serverB" => "umeta_id"
                                            , "key_identifier_serverA" => "umeta_id"
                                            , "column" => array(
                                                "umeta_id" => "",
                                                "user_id" => "",
                                                "meta_key" => "",
                                                "meta_value" => "",
                                                "sync_check" => ""
                                            )
                        ) 
                   );
        
    

         // SERVER B -> SQL SERVER
         #  PREPARE DATA
         $ServerB_dtUser = array();
         
        foreach($tables as $k => $v){
            $CI = & get_instance();
            $query = $CI->db->select('*')
            ->from("{$k}")
            ->get();
           
            if($query->num_rows() > 0){
                $results = $query->result_array();
                $ServerB_dtUser[$k] = $results;
            }
            else{
                $ServerB_dtUser[$k] = array();
            }
        }
         // EOF SERVER B

         // SERVER A    
         $ServerA_dtUser = array();
         // *** KEY IS TABLE NAME **** 
         foreach($tables as $key => $value){
            $query = "select * from {$key} ";
            $result = $this->conn->query($query);
            if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                    /* Check is exist in SERVER B */
                    $valueIdentifier_wp = $row[$tables[$key]["key_identifier_serverA"]];
                    $keyIdentifier_serverA = $tables[$key]["key_identifier_serverA"];
                    $keyIdentifier_serverB = $tables[$key]["key_identifier_serverB"];
                    $isValueExist = $this->isValueExist( $ServerB_dtUser[$key] , $keyIdentifier_serverB ,  $valueIdentifier_wp );
                    # CASE 1 :  if Exist UPDATE 
                   
                    // EDIT KEYIDENTIFIER FOR THIS ACTION
                    # unset old key
                    if(isset($row[$keyIdentifier_serverA])){
                        unset($row[$keyIdentifier_serverA]);
                        # change to new key
                        $row[$keyIdentifier_serverB] = $valueIdentifier_wp;
                        $row["sync_check"] = 1;
                    }
                   
                    if($isValueExist == true || $isValueExist == 1){
                        # code ..
                        //set sync is check
                        // QUERY BUILDER ( UPDATE )
                        $CI->db->update(
                            "{$key}"
                            , $row
                            , ["{$keyIdentifier_serverB}"=> "{$valueIdentifier_wp}"]
                        );
                        //eof
                    }else{
                        # CASE 2 :  if not exist insert
                        // QUERY BUILDER ( INSERT )
                         $CI->db->insert(
                             "{$key}" , $row                       
                         );
                    }
                     
            }

            }

            
            $data = array(
                'dataUser' => $dataUser
            );
        }
         // EOF SERVER A
        

         return array(
            'status' => true,
            'result_code' => '000',
            'result_desc' => 'Success'
        );

    

    }

    public function SyncFixITData(){

        $CI = & get_instance();

        // GET DATA FROM SERVER B AND INSERT TO SERVER A
        $query_truncate = "TRUNCATE TABLE FixITCustomer"; 
        $execute_query_clear = $CI->db->query($query_truncate);

        $query_sync = 'INSERT INTO [MYPSILocalhost].[dbo].[FixITCustomer](UserName,Password,CustName,Email,LoginType,Device_Token,OS,CustID,CustAddress,Telephone,Status,District,Province)
         SELECT UserName,Password,CustName,Email,LoginType,Device_Token,OS,CustID,CustAddress,Telephone,Status,District,Province From [WINDBSERVER41].[MyPSI].dbo.[FixITCustomer]';
        $execute_query_sync = $CI->db->query($query_sync);
 
        return array(
            'status' => true,
            'result_code' => '000',
            'result_desc' => 'Success'
        );

  
    }

    public function isValueExist($array, $key, $val) {
        
        if(empty($array)){
            return false;
        }

        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return true;
        return false;
    }

    public function connect_mysql(){

        if(ENVIRONMENT == "development"){
            // $servername = "localhost";
            // $username = "root";
            // $password = "";
			// $dbname = "psitv";
		    $servername = "61.19.247.229";
            $username = "psisat";
            $password = "PsIMaiL_SerVER2562";
            $dbname = "psisat_2018";

        }else if(ENVIRONMENT == "testing"){
            $servername = "61.19.247.229";
            $username = "psisat";
            $password = "PsIMaiL_SerVER2562";
            $dbname = "psisat_2018";

        }else if(ENVIRONMENT == "production"){
          
            $servername = "61.19.247.229";
            $username = "psisat";
            $password = "PsIMaiL_SerVER2562";
            $dbname = "psisat_2018";

        }

        // Create connection
        $this->conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        $this->conn->set_charset("utf8");
        if ($this->conn->connect_error) {
         
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }


    private function connectSeminarDB(){
       
		if(ENVIRONMENT == 'development'){


	    	$serverName = $this->db_config['development']['servername']; //serverName\instanceName
	    	$connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
	    	$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);


	    	if(!$this->seminar_conn){
	    		echo "Connection could not be established.<br />";
	    		die( print_r( sqlsrv_errors(), true));
	    	}
	    }else if(ENVIRONMENT == 'testing'){
			$serverName = $this->db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){
                
			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);
            
			if(!$this->seminar_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}
     


		}
	}
   

}   