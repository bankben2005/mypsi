<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class SeminarLibrary{

    private $ci;
    private $seminar_conn;
    private $db_config;
    private $api_config;
    function __construct() {
        
        $this->ci =& get_instance();
        $this->ci->load->config('internaldb');
        $this->ci->load->config('api');

        $this->db_config = $this->ci->config->item('seminar_db');
        $this->api_config = $this->ci->config->item('seminar');

        // print_r($api_config);exit;
        $this->connectSeminarDB();
    }

    public function getTicketDetail($requestCriteria){
            // $this->getMPCODEAvailable();
        //print_r($requestCriteria);exit;
          
    		$arReturn = array();
    		$seminar_data = array();
    		/* Check Seminar Available */
    		$seminar_data = $this->getSeminarAvailable();

            //$seminar_all_data  = $this->getSeminarAllAvailable();

            if(isset($_GET['test']) && $_GET['test'] != ''){
                print_r($seminar_data);exit;
            }
            //print_r($seminar_all_data);exit;

            //print_r($seminar_data);exit;

            //print_r($seminar_data);exit;
            if(count($seminar_data) == 1){

                $seminar_data = $seminar_data[0];

                //print_r($seminar_data);exit; 

                if($seminar_data->SeminarType == '3'){
                    return $this->getSeminarDataQuotaRivisionOne($requestCriteria);
                }else if($seminar_data->SeminarType == '1'){
                    return $this->getSeminarDataTicket($seminar_data,$requestCriteria);
                }

                

            }else if(count($seminar_data) > 1){
                $seminar_data_1 = @$seminar_data[0];

                if(count((array)$seminar_data_1) > 1){

                //print_r($seminar_data_1);exit;
                if($seminar_data_1->SeminarType == '3'){
                    /* check is able for type 3 */
                    //print_r('aaaa');exit;
                    //$data_callback = $this->getSeminarDataQuota($seminar_data_1,$seminar_data[1],$requestCriteria);
                    return $this->getSeminarDataQuotaRivisionOne($requestCriteria);
                    //return $this->getSeminarDataQuota($seminar_data_1,$seminar_data[1],$requestCriteria);


                }else if($seminar_data_1->SeminarType == '1'){
                    //echo 'abcd';
                    return $this->getSeminarDataTicket($seminar_data_1,$requestCriteria);
                }
                }else{
                    return array(
                    'status' => false,
                    'result_code' => '-002',
                    'result_desc' => 'ไม่พบข้อมูลสัมนาสำหรับวันนี้'

                    );
                }

                // print_r($seminar_data_1);exit;


            }else if(count($seminar_data) <= 0){

                    return array(
                    'status' => false,
                    'result_code' => '-002',
                    'result_desc' => 'ไม่พบงานสัมนาเร็วๆนี้'

                    );

            }

    		

    }

    public function setSeminarCheckin($requestCriteria){
        //print_r($requestCriteria);exit;
            //echo 'aaaaa';exit;
    		/* get Agent Data first */
    		$agent = new M_agent();
    		$agent->where('AgentCode',$requestCriteria->agentcode)->get();

    		if($agent->result_count() > 0){
    			$agent_data = $agent->to_array();

                //print_r($agent_data);exit;
    			$seminar_data = $this->getSeminarAvailable();

                if(!$seminar_data){
                    return array(
                        'status' => false,
                        'result_code' => '-002',
                        'result_desc' => 'ไม่พบงานสัมนา'
                    );
                }

                $seminar_data = $seminar_data[0];
                //print_r($seminar_data);exit;

    			/* check already check in*/
    			if($this->checkIsTechnicianCheckin(array('agent_code'=>$agent_data['AgentCode'],'seminar_id'=>$seminar_data->SeminarNo))){
    				return array(
    					'status' => false,
    					'result_code' => '-006',
    					'result_desc' => 'คุณได้ทำการลงทะเบียนแล้ว'
    				);
    			}else{
                    // echo 'aaaaa';exit;
    				$dataticket = $this->getSeminarTicketByMPCODEAndCustomer(array('agent_code'=>$requestCriteria->agentcode,'mpcode'=>$this->getMPCODEAvailable()))->row_array();

    				//print_r($dataticket);exit;

    				$data_insert = array(
    					'seminar_id'=>$seminar_data->SeminarNo,
    					'agent_code'=>$agent_data['AgentCode'],
    					'branch_code' => $agent_data['BranchCode'],
    					'last_update' => date('Y-m-d H:i:s'),
    					'purchase_date' => $dataticket['POSTDATE'],
    					'receive_date' => $dataticket['POSTDATE'],
    					'customer_name' => $agent_data['AgentName'].' '.$agent_data['AgentSurName'],
    					'customer_address' => $agent_data['R_Addr1'],
    					'customer_district' => $agent_data['R_District'],
    					'customer_province' => $agent_data['R_Province'],
    					'customer_phone' => $agent_data['Telephone'],
                        'agent_cardno' => $agent_data['CardNo']

    				);
                    

                    /* eof check seminar product available */
                    //print_r($data_insert);exit;
    				if($this->insertCheckinData($data_insert)){
    					return array(
    						'status' => true,
    						'result_code' => '000',
    						'result_desc' => 'ทำการลงทะเบียนเรียบร้อย'
    					);

    				}else{

    					return array(
    						'status' => false,
    						'result_code' => '-003',
    						'result_desc' => 'เกิดข้อผิดพลาดในการลงทะเบียน กรุณาลองใหม่อีกครั้ง'

    					);

    				}


    			}


    		}else{
    			return array(
    				'status' => false,
    				'result_code' => '-002',
    				'result_desc' => 'ไม่พบข้อมูลช่าง'

    			);
    		}

    }

    public function checkIn(){
    	$query = "select * from SeminarData where AgentCode = '".$requestCriteria->agentcode."'";

    		$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

    		if(sqlsrv_num_rows($stmt) > 0){

    			while ($obj = sqlsrv_fetch_object($stmt)) {
    				# code...
    				print_r($obj);
    			}

    		}

    	
    	//exit;
    }

    private function connectSeminarDB(){


    	if(ENVIRONMENT == 'development'){


	    	$serverName = $this->db_config['development']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}
		}else if(ENVIRONMENT == 'testing'){
			$serverName = $this->db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}



		}
    }

    public function closeConnectSeminarDB(){
    	sqlsrv_close( $this->seminar_conn );
    }

    private function getMPCODEAvailable(){
        //return 'O1600002';
    	   $objSeminar = $this->getSeminarAvailable();

           if(!empty($objSeminar)){
                $nearestSeminar = $objSeminar[0];

                //echo $nearestSeminar->MPCODE;exit;

                return $nearestSeminar->MPCODE;

           }


    }

    private function getSeminarAvailable(){
        $seminarInfoArray = array();
        $seminarType = array();


    	$query = "select TOP 2 * from SeminarInfo where SeminarDate >= '".date('Y-m-d')."' order by SeminarDate asc";
     //echo $query;exit;

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

    	if(sqlsrv_num_rows($stmt) > 0){
            //echo 'aaaa';exit;
            $data_return = array();

            if(sqlsrv_num_rows($stmt) >= 1){
                while ($result = sqlsrv_fetch_object($stmt)) {
                    # code...
                    array_push($data_return, $result);
                }

            }

            //print_r($data_return);exit;

    		return $data_return;
    	}else{
    		return array();
    	}
    }

    private function getSeminarAllAvailable(){
        $seminarInfoArray = array();
        $seminarType = array();


        $query = "select * from SeminarInfo where SeminarDate >= '".date('Y-m-d')."' order by SeminarDate asc";
     //echo $query;exit;

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt) > 0){
            //echo 'aaaa';exit;
            $data_return = array();

            if(sqlsrv_num_rows($stmt) >= 1){
                while ($result = sqlsrv_fetch_object($stmt)) {
                    # code...
                    array_push($data_return, $result);
                }

            }

            //print_r($data_return);exit;

            return $data_return;
        }else{
            return array();
        }
    }

    private function checkIsTechnicianCheckin($data = array()){

            /* SeminarId Group */
            $queryGroup = "select * from SeminarInfo where SeminarNo = '".$data['seminar_id']."'";

            $stmt1 = sqlsrv_query( $this->seminar_conn, $queryGroup,array(),array( "Scrollable" => 'static' ));

            if(sqlsrv_num_rows($stmt1)){
                    $resultObj = sqlsrv_fetch_object($stmt1);
                    $groupObj = $resultObj->SeminarInfoGroup;

                    /* Create log file for debug*/
                            // $log_file_path = $this->createLogFilePath('CheckTechnicianCheckin');
                            // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode((array)$resultObj) . "\n";
                            // file_put_contents($log_file_path, $file_content, FILE_APPEND);
                            // unset($file_content);


                    $query = "select * from SeminarData join SeminarInfo on SeminarData.SeminarNo = SeminarInfo.SeminarNo where SeminarData.AgentCode = '".$data['agent_code']."' order by RegisterDateTime desc";

                    $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

                    if(sqlsrv_num_rows($stmt) > 0){
                        // return true;
                        //$returnObj = sqlsrv_fetch_object($stmt);
                        // $registerDateTime = new DateTime($returnObj->RegisterDateTime);
                        $dataGroup = array();
                        $dataArray = array();
                        $dataReturnData = array();
                        while ($result = sqlsrv_fetch_object($stmt)) {
                            # code...
                            // if($result->SeminarInfoGroup == $groupObj){
                            //     return array(
                            //         'SeminarNo' => $result->SeminarNo,
                            //         'AgentCode' => $result->AgentCode,
                            //         'CheckinDate' => $result->RegisterDateTime->format('Y-m-d'),
                            //         'CheckinTime' => $result->RegisterDateTime->format('H:i:s')
                            //     );
                            // }
                            array_push($dataGroup, $result->SeminarInfoGroup);
                            array_push($dataArray, $result);

                        }

                        /* Create log file for debug*/
                            // $log_file_path = $this->createLogFilePath('CheckTechnicianCheckin');
                            // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($dataArray) . "\n";
                            // file_put_contents($log_file_path, $file_content, FILE_APPEND);
                            // unset($file_content);


                        if(in_array($groupObj, $dataGroup)){

                                $dataReturnData = array(
                                    'SeminarNo' => $dataArray[0]->SeminarNo,
                                    'AgentCode' => $dataArray[0]->AgentCode,
                                    'CheckinDate' => $dataArray[0]->RegisterDateTime->format('Y-m-d'),
                                    'CheckinTime' => $dataArray[0]->RegisterDateTime->format('H:i:s')
                                );
                                return $dataReturnData;

                                // print_r($dataReturnData);exit;

                        }else{
                            return null;

                        }
                        

                        
                    }else{
                        return null;
                    }


            }


    		

    }

    private function insertCheckinData($data = array()){
    	// print_r($data);exit;
    		$query = "insert into SeminarData(
    			SeminarNo,
    			CardNo,
    			RegisterDateTime,
    			AgentCode,
                Username,
    			Remark
    			)values(
	    		'".$data['seminar_id']."',
	    		'".$data['agent_cardno']."',
	    		'".date('Y-m-d H:i:s')."',
	    		'".$data['agent_code']."',
                '".$data['agent_code']."',
	    		'mobile_checkin'
	    		)";

	    	 // echo $query;exit;
    		$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

    		if($stmt){
    			return true;
    		}else{
    			return false;
    		}


    }

    private function getSeminarTicketByMPCODEAndCustomer($data = array()){

        // print_r($data);exit;
    	return $this->ci->db->select('POSTRANSACTION.AgentCode,POSTRANSACTION.TMABBNUMBER,POSITEM.MPCODE,POSITEM.TMABBNUMBER,POSITEM.POSIQUANTITY,POSITEM.POSIPRICE,POSITEM.POSINETPRICE,POSITEM.POSTDATE,POSTRANSACTION.POSTSTATUS,MasterProduct.MPNAME,MasterProduct.MPDESCRIPTION,MasterProduct.ProductImage')
		    	->from('POSITEM')
		    	->join('POSTRANSACTION','POSTRANSACTION.TMABBNUMBER = POSITEM.TMABBNUMBER')
		    	->join('MasterProduct','MasterProduct.MPCODE = POSITEM.MPCODE')
                ->where('POSTRANSACTION.POSTSTATUS','T')
		    	->where('POSITEM.MPCODE',$data['mpcode'])
		    	->where('POSTRANSACTION.AgentCode',$data['agent_code'])
		    	->order_by('POSITEM.POSTDATE','desc')
		    	// ->limit(1)
		    	->get();
    }



    public function deleteDemoRegister(){

        $agentCode = $_GET['agentcode'];
        // echo $agentCode;exit;
        // echo $agentCode;exit;
        $strQuery = "delete from SeminarData where AgentCode = '".$agentCode."' and SeminarNo = 'PSI_TRANSFORM_PT'";

        $stmt = sqlsrv_query( $this->seminar_conn, $strQuery,array(),array( "Scrollable" => 'static' ));

        if($stmt){
            echo 'delete success';
        }else{
            echo 'can not delete';
        }



    }

    private function checkSeminarQuotaRegister($data = array()){
        //print_r($data);exit;
        $query = "select * from SeminarRegister where SeminarNo = '".$data['SeminarNo']."' and AgentCode = '".$data['AgentCode']."'";

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt) > 0){
            return sqlsrv_fetch_object($stmt);
        }else{
            return false;

        }



    }


    private function getSeminarGroupData($seminargroup_name){

        $query = "select * from SeminarInfoGroup where SeminarInfoGroup = '".$seminargroup_name."'";

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt) > 0){
            return sqlsrv_fetch_object($stmt);
        }

    }


    private function getSeminarDataTicket($seminar_data = array(),$requestCriteria){
                                $dataReturn = array();

                                $requestCriteria->mpcode = $this->getMPCODEAvailable();

                                $qTicket = $this->getSeminarTicketByMPCODEAndCustomer(array('mpcode'=>$requestCriteria->mpcode,'agent_code'=>$requestCriteria->agentcode));

                                //print_r($qTicket);exit;
                                // echo $qTicket->num_rows();exit;

                                
                                if($qTicket->num_rows() > 0){
                                        $quantity_amount = 0;
                                        $grand_totals_amount = 0;
                                        if($qTicket->num_rows() > 1){
                                            foreach ($qTicket->result() as $key => $value) {
                                                # code...
                                                $quantity_amount += $value->POSIQUANTITY;
                                                $grand_totals_amount += $value->POSINETPRICE;

                                            }

                                        }else{

                                            $quantity_amount = $qTicket->row()->POSIQUANTITY;
                                            $grand_totals_amount = $qTicket->row()->POSINETPRICE;
                                        }

                                        $row = $qTicket->row();

                                        $arReturn = array(
                                            'ticket_name' => $row->MPNAME,
                                            'ticket_description' => $row->MPDESCRIPTION,
                                            // 'seminar_name' => $seminar_data['SeminarDesc'],
                                            'price_per_items' => (float)$row->POSIPRICE,
                                            'quantity' => (string)(int)$quantity_amount,
                                            'grand_totals' => (float)$grand_totals_amount,
                                            'date' => date('Y-m-d H:i:s',strtotime($row->POSTDATE))

                                        );
                                        
                                    
                                     //print_r($arReturn);exit;
                                   

                                    $dataReturn['status'] = true;
                                    $dataReturn['data_ticket'] = $arReturn;
                                    $dataReturn['is_checkin'] = ($this->checkIsTechnicianCheckin(array('agent_code'=>$requestCriteria->agentcode,'seminar_id'=>$seminar_data->SeminarNo)))?true:false;
                                    $dataReturn['ticket_image'] = ($dataReturn['is_checkin'])?base_url('uploaded/seminar/ticket/seminar_card_registered.png?'.time()):base_url('uploaded/seminar/ticket/seminar_card.png?'.time());
                                    $dataReturn['data_checkin'] = $this->checkIsTechnicianCheckin(array('agent_code'=>$requestCriteria->agentcode,'seminar_id'=>$seminar_data->SeminarNo));

                                    $dataReturn['webview_orderproduct'] = $this->api_config['webview_orderproduct'];
                                    $dataReturn['booking_available'] = ($this->countSeminarProductAvailable($seminar_data->SeminarNo))?true:false;

                                    $dataReturn['result_code'] = '000';
                                    $dataReturn['result_desc'] = 'Success';

                                    if(date('Y-m-d') == $seminar_data->SeminarDate->format('Y-m-d')){
                                        $dataReturn['is_active'] = true;
                                    }else{
                                        $dataReturn['is_active'] = false;
                                    }
                                    //print_r($dataReturn);exit;
                                    return $dataReturn;


                                }else{
                                    //echo 'aaa';exit;

                                    return array(
                                        'status' => false,
                                        'result_code' => '-002',
                                        'result_desc' => 'ไม่พบข้อมูลการซื้อบัตร'

                                    );

                                }
    }

    private function getSeminarDataQuota($seminar_data,$seminar_data_2,$requestCriteria){

                        $arReturn = array();
                        if($this->checkSeminarQuotaRegister(array('SeminarNo'=>$seminar_data->SeminarNo,'AgentCode'=>$requestCriteria->agentcode))){
                                // print_r('im here');
                                    /* check seminar today */
                                    // print_r($seminar_data->SeminarDate);exit;
                                    // echo date('Y-m-d')."<br>".date('Y-m-d',strtotime($seminar_data->SeminarDate));exit;

                                    // echo date('Y-m-d')."<br>".$seminar_data->SeminarDate->format('Y-m-d');exit;
                                    
                                    /* eof check seminar today*/

                                    $dataSeminarRegister = $this->checkSeminarQuotaRegister(array('SeminarNo'=>$seminar_data->SeminarNo,'AgentCode'=>$requestCriteria->agentcode));

                                        $arReturn = array(
                                            'ticket_name' => '',
                                            'ticket_description' => '',
                                            // 'seminar_name' => $seminar_data['SeminarDesc'],
                                            'price_per_items' => 0,
                                            'quantity' => (string)(int)$dataSeminarRegister->Qty,
                                            'grand_totals' => (float)0,
                                            'date' => date('Y-m-d H:i:s')

                                        );
                                    $dataReturn = array(
                                        'status' => true,
                                        'data_ticket' => $arReturn,
                                        'is_checkin' => ($this->checkIsTechnicianCheckin(array('agent_code'=>$requestCriteria->agentcode,'seminar_id'=>$seminar_data->SeminarNo)))?true:false,
                                        'data_checkin' => $this->checkIsTechnicianCheckin(array('agent_code'=>$requestCriteria->agentcode,'seminar_id'=>$seminar_data->SeminarNo)),
                                        'webview_orderproduct' => $this->api_config['webview_orderproduct'],
                                        'booking_available' => ($this->countSeminarProductAvailable($seminar_data->SeminarNo))?true:false,
                                        'result_code' => '000',
                                        'result_desc' => 'Success'

                                    );

                                   
                                    if(date('Y-m-d') == $seminar_data->SeminarDate->format('Y-m-d')){
                                        $dataReturn['is_active'] = true;
                                    }else{
                                        $dataReturn['is_active'] = false;
                                    }
                                    $dataReturn['ticket_image'] = ($dataReturn['is_checkin'])?base_url('uploaded/seminar/ticket/seminar_card_registered.png?'.time()):base_url('uploaded/seminar/ticket/seminar_card.png?'.time());
                                   
                                    return $dataReturn;

                    }else{

                       // print_r($seminar_data_2);

                       // print_r($requestCriteria);

                       // exit;

                        return array(
                            'status'=>false,
                            'result_code' =>'-002',
                            'result_desc' => 'ไม่พบข้อมูลสิทธิการเข้างานสัมนา'

                        );

                        
                           return  $this->getSeminarDataTicket($seminar_data_2,$requestCriteria);





                    }

    }

    private function getSeminarDataQuotaRivisionOne($requestCriteria){
            $dataReturn = array();
            $seminar_data = $this->getSeminarAllAvailable();

            foreach ($seminar_data as $key => $value) {
                # code...
                if(!empty($this->setReturnSeminarDataQuotaLoop($value,$requestCriteria->agentcode))){
                    array_push($dataReturn,$this->setReturnSeminarDataQuotaLoop($value,$requestCriteria->agentcode));
                }
            }
            return $dataReturn[0];
            //print_r($dataReturn[0]);exit;
            // print_r($seminar_data);exit;



    }
    private function setReturnSeminarDataQuotaLoop($seminar_data,$agentcode){
        $dataReturn = array();
        $dataSeminarRegister = $this->checkSeminarQuotaRegister(array('SeminarNo'=>$seminar_data->SeminarNo,'AgentCode'=>$agentcode));
        //print_r($dataSeminarRegister);exit;
            if($dataSeminarRegister){
                                        $arReturn = array(
                                            'ticket_name' => '',
                                            'ticket_description' => '',
                                            // 'seminar_name' => $seminar_data['SeminarDesc'],
                                            'price_per_items' => 0,
                                            'quantity' => (string)(int)@$dataSeminarRegister->Qty,
                                            'grand_totals' => (float)0,
                                            'date' => date('Y-m-d H:i:s')

                                        );
                                    $dataReturn = array(
                                        'status' => true,
                                        'data_ticket' => $arReturn,
                                        'is_checkin' => ($this->checkIsTechnicianCheckin(array('agent_code'=>$agentcode,'seminar_id'=>$seminar_data->SeminarNo)))?true:false,
                                        'data_checkin' => $this->checkIsTechnicianCheckin(array('agent_code'=>$agentcode,'seminar_id'=>$seminar_data->SeminarNo)),
                                        'webview_orderproduct' => $this->api_config['webview_orderproduct'],
                                        'booking_available' => ($this->countSeminarProductAvailable($seminar_data->SeminarNo))?true:false,
                                        'result_code' => '000',
                                        'result_desc' => 'Success'

                                    );

                                   
                                    if(date('Y-m-d') == $seminar_data->SeminarDate->format('Y-m-d')){
                                        $dataReturn['is_active'] = true;
                                    }else{
                                        $dataReturn['is_active'] = false;
                                    }
                                    $dataReturn['ticket_image'] = ($dataReturn['is_checkin'])?base_url('uploaded/seminar/ticket/seminar_card_registered.png?'.time()):base_url('uploaded/seminar/ticket/seminar_card.png?'.time());
                                   
                                    return $dataReturn;
            }


    }
    private function createLogFilePath($filename = '') {
        
        //$document_root = $_SERVER['DOCUMENT_ROOT'];

        $log_path = APPPATH.'logs/checktechniciancheckin';
        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    public function testTechnicianCheckin(){


        $data = array('seminar_id'=>'PSI_TRANSFORM_002','agentcode'=>'5830512');

        //print_r($data);exit;
        /* SeminarId Group */
            $queryGroup = "select * from SeminarInfo where SeminarNo = '".$data['seminar_id']."'";

            $stmt1 = sqlsrv_query( $this->seminar_conn, $queryGroup,array(),array( "Scrollable" => 'static' ));

            if(sqlsrv_num_rows($stmt1)){
                //echo 'abcd';
                    $resultObj = sqlsrv_fetch_object($stmt1);
                    $groupObj = $resultObj->SeminarInfoGroup;

                    //echo $groupObj;exit;

                    /* Create log file for debug*/
                            // $log_file_path = $this->createLogFilePath('CheckTechnicianCheckin');
                            // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode((array)$resultObj) . "\n";
                            // file_put_contents($log_file_path, $file_content, FILE_APPEND);
                            // unset($file_content);


                    $query = "select * from SeminarData join SeminarInfo on SeminarData.SeminarNo = SeminarInfo.SeminarNo where SeminarData.AgentCode = '".$data['agentcode']."' order by RegisterDateTime desc";

                    //echo $query;exit;

                    $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

                    if(sqlsrv_num_rows($stmt) > 0){
                        // return true;
                        //$returnObj = sqlsrv_fetch_object($stmt);
                        // $registerDateTime = new DateTime($returnObj->RegisterDateTime);
                        $dataGroup = array();
                        $dataArray = array();
                        $dataReturnData = array();
                        while ($result = sqlsrv_fetch_object($stmt)) {
                            # code...
                            // if($result->SeminarInfoGroup == $groupObj){
                            //     return array(
                            //         'SeminarNo' => $result->SeminarNo,
                            //         'AgentCode' => $result->AgentCode,
                            //         'CheckinDate' => $result->RegisterDateTime->format('Y-m-d'),
                            //         'CheckinTime' => $result->RegisterDateTime->format('H:i:s')
                            //     );
                            // }
                            array_push($dataGroup, $result->SeminarInfoGroup);
                            array_push($dataArray, $result);

                        }

                        

                        /* Create log file for debug*/
                            // $log_file_path = $this->createLogFilePath('CheckTechnicianCheckin');
                            // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($dataArray) . "\n";
                            // file_put_contents($log_file_path, $file_content, FILE_APPEND);
                            // unset($file_content);


                        if(in_array($groupObj, $dataGroup)){

                                $dataReturnData = array(
                                    'SeminarNo' => $dataArray[0]->SeminarNo,
                                    'AgentCode' => $dataArray[0]->AgentCode,
                                    'CheckinDate' => $dataArray[0]->RegisterDateTime->format('Y-m-d'),
                                    'CheckinTime' => $dataArray[0]->RegisterDateTime->format('H:i:s')
                                );

                                print_r($dataReturnData);exit;

                        }else{
                            return null;

                        }
                        

                        
                    }else{
                        return null;
                    }


            }


    }

    private function countSeminarProductAvailable($seminar_no = ""){
       
            $query = "select count(id) as totals_available from SeminarProductAvailable where SeminarNo = '".$seminar_no."'";

            //echo $query;exit;

            $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

            if(sqlsrv_num_rows($stmt) > 0){
                $result = sqlsrv_fetch_object($stmt);
                return $result->totals_available;
            }else{
                return 0;
            }

    }
}