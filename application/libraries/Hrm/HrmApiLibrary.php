<?php


class HrmApiLibrary{

    private $ci;
    private $conn_mysql;
    private $conn_hrm;
    private $hrmConfig = array();
    private $onesignal_psihrm;
    function __construct() {
        $CI =& get_instance();
        $this->ci = $CI;
        $CI->load->config('api');
        $this->hrmConfig = $CI->config->item('hrm');
        $this->onesignal_psihrm = $CI->config->item('onesignal_psihrm');
        // print_r($this->hrmConfig);

        $this->connect_mysql();
        $this->connect_hrm_mysql();
       
    }

    public function getTokenSignin($requestCriteria){

    	/* print_r($requestCriteria);exit; */
    	$account_data = $this->checkUserPassword(array('username'=>$requestCriteria->username,'password'=>$requestCriteria->password));
    	if($account_data){
    		if(!$account_data['active']){
    			return array(
    				'status' => false,
    				'result_code' => '-009',
    				'result_desc' => 'บัญชีของคุณถูกระงับการใช้งาน กรุณาติดต่อผู้ดูแลระบบ'
    			);
    		}

    		return $this->genTokenForAccount($account_data);


    	}else{
    		return array(
        		'status' => false,
        		'result_code' => '-002',
        		'result_desc' => 'user หรือ password ไม่ถูกต้อง'

        	);


    	}

    }

    public function setRemindEvaluationTask($requestCriteria){

            $arDeviceToken = array();
            //print_r($requestCriteria);exit;

            // get all response from api and add data to table if empid doesn't exist 
            $curlData = $this->curlGetListRemindHrm();

            //print_r($curlData);exit;
            foreach ($curlData->EmployeeShorts as $key => $value) {
                # code...
                        
                    $sqlInsert = "insert into evaluationremind(
                        empid,
                        empcode,
                        empname,
                        empsurname,
                        empdevice_token,
                        created

                    )values(
                        '".$value->empid."',
                        '".$value->empcode."',
                        '".$value->empname."',
                        '".$value->empsurname."',
                        '".$value->device_token."',
                        '".date('Y-m-d H:i:s')."'
                    )";
                    //echo $sqlInsert;exit;
                    $this->conn_hrm->query($sqlInsert);

                    //array_push($arDeviceToken, $value->device_token);
                
            }

            $sqlselect = "select * from evaluationremind WHERE DATE_FORMAT(created,'%Y-%m-%d') = '".date('Y-m-d')."' and empdevice_token != ''";

            $result = $this->conn_hrm->query($sqlselect);

            if($result->num_rows > 0){
              while($row = $result->fetch_assoc()){
                    array_push($arDeviceToken, $row['empdevice_token']);
              }
            }

            //$arDeviceToken = array_filter($arDeviceToken);

            //$arDeviceToken = array('3f9eaaca-6175-4166-b9f9-eb6fa5341c36');

            
            //print_r($arDeviceToken);exit;
            if($this->sendRemindEvaluationNotification($arDeviceToken)){
                    /* update status for evaluation remind */
                    foreach ($arDeviceToken as $key => $value) {
                        # code...
                        $sqlUpdate = "update evaluationremind set status = '1' WHERE DATE_FORMAT(created,'%Y-%m-%d') = '".date('Y-m-d')."' and empdevice_token = '".$value."'";
                        //echo $sqlUpdate;exit;
                        $this->conn_hrm->query($sqlUpdate);

                    }
                    /* eof update evaluation remind */

            }


            //print_r($arDeviceToken);



    }

    public function sendLeaveNotification($requestCriteria){
                //$requestCriteria['device_token'] = '7560fc12-fb71-4eb6-a126-04b15bf2bbb5';
                $txt_notification = "";
                $txt_notification .= $requestCriteria['emp_name']." ".$requestCriteria['emp_lastname']." \r\n";
                $txt_notification .= "ยื่นเรื่อง".$requestCriteria['leave_type']." จำนวน ".$requestCriteria['leave_totals']." วัน";

                $content = array(
                                'en' => $txt_notification
                );

                $fields = array(
                        'app_id' => $this->onesignal_psihrm['app_id'],
                        'include_player_ids' => array($requestCriteria['device_token']),
                        'data' => array("jobstatus" => "REMIND"),
                        'contents' => $content
                );

                        
                        $post_data = json_encode($fields);
                            
                        $response = json_decode($this->onesignalPsiHrmNotification($post_data));

                        if($response->recipients > 0){
                                return array(
                                        'status'=>true
                                );

                        }else{
                                return array(
                                        'status'=>false
                                );
                        }
    }


    private function checkUserPassword($data = array()){

    	$sql = "select * from oauth_account where user = '".$data['username']."' and password = '".sha1($data['password'])."'";

    	$result = $this->conn_mysql->query($sql);

        if($result->num_rows > 0){
          	return $result->fetch_assoc();
      	}else{
      		return false;
      	}
    }

    private function genTokenForAccount($account_data =array()){

    	$expires = "";

    	//=========== Token pattern ============
    	$token = rand().'-PSIHRM'.$account_data['user'].date('Y-m-d H:i:s').'-'.rand();

    	//echo $token;exit;
    	$token = base64_encode(base64_encode($token));

    	//echo $token;exit;

    	/* calcualte expires for token */
    	switch ($this->hrmConfig['token_expires']['unit']) {
    		case 'hours':
    			# code...
    		break;
    		case 'minute':
    				$expires = new DateTime();
    				$expires->add(new DateInterval('PT'.$this->hrmConfig['token_expires']['time'].'M'));
    		break;

    		case 'second':

    		break;
    		
    		default:
    			# code...
    		break;
    	}

    	$sqlInsert = "insert into oauth_access_token(
    		access_token,
    		clientid,
    		expires,
    		created
    	)values(
    		'".$token."',
    		'".$account_data['id']."',
    		'".$expires->format('Y-m-d H:i:s')."',
    		'".date('Y-m-d H:i:s')."'
    	)";

    	if($this->conn_mysql->query($sqlInsert)){
    		return array(
    			'status' => true,
    			'access_token' => $token,
    			'access_token_data' => array(
    				'access_token' => $token,
    				'account' => $account_data,
    				'expires' => $expires->format('Y-m-d H:i:s'),
    			),
    			'result_code' => '000',
    			'result_desc' => 'Success'

    		);
    		// echo $this->conn_mysql->insert_id;exit;
    	}else{
    		return array(
    			'status' => false,
    			'result_code' => '-003',
    			'result_desc' => 'เกิดข้อผิดพลาดระหว่างการสร้าง token กรุณาลองใหม่อีกครั้ง'

    		);

    	}

    	/* eof calculate expires for token */

    }


    private function connect_mysql(){

        if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "hrmoauth";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "hrmoauth";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "hrmoauth";

        }

        // Create connection
        $this->conn_mysql = new mysqli($servername, $username, $password, $dbname);
        $this->conn_mysql->set_charset("utf8");
        // Check connection
        if ($this->conn_mysql->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }

    private function connect_hrm_mysql(){

        if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "psihrm";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psihrm";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psihrm";

        }

        // Create connection
        $this->conn_hrm = new mysqli($servername, $username, $password, $dbname);
        $this->conn_hrm->set_charset("utf8");
        // Check connection
        if ($this->conn_mysql->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }
    private function onesignalPsiHrmNotification($post_data){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->onesignal_psihrm['url']);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->onesignal_psihrm['header']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

               
                return $response;

    }

    private function curlGetListRemindHrm(){
                $post_data = array();
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'http://110.170.36.24/IHRApiService/GetEmps.svc/api/getempevalnotfinishlist');
               // curl_setopt($ch, CURLOPT_HTTPHEADER, '');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                $dataReturn = json_decode($response);

                return $dataReturn;
                //print_r($dataReturn);exit;
                // print_r($response->EmployeeShorts);
    }

    private function sendRemindEvaluationNotification($arDeviceToken){

                $content = array(
                                'en' => "ท่านยังทำการประเมินไม่ครบในไตรมาสนี้ \r\n กรุณาทำการประเมินให้ครบถ้วน"
                );

                $fields = array(
                        'app_id' => $this->onesignal_psihrm['app_id'],
                        'include_player_ids' => $arDeviceToken,
                        "ios_badgeType"=>"Increase",
                        "ios_badgeCount"=>1,
                        'data' => array("jobstatus" => "REMIND"),
                        'contents' => $content
                );

                        
                $post_data = json_encode($fields);
                            
                $response = json_decode($this->onesignalPsiHrmNotification($post_data));
                $log_message = "";
                if($response->recipients > 0){
                        $log_message = json_encode($response->recipients);
                        
                }else{
                        $log_message = json_encode(array('status'=>false));
                }
                        $insertLog  = "insert into evaluationremindlogs(
                            log_message,
                            log_date,
                            created
                        )values(
                            '".$log_message."',
                            '".date('Y-m-d')."',
                            '".date('Y-m-d H:i:s')."'
                        )";
                        $this->conn_hrm->query($insertLog);

                return true;


    }



}
