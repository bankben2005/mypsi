<?php
     class QuestionnaireLibrary extends CI_Controller{
             
             public $data = array();
             public $page_num = 10;
             public $controller = '';
             public $method = '';
             public $language = 'th';
             public $default_language;
             public $site_setting;
             public $currency_symbol;
             public $path_image = '';
             public $path_assets = '';
             public $language_id;
             public $language_name;
             public $facebook_user = array();
             protected $facebook_id = '';
             protected $facebook_secret = '';
             protected $facebook_permission = array();

         public function __construct() {
                    parent::__construct();
                    $CI = & get_instance();

                    $this->setController($CI->router->class);
                    $this->setMethod($CI->router->method);

                    //$CI->load->helper(array('frontend','lang','our'));
                    //$CI->load->library(array('Msg','Lang_controller'));
                    $this->load->library(array('Lang_controller'));
                    $this->load->helper(array('lang', 'our','url'));
                    
                    $this->template->set_template('questionnaire/template');

                    $this->_load_js();
                    $this->_load_css();

            }
       
    

    
    
     /**
     * load javascript
     */
    private function _load_js() {
        $this->template->javascript->add(base_url('assets/js/jquery/jquery.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap/bootstrap.bundle.min.js'));
        // $this->template->javascript->add(base_url('assets/js/frontend/instance.js'));

    }
    /**
     * load style sheet
     */
    private function _load_css() {

         $this->template->stylesheet->add(base_url('assets/css/bootstrap/bootstrap.min.css'));
         $this->template->stylesheet->add('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        
       

    }


    /**
     * change lang system
     */
    public function change_lang() {
       
        if ($this->input->post(NULL, FALSE)) {
            $this->session->set_userdata('language', $this->input->post('language'));
        }
    }
   
    public function getController() {
        return $this->controller;
    }

    public function setController($controller) {
        $this->controller = $controller;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        $this->method = $method;
    }
    public function getData($key = null) {
        if (is_null($key) || empty($key))
            return $this->data;
        else
            return $this->data[$key];
    }

    public function setData($key, $data) {
        $this->data[$key] = $data;
    }

    /**
     * จัด�?ารเรื่อง pagination
     * @param <type> $total จำนวน�?ถวทั้งหมด
     * @param <type> $cur_page หน้าปัจจุบัน
     */
    public function config_page($total, $cur_page) {
                $config['base_url'] = base_url();
                $config['total_rows'] = $total;
                $config['per_page'] = $this->page_num;

                $config['full_tag_open'] = '<ul class="pagination pull-right">';
                $config['full_tag_close'] = '</ul>';
                $config['num_tag_open'] = '<li class="page-item">';
                $config['num_tag_close'] = '</li>';

                $config['prev_tag_open'] = '<li class="page-item disabled">';
                $config['prev_tag_close'] = '</li>';
                $config['prev_link'] = '« ' . __('Previous', 'b2c_default');

                $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link">';
                $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
                $config['next_link'] = __('Next', 'b2c_default') . ' »';
                $config['next_tag_open'] = '<li class="page-item">';
                $config['next_tag_close'] = '</li>';
                $config['num_links'] = 3;

                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['first_link'] = __('First', 'b2c_default');
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['last_link'] = __('Last', 'b2c_default');
                $config['cur_page'] = $cur_page;
                $config['attributes'] = array('class' => 'page-link');
                $this->data['config_page'] = $config;
                $this->setData('config_page', $config);
    }
    
    
    
    
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>