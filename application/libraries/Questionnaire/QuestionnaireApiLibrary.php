<?php


class QuestionnaireApiLibrary{

    private $ci;
    function __construct() {
        $CI =& get_instance();
        $this->ci = $CI;
       // $CI->load->config('agent');
        $CI->load->config('api');

       // $this->apiConfigPWT = $CI->config->item('water_filter');
       
    }

    public function getArmActivity($requestCriteria){

        /* Get Activity Available */

        $data = $this->getActivityAvailable();
        if($data){
            
            return array(
                'status' => true,
                'activity' => $data,
                'is_active' => ($data)?true:fasle,
                'result_code' => '000',
                'result_desc' => 'success'
            );


        }else{

                    return array(
                    'status' => false,
                    'result_code' => '-002',
                    'result_desc' => 'ไม่พบกิจกรรมในขณะนี้',
                    'is_active'=>false

                    );


        }





    }

    private function getActivityAvailable(){

        $arrData = array();

        $query = $this->ci->db->select('*')
        ->from('ArmActivity')
        ->where('StartDate <=',date('Y-m-d'))
        ->where('EndDate >=',date('Y-m-d'))
        ->limit(1)
        ->get();

        if($query->num_rows() > 0){
            $row = $query->row();
            $row->{'Cover'} = base_url('uploaded/activity/'.$row->id.'/'.$row->Cover);
            $row->{'UriString'} = base_url($row->UriString);

            return $row;

        }else{
            return false;
        }


    }


}