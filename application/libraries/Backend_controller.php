<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
     class Backend_controller extends CI_Controller{
            
             public $meta_title = '';
             public $meta_keywords = '';
             public $meta_description = '';
             public $data = array();
             public $page_num = 10;
             public $controller = '';
             public $method = '';
             public $language = 'th';
             public $default_language;
             public $site_setting = '';
             public $account_data = array();
             private $conn_mysql;
             public $event_ability;
         public function __construct() {
                    parent::__construct();
                    $CI = & get_instance();
                    $this->setController($CI->router->class);
                    $this->setMethod($CI->router->method);

                    //$CI->config->set_item('enable_hooks',TRUE);
                   // $this->load->add_package_path('themes/admin');
                    $this->load->library(array('datamapper', 'Msg', 'Lang_controller'));
                    $this->load->helper(array('lang', 'our', 'inflector','html','cookie','url'));
                    
                    $this->template->set_template('backend/template');

                    $this->_checkAlready_signin();
                    $this->_getAccountData();

                    //$this->checkMenuPermission();

                    // print_r($this->session->userdata('member_data'));
                    $this->_load_js();
                    $this->_load_css();

                    //print_r($this->session->userdata());

                    //print_r($this->session->userdata('member_data'));
            }
     
    /**
     * load javascript
     */
    public function _load_js() {

            $this->template->javascript->add(base_url('assets/backend/vendors/jquery/dist/jquery.min.js'));
            $this->template->javascript->add(base_url('assets/backend/vendors/bootstrap/dist/js/bootstrap.min.js'));
            $this->template->javascript->add(base_url('assets/backend/vendors/fastclick/lib/fastclick.js'));
            $this->template->javascript->add(base_url('assets/backend/vendors/nprogress/nprogress.js'));
            $this->template->javascript->add(base_url('assets/backend/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'));
            $this->template->javascript->add(base_url('assets/backend/build/js/custom.min.js'));

            $this->template->javascript->add(base_url('assets/backend/individual/js/backendLibrary.js'));


    }
    /**
     * load style sheet
     */
    public function _load_css() {
            $this->template->stylesheet->add(base_url('assets/backend/vendors/bootstrap/dist/css/bootstrap.min.css'));
            $this->template->stylesheet->add(base_url('assets/backend/vendors/font-awesome/css/font-awesome.min.css'));
            $this->template->stylesheet->add(base_url('assets/backend/vendors/nprogress/nprogress.css'));
            $this->template->stylesheet->add(base_url('assets/vendors/iCheck/skins/flat/green.css'));
            $this->template->stylesheet->add(base_url('assets/backend/build/css/custom.min.css'));
            $this->template->stylesheet->add(base_url('assets/backend/individual/css/backend_style.css'));
    }
    
    public function _checkAlready_signin(){
        if(!$this->session->userdata('user_id')){
            redirect(base_url().'backend');
        }
    }

    public function _getAccountData(){
        if($this->session->userdata('member_data') && empty($this->account_data)){
            $this->account_data = $this->session->userdata('member_data');
        }

    }

    public function loadValidator(){
        $this->template->stylesheet->add(base_url('assets/backend/individual/css/bootstrapvalidator/bootstrapValidator.min.css'));
        $this->template->javascript->add(base_url('assets/backend/individual/js/bootstrapvalidator/bootstrapValidator.min.js'));

        if($this->config->item('language') == 'th'){
            $this->template->javascript->add(base_url('assets/backend/individual/js/bootstrapvalidator/language/th_TH.js'));
        }else{
            $this->template->javascript->add(base_url('assets/backend/individual/js/bootstrapvalidator/language/en_US.js'));
        }
    }

    public function loadSweetAlert(){
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js');
    }
    /**
     * get language all
     */
    public function _get_lang() {
        $lang = new M_language();
        if ($this->session->userdata('language'))
            $lang->where('foreign_language_name', $this->session->userdata('language'))->get();
        else
            $lang->where('foreign_language_name', $this->default_language)->get();
        $this->language_id = $lang->getId();
        $this->config->set_item('language', $lang->getForeign_language_name());
        $this->language = $lang->getForeign_language_name();
        $this->language_name = $lang->getLanguage_name();
    }
    
   
    /**
     * get default language of hotel
     */
    public function _default_langauge() {
        $shop_language = new M_gshop_shop_language();
        $shop_language->where('gshop_shop_id', $this->shop_id)->where('default_language', 1)->get();
        $this->default_language = $shop_language->language->get()->getForeign_language_name();
       // echo "default lang =".$this->default_language;
    }

    /**
     * change lang system
     */
    public function change_lang() {
       
        if ($this->input->post(NULL, FALSE)) {
            $this->session->set_userdata('language', $this->input->post('language'));
        }
    }

    /**
     *
     * @param <type> $file_view  ชื่อ file view
     * @param <type> $return true = return view �?ลับมา�?สดงเรย / false = ไม่ return view �?ลับ
     */
    public function setView($file_view = null, $return = false) {
        if ($return)
            $this->load->view($file_view, $this->getData());
        else
            $this->setData('the_content', $this->load->view($file_view, $this->getData(), true));
    }

    /**
     * �?สดง view
     */
    public function getView() {
        $this->load->view($this->getTheme(), $this->getData());
    }

    /**
     *
     * @return them template
     */
    public function getTheme() {
        return $this->theme;
    }

    public function setTheme($theme) {
        $this->theme = $theme;
    }

    public function getMeta_title() {
        return $this->meta_title;
    }

    public function setMeta_title($meta_title) {
        $this->setData('meta_title', $meta_title);
        $this->meta_title = $meta_title;
    }

    public function getMeta_keywords() {
        return $this->meta_keywords;
    }

    public function setMeta_keywords($meta_keywords) {
        $this->setData('meta_keywords', $meta_keywords);
        $this->meta_keywords = $meta_keywords;
    }

    public function getMeta_description() {
        return $this->meta_description;
    }

    public function setMeta_description($meta_description) {
        $this->setData('meta_description', $meta_description);
        $this->meta_description = $meta_description;
    }

    public function getData($key = null) {
        if (is_null($key) || empty($key))
            return $this->data;
        else
            return $this->data[$key];
    }

    public function setData($key, $data) {
        $this->data[$key] = $data;
    }

    public function getPage_num() {
        return $this->page_num;
    }

    public function setPage_num($page_num) {
        $this->page_num = $page_num;
    }

    public function getController() {
        return $this->controller;
    }

    public function setController($controller) {
        $this->setData('controller', $controller);
        $this->controller = $controller;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        $this->setData('method', $method);
        $this->method = $method;
    }

    /**
     * จัด�?ารเรื่อง pagination
     * @param <type> $total จำนวน�?ถวทั้งหมด
     * @param <type> $cur_page หน้าปัจจุบัน
     */
    public function config_page($total, $cur_page) {
                $config['base_url'] = base_url();
                $config['total_rows'] = $total;
                $config['per_page'] = $this->page_num;

                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['num_tag_open'] = '<li class="links">';
                $config['num_tag_close'] = '</li>';

                $config['prev_tag_open'] = '<li class="paging_btn">';
                $config['prev_tag_close'] = '</li>';
                $config['prev_link'] = '« ' . __('Previous', 'b2c_default');

                $config['cur_tag_open'] = '<li class="active"><a>';
                $config['cur_tag_close'] = '</a></li>';
                $config['next_link'] = __('Next', 'b2c_default') . ' »';
                $config['next_tag_open'] = '<li class="paging_btn">';
                $config['next_tag_close'] = '</li>';
                $config['num_links'] = 5;

                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['first_link'] = __('First', 'b2c_default');
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['last_link'] = __('Last', 'b2c_default');
                $config['cur_page'] = $cur_page;
                $this->data['config_page'] = $config;
                $this->setData('config_page', $config);
            }
    
    
    //================ IF SET COOKIE TO KEEP SIGN IN ==================
    public  function check_stay_signin($cookie_status,$cookie_accid){
			if($cookie_status == base64_decode(base64_decode('1'))){
				$user_data = array(
				'customer_id' => base64_decode(base64_decode($cookie_accid))
				);
				$this->session->set_userdata($user_data);	
                               // var_dump($this->session->userdata);exit;
			}
    }
    
    public function getPath_online(){
        switch (ENVIRONMENT){
            case "development":
                $this->path_online = "http://cooshop_system.local/";
            break;
            case "testing":
                
            break;
            case "production":
                
            break;
        }
        
    }
    public function getSite_setting(){
        $setting = new M_setting();
        $setting->where('id',1)->get();
        $this->site_setting = $setting;
    }
    protected function dataTableJSPackages(){
    
    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net/js/jquery.dataTables.min.js'));

    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'));

    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'));

    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-buttons/js/buttons.flash.min.js'));

    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-buttons/js/buttons.html5.min.js'));

    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-buttons/js/buttons.print.min.js'));

    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'));

    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js'));

    $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'));

     $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'));

     $this->template->javascript->add(base_url('assets/backend/vendors/datatables.net-scroller/js/dataTables.scroller.min.js'));
     
    }
    protected function dataTableCSSPackages(){
    
        $this->template->stylesheet->add(base_url('assets/backend/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'));

        $this->template->stylesheet->add(base_url('assets/backend/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'));

        $this->template->stylesheet->add(base_url('assets/backend/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'));

        $this->template->stylesheet->add(base_url('assets/backend/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'));

        $this->template->stylesheet->add(base_url('assets/backend/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'));

    }

    public function checkMenuPermission(){  
        /* get all menu */
        /* get first class and method available to access */
        $first_class = "";
        $first_method = "";

        $first_class = $this->account_data['member_permissions'][0]['controller_name'];
        $first_method = $this->account_data['member_permissions'][0]['method_name'];



        // print_r($this->account_data['member_permissions']);
        $checkpermission_class_method = checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>$this->controller,'method_name'=>$this->method));

        //print_r($checkpermission_class_method);
        $this->event_ability = json_decode((string)$checkpermission_class_method);

        if($checkpermission_class_method || in_array($this->controller, $this->account_data[
            'member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){
            $this->event_ability = json_decode((string)$checkpermission_class_method);
        }else{
            //echo 'aaaa';exit;
            redirect(base_url('backend/'.$first_class.'/'.$first_method));
        }
        //echo $this->controller.'<br>'.$this->method;

        /* eof check menu permission */

    }

 
    
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 

