<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class ProductLibrary{

    private $apiConfigPWT = array();
    private $onesignalConfig = array();
    private $onesignalFixitArmConfig = array();
    private $ci;
    function __construct() {
        $CI =& get_instance();
        $this->ci = $CI;

        $CI->load->config('api');

        // $CI->load->library(array('Lang_controller'));
        // $CI->load->helper(array('lang','our'));

        $this->apiConfigPWT = $CI->config->item('water_filter');
        $this->onesignalConfig  = $CI->config->item('onesignal');
        $this->onesignalFixitArmConfig = $CI->config->item('onesignal_fixitarm');

        // print_r($this->onesignalConfig);exit;

        // echo ENVIRONMENT;exit;
       
    }

    public function registerProduct($requestCriteria){

        // $this->clearWE2();exit;
        //$this->testFixitSendNotification();exit;

        // $this->getRegisterNotificationStringByProductType('PWT','2019-12-31 00:00:00.000');exit;
        $product = $this->getProductGroupBySerialNo($requestCriteria->serialno);

        //print_r($product);exit;
        $product_group = $product['MPGroupProduct'];
        // echo $product_group;exit;

        if(!$product_group){
                    $output = array(
                        'status' => false,
                        'result_code' => '-002',
                        'result_desc' => 'ไม่พบข้อมูลสินค้า'
                    );
                return $output;
        }

        switch ($product_group) {
            case 'SAT':
                return $this->__registerProductClassic($requestCriteria);
            break;
            case 'NET':     
                return $this->__registerProductClassic($requestCriteria);
            break;
            case 'OCS':
                return $this->__registerProductClassic($requestCriteria);
            break;
            case 'AIR':
                return $this->__registerProductClassic($requestCriteria);
            break;
            case 'PWT':
                return $this->__registerProductModern($requestCriteria,$product_group);
            break;
            case 'WE2':
                return $this->__registerProductModern($requestCriteria,$product_group);
            break;
            case 'PWP':
                    $output = array(
                        'status' => false,
                        'result_code' => '-007',
                        'result_desc' => 'ไม่สามารถลงทะเบียนสินค้านี้ได้'
                    );
                    return $output;
            break;
            case '':
                return $this->__registerProductModern($requestCriteria,$product_group);
            break;
            default:
                return $this->__registerProductModern($requestCriteria,$product_group);
            break;
        }

    }

    public function getProductInfo($requestCriteria){
        // print_r($requestCriteria);exit;
        $arReturn = array();
        // echo 'abcd';exit;



        // $customer_products = new M_customerproducts();
        // $customer_products->where('consumerid',$requestCriteria->consumerid)->get();

        $product_warranty = new M_productwarranty();
        $product_warranty->where('ConsumerID',$requestCriteria->consumerid)->get();


        if($product_warranty->result_count() > 0){
            $arProduct['ProductWarrantyInfos'] = array();
            foreach ($product_warranty as $key => $value) {
                $masterproduct = new M_masterproduct();
                $masterproduct->where('MPCODE',$value->MPCODE)->get();

                # code...   
                $data_push = array(
                    'expire_warranty' =>  date('d-m-Y H:i:s',strtotime($value->Expire_WarrantyDate)),
                    'productid' => $value->MPCODE,
                    'productimage' => $masterproduct->ProductImage,
                    'productname' => $masterproduct->MPDESCRIPTION,
                    'product_type' => $masterproduct->MPGroupProduct,
                    'result_code' => '000',
                    'result_desc' => 'Success',
                    'serialno' => $value->SerialNO,
                    'start_warranty' => date('d-m-Y H:i:s',strtotime($value->Start_WarrantyDate)),
                    'warranty_status' => 'normal'

                );


                array_push($arProduct['ProductWarrantyInfos'], $data_push);
            }
            $arProduct['result_code'] = '000';
            $arProduct['result_desc'] = 'Success';
            return $arProduct;

        }else{

                        $output = array(
                            'status' => false,
                            'result_code' => '-002',
                            'result_desc' => 'ไม่พบข้อมูลการประกันสินค้า'
                        );
                        return $output;
        }

        
    }

    public function getProductDetail($requestCriteria){
       
        $arReturn = array();


        $product_warranty = new M_productwarranty();
        $product_warranty->where('SerialNO',$requestCriteria->serialno)->get();

        if($product_warranty->result_count()>0){

            $start_warranty = new Datetime($product_warranty->Start_WarrantyDate);
            $to_day = new Datetime();
            $expire_warranty = new Datetime($product_warranty->Expire_WarrantyDate);

            $intervalExpire = $start_warranty->diff($expire_warranty);

            $intervalToday = $start_warranty->diff($to_day);

            
            $passed_percent = 0.00;
            $passed_percent = ($intervalToday->days/$intervalExpire->days)*100;
            $passed_percent = ($passed_percent <= 100 && $passed_percent >= 0)?$passed_percent:100;

            $defaultDataArray = array(
                    'consumerid' => $product_warranty->ConsumerID,
                    'serial_no' => $product_warranty->SerialNO,
                    'warranty_start_date' => $product_warranty->Start_WarrantyDate,
                    'warranty_expire_date' => $product_warranty->Expire_WarrantyDate,
                    'max_month' => (int)($intervalExpire->y*12),
                    'passed_month' => (int)$intervalToday->m,
                    'passed_percent' => (float)$passed_percent,
                    'productid' => $product_warranty->MPCODE
             );
            $arProductTimeLine = $this->getProductLogs($product_warranty->SerialNO);
            $arReturn = $defaultDataArray;

            $arReturn['product_timeline'] = $arProductTimeLine;
            $arReturn['product_part_status'] = $this->getPWTProductPartStatus($requestCriteria->serialno);
            $arReturn['agent_data'] = $this->getAgentData($product_warranty->SerialNO);
            $arReturn['status'] = true;
            $arReturn['result_code'] = "000";
            $arReturn['result_desc'] = "Success";
            return $arReturn;
        }else{

                        $output = array(
                            'status' => false,
                            'result_code' => '-002',
                            'result_desc' => 'ไม่พบข้อมูลสินค้า'
                        );
                        return $output;
        }
        


    }

    public function getProductPartDetail($requestCriteria){

            $arReturn = array();

            $product_part_notification = new M_productpartnotification();
            $product_part_notification->where('serial_no',$requestCriteria->serialno)->get();

            if($product_part_notification->result_count() > 0){

                $arReturn['product_part_warranty'] = $this->getProductPartWarranty($product_part_notification->serial_no);
                $arReturn['product_part_status'] = $this->getPWTProductPartStatus($requestCriteria->serialno);
                $arReturn['result_code'] = '000';
                $arReturn['result_desc'] = 'Success';
                return $arReturn;

            }else{
                        $output = array(
                            'status' => false,
                            'result_code' => '-002',
                            'result_desc' => 'ไม่พบข้อมูลสินค้า'
                        );
                        return $output;

            }

    }

    public function setProductNotification($requestCriteria){

                /* Get ProductPartNotification record that has to remind and change filter */
                $notification_records = new M_productpartnotification();
                $notification_records->where('product_part_next_notification_date',date('Y-m-d'))->where('product_part_status_id < ',3)->get();

                
                $arAllToken = array();
                if($notification_records->result_count() > 0){

                        //print_r($notification_records->to_array());exit;
                        $arToken = array();
                        foreach($notification_records as $key => $value){

                            /* older version
                            $customer_product = new M_customerproducts();
                            $customer_product->where('serial_no',$value->serial_no)->get();
                            */
                            $product_warranty = new M_productwarranty();
                            $product_warranty->where('SerialNO',$value->serial_no)->get();

                           

                            $fixit_customer = new M_fixitcustomer();
                            $fixit_customer->where('CustID',$product_warranty->ConsumerID)->get();

                            $product_status = new M_productpartstatus();
                            $product_status->where('id',$value->product_part_next_status_id)->get();


                            $data_insert = array(
                                    'id' => $value->id,
                                    'timely_date' => date('Y-m-d',strtotime($value->warranty_expire_date)),
                                    'remind_type' => $product_status->name,
                                    'Device_Token' => $fixit_customer->Device_Token,
                                    'CustID'=>$fixit_customer->CustID,
                                    'product_part_notification' => $value->productpart->get()->part_name,
                                    'product_part_id' => $value->ProductPart_id
                            );


                            array_push($arAllToken, $fixit_customer->Device_Token);
                            /* eof get title from status */
                            array_push($arToken, $data_insert);







                        }
                        $arAllToken = array_unique($arAllToken);

                        //print_r($arToken);exit;

                        $arArange = array();
                        /* arrange array */
                        foreach ($arAllToken as $key => $value) {
                                # code...
                                $arArange[$key] = array(
                                        'token' => $value,
                                        'part_notification' => array(),
                                        'part_notification_id' => array(),
                                        'product_part_id' => array(),
                                        'remind_type' => array(),
                                        'timely_date' => array()
                                );
                                foreach ($arToken as $k => $v) {
                                    # code...
                                    
                                    if($value == $v['Device_Token']){
                                        $arArange[$key]['CustID'] = $v['CustID'];
                                        array_push($arArange[$key]['product_part_id'], $v['product_part_id']);
                                        array_push($arArange[$key]['part_notification'],$v['product_part_notification']);
                                        array_push($arArange[$key]['part_notification_id'], $v['id']);
                                        array_push($arArange[$key]['remind_type'], $v['remind_type']);
                                        array_push($arArange[$key]['timely_date'], $v['timely_date']);
                                    }
                                }

                        }

                        /* eof arrange array */

                        //print_r($arArange);exit;

                        /* remind to technicial */

                        /* send notification into fixit arm for agent */
                            $this->setProductPartNotificationToAgent(array(
                                'arArange'=>$arArange,
                                'notification_records'=>$notification_records
                            ));
                        /* eof send notification into fixit arm for agent */
                        
                        //print_r($arArange);exit;
                        foreach ($arArange as $key => $value) {
                            # code...
                            // echo $value['remind_type'][0];exit;
                            $text_content_title = $this->getNotificationTitleString($value['remind_type'][0]);
                            // echo 'text title = '. $text_content_title.'<br>';
                            $text_content = $this->getNotificationString($value['part_notification']);
                            // echo 'text content = '.$text_content.'<br>';

                            $text_content_footer = $this->getNotificationFooterString($value['remind_type'][0],$value['timely_date'][0]);

                            $content = array(
                                'en' => $text_content_title.$text_content.$text_content_footer
                            );
                            //print_r($content);exit;

                            $fields = array(
                                'app_id' => $this->onesignalConfig['app_id'],
                                'include_player_ids' => array($value['token']),
                                'data' => array("jobstatus" => "ACCEPT"),
                                'contents' => $content
                            );

                            $post_data = json_encode($fields);
                            // echo $post_data;

                           
                            $response = json_decode($this->onesignalCurlNotification($post_data));

                            if($response->recipients > 0){
                                /* update status and insert data to log table */

                                $this->updateProductPartNotificationByStatus($value['part_notification_id'],$value['remind_type'],$value['timely_date']);
                                
                                /* update for notification logs */
                                $this->updateProductNotificationLogs($value,"send");


                            }else{
                                /* jus record to log table was uncoutable */

                                $this->updateProductNotificationLogs($value,"unsend");

                            }

                            

                        }

                        $output = array(
                            'status' => true
                        );



                }else{
                        $output = array(
                            'status' => false,
                            'error' => array(
                                'title' => 'no-record-for-notification',
                                'messages' => 'We could not found record for notification now.'
                            )
                        );

                }


                return $output;
        
                
    }

    public function resetProductPartWarranty($requestCriteria){

            // print_r($requestCriteria);exit;
            /* check already sn */
            if(property_exists($requestCriteria, 'barcode') && $requestCriteria->barcode != ""){
                $checkProductSn = new M_masterproductsn();
                $checkProductSn->where('SerialNO',$requestCriteria->barcode)->get();

                if($checkProductSn->result_count() <= 0){

                    return array(
                            'status' => false,
                            'result_code' => '-002',
                            'result_desc' => 'ไม่พบซีเรียลนัมเบอร์ดังกล่าว'
                    );
                }else{

                    /* check this barcode match with purified */
                        $product_part_notification = new M_productpartnotification();
                        $product_part_notification->where('id',$requestCriteria->product_part_id)->get();

                        //echo $product_part_notification->check_last_query();exit;

                        //echo $product_part_notification->check_last_query();exit;
                        $product_notification_mp_code = $product_part_notification->productpart->get()->MPCODE;

                        if($product_notification_mp_code != $checkProductSn->MPCODE){

                            /* check level 2 check on product part replacement mpcode */
                            $product_part_replacement = new M_productpartreplacement();
                            $product_part_replacement->where('ProductPart_id',$product_part_notification->ProductPart_id)->get();

                            if($product_part_replacement->result_count() > 0){
                                $arr_replacement = [];
                                foreach ($product_part_replacement as $key => $value) {
                                    # code...
                                    array_push($arr_replacement, $value->MPCODE);
                                }

                                if(!in_array($checkProductSn->MPCODE, $arr_replacement)){
                                    return array(
                                        'status' => false,
                                        'result_code' => '-007',
                                        'result_desc' => 'ไม่สามารถเปลี่ยนอุปกรณ์ของคุณกับช่องที่มีอยู่ได้'
                                    );
                                }
                            }else{

                                /* eof check level 2 check on product part replacement mpcode */

                                return array(
                                    'status' => false,
                                    'result_code' => '-007',
                                    'result_desc' => 'ไม่สามารถเปลี่ยนอุปกรณ์ของคุณกับช่องที่มีอยู่ได้'
                                );
                            }
                        }
                        //echo $product_notification_mp_code;

                    /* eof check barcode match type */
                }

            }

            /* eof check already sn*/

            /* check product part notification id first */
                $productpartnotification = new M_productpartnotification();
                $productpartnotification->where('id',$requestCriteria->product_part_id)->get();
            /* eof check product part notification */
                if($productpartnotification->id){

                    /* checkproductpart already use */
                    if(property_exists($requestCriteria, 'barcode') && $this->checkProductPartAlreadyUse(@$requestCriteria->barcode)){
                        return array(
                            'status' => false,
                            'result_code' => '-006',
                            'result_desc' => 'สินค้าถูกใช้งานแล้ว กรุณาลองสินค้าชิ้นอื่น.'
                        );
                    }
                    /* eof check product part already use*/

                    /* origin warranty */
                    $warranty_days = $productpartnotification->warranty_days;


                    /* new start date warranty and end date warranty*/
                    $newStartDateWarranty = new DateTime();
                    $newEndDateWarranty = $newStartDateWarranty->add(new DateInterval('P'.(int)$warranty_days.'D'));
                    $early_remind = $newEndDateWarranty->sub(new DateInterval('P'.$this->apiConfigPWT['early_remind_minus_days'].'D'));

                    // echo $early_remind->format('Y-m-d');

                    $productpartnotification->product_part_status_id = 0;
                    $productpartnotification->product_part_next_status_id = 1;
                    $productpartnotification->product_part_next_notification_date = $early_remind->format('Y-m-d');
                    $productpartnotification->updated = date('Y-m-d H:i:s');
                    $productpartnotification->warranty_expire_date = $newEndDateWarranty->format('Y-m-d');
                    $productpartnotification->warranty_start = date('Y-m-d');

                    // $productpartnotification->save();

                    if($productpartnotification->save()){

                        /* insert data to productpartnotificationresetlog */
                            $resetlog = new M_productpartnotificationresetlogs();
                            $resetlog->ProductPartNotification_id = $productpartnotification->id;
                            $resetlog->log_message = json_encode($productpartnotification->to_array());
                            $resetlog->created = date('Y-m-d H:i:s');
                            $resetlog->updated = date('Y-m-d H:i:s');
                            $resetlog->barcode = ($requestCriteria->barcode && $requestCriteria->barcode != '')?$requestCriteria->barcode:'';
                            $resetlog->save();

                        /* eof insert data */
                        $output = array(
                            'status' => true,
                            'result_code' => '000',
                            'result_desc' => 'ทำการเปลี่ยนไส้กรองเรียบร้อยแล้ว'
                        );
                        return $output;
                    }

                }else{
                        $output = array(
                            'status' => false,
                            'result_code' => '-006',
                            'result_desc' => 'สินค้าถูกใช้งานแล้ว กรุณาลองสินค้าชิ้นอื่น'
                        );
                        return $output;

                }

            


    }

    public function checkRegisterProductSN($requestCriteria){
            // $this->getRegisterNotificationStringByProductType('PWT','2019-12-31 00:00:00.000');exit;
        $product = $this->getProductGroupBySerialNo($requestCriteria->serialno);


        if(@$_GET['test'] == 'test'){
            print_r($product);exit;
        }
        // print_r($product);exit;
        //echo $product_group;exit;

        if(empty($product)){
                    $output = array(
                        'status' => false,
                        'result_code' => '-002',
                        'result_desc' => 'ไม่พบข้อมูลสินค้า'
                    );
                return $output;
        }else{




                if($this->checkProductAlreadyRegister($product['serial_no'])){
                            $output = array(
                                'status' => false,
                                'productid' => $product['MPCODE'],
                                'productname' => $product['MPNAME'],
                                'producttype' => $product['MPGroupProduct'],
                                'productimage' => $product['ProductImage'],
                                'serialstatus' => 'EXISTS',
                                'productstatus' => 'OK',
                                'result_code' => '000',
                                'result_desc' => 'Success'
                            );
                            return $output;
                }

                return array(
                    'status' => true,                
                    'productid' => $product['MPCODE'],
                    'productname' => $product['MPNAME'],
                    'producttype' => $product['MPGroupProduct'],
                    'productimage' => $product['ProductImage'],
                    'serialstatus' => 'OK',
                    'productstatus' => 'OK',
                    'result_code' => '000',
                    'result_desc' => 'Success'

                );

            
        }

        // echo 'abcd';


    }

    //========================== for campaign ==================================
    //==========================================================================
    public function campaignCronSendInsuranceToTip($requestCriteria){
            /* first select all of insurace register today */
            $arrKeepId = array();
            $camp = new M_campaigninsuranceregister();
            //$camp->where('CONVERT(char(10), created,126) = ',date('Y-m-d'))->where('active',1)->get();
            $camp->where('CONVERT(char(10), created,126) = ',date('Y-m-d'))->where('active',1)->get();
            //echo $camp->check_last_query();exit;

            $arrRecord = array();
            foreach ($camp as $key => $value) {
                # code...
               array_push($arrRecord, $value->to_array());

               $arrRecord[$key]['province'] = $value->provinces->get()->province_name;
               $arrRecord[$key]['amphur'] = $value->amphurs->get()->amphur_name;
               $arrRecord[$key]['district'] = $value->districts->get()->district_name;
            }

            $ids = array_column($arrRecord, 'ConsumerID');
            $ids = array_unique($ids);
            $arrRecord = array_filter($arrRecord, function ($key, $value) use ($ids) {
                return in_array($value, array_keys($ids));
            }, ARRAY_FILTER_USE_BOTH);

            // print_r($arrRecord);

            // exit;

            //echo $camp->check_last_query();exit;
            // $camp->where('CONVERT(char(10), created,126) = ','2018-05-21')->where('active',1)->get();
           
            //print_r($camp->to_array());
            $filename = "";
            $filename = "PsiTipDailyInsurance_".date('d-m-Y').'.xlsx';
            // $filename = "PsiTipDailyInsurance_2018-05-21.xlsx";



            require_once 'assets/backend/plugin/PHPExcel/Classes/PHPExcel.php';


                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();

                //exit;

                // Set document properties
                $objPHPExcel->getProperties()->setCreator("PSI CORP")
                                             ->setLastModifiedBy("PSIFIXIT Admin")
                                             ->setTitle($filename)
                                             ->setSubject("Excel file for register insurance")
                                             ->setDescription("Excel file for register insurance")
                                             ->setKeywords("Tip insurance register")
                                             ->setCategory("Report");

                //print_r($arDataOrder);


                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1')->setCellValue('A1',$filename);
                $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A2','#')
                            ->setCellValue('B2','ลักษณะบ้าน')
                            ->setCellValue('C2','คำนำหน้าชื่อ')
                            ->setCellValue('D2','ชื่อ')
                            ->setCellValue('E2','นามสกุล')
                            ->setCellValue('F2','เบอร์โทร')
                            ->setCellValue('G2','อีเมล์')
                            ->setCellValue('H2','บ้านเลขที่')
                            ->setCellValue('I2','จังหวัด')
                            ->setCellValue('J2','อำเภอ')
                            ->setCellValue('K2','ตำบล')
                            ->setCellValue('L2','รหัสไปรษณีย์')
                            ->setCellValue('M2','หมายเลขอ้างอิง');

                $count = 1;
                $table_row = 3;
                foreach ($arrRecord as $key => $row) {
                    
                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$table_row.'',@$count)
                                ->setCellValue('B'.$table_row.'',@$row['home_type'])
                                ->setCellValue('C'.$table_row.'',@$row['title'])
                                ->setCellValue('D'.$table_row.'',@$row['firstname'])
                                ->setCellValue('E'.$table_row.'',@$row['lastname'])
                                ->setCellValue('F'.$table_row.'',@$row['telephone'])
                                ->setCellValue('G'.$table_row.'',@$row['email'])
                                ->setCellValue('H'.$table_row.'',@$row['address_house_no'])
                                ->setCellValue('I'.$table_row.'',@$row['province'])
                                ->setCellValue('J'.$table_row.'',@$row['amphur'])
                                ->setCellValue('K'.$table_row.'',@$row['district'])
                                ->setCellValue('L'.$table_row.'',@$row['zipcode'])
                                ->setCellValue('M'.$table_row.'',@$row['ref_code']);
                                $count++;
                                $table_row++;
                                array_push($arrKeepId, $row['id']);
                }


                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('A')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('B')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('C')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('D')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('E')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('F')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('G')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('H')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('I')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('J')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('K')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('L')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('M')
                ->setAutoSize(true);


                // Rename worksheet
                $objPHPExcel->getActiveSheet()->setTitle('InsuranceReport');


                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);


                // Redirect output to a client’s web browser (Excel2007)
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$filename.'"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');

                // If you're serving to IE over SSL, then the following may be needed
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $file_path = './cron/campaign/tip/excel_file/'.$filename;

                if(file_exists($file_path)){
                    unlink($file_path);
                }

                try {
                    $objWriter->save($file_path);

                    // exit;
                    if($this->sendEmailToTip(array('file_path'=>$file_path,'campaign_insurance_register'=>$camp
                    ))){
                        $array_return =array(
                            'status'=>true,
                            'record_id'=>json_encode($arrKeepId),
                            'record_total'=>count($arrKeepId)
                        );
                        /* create log for debugging*/
                        $log_file_path = $this->createTipLogFilePath('SendInsuranceToTipSuccess');
                        $file_content = date("Y-m-d H:i:s") . ' error : ' . json_encode($array_return) . "\n";
                        file_put_contents($log_file_path, $file_content, FILE_APPEND);
                        unset($file_content);


                        return $array_return;

                        
                    }
                } catch (Exception $e) {
                    echo 'ERROR: ', $e->getMessage();
                    die();
                }

                
                

            //print_r($query->result());

    }

    public function testSendExpiredWaterFilter_get($requestCriteria){
        //print_r($requestCriteria);
                    $tech_content_title = "แจ้งเตือนเปลี่ยนไส้กรองเครื่องแยกน้ำที่คุณไปติดตั้ง ดังนี้ ";

                    $tech_content = "กรองที่ 1 (First Filter)";

                    $tech_content_footer = $this->getTechnicialNotificationFooterString('early','29-11-2018','edfdb88f1bdef6c');

                    $content = array(
                        'en' => $tech_content_title.$tech_content.$tech_content_footer
                    );
                    //print_r($content);exit;

                    $fields = array(
                        'app_id' => $this->onesignalFixitArmConfig['app_id'],
                        'include_player_ids' => array('c513d7fd-d269-4786-b834-8fa3aa9e2455'),
                        'data' => array("jobstatus" => "ACCEPT"),
                        'contents' => $content
                    );

                    $post_data = json_encode($fields);
                    // echo $post_data;

                                       
                    $response = json_decode($this->onesignalCurlFixitArmNotification($post_data));

                    if($response->recipients > 0){

                    }

    }

    private function __registerProductClassic($requestCriteria){

                    /* check product warranty first */
        if($this->__checkAlreadySerialNumber($requestCriteria->serialno)){
                $error = array(
                    'status'=>false,
                    'result_code' => '-006',
                    'result_desc' => 'สินค้าชิ้นนี้ถูกลงทะเบียนแล้ว กรุณาตรวจสอบซีเรียลนัมเบอร์ของสินค้า'
                );
            return $error;
        }else{

            $warrantyMonth = 0;
            $datetimeNow = new Datetime();
            $datetimeEndWarranty = $datetimeNow;
            // print_r($datetimeNow->format('Y-m-d H:i:s'));exit;
            /* Get warranty policy */
            $startWarranty = new Datetime();
            $startWarrantyTxt = "";

            $masterproduct = new M_masterproduct();
            $masterproduct->where('MPCode',$requestCriteria->productid)->get();

            if($masterproduct->result_count() > 0){
                    $warrantyMonth = $masterproduct->WarrantyMonths;
            }

            /* check if agent already register in job */
            $queryJobProduct = $this->ci->db->select('*')
            ->from('FixITJobProduct')
            ->where('SerialNO',$requestCriteria->serialno)
            ->get();

            if($queryJobProduct->num_rows() > 0){
                $rowJobProduct = $queryJobProduct->row();
                $startWarranty = new Datetime($rowJobProduct->Created);
                $startWarrantyTxt = $startWarranty->format('Y-m-d H:i:s');

                $datetimeEndWarranty = $startWarranty->add(new DateInterval('P'.(int)$warrantyMonth.'M'));


            }else{
                    $startWarrantyTxt = $datetimeNow->format('Y-m-d H:i:s');
                    $datetimeEndWarranty = $datetimeNow->add(new DateInterval('P'.(int)$warrantyMonth.'M'));
                        
            }


            

            // print_r($startWarrantyTxt);
            // print_r($datetimeEndWarranty);
            // exit;

            $setProductWarranty = new M_productwarranty();
            $setProductWarranty->SerialNO = $requestCriteria->serialno;
            $setProductWarranty->MPCODE = $requestCriteria->productid;
            $setProductWarranty->ConsumerID = $requestCriteria->consumerid;
            $setProductWarranty->Start_WarrantyDate = $startWarrantyTxt;
            $setProductWarranty->Expire_WarrantyDate = $datetimeEndWarranty->format('Y-m-d H:i:s');

            if($setProductWarranty->save()){

                /* send notification to customer */
                $customer_token = "";
                $product_name = "";
                $fixitcustomer = new M_fixitcustomer();
                $fixitcustomer->where('CustID',$requestCriteria->consumerid)->get();

                if($fixitcustomer->result_count() > 0){
                    $customer_token = $fixitcustomer->Device_Token;
                }
                $product_name = $masterproduct->MPDESCRIPTION;

                $txt_notification = "คุณได้ทำการลงทะเบียนรับประกันสินค้า ".$product_name." เรียบร้อย";
                $data_notification = array(
                    'text_notification'=>$txt_notification,
                    'customer_token'=>$customer_token
                );

                $this->sendRegisterProductClassiceNotification($data_notification);

                /* eof send notification to customer */

                /* insert data to */
                $output = array(
                    'status' => true,
                    'result_code' => '000',
                    'result_desc' => 'Success'

                );
                return $output;


            }else{
                $output = array(
                    'status' => false,
                    'result_code' => '-003',
                    'result_desc' => 'ไม่สามารถบันทึกข้อมูลรับประกันสินค้าได้ กรุณาลองใหม่อีกครั้ง'
                );
                return $output;
            }




        }

    }

    private function __registerProductModern($requestCriteria,$product_group){
          /* check already serial number from ProductPartNotification */
            if($this->__checkAlreadySerialNumberModern($requestCriteria->serialno)){
                // echo 'aaaa';
                        $error = array(
                            'status'=>false,
                            'result_code' => '-006',
                            'result_desc' => 'สินค้าชิ้นนี้ถูกลงทะเบียนแล้ว'
                        );
                        return $error;

            }else{

                    switch ($product_group) {
                        case 'PWT':
                        
                            return  $this->__registerProductModernPWT($requestCriteria);
                        break;
                        
                        case 'WE2':
                            return $this->__registerProductModernWE2($requestCriteria);
                        break;

                        default:
                            # code...
                            break;
                    }




            }

          /* eof check already serial number form ProductPartNotifivation*/



    }

    private function __registerProductModernPWT($requestCriteria){

                            // $ppmValue = "M";
                            $startWarranty = date('Y-m-d H:i:s');
                            /* check already register with technician */
                            $fixitjobproduct = new M_fixitjobproduct();
                            $fixitjobproduct->where('SerialNO',$requestCriteria->serialno)->where('Is_Install','T')->get();


                            if($fixitjobproduct->result_count() > 0){

                                $requestCriteria->ppm = $fixitjobproduct->PPM;
                                $startWarranty = date('Y-m-d H:i:s',strtotime($fixitjobproduct->Created));

                            }


                            //print_r($requestCriteria);exit;
                            // exit;


                            /* eof check already register with technician */

                            /* Calculate product warranty by density of water */
                            $warranty_data = array();
                    
                            $warranty_data = $this->getPPMWarrantyDays($requestCriteria->ppm);

                            //print_r($warranty_data);exit;
                            //print_r($warranty_days);exit;

                            if(!$warranty_data){
                                    return array(
                                        'status' => false,
                                        'result_code' => '-007',
                                        'result_desc' => 'คุณไม่สามารถลงทะเบียนได้เนื่องจากค่า PPM เกินจากมาตรฐาน'

                                    );

                            }






                            /* check product part warranty */
                            $warranty = new M_warranty();
                            $warranty->where('value',$warranty_data['warranty_value'])->where('type',$warranty_data['warranty_unit'])->get();

                            //echo $warranty->check_last_query();exit;


                            if($warranty->result_count() <= 0){
                                /* insert new warranty id*/
                                $insert_warranty = new M_warranty();
                                $insert_warranty->value = $warranty_data['warranty_value'];
                                $insert_warranty->type = $warranty_data['warranty_unit'];
                                $insert_warranty->save();

                            }



                            //echo 'aaa';exit;

                            //print_r($warranty_days);exit;

                            
                                /* get product id from prefix */
                                $product = new M_product();
                                $product->where('code',$this->apiConfigPWT['prefix'])->get();
                                
                                // $product_machine_warranty = $product->warranty->get();

                                //     $this->setCustomerProduct($requestCriteria,$product_machine_warranty,'PWT',$startWarranty);
                                // exit;


                                if($product->result_count() > 0){
                                   $product_part = new M_productpart();
                                   $product_part->where('Product_id',$product->id)->get();

                                   $product_noti_array = array();
                                   foreach ($product_part as $key => $value) {
                                            # code...
                                            /* insert this data to table product part notification */

                                            if($value->id == $this->apiConfigPWT['product_part_fix_month_id']){
                                                $data_warranty = $value->warranty->get();
                                                $part_warranty =new stdClass();
                                                $part_warranty->{'value'} = $data_warranty->value;
                                                $part_warranty->{'type'} = $data_warranty->type;
                                            }else{
                                                $part_warranty =new stdClass();
                                                $part_warranty->{'value'} = $warranty_data['warranty_value'];
                                                $part_warranty->{'type'} = $warranty_data['warranty_unit'];
                                               
                                            }
                                            //exit;
                                            
                                            $dateWarrantyArray = $this->__calculateRemindDate(1,$part_warranty,$startWarranty);

                                            //print_r($dateWarrantyArray);exit;

                                            $insert_notification = new M_productpartnotification();
                                            $insert_notification->ProductPart_id = $value->id;
                                            $insert_notification->serial_no = $requestCriteria->serialno;
                                            $insert_notification->product_part_status_id = 0;
                                            $insert_notification->product_part_next_status_id = 1;
                                            $insert_notification->product_part_next_notification_date = $dateWarrantyArray['next_notification_date'];

                                            // echo $insert_notification->product_part_next_notification_date;exit;
                                            $insert_notification->updated = date('Y-m-d H:i:s');
                                            $insert_notification->warranty_expire_date = $dateWarrantyArray['warranty_expire_date'];
                                            $insert_notification->warranty_start = $startWarranty;
                                            $insert_notification->active = 1;
                                            $insert_notification->warranty_days = $dateWarrantyArray['warranty_days'];
                                            $insert_notification->save();
                                            array_push($product_noti_array, $insert_notification->to_array());

                                   }



                                   $arProductPart = array();
                                   $rProductPart = new M_productpart();

                                   // print_r($rProductPart->to_array());exit;
                                   foreach ($rProductPart->get() as $key => $value) {
                                       array_push($arProductPart, $value->to_array());
                                   }

                                   $arProductPartStatus = array();
                                   $rProductPartStatus = new M_productpartstatus();
                                   foreach ($rProductPartStatus->get() as $key => $value) {
                                       array_push($arProductPartStatus, $value->to_array());
                                   }

                                   /* insert data to ProductLogs table */
                                    $this->setProductLogs('Register Product',$requestCriteria);
                                    $product_machine_warranty = $product->warranty->get();

                                    $this->setCustomerProduct($requestCriteria,$product_machine_warranty,'PWT',$startWarranty);

                                    

                                    /* eof insert data to ProductLogs table */

                                   $output = array(
                                        'status' => true,
                                        'result_code' => '000',
                                        'result_desc' => 'Success'
                                    );        

                                   return $output;

                                }else{
                                    $output = array(
                                        'status' => false,
                                        'result_code' => '-002',
                                        'result_desc' => 'ไม่พบสินค้าประเภทดังกล่าว'
                                    );
                                    return $output;
                                }
                                // exit;



                    // echo $warranty_days;exit;
    }

    private function __registerProductModernWE2($requestCriteria){



            $startWarranty = date('Y-m-d H:i:s');
            /* check already register with technician */
            $fixitjobproduct = new M_fixitjobproduct();
            $fixitjobproduct->where('SerialNO',$requestCriteria->serialno)->where('Is_Install','T')->get();


            if($fixitjobproduct->result_count() > 0){

                $requestCriteria->ppm = $fixitjobproduct->PPM;
                $startWarranty = date('Y-m-d H:i:s',strtotime($fixitjobproduct->Created));

            }

            //print_r($startWarranty);exit;
            $we2_config = $this->ci->config->item('we2');

            /*check ppm over limited*/
            if((int)$requestCriteria->ppm >= $we2_config['ppm_over_start']){
                        return array(
                            'status' => false,
                            'result_code' => '-007',
                            'result_desc' => 'คุณไม่สามารถลงทะเบียนได้เนื่องจากค่า PPM เกินจากมาตรฐาน'

                        );
            } 

            $product = new M_product();
            $product->where('code',trim($we2_config['prefix']))->get();
            
            // echo $product_pwt2->check_last_query();exit;
            //print_r($product->result_count());exit;
            //print_r($product->warranty->get()->value);exit;

            if($product->result_count() > 0){
                    $product_part = new M_productpart();
                    $product_part->where('Product_id',$product->id)->get();

                    $product_noti_array = array();
                    foreach ($product_part as $key => $value) {
                        # code...
                        //print_r($value->to_array());
                        if($value->id == $we2_config['product_part_fix_month_id']){
                            $data_warranty = $value->warranty->get();
                            $part_warranty =new stdClass();
                            $part_warranty->{'value'} = $data_warranty->value;
                            $part_warranty->{'type'} = $data_warranty->type;
                        }else{
                            $warranty_data = $this->getWE2WarrantyDataByConfig([
                                'requestCriteria'=>$requestCriteria,
                                'product_part_mpcode'=>$value->MPCODE
                            ]);

                            $part_warranty =new stdClass();
                            $part_warranty->{'value'} = $warranty_data['warranty'];
                            $part_warranty->{'type'} = $warranty_data['warranty_unit'];
                                               
                        }

                        //print_r($part_warranty);
                        $dateWarrantyArray = $this->__calculateRemindDate(1,$part_warranty,$startWarranty);

                        $insert_notification = new M_productpartnotification();
                        $insert_notification->ProductPart_id = $value->id;
                        $insert_notification->serial_no = $requestCriteria->serialno;
                        $insert_notification->product_part_status_id = 0;
                        $insert_notification->product_part_next_status_id = 1;
                        $insert_notification->product_part_next_notification_date = $dateWarrantyArray['next_notification_date'];

                                            
                        $insert_notification->updated = date('Y-m-d H:i:s');
                        $insert_notification->warranty_expire_date = $dateWarrantyArray['warranty_expire_date'];
                        $insert_notification->warranty_start = $startWarranty;
                        $insert_notification->active = 1;
                        $insert_notification->warranty_days = $dateWarrantyArray['warranty_days'];
                        $insert_notification->save();
                        array_push($product_noti_array, $insert_notification->to_array());
                    }

                    /* insert data to ProductLogs table */
                    $this->setProductLogs('Register Product',$requestCriteria);
                    $product_machine_warranty = $product->warranty->get();

                    $this->setCustomerProduct($requestCriteria,$product_machine_warranty,'WE2',$startWarranty);

                    $output = array(
                        'status' => true,
                        'result_code' => '000',
                        'result_desc' => 'Success'
                    );        

                    return $output;


            }else{
                    $output = array(
                        'status' => false,
                        'result_code' => '-002',
                        'result_desc' => 'ไม่พบสินค้าประเภทดังกล่าว'
                    );
                    return $output;

            }







            // print_r($we2_config);exit;



    }

    private function __calculateRemindDate($status,$part_warranty,$startDate){
        $dataReturn = array();

        // print_r($part_warranty);exit;
        switch ($status) {
            case '1': //---- early remind
                    $today = new DateTime($startDate);
                    $early_remind_minus_date = $this->apiConfigPWT['early_remind_minus_days'];
                    if($part_warranty->type == 'day') {
                            //echo 'aaa';exit;
                            $warranty_date = $today->add(new DateInterval('P'.$part_warranty->value.'D'));
                            $new_today = new Datetime($startDate);
                            $interval = $new_today->diff($warranty_date);
                            $warranty_date_txt = $warranty_date->format('Y-m-d');
                            

                    }else if($part_warranty->type == 'month'){

                            $warranty_date = $today->add(new DateInterval('P'.$part_warranty->value.'M'));
                            $new_today = new Datetime($startDate);
                            $interval = $new_today->diff($warranty_date);
                            $warranty_date_txt = $warranty_date->format('Y-m-d');
                            
                    }else if($part_warranty->type == 'year'){

                            $warranty_date = $today->add(new DateInterval('P'.$part_warranty->value.'Y'));
                            $new_today = new Datetime($startDate);
                            $interval = $new_today->diff($warranty_date);
                            $warranty_date_txt = $warranty_date->format('Y-m-d');
                            
                    }
                    $remind_date = $warranty_date->sub(new DateInterval('P'.$early_remind_minus_date.'D'));

                    
                    //print_r($interval);exit;

                    $dataReturn = array(
                            'warranty_expire_date' => $warranty_date_txt,
                            'next_notification_date' => $remind_date->format('Y-m-d'),
                            'warranty_days'=>$interval->days

                    );
                    
                    return $dataReturn;
                    /*
                    if($warranty_days >= 90){ // remind especially more than ninety minute
                            $early_remind_minus_date = $this->apiConfigPWT['early_remind_minus_days'];
                            $today = new DateTime();

                            $warranty_date = $today->add(new DateInterval('P'.$warranty_days.'D'));
                            $remind_date = $warranty_date->sub(new DateInterval('P'.$early_remind_minus_date.'D'));

                            $dataReturn = array(
                                    'warranty_expire_date' => $warranty_date->format('Y-m-d'),
                                    'next_notification_date' => $remind_date->format('Y-m-d')

                            );
                            return $dataReturn;

                    }else{
                            
                            $today = new DateTime();

                            $remind_date = $today->add(new DateInterval('P'.$warranty_days.'D'));

                            $dataReturn = array(
                                    'warranty_expire_date' => $remind_date->format('Y-m-d'),
                                    'next_notification_date' => $remind_date->format('Y-m-d')
                            );
                            return $dataReturn;
                    }
                    */
            break;
            case '2': //---- timely remind

            break;
            case '3': //------lately remind

            break;
            
            default:
                # code...
                break;
        }


    }

    private function __checkAlreadySerialNumber($serialno){
        // print_r($serialno);exit;
        /* older version 
        $customer_product = new M_customerproducts();
        $customer_product->where('serial_no',$serialno)->get();
        */

        $product_warranty = new M_productwarranty();
        $product_warranty->where('SerialNO',$serialno)->get();

        if($product_warranty->result_count() > 0){
            return true;
        }else{
            return false;
        }

    }

    private function __checkAlreadySerialNumberModern($serial_no){
                $product_part_notification = new M_productpartnotification();
                $product_part_notification->where('serial_no',$serial_no)->get();

                if($product_part_notification->result_count() > 0){
                    return true;
                }else{
                    return false;
                }

    }

    private function getProductGroupBySerialNo($serialno){

        if(strpos($serialno, "S/N:") !== false){
            $serialno = substr($serialno, 4);
        }

        $master_product_sn = new M_masterproductsn();
        $master_product_sn->where('SerialNO',$serialno)->get();


        if($master_product_sn->result_count() > 0){
            $mpCode = $master_product_sn->MPCODE;
            // echo $mpCode;
            $master_product = new M_masterproduct();
            $master_product->where('MPCODE',$mpCode)->get();
                if($master_product->result_count() > 0){
                        // echo $master_product->MPGroupProduct;exit;
                        $arrReturn = $master_product->to_array();
                        $arrReturn['serial_no'] = $serialno;
                        return $arrReturn;
                }

        }else{
                return false;
        }


    }
    private function setProductLogs($type,$requestCriteria){
        $log_data = array(
            'log_type' => $type,
            'consumerid' => $requestCriteria->consumerid,
            'productid' => $requestCriteria->productid,
            'created' => date('Y-m-d H:i:s')

        );

        $product_logs = new M_productlogs();
        $product_logs->serial_no = $requestCriteria->serialno;
        $product_logs->log_message = json_encode($log_data);
        $product_logs->created = date('Y-m-d H:i:s');

        $product_logs->save();


    }
    private function setCustomerProduct($requestCriteria,$product_machine_warranty,$product_type,$startWarranty){

        $register_date = new Datetime($startWarranty);
        $start_date = $register_date->format('Y-m-d H:i:s');


        switch ($product_machine_warranty->type) {
            case 'day':
                $warranty_expire_date = $register_date->add(new DateInterval('P'.(int)$product_machine_warranty->value.'D'));
            break;
            case 'month':
                $warranty_expire_date = $register_date->add(new DateInterval('P'.(int)$product_machine_warranty->value.'M'));
            break;
            case 'year':
                $warranty_expire_date = $register_date->add(new DateInterval('P'.(int)$product_machine_warranty->value.'Y'));
            break;
            
            default:
                # code...
                break;
        }
        //print_r($warranty_expire_date);exit;
        


        /* change to set to product warranty table */
        $product_warranty = new M_productwarranty();
        $product_warranty->SerialNO = $requestCriteria->serialno;
        $product_warranty->MPCODE = $requestCriteria->productid;
        $product_warranty->ConsumerID = $requestCriteria->consumerid;
        $product_warranty->Start_WarrantyDate = $start_date;
        $product_warranty->Expire_WarrantyDate = $warranty_expire_date->format('Y-m-d H:i:s');


        /* eof insert to product warranty table */

        if($product_warranty->save()){
            $data_noti = array(
                'consumerid' => $requestCriteria->consumerid,
                'product_type' => $product_type,
                'warranty_expire_date' => $warranty_expire_date->format('Y-m-d H:i:s')
            );

            // print_r($data_noti);exit;
            $this->sendRegisterNotification($data_noti);

        }



        /* this is older version */

        /*
        $customer_product = new M_customerproducts();
        $customer_product->consumerid = $requestCriteria->consumerid;
        $customer_product->serial_no = $requestCriteria->serialno;
        $customer_product->created = date('Y-m-d H:i:s');
        $customer_product->register_date = $today;
        $customer_product->warranty_expire_date = $warranty_expire_date->format('Y-m-d');
        $customer_product->mpcode = $requestCriteria->productid;

        if($customer_product->save()){
            $data_noti = array(
                'consumerid' => $requestCriteria->consumerid,
                'product_type' => $product_type,
                'warranty_expire_date' => $warranty_expire_date->format('Y-m-d H:i:s')
            );

            $this->sendRegisterNotification($data_noti);
        }

        */



    }

    private function getProductLogs($serial_no){
                $product_timeline = new M_productlogs();
                $product_timeline->where('serial_no',$serial_no)->get();
                $arProductTimeLine = array();

                if($product_timeline->result_count() > 0){
                    foreach ($product_timeline as $key => $value) {
                        $dataTimeline = json_decode($value->log_message);
                        $dataTimelineAr = array(
                            'title' => $dataTimeline->log_type,
                            'description' => 'ลงทะเบียนผลิตภัณฑ์',
                            'date' => date('Y-m-d',strtotime($dataTimeline->created))
                        );
                        array_push($arProductTimeLine, $dataTimelineAr);
                    }
                }else{
                    $queryProductWarranty = $this->ci->db->select('*')
                    ->from('ProductWarranty')
                    ->where('SerialNO',$serial_no)
                    ->get();
                    if($queryProductWarranty->num_rows() > 0){
                            $rowProductWarranty =$queryProductWarranty->row();
                            /* insert data to product log first */
                            $dataInsert = array(
                                'serial_no'=>$serial_no,
                                'log_message'=>json_encode(array(
                                        'log_type'=>'Register Product',
                                        'consumerid'=>$rowProductWarranty->ConsumerID,
                                        'productid'=>$rowProductWarranty->MPCODE,
                                        'created'=>$rowProductWarranty->Start_WarrantyDate
                                )),
                                'created'=>$rowProductWarranty->Start_WarrantyDate,

                            );

                            if($this->ci->db->insert('ProductLogs',$dataInsert)){
                                $dataTimelineAr = array(
                                    'title' => 'Register Product',
                                    'description' => 'ลงทะเบียนผลิตภัณฑ์',
                                    'date' => $rowProductWarranty->Start_WarrantyDate
                                );
                                array_push($arProductTimeLine, $dataTimelineAr);

                            }

                    }
                }
                /* check master product is pwt then insert part notification */
                $msproductquery = $this->ci->db->select('MasterProduct.MPCODE,MasterProduct.MPGroupProduct,MasterProductSN.MPCODE,MasterProductSN.SerialNO')
                ->from('MasterProduct')
                ->join('MasterProductSN','MasterProduct.MPCODE = MasterProductSN.MPCODE')
                ->where('MasterProductSN.SerialNO',$serial_no)
                ->get();

                //echo $this->ci->db->last_query();exit;
                $MPGroupProduct = "";
                if($msproductquery->num_rows() > 0){
                    $MPGroupProduct = $msproductquery->row()->MPGroupProduct;
                }
                /* eof check master product is pwt */

                if($MPGroupProduct == "PWT"){
                    //========== push product timeline for change soon 
                    $product_part_notification = new M_productpartnotification();
                    // $product_part_notification->select_min('warranty_expire_date');
                    $product_part_notification->where('serial_no',$serial_no);
                    $product_part_notification->order_by('warranty_expire_date','esc')->limit(1)->get();
                    
                    $data_push = array(
                        'title' => 'Change product part',
                        'description' => 'เปลี่ยนอุปกรณ์ครั้งต่อไป '.$product_part_notification->productpart->get()->part_name,
                        'date' => date('Y-m-d',strtotime($product_part_notification->warranty_expire_date))

                    );
                    array_push($arProductTimeLine, $data_push);
                }

                if($MPGroupProduct == "WE2"){
                    //========== push product timeline for change soon 
                    $product_part_notification = new M_productpartnotification();
                    // $product_part_notification->select_min('warranty_expire_date');
                    $product_part_notification->where('serial_no',$serial_no);
                    $product_part_notification->order_by('warranty_expire_date','esc')->limit(1)->get();
                    
                    $data_push = array(
                        'title' => 'Change product part',
                        'description' => 'เปลี่ยนอุปกรณ์ครั้งต่อไป '.$product_part_notification->productpart->get()->part_name,
                        'date' => date('Y-m-d',strtotime($product_part_notification->warranty_expire_date))

                    );
                    array_push($arProductTimeLine, $data_push);
                }

                return $arProductTimeLine;
    }
    private function getProductPartWarranty($serial_no){

            $arData = array();

            $product_part_warranty = new M_productpartnotification();
            $product_part_warranty->where('serial_no',$serial_no)->get();


            foreach ($product_part_warranty as $key => $value) {
                # code...
                $product_part = new M_productpart($value->ProductPart_id);

                $data_current_month = $this->checkCurrentMonth($value->warranty_start,$value->warranty_expire_date,$value->warranty_days);

                array_push($arData,
                    array(
                        'product_part_id' => $value->id,
                        'part_name' => $product_part->part_name,
                        'warranty_start'=>date('d/m/Y',strtotime($value->warranty_start)),
                        'warranty_expire'=>date('d/m/Y',strtotime($value->warranty_expire_date)),
                        'max_month' => round(($value->warranty_days/30)),
                        'passed_month' => $data_current_month['current_month'],
                        'passed_percent' => (float)$data_current_month['passed_percent'],
                        'status' => $this->calculateStatusByPassedPercent($data_current_month['passed_percent']),
                        'reset_logs' => $this->getProductPartResetLog($value->id)
                    )
                );
            }
            return $arData;



    }

    private function getPWTProductPartStatus($serial_no){
            $arData = array();

            $product_part_warranty = new M_productpartnotification();
            $product_part_warranty->where('serial_no',$serial_no)->get();


            if($product_part_warranty->result_count() > 0){
                    $filter_number = 1;
                    foreach ($product_part_warranty as $key => $value) {

                        # code...
                        $product_part = new M_productpart($value->ProductPart_id);

                        $data_current_month = $this->checkCurrentMonth($value->warranty_start,$value->warranty_expire_date,$value->warranty_days);

                        $passed_percent = (float)$data_current_month['passed_percent'];

                        if($passed_percent >= 0 && $passed_percent < 49){
                            $arData['filter'.$filter_number.'_status'] = 1;

                        }else if($passed_percent >= 49 && $passed_percent < 69){
                            $arData['filter'.$filter_number.'_status'] = 2;

                        }else if($passed_percent >= 69 && $passed_percent <= 100){
                            $arData['filter'.$filter_number.'_status'] = 3;
                        }




                        $filter_number++;

                    }
                return $arData;
            }else{
                                    $output = array(
                                        'status' => false,
                                        'result_code' => '-002',
                                        'result_desc' => 'ไม่พบข้อมูลรับประกันของชิ้นส่วนดังกล่าว'
                                    );
                                    return $output;

            }

    }
    private function calculateStatusByPassedPercent($passed_percent = 0){
                        if($passed_percent >= 0 && $passed_percent < 49){
                            return 1;

                        }else if($passed_percent >= 49 && $passed_percent < 69){
                            return 2;

                        }else if($passed_percent >= 69 && $passed_percent <= 100){
                            return 3;
                        }
    }
    private function checkCurrentMonth($warranty_start,$warranty_expire,$warranty_days){

        $arReturn = array();

        $today = new DateTime();
        $start_warranty = new Datetime($warranty_start);


        $interval = $today->diff($start_warranty);


        $daypass = $interval->days;

        $calculate_percent = ($daypass == 0)?'0':($daypass/$warranty_days)*100;
        $arReturn = array(
            'current_month' => $interval->m,
            'passed_percent' => ($calculate_percent >= 100)?100.00:number_format((float)$calculate_percent,2)
        );
        return $arReturn;


    }

    private function getAgentData($serialno = ""){

        /* check fixitjobproduct */
        $arReturn = array();
        $ci =& get_instance();
        $query = $ci->db->select('FixITJobProduct.FixITJob_id,FixITJobProduct.SerialNO,FixITJob.JobID,FixITJob.AgentCode')
        ->from('FixITJobProduct')
        ->join('FixITJob','FixITJobProduct.FixITJob_id = FixITJob.JobID')
        ->where('FixITJobProduct.SerialNO',$serialno)
        ->get();

        if($query->num_rows() > 0){
            $row = $query->row();
            /* get Agent Data */
            $queryAgent = $ci->db->select('AgentCode,CreditRemain,Image,FixIt_Latitude,FixIt_Longtitude,AgentName,AgentSurName,FixITOnline,PointSum,AgentRating,Telephone,TradeName,AgentType,FixIt01,FixIt02,FixIt03,FixIt04,FixIt05,FixIt06,FixITType,FixIt_Telephone')
            ->from('Agent')
            ->where('AgentCode',$row->AgentCode)
            ->get();

            if($queryAgent->num_rows() > 0){

                $rowAgent = $queryAgent->row();

                $arReturn = array(
                    "agentcode" =>  $rowAgent->AgentCode,
                    "agentcreditremain" =>  $rowAgent->CreditRemain,
                    "agentdistance" => "",
                    "agentimage" => base64_encode($rowAgent->Image),
                    "agentlatitude" => $rowAgent->FixIt_Latitude,
                    "agentlongtitude" => $rowAgent->FixIt_Longtitude,
                    "agentname" => $rowAgent->AgentName,
                    "agentonline" => $rowAgent->FixITOnline,
                    "agentpoint" => $rowAgent->PointSum,
                    "agentrating" => $rowAgent->AgentRating,
                    "agentsurname" => $rowAgent->AgentSurName,
                    "agenttelephone" => $rowAgent->FixIt_Telephone,
                    "agenttradename" => $rowAgent->TradeName,
                    "agenttype" => $rowAgent->AgentType,
                    "fixit01" => $rowAgent->FixIt01,
                    "fixit02" => $rowAgent->FixIt02,
                    "fixit03" => $rowAgent->FixIt03,
                    "fixit04" => $rowAgent->FixIt04,
                    "fixit05" => $rowAgent->FixIt05,
                    "fixit06" => $rowAgent->FixIt06,
                    "fixittype" => $rowAgent->FixITType,
                    "haveteam" => ""
                );


            }else{
                $arReturn = $this->apiConfigPWT['default_agent_data'];
            }


       
        }else{
                $arReturn = $this->apiConfigPWT['default_agent_data'];
        }

        return $arReturn;
    }

    private function getNotificationString($part_notification = array()){
            $returnTxt = "";
            foreach ($part_notification as $key => $value) {
                # code...
                $returnTxt .= $value." ";
            }

            return $returnTxt;
            // echo $returnTxt;exit;
    }
    private function getNotificationTitleString($remind_type = ""){
            $strReturn = "";

            // echo $remind_type;exit;

            if(trim($remind_type) == "early"){
                    $strReturn = "แจ้งเตือนการเปลี่ยนไส้กรองเครื่องแยกน้ำ มีรายการดังนี้ \r\n";
            }else if(trim($remind_type) == "timely"){
                    $strReturn = "แจ้งเตือนครบกำหนดเปลี่ยนไส้กรองเครื่องแยกน้ำ มีรายการดังนี้ \r\n";

            }
            // echo $strReturn;exit;
               
            return $strReturn;
            

    }
    private function getNotificationFooterString($remind_type,$timely_date){
            $strReturn = "";

            switch (trim($remind_type)) {
                case 'early':
                    $strReturn = "กรุณาติดต่อช่างภายในวันที่ ".date('d-m-Y',strtotime($timely_date));
                    return $strReturn;
                break;

                case 'timely':
                    $strReturn = "กรุณาติดต่อช่างเพื่อเปลี่ยนอุปกรณ์ของคุณ";
                    return $strReturn;
                break;
                
                default:
                    # code...
                    break;
            }
            

    }

    private function getTechnicialNotificationFooterString($remind_type,$timely_date,$CustID){
            $strReturn = "";
            $customer_text = "";

            $fixitcustomer = new M_fixitcustomer();
            $fixitcustomer->where('CustID',$CustID)->get();

            if($fixitcustomer->result_count() > 0){
                //echo 'aaa';exit;
                $customer_name = ($fixitcustomer->CustName)?$fixitcustomer->CustName:'-';
                $customer_address = ($fixitcustomer->CustAddress)?$fixitcustomer->CustAddress:'-';
                $customer_telephone = ($fixitcustomer->Telephone)?$fixitcustomer->Telephone:'-';
                
                $customer_text = 'คุณ '.$customer_name." ";
                $customer_text .= 'เบอร์โทร:'.$customer_telephone." ";
                $customer_text .= 'ที่อยู่:'.$customer_address;
                


            }
            //print_r($customer_text);

            switch (trim($remind_type)) {
                case 'early':
                    $strReturn = "กรุณาติดต่อลูกค้าภายในวันที่ ".date('d-m-Y',strtotime($timely_date));
                    $strReturn .= " ".$customer_text;
                    return $strReturn;
                break;

                case 'timely':
                    $strReturn = "กรุณาติดต่อลูกค้าเพื่อเปลี่ยนอุปกรณ์ไส้กรอง";
                    $strReturn .= " ".$customer_text;
                    return $strReturn;
                break;
                
                default:
                    # code...
                    break;
            }
    }

    private function getTechnicialNotificationTitleString($remind_type = ""){
            $strReturn = "";

            // echo $remind_type;exit;

            if(trim($remind_type) == "early"){
                    $strReturn = "แจ้งเตือนเปลี่ยนไส้กรองเครื่องแยกน้ำที่คุณไปติดตั้ง ดังนี้ ";
            }else if(trim($remind_type) == "timely"){
                    $strReturn = "แจ้งเตือนครบกำหนดเปลี่ยนไส้กรองเครื่องแยกน้ำที่คุณไปติดตั้ง ดังนี้ ";

            }
            // echo $strReturn;exit;
               
            return $strReturn;

    }



    private function updateProductPartNotificationByStatus($product_part_id = array(),$remind_type = array(),$timely_date = array()){

        foreach ($product_part_id as $key => $value) {
            # code...
            $productpartnotification = new M_productpartnotification();
            $productpartnotification->where('id',$value)->get();

            if($productpartnotification->result_count() > 0){

                $current_product_part_status = $productpartnotification->product_part_status;                
                $productpartnotification->product_part_status_id = (int)$productpartnotification->product_part_status_id+1;
                $productpartnotification->product_part_next_status_id = (int)$productpartnotification->product_part_next_status_id+1;

                $type_remind = $remind_type[$key];
                // echo $type_remind;exit;

                if(trim($type_remind) == "early"){
                    // echo 'aaaa';exit;
                    $productpartnotification->product_part_next_notification_date = $timely_date[$key];

                }else if(trim($type_remind) == "timely"){
                    // echo 'bbb';exit;
                    $timelyDate = new DateTime($timely_date[$key]);
                    $next_notification_date = $timelyDate->add(new DateInterval('P15D'));
                    $productpartnotification->product_part_next_notification_date = $next_notification_date->format('Y-m-d');

                }else if($remind_type[$key] == "lately"){


                }

                $productpartnotification->updated = date('Y-m-d H:i:s');
                $productpartnotification->save();

                
            }

            
        }





    }

    private function updateProductNotificationLogs($arData = array(),$type=""){

        if($type == "send"){

            foreach ($arData['part_notification_id'] as $key => $value) {
                # code...
                $notilogs = new M_productpartnotificationlogs();
                $notilogs->ProductPartNotification_id = $value;
                $notilogs->created = date('Y-m-d H:i:s');
                $notilogs->updated = date('Y-m-d H:i:s');

                $dataLogMessage = array(
                    'token' => $arData['token'],
                    'remind_type' => $arData['remind_type'][$key],
                    'product_part_id' => $arData['product_part_id'][$key],
                    'send' => true
                );
                $notilogs->log_message = json_encode($dataLogMessage);
                $notilogs->save();

            }
        }else{
            foreach ($arData['part_notification_id'] as $key => $value) {
                # code...
                $notilogs = new M_productpartnotificationlogs();
                $notilogs->ProductPartNotification_id = $value;
                $notilogs->created = date('Y-m-d H:i:s');
                $notilogs->updated = date('Y-m-d H:i:s');

                $dataLogMessage = array(
                    'token' => $arData['token'],
                    'remind_type' => $arData['remind_type'][$key],
                    'product_part_id' => $arData['product_part_id'][$key],
                    'send' => false
                );
                $notilogs->log_message = json_encode($dataLogMessage);
                $notilogs->save();
            }
        }



    }

    private function onesignalCurlNotification($post_data){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->onesignalConfig['url']);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->onesignalConfig['header']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

               
                return $response;

    }

    private function onesignalCurlFixitArmNotification($post_data){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->onesignalFixitArmConfig['url']);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->onesignalFixitArmConfig['header']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

               
                return $response;
    }

    private function getProductPartResetLog($productpartnotification_id){

        $arReturn = array();
        $productpartnotificationresetlog = new M_productpartnotificationresetlogs();
        $productpartnotificationresetlog->where('ProductPartNotification_id',$productpartnotification_id)->get();

        foreach ($productpartnotificationresetlog as $key => $value) {
            # code...
            $arReturn[$key]['log_message'] = json_decode($value->log_message);
            $arReturn[$key]['barcode'] = $value->barcode;
        }
        return $arReturn;

    }
    private function sendRegisterNotification($data = array()){
        $customer = new M_fixitcustomer();
        $customer->where('CustID',$data['consumerid'])->get();

        if($customer->result_count() > 0){
                $customer_token_care = $customer->Device_Token;

                // echo 'device token = '.$customer_token_care."<br>";



                            $txt_notification = $this->getRegisterNotificationStringByProductType($data['product_type'],$data['warranty_expire_date']);

                // echo $txt_notification.'<br>';exit;

                            $content = array(
                                'en' => $txt_notification
                            );

                            $fields = array(
                                'app_id' => $this->onesignalConfig['app_id'],
                                'include_player_ids' => array($customer_token_care),
                                'data' => array("jobstatus" => "REGISTER"),
                                'contents' => $content
                            );

                            
                            $post_data = json_encode($fields);
                            
                            $response = json_decode($this->onesignalCurlNotification($post_data)); 

                            if(isset($_GET['test']) && $_GET['test'] == 'test'){
                                print_r($response);exit;
                            }

                            if(@$response->recipients > 0){

                            }
        }
        return true;

    }
    private function getPPMWarrantyDays($ppm = ""){

            //print_r($ppm);exit;
            $ppm_level = $this->apiConfigPWT['ppm_level'];
            $key = array_search($ppm, array_column($ppm_level, 'level'));

            //var_dump($ppm_level[$key]);exit;
            $today = new DateTime();
            $enddate = new Datetime();

            switch (strtoupper($ppm)) {
                case 'H':
                        $interval = $this->getInterVal('day',$this->apiConfigPWT['high_ppm_filter_warranty']);
                     
                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);

                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>'day','warranty_value'=>$this->apiConfigPWT['high_ppm_filter_warranty']);
                    //return $this->apiConfigPWT['high_ppm_filter_warranty'];
                break;
                case 'M':
                //echo 'aaa';exit;
                     $interval = $this->getInterVal('day',$this->apiConfigPWT['medium_ppm_filter_warranty']);
                     
                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);

                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>'day','warranty_value'=>$this->apiConfigPWT['medium_ppm_filter_warranty']);
                break;

                case 'L':
                        $interval = $this->getInterVal('day',$this->apiConfigPWT['low_ppm_filter_warranty']);
                     
                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);

                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>'day','warranty_value'=>$this->apiConfigPWT['low_ppm_filter_warranty']);
                    //return $this->apiConfigPWT['low_ppm_filter_warranty'];
                break;

                case '0':
                        $interval = $this->getInterVal($ppm_level[$key]['warranty_unit'],$ppm_level[$key]['warranty']);

                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);

                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>$ppm_level[$key]['warranty_unit'],'warranty_value'=>$ppm_level[$key]['warranty']);

                        //return $interval_diff->days;
                break;

                case '1':
                        $interval = $this->getInterVal($ppm_level[$key]['warranty_unit'],$ppm_level[$key]['warranty']);

                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);
                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>$ppm_level[$key]['warranty_unit'],'warranty_value'=>$ppm_level[$key]['warranty']);
                break;
                case '2':
                        $interval = $this->getInterVal($ppm_level[$key]['warranty_unit'],$ppm_level[$key]['warranty']);

                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);
                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>$ppm_level[$key]['warranty_unit'],'warranty_value'=>$ppm_level[$key]['warranty']);
                break;
                case '3':
                        $interval = $this->getInterVal($ppm_level[$key]['warranty_unit'],$ppm_level[$key]['warranty']);

                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);
                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>$ppm_level[$key]['warranty_unit'],'warranty_value'=>$ppm_level[$key]['warranty']);
                break;
                case '4':
                       $interval = $this->getInterVal($ppm_level[$key]['warranty_unit'],$ppm_level[$key]['warranty']);

                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);
                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>$ppm_level[$key]['warranty_unit'],'warranty_value'=>$ppm_level[$key]['warranty']);
                break;

                case '5':

                        return false;
                default:
                        $interval = $this->getInterVal('day',$this->apiConfigPWT['medium_ppm_filter_warranty']);
                     
                        $enddate->add($interval);
                        $interval_diff = $today->diff($enddate);

                        return array('date_diff'=>$interval_diff->days,'warranty_unit'=>'day','warranty_value'=>$this->apiConfigPWT['medium_ppm_filter_warranty']);
                break;
            }


    }
    private function getRegisterNotificationStringByProductType($product_type,$warranty_expire_date){
        $return_txt = "";
        switch (trim($product_type)) {
            case 'PWT':
                # code...
                $product = new M_product();
                $product->where('code',$product_type)->get();

                $warranty_expire = new Datetime($warranty_expire_date);

                $return_txt = "คุณได้ทำการลงทะเบียนเครื่องแยกน้ำเรียบร้อยแล้ว \r\n";
                $return_txt .= "ระบบจะทำการแจ้งเตือนเมื่อไส้กรองของคุณถึงกำหนดเปลี่ยน \r\n";
                // $return_txt .= "สินค้าของคุณได้รับประกัน ".@$product->warranty_years." ปี สิ้นสุดในวันที่ ".$warranty_expire->format('d/m/Y');

                // echo $return_txt;exit;
                return $return_txt;
            break;

            case 'WE2':
                $product = new M_product();
                $product->where('code',$product_type)->get();

                $warranty_expire = new Datetime($warranty_expire_date);

                $return_txt = "คุณได้ทำการลงทะเบียนเครื่องแยกน้ำเรียบร้อยแล้ว \r\n";
                $return_txt .= "ระบบจะทำการแจ้งเตือนเมื่อไส้กรองของคุณถึงกำหนดเปลี่ยน \r\n";
                // $return_txt .= "สินค้าของคุณได้รับประกัน ".@$product->warranty_years." ปี สิ้นสุดในวันที่ ".$warranty_expire->format('d/m/Y');

                // echo $return_txt;exit;
                return $return_txt;
            break;
            
            default:
                # code...
                break;
        }


    }
    private function checkProductPartAlreadyUse($barcode = ""){

        $productpartresetlog = new M_productpartnotificationresetlogs();
        $productpartresetlog->where('barcode',$barcode)->get();

        if($productpartresetlog->result_count() > 0){
            return true;
        }else{
            return false;
        }

    }
    private function isWaterFilter($serialno = ""){
        $returnStatus = false;
        $arPrefixWaterFilter = $this->apiConfigPWT['water_filter_prefix'];
        $txtPrefix = substr($serialno, 0,2);

        //echo $txtPrefix;exit;
        if(in_array(strtolower($txtPrefix), $arPrefixWaterFilter)){
            $returnStatus = true;
        }

        return $returnStatus;
    }

    private function checkProductAlreadyRegister($serialno = ""){

        $product_warranty = new M_productwarranty();
        $product_warranty->where('SerialNO',$serialno)->get();

        if($product_warranty->result_count() > 0){
            return true;
        }else{
            return false;
        }
    }

    private function checkAgentAbletoInstallProduct($data = array()){
        $arAgentAvailable = array();
        $query = $this->ci->db->select('AgentCode,FixIt01,FixIt02,FixIt03,FixIt04,FixIt05')
        ->from('Agent')
        ->where('AgentCode',$data['agentcode'])
        ->get();

        if($query->num_rows() > 0){
            $row = $query->row();

            if($row->FixIt01 == 'T'){
                array_push($arAgentAvailable, 'SAT');
            }
            if($row->FixIt02 == 'T'){
                array_push($arAgentAvailable, 'OCS');
            }
            if($row->FixIt03 == 'T'){
                array_push($arAgentAvailable, 'AIR');
            }
            if($row->FixIt04 == 'T'){
                array_push($arAgentAvailable, 'NET');
            }
            if($row->FixIt05 == 'T'){
                array_push($arAgentAvailable, 'PWT');
            }

            print_r($arAgentAvailable);exit;

        }else{




        }


    }

    private function getInterVal($type = "",$total = 0){
        
        switch ($type) {
            case 'minute':
                # code...
            break;
            case 'day':
                return new DateInterval('P'.$total.'D');
            break;

            case 'month':
                return new DateInterval('P'.$total.'M');
            break;

            case 'year':
                return new DateInterval('P'.$total.'Y');
            break;
            
            default:
                # code...
                break;
        }


    }

    //==================== for campaign ========================


    private function sendEmailToTip($data = array()){

            $file_path = $data['file_path'];
            $campaign_insurance_register = $data['campaign_insurance_register'];

            $count_register = $campaign_insurance_register->result_count();

            //print_r($data);exit;
            $tipconfig = $this->ci->config->item('campaign_tip');
            //print_r($tipconfig);exit;

            $mail_subject = "[PSI][Campaign] - รายชื่อลูกค้าที่ลงทะเบียนขอรับสิทธิประกันภัยที่อยู่อาศัย";
            $mail_to = $tipconfig['email_to'];
            $mail_message = "";
                    $mail_message .= "<p>Dear. ทิพยประกันภัย</p>";
                    $mail_message .= "<p>อีเมล์ฉบับนี้เป็นอีเมล์อัตโนมัติ ที่ทาง PSI ส่งไปถึงเพื่อแจ้งรายละเอียดรายชื่อผู้ที่ลงทะเบียนรับสิทธิประกันภัยที่อยู่อาศัย ในกรณีที่มีผู้ลงทะเบียนข้อมูลจะสรุปอยู่ในไฟล์ Excel ที่แนบมากับอีเมล์ฉบับนี้ มีรายละเอียดโดยสรุปดังนี้</p>";
                    $mail_message .= "<p><strong>วันที่ : </strong>".date('d/m/Y')."</p>";
                    $mail_message .= "<p><strong>จำนวนผู้ลงทะเบียน : </strong>".$count_register." ท่าน</p>";


                    /* for signature */
                    $mail_message .= '<div>
<div><br /><br />
<table>
<tbody>
<tr>
<td>
<div><img src="https://psi.co.th/logo_psi_mail.gif" alt="LogoPSI" /> </div>
</td>
<td>
<div><strong>PSI Corporation Co.,Ltd.</strong><br /><strong>Website :&nbsp;<a href="http://www.psi.co.th">www.psi.co.th</a></strong></div>
</td>
</tr>
</tbody>
</table>
<br />
<div><strong>Disclaimer:</strong>&nbsp;The information in this email (and any attachments) is confidential. If you are not the intended recipient, you must not use or disseminate the information. If you have received this email in error, please immediately notify me by "Reply" command and permanently delete the original and any copies or printouts thereof. Although this email and any attachments are believed to be free of any virus or other defect that might affect any computer system into which it is received and opened, it is the responsibility it is virus free and no responsibility is accepted by PSI Corporation or its subsidiaries or affiliates either jointly or severally, for any loss or damage arising in any way from its use.<br />588 Srinakarin Rd. Suanluang Bangkok 10250</div>
</div>
</div>';

            $mail_from = $tipconfig['email_from'];
            //$mail_from_name = $tipconfig['email_from_name'];
            $mail_from_name = "PSI Corporation";



            $this->ci->load->library('email');
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';


            $config['protocol']= 'smtp';
            $config['smtp_host'] = 'psisat.com';
            //$config['smtp_port'] = '465';
            $config['smtp_user'] = 'kridsada@psisat.com';
            $config['smtp_pass'] = 'isylzjkoemail';
            $config['smtp_timeout']='80';
            $config['_smtp_auth']=TRUE;

            $config['newline'] = "\r\n";
            $config['crlf'] = "\r\n";

            $this->ci->email->initialize($config);
            $this->ci->email->from($mail_from, $mail_from_name);
            $this->ci->email->to($mail_to);

            if ($tipconfig['email_bcc']){
                    $this->ci->email->bcc($tipconfig['email_bcc']);
            }
            if($tipconfig['email_cc']){
                    $this->ci->email->cc($tipconfig['email_cc']);
            }
            

            $this->ci->email->subject($mail_subject);
            $this->ci->email->message($mail_message);
            if($count_register > 0){
                $this->ci->email->attach($file_path);
            }
            
            if($this->ci->email->send()){
                return true;
            }else{
                
                $log_file_path = $this->createTipLogFilePath('SendmailErrorLog');
                $file_content = date("Y-m-d H:i:s") . ' error : ' . $this->ci->email->print_debugger() . "\n";
                file_put_contents($log_file_path, $file_content, FILE_APPEND);
                unset($file_content);
            }   
    }
    private function createTipLogFilePath($filename = '') {
        $log_path = './cron/campaign/tip/logs';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function createProductPartNotificationLogFilePath($filename = ''){
        $log_path = './logs/waterfilter/cron';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function onesignalRegisterProductClassicSuccess($post_data){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->onesignalConfig['url']);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->onesignalConfig['header']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

               
                return $response;
    }


    private function sendRegisterProductClassiceNotification($data = array()){
            $text_notification = $data['text_notification'];
            $customer_token = $data['customer_token'];

                            $content = array(
                                'en' => $text_notification
                            );

                            $fields = array(
                                'app_id' => $this->onesignalConfig['app_id'],
                                'include_player_ids' => array($customer_token),
                                'data' => array("jobstatus" => "REGISTER"),
                                'contents' => $content
                            );

                            
                            $post_data = json_encode($fields);
                            
                            $response = json_decode($this->onesignalRegisterProductClassicSuccess($post_data));

                            if($response->recipients > 0){

                            }


    }

    private function setProductPartNotificationToAgent($data = array()){
        /* check technicial in fixitjobproduct table and get token */
                    $arArange = $data['arArange'];
                    $notification_records = $data['notification_records'];

                    foreach ($notification_records as $key => $value) {
                        # code...
                        $jobproduct = new M_fixitjobproduct();
                        $jobproduct->where('SerialNO',$value->serial_no)->where('Is_Install','T')->get();

                        if($jobproduct->result_count() > 0){
                        // print_r($jobproduct->to_array());exit;
                            $queryFixitjob = $this->ci->db->select('FixITJob.JobID,FixITJob.AgentCode,Agent.FixIt_Device_Token')
                            ->from('FixITJob')
                            ->join('Agent','FixITJob.AgentCode = Agent.AgentCode')
                            ->where('FixITJob.JobID',$jobproduct->FixITJob_id)->get();
                            
                            if($queryFixitjob->num_rows()){
                                $agentData = $queryFixitjob->row();
                                if($agentData->FixIt_Device_Token){
                                    //print_r($agentData);exit;

                                        $tech_content_title = $this->getTechnicialNotificationTitleString($arArange[$key]['remind_type'][0]);

                                        $tech_content = $this->getNotificationString($arArange[$key]['part_notification']);

                                        $tech_content_footer = $this->getTechnicialNotificationFooterString($arArange[$key]['remind_type'][0],$arArange[$key]['timely_date'][0],$arArange[$key]['CustID']);

                                        $content = array(
                                            'en' => $tech_content_title.$tech_content.$tech_content_footer
                                        );

                                        $fields = array(
                                            'app_id' => $this->onesignalFixitArmConfig['app_id'],
                                            'include_player_ids' => array($agentData->FixIt_Device_Token),
                                            'data' => array("jobstatus" => "ACCEPT"),
                                            'contents' => $content
                                        );

                                        $post_data = json_encode($fields);
                                        // echo $post_data;

                                       
                                        $response = json_decode($this->onesignalCurlFixitArmNotification($post_data));

                                        if($response->recipients > 0){

                                        }

                                }else{
                                    /* write log to file not found agent device token */
                                    $log_file_path = $this->createProductPartNotificationLogFilePath('NotFoundAgentDeviceToken');
                                    $file_content = date("Y-m-d H:i:s") . ' data : ' . json_encode($arArange) . "\n";
                                    file_put_contents($log_file_path, $file_content, FILE_APPEND);
                                    unset($file_content);


                                }
                            }
                        }else{
                            /* write log to file */
                                $log_file_path = $this->createProductPartNotificationLogFilePath('NotFoundDataFromFixitJobProduct');
                                $file_content = date("Y-m-d H:i:s") . ' data : ' . json_encode($arArange) . "\n";
                                file_put_contents($log_file_path, $file_content, FILE_APPEND);
                                unset($file_content);
                            
                        }
                    }

                        
    }

    private function getWE2WarrantyDataByConfig($data = []){
        $requestCriteria = $data['requestCriteria'];
        $we2_config = $this->ci->config->item('we2');
        $arReturn = [];

        foreach ($we2_config['ppm_level'] as $key => $row) {
            # code...
            if($key == $data['product_part_mpcode']){
                // print_r($row);
                foreach ($row as $k => $v) {
                    if((int)$requestCriteria->ppm >= $v['ppm_rank_start'] && (int)$requestCriteria->ppm <= $v['ppm_rank_end']){
                        $arReturn = $v;
                    }
                }
                
            }

        }

        return $arReturn;





    }

    private function testFixitSendNotification(){

                            $txt_notification = 'test';

                            $content = array(
                                'en' => $txt_notification
                            );

                            $fields = array(
                                'app_id' => $this->onesignalConfig['app_id'],
                                'include_player_ids' => array('ca1cfe6b-60d3-4ff5-b1ba-163d6172eaf6'),
                                'data' => array("jobstatus" => "REGISTER"),
                                'contents' => $content
                            );

                            
                            $post_data = json_encode($fields);
                            
                            $response = json_decode($this->onesignalCurlNotification($post_data));

                            if($response->recipients > 0){
                                print_r($response);exit;
                            }

    }

    private function clearWE2(){
        $this->ci->db->delete('ProductPartNotification',[
            'serial_no'=>'we021111'
        ]);

        $this->ci->db->delete('ProductLogs',[
            'serial_no'=>'we021111'
        ]);

        $this->ci->db->delete('ProductWarranty',[
            'SerialNO'=>'we021111'
        ]);
    }

}

