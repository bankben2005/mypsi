<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of campaign helper
 *
 * @author kridsada 
 */
if (!function_exists('textGetCampaignFrom')) {

    function textGetCampaignFrom($txtVariable) {

        switch ($txtVariable) {
            case 'psi_website':
                return 'Website : psi.co.th';
            break;

            case 'fb_psisatellitedise':
                return 'FB : psisatellitedise';
            break;

            case 'fb_psisaradee99':
                return 'FB : psisaradee99';
            break;

            case 'psi_official':
                return 'Psi Official';
            break;

            case 'channel_psisaradee99':
                return 'ช่องพีเอสไอ สาระดี';
            break;

            case 'other':
                return 'อื่น ๆ';
            break;
            
            default:
                # code...
            break;
        }
    }

}