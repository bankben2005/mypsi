<?php 

if(!function_exists('checkIsPermissionAvailable')){
    function checkIsPermissionAvailable($member_menus_id,$member_permissions){
        

       
        //print_r($member_permissions);exit;
        $key = array_search($member_menus_id, array_column($member_permissions, 'member_menus_id'));

        if(is_numeric($key)){
            return true;
        }else{
            return false;
        }
        

    }

}

if(!function_exists('getEventAbility')){
    function getEventAbility($event,$menu_id,$member_permissions){
        $key = array_search($menu_id, array_column($member_permissions, 'member_menus_id'));

        if(is_numeric($key)){
            $decode_json = json_decode($member_permissions[$key]['event_ability'],true);
            // print_r($event);
            if($decode_json[$event]){
                return true;
            }else{
                return false;
            }
            // print_r($decode_json[$event]);
        }else{
            return false;
        }
    }

}

if(!function_exists('flashsaleCalculateProductRemainByProductId')){
    function flashsaleCalculateProductRemainByProductId($flashsalesetproduct_id){
        $ci =& get_instance();
        //print_r($ci);

        /* get total available */
        $query_total_available = $ci->db->select('total_available')
        ->from('FlashSaleSetProduct')
        ->where('id',$flashsalesetproduct_id)
        ->get();

        $total_available = $query_total_available->row()->total_available;

        /* count all product has been ordered */
        $query_count = $ci->db->select('sum(amount) as product_ordered')
        ->from('FlashSaleTransactionProduct')
        ->where('FlashSaleSetProduct_id',$flashsalesetproduct_id)
        ->where('active',1)
        ->get();

        $row_count = $query_count->row();

        return number_format($total_available-$row_count->product_ordered,2);


    }
}

?>