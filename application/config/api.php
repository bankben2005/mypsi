<?php
		// $config = array();
		$config['water_filter'] = array(
				'prefix' => 'PWT',
				'product_part_fix_month_id' => 1, 
				'low_ppm_filter_warranty' => 547,
				'medium_ppm_filter_warranty' => 365,
				'high_ppm_filter_warranty' => 180,
				'early_remind_minus_days' => 3,
				'lately_remind_plus_days' => 10,
				'default_agent_code' => '0000',
				'water_filter_prefix' => array('ff','df','et','pf'),
				'default_agent_data' => array(
					"agentcode" =>  "",
		            "agentcreditremain" =>  0,
		            "agentdistance" => '',
		            "agentimage" => "",
		            "agentlatitude" => '',
		            "agentlongtitude" => '',
		            "agentname" => "PSI",
		            "agentonline" => "",
		            "agentpoint" => 0,
		            "agentrating" => 5,
		            "agentsurname" => "",
		            "agenttelephone" => "1247",
		            "agenttradename" => "-",
		            "agenttype" => "",
		            "fixit01" => "T",
		            "fixit02" => "T",
		            "fixit03" => "T",
		            "fixit04" => "T",
		            "fixit05" => "T",
		            "fixit06" => "T",
		            "fixittype" => "OTHER",
		            "haveteam" => ""
				),
				'ppm_level' => array(
					array(
						'level' => 0,
						'ppm_rank' => '0-200',
						'warranty' => 24,
						'warranty_unit' => 'month' 
					),
					array(
						'level' => 1,
						'ppm_rank' => '201-300',
						'warranty' => 22,
						'warranty_unit' => 'month' 

					),
					array(
						'level' => 2,
						'ppm_rank' => '301-400',
						'warranty' => 18,
						'warranty_unit' => 'month'
					),
					array(
						'level' => 3,
						'ppm_rank' => '401-500',
						'warranty' => 15,
						'warranty_unit' => 'month'

					),
					array(
						'level' => 4,
						'ppm_rank' => '501-600',
						'warranty' => 12,
						'warranty_unit' => 'month'
					),
					array(
						'level' => 5,
						'ppm_rank' => '601-over',
						'warranty' => 0,
						'warranty_unit' => 'month'
					)

				)
		);

		$config['hrm'] = array(
			'token_expires' => array(
				'time' => 15,
				'unit' => 'minute'
			),
			'token_pattern' => '(randomnumber)-(PSIHRM)+(username)+(Y-m-d H:i:s)-(randomnumber)'

		);

		$config['onesignal'] = array(
				'app_id' => 'f6d99d18-ffb9-4723-99f8-e5e09470ae17',
				'url' => 'https://onesignal.com/api/v1/notifications',
				'header' => array(
					'Content-Type: application/json; charset=utf-8',
					'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'
				)
		);

		$config['onesignal_fixitarm'] = array(
				'app_id' => 'd1a9a83b-41de-4691-a034-31a11f351d00',
				'url' => 'https://onesignal.com/api/v1/notifications',
				'header' => array(
					'Content-Type: application/json; charset=utf-8',
					'Authorization: Basic ZGJkNThjZWItN2IzYy00NTNhLWFhNGMtNTA5ZTdhMmE5ZGYy'
				)
		);
		

		$config['seminar'] = array(
				'webview_orderproduct' => base_url('seminar/OrderProduct')

		);
		
		$config['onesignal_psiarm'] = array(
				'app_id' => 'eb366995-8215-4a3b-b943-31e0a618180f',
				'url' => 'https://onesignal.com/api/v1/notifications',
				'header' => array(
					'Content-Type: application/json; charset=utf-8',
					'Authorization: Basic NzgwNjMzMDItNzYzMi00YzhlLTlmNmMtZDFlODk0N2Y0ZDRh'
				)
		);

		$config['onesignal_psihrm'] = array(
				'app_id' => '016659b9-a62f-48a0-a913-7b3aabf88a67',
				'url' => 'https://onesignal.com/api/v1/notifications',
				'header' => array(
					'Content-Type: application/json; charset=utf-8',
					'Authorization: Basic N2M0ZTVjMTYtMzAzNi00YzUzLTkzYjYtNGE0ODdjMGIwN2Ri'
				)
		);

		$config['tvapis'] = array(
			'bitrates' => array(
				array(
					'name'=>'Low',
					'value'=>'300'
				),
				array(
					'name'=>'High',
					'value'=>'600'
				)
			),
			'get_channel'=>'http://api.psitv.tv/api/get_channel.php'
		);

		$config['psiarm'] = array(
			'redeem_coupon'=>array(
				'duration_use'=>2,
				'duration_type'=>'Y'
			)
		);

		$config['campaign_tip'] = array(
				'email_to'=>array(
					'rattanab@dhipaya.co.th',
					'daungrudeet@dhipaya.co.th'
				),
				'email_from_name'=>'kridsada@psisat.com',
				'email_from'=>'kridsada@psisat.com',
				'email_bcc'=>array(
					'phanuwat@psisat.com',
					'jquery4me@gmail.com'
				),
				'email_cc'=>'pattanawit@psisat.com'
		);

		//echo ENVIRONMENT;

		$config['we2'] = [
			'product_part_fix_month_id'=>6,
			'prefix'=>'WE2',
			'ppm_over_start'=>601,
			'ppm_level'=>[

				'885190004'=>[ // double filter
					array(
						'level' => 0,
						'ppm_rank'=>'0-200',
						'ppm_rank_start' => 0,
						'ppm_rank_end'=>200,
						'warranty' => 24,
						'warranty_unit' => 'month' 
					),
					array(
						'level' => 1,
						'ppm_rank' => '201-300',
						'ppm_rank_start'=>201,
						'ppm_rank_end'=>300,
						'warranty' => 22,
						'warranty_unit' => 'month' 

					),
					array(
						'level' => 2,
						'ppm_rank' => '301-400',
						'ppm_rank_start'=>301,
						'ppm_rank_end'=>400,
						'warranty' => 18,
						'warranty_unit' => 'month'
					),
					array(
						'level' => 3,
						'ppm_rank' => '401-500',
						'ppm_rank_start'=>401,
						'ppm_rank_end'=>500,
						'warranty' => 15,
						'warranty_unit' => 'month'

					),
					array(
						'level' => 4,
						'ppm_rank' => '501-600',
						'ppm_rank_start'=>501,
						'ppm_rank_end'=>600,
						'warranty' => 12,
						'warranty_unit' => 'month'
					),
				],
				'885190007'=>[
					array(
						'level' => 0,
						'ppm_rank' => '0-200',
						'ppm_rank_start' => 0,
						'ppm_rank_end'=>200,
						'warranty' => 24,
						'warranty_unit' => 'month' 
					),
					array(
						'level' => 1,
						'ppm_rank' => '201-300',
						'ppm_rank_start'=>201,
						'ppm_rank_end'=>300,
						'warranty' => 22,
						'warranty_unit' => 'month' 

					),
					array(
						'level' => 2,
						'ppm_rank' => '301-400',
						'ppm_rank_start'=>301,
						'ppm_rank_end'=>400,
						'warranty' => 18,
						'warranty_unit' => 'month'
					),
					array(
						'level' => 3,
						'ppm_rank' => '401-500',
						'ppm_rank_start'=>401,
						'ppm_rank_end'=>500,
						'warranty' => 15,
						'warranty_unit' => 'month'

					),
					array(
						'level' => 4,
						'ppm_rank' => '501-600',
						'ppm_rank_start'=>501,
						'ppm_rank_end'=>600,
						'warranty' => 12,
						'warranty_unit' => 'month'
					),
				],
				'885190008'=>[
					array(
						'level' => 0,
						'ppm_rank' => '0-200',
						'ppm_rank_start' => 0,
						'ppm_rank_end'=>200,
						'warranty' => 48,
						'warranty_unit' => 'month' 
					),
					array(
						'level' => 1,
						'ppm_rank' => '201-300',
						'ppm_rank_start'=>201,
						'ppm_rank_end'=>300,
						'warranty' => 44,
						'warranty_unit' => 'month' 

					),
					array(
						'level' => 2,
						'ppm_rank' => '301-400',
						'ppm_rank_start'=>301,
						'ppm_rank_end'=>400,
						'warranty' => 36,
						'warranty_unit' => 'month'
					),
					array(
						'level' => 3,
						'ppm_rank' => '401-500',
						'ppm_rank_start'=>401,
						'ppm_rank_end'=>500,
						'warranty' => 30,
						'warranty_unit' => 'month'

					),
					array(
						'level' => 4,
						'ppm_rank' => '501-600',
						'ppm_rank_start'=>501,
						'ppm_rank_end'=>600,
						'warranty' => 24,
						'warranty_unit' => 'month'
					),
				]
			]
		];

		$config['request_technician_from_app_chaang'] = [
			'request_url'=>'https://apichaang.psi.co.th/welcome/getAllPSITechnicial'
		];
		

