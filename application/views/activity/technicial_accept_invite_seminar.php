<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>ตรวจสอบและตอบรับการเข้าสัมนา</title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="Keywords" content="psi ตรวจสอบการลงทะเบียน,ตรวจสอบลงทะเบียน">
  <meta name="Description" content="PSI ตรวจสอบการลงทะเบียนสินค้า">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css')?>">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <style type="text/css"> 
    .mt-2{
      margin-top: 20px;
    }
    .mt-3{
      margin-top: 30px;
    }
    .mb-2{
      margin-bottom: 20px;
    }
    .text-center{
      text-align: center;
    }

    @media(max-width: 425px){
      .h1, h1 {
        font-size: 27px;
      }
    }

  </style>
</head>
<body> 
  <input type="hidden" name="base_url" value="<?php echo base_url()?>">
 
  <div class="container mt-2">
    <div class="row mb-2">
      <div class="col-lg-12 text-center">
        <img src="<?php echo base_url('assets/images/logo/psi_logo21.png')?>" style="max-width: 150px;">
      </div>
    </div>
    <div class="panel panel-primary">
      <div class="panel-heading text-center"> 

        <h3>ข้อมูลการยืนยันการเข้าสัมมนา</h3> 
      </div>
      <div class="panel-body">
        <span class="show_agent_code"></span> 
        <?php echo form_input([
        'type'=>'hidden',
        'name'=>'agentcode'
        ])?>

        <div id="show_data_panel">

        </div>

      </div>
    </div>
  </div>

  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery/jquery.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js')?>"></script> <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <script type="text/javascript" src="<?php echo base_url('assets/activity/js/technicial_accept_invite_seminar.js?'.time())?>"></script>
</body>
</html>