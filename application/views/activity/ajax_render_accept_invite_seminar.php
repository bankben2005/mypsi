<div class="row">

	<div class="col-lg-12" style="text-align: center;">
		<h4 style="text-align: center;"><?php echo $seminar_data->SeminarDesc?></h4>
		<img src="<?php echo $seminar_data->SeminarTickerCover?>" class="img-responsive">


		<?php if(@$seminar_register->AcceptJoin == 1){?>

				<div class="alert alert-info" style="margin-top: 20px;">
					<p>
						<strong>รหัสช่าง : </strong> <?php echo $technician_data->AgentCode?>
					</p>
					<p>
						<strong>สถานะ : </strong> <label class="label label-success">ยืนยันเข้าสัมนา</label>
					</p>
					<p>
						<strong>ชื่อ - สกุล : </strong> <?php echo $technician_data->AgentName?> <?php echo $technician_data->AgentSurName?>
					</p>
					<p>
						<strong>ยืนยันการเข้าสัมนาเมื่อ : </strong> <?php echo $seminar_register->AcceptJoinDatetime->format('d/m/Y H:i')?>
					</p>
				</div>
		<?php }else{?>
				<button type="button" class="btn btn-success btn-block" data-agentcode="<?php echo $technician_data->AgentCode?>" data-seminarno="<?php echo $seminar_data->SeminarNo?>" onclick="handleClickAcceptSeminar(this)" style="margin-top: 20px;">เข้าร่วมสัมนา</button>
		<?php }?>
	</div>
</div>