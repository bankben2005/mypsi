<div class="row">
  <div class="pb-30">
    <div class="col-md-12">
    <!-- <h2>ค้นหาตัวแทนติดตั้ง</h2> -->
  </div>
                      

                      <?php echo form_open('',array('id'=>'search-agent','method'=>'GET'))?>
                        <div class="col-md-12 mb-20 mt-30">
                            
                            <div class="col-md-6 col-sm-6">
                              <?php echo form_dropdown('search_province_select',@$Province,@$this->input->get('search_province_select'),'class="form-control selectpicker" onchange="changeProvince(this.value)" data-live-search="true"');?>
                              <?php echo form_input(array('type'=>'hidden','name'=>'search_province','value'=>@$this->input->get('search_province')))?>
                            </div>
                            <div class="col-md-6 col-sm-6">
                              <?php //echo form_dropdown('search_amphur','',@$this->input->get('search_amphur'),'class="form-control selectpicker" id="search_amphur" data-live-search="true"');?>

                              <?php echo form_input(array('name'=>'search_amphur','value'=>@$this->input->get('search_amphur'),'class'=>'form-control','autocomplete'=>'off','disabled'=>'disabled','placeholder'=>'อำเภอ'));?>
                              <?php echo form_input(array('type'=>'hidden','name'=>'ProvinceID','value'=>''))?>
                            </div>

                            
                        </div>
                        <div class="clearfix"></div>

                        <div id="search_advance" class="collapse mb-30">
                            <div class="col-md-12 mb-20">
                              <div class="col-md-3 col-sm-6">
                              <?php echo form_input(array('name'=>'search_name','value'=>@$this->input->get('search_name'),'class'=>'form-control','placeholder'=>'ชื่อ'))?>
                              </div>

                              <div class="col-md-3 col-sm-6">
                              <?php echo form_input(array('name'=>'search_surname','value'=>@$this->input->get('search_surname'),'class'=>'form-control','placeholder'=>'นามสกุล'))?>
                              </div>

                              <div class="col-md-3 col-sm-6">
                              <?php echo form_input(array('name'=>'search_address','value'=>@$this->input->get('search_address'),'class'=>'form-control','placeholder'=>'ที่อยู่'))?>
                              </div>

                              <div class="col-md-3 col-sm-6">
                              <?php echo form_input(array('name'=>'search_telephone','value'=>@$this->input->get('search_telephone'),'class'=>'form-control','placeholder'=>'เบอร์โทรศัพท์'))?>
                              </div>

                              
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-3 col-sm-6">
                                <?php echo form_input(array('name'=>'search_code','value'=>@$this->input->get('search_code'),'class'=>'form-control','placeholder'=>'รหัสตัวแทนติดตั้ง'))?>
                                </div>

                                <div class="col-md-3 col-sm-6">
                                <?php echo form_input(array('name'=>'search_shopname','value'=>@$this->input->get('search_shopname'),'class'=>'form-control','placeholder'=>'ชื่อร้านค้า'))?>
                                </div>
                                <div class="col-md-12">
                                  <!-- <button type="submit" class="btn btn-success mt-20 mb-20 btn-block"><i class="fa fa-search"></i> ค้นหา</button> -->
                              </div>
                            </div>
                           
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12 text-right">
                                
                                <div class="col-md-12">
                                <div class="row">
                                  <?php if($this->input->get()){?>
                                <a href="<?php echo base_url()?>" class="btn btn-danger"><i class="fa fa-close"></i> รีเซ็ต</a>
                                <?php } ?>
                                <a href="javascript:void(0)" id="mobile_filter" data-toggle="collapse" class="btn btn-default hidden-lg hidden-md hidden-sm"><i class="fa fa-search"></i> ประเภทช่าง</a>
                                <a href="#search_advance" data-toggle="collapse" class="btn btn-default mt-20 mb-20"><i class="fa fa-cog"></i> เพิ่มเติม</a>    
                                <button type="submit" class="btn btn-success mt-20 mb-20"><i class="fa fa-search"></i> ค้นหา</button>
                                  </div>
                                </div>
                                
                            </div>
                          </div>
                          <div class="clearfix"></div>

                          
                        
                        <?php echo form_close();?>
                      

                      
                    <div class="col-md-12">
                       
                        
                            <div class="col-md-3 hidden-sm hidden-xs" id="filter_technician">
                              <div class="panel-group">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#filterOne">ประเภทช่าง</a>
                                  </h4>
                                </div>
                                <div id="filterOne" class="panel-collapse collapse in">
                                  <div class="panel-body">

                                      <div class="col-md-12">
                                        <?php echo form_checkbox('agent_type','FixIt01',(@$this->input->get('FixIt01') == 'true'),'class="with-font" id="FixIt01"');?>
                                        <label for="FixIt01">จานดาวเทียม</label>
                                      </div>

                                      <div class="col-md-12">
                                      <?php echo form_checkbox('agent_type','FixIt02',(@$this->input->get('FixIt02') == 'true'),'class="with-font" id="FixIt02"');?>
                                      <label for="FixIt02">กล้อง OCS</label>
                                    </div>


                                      <div class="col-md-12">
                                      <?php echo form_checkbox('agent_type','FixIt03',(@$this->input->get('FixIt03') == 'true'),'class="with-font" id="FixIt03"');?>
                                      <label for="FixIt03">แอร์ PSI</label>
                                    </div>

                                      <div class="col-md-12">
                                      <?php echo form_checkbox('agent_type','FixIt04',(@$this->input->get('FixIt04') == 'true'),'class="with-font" id="FixIt04"');?>
                                      <label for="FixIt04">ไฟฟ้า</lafel>
                                    </div>

                                      <div class="col-md-12">
                                      <?php echo form_checkbox('agent_type','FixIt05',(@$this->input->get('FixIt05') == 'true'),'class="with-font" id="FixIt05"');?>
                                      <label for="FixIt05">เครื่องแยกน้ำ</label>
                                    </div>

                                    <!-- <div class="col-md-12">
                                      <?php echo form_checkbox('agent_type','CheckAll','','class="with-font" id="FixIt05"');?>
                                      <label for="FixIt05">ทั้งหมด</label>
                                    </div> -->



                                  </div>
                                 
                                </div>
                              </div>
                            </div>
                                
                            </div>
                            <div class="col-md-9">
                               <div class="alert alert-success">
                                      ผลการค้นหาทั้งหมด : <?php echo $result_count.' คน'?>
                               </div>
                               <div id="search_result">
                               <?php if($result_count <= 0){?>
                                    <p class="text-center">ไม่พบข้อมูล กรุณาค้นหาอีกครั้ง</p>
                               <?php }?>   
                               <?php foreach($agent as $key=>$row){?>
                                        <div class="technician_list">
                                        <div class="col-md-2 col-xs-4" style="display: none;">
                                            <img src="<?php echo base_url('assets/images/user/user.png')?>" class="img-responsive">
                                           <p class="text-center agent_code"><?php echo $row->AgentCode;?></p>
                                        </div>
                                        <div class="col-md-5 col-xs-8" id="technician_info">
                                          <p class="firstname-lastname"><strong><?php echo $row->AgentName.' '.$row->AgentSurName;?></strong></p>
                                          <p><strong>ชื่อร้าน : </strong> <?php echo ($row->TradeName)?$row->TradeName:'-'?></p>
                                          <p>
                                            <strong>ที่อยู่ : </strong><?php echo $row->R_Addr1;?>  <?php echo $row->R_District;?> <?php echo $row->R_Province;?>
                                          </p>
                                          <p>
                                            <strong>เบอร์โทร : </strong><?php echo $row->Telephone;?>
                                          </p>
                                          <p>
                                            <strong>สาขาที่ดูแล : </strong><?php echo $row->Branch;?>
                                          </p>
                                        </div>
                                        <div class="col-md-7" id="technician_skill">
                                              <div class="clearfix"></div>

                                                  <?php if($row->FixIt01 && $row->FixIt01 != 'F'){?>
                                                    <div class="col-md-12 col-xs-6">
                                                    <p>
                                                    
                                                      <i class="fa fa-check-square-o"></i>
                                                      <label>จานดาวเทียม </label>
                                                    
                                                    </p>
                                                      </div>
                                                    <?php }?>
                                                    
                                                    <?php if($row->FixIt02 && $row->FixIt02 != 'F'){?>
                                                    <div class="col-md-12 col-xs-6">
                                                    <p>                                                    
                                                    <i class="fa fa-check-square-o"></i>
                                                    <label>กล้อง OCS </label>
                                                    
                                                  </p>
                                                </div>
                                                <?php }?>
                                                 

                                                 <?php if($row->FixIt03 && $row->FixIt03 != 'F'){?>
                                                  <div class="col-md-12 col-xs-6">
                                                    <p>                                                    
                                                    <i class="fa fa-check-square-o"></i>
                                                    <label>แอร์ PSI </label>                                                   
                                                      </p>
                                                    </div>
                                                  <?php }?>

                                                    <?php if($row->FixIt04 && $row->FixIt04 != 'F'){?>
                                                    <div class="col-md-12 col-xs-6">
                                                        <p>                                                    
                                                    <i class="fa fa-check-square-o"></i>
                                                    <label>ไฟฟ้า </label>                                                    
                                                  </p>
                                                </div>
                                                <?php }?>

                                                  <?php if($row->FixIt05 && $row->FixIt05 != 'F'){?>
                                                <div class="col-md-12 col-xs-6">
                                                  <p>

                                                    
                                                    <i class="fa fa-check-square-o"></i>
                                                    <label>เครื่องแยกน้ำ </label>
                                                   
                                                  </p>
                                                </div>
                                                 <?php }?>
                                                <div class="clearfix"></div>

                                                  </div>
                                                 

                                        <!-- </div> -->
                                        <!-- <div class="col-md-2 col-xs-12 text-center">
                                                <a href="javascript:void(0)"><i class="fa fa-map-marker fa-3x"></i></a>
                                        </div> -->
                                        <div class="clearfix"></div>
                                      </div>
                                      
                                      <!-- <hr> -->
                                <?php }?>
                              </div>
                            </div>
                        
                      

                    </div>



                      <div class="col-md-12">
                      <nav aria-label="...">
                          <div class="col-md-12">
                          <?php echo $pages;?>
                        </div>
                      </nav>
                    </div>


              
  </div>

    </div>
