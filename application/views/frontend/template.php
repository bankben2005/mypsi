<!DOCTYPE html>
<html lang="en">

  <head>
    <title><?php echo $seo_title?></title>
    <meta charset="utf-8">
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="<?php echo $seo_title?>">
    <meta name="description" content="<?php echo $seo_description?>">
    

    <?php echo $this->template->meta;?>

    <?php //print_r($this->template->meta)?>
    <!-- meta for facebook -->
    <!-- OG DATA FOR FACEBOOK -->
                     <meta property="fb:app_id" content="199759027239735">
                     <meta property="og:url" content="<?php echo base_url(uri_string())?>">
                     <meta property="og:title" content="<?php echo $seo_title;?>">
                     <meta property="og:description" content="<?php echo $seo_description;?>">
                     <meta property="og:type" content="website">
                     <meta property="og:image" content="<?php echo base_url('uploaded/pagecover/pagecover_test.jpg')?>">
                     <meta property="og:image:width" content="640">
                     <meta property="og:image:height" content="442">


    <!-- eof meta for facebook -->

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('uploaded/icon/psi-icon.ico')?>" />
    <link rel="icon" type="image/x-icon" href="<?php echo base_url('uploaded/icon/psi-icon.ico')?>" />
    
    <?php echo $this->template->stylesheet;?>

  </head>

  <body>
    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
    <!-- <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '199759027239735',
      xfbml      : true,
      version    : 'v2.5'
    });
     
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script> -->
    <div class="loading">Loading&#8230;</div>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-psi navbar-fixed-top">
      <div class="container">
        <a class="navbar-brand" href="https://psisat.com"><img src="<?php echo base_url('assets/images/logo/logo.png')?>"> </a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> -->
        <h1>ตัวแทนติดตั้งมาตรฐาน</h1>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <!-- <li class="nav-item active">
              <a class="nav-link" href="#">หน้าหลัก
                <span class="sr-only">(current)</span>
              </a>
            </li> -->
           <!--  <li class="nav-item">
              <a class="nav-link" href="#">สินค้า</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">ช่องรายการ</a>
            </li> -->
            <!-- <li class="nav-item">
              <a class="nav-link" href="https://psisat.com/newsite/service/">บริการช่วยเหลือ</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://psisat.com/newsite/services-center/">ศูนย์บริการ</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://psisat.com/newsite/contact-us/">ติดต่อเรา</a>
            </li> -->
          </ul>
          
        </div>
      </div>
    </nav>



    
    <!-- Page Content -->
    <div class="container psi-container">

      <?php echo $this->template->content;?>

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright © SoftTech Network Company Limited 2018</p>
      </div>
      <!-- /.container -->
    </footer>
    <!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92024841-5"></script> 
<script> 
window.dataLayer = window.dataLayer || []; 
function gtag(){dataLayer.push(arguments);} 
gtag('js', new Date()); 

gtag('config', 'UA-92024841-5'); 
</script>
    <!-- Bootstrap core JavaScript -->
    <!-- <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
    <?php echo $this->template->javascript;?>

  </body>

</html>
