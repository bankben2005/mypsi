<div class="row">
	<div class="col-md-12">
		<p class="mt-20">
		<a href="<?php echo base_url('questionnaire/Questionnaire?agentcode='.$this->input->get('agentcode'))?>"><i class="fa fa-chevron-circle-left"></i> กลับไปหน้าแรก</a>
		</p>
		
		<?php echo form_input(array('name'=>'agentcode','type'=>'hidden','value'=>''))?>

		<div id="list-branch-pt">
				<?php echo form_open('',array('name'=>'frm-questionnaire'))?>
				<?php echo form_input(array('type'=>'hidden','name'=>'agentcode','value'=>@$this->input->get('agentcode')))?>

				<?php echo form_input(array('type'=>'hidden','name'=>'suname','value'=>@$this->input->get('suname')))?>

				<div class="branch-row">
					<div class="row">
					<!-- <div class="col-md-12 text-center">
							<h3 style="text-decoration: underline;">แบบประเมิน</h3>
					</div> -->
					<div class="col-xs-6">
						<span class="info-title">ชื่อร้าน : </span>
						<p><?php echo ($sunamedata->AgentType == 'AD')?$sunamedata->TradeName.' '.$sunamedata->AgentName:$sunamedata->TradeName;?></p>
						<span class="info-title">ชื่อ-นามสกุล : </span>
						<p><?php echo ($sunamedata->AgentType == 'AD')?'-':$sunamedata->AgentName.' '.$sunamedata->AgentSurName;?></p>
					</div>

					<div class="col-xs-4">
						<span class="info-title">จังหวัด : </span>
						<p>
							<?php echo $sunamedata->Province;?>
						</p>
					</div>
					<div class="col-xs-2">
						
						<div class="clearfix"></div>
					</div>
					</div>
					<div class="clearfix"></div>
				</div>


				<?php if(count($questionnaire) > 0){

					foreach($questionnaire as $key => $row){
					?>

						<div class="question-row">
								<span class="info-title"><?php echo $row->Question;?> : </span>
								<p>
										<input name="rating_<?php echo $row->questionnaire_id;?>" value="0" type="text" class="rating" data-min=0 data-max=5 data-step=1 data-size="xs" title="">
										<!-- <input type="text" name="<?php echo $row->id?>" class="question-rating" required title=""> -->
								</p>


						<div class="clearfix"></div>
						</div>


				<?php } }?>

				<div class="question-row">
							<span class="info-title">เสนอความคิดเห็นเพิ่มเติม :</span>
						<?php echo form_textarea(array('name'=>'Recommend','class'=>'form-control','rows'=>2))?>
						<br>
						<?php echo form_submit('_submit','ส่งแบบสอบถาม','class="btn btn-primary btn-block form-control"')?>
						<a href="<?php echo base_url('questionnaire/Questionnaire?agentcode='.$this->input->get('agentcode'))?>" class="btn btn-default btn-block">กลับไปหน้าแรก</a>
				</div>


			

				<?php echo form_close();?>
		</div>


	</div>
	<div class="clearfix"></div>
</div>