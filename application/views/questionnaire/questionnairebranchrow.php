			<?php 
			if(count($branchrow) > 0){
			foreach($branchrow  as $key => $row){?>
				<div class="branch-row" onclick="gotoQuestionnaire(this)">
					<?php echo form_input(array('name'=>'SUNAME','type'=>'hidden','value'=>$row->AgentCode))?>
					<?php echo form_input(array('type'=>'hidden','name'=>'is_answer','value'=>$row->is_answer))?>
					<div class="row">
					<div class="col-xs-6">
						<span class="info-title">ชื่อร้าน : </span>
						<p><?php echo ($row->AgentType == 'AD')?$row->TradeName.' '.$row->AgentName:$row->TradeName;?></p>
						<span class="info-title">ชื่อ-นามสกุล : </span>
						<p><?php echo ($row->AgentType == 'AD')?'-':$row->AgentName.' '.$row->AgentSurName;?></p>
					</div>

					<div class="col-xs-4">
						<span class="info-title">จังหวัด : </span>
						<p>
							<?php echo $row->Province;?>
						</p>
					</div>
					<div class="col-xs-2">
						<p class="right-icon pull-right">
								<?php if($row->is_answer){?>

								<i class="fa fa-check-circle fa-2x"></i>
								<?php }else{?>
								<i class="fa fa-chevron-right fa-2x"></i>
								<?php }?>
						</p>
						<div class="clearfix"></div>
					</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<?php }?>

			<?php }else{?>
				<div class="alert alert-warning">
					<strong>แจ้งเตือน !</strong>
					<p>
						ไม่พบประวัติการซื้อสินค้า
					</p>
				</div>

			<?php }?>