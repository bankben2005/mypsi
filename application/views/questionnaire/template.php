<!DOCTYPE html>
<html lang="en">

  <head>
    <title><?php echo $seo_title?></title>
    <meta charset="utf-8">
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="<?php echo $seo_title?>">
    <meta name="description" content="<?php echo $seo_description?>">
    

    <?php echo $this->template->meta;?>

    <?php //print_r($this->template->meta)?>
    <!-- meta for facebook -->
    <!-- OG DATA FOR FACEBOOK -->
                     <meta property="fb:app_id" content="199759027239735">
                     <meta property="og:url" content="<?php echo base_url(uri_string())?>">
                     <meta property="og:title" content="<?php echo $seo_title;?>">
                     <meta property="og:description" content="<?php echo $seo_description;?>">
                     <meta property="og:type" content="website">
                     <meta property="og:image" content="<?php echo base_url('uploaded/pagecover/pagecover_test.jpg')?>">
                     <meta property="og:image:width" content="640">
                     <meta property="og:image:height" content="442">


    <!-- eof meta for facebook -->

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('uploaded/icon/psi-icon.ico')?>" />
    <link rel="icon" type="image/x-icon" href="<?php echo base_url('uploaded/icon/psi-icon.ico')?>" />
    
    <?php echo $this->template->stylesheet;?>

  </head>

  <body>
    <div class="loading">Loading&#8230;</div>
    <input type="hidden" name="base_url" value="<?php echo base_url();?>">
    <div class="container">
        <?php echo $this->template->content;?>
    </div>
      


<?php echo $this->template->javascript;?>
  </body>


  </html>