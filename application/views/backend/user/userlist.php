          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('UserList')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    
                    <li class="active"><?php echo __('UserList')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                
                <div class="col-md-12">
                  <div class="row">
                  <a href="<?php echo base_url('backend/'.$this->controller.'/createUser')?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> <?php echo __('Create User')?></a>
                </div>
                </div>
                <div class="clearfix"></div>

                <div class="x_panel">
                  <div class="x_title">
                    
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->
                    <?php echo form_open('',array('name'=>'user-frm'))?>
                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo __('Picture')?></th>
                          <th><?php echo __('Firstname&Lastname')?></th>
                          <th><?php echo __('Email')?></th>
                          <th><?php echo __('AccessType')?></th>
                          <th><?php echo __('Created')?></th>
                          <th><?php echo __('Status')?></th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($userlist as $key => $row){?>
                        <tr>
                          <td></td>
                          <td>
                            <?php if($row['cover']){?>
                                  <img src="<?php echo base_url('uploaded/member/'.$row['member_id'].'/'.$row['cover'])?>" style="height: 50px;width: 50px;">
                            <?php }else{?>
                                  <img src="<?php echo base_url('uploaded/member/default.png')?>" style="height: 50px;width: 50px;">
                            <?php }?>
                          </td>
                          <td><?php echo $row['member_name'].' '.$row['lastname']?></td>
                          <td><?php echo $row['email']?></td>
                          <td><?php echo $row['name']?></td>
                          <td><?php echo $row['member_created']?></td>
                          <td>
                            <?php if($row['active']){?>
                              <label class="label label-success"><?php echo __('Active','backend/default')?></label>

                            <?php }else{?>

                              <label class="label label-danger"><?php echo __('Unactive','backend/default')?></label>
                            <?php }?>
                          </td>
                          <td>
                              
                              <?php if(@$this->event_ability->edit || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                              <a href="<?php echo base_url('backend/'.$this->controller.'/editUser/'.$row['member_id'])?>" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> <?php echo __('Edit','backend/default')?></a>
                              <?php }?>

                              <?php if(@$this->event_ability->delete || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                              <a href="javascript:void(0);" onclick="if(confirm('ต้องการลบใช่หรือไม่') == true){window.location.href='<?php echo base_url('backend/'.$this->controller.'/deleteUser/'.$row['member_id']);?>'}" class="btn btn-danger btn-xs">
                                <i class="fa fa-trash"></i> <?php echo __('Delete','backend/default');?>
                              </a>
                              <?php }?>

                          </td>

                        </tr>
                        <?php }?>                         
                      </tbody>

                    </table>


                      <?php echo form_close();?>
                    

                  </div>
                </div>

              </div>

            </div>