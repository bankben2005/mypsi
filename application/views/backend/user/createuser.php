          <div class="page-title">
              <div class="title_left">
                <h3><?php echo ($member['id'])?__('Edit'):__('Create').''.__('User')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>

                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/userlist')?>"><?php echo __('UserList')?></a></li>
                    
                    <li class="active"><?php echo ($member['id'])?__('Edit'):__('Create').__('User')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">
                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php ($member['id'])?__('Edit'):__('Create').__('User')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php echo form_open_multipart('',array('name'=>'createuser-frm'))?>
                      <?php echo form_input(array('type'=>'hidden','name'=>'frm_member_id','value'=>@$member['id']))?>
                      <div class="col-md-3">
                        <div class="row">

                          <div class="form-group">
                              <?php 
                                $image_path = "";

                                if($member['cover'] && file_exists('uploaded/member/'.$member['id'].'/'.$member['cover'])){
                                  $image_path = base_url('uploaded/member/'.$member['id'].'/'.$member['cover']);

                                }else{
                                  $image_path = base_url('uploaded/member/default.png');

                                }
                              ?>
                              <img src="<?php echo $image_path?>" alt="..." class="img-thumbnail img-responsive profile_img" style="height: 180px;">
                          </div>
                          <div class="input-group">
                              <span class="input-group-btn">
                                  <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('member_cover', '', '');?>
                                  </span>
                              </span>
                              <input type="text" class="form-control" readonly>
                          </div>

                        </div>
                      </div>

                      <div class="col-md-9">
                          <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="name"><?php echo __('Firstname')?> : </label>
                                  <?php echo form_input(array('name'=>'name','value'=>@$member['name'],'class'=>'form-control','id'=>'name'))?>
                                </div>
                                 <div class="form-group">
                                  <label for="email"><?php echo __('Email')?> : </label>
                                  <?php echo form_input(array('name'=>'email','value'=>@$member['email'],'class'=>'form-control','id'=>'email'))?>
                                </div>
                                <div class="form-group">
                                  <label><?php echo __('Access Type')?> : </label>
                                  <?php echo form_dropdown('member_access_types_id',$member_access_types,@$member['member_access_types_id'],'class="form-control"')?>
                                </div>

                                <div class="form-group">
                                    <label><?php echo __('Status','backend/default')?> : </label>
                                    <?php echo form_dropdown('active',array('0'=>__('Unactive','backend/default'),'1'=>__('Active','backend/default')),@$member['active'],'class="form-control"')?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label for="lastname"><?php echo __('Lastname')?> : </label>
                                  <?php echo form_input(array('name'=>'lastname','value'=>@$member['lastname'],'class'=>'form-control','id'=>'lastname'))?>
                                </div>

                                <?php if($member){?>
                                <div class="form-group">
                                  <a href="javascript:void(0);" data-toggle="modal" data-target="#resetPasswordModal" data-member="<?php echo @$member['id']?>" class="btn-resetpassword btn btn-primary btn-block"><i class="fa fa-key"></i> <?php echo __('Reset Password')?></a>
                                </div>
                                <?php }else{?>
                                <div class="form-group">
                                  <label for="member_password"><?php echo __('Password')?> : </label>
                                  <?php echo form_input(array('type'=>'password','name'=>'member_password','class'=>'form-control','id'=>'member_password'))?>
                                </div>
                                <div class="form-group">
                                  <label for="member_confirm_password"><?php echo __('Confirm Password')?> : </label>
                                  <?php echo form_input(array('type'=>'password','name'=>'member_confirm_password','class'=>'form-control','id'=>'member_confirm_password'))?>
                                </div>
                                <?php }?>
                            </div>

                            <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
                            <div class="clearfix">
                          </div>
                        </div>
                      </div>
                   
                          </div>
                      </div>
                       <?php echo form_close();?>
                      <div class="clearfix"></div>
                      <hr>
                      <?php //print_r($member);?>
                      <?php if($member && @$member['member_access_types_id'] != '1' && @$member['member_access_types_id'] != '2'){?>
                      <div class="col-md-12">
                                <h2 class="text-center"><?php echo __('User Permissions')?></h2>
                                <table class="table table-striped projects">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th><?php echo __('Menu')?></th>
                                    <th><?php echo __('Controller Name')?></th>
                                    <th><?php echo __('Method Name')?></th>
                                    <th><?php echo __('Status','backend/default')?></th>
                                    <th><?php echo __('Event Availability')?></th>
                                    <th style="width: 20%"></th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($member_menus as $key => $row){?>
                                      <tr>
                                          <td></td>
                                          <td><?php echo $row['name']?></td>
                                          <td><?php echo $row['controller_name']?></td>
                                          <td><?php echo $row['method_name']?></td>
                                          <td>
                                            <?php if(checkIsPermissionAvailable($row['id'],$member_permissions)){?>
                                              <a href="javascript:void(0);" data-active="active" data-user="<?php echo @$member['id']?>" data-menu="<?php echo $row['id']?>" class="btn-act-active btn btn-success btn-xs"><?php echo __('Active')?></a>

                                            <?php }else{?>
                                              <a href="javascript:void(0);" data-active="unactive" data-user="<?php echo @$member['id']?>" data-menu="<?php echo $row['id']?>" class="btn-act-active btn btn-danger btn-xs"><?php echo __('Unactive')?></a>

                                            <?php }?>
                                          </td>
                                          <td>
                                            <?php if(checkIsPermissionAvailable($row['id'],$member_permissions)){?>
                                            <label><?php echo form_checkbox(array(
                                              'name'=>'view',
                                              'checked'=>getEventAbility('view',$row['id'],$member_permissions),
                                              'data-user'=>@$member['id'],
                                              'data-menu'=>$row['id'],
                                              'data-event'=>'view',
                                              'class'=>'event-checkbox'
                                              ))?> <?php echo __('View')?> /</label>

                                            <label><?php echo form_checkbox(array(
                                              'name'=>'edit',
                                              'checked'=>getEventAbility('edit',$row['id'],$member_permissions),
                                              'data-user'=>@$member['id'],
                                              'data-menu'=>$row['id'],
                                              'data-event'=>'edit',
                                              'class'=>'event-checkbox'
                                              ))?> <?php echo __('Edit')?> /</label>

                                            <label><?php echo form_checkbox(array(
                                              'name'=>'delete',
                                              'checked'=>getEventAbility('delete',$row['id'],$member_permissions),
                                              'data-user'=>@$member['id'],
                                              'data-menu'=>$row['id'],
                                              'data-event'=>'delete',
                                              'class'=>'event-checkbox'
                                            ))?> <?php echo __('Delete')?></label>
                                            <?php }?>
                                          </td>
                                          <td>
                                            
                                          </td>
                                      </tr
                                    <?php }?>
                                </tbody>
                                </table>
                      </div>
                      <?php }?>
                      
                    

                    
                </div>

              </div>
            </div>
          </div>

<!-- Modal -->
<div id="resetPasswordModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-key"></i> <?php echo __('Reset Password')?></h4>
      </div>
      <?php echo form_open('',array('name'=>'reset-frm'))?>
      <?php echo form_input(array('type'=>'hidden','name'=>'members_id'))?>
      <div class="modal-body">

        <div class="col-md-6 col-md-offset-3">
          <div class="form-group">
            <label for="password"><?php echo __('New password')?> : </label>
            <?php echo form_input(array('type'=>'password','name'=>'password','class'=>'form-control','id'=>'password'))?>
          </div>
          <div class="form-group">
            <label for="confirm_password"><?php echo __('Confirm new password')?> : </label>
            <?php echo form_input(array('type'=>'password','name'=>'confirm_password','class'=>'form-control','id'=>'confirm_password'))?>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><?php echo __('Save','backend/default')?></button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
      <?php echo form_close();?>
    </div>

  </div>
</div>
