          <div class="page-title">
              <div class="title_left">
                <h3><?php echo ($coupon->getId())?__('Edit'):__('Create').__('Coupon')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>

                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/index')?>"><?php echo __('CouponList')?></a></li>
                    
                    <li class="active"><?php echo ($coupon->getId())?__('Edit'):__('Create').__('Coupon')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">
                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php ($coupon->getId())?__('Edit'):__('Create').__('Coupon')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php echo form_open_multipart('',array('id'=>'createcoupon-frm'))?>

                      <div class="col-md-5">
                        <div class="row">
                          <div class="form-group">
                            <label><?php echo __('CouponType')?></label>
                            <?php echo form_dropdown('CouponType_id',$coupontype,@$coupon->CouponType_id,'class="form-control"')?>
                          </div>

                          <div class="form-group">
                            <label for="Code"><?php echo __('Coupon Code')?> : <span class="error"></span></label>
                            <?php echo form_input(array('name'=>'Code','value'=>@$coupon->Code,'class'=>'form-control','id'=>'Code'))?>
                          </div>

                          <div class="form-group">
                            <label for="Name"><?php echo __('Coupon Name')?> : </label>
                            <?php echo form_input(array('name'=>'Name','value'=>@$coupon->Name,'class'=>'form-control','id'=>'Name'))?>
                          </div>

                          <div class="form-group">
                              <label for="Description"><?php echo __('Coupon Description')?> : </label>
                              <?php echo form_textarea(array('name'=>'Description','value'=>@$coupon->Description,'class'=>'form-control','rows'=>3,'id'=>'Description'))?>
                          </div>

                          <div class="form-group">
                              <label for="PointUse"><?php echo __('Point Use')?> : </label>
                              <?php echo form_input(array(
                                  'name'=>'PointUse',
                                  'value'=>@$coupon->PointUse,
                                  'class'=>'form-control',
                                  'onkeypress'=>'onlynumber(event)',
                                  'id'=>'PointUse'
                                ))?>
                          </div>
                          <div class="form-group">
                            <label for="DiscountRate"><?php echo __('Discount Rate')?> : </label>
                            <?php echo form_input(array('name'=>'DiscountRate','value'=>@$coupon->DiscountRate,'class'=>'form-control','onkeypress'=>'onlynumber(event)','id'=>'DiscountRate'))?>
                          </div>

                          <div class="form-group">
                            <label><?php echo __('Discount Type')?> : </label>
                            <?php echo form_dropdown('DiscountType',array('amount'=>__('Amount'),'percent'=>__('Percent')),@$coupon->DiscountType,'class="form-control"')?>
                          </div>

                          

                          <div class="form-group">
                            <label><?php echo __('Duration')?> : </label>
                            <?php echo form_input(array('name'=>'Duration','value'=>@$duration,'class'=>'form-control'))?>
                          </div>

                          
                          
                          <div class="form-group">
                            <label><?php echo __('Active')?> : </label>
                            <?php echo form_dropdown('Active',array('1'=>__('Active'),'0'=>__('Unactive')),@$questionnaire->Active,'class="form-control"')?>
                          </div>
                          
                      </div>
                      </div>
                      <div class="col-md-7">
                                    <div class="form-group">
                                        <label>รูปภาพคูปอง : gif|jpg|png</label>
                                        <div class="clearfix"></div>
                                        <?php if($coupon->Image && file_exists('uploaded/coupon/coupon_image/'.$coupon->id.'/'.$coupon->Image)){?>
                                        
                                        <div class="view view-first" style="margin:5px 0px;">
                                                    <img src="<?php echo base_url().'uploaded/coupon/coupon_image/'.$coupon->id.'/'.$coupon->Image;?>" style="width:150px;height: 100px;">
                                                     <div class="mask">
                                                      <a href="javascript:void(0);" class="pop btn btn-default"><i class="fa fa-eye"></i></a>
                                                         <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('backend/').$this->controller.'/delete_image/'.$coupon->id.'/image';?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                     </div>
                                        </div>
                                   
                                        
                                        <?php }?>
                                    
                                    <div class="clearfix"></div>
                                    
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('coupon_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                                         </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label>รูปภาพคูปอง (ใช้งานแล้ว) : gif|jpg|png</label>
                                        <div class="clearfix"></div>
                                        <?php if($coupon->ImageUsed && file_exists('uploaded/coupon/coupon_imageused/'.$coupon->id.'/'.$coupon->ImageUsed)){?>
                                        
                                        <div class="view view-first" style="margin:5px 0px;">
                                                    <img src="<?php echo base_url().'uploaded/coupon/coupon_imageused/'.$coupon->id.'/'.$coupon->ImageUsed;?>" style="width:150px;height: 100px;">
                                                     <div class="mask">
                                                         <a href="javascript:void(0);" class="pop btn btn-default"><i class="fa fa-eye"></i></a>
                                                         <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('backend/').$this->controller.'/delete_image/'.$coupon->id.'/used';?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                     </div>
                                        </div>
                                   
                                        
                                        <?php }?>
                                    
                                    <div class="clearfix"></div>
                                    
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('coupon_imageused', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                                         </div>
                                    </div>
                                    <hr>

                                    <div class="form-group">
                                        <label>รูปภาพคูปอง (หมดอายุแล้ว) : gif|jpg|png</label>
                                        <div class="clearfix"></div>
                                        <?php if($coupon->ImageExpired && file_exists('uploaded/coupon/coupon_imageexpired/'.$coupon->id.'/'.$coupon->ImageExpired)){?>
                                        
                                        <div class="view view-first" style="margin:5px 0px;">
                                                    <img src="<?php echo base_url().'uploaded/coupon/coupon_imageexpired/'.$coupon->id.'/'.$coupon->ImageExpired;?>" style="width:150px;height: 100px;">
                                                     <div class="mask">
                                                      <a href="javascript:void(0);" class="pop btn btn-default"><i class="fa fa-eye"></i></a>
                                                         <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('backend/').$this->controller.'/delete_image/'.$coupon->id.'/expired';?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                     </div>
                                        </div>
                                   
                                        
                                        <?php }?>
                                    
                                    <div class="clearfix"></div>
                                    
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('coupon_imageexpired', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                                         </div>
                                    </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                            <?php echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
                            <div class="clearfix">
                          </div>
                        </div>
                      </div>
                    <?php echo form_close();?>
                    

                    
                </div>

              </div>
            </div>
          </div>

          <!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('Image Preview')?></h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" class="img-responsive" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>
