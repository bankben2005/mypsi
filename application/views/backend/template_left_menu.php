<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url('backend')?>" class="site_title"><i class="fa fa-cog"></i> <span>PSI FIXIT!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo getCoverImage($this->account_data['data'])?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span><?php echo __('Welcome')?>,</span>
                <h2><?php echo $this->account_data['data']['name']?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <?php if(in_array('admin_dashboard',$this->account_data['member_controller_available']) || in_array('admin_customer', $this->account_data['member_controller_available']) || in_array('admin_technician', $this->account_data['member_controller_available'])){?>
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu"> 


                  <?php if(in_array('admin_dashboard',$this->account_data['member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                  <li><a href="<?php echo base_url('backend')?>"><i class="fa fa-home"></i> <?php echo __('DASHBOARD')?></a>
                  </li>
                  <?php }?> 

                  <li><a href="<?php echo base_url('backend/admin_optional/psicare')?>"><i class="fa fa-home"></i> <?php echo __('PSICARE BENEFICIARY')?></a>
                  </li>



                  <?php if(in_array('admin_customer',$this->account_data['member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                  <li><a><i class="fa fa-edit"></i> <?php echo __('CUSTOMERS')?> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_customer','method_name'=>'index')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li><a href="<?php echo base_url('backend/admin_customer')?>"><?php echo __('All Customers')?></a></li>
                      <?php }?>
                     

                    </ul>
                  </li>
                  <?php }?>



                  <?php if(in_array('admin_technician',$this->account_data['member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                  <li><a><i class="fa fa-desktop"></i> <?php echo __('TECHICIAN')?> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_technician','method_name'=>'satellite')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li><a href="<?php echo base_url('backend/admin_technician/satellite')?>"><?php echo __('Satellite')?></a></li>
                      <?php }?>

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_technician','method_name'=>'ocs')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li><a href="<?php echo base_url('backend/admin_technician/ocs')?>"><?php echo __('OCS Camera')?></a></li>
                      <?php }?>

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_technician','method_name'=>'airconditioner')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li><a href="<?php echo base_url('backend/admin_technician/airconditioner')?>"><?php echo __('PSI Airconditioner')?></a></li>
                      <?php }?>

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_technician','method_name'=>'electricity')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li><a href="<?php echo base_url('backend/admin_technician/electricity')?>"><?php echo __('Electricity')?></a></li>
                      <?php }?>

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_technician','method_name'=>'waterfilter')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li><a href="<?php echo base_url('backend/admin_technician/waterfilter')?>"><?php echo __('Water Filter')?></a></li>
                      <?php }?>
                    
                    </ul>
                  </li>

                  <?php }?>
                  
                </ul>
              </div>

              <?php }?>


              <?php if(in_array('admin_technician',$this->account_data['member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
              <div class="menu_section">
                <h3><?php echo __('Product')?></h3>
                <ul class="nav side-menu">
                  
                  <li><a><i class="fa fa-sitemap"></i> <?php echo __('Product List')?> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a><?php echo __('Water Filter')?><span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="<?php echo base_url('backend/admin_product/export_expired_filter')?>"><?php echo __('Export expired filter')?></a>
                            </li>                            
                          </ul>
                        </li>
                       
                    </ul>
                  </li>                  
                  
                </ul>
              </div>
              <?php }?>

              <?php if(in_array('admin_staff',$this->account_data['member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available']) || $this->account_data['data']['member_access_types_id'] == '4'){?>
              <!-- Staff menu-->

               <div class="menu_section" style="">
                <h3><?php echo __('Staff')?></h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bar-chart"></i> <?php echo __('Seminar')?> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_staff','method_name'=>'clothdeal')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li>
                        <a href="<?php echo base_url('backend/admin_staff/clothdeal')?>">
                          <?php echo __('Cloth Deal');?>
                            
                        </a>
                      </li>
                      <?php }?>

                      <li>
                        <a href="<?php echo base_url('backend/admin_staff/seminar_permit')?>">
                          <?php echo __('Check Seminar Available')?>
                        </a>
                      </li>

                     <!--  <li>
                        <a href="<?php echo base_url('backend/admin_staff/s3dealsseminarlist')?>">
                          <?php echo __('S3 Deals')?>
                        </a>
                      </li> -->

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_staff','method_name'=>'seminarorder')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li>
                      <li>
                        <a href="<?php echo base_url('backend/admin_staff/seminarorder')?>">
                          <?php echo __('Seminar Order')?>
                        </a>
                      </li>
                      <?php }?>

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_staff','method_name'=>'orderproductlist')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li>
                      <li>
                        <a href="<?php echo base_url('backend/admin_staff/orderproductlist')?>">
                          <?php echo __('Seminar Order Product List')?>
                        </a>
                      </li>
                      <?php }?>

                      <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_staff','method_name'=>'orderproductcancel')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                      <li>
                      <li>
                        <a href="<?php echo base_url('backend/admin_staff/orderproductcancel')?>">
                          <?php echo __('Seminar Order Product Cancel')?>
                        </a>
                      </li>
                      <?php }?>


                    </ul>
                  </li>

                  <li><a><i class="fa fa-clipboard"></i> <?php echo __('Questionnaire')?> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_questionnaire','method_name'=>'questionnaireSetList')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                          <li>
                            <a href="<?php echo base_url('backend/admin_questionnaire/questionnaireSetList')?>">
                              <?php echo __('Questionnaire Set List');?>
                                
                            </a>
                          </li>
                          <?php }?>

                        <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_questionnaire','method_name'=>'questionnaireCategory')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                          <li>
                            <a href="<?php echo base_url('backend/admin_questionnaire/questionnaireCategory')?>">
                              <?php echo __('Questionnaire Category List');?>
                                
                            </a>
                          </li>
                          <?php }?>

                        <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_questionnaire','method_name'=>'index')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                          <li>
                            <a href="<?php echo base_url('backend/admin_questionnaire/index')?>">
                              <?php echo __('Questionnaire List');?>
                                
                            </a>
                          </li>
                          <?php }?>

                          <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_questionnaire','method_name'=>'questionnaireAnswerList')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                          <li>
                            <a href="<?php echo base_url('backend/admin_questionnaire/questionnaireAnswerList')?>">
                              <?php echo __('Questionnaire Answer List');?>
                                
                            </a>
                          </li>
                          <?php }?>

                          

                    </ul>
                  </li>


                  <!-- for promotion -->
                  <li><a><i class="fa fa-bullhorn"></i> <?php echo __('Promotion')?> <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_promotion','method_name'=>'couponlist')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                          <li>
                            <a href="<?php echo base_url('backend/admin_promotion/couponlist')?>">
                              <?php echo __('Coupon');?>
                                
                            </a>
                          </li>
                          <?php }?>

                    </ul>
                  </li>

                  <!-- eof promotion -->

                </ul>
              </div>
              <!-- Eof staff menu-->
              <?php }?>


              <?php if(in_array('admin_additional',$this->account_data['member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
              <!-- Register Ofive  menu-->

               <div class="menu_section" style="">
                  <h3><?php echo __('Additional')?></h3>
                  <ul class="nav side-menu">
                    <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_additional','method_name'=>'registerofivelist')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                    <li>
                      <a href="<?php echo base_url('backend/admin_additional/registerofivelist')?>">
                        <i class="fa fa-registered"></i> <?php echo __('RegisterOFive')?> 
                      </a>
                    </li>
                    <?php }?>
                    
                  </ul>
               </div>
               <?php }?>

               <?php if(in_array('admin_psiarm',$this->account_data['member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
              <!-- Register Ofive  menu-->

                          <div class="menu_section" style="">
                              <h3><?php echo __('Activity')?></h3>
                              <ul class="nav side-menu">
                                <li><a><i class="fa fa-comments"></i> <?php echo __('Manage Activity')?> <span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">

                                    <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_psiarm','method_name'=>'activitylist')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                    <li>
                                      <a href="<?php echo base_url('backend/admin_psiarm/activitylist')?>">
                                        <?php echo __('ActivityList');?>
                                          
                                      </a>
                                    </li>
                                    <?php }?>

                                    <?php if(checkAvailableMethod($this->account_data['member_permissions'],array(
                                      'controller_name'=>'admin_psiarm',
                                      'method_name'=>'flashsale'
                                    )) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                      <li>
                                      <a href="<?php echo base_url('backend/admin_psiarm/flashsale')?>">
                                        <?php echo __('FlashSale')?>
                                      </a>
                                      </li>
                                    <?php }?>

                                    <?php if(checkAvailableMethod($this->account_data['member_permissions'],array(
                                      'controller_name'=>'admin_psiarm',
                                      'method_name'=>'notification'
                                    )) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                      <li>
                                      <a href="<?php echo base_url('backend/admin_psiarm/notification')?>">
                                        <?php echo __('Notification')?>
                                      </a>
                                      </li>
                                    <?php }?>


                                  </ul>
                                </li>
                              </ul>
                            </div>
               <?php }?>



              <?php if(in_array('admin_user',$this->account_data['member_controller_available']) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                            <!-- Staff menu-->

                             <div class="menu_section" style="">
                              <h3><?php echo __('User Controller')?></h3>
                              <ul class="nav side-menu">
                                <li><a><i class="fa fa-user"></i> <?php echo __('Manage User')?> <span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">

                                    <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_user','method_name'=>'userlist')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                    <li>
                                      <a href="<?php echo base_url('backend/admin_user/userlist')?>">
                                        <?php echo __('User List');?>
                                          
                                      </a>
                                    </li>
                                    <?php }?>


                                    <?php if(checkAvailableMethod($this->account_data['member_permissions'],array('controller_name'=>'admin_user','method_name'=>'membermenulist')) || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                    <li>
                                      <a href="<?php echo base_url('backend/admin_user/membermenulist')?>">
                                        <?php echo __('Member Menu List');?>
                                          
                                      </a>
                                    </li>
                                    <?php }?>

                                  </ul>
                                </li>
                              </ul>
                            </div>
                <?php }?>
             

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url('backend/Login/logout')?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>