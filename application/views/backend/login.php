<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Administrator Login</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css')?>">
                
                <style type="text/css">
                    .alert {
    background: #fffaef;
    border-color: #ecdcb6;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset;
    -moz-box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset;
    box-shadow: 0 1px 0 rgba(255,255,255,0.5) inset;
    color: #b68b53
}
.alert strong, .alert b {
    color: inherit
}
.alert-danger, .alert-error {
    background: #f7e7e4;
    border-color: #ecbeb6;
    color: #b55351
}
.alert-success {
    background: #eaf4e2;
    border-color: #c1dea9;
    color: #61a06f
}
                    </style>
    </head>
    <body>
<!--login modal-->
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true" style="background: #dddddd;">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <center><img src="<?php echo base_url('assets/seminar/images/logo_psi.png')?>" height="100" width="100"></center>
          <h1 class="text-center">BACKEND-SYSTEM</h1>
          
          
      </div>
      <div class="modal-body" style="overflow: auto;">
          <div class="col-md-12">
          <?php echo message_warning($this);?>
      </div>
          <?php echo form_open('',array('class'=>'form col-md-12 center-block','id'=>'login-form'));?>
         
            <div class="form-group">
<!--              <input type="text" class="form-control input-lg" placeholder="Email">-->
              <?php echo form_input(array('name'=>'email','class'=>'form-control input-lg','placeholder'=>'Email or Username'));?>
            </div>
            <div class="form-group">
                
<!--              <input type="password" class="form-control input-lg" placeholder="Password">-->
              <?php echo form_password('password', '', 'class="form-control input-lg" placeholder="Password"');?>
            </div>
            <div class="form-group">
<!--              <button class="btn btn-primary btn-lg btn-block">Sign In</button>-->
              <?php echo form_submit('submit','Sign in','class="btn btn-primary btn-lg btn-block"');?>
             
            </div>
          <?php echo form_close();?>
          
      </div>
      <div class="modal-footer">
          
      </div>
  </div>
  </div>
</div>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery/jquery.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js')?>"></script>
    </body>
</html>


<script type="text/javascript">
  function urlParameter() {
    var url = window.location.href,
    retObject = {},
    parameters;

    if (url.indexOf('?') === -1) {
        return null;
    }

    url = url.split('?')[1];

    parameters = url.split('&');

    for (var i = 0; i < parameters.length; i++) {
        retObject[parameters[i].split('=')[0]] = parameters[i].split('=')[1];
    }

    return retObject;
}

  $(document).ready(function(){
      var parameters = urlParameter();
      // console.log(parameters); return false;
      if(parameters.username && parameters.password){

        $('input[name="email"]').val(parameters.username);
        $('input[name="password"]').val(parameters.password);

        $('input[type="submit"]').trigger('click');
      }
  });



</script>