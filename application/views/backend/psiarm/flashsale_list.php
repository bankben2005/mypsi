          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('FlashSale List')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    
                    <li class="active"><?php echo __('FlashSale List')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                
                <div class="col-md-12">
                  <div class="row">
                  <a href="javascript:void(0)" onclick="checkCurrentFlashSaleExist()" class="btn btn-success pull-right"><i class="fa fa-plus"></i> <?php echo __('Create FlashSale')?></a>
                </div>
                </div>
                <div class="clearfix"></div>

                <div class="x_panel">
                  <div class="x_title">
                    
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->
                    <?php echo form_open('',array('name'=>'coupon-frm'))?>
                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th>#</th>
                          
                          <th><?php echo __('Name')?></th>
                          <th><?php echo __('Description')?></th>
                          <th><?php echo __('Start')?></th>
                          <th><?php echo __('End')?></th>
                          <th><?php echo __('Created')?></th>
                          <th><?php echo __('Updated')?></th>
                          <th><?php echo __('Expired Status')?></th>
                          <th><?php echo __('Status')?></th>
                          <th><?php echo __('Remaining')?></th>
                          <th><?php echo __('Product Quantity')?></th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($flashsaleset->get() as $key => $row){?>
                          <tr>
                            <td><?php echo ++$key?></td>
                            <td><?php echo $row->name?></td>
                            <td><?php echo $row->description?></td>
                            <td><?php echo $row->start_datetime?></td>
                            <td><?php echo $row->end_datetime?></td>
                            <td><?php echo $row->created?></td>
                            <td><?php echo $row->updated?></td>
                            <td>
                                <?php if(strtotime(date('Y-m-d H:i:s')) > strtotime($row->end_datetime)){?>
                                  <label class="label label-warning"><?php echo __('Expired')?></label>
                                <?php }else{?>
                                  <label class="label label-success"><?php echo __('Active')?></label>
                                <?php }?>
                            </td>
                            <td>
                              <?php if($row->active){?>
                                  <label class="label label-success"><?php echo __('Active','default')?></label>
                              <?php }else{?>
                                  <label class="label label-danger"><?php echo __('Unactive','default')?></label>
                              <?php }?>
                            </td>

                            <td>
                              <div id="flashsaleset_<?php echo $row->id?>">

                              </div>
                            </td>
                            <td>
                              <?php echo $row->flashsalesetproduct->get()->result_count()?>
                            </td>

                            <td align="center">

                              <a href="<?php echo base_url('backend/'.$this->controller.'/view_flashsale/'.$row->id)?>" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>




                              <!-- <a href="javascript:void(0)" onclick="if(confirm('ต้องการลบใช่หรือไม่') == true){window.location.href='<?php echo base_url('backend/'.$this->controller.'/delete_flashsale/'.$row->id)?>'}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a> -->

                              
                              <?php echo form_input(array(
                                'name'=>'hide_end_datetime[]',
                                'type'=>'hidden',
                                'value'=>$row->end_datetime,
                                'data-flashid'=>$row->id
                              ))?>

                            </td>

                          </tr>
                        <?php }?>
                      </tbody>
                    </table>
                  </div>
              </div>
            </div>
          </div>
