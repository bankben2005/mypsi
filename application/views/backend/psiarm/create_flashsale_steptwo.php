          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Create').__('Flash Sale')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>

                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/flashsale')?>"><?php echo __('FlashSale List')?></a></li>
                    
                    <li class="active"><?php echo __('Create').__('FlashSale')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">
                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo __('Create').__('FlashSale')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="row bs-wizard" style="border-bottom:0;">
                
                    <div class="col-lg-4 bs-wizard-step complete">
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 1','backend/psiarm/create_flashsale')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="<?php echo base_url('backend/'.$this->controller.'/createFlashSale')?>" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Information Form','backend/psiarm/create_flashsale')?></div>
                    </div>
                    
                    <div class="col-lg-4 bs-wizard-step active"><!-- complete -->
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 2','backend/psiarm/create_flashsale')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Choose Product Item(s)','backend/psiarm/create_flashsale')?></div>
                    </div>
                    
                    <div class="col-lg-4 bs-wizard-step disabled"><!-- complete -->
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 3','backend/psiarm/create_flashsale')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Confirm PR','backend/psiarm/create_flashsale')?></div>
                    </div>
                
                  </div>

                  <!-- <div class="row">
                    <div class="col-lg-12">
                      <a href="#" class="btn btn-success pull-right"><?php echo __('Add More Product')?></a>
                    </div>
                  </div> -->
                  <div class="x_content">
                    <?php echo form_open('',array('name'=>'create-flashsalesteptwo-frm'))?>

                      <div class="row row-product">
                        <div class="col-md-6">

                          <div class="panel panel-default">
                            <div class="panel-heading text-center"><?php echo __('Search Product')?></div>
                            <div class="panel-body">

                                
                                <div class="form-group">
                                  <label>
                                    <?php echo form_radio(array(
                                      'name'=>'search_type',
                                      'value'=>'mpcode',
                                      'checked'=>TRUE
                                    ))?> <span><?php echo __('Search By MPCODE')?></span>
                                  </label>


                                  <label>
                                    <?php echo form_radio(array(
                                      'name'=>'search_type',
                                      'value'=>'product_name'
                                    ))?> <span><?php echo __('Search By ProductName')?></span>
                                  </label>
                                </div>

                                <div class="input-group" id="search-type-mpcode">
                                      <!-- <input type="text" class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2"> -->
                                      <?php echo form_input(array(
                                        'name'=>'search_mpcode',
                                        'class'=>'form-control',
                                        'placeholder'=>__('MPCODE..')
                                      ))?>
                                      <span class="input-group-addon" id="basic-addon2"><a href="javascript:void(0)" onclick="searchMPCODE(this)" class="anchor-search-mpcode"><i class="fa fa-search"></i> <?php echo __('Search','default')?></a></span>
                                </div>

                                <div class="input-group" id="search-type-mpname" style="display: none;">
                                      <?php echo form_input(array(
                                          'name'=>'search_mpname',
                                          'class'=>'form-control',
                                          'placeholder'=>__('MPNAME..')
                                      ))?>
                                      <span class="input-group-addon" id="basic-addon2"><a href="javascript:void(0)" onclick="searchMPNAME(this)" class="anchor-search-mpname"><i class="fa fa-search"></i> <?php echo __('Search','default')?></a></span>
                                </div>


                                <div id="search_result">

                                </div>


                            </div>
                          </div>

                          



                        </div>
                        <div class="col-md-6" style="border-left: 1px solid #ddd;">
                            <div class="panel panel-default">
                                <div class="panel-heading text-center"><?php echo __('Product Choosed')?></div>
                                <div class="panel-body">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th><?php echo __('Product Code')?></th>
                                          <th><?php echo __('Product Name')?></th>
                                          <th><?php echo __('Quantity')?></th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        
                                      </tbody>
                                      
                                    </table>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12">

                            <?php echo form_button(array(
                              'type'=>'submit',
                              'class'=>'btn btn-success pull-right',
                              'content'=>__('Continue','default')
                            ))?>

                            <?php echo form_button(array(
                              'type'=>'button',
                              'class'=>'btn btn-default pull-right',
                              'content'=>__('Back'),
                              'onclick'=>"window.location.href='".base_url('backend/'.$this->controller.'/createFlashSale')."'"
                            ))?>


                        </div>
                      </div>  
                    <?php echo form_close()?>

                  </div>
              </div>
            </div>
          </div>