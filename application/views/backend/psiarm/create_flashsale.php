          <div class="page-title">
              <div class="title_left">
                <h3><?php echo ($flashsaleset->getId())?__('Edit'):__('Create').__('Flash Sale')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>

                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/flashsale')?>"><?php echo __('FlashSale List')?></a></li>
                    
                    <li class="active"><?php echo ($flashsaleset->getId())?__('Edit'):__('Create').__('FlashSale')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">
                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo ($flashsaleset->getId())?__('Edit'):__('Create').__('FlashSale')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="row bs-wizard" style="border-bottom:0;">
                
                    <div class="col-lg-4 bs-wizard-step active">
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 1')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Information Form')?></div>
                    </div>
                    
                    <div class="col-lg-4 bs-wizard-step disabled"><!-- complete -->
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 2')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Choose Product Item(s)')?></div>
                    </div>
                    
                    <div class="col-lg-4 bs-wizard-step disabled"><!-- complete -->
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 3')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Confirm PR')?></div>
                    </div>
                
                  </div>
                  <div class="x_content">
                    <?php echo form_open_multipart('',array('name'=>'create-flashsale-frm'))?>
                        <div class="row">
                          <div class="col-md-5">
                              <div class="form-group">
                                <label><strong><?php echo __('Name')?> : </strong></label>
                                <?php echo form_input(array(
                                  'name'=>'name',
                                  'class'=>'form-control',
                                  'value'=>@$this->session->userdata('stepone')['name']
                                ))?>
                              </div>

                              <div class="form-group">
                                <label><strong><?php echo __('Description')?> : </strong></label>
                                <?php echo form_textarea(array(
                                  'name'=>'description',
                                  'class'=>'form-control',
                                  'rows'=>3,
                                  'value'=>@$this->session->userdata('stepone')['description']
                                ))?>
                              </div>

                              <div class="form-group">
                                <label><strong><?php echo __('Status','default')?> : </strong></label>
                                <?php echo form_dropdown('active',array(
                                  '1'=>__('Active','default'),
                                  '0'=>__('Unactive','default')
                                ),@$this->session->userdata('stepone')['active'],'class="form-control"')?>
                              </div>  
                          </div>
                          <div class="col-md-7">
                              <div class="form-group">
                                <label><strong><?php echo __('Start Datetime')?> : </strong></label>
                                <?php echo form_input(array(
                                  'name'=>'startend_datetime',
                                  'class'=>'form-control',
                                  'value'=>@$this->session->userdata('stepone')['startend_datetime']
                                ))?>
                              </div>

                              <!-- <div class="form-group">
                                <label><strong><?php echo __('End Datetime')?> : </strong></label>
                                <?php echo form_input(array(
                                  'name'=>'end_datetime',
                                  'class'=>'form-control',
                                  'value'=>@$flashsaleset->end_datetime
                                ))?>
                              </div> -->
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-12">
                            <?php echo form_button(array(
                              'type'=>'submit',
                              'class'=>'btn btn-success pull-right',
                              'content'=>__('Continue','default')
                            ))?>
                          </div>
                        </div>

                    <?php echo form_close()?>

                  </div>
                </div>
              </div>
            </div>