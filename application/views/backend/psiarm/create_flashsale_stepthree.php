          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Create').__('Flash Sale')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>

                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/flashsale')?>"><?php echo __('FlashSale List')?></a></li>
                    
                    <li class="active"><?php echo __('Create').__('FlashSale')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">
                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo __('Create').__('FlashSale')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="row bs-wizard" style="border-bottom:0;">
                
                    <div class="col-lg-4 bs-wizard-step complete">
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 1','backend/psiarm/create_flashsale')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="<?php echo base_url('backend/'.$this->controller.'/createFlashSale')?>" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Information Form','backend/psiarm/create_flashsale')?></div>
                    </div>
                    
                    <div class="col-lg-4 bs-wizard-step complete"><!-- complete -->
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 2','backend/psiarm/create_flashsale')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="<?php echo base_url('backend/'.$this->controller.'/createFlashSale_steptwo')?>" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Choose Product Item(s)','backend/psiarm/create_flashsale')?></div>
                    </div>
                    
                    <div class="col-lg-4 bs-wizard-step active"><!-- complete -->
                      <div class="text-center bs-wizard-stepnum"><?php echo __('Step 3','backend/psiarm/create_flashsale')?></div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"><?php echo __('Confirm PR','backend/psiarm/create_flashsale')?></div>
                    </div>
                
                  </div>

                  <!-- <div class="row">
                    <div class="col-lg-12">
                      <a href="#" class="btn btn-success pull-right"><?php echo __('Add More Product')?></a>
                    </div>
                  </div> -->
                  <div class="x_content">
                    <?php echo form_open('',array('name'=>'create-flashsalestepthree-frm'))?>
                      <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                              <div class="panel-heading text-center"><?php echo __('General Information')?></div>
                              <div class="panel-body">
                                <p><strong><?php echo __('Name')?> : </strong><?php echo $stepone_data['name']?></p>
                                <p><strong><?php echo __('Description')?> : </strong><?php echo $stepone_data['description']?></p>
                                <p><strong><?php echo __('Duration')?> : </strong><?php echo $stepone_data['startend_datetime']?></p>
                              </div>
                              
                            </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                              <div class="panel-heading text-center"><?php echo __('Product Information')?></div>
                              <div class="panel-body">
                                  <table class="table">
                                      <thead>
                                        <tr>
                                          <th>#</th>
                                          <th><?php echo __('Product Code')?></th>
                                          <th><?php echo __('Product Name')?></th>
                                          <th><?php echo __('Product Description')?></th>
                                          <th><?php echo __('Quantity')?></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <?php foreach($steptwo_data as $key => $row){?>
                                            <tr>
                                              <td><?php echo ++$key;?></td>
                                              <td><?php echo $row['MPCODE']?></td>
                                              <td><?php echo $row['MPNAME']?></td>
                                              <td><?php echo $row['MPDESCRIPTION']?></td>
                                              <td><?php echo $row['quantity']?></td>
                                            </tr>
                                          <?php }?>
                                      </tbody>
                                  </table>
                              </div>
                            </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12">
                          <?php echo form_button(array(
                            'type'=>'button',
                            'class'=>'btn btn-success pull-right',
                            'content'=>__('Confirm FlashSale'),
                            'onclick'=>"confirmCreateFlashSale(this)"
                          ))?>
                        </div>
                      </div>
                    <?php echo form_close()?>
                  </div>
                </div>
              </div>
            </div>