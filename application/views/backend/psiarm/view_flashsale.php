          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('FlashSale Detail')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    <li>
                        <a href="<?php echo base_url('backend/'.$this->controller.'/flashsale')?>">
                          <?php echo __('FlashSale List')?>
                        </a>
                    </li>
                    
                    <li class="active"><?php echo __('FlashSale Detail')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                
                <div class="col-md-12">
                  <div class="row">
                  
                </div>
                </div>
                <div class="clearfix"></div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo __('FlashSale Detail')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo message_warning($this)?>
                        </div>
                        <div class="col-md-6">
                          <div class="panel panel-info">
                            <div class="panel-heading">
                              <h5><?php echo __('General Information')?></h5>
                            </div>
                            <div class="panel-body">
                              <p><strong><?php echo __('Name')?> : </strong> <?php echo $flashsaleset->name;?></p>

                              <p><strong><?php echo __('Description')?> : </strong> <?php echo $flashsaleset->description?></p>

                              <p><strong><?php echo __('Duration')?> : </strong> 
                                <?php echo form_input(array(
                                  'type'=>'hidden',
                                  'name'=>'hide_startdate',
                                  'value'=>date('d-m-Y H:i',strtotime($flashsaleset->start_datetime))
                                ))?>

                                <?php echo form_input(array(
                                  'type'=>'hidden',
                                  'name'=>'hide_enddate',
                                  'value'=>date('d-m-Y H:i',strtotime($flashsaleset->end_datetime))
                                ))?>

                                <?php echo form_input(array(
                                  'type'=>'hidden',
                                  'name'=>'hide_flashsaleset_id',
                                  'value'=>$flashsaleset->id
                                ))?>

                                <div class="row">

                                  <div class="col-lg-12">
                                      <?php if($this->session->userdata('member_data')['data']['member_access_types_id'] == '1' || $this->session->userdata('member_data')['data']['member_access_types_id'] == '2'){

                                        echo form_input(array(
                                          'name'=>'duration',
                                          'class'=>'form-control',
                                          'value'=>date('d-m-Y H:i',strtotime($flashsaleset->start_datetime)).' - '.date('d-m-Y H:i',strtotime($flashsaleset->end_datetime))
                                        ));
                                      }else{
                                        echo date('d/m/Y H:i',strtotime($flashsaleset->start_datetime)).' - '.date('d/m/Y H:i',strtotime($flashsaleset->end_datetime));
                                      }
                                      ?>
                                  </div>
                                </div>




                                </p>

                              <p><strong><?php echo __('Created')?> : </strong> <?php echo date('d/m/Y H:i',strtotime($flashsaleset->created))?></p>

                              <p><strong><?php echo __('Updated')?> : </strong> <?php echo date('d/m/Y H:i',strtotime($flashsaleset->updated))?></p>

                              <p><strong><?php echo __('Remaining')?> : </strong> <span id="getting-started"></span></p>



                              <?php if($this->session->userdata('member_data')['data']['member_access_types_id'] == '1' || $this->session->userdata('member_data')['data']['member_access_types_id'] == '2'){?>
                              <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <?php echo form_dropdown('active',array(
                                          '0'=>__('Unactive','default'),
                                          '1'=>__('Active','default')
                                        ),@$flashsaleset->active,'class="form-control" onchange="changeFlashSaleStatus(this)" data-flashsalesetid="'.$flashsaleset->id.'"')?>
                                    </div>
                                </div>
                              </div>
                              <?php }?>
                             
                              
                              <?php echo form_input(array(
                                'type'=>'hidden',
                                'name'=>'hide_end_datetime',
                                'value'=>$flashsaleset->end_datetime
                              ))?>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="panel panel-info">
                            <div class="panel-heading">
                              <h5><?php echo __('Export order to excel')?></h5>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                  <label><strong><?php echo __('Select Branch')?> : </strong></label>
                                  <?php echo form_dropdown('select_branch',$select_branch,'','class="form-control"')?>

                                </div>

                                <div class="form-group">
                                  <?php echo form_button(array(
                                    'type'=>'button',
                                    'class'=>'btn btn-success btn-block',
                                    'content'=>'<i class="fa fa-file-excel-o"></i> '.__('Export Order'),
                                    'onclick'=>'exportOrderToExcel(this)'
                                  ))?>
                                </div>
                            </div>
                          </div>

                        </div>

                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <div class="panel panel-info">
                            <div class="panel-heading"><h5><?php echo __('Product Information')?></h5></div>
                            <div class="panel-body">
                              <table class="table">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th><?php echo __('Product Code')?></th>
                                      <th><?php echo __('Product Name')?></th>
                                      <th><?php echo __('Product Description')?></th>
                                      <th><?php echo __('Product Quantity')?></th>
                                      <th><?php echo __('Product Remain')?></th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php 
                                      $edit_status = (strtotime(date('Y-m-d H:i:s')) <= strtotime($flashsaleset->end_datetime))?true:false;
                                    ?>
                                    <?php foreach($flashsaleset->flashsalesetproduct->get() as $key => $row){?>
                                        <tr>
                                          <td><?php echo ++$key?></td>
                                          <td><?php echo $row->MPCODE?></td>
                                          <td><?php echo $row->name?></td>
                                          <td><?php echo $row->description?></td>
                                          <td><?php echo $row->total_available?></td>
                                          <td><?php echo flashsaleCalculateProductRemainByProductId($row->id)?></td>
                                          <td>
                                            <?php if($edit_status){?>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#editSetProduct" data-id="<?php echo $row->id?>" data-name="<?php echo $row->name?>" data-description="<?php echo $row->description?>" data-imagelandscape="<?php echo $row->image_landscape?>" data-imageportait="<?php echo $row->image_portait?>" onclick="clickEditSetProduct(this)" data-totalavailable="<?php echo $row->total_available?>"><i class="fa fa-pencil"></i></a>
                                            <?php }?>
                                          </td>

                                        </tr>

                                    <?php }?>
                                  </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>

                  </div>
                </div>
              </div>
            </div>

<!-- Modal -->
<div class="modal fade" id="editSetProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-large" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <?php echo form_open_multipart('',array('name'=>'edit-setproduct-form'))?>
      <div class="modal-body">
         
            <?php echo form_input(array(
              'type'=>'hidden',
              'name'=>'hide_setproduct_id',
              'value'=>''
            ))?>
            <div class="row">
              <div class="col-lg-12">

                <div class="form-group">
                  <label><strong><?php echo __('Name')?> : </strong></label>
                  <?php echo form_input(array(
                    'name'=>'name',
                    'class'=>'form-control'
                  ))?>
                </div>

                <div class="form-group">
                  <label><strong><?php echo __('Description')?> : </strong></label>
                  <?php echo form_textarea(array(
                    'name'=>'description',
                    'class'=>'form-control',
                    'rows'=>4
                  ))?>
                </div>

                <div class="form-group">
                  <label><strong><?php echo __('Product Quantity')?> : </strong></label>
                  <?php echo form_input(array(
                    'name'=>'total_available',
                    'class'=>'form-control',
                    'onkeypress'=>'validate(event)'
                  ))?>
                </div>

                <div class="form-group">
                                        <label>Landscape Image : gif|jpg|png (400x200 pixels)</label>
                                        <div class="clearfix"></div>
                                        
                                        <div class="view view-first" style="margin:5px 0px;width: 100%;height: 100%;" id="view_image_landscape">
                                                    <img src="" class="image_landscape img-responsive">
                                                     <!-- <div class="mask">
                                                      <a href="javascript:void(0);" class="pop btn btn-default"><i class="fa fa-eye"></i></a>
                                                         
                                                     </div> -->
                                        </div>
                                   
                                        
                                        
                                    
                                    <div class="clearfix"></div>
                                    
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('landscape_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                                         </div>
                        </div>


                        <div class="form-group">
                                        <label>Portait Image : gif|jpg|png (400x600 pixels)</label>
                                        <div class="clearfix"></div>
                                        
                                        <div class="view view-first" style="margin:5px 0px;width: 100%;height: 100%;" id="view_image_portait">
                                                    <img src="" class="image_portait img-responsive">
                                                     <!-- <div class="mask">
                                                      <a href="javascript:void(0);" class="pop btn btn-default"><i class="fa fa-eye"></i></a>
                                                         
                                                     </div> -->
                                        </div>
                                   
                                        
                                        
                                    
                                    <div class="clearfix"></div>
                                    
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('portait_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                                         </div>
                        </div>
              </div>
              
                        
              
            </div>

          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>'Save Change'
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>