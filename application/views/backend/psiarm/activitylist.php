          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('ActivityList')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    
                    <li class="active"><?php echo __('ActivityList')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                
                <div class="col-md-12">
                  <div class="row">
                  <a href="<?php echo base_url('backend/'.$this->controller.'/createActivity')?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> <?php echo __('Create Activity')?></a>
                </div>
                </div>
                <div class="clearfix"></div>

                <div class="x_panel">
                  <div class="x_title">
                    
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->
                    <?php echo form_open('',array('name'=>'coupon-frm'))?>
                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th>#</th>
                          
                          <th><?php echo __('Activity Name')?></th>
                          <th><?php echo __('Cover')?></th>
                          <th><?php echo __('StartDate')?></th>
                          <th><?php echo __('EndDate')?></th>
                          <th><?php echo __('Created')?></th>
                          <th><?php echo __('Updated')?></th>
                          <th><?php echo __('Status')?></th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php foreach($activitylist as $key => $row){?>
                            <tr>
                              <td></td>
                              <td><?php echo $row->activity_name;?></td>
                              <td>
                                <img src="<?php echo base_url('uploaded/activity/'.$row->activity_id.'/'.$row->Cover)?>" style="width:50px;height: 50px;">
                              </td>
                              <td><?php echo date('Y-m-d',strtotime($row->StartDate));?></td>
                              <td><?php echo date('Y-m-d',strtotime($row->EndDate));?></td>
                              <td><?php echo date('Y-m-d H:i:s',strtotime($row->activity_created))?></td>
                              <td><?php echo ($row->activity_updated)?date('Y-m-d H:i:s',strtotime($row->activity_updated)):$row->activity_created;?></td>
                              <td>
                                <?php if($row->Active){?>
                                  <label class="label label-success"><?php echo __('Active','backend/default')?></label>
                                <?php }else{?>
                                  <label class="label label-danger"><?php echo __('Unactive','backend/default')?></label>
                                <?php }?>
                              </td> 

                              <td>
                                  <?php if(@$this->event_ability->edit || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                  <a href="<?php echo base_url('backend/'.$this->controller.'/editActivity/'.$row->activity_id)?>" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> <?php echo __('Edit','backend/default')?></a>
                                  <?php }?>

                                  <?php if(@$this->event_ability->delete || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                    <a href="javascript:void(0);" onclick="if(confirm('ต้องการลบใช่หรือไม่') == true){window.location.href='<?php echo base_url('backend/'.$this->controller.'/deleteActivity/'.$row->activity_id);?>'}" class="btn btn-danger btn-xs">
                                      <i class="fa fa-trash"></i> <?php echo __('Delete','backend/default');?>
                                    </a>
                              <?php }?>
                              </td>

                            </tr>

                          <?php }?>
                      </tbody>

                    </table>


                      <?php echo form_close();?>
                    

                  </div>
                </div>

              </div>

            </div>