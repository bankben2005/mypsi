          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Create').__('Notification')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>

                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/notification')?>"><?php echo __('Notification List')?></a></li>
                    
                    <li class="active"><?php echo __('Create').__('Notification')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">
                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo __('Create').__('Notification Form')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <?php echo form_open('',array('name'=>'create-notification-form'))?>
                        <div class="row">
                          <div class="col-lg-4">

                              <div class="form-group">
                                <label><strong><?php echo __('Send Type')?> : </strong></label>
                                <br>

                                <label>
                                <?php echo form_radio(array(
                                  'name'=>'send_type',
                                  'value'=>'device_token',
                                  'checked'=>TRUE
                                ))?> <span><?php echo __('Send By Device Token')?></span> </label>

                                <label>
                                <?php echo form_radio(array(
                                  'name'=>'send_type',
                                  'value'=>'all',
                                ))?> <span><?php echo __('Send All')?></span> </label>

                              </div>
                              <div class="form-group">
                                <label><strong><?php echo __('Device Token')?> : </strong></label>
                                <?php echo form_input(array(
                                  'name'=>'device_token',
                                  'class'=>'form-control'
                                ))?>
                              </div>
                              <div class="form-group">
                                <label><strong><?php echo __('Notification Message')?> : </strong></label>
                                <?php echo form_textarea(array(
                                  'name'=>'text_message',
                                  'class'=>'form-control',
                                  'rows'=>4,
                                  'required'=>'required'
                                ))?>
                              </div>

                              <div class="form-group">
                                <?php echo form_button(array(
                                  'type'=>'submit',
                                  'class'=>'btn btn-success pull-right',
                                  'content'=>__('Submit','default')
                                ))?>
                              </div>
                          </div>
                          <div class="col-lg-4">

                          </div>

                        </div>

                      <?php echo form_close()?>
                  </div>
                </div>

            </div>
          </div>