          <div class="page-title">
              <div class="title_left">
                <h3><?php echo ($activity->getId())?__('Edit'):__('Create').__('Activity')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>

                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/activitylist')?>"><?php echo __('ActivityList')?></a></li>
                    
                    <li class="active"><?php echo ($activity->getId())?__('Edit'):__('Create').__('Activity')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">
                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php ($activity->getId())?__('Edit'):__('Create').__('Activity')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php echo form_open_multipart('',array('id'=>'createactivity-frm'))?>

                      <div class="col-md-5">
                        <div class="row">
                          <div class="form-group">
                            <label><?php echo __('Activity Name')?> : </label>
                            <?php echo form_input(array('name'=>'Name','value'=>@$activity->Name,'class'=>'form-control'))?>
                          </div>
                          <div class="form-group">
                            <label><?php echo __('Duration')?> : </label>
                            <?php echo form_input(array('name'=>'Duration','value'=>@$duration,'class'=>'form-control'))?>
                          </div>
                          <div class="form-group">
                            <label><?php echo __('Url')?> : </label>
                            <?php echo form_input(array('name'=>'UriString','value'=>@$activity->UriString,'class'=>'form-control'))?>
                          </div>
                          <div class="form-group">
                            <label><?php echo __('ActivityType')?></label>
                            <?php echo form_dropdown('ArmActivityType_id',$activity_type,@$activity->ArmActivityType_id,'class="form-control"')?>
                          </div>
                          <div class="form-group">
                            <label><?php echo __('Status')?> : </label>
                            <?php echo form_dropdown('Active',array('1'=>__('Active'),'0'=>__('UnActive')),@$activity->Active,'class="form-control"')?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7">
                              <div class="form-group">
                                        <label>รูปภาพกิจกรรม : gif|jpg|png</label>
                                        <div class="clearfix"></div>
                                        <?php if($activity->Cover && file_exists('uploaded/activity/'.$activity->id.'/'.$activity->Cover)){?>
                                        
                                        <div class="view view-first" style="margin:5px 0px;">
                                                    <img src="<?php echo base_url().'uploaded/activity/'.$activity->id.'/'.$activity->Cover;?>" style="width:150px;height: 100px;">
                                                     <div class="mask">
                                                      <a href="javascript:void(0);" class="pop btn btn-default"><i class="fa fa-eye"></i></a>
                                                         <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('backend/').$this->controller.'/delete_image/'.$activity->id.'/image';?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                     </div>
                                        </div>
                                   
                                        
                                        <?php }?>
                                    
                                    <div class="clearfix"></div>
                                    
                                        <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('activity_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                                         </div>
                                    </div>

                      </div>
                      <div class="col-md-12">
                        <?php echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
                      </div>
                      <?php echo form_close();?>
                  </div>
                </div>

              </div>
            </div>


<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('Image Preview')?></h4>
      </div>
      <div class="modal-body">
        <img src="" id="imagepreview" class="img-responsive" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>