        <div class="row">
        <div class="col-md-12">
                  <ul class="breadcrumb">
                      <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                      <li><a href="<?php echo base_url('backend/admin_staff/seminar/clothdeallist/'.$seminarno)?>"><?php echo __('ClothDeal List')?></a></li>
                      <li class="active"><?php echo __('Add Seminar ClothDeal')?></li>
                  </ul>
                <div class="x_panel">
                  <div class="x_title">
                    <h2 style="overflow: inherit;"><i class="fa fa-registered"></i> <?php echo __('Register ClothDeal')?> <small><?php echo @$seminar_data->SeminarDesc?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="display: none;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#scanqr_content" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-qrcode"></i> <?php echo __('Scan QR Code')?> / <?php echo __('Enter Detail')?></a>
                        </li>
                        <!-- <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-pencil"></i> <?php echo __('Enter Detail')?></a>
                        </li> -->
                        <!-- <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="true">Profile</a>
                        </li> -->
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="scanqr_content" aria-labelledby="home-tab">
                          <div class="col-md-12">
                            <div class="row">
                                
                                <?php echo form_open('',array('name'=>'scanqr-form'));?>
                                <?php echo form_input(array('type'=>'hidden','name'=>'quantity','value'=>''));?>
                                <div class="col-md-3">
                                  <img src="<?php echo base_url('uploaded/seminar/ticket/user-icon.png')?>" id="AgentImage" style="width:100%;" class="img-thumbnail" />
                                </div>

                                <div class="col-md-9">
                                  <div id="seminarinfo">
                                    <!-- <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#chooseSizeModal"><i class="fa fa-user-plus"></i> เลือกไซด์และยืนยันการรับเสื้อ</button> -->
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="panel panel-default">
                                <div class="panel-heading"><h4>ข้อมูลส่วนตัว</h4></div>
                                <div class="panel-body">
                                  
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('CardNo')?> : </label>
                                    
                                    <div class="col-md-5">
                                      <?php echo form_input(array(
                                        'name'=>'CardNo',
                                        'class'=>'form-control',
                                        'placeholder'=>__('Input cardno for register/clothdeal')
                                      ))?>
                                    </div>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'AgentCode','class'=>'form-control','placeholder'=>__('AgentCode'),'readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Name&Lastname')?> : </label>
                                    <div class="col-md-5">
                                    <?php echo form_input(array('name'=>'AgentName','class'=>'form-control','readonly'=>'readonly'));?>
                                    </div>
                                    <div class="col-md-5">
                                       <?php echo form_input(array('name'=>'AgentSurName','class'=>'form-control','placeholder'=>__('Lastname'),'readonly'=>'readonly'));?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Email')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'emailaddress','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Telephone')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'Telephone','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Address')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_textarea(array('name'=>'R_Addr1','class'=>'form-control','rows'=>2,'readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Amphur')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'R_District','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Province')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'R_Province','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Branch')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'Branch','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>


                                    </div>
                                  </div>
                                </div>
                                <?php echo form_close();?>

                              </div>
                          </div>
                        </div>

                        <!-- Manual Tab Panel-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                          <!-- <button type="button" class="btn btn-primary pull-right btn-add-rows"><i class="fa fa-plus"></i> <?php echo __('Add rows');?></button> -->
                          <div class="clearfix"></div>
                          <?php echo form_open('',array('name'=>'manual-form'));?>

                            <div class="rowAgentClothDeal">
                            <?php echo form_input(array('name'=>'manualSelectSize','type'=>'hidden'));?>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?php echo __('CardNo')?> : </label>
                                    <?php echo form_input(array('name'=>'CardNoManual','class'=>'form-control','placeholder'=>__('Fill agent card number and choose size'),'onkeyup'=>'cardNoManualKeyup(this)'));?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?php echo __('Name&Lastname')?> : </label>
                                    <?php echo form_input(array('name'=>'AgentNameAndSurName','class'=>'form-control','readonly'=>'readonly'))?>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label><?php echo __('Total Tickets')?> : </label>
                                    <?php echo form_input(array('name'=>'TotalTickets','class'=>'form-control','readonly'=>'readonly'))?>

                                </div>
                            </div>
                            <div class="col-md-1" style="display: none;">
                                <div class="form-group">
                                    <label><?php echo __('Choose Size')?> : </label>
                                    <p>
                                    <button type="button" class="btn btn-default btn-select-size" disabled="disabled"><i class="fa fa-pencil"></i></button>
                                    </p>
                                </div>
                                
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <p>
                                    <button type="button" class="btn btn-danger btn-delete-row" onclick="$(this).closest('.rowAgentClothDeal').remove();"><i class="fa fa-minus"></i></button>
                                  </p>
                                </div>
                            </div>

                            

                            <div class="clearfix"></div>
                            </div>

                          <?php echo form_close();?>
                        </div>
                        <!-- Eof Manual Tab Panel-->


                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>

<!-- Modal -->
<div class="modal fade" id="chooseSizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">เลือกไซด์และยืนยันการรับเสื้อ</h4>
      </div>
      <div class="modal-body">
          <div id="modal-alert">

          </div>
          <?php echo form_open('',array('name'=>'chooseClothSize'));?>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>ไซต์เสื้อ</th>
                  <th>จำนวน / ตัว</th>
                </tr>

              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td>2S</td>
                  <td>
                    
                            <button class="minus-btn" type="button" name="button" data-product="size-2s">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="2s" value="0" class="quantity">
                            <button class="plus-btn" type="button" name="button" data-product="size-2s">
                              <i class="fa fa-plus"></i>
                            </button>
                            
                          
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>S</td>
                  <td>
                    
                            <button class="minus-btn" type="button" name="button" data-product="size-s">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="s" value="0" class="quantity">
                            <button class="plus-btn" type="button" name="button" data-product="size-s">
                              <i class="fa fa-plus"></i>
                            </button>
                            
                          
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>M</td>
                  <td>
                          <button class="minus-btn" type="button" name="button" data-product="size-m">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="m" value="0" class="quantity">
                            <button class="plus-btn" type="button" name="button" data-product="size-m">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>L</td>
                  <td>
                            <button class="minus-btn" type="button" name="button" data-product="size-l">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="l" value="0" class="quantity">
                            <button class="plus-btn" type="button" name="button" data-product="size-l">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>XL</td>
                  <td>
                            <button class="minus-btn" type="button" name="button" data-product="size-xl">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="xl" value="0" class="quantity">
                            <button class="plus-btn" type="button" name="button" data-product="size-xl">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>XXL</td>
                  <td>
                            <button class="minus-btn" type="button" name="button" data-product="size-xxl">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="xxl" value="0" class="quantity">
                            <button class="plus-btn" type="button" name="button" data-product="size-xxl">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>XXXL</td>
                  <td>
                            <button class="minus-btn" type="button" name="button" data-product="size-xxxl">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="xxxl" value="0" class="quantity">
                            <button class="plus-btn" type="button" name="button" data-product="size-xxxl">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

              </tbody>

            </table>


          <?php echo form_close();?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        <button type="button" class="btn btn-primary btn-submit-clothdeal">ยืนยันและบันทึกการรับเสื้อ</button>
      </div>
    </div>
  </div>
</div>




<!-- Manual Choose Size -->
<!-- Modal -->
<div class="modal fade" id="manualChooseSizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="myModalLabel">เลือกไซด์และยืนยันการรับเสื้อ</h4>
      </div>
      <div class="modal-body">
          <div id="modal-manual-alert">

          </div>
          <?php echo form_open('',array('name'=>'manualChooseClothSize'));?>
          <?php echo form_input(array('name'=>'data_card_no','type'=>'hidden'))?>
          <?php echo form_input(array('name'=>'manual_quantity','type'=>'hidden'));?>
          <?php echo form_input(array('name'=>'manual_agentcode','type'=>'hidden'));?>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>ไซต์เสื้อ</th>
                  <th>จำนวน / ตัว</th>
                </tr>

              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td>S</td>
                  <td>
                    
                            <button class="manual-minus-btn" type="button" name="button" data-product="size-x">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="s" value="0" class="m_quantity">
                            <button class="manual-plus-btn" type="button" name="button" data-product="size-x">
                              <i class="fa fa-plus"></i>
                            </button>
                            
                          
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>M</td>
                  <td>
                          <button class="manual-minus-btn" type="button" name="button" data-product="size-m">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="m" value="0" class="m_quantity">
                            <button class="manual-plus-btn" type="button" name="button" data-product="size-m">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>L</td>
                  <td>
                            <button class="manual-minus-btn" type="button" name="button" data-product="size-l">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="l" value="0" class="m_quantity">
                            <button class="manual-plus-btn" type="button" name="button" data-product="size-l">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>XL</td>
                  <td>
                            <button class="manual-minus-btn" type="button" name="button" data-product="size-xl">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="xl" value="0" class="m_quantity">
                            <button class="manual-plus-btn" type="button" name="button" data-product="size-xl">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

                <tr>
                  <td></td>
                  <td>XXL</td>
                  <td>
                            <button class="manaul-minus-btn" type="button" name="button" data-product="size-xxl">
                              <i class="fa fa-minus"></i>
                            </button>
                            
                            <input type="text" name="xxl" value="0" class="m_quantity">
                            <button class="manual-plus-btn" type="button" name="button" data-product="size-xxl">
                              <i class="fa fa-plus"></i>
                            </button>
                  </td>

                </tr>

              </tbody>

            </table>


          <?php echo form_close();?>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button> -->
        <button type="button" class="btn btn-primary btn-submitmanual-clothdeal">ยืนยันและบันทึกการรับเสื้อ</button>
      </div>
    </div>
  </div>
</div> 


<!-- Modal -->
<div class="modal fade" id="modalSetSeatAndRoomNo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ระบุเลขที่นั่ง / เลขห้อง</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('',['name'=>'seat-room-form'])?>

          <div class="row">
            <div class="col-lg-4">
              <div class="form-group">
                <label><strong>หมายเลขที่นั่ง : </strong></label>
                <?php echo form_input([
                  'name'=>'SeatNo',
                  'class'=>'form-control'
                ])?>
              </div>

            </div>
            <div class="col-lg-4">

            </div>
          </div>
          
        <?php echo form_close()?>

      </div>
    </div>
  </div>
</div>