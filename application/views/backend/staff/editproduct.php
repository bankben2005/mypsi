            <input type="hidden" name="method" value="<?php echo $this->method?>">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Edit Seminar Product')?></h3>
              </div>

              
            </div>
            <div class="clearfix"></div>


            <div class="row">
              <div class="col-md-12">
                 
                  <ul class="breadcrumb">
                      <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                      <li><a href="<?php echo base_url('backend/'.$this->controller.'/seminarorder')?>"><?php echo __('SeminarOrder','backend/default')?></a></li>
                      <li class="active"><?php echo __('Edit Seminar Order Product')?></li>
                  </ul>
                 
              </div>

              <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->

                    <!-- start project list -->
                    <?php echo form_open('',array('id'=>'form-product'))?>
                    
                    <table  class="table table-striped projects">

                      <thead>
                        <tr>
                          <th>MPCODE</th>
                          <th style="width: 20%"><?php echo __('Name')?></th>
                          <th></th>
                          <th><?php echo __('Amount')?></th>
                          <th><?php echo __('Active')?></th>
                          <!-- <th style="width: 20%"></th> -->
                        </tr>
                      </thead>
                      <tbody>

                          <?php foreach($product as $key => $row){
                            
                            $checked_status = '';
                            $product_amount = 0;
                            $mpcode = $row->MPCODE;

                           
                            if(in_array($mpcode, $productavailable)){
                              $checked_status = 'checked';
                              $product_amount = $productamount[$row->MPCODE];
                              // echo $product_amount."<br>";exit;
                            }
                            // echo $checked_status."<br>";

                            ?>
                            <tr>
                                <td><?php echo $row->MPCODE?></td>
                                <td><?php echo $row->MPNAME?></td>
                                <td><?php echo $row->MPDESCRIPTION?></td>
                                <td><?php echo form_input(array('type'=>'number','name'=>'amount_'.$row->MPCODE,'value'=>$product_amount,'class'=>'form-control','min'=>'0','onkeypress'=>'validate(event)'))?></td>
                                <td><?php echo form_checkbox(array('name'=>'active[]','value'=>$row->MPCODE,'checked'=>$checked_status))?> <?php echo form_input(array('type'=>'hidden','name'=>'seminarno','value'=>@$seminarno))?></td>
                            </tr>


                          <?php }?>

                      </tbody>
                      
                      </table>
                      <div class="col-md-12">
                        <?php echo form_button('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right submit-btn"')?>
                      </div>
                      <?php echo form_close();?>


            </div>

          </div>

          