          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('SeminarOrderProductList')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    
                    <li class="active"><?php echo __('Order Product List')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                <a href="<?php echo base_url('backend/'.$this->controller.'/exportorderproduct')?>" type="button" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> <?php echo __('Export to excel file')?></a>
                        <div class="clearfix"></div>

                

                <div class="x_panel">
                  <div class="x_title">
                    
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->
                    <?php echo form_open('',array('name'=>'orderlist-frm'))?>
                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th><?php echo form_checkbox(array('name'=>'checkall','value'=>'checkall','onclick'=>'checkUncheck(this)'))?></th>
                          <th><?php echo __('SeminarName')?></th>
                          <th><?php echo __('MPNAME')?></th>
                          <th><?php echo __('AgentCode')?></th>
                          <!-- <th><?php echo __('CardNo')?></th> -->
                          <th><?php echo __('CustomerName')?></th>
                          <th><?php echo __('CustomerTelephone')?></th>
                          <!-- <th><?php echo __('Branch')?></th> -->
                          <th><?php echo __('Amount')?></th>
                          <th><?php echo __('Created');?></th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php 
                          if(!empty($orderproductlist)){
                          foreach($orderproductlist as $key => $row){?>
                            <tr>
                                <td><?php echo form_checkbox(array('name'=>'record_action[]','value'=>$row->id,'class'=>'record_action'))?></td>
                                <td><?php echo $row->SeminarDesc;?></td>
                                <td><?php echo $row->MPNAME;?></td>
                                <td><?php echo $row->AgentCode;?></td>
                               <!-- <td><?php echo $row->CardNo;?></td> -->
                                <td><?php echo $row->CustomerName;?></td>
                                <td><?php echo $row->CustomerPhone;?></td>
                                <!-- <td><?php echo $row->Branch;?></td> -->
                                <td><?php echo $row->Amount;?></td>
                                <td>
                                  <?php echo $row->Created->format('d/m/Y H:i:s');?>
                                </td>
                                <td></td>

                            </tr>

                          <?php } }?>
                      </tbody>

                    </table>

                    
                        <div class="row">
                        
                        <div class="col-md-1">
                          จัดการ : 
                        </div>
                        <div class="col-md-2">
                            <?php echo form_dropdown('choose_action',array(''=>'--','cancel'=>__('Cancel')),'','class="form-control"')?>
                        </div>
                        <div class="col-md-1">
                          <?php echo form_button(array('type'=>'submit','class'=>'btn btn-primary','content'=>__('OK'),'id'=>'btn_action'))?>
                        </div>
                        
                      </div>

                      <?php echo form_close();?>
                    

                  </div>
                </div>

              </div>

            </div>