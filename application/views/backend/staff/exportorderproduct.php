          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('ExportOrderProduct')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/orderproductlist')?>"><?php echo __('Order Product List')?></a></li>
                    <li class="active"><?php echo __('Export Order Product')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
               

                <div class="x_panel">
                    <div class="x_title">
                      <h2><?php echo __('Export to excel file')?></h2>
                      <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <?php echo form_open('',array('name'=>'filter_form','class'=>'form-inline'));?>
                        <div class="form-group">
                          <?php 

                            $data_select_year[''] = __('Select Year'); 
                            $thisyear = (int)date('Y');


                            for($x=$thisyear;$x >= (int)($thisyear-3);$x--){
                                $data_select_year[$x] = $x;
                            }
                          ?>

                          <?php echo form_dropdown('select_year',$data_select_year,'','class="form-control"');?>

                        </div>

                        <div class="form-group">
                            <?php echo form_dropdown('select_group',array(''=>__('Select Seminar Group')),'','class="form-control" disabled="disabled"')?>
                        </div>

                        <div class="form-group">
                            <?php echo form_dropdown('select_branch',$branchlist,'','class="form-control" disabled="disabled"')?>
                        </div>

                        <div class="form-group">
                            <?php echo form_button(array('type'=>'button','class'=>'btn btn-success btn-xs','content'=>'<i class="fa fa-file-excel-o"></i>'.' '.__('Export to excel file'),'disabled'=>'disabled','id'=>'search_btn'));?>
                        </div>

                      <?php echo form_close();?>
                  </div>

                </div>
              </div>

            </div>

