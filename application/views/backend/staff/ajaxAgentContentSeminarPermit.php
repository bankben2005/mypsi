<img src="" class="img-responsive" id="AgentImage">

<br>
<p><strong><?php echo __('Card No')?> : </strong><?php echo $agent_data->CardNo?></p>

<p><strong><?php echo __('Agent Code')?> : </strong><?php echo $agent_data->AgentCode?></p>

<p><strong><?php echo __('Firstname&Lastname')?> : </strong><?php echo $agent_data->AgentName.' '.$agent_data->AgentSurName?></p>

<p><strong><?php echo __('Email')?> : </strong> <?php echo ($agent_data->emailaddress)?$agent_data->emailaddress:'-'?></p>

<p><strong><?php echo __('Telephone')?> : </strong> <?php echo ($agent_data->Telephone)?$agent_data->Telephone:'-'?></p>

<p><strong><?php echo __('Address')?> : </strong> <?php echo ($agent_data->R_Addr1)?$agent_data->R_Addr1:'-'?></p>

<p><strong><?php echo __('Amphur')?> : </strong><?php echo ($agent_data->R_District)?$agent_data->R_District:'-'?></p>

<p><strong><?php echo __('Province')?> : </strong><?php echo ($agent_data->R_Province)?$agent_data->R_Province:'-'?></p>

<p><strong><?php echo __('Branch')?> : </strong><?php echo ($agent_data->BranchName)?$agent_data->BranchName:'-'?></p>