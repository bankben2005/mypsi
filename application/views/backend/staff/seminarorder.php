<div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Staff')?> <small><?php echo __('SeminarOrder')?></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    <li><a href="#"><?php echo __('Seminar','backend/default')?></a></li>
                    <li class="active"><?php echo __('Seminar List')?></li>
                </ul>
                <!-- eof bread crumb-->


                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo __('Seminar list')?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="display: none;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->

                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="width: 1%">#</th>
                          <th style="width: 20%"><?php echo __('Name')?></th>
                          <th><?php echo __('SeminarDate')?></th>
                          <th><?php echo __('Product Available')?></th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php foreach($seminar as $key => $row){?>
                        <tr>
                          <td>#</td>
                          <td>
                            <?php echo $row->SeminarDesc;?>
                          </td>
                          <td>
                            <?php echo $row->SeminarDate->format('d-m-Y');?>
                          </td>
                          <td>

                          </td>
                          <td>
                            
                            <a href="<?php echo base_url('backend/admin_staff/seminar/edit/'.($row->SeminarNo))?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> <?php echo __('Edit','default_backend')?> </a>
                           
                          </td>
                        </tr>
                        <?php }?>
                        
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>