<?php 
//print_r($ticket_data);
if(is_array($ticket_data) && count($ticket_data) > 0){?>

	<table class="table">
		<thead>
			<tr>
				<th><?php echo __('Seminar')?></th>
				<th><?php echo __('Quantity')?></th>
				<th><?php echo __('Bought')?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($ticket_data as $key => $row){?>
				<tr>
					<td><?php echo $row->MPNAME?></td>
					<td><?php echo $row->POSIQUANTITY?></td>
					<td><?php echo date('d/m/Y',strtotime($row->POSTDATE))?></td>
				</tr>

			<?php }?>
		</tbody>
	</table>
	

<?php }else{?>
	<label class="label label-danger"><?php echo __('Not found ticket history')?></label>
<?php }?>