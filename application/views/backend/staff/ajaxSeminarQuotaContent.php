<?php //print_r($quota_histories);exit;?>

<?php if(count($quota_histories) > 0){?>

	<table class="table">
		<thead>
			<tr>
				<th><?php echo __('Seminar')?></th>
				<th><?php echo __('Quantity')?></th>
				<th><?php echo __('SeminarDate')?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($quota_histories as $key => $row){?>
				<tr>
					<td><?php echo $row->SeminarDesc?></td>
					<td><?php echo $row->Qty?></td>
					<td><?php echo date('d/m/Y',strtotime($row->SeminarDate->format('Y-m-d')))?></td>
				</tr>

			<?php }?>
		</tbody>
	</table>
	

<?php }else{?>
	<label class="label label-danger"><?php echo __('Not found quota history')?></label>
<?php }?>