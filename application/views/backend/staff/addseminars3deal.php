        <div class="row">
        <div class="col-md-12">
                  <ul class="breadcrumb">
                      <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                      <li><a href="<?php echo base_url('backend/admin_staff/cloths3list/'.$seminarno)?>"><?php echo __('S3Deal List')?></a></li>
                      <li class="active"><?php echo __('Add Seminar S3Deal')?></li>
                  </ul>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-registered"></i> <?php echo __('Register S3Deal')?> <small><?php echo @$seminar_data->SeminarDesc?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="display: none;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#scanqr_content" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-qrcode"></i> <?php echo __('Scan QR Code')?> / <?php echo __('Enter Detail')?></a>
                        </li>
                        <!-- <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-pencil"></i> <?php echo __('Enter Detail')?></a>
                        </li> -->
                        <!-- <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="true">Profile</a>
                        </li> -->
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="scanqr_content" aria-labelledby="home-tab">
                          <div class="col-md-12">
                            <?php echo form_open('',array('name'=>'scanqr-form'));?>
                            <div class="row">
                                
                                
                                <?php echo form_input(array('type'=>'hidden','name'=>'quantity','value'=>''));?>
                                <div class="col-md-3">
                                  <img src="<?php echo base_url('uploaded/seminar/ticket/user-icon.png')?>" id="AgentImage" style="width:100%;height: 238px;" class="img-thumbnail" />
                                </div>

                                <div class="col-md-9">
                                  <div id="seminarinfo">
                                    <!-- <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#chooseSizeModal"><i class="fa fa-user-plus"></i> เลือกไซด์และยืนยันการรับเสื้อ</button> -->
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="panel panel-default">
                                <div class="panel-heading"><h4>ข้อมูลส่วนตัว</h4></div>
                                <div class="panel-body">
                                  
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('CardNo')?> : </label>
                                    
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'CardNo','class'=>'form-control'))?>
                                    </div>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'AgentCode','class'=>'form-control','placeholder'=>__('AgentCode'),'readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Name&Lastname')?> : </label>
                                    <div class="col-md-5">
                                    <?php echo form_input(array('name'=>'AgentName','class'=>'form-control','readonly'=>'readonly'));?>
                                    </div>
                                    <div class="col-md-5">
                                       <?php echo form_input(array('name'=>'AgentSurName','class'=>'form-control','placeholder'=>__('Lastname'),'readonly'=>'readonly'));?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Email')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'emailaddress','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Telephone')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'Telephone','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Address')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_textarea(array('name'=>'R_Addr1','class'=>'form-control','rows'=>2,'readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Amphur')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'R_District','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Province')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'R_Province','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-md-2 text-right"><?php echo __('Branch')?> : </label>
                                    <div class="col-md-5">
                                      <?php echo form_input(array('name'=>'Branch','class'=>'form-control','readonly'=>'readonly'))?>
                                    </div>
                                    <div class="clearfix"></div>
                                  </div>


                                    </div>
                                  </div>
                                </div>
                                

                              </div>

                              <div class="row">
                                <div class="col-lg-12">
                                  <?php echo form_button(array(
                                    'type'=>'button',
                                    'class'=>'btn btn-success pull-right',
                                    'content'=>__('Submit','backend/default'),
                                    'style'=>'display:none'
                                  ))?>
                                </div>
                              </div>
                              <?php echo form_close();?>
                          </div>
                        </div>

                        <!-- Manual Tab Panel-->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                          <!-- <button type="button" class="btn btn-primary pull-right btn-add-rows"><i class="fa fa-plus"></i> <?php echo __('Add rows');?></button> -->
                          <div class="clearfix"></div>
                          <?php echo form_open('',array('name'=>'manual-form'));?>

                            <div class="rowAgentClothDeal">
                            <?php echo form_input(array('name'=>'manualSelectSize','type'=>'hidden'));?>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?php echo __('CardNo')?> : </label>
                                    <?php echo form_input(array('name'=>'CardNoManual','class'=>'form-control','placeholder'=>__('Fill agent card number and choose size'),'onkeyup'=>'cardNoManualKeyup(this)'));?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?php echo __('Name&Lastname')?> : </label>
                                    <?php echo form_input(array('name'=>'AgentNameAndSurName','class'=>'form-control','readonly'=>'readonly'))?>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label><?php echo __('Total Tickets')?> : </label>
                                    <?php echo form_input(array('name'=>'TotalTickets','class'=>'form-control','readonly'=>'readonly'))?>

                                </div>
                            </div>
                            <div class="col-md-1" style="display: none;">
                                <div class="form-group">
                                    <label><?php echo __('Choose Size')?> : </label>
                                    <p>
                                    <button type="button" class="btn btn-default btn-select-size" disabled="disabled"><i class="fa fa-pencil"></i></button>
                                    </p>
                                </div>
                                
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <p>
                                    <button type="button" class="btn btn-danger btn-delete-row" onclick="$(this).closest('.rowAgentClothDeal').remove();"><i class="fa fa-minus"></i></button>
                                  </p>
                                </div>
                            </div>

                            

                            <div class="clearfix"></div>
                            </div>

                          <?php echo form_close();?>
                        </div>
                        <!-- Eof Manual Tab Panel-->


                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>