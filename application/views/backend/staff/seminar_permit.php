  <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Seminar')?> <small><?php echo __('Check Permit')?></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    <!-- <li><a href="<?php echo base_url('backend/'.$this->controller)?>"><?php echo __('Seminar List','backend/default')?></a></li> -->
                    <li class="active"><?php echo __('Seminar check permit')?></li>
                </ul>
                <!-- eof bread crumb-->

                <div class="x_panel">
                  <div class="x_title">
                    
                     <h2><?php echo __('Check permit form')?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="row">
                        <?php echo form_open('',array('name'=>'check-permit-form'))?>
                        <div class="col-lg-4 col-lg-offset-4">
                            <label><strong><?php echo __('Card No')?> : </strong></label>
                            <div class="form-group">
                              <?php echo form_input(array(
                                'name'=>'card_no',
                                'class'=>'form-control'
                              ))?>
                            </div>


                            <div class="form-group">
                              <?php echo form_button(array(
                                'type'=>'button',
                                'class'=>'btn btn-primary btn-block btn-xs',
                                'content'=>__('Check Permission'),
                                'onclick'=>'checkPermitByCardNo(this)'
                              ))?>
                            </div>
                        </div>
                        <?php echo form_close()?>

                      </div>
                      <hr>
                      <div class="row" id="result-data">

                        <div class="col-lg-4" id="agent-content" style="display: none;">
                            <div class="panel panel-default">
                              <div class="panel-heading text-center"><?php echo __('Agent Data')?></div>
                              <div class="panel-body">
                                  

                              </div>
                            </div>
                        </div>

                        <div class="col-lg-8" id="ticket-history-content" style="display: none;">
                            <div class="panel panel-default panel-ticket-history">
                              <div class="panel-heading text-center"><?php echo __('Ticket History')?></div>
                              <div class="panel-body">
                                

                              </div>
                            </div>

                            <div class="panel panel-default panel-quota-history">
                              <div class="panel-heading text-center"><?php echo __('Quota History')?></div>
                              <div class="panel-body">
                                

                              </div>
                            </div>


                        </div>

                      </div>
                  </div>
                </div>
                </div>
            </div>
