            <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Customer Data')?> <small><?php echo __('Customer')?></small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                  <!-- <button class="btn btn-default pull-right" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search"></i> <?php echo __('Search','backend/default')?></button> -->
                  <!-- <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div> -->

                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    <li><a href="<?php echo base_url('backend/'.$this->controller)?>"><?php echo __('Customer','backend/default')?></a></li>
                    <li class="active"><?php echo __('Customer List')?></li>
                </ul>

                <!-- Search Panel -->
                <div class="panel-group">
                <div class="panel panel-success">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" href="#searchCustomer"><i class="fa fa-search"></i> <?php echo __('Search Customer')?></a>
                    </h4>
                  </div>
                  <div id="searchCustomer" class="panel-collapse collapse">
                    <div class="panel-body">
                      <!-- <form class="form-inline" action="/action_page.php"> -->
                      <?php echo form_open('',array('class'=>'form-inline','method'=>'GET'))?>
                      <div class="form-group">
                        <label for="customer_id"><?php echo __('Customer ID')?> :</label>
                        <?php echo form_input(array('name'=>'customer_id','value'=>@$this->input->get('customer_id'),'class'=>'form-control'));?>
                      </div>
                      <div class="form-group">
                        <label for="customer_name"><?php echo __('Customer Name')?> :</label>
                        <?php echo form_input(array('name'=>'customer_name','value'=>@$this->input->get('customer_name'),'class'=>'form-control'));?>
                      </div>
                      <div class="form-group">
                        <label for="pwd"><?php echo __('Customer Email')?> :</label>
                        <?php echo form_input(array('name'=>'customer_email','value'=>@$this->input->get('customer_email'),'class'=>'form-control'));?>
                      </div>
                      <!-- <div class="form-group" style="display: none;">
                        <label><?php echo __('Customer Telephone')?> :</label>
                        <?php echo form_input(array('name'=>'customer_telephone','class'=>'form-control'));?>
                      </div> -->
                      <!-- <div class="checkbox">
                        <label><input type="checkbox"> Remember me</label>
                      </div> -->
                      <div class="form-group">
                      <button type="submit" class="btn btn-success"><?php echo __('Submit','backend/default')?></button>
                      <a href="<?php echo base_url('backend/'.$this->controller);?>" class="btn btn-danger btn-reset"><?php echo __('Reset','backend/default')?></a>
                    </div>
                    <?php echo form_close();?>
                    </div>
                    <!-- <div class="panel-footer">Panel Footer</div> -->
                  </div>
                </div>
              </div>
                <!-- Eof search panel -->
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo __('Customer list')?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-sort"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">-</a>
                          </li>
                          <li><a href="#">-</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                   
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo __('CustomerID')?></th>
                          <th><?php echo __('Name')?></th>
                          <th><?php echo __('Email')?></th>
                          <th><?php echo __('Telephone')?></th>
                          <th><?php echo __('Product Item(s)')?></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php foreach($customer as $key => $row){
                          $product_warranty = new M_productwarranty();
                          $product_warranty->where('ConsumerID',$row->CustID);
                          //echo $product_warranty->count();
                        ?>
                        <tr>
                          <td>#</td>
                          <td><?php echo $row->CustID;?></td>
                          <td><?php echo $row->CustName;?></td>
                          <td><?php echo $row->Email;?></td>
                          <td><?php echo $row->Telephone;?></td>
                          <td><?php echo $product_warranty->count();?></td>
                          <td>
                            <a href="<?php echo base_url('backend/'.$this->controller.'/view/'.($row->CustID))?>" class="btn btn-default btn-xs" title="<?php echo __('View','default_backend')?>"><i class="fa fa-eye"></i></a>

                            <a href="<?php echo base_url('backend/'.$this->controller.'/edit/'.($row->CustID))?>" class="btn btn-info btn-xs" title="<?php echo __('Edit','default_backend')?>"><i class="fa fa-pencil"></i></a>

                            <a href="<?php echo base_url('backend/'.$this->controller.'/delete/'.($row->CustID))?>" class="btn btn-danger btn-xs" title="<?php echo __('Delete','default_backend')?>"><i class="fa fa-trash-o"></i></a>
                          </td>
                        </tr>
                        <?php }?>
                        
                      </tbody>
                      <tfoot>
                        
                        
                      </tfoot>
                    </table>
                    <!-- end project list -->


                    <!-- section for pagination -->
                    <div class="text-right">
                    <?php echo $pages;?>
                    </div>
                    <!-- eof section for pagination -->
                  </div>
                </div>
              </div>
            </div>
