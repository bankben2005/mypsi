            <input type="hidden" name="method" value="<?php echo $this->method?>">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo ($customer->CustID)?__('Edit Customer'):__('Create Customer')?></h3>
              </div>

              
            </div>
            <div class="clearfix"></div>


            <div class="row">
              <div class="col-md-12">
                 
                  <ul class="breadcrumb">
                      <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                      <li><a href="<?php echo base_url('backend/'.$this->controller)?>"><?php echo __('Customer','backend/default')?></a></li>
                      <li class="active"><?php echo (@$customer->CustID)?__('Edit Customer'):__('Create Customer')?></li>
                  </ul>
                 
              </div>

              <div class="col-md-6">

                <div class="x_panel">
                  <div class="x_title">
                    <h2>
                      <?php if(@$customer->CustID){
                        echo $customer->CustName;
                      }else{  
                        echo __('Create customer form');

                      }?>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo __('Name')?></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          
                          <?php echo form_input(array('name'=>'CustName','value'=>@$customer->CustName,'class'=>'form-control'))?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo __('Email')?></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <?php echo form_input(array('type'=>'email','name'=>'Email','value'=>@$customer->Email,'class'=>'form-control'))?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo __('Telephone')?></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <?php echo form_input(array('name'=>'Telephone','value'=>@$customer->Telephone,'class'=>'form-control'))?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo __('Address')?><span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <?php echo form_textarea(array('name'=>'CustAddress','value'=>@$customer->CustAddress,'rows'=>'3','class'=>'form-control'))?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo __('Login Type')?></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <?php echo form_input(array('name'=>'LoginType','value'=>@$customer->LoginType,'class'=>'form-control'))?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo __('Device Token')?></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <?php echo form_input(array('name'=>'Device_Token','value'=>@$customer->Device_Token,'class'=>'form-control'))?>
                        </div>
                      </div>




                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <a href="<?php echo base_url('backend/'.$this->controller)?>" type="button" class="btn btn-primary"><?php echo __('Back','backend/default')?></a>
                          <!-- <button type="reset" class="btn btn-primary">Reset</button> -->
                          <button type="submit" class="btn btn-success"><?php echo __('Submit')?></button>


                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>

              <?php if($count_customer_product > 0){?>
              <div class="col-md-6">
               
                  <div class="x_panel">
                <div class="x_title">
                  <h2><?php echo __('Product List')?> <small>(<?php echo $count_customer_product;?>)</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <ul class="list-unstyled msg_list">

                  <?php foreach($customer_product as $key => $row){
                    $status = "<label class='label label-success'>".__('Active')."</label>";
                    $master_product = new M_masterproduct();
                    $master_product->where('MPCODE',$row->MPCODE)->get();

                    $product_image = 'http://apifixit.psisat.com/'.$master_product->ProductImage;

                    /* check expired */
                    if(strtotime(date('Y-m-d H:i:s')) > strtotime($row->Expire_WarrantyDate)){
                      $status = '<label class="label label-danger">'.__('Expired').'</label>';
                    }
                    /* */
                    // print_r($master_product->to_array());
                  ?>
                    <li>
                      <a>
                        <span class="image">
                          <img src="<?php echo $product_image;?>" alt="img">
                        </span>
                        <span>
                          <span><?php echo $master_product->MPNAME;?></span>
                          <span class="time"><?php echo timeAgo(date($row->Start_WarrantyDate)). ' '.$status; ?></span>
                        </span>
                        <span class="message">
                          <strong><?php echo __('Start Warranty')?> : </strong><?php echo date('Y-m-d H:i:s',strtotime($row->Start_WarrantyDate));?> <strong><?php echo __('Expire Warranty')?> : </strong><?php echo date('Y-m-d H:i:s',strtotime($row->Expire_WarrantyDate));?>
                        </span>
                      </a>
                    </li>
                  <?php }?>
                    
                  </ul>
                </div>
              </div>

              
              </div>
              <?php }else{?>
                <div class="col-md-6">
               
                  <div class="x_panel text-center" style="display: none;">
                      <p class="not-found-error">
                      <?php echo __('Not found product for this customer');?>
                      </p>
                  </div>
                  <div class="alert alert-warning alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong><?php echo __('Not found product for this customer');?></strong>
                  </div>
                </div>


              <?php }?>

            </div>

<script type="text/javascript">
  
(function() {
  var method = document.querySelector('input[name="method"]');
  if(method.value === "view"){
     
      document.querySelector('.btn.btn-success').style.display = 'none';
      var allInput = document.querySelectorAll('.form-control');
      for(var i=0;i<allInput.length;i++){
        allInput[i].readOnly  = true;
      }
  }

})();

</script>