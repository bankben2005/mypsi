<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PSI FIXIT !</title>

    <?php echo $this->template->stylesheet;?>
  </head>

  <body class="nav-md">
    <?php echo form_input(array('type'=>'hidden','name'=>'base_url','value'=>base_url()));?>
    <div class="container body">
      <div class="main_container">
        
        <!-- template left menu -->
        <?php $this->load->view('backend/template_left_menu')?>
        <!-- eof template left menu -->

        <!-- top navigation -->
        <?php $this->load->view('backend/template_top')?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <?php echo $this->template->content;?>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php echo $this->template->javascript;?>
  </body>
</html>