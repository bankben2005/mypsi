          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Expired Filter List')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    
                    <li class="active"><?php echo __('Expired Filter List')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>

                <div class="x_panel">
                  <div class="x_content">
                      <?php echo form_open('',array('name'=>'click-export-form'))?>
                      <?php echo form_input(array(
                        'type'=>'hidden',
                        'name'=>'hidden_export',
                        'value'=>1
                      ))?>
                      <div class="form-group">
                        <?php echo form_button(array(
                          'type'=>'submit',
                          'class'=>'btn btn-success pull-right',
                          'content'=>__('Export to excel file')
                        ))?>
                      </div>

                      <?php echo form_close()?>
                  </div>
                </div>
                <div class="x_panel">
                  <div class="x_title">
                    
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->
                    <?php echo form_open('',array('name'=>'export-expired-filter-frm'))?>
                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th>#</th>
                          
                          <th><?php echo __('Serial Number')?></th>
                          <th><?php echo __('Product Part Name')?></th>
                          <th><?php echo __('Customer Contact')?></th>
                          <th><?php echo __('Customer Telephone')?></th>
                          <th><?php echo __('Customer Address')?></th>
                          <th><?php echo __('Expired Datetime')?></th>
                          <th><?php echo __('Technicial Install')?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($product_part_notification as $key => $row){?>
                          <tr>
                            <td><?php echo ++$key?></td>
                            <td><?php echo $row['serial_number']?></td>
                            <td><?php echo $row['part_name']?></td>
                            <td><?php echo $row['customer_contact']?></td>
                            <td><?php echo $row['customer_telephone']?></td>
                            <td><?php echo $row['customer_address']?></td>
                            <td><?php echo $row['warranty_expir_date']?></td>
                            <td><?php echo $row['agent_code'];?></td>
                          </tr>

                        <?php }?>
                      </tbody>
                  </table>
                  <?php echo form_close()?>
                  </div>
            </div>
        </div>
      </div>