          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('RegisterOfiveList')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    
                    <li class="active"><?php echo __('RegisterOfiveList')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                
                <div class="col-md-12">
                  <div class="row">
                  <!-- <a href="<?php echo base_url('backend/'.$this->controller.'/createCoupon')?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> <?php echo __('Create Coupon')?></a> -->
                </div>
                </div>
                <div class="clearfix"></div>

                <div class="x_panel">
                  <div class="x_title">
                    
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->
                    <?php echo form_open('',array('name'=>'registerofive-frm'))?>
                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo __('SN')?></th>
                          <th><?php echo __('Firstname')?></th>
                          <th><?php echo __('Lastname')?></th>
                          <th><?php echo __('Telephone')?></th>
                          <th><?php echo __('Created')?></th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>
                           <?php foreach($registerdata as $key => $row){?>
                            <tr>
                                <td></td>
                                <td><?php echo $row['logs']->product_sn;?></td>
                                <td><?php echo $row['firstname'];?></td>
                                <td><?php echo $row['lastname'];?></td>
                                <td><?php echo $row['logs']->telephone;?></td>
                                <td><?php echo $row['created'];?></td>
                                <td></td>

                            </tr>
                           <?php }?>                   
                      </tbody>

                    </table>


                      <?php echo form_close();?>
                    

                  </div>
                </div>

              </div>

            </div>
           