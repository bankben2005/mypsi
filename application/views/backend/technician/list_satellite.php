<div class="page-title">
              <div class="title_left">
                <h3><?php echo __('Technician')?> <small><?php echo __('Satellite')?></small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    <li><a href="<?php echo base_url('backend/'.$this->controller)?>"><?php echo __('Technician','backend/default')?></a></li>
                    <li class="active"><?php echo __('Technician Satellite List')?></li>
                </ul>
                <!-- eof bread crumb-->

                <!-- Search Panel -->
                <div class="panel-group">
                <div class="panel panel-success">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" href="#searchTechnician"><i class="fa fa-search"></i> <?php echo __('Search Technician')?></a>
                    </h4>
                  </div>
                  <div id="searchTechnician" class="panel-collapse collapse">
                    <div class="panel-body">
                      <?php echo form_open('',array('class'=>'form-inline','method'=>'GET'))?>
                      <div class="form-group">
                          <label><?php echo __('Province')?> :</label>
                          <?php echo form_dropdown('search_province',@$provinces,@$this->input->get('search_province'),'class="form-control"')?>
                      </div>
                      <div class="form-group">
                          <label><?php echo __('Amphur')?> :</label>
                          <?php echo form_input(array('name'=>'search_amphur','value'=>@$this->input->get('search_amphur'),'class'=>'form-control'))?>
                      </div>
                      <div class="form-group">
                      <button type="submit" class="btn btn-success"><?php echo __('Submit','backend/default')?></button>
                      <a href="<?php echo base_url('backend/'.$this->controller);?>" class="btn btn-danger btn-reset"><?php echo __('Reset','backend/default')?></a>
                      </div>
                    <?php echo form_close();?>
                    </div>
                    <!-- <div class="panel-footer">Panel Footer</div> -->
                  </div>
                </div>
              </div>
                <!-- Eof search panel -->

                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo __('Technician satellite list')?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->

                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="width: 1%">#</th>
                          <th style="width: 20%"><?php echo __('Name')?></th>
                          <th><?php echo __('Address')?></th>
                          <th><?php echo __('Telephone')?></th>
                          <th><?php echo __('Technician Skill')?></th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php foreach($agent as $key => $row){?>
                        <tr>
                          <td>#</td>
                          <td>
                            <?php echo $row->AgentName.' '.$row->AgentSurName;?>
                          </td>
                          <td>
                            <?php echo $row->R_Addr1;?>
                          </td>
                          <td class="project_progress">
                            <?php echo $row->Telephone?>
                          </td>
                          <td>
                                                  <p>
                                                    <?php if($row->FixIt01 && $row->FixIt01 != 'F'){?>
                                                      <i class="fa fa-check-square-o"></i>
                                                      <label>จานดาวเทียม </label>
                                                    <?php }?>
                                                  </p>
                                                    
                                                    <p>
                                                    <?php if($row->FixIt02 && $row->FixIt02 != 'F'){?>
                                                    <i class="fa fa-check-square-o"></i>
                                                    <label>กล้อง OCS </label>
                                                    <?php }?>
                                                  </p>
                                                 

                                                    <p>
                                                    <?php if($row->FixIt03 && $row->FixIt03 != 'F'){?>
                                                    <i class="fa fa-check-square-o"></i>
                                                    <label>แอร์ PSI </label>
                                                    <?php }?>
                                                      </p>


                                                        <p>
                                                    <?php if($row->FixIt04 && $row->FixIt04 != 'F'){?>
                                                    <i class="fa fa-check-square-o"></i>
                                                    <label>ไฟฟ้า </label>
                                                    <?php }?>
                                                  </p>
                                                  <p>

                                                    <?php if($row->FixIt05 && $row->FixIt05 != 'F'){?>
                                                    <i class="fa fa-check-square-o"></i>
                                                    <label>เครื่องกรองน้ำ </label>
                                                    <?php }?>
                                                  </p>
                          </td>
                          <td>
                            <a href="<?php echo base_url('backend/admin_technician/satellite/view/'.($row->AgentCode))?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> <?php echo __('View','default_backend')?> </a>
                            <a href="<?php echo base_url('backend/admin_technician/satellite/edit/'.($row->AgentCode))?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> <?php echo __('Edit','default_backend')?> </a>
                            <a href="<?php echo base_url('backend/admin_technician/satellite/delete/'.($row->AgentCode))?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> <?php echo __('Delete','default_backend')?> </a>
                          </td>
                        </tr>
                        <?php }?>
                        
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
            </div>