          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('QuestionnaireAnswerDetail')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    
                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/questionnaireAnswerList')?>"><?php echo __('QuestionnaireAnswer')?></a></li>
                    <li class="active"><?php echo __('QuestionnaireAnswerDetail')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                
                <div class="clearfix"></div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $agentdata->AgentName.' '.$agentdata->AgentSurName;?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="col-md-12">
                          <div class="alert alert-success">
                          <p class="text-center">
                            <span><?php echo $answerdata[0]->setname;?></span>
                          </p>
                          </div>
                      </div>
                      <div class="col-md-4 col-sm-12 col-xs-12">
                          
                          <div class="panel panel-default">
                          <div class="panel-heading"><?php echo __('Assessor')?></div>
                          <div class="panel-body">
                            <p>
                                <strong><?php echo __('Name&Lastname')?> : </strong>
                                <span><?php echo $agentdata->AgentName.' '.$agentdata->AgentSurName;?></span>
                                
                            </p>
                            <p>
                                <strong><?php echo __('AgentCode')?> : </strong>
                                <span><?php echo $agentdata->AgentCode;?></span>
                            </p>

                            <p>
                                <strong><?php echo __('Telephone')?> : </strong>
                                <span><?php echo $agentdata->Telephone;?></span>
                            </p>
                            <p>
                                <strong><?php echo __('Province')?> : </strong>
                                <span><?php echo $agentdata->Province;?></span>
                            </p>


                          </div>
                          </div>

                          <div class="panel panel-default">
                          <div class="panel-heading"><?php echo __('Assessment')?></div>
                          <div class="panel-body">
                            <p>
                                <strong><?php echo __('Name&Lastname')?> : </strong>
                                <span><?php echo $sunamedata->AgentName.' '.$sunamedata->AgentSurName;?></span>
                                
                            </p>
                            <p>
                                <strong><?php echo __('AgentCode')?> : </strong>
                                <span><?php echo $sunamedata->AgentCode;?></span>
                            </p>

                            <p>
                                <strong><?php echo __('Telephone')?> : </strong>
                                <span><?php echo $sunamedata->Telephone;?></span>
                            </p>
                            <p>
                                <strong><?php echo __('Province')?> : </strong>
                                <span><?php echo $sunamedata->Province;?></span>
                            </p>


                          </div>
                          </div>
                      </div>

                      <div class="col-md-8">
                              <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo __('Question')?></th>
                                    <th><?php echo __('QuestionnaireCategory')?></th>
                                    <th><?php echo __('AssessmentResult')?></th>
                                </tr>
                              </thead>
                              <tbody>
                                    <?php foreach($answerdata as $key => $row){?>
                                      <tr>
                                          <td></td>
                                          <td><?php echo $row->Question?></td>
                                          <td><?php echo $row->Name;?></td>
                                          <td><?php echo $row->rate_name;?></td>
                                      </tr>

                                    <?php }?>

                              </tbody>
                                

                              </table>

                              <div class="control-group">
                                  <p><strong><?php echo __('Recommend')?> : </strong>
                                  <span><?php echo $answerdata[0]->Recommend;?></span>
                                  </p>
                              </div>
                      </div>
                      <div class="clearfix"></div>
                    

                  </div>
                </div>

              </div>

            </div>