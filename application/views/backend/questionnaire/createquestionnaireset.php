          <div class="page-title">
              <div class="title_left">
                <h3><?php echo ($questionnaireset->getId())?__('Edit'):__('Create').__('QuestionnaireSet')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>

                    <li><a href="<?php echo base_url('backend/'.$this->controller.'/questionnaireSetList')?>"><?php echo __('QuestionnaireSetList')?></a></li>
                    
                    <li class="active"><?php echo ($questionnaireset->getId())?__('Edit'):__('Create').__('QuestionnaireSet')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">
                    </div>

                </div> -->
                
                
                <div class="clearfix"></div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php ($questionnaireset->getId())?__('Edit'):__('Create').__('QuestionnaireSet')?></h2>
                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php echo form_open('',array('id'=>'create-frm'))?>

                      <div class="col-md-4">
                        <div class="row">
                          <div class="form-group">
                            <label><?php echo __('Name')?> : </label>
                            <?php echo form_input(array('name'=>'Name','value'=>@$questionnaireset->Name,'class'=>'form-control'))?>
                          </div>
                          <div class="form-group">
                            <label><?php echo __('Description')?> : </label>
                            <?php echo form_textarea(array('name'=>'Description','value'=>@$questionnaireset->Description,'class'=>'form-control','rows'=>2))?>
                          </div>

                          <div class="form-group">
                            <label><?php echo __('Duration')?> : </label>
                            <?php echo form_input(array('name'=>'Duration','value'=>@$duration,'class'=>'form-control'))?>
                          </div>
                          <!-- <div class="form-group">
                            <label><?php echo __('Start Date')?> : </label>
                            <?php echo form_input(array('name'=>'StartDate','class'=>'form-control'))?>
                          </div>
                          <div class="form-group">
                            <label><?php echo __('End Date')?> : </label>
                            <?php echo form_input(array('name'=>'EndDate','class'=>'form-control'))?>
                          </div> -->


                          <div class="form-group">
                            <label><?php echo __('Active')?> : </label>
                            <?php echo form_dropdown('Active',array('1'=>__('Active'),'0'=>__('Unactive')),@$questionnaireset->Active,'class="form-control"')?>
                          </div>
                          <div class="form-group">
                            <?php echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
                            <div class="clearfix">
                          </div>
                        </div>
                      </div>

                    <?php echo form_close();?>
                  </div>
                </div>

              </div>
            </div>
