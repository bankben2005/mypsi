          <div class="page-title">
              <div class="title_left">
                <h3><?php echo __('QuestionnaireAnswer')?> <small></small></h3>
              </div>

              <div class="title_right" style="display: none;">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <!-- bread crumb-->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('backend')?>"><?php echo __('Home','backend/default')?></a></li>
                    
                    <li class="active"><?php echo __('QuestionnaireAnswer')?></li>
                </ul>
                <!-- eof bread crumb-->
                <?php echo message_warning($this)?>

                <!-- <div class="x_panel">
                    <div class="x_content">

                    </div>

                </div> -->
                
                <div class="clearfix"></div>

                <!-- Search Panel -->
                <div class="panel-group">
                <div class="panel panel-success">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" href="#exportQuestionnaireAnswer"><i class="fa fa-file-excel-o"></i> <?php echo __('Export to xlsx')?></a>
                    </h4>
                  </div>
                  <div id="exportQuestionnaireAnswer" class="panel-collapse collapse">
                    <div class="panel-body">
                      <!-- <form class="form-inline" action="/action_page.php"> -->
                      <?php echo form_open(base_url('backend/admin_questionnaire/exportQuestionnaire'),array('class'=>'form-inline'))?>
                        <div class="form-group">
                            <?php echo form_dropdown('export_questionnaireset',$questionnaireset,'','class="form-control"')?>
                        </div>

                        <div class="form-group">
                            <?php echo form_button(array(
                              'type'=>'submit',
                              'class'=>'btn btn-success btn-xs',
                              'content'=>__('Export')
                              ))?>
                        </div>
                      <?php echo form_close();?>
                    </div>
                    
                    </div>
                    <!-- <div class="panel-footer">Panel Footer</div> -->
                  </div>
                </div>
              
                <!-- Eof search panel -->



                <div class="x_panel">
                  <div class="x_title">
                    <div class="row">
                      <div class="col-md-3">
                    <?php echo form_dropdown('questionnaireset',$questionnaireset,@$this->input->get('questionnaireset'),'class="form-control" onchange="changeQuestionnaireSet(this.value)"')?>
                      </div>
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                  </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- <p>Simple table with project listing with progress and editing options</p> -->
                    <?php echo form_open('',array('name'=>'questionnaire-frm'))?>
                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th><?php echo __('QuestionnaireSet')?></th>
                          <th><?php echo __('AgentCode')?></th>
                          <th><?php echo __('Assessor')?></th>
                          <th><?php echo __('Assessment')?></th>
                          <th><?php echo __('Summary')?></th>
                          <th><?php echo __('Recommend')?></th>
                          <th><?php echo __('Created')?></th>
                          <th style="width: 20%"></th>
                        </tr>
                      </thead>
                      <tbody>
                            <?php foreach($questionnaireanswer as $key => $row){?>
                              <tr>
                                  <td></td>
                                  <td><?php echo $row->Description?></td>
                                  <td><?php echo $row->Assessor->AgentCode?></td>
                                  <td><?php echo $row->Assessor->AgentName.' '.$row->Assessor->AgentSurName;?></td>
                                  <td>
                                    <?php if($row->Assessment->AgentType == 'AD'){
                                        echo $row->Assessment->TradeName.' '.$row->Assessment->AgentName;
                                    }else{
                                        echo $row->Assessment->AgentName.' '.$row->Assessment->AgentSurName;
                                    }

                                    ?>
                                  </td>
                                  <td class="project_progress">
                                  <div class="progress progress_sm">
                                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="<?php echo $row->summary_percent?>"></div>
                                  </div>
                                  <small><?php echo $row->summary_percent?>%</small>
                                </td>
                                  
                                  <td>
                                    <?php echo $row->Recommend?>
                                  </td>
                                  <td>
                                      <?php echo date('Y-m-d H:i:s',strtotime($row->answer_created));?>
                                  </td>
                                  <td>

                                    <?php if(@$this->event_ability->view || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                      <a href="<?php echo base_url('backend/'.$this->controller.'/viewQuestionnaireAnswerDetail/'.$row->answer_id)?>" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>
                                    <?php }?>

                                    <?php if(@$this->event_ability->delete || in_array('Administrator', $this->account_data['member_controller_available'])){?>
                                      <a href="javascript:void(0);" onclick="if(confirm('ต้องการลบใช่หรือไม่') == true){window.location.href='<?php echo base_url('backend/'.$this->controller.'/deleteQuestionnaireAnswer/'.$row->answer_id);?>'}" class="btn btn-danger btn-xs">
                                        <i class="fa fa-trash"></i> <?php echo __('Delete','backend/default');?>
                                      </a>
                                      <?php }?>
                                  </td>
                              </tr>

                            <?php }?>           
                      </tbody>

                    </table>


                      <?php echo form_close();?>
                    

                  </div>
                </div>

              </div>

            </div>