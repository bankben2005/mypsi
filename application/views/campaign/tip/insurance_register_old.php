<!-- Navigation-->
  <nav class="navbar bg-dark fixed-top" id="mainNav" style="max-width:100%;background:#0f0e9f !important;padding: 10px;">
 <img src="<?php echo base_url('assets/images/campaign/tip/logo-dhipaya-white.png')?>" style="max-width:150px;">
  </nav>

<div class="container">
	
		<div class="col-md">
			<div class="alert alert-success">
				สิทธิพิเศษสำหรับลูกค้า PSI กรอกข้อมูลส่วนตัวและข้อมูลที่อยู่อาศัย เพื่อรับประกันภัยให้กับที่อยู่อาศัยของท่าน 3 เดือน (ไม่มีค่าใช้จ่าย)		
			</div>
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa fa-pencil"></i> กรอกข้อมูลส่วนตัวและข้อมูลที่อยู่</div>
				<div class="panel-body">

					<?php echo form_open('',array('name'=>'insurance-register-form'))?>
					<?php echo form_input(array('type'=>'hidden','name'=>'ConsumerID','value'=>''))?>

					<div class="form-group">
						<?php echo form_dropdown('home_type',array(
								'บ้านปูน'=>'บ้านปูน',
								'บ้านครึ่งปูนครึ่งไม้'=>'บ้านครึ่งปูนครึ่งไม้',
								'บ้านไม้'=>'บ้านไม้',
							),'','class="form-control"')?>
					</div>
					<div class="form-group">
						<?php echo form_dropdown('title',array(
								'นาย'=>'นาย',
								'นาง'=>'นาง',
								'นางสาว'=>'นางสาว'
							),'','class="form-control"')?>
					</div>

					<div class="form-group">

						<?php echo form_input(array(
								'name'=>'firstname',
								'class'=>'form-control',
								'placeholder'=>'ชื่อ'
							))?>
					</div>

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'lastname',
								'class'=>'form-control',
								'placeholder'=>'นามสกุล'
							))?>
					</div>

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'telephone',
								'class'=>'form-control',
								'placeholder'=>'เบอร์โทรศัพท์'
							))?>
					</div>

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'email',
								'class'=>'form-control',
								'placeholder'=>'อีเมล์'
							))?>
					</div>		

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'id_card',
								'class'=>'form-control',
								'placeholder'=>'เลขบัตรประจำตัวประชาชน'
							))?>
					</div>

					

					<div class="form-group">
						<label><strong>วัน/เดือน/ปี เกิด : </strong></label>
						<?php echo form_input(array(
								'name'=>'dob',
								'class'=>'form-control',
								'placeholder'=>'วัน/เดือน/ปี เกิด'
							))?>
					</div>

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'address_no',
								'class'=>'form-control',
								'placeholder'=>'บ้านเลขที่'
							))?>
					</div>

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'address_moo',
								'class'=>'form-control',
								'placeholder'=>'หมู่'
							))?>
					</div>

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'address_soi',
								'class'=>'form-control',
								'placeholder'=>'ซอย'
							))?>
					</div>

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'address_road',
								'class'=>'form-control',
								'placeholder'=>'ถนน'
							))?>
					</div>

					<div class="form-group">
						<?php echo form_dropdown('province_id',$select_province,'','class="form-control" onchange="changeProvince(this.value)"')?>
					</div>

					<div class="form-group">
						<?php echo form_dropdown('amphur_id',array(''=>'อำเภอ'),'','class="form-control" onchange="changeAmphur(this.value)" disabled')?>
					</div>

					<div class="form-group">
						<?php echo form_dropdown('district_id',array(''=>'ตำบล'),'','class="form-control" onchange="changeDistrict(this.value)" disabled')?>
					</div>

					<div class="form-group">
						<?php echo form_input(array(
								'name'=>'zipcode',
								'class'=>'form-control',
								'placeholder'=>'รหัสไปรษณีย์',
								'disabled'=>'disabled'
							))?>
					</div>

					
					<div class="form-group condition-content">
							<label>
							<?php echo form_checkbox(array(
									'name'=>'accept_condition',
									'value'=>'1'
								));?> คุณยอมรับเงื่อนไขและผลประโยชน์ที่ได้รับ <a href="javascript:void(0)" data-toggle="modal" data-target="#conditionModal"><u>>>อ่านเงื่อนไข<<</u></a>
							</label> 

							
					</div>
					<div class="form-group condition-content">
							<label>
								<?php echo form_checkbox(array(
									 'name'=>'accept_enewsletter',
									 'value'=>'1'
									))?> ต้องการรับข้อมูลข่าวสารผ่านอีเมล์
							</label>
					</div>

					<div class="form-group">
						<?php echo form_button(array(
								'type'=>'submit',
								'class'=>'btn btn-primary btn-block',
								'content'=>'<i class="fa fa-registered"></i> '.'ลงทะเบียน'
							))?>
					</div>

	
					<?php echo form_close();?>
				</div>
			</div>




		</div>
	

</div>


<!-- Modal -->
<div class="modal fade" id="conditionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">เงื่อนไขและผลประโยชน์ที่ได้รับ</h4>
      </div>
      <div class="modal-body">
      	<table class="table">
			<thead>
				<tr>
					<th class="text-center">เงื่อนไขโครงการ สิทธิพิเศษเฉพาะลูกค้า PSI</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1. บริษัทฯ ขอสงวนสิทธิ์ เฉพาะลูกค้าที่ลงทะเบียนรับสิทธิ์ผ่านทาง FIXIT เท่านั้น</td>
				</tr>
				<tr>
					<td>2. ระยะเวลาการรับสิทธิ์ ตั้งแต่วันที่ 1 พฤษภาคม 2561 ถึงวันที่ 31 กรกฎาคม 2561</td>
				</tr>
				<tr>
					<td>3. ให้ความคุ้มครองเฉพาะสถานที่ ที่ใช้เป็นที่อยู่อาศัยเท่านั้น (ต้องไม่มีการประกอบกิจการหรือการค้าขายใดๆ)</td>
				</tr>
				<tr>
					<td>4. จำนวนผู้รับสิทธิ์ทั้งโครงการ 10,000 สิทธิ</td>
				</tr>
				<tr>
					<td>5. รับประกันภัยโดยบริษัท ทิพยประกันภัย จำกัด (มหาชน)</td>
				</tr>
				<tr>
					<td>6. เงื่อนไขความคุ้มครอง เงื่อนไขทั่วไป และข้อยกเว้น เป็นไปตามกรมธรรม์ประกันอัคคีภัยที่อยู่อาศัยฉบับมาตรฐาน</td>
				</tr>
				<tr>
					<td>7. ติดต่อเรื่องสินไหมทดแทน โทร 02-239- 2200 ต่อ 2403 และหากมีข้อสงสัย</td>
				</tr>
			</tbody>
		</table>

      	<p>- คุ้มครองทั้งตัวบ้านและเฟอร์นิเจอร์</p>
      	<p>- สถานที่ใช้เป็นที่อยู่อาศัยเท่านั้น (ไม่สามารถประกอบกิจการหรือเก็บสต๊อกสินค้าได้)</p>
		<p>- วงเงินคุ้มครอง 100,000 บาท</p>
		<p>- ระยะเวลา 3 เดือน</p>

		<table class="table">
			<thead>
				<tr>
					<th>ความคุ้มครอง</th>
					<th>วงเงินคุ้มครอง</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>ไฟไหม้,ฟ้าผ่า,ระเบิด,ภัยจากยานพาหนะ,ภัยจากอากาศยาน,ภัยเนื่องจากน้ำ</td>
					<td class="text-right">100,000 บาท</td>
				</tr>
				<tr>
					<td>ภัยน้ำท่วม,ภัยลมพายุ,ภัยแผ่นดินไหว,ภัยลูกเห็บ</td>
					<td class="text-right">5,000 บาท</td>
				</tr>
			</tbody>

		</table>



		<div class="alert alert-warning">สอบถามรายละเอียดเพิ่มเติมติดต่อ  <br>02-2392200 ต่อ 1942</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        
      </div>
    </div>
  </div>
</div>