<!-- Navigation-->
  <nav class="navbar bg-dark fixed-top" id="mainNav" style="max-width:100%;background:#0f0e9f !important;padding: 10px;">
 <img src="<?php echo base_url('assets/images/campaign/tip/logo-dhipaya-white.png?'.strtotime(date('Ymd H:i:s')))?>" style="max-width:150px;">
  </nav>

<div class="container">
	
		<div class="col-md">
					<div class="panel panel-warning">
							<div class="panel-heading text-center"><h4>คุณได้ทำการลงทะเบียนเข้าร่วมโครงการแล้ว!!</h4></div>

							<div class="panel-body">
								<p>คุณสามารถลงทะเบียนได้เพียงท่านละ 1 สิทธิ์ เท่านั้น ในกรณีที่คุณมีหลายผลิตภัณฑ์ การลงทะเบียนเพียง 1 สิทธิ์ คุ้มครองครอบคลุมตามเงื่อนไขและผลตอบแทนทุกประการ</p>
							</div>
					</div>
		</div>

</div>