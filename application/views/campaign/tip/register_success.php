<!-- Navigation-->
  <nav class="navbar bg-dark fixed-top" id="mainNav" style="max-width:100%;background:#0f0e9f !important;padding: 10px;">
 <img src="<?php echo base_url('assets/images/campaign/tip/logo-dhipaya-white.png?'.strtotime(date('Ymd H:i:s')))?>" style="max-width:150px;">
  </nav>

<div class="container">
	
		<div class="col-md">
					<div class="panel panel-success">
							<div class="panel-heading text-center"><h4>ลงทะเบียนรับความคุ้มครองสำเร็จ!!</h4></div>

							<div class="panel-body">
								<table class="table">
						      		<thead>
						      			<tr>
						      				<th class="text-center">เงื่อนไขโครงการ</th>
						      			</tr>
						      		</thead>
						      		<tbody>
						      			<tr>
						      				<td>- สงวนสิทธิ์ เฉพาะลูกค้าที่ลงทะเบียนรับสิทธิ์ผ่านทาง Application FIXIT เท่านั้น</td>
						      			</tr>
						      			<tr>
											<td>- จำกัดจำนวนผู้ร่วมรับสิทธิ์กับโครงการ</td>
										</tr>
						      			<tr>
						      				<td>- จำกัด 1 รายชื่อต่อ 1 สิทธิ์</td>
						      			</tr>
						      			<tr>
						      				<td>- ระยะเวลาการรับสิทธิ์ ตั้งแต่วันที่ 16 พฤษภาคม 2561 ถึงวันที่ 15 สิงหาคม 2561</td>
						      			</tr>
						      			<tr>
						      				<td>- การรับสิทธิ์นี้ไม่สามารถเปลี่ยนแปลงเป็นเงินสดได้</td>
						      			</tr>
						      			<tr>
						      				<td>- ติดต่อสอบถามความคุ้มครอง โทร 02-239-2200 ต่อ 1942,2170</td>
						      			</tr>
						      		</tbody>

						      	</table>

						      	<table class="table">
									<thead>
										<tr>
											<th class="text-center">สรุปความคุ้มครอง</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>คุ้มครอง
						สินค้าPSI ที่ลงทะเบียนผ่าน application fixit รวมตัวบ้านและเฟอร์นิเจอร์ในบ้าน วงเงิน 100,000 บาท</td>
										</tr>
										
									</tbody>
								</table>
								<!-- <p>ท่านได้รับสิทธิ์ตามที่ลงทะเบียนแคมเปญ PSI-TIP เรียบร้อยแล้ว ใบสรุปความคุ้มครองจะจัดส่งให้ตามอีเมลที่ลงทะเบียนไว้อีกครั้ง</p>

								<p><strong>สิทธิผลประโยชน์ที่ได้รับ</strong></p>

								<p>- คุ้มครองทั้งตัวบ้านและเฟอร์นิเจอร์</p>
								<p>- วงเงินคุ้มครอง 100,000 บาท</p>
								<p>- ระยะเวลา 3 เดือน</p>

								<table class="table">
									<thead>
										<tr>
											<th>ความคุ้มครอง</th>
											<th>วงเงินคุ้มครอง</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1. ไฟไหม้,ฟ้าผ่า,ระเบิด,ภัยจากยานพาหนะ,ภัยจากอากาศยาน,ภัยเนื่องจากน้ำ</td>
											<td class="text-right">100,000 บาท</td>
										</tr>
										<tr>
											<td>2. ภัยน้ำท่วม,ภัยลมพายุ,ภัยแผ่นดินไหว,ภัยลูกเห็บ</td>
											<td class="text-right">5,000 บาท</td>
										</tr>
									</tbody>

								</table>
								- วงเงินชดใช้ข้อ 1 กับ 2 รวมกันไม่เกิน 100,000 บาท
								<div class="alert alert-warning">สอบถามรายละเอียดเพิ่มเติมติดต่อ  <br>02-2392200 ต่อ 1942</div> -->
							</div>
					</div>


					

		</div>
</div>