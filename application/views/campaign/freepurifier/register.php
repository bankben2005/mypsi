
<style type="text/css">
	
</style>
<img src="<?php echo base_url('assets/campaign/freepurifier/images/banner2_1.jpg')?>" id="bg">
<!-- <div class="container-fluid" style="background-image: url('<?php echo base_url('assets/campaign/freepurifier/images/banner2_2.jpg')?>');background-size: 100%;background-repeat: no-repeat;"> -->
<div class="container-fluid" id="container-bg">
<div class="container mt-2" id="container-form">

	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
				<div class="panel panel-primary panel-register">
		<div class="panel-heading text-center">
			<!-- <img src="<?php echo base_url('assets/campaign/freepurifier/images/logo-psi2018.png')?>" style="max-width: 80px;"> -->
		<h1> ลงทะเบียนโครงการพีเอสไอ วี แคร์</h1>

	</div>
		<div class="panel-body">
			<?php echo form_open('',['name'=>'register-free-purifier','style'=>'margin-top:10px;','action'=>'javascript:return;'])?>

					<div class="row">
						<div class="col-lg-12">


							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Firstname')?></strong> <span class="required">*</span></label>
										<?php echo form_input([
											'name'=>'firstname',
											'class'=>'form-control',
											'placeholder'=>__('Firstname')
										])?>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Lastname')?></strong> <span class="required">*</span></label>
										<?php echo form_input([
											'name'=>'lastname',
											'class'=>'form-control',
											'placeholder'=>__('Lastname')
										])?>
									</div>

								</div>
							</div>

							

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Telephone')?> : </strong> <span class="required">*</span></label>
										<?php echo form_input([
											'name'=>'telephone',
											'class'=>'form-control'
										])?>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Email')?> : </strong></label>
										<?php echo form_input([
											'type'=>'email',
											'name'=>'email',
											'class'=>'form-control'
										])?>
									</div>

								</div>
							</div>

							

							<div class="form-group">
								<label><strong><?php echo __('Address')?> : </strong> <span class="required">*</span></label>
								<?php echo form_textarea([
									'name'=>'address',
									'class'=>'form-control',
									'rows'=>2
								])?>
							</div>

							<hr>
							<h2 class="text-center">มีความประสงค์ นำเสนอ </h2>
							<hr>
							<!-- <p>มีความประสงค์ นำเสนอ (ชื่อสถานพยาบาล)</p> -->

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong>(ชื่อสถานพยาบาล) : </strong> <span class="required">*</span></label>
										<?php echo form_input([
											'name'=>'place_name',
											'class'=>'form-control'
										])?>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Telephone')?> : </strong> <span class="required">*</span></label>
										<?php echo form_input([
											'name'=>'place_telephone',
											'class'=>'form-control'
										])?>
									</div>

								</div>
							</div>
							

							<div class="form-group">
								<label><strong><?php echo __('Address')?> : </strong> <span class="required">*</span></label>
								<?php echo form_textarea([
									'name'=>'place_address',
									'class'=>'form-control',
									'rows'=>2
								])?>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Province')?> : </strong> <span class="required">*</span></label>
										<?php echo form_dropdown('select_province',@$select_province,'','class="form-control" onchange="changeProvince(this.value)"')?>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Amphur')?> : </strong> <span class="required">*</span></label>
										<?php echo form_dropdown('select_amphur',@$select_amphur,'','class="form-control" onchange="changeAmphur(this.value)" readonly="readonly"')?>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('District')?> : </strong> <span class="required">*</span></label>
										<?php echo form_dropdown('select_district',@$select_district,'','class="form-control" onchange="changeDistrict(this.value)" readonly="readonly"')?>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Zipcode')?> : </strong></label>
										<?php echo form_input([
											'name'=>'zipcode',
											'class'=>'form-control',
											'readonly'=>'readonly'
										])?>
									</div>

								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Coordinator')?> : </strong> <span class="required">*</span></label>
										<?php echo form_input([
											'name'=>'place_coordinator',
											'class'=>'form-control'
										])?>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label><strong><?php echo __('Coordinator Telephone')?> : </strong> <span class="required">*</span></label>
										<?php echo form_input([
											'name'=>'place_coordinator_telephone',
											'class'=>'form-control'
										])?>
									</div>

								</div>
							</div>

							

							<!-- <div class="form-group">
								<label><strong><?php echo __('Advisor Customer')?></strong></label>
								<?php echo form_input([
									'name'=>'place_advisor_customer',
									'class'=>'form-control'
								])?>
							</div> -->
							<div class="alert alert-warning" style="font-size: 14px;">

								<div class="form-group">
									<label><strong><u>ท่านทราบข้อมูลโครงการจากช่องทางใด?</u></strong></label> <br><br>


									<div class="row">
										<div class="col-lg-4">
											<label>
											<?php echo form_checkbox([
												'name'=>'get_campaign_from[]',
												'value'=>'psi_website'
											])?> <span>Website : psi.co.th</span>
											</label>

										</div>

										<div class="col-lg-4">
											<label>
											<?php echo form_checkbox([
												'name'=>'get_campaign_from[]',
												'value'=>'fb_psisatellitedise'
											])?> <span>FB : psisatellitedise</span>
											</label>

										</div>
										<div class="col-lg-4">
											<label>
											<?php echo form_checkbox([
												'name'=>'get_campaign_from[]',
												'value'=>'fb_psisaradee99'
											])?> <span>FB : psisaradee99</span>
											</label>

										</div>

										
									</div>

									<div class="row mt-1">
										<div class="col-lg-4">
											<label>
											<?php echo form_checkbox([
												'name'=>'get_campaign_from[]',
												'value'=>'psi_official'
											])?> <span>Psi Official</span>
											</label>

										</div>

										<div class="col-lg-4">
											<label>
											<?php echo form_checkbox([
												'name'=>'get_campaign_from[]',
												'value'=>'channel_psisaradee99'
											])?> <span>ช่องพีเอสไอ สาระดี</span>
											</label>
										</div>

										<div class="col-lg-4">
											<label>
											<?php echo form_checkbox([
												'name'=>'get_campaign_from[]',
												'value'=>'other'
											])?> <span>อื่น ๆ</span>
											</label>
										</div>
									</div>

									<div class="row mt-1" id="type-other-desc" style="display: none;">
										<div class="col-lg-12">
											<?php echo form_textarea([
												'name'=>'other_description',
												'class'=>'form-control',
												'rows'=>3,
												'placeholder'=>'ใส่แหล่งรับข้อมูลโครงการช่องทางอื่นๆ..'
											])?>
										</div>
									</div>
									

								</div>
								

							</div>	


							<div class="form-group">
								<label>&nbsp;</label>
								<?php echo form_button([
									'type'=>'submit',
									'class'=>'btn btn-block btn-primary',
									'content'=>__('REGISTER')
								])?>
							</div>


							

						</div>
						<!-- <div class="col-lg-6">
							
						</div> -->
					</div>

			<?php echo form_close()?>
		</div>
	</div>

		</div>
	</div>


</div>

</div>

