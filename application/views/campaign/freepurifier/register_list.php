
<style type="text/css">
	.pd-0{
		padding: 0px;
	}
	.pd-2{
		padding: 2px;
	}
	.mt-2{
		margin-top: 20px;
	}
  table.table div.alert{
    font-size: 8px;
  }
</style>
<div class="container-fluid text-center">
	<img src="<?php echo base_url('assets/campaign/freepurifier/images/logo-psi2018.png')?>" style="max-width: 80px;">
	<h1>ข้อมูลการลงทะเบียนขอรับเครื่องแยกน้ำ</h1>
</div>
<div class="container-fluid mt-2">


<div class="row mb-2">
  <div class="col-lg-12">
    <a href="<?php echo base_url('campaign/'.$this->controller.'/exportToExcel')?>" class="btn btn-success pull-right"><i class="fa fa-file-excel-o"></i> ส่งออกไฟล์เป็น Excel</a>
  </div>
</div>

<div class="table-responsive">
	<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">ข้อมูลผู้ยื่นเรื่อง</th>
      
      <th scope="col">ข้อมูลสถานที่</th>
      <th scope="col">เบอร์โทรสถานที่</th>
      <th scope="col">ผู้ประสานงาน</th>
      <th scope="col">รับข้อมูลข่าวสารโครงการจาก?</th>
      <th scope="col">วัน/เวลา ที่สมัคร</th>
      <th scope="col">รายละเอียดเพิ่มเติม</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  	<?php foreach($purifier_register as $key => $row){?>
  		<tr>
  			<td><?php echo ++$key?></td>
  			<td>
          <p>
             <?php echo $row->firstname.' '.$row->lastname?> <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="<?php echo 'ที่อยู่ : '.$row->address?>" data-html="true"><i class="fa fa-info-circle"></i></a>
          </p>
          <p>
            <?php echo $row->telephone?>
          </p>
         
            
          </td>
  			<!-- <td><?php echo $row->telephone?></td>
  			<td><?php echo $row->email?></td>
  			<td>
  				<?php echo $row->address?> ต.<?php echo $row->districts->get()->district_name?> อ.<?php echo $row->amphurs->get()->amphur_name?> จ.<?php echo $row->provinces->get()->province_name?> <?php echo $row->zipcode?>
  			</td> -->
  			
        <td>
          <p>
            <?php 
            $place_address = $row->place_address; 

            // print_r($row->districts->get()->to_array());exit;
            // echo $row->districts->get()->district_name;exit;

            $place_address .= ' ต.'.$row->districts->get()->district_name;
            $place_address .= ' อ.'.$row->amphurs->get()->amphur_name;
            $place_address .= ' จ.'.$row->provinces->get()->province_name;
            $place_address .= ' '.$row->zipcode;

            ?>
            <strong>ชื่อสถานที่ : </strong><span><?php echo $row->place_name?></span>
          </p>
          <p>
            ที่อยู่ : <?php echo $place_address?>
          </p>
        </td>
        <td>
          <?php echo $row->place_telephone?>
        </td>
        <td>
          <?php echo ($row->coordinator)?$row->coordinator:'-'?>
        </td>
        <td>
          <?php 
            $decode_getfrom = json_decode($row->get_campaign_from);

            if(count($decode_getfrom) > 0){
              foreach ($decode_getfrom as $key => $value) {
                # code...
                $other_desc = ($value == 'other')?$row->get_campaign_from_other_remark:'';

                if($other_desc){
                  echo '<label class="label label-info" style="margin-right:5px;">'.textGetCampaignFrom($value).' : '.$other_desc.'</label>';
                }else{
                  echo '<label class="label label-info" style="margin-right:5px;">'.textGetCampaignFrom($value).'</label>';
                }
                // echo '<label class="label label-info" style="margin-right:5px;">'.$value.'</label>';
              }
            }
          ?>
          
        </td>
  			<td><?php echo date('d/m/Y H:i',strtotime($row->created))?></td>
  			<td>
          <div class="alert alert-info">
  				<span class="show-remark" data-registerid="<?php echo $row->id?>"><?php echo ($row->remark)?$row->remark:'<small>กรอกรายละเอียดเพิ่มเติม...</small>'?></span><span>&nbsp;&nbsp;<a href="javascript:void(0)" class="btn btn-success btn-xs" data-remark="<?php echo $row->remark?>" onclick="clickAddRemark(this)" data-registerid="<?php echo $row->id?>"><i class="fa fa-pencil"></i></a></span>
          </div>
  				
  				
  			</td>
        <td>
          <?php if(@$digest_username == 'admin'){?>
            <a href="javascript:void(0);" class="btn btn-danger" onclick="if(confirm('ต้องการลบรายการนี้ใช่หรือไม่') ==true){window.location.href='<?php echo base_url('campaign/FreeWaterPurifier/deletePurifierRegister/'.$row->id)?>'}">
              <i class="fa fa-trash"></i>
            </a>
          <?php }?>
        </td>
  		</tr>

  	<?php }?>
  </tbody>
</table>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="remarkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">รายละเอียดเพิ่มเติม</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',['name'=>'remark-form'])?>
      <div class="modal-body">
      	<?php echo form_input([
      		'type'=>'hidden',
      		'name'=>'hide_register_id'
      	])?>
      	<div class="form-group">
      		<?php echo form_textarea([
      			'name'=>'remark',
      			'class'=>'form-control',
      			'rows'=>10,
      			'placeholder'=>'กรอกรายละเอียดเพิ่มเติม...'
      		])?>
      	</div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
        <button type="button" class="btn btn-primary" onclick="saveRemark(this)">บันทึก</button>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>

