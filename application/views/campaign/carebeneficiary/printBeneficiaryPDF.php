<!DOCTYPE html>
<html>
<head>
	<title></title>

	<style>
        body{
            font-family: "thsarabun";
            font-size: 15px;
        }
        

    </style>
</head>
<body>
	<div style="width: 100%;">
		<h1 style="text-align: center;font-size: 20px;margin:0">ใบตรวจสอบเงินช่วยเหลือ</h1>
	</div>

	<div style="width: 100%;text-align: center;">
		<img src="<?php echo base_url('assets/campaign/checkbeneficiary/images/logo_psicare.png')?>" style="max-width: 150px;">
	</div>

	<div style="width: 100%;text-align: right;">
			วันที่ <?php echo date('d / m / Y')?>
	</div>



	<div style="width: 100%;margin-top: 5px;">
		<div style="width: 50%;float:left;">
			
				<strong>รหัสสมาชิก (PSI ARM) : </strong><?php echo $agent_data->AgentCode?><br>

				<strong>ชื่อ : </strong> <?php echo $agent_data->AgentName.' '.$agent_data->AgentSurName?>
					
		</div>

		<div style="width: 50%;float: left;">
				<strong>สาขา : </strong><?php echo $agent_data->BranchData->BranchName?><br>

				
		</div>
	</div>

	<div style="clear:both;"></div>

	<div style="width: 100%;">
		<strong>ประเภทบัตร : </strong><?php echo $agent_data->AgentType?>
	</div>


	<div style="background: #ddd;padding-left: 20px;margin:10px 0;">
	<div style="width: 100%;">
		&bull; ข้อมูลซื้อ - ขายย้อนหลัง <?php echo ($be_protected_type=='six_month')?'6':'12'?> เดือน (เกิน <?php echo ($be_protected_type=='six_month')?'180,000':'360,000'?> บาท)
	</div>	

	<div style="width: 100%;">

		<div style="width: 50%;float:left;">
			<strong>ตั้งแต่วันที่ :</strong> 

			<?php if($be_protected_type == 'six_month'){?>
				<?php echo $sixMonthData['start_date']?>
			<?php }else{?>
				<?php echo $oneYearData['start_date']?>
			<?php }?>	
		</div>

		<div style="width: 50%;float: left;">
			<strong>ถึงวันที่ :</strong> 

			<?php if($be_protected_type == 'six_month'){?>
				<?php echo $sixMonthData['end_date']?>
			<?php }else{?>
				<?php echo $oneYearData['end_date']?>
			<?php }?>	
		</div>
	</div>

	<div style="width: 100%;">
		<div style="width: 50%;float: left;">
			<?php $total_price = ($be_protected_type == 'six_month')?number_format($sixMonthData['sum_netprice'],2):number_format($oneYearData['sum_netprice'],2);?>
			<strong>มียอดซื้อ :</strong> <?php echo $total_price?> บาท
		</div>
		<div style="width: 50%;float: left;">
			(<?php echo num2wordsThai($total_price);?>)
		</div>
	</div>


	<!-- history of  treatment -->
	<div style="width: 100%;">
		&bull; ประวัติการเบิกค่ารักษาพยาบาลย้อนหลัง 12 เดือน
	</div>

	<div style="width: 100%;">
		<div style="width: 50%;float:left;">
			<strong>จำนวน</strong> <?php echo @$treatmentData['count_treatment']?> ครั้ง
		</div>
		<div style="width: 50%;float:left;">
			<strong>รวมเป็นเงิน</strong> <?php echo number_format(@$treatmentData['sum_treatment_cost'],2)?> บาท
		</div>
	</div>
	<!-- eof history of treatment -->
	</div>

	<div style="width: 100%;">
		&bull; ขอรับเงินช่วยเหลือกรณี
		<br>
		&nbsp;&nbsp;&nbsp;&nbsp; <img src="<?php echo base_url('assets/campaign/checkbeneficiary/images/blank-check-circle.png')?>" style="width: 20px;height: 20px;"> ค่ารักษากรณีเกิดอุบัติเหตุ (ไม่เกิน 50,000 บาท / ปี) แนบหลักฐาน อย่างละ 1 ชุด ดังนี้
		<ul style="margin:0 0 0 100px;padding: 0;position: relative;">
			<li>
				ใบเสร็จรับเงินค่ารักษาพยาบาลตัวจริง ระบุชื่อผู้ขอรับเงินช่วยเหลือ *<br>
				(ยื่นใบเสร็จภายใน 14 วัน นับจากวันที่ลงในใบเสร็จ)
			</li>
			<li>สำเนาบัตรประจำตัวประชาชนของผู้รับเงินช่วยเหลือ *</li>
			<li>สำเนาหน้าสมุดบัญชีธนาคารของผู้รับเงินช่วยเหลือ *</li>
			<li>ใบรับรองแพทย์ *</li>
		</ul>

		&nbsp;&nbsp;&nbsp;&nbsp; <img src="<?php echo base_url('assets/campaign/checkbeneficiary/images/blank-check-circle.png')?>" style="width: 20px;height: 20px;"> 
		เสียชีวิตจากอุบัติเหตุ แนบหลักฐาน อย่างละ 1 ชุด ดังนี้
		<ul style="margin:0 0 0 100px;padding: 0;position: relative;">
			<li>
				สำเนาใบมรณะบัตร (ยื่นใบเสร็จภายใน 7 วัน นับจากวันที่ระบุในใบมรณะบัตร) *
			</li>
			<li>สำเนาบัตรประจำตัวประชาชนของทายาทที่จะรับเงินช่วยเหลือ (เป็นคนเดียวกับที่ระบุใน APP PSIARM) *</li>
			<li>สำเนาหน้าสมุดบัญชีธนาคารของทายาทที่จะรับเงินช่วยเหลือ *</li>
		</ul>
	</div>

	<br>
	<div style="width: 100%;">
		<div style="width: 50%;float: left;">
			ยอดที่ขอรับเงินช่วยเหลือ...........................................................................บาท
		</div>
		<div style="width: 50%;float: left;">
			(............................................................................................................ บาทถ้วน)
		</div>
	</div>

	<div style="width: 100%;margin-top: 5px;">
		<div style="width: 50%;float:left;text-align: center;">
			ผู้ขอรับเงินช่วยเหลือ <br><br>
			..............................................................................<br><br>
			(............................................................................................)<br><br>
			วันที่ ..................... / ..................... / .....................
		</div>

		<div style="width: 50%;float:left;text-align: center;">
			ผู้จัดการสาขา PSI นำเสนอ <br><br>
			..............................................................................<br><br>
			(............................................................................................)<br><br>
			วันที่ ..................... / ..................... / .....................
		</div>

	</div>

	<div style="width: 100%; border: 1px solid #ddd;margin-top: 10px;border-radius: 20px;padding: 20px;">
		<div style="width: 50%;float: left;">
			ยอดเงินที่อนุมัติช่วยเหลือ.....................................................................บาท
		</div>
		<div style="width: 50%;float: left;">
			&nbsp;&nbsp;&nbsp;&nbsp;(....................................................................................................... บาทถ้วน)
		</div>

		<div style="width: 100%;margin-top: 10px;">
			<div style="width: 50%;float: left;text-align: center;">
				<img src="<?php echo base_url('assets/campaign/checkbeneficiary/images/blank-check-circle.png')?>" style="width: 20px;height: 20px;"> &nbsp;อนุมัติ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url('assets/campaign/checkbeneficiary/images/blank-check-circle.png')?>" style="width: 20px;height: 20px;">&nbsp;ไม่อนุมัติ <br>
				ประธานเจ้าหน้าที่สายบัญชีและการเงิน <br><br>
				..............................................................<br><br>
				(.........................................................................)<br><br>
				วันที่ ..................... / ..................... / .....................

			</div>
			<div style="width: 50%;float: left;text-align: center;">
				<img src="<?php echo base_url('assets/campaign/checkbeneficiary/images/blank-check-circle.png')?>" style="width: 20px;height: 20px;"> &nbsp;อนุมัติ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url('assets/campaign/checkbeneficiary/images/blank-check-circle.png')?>" style="width: 20px;height: 20px;">&nbsp;ไม่อนุมัติ <br>
				ประธานเจ้าหน้าที่สายงานขาย <br><br>
				..............................................................<br><br>
				(.........................................................................)<br><br>
				วันที่ ..................... / ..................... / .....................
			</div>
		</div>
	</div>





</body>
</html>