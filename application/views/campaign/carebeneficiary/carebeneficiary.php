<!DOCTYPE html>
<html>
    
	<head>
	    <meta charset="utf-8">
	    <meta name="author" content="PSICARE">
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="เงื่อนไขที่ช่างในระบบ PSIARM จะได้รับคุ้มครอง">
		<meta name="keywords" content="PSI, Care, Arm, ช่างมาตรฐาน, เงื่อนไขการคุ้มตรอง, ประกันอุบัติเหต, ประกันชีวิต">
	
		<!-- Custom CSS -->
		<link href="<?php echo base_url('assets/campaign/main.css')?>" rel="stylesheet">
		
		<!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Kanit:100,200,300,400,700|Kanit:100,200,300,400,600" rel="stylesheet">
        
        <!-- Boostrap 4 -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<title>เงื่อนไขที่ช่างในระบบ PSIARM ที่จะได้รับการคุ้มครอง</title>
		
	</head>
	
	
	
	<body class="bg" style="font-family: 'Kanit', serif; -webkit-font-smoothing: antialiased;">
    
    
    <!--Page Content -->
	        <div class="container-fluid">
	        
    <div class="p-3 my-3 bg-blue rounded">

    <img class="width-150 img-fluid" src="<?php echo base_url('assets/campaign/images/logo_psicare200x70px.svg')?>" alt="logo_psicare">

  </div>
   
   
   
    <div> 
      <h5 class="text-blue-50">โครงการมอบความช่วยเหลือแก่ช่างมาตรฐาน PSI</h5>
   </div>	
   
   <!-- ++++++++++++++++++++++++++++++++++++++++++++++ card content 01 ++++++++++++++++++++++++++++++++++++++++++++++ -->
   

<?php if(!$beneficiaryData){?>
   <div class="my-3 p-3 card special-card shadow-sm">
    <h6 class="border-bottom pb-2 mb-0">ชื่อผู้รับผลประโยชน์ (คุณสามารถลงชื่อผู้รับผลประโยชน์ได้คนเดียวเท่านั้น)</h6>
    
    <div class="media text-muted pt-3">
        
        
   <!-- ++++++++++++++++++++++++++++++++++++++++++++++ form ++++++++++++++++++++++++++++++++++++++++++++++  -->
   
<?php echo form_open($this->uri->uri_string().'?agentcode='.$this->input->get('agentcode'),['name'=>'beneficiary-form'])?>
    <p>
  ชื่อ:<br>
  <input type="text" name="firstname" placeholder="ใส่ชื่อผู้รับผลประโยชน์" size="50" maxlength="1000" class="form-control" required="required" autocomplete="off">
    </p>

    <p>
  นามสกุล:<br>
  <input type="text" name="lastname" placeholder="ใส่นามสกุลผู้รับผลประโยชน์" size="50" maxlength="1000" class="form-control" required="required" autocomplete="off">
  <br>
  </p>
  
  <p>
  เกียวข้องเป็น:<br>
  <input type="text" name="relationship" placeholder="ใส่ความเกี่ยวข้องของผู้รับผลประโยชน์" size="50" maxlength="1000" class="form-control" required="required" autocomplete="off">
  <br><br>
  </p>
  
  <div>
  <input type="submit" class="btn btn-info" style="width: 100px; padding: 5px;"  value="ยืนยันข้อมูล">
  </div>
<?php echo form_close()?>
    </div>
    </div>

<?php }else{?>
<div class="alert alert-success">
  <h4><u>ข้อมูลผู้รับผลประโยชน์</u></h4>
  <?php //print_r($beneficiaryData)?>
  <strong> ชื่อ-สกุล:</strong> <?php echo $beneficiaryData->beneficiary_firstname.' '.$beneficiaryData->beneficiary_lastname?>
  <br>
  <strong>เกี่ยวข้องเป็น : </strong> <?php echo $beneficiaryData->beneficiary_relationship?>
</div>
<?php }?>
    
   <!-- ++++++++++++++++++++++++++++++++++++++++++++++ card content 02 ++++++++++++++++++++++++++++++++++++++++++++++ -->
   
    <div class="my-3 p-3 card special-card shadow-sm">
    <h6 class="border-bottom pb-2 mb-0">คุณสมบัติ / เงื่อนไข</h6>
    
    <div class="media text-muted pt-3">
      <svg class="mr-3 rounded" width="15" height="15" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 20x20"><title>Placeholder</title><rect width="100%" height="100%" fill="#009EE6"/><text x="35%" y="50%" fill="#FFFFFF" dy=".3em">1</text></svg>
          <p class="media-body pb-3 mb-0 border-bottom">
       เป็นช่างมาตรฐาน PSI มีชื่อตามฐานข้อมูลสมาชิก ARM
          </p>
    </div>
      
    <div class="media text-muted pt-3">
      <svg class="mr-3 rounded" width="15" height="15" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 10x20"><title>Placeholder</title><rect width="100%" height="100%" fill="#009EE6"/><text x="35%" y="50%" fill="#FFFFFF" dy=".3em">2</text></svg>
          <p class="media-body pb-3 mb-0 border-bottom">
       เฉพาะบัตรที่มีวงเงินเครดิตเท่าน้ัน (CP, SV, GD, Bl, PT)
          </p>
    </div>
    
    <div class="media text-muted pt-3">
      <svg class="mr-3 rounded" width="15" height="15" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 10x20"><title>Placeholder</title><rect width="100%" height="100%" fill="#009EE6"/><text x="35%" y="50%" fill="#FFFFFF" dy=".3em">3</text></svg>
          <p class="media-body pb-3 mb-0">
       มียอดการซื้อขาย 360,000 บาท / ปี (อัพเดตข้อมูลย้อนหลัง 180,000 บาท / 6 เดือน ในแอพพลิเคชัน PSI ARM)
          </p>
    </div>  
    
    </div>
    
    
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++ card content 03 ++++++++++++++++++++++++++++++++++++++++++++++ -->
        
    <div class="my-3 p-3 card special-card shadow-sm table-responsive">
    <h6 class="border-bottom pb-2 mb-0">เงินช่วยเหลือจาก PSI</h6>
    
    
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++ table ++++++++++++++++++++++++++++++++++++++++++++++ -->
        
    <table class="table">

    <!-- ++++++++++++++++++++++++++++++++++++++++++++++ row title ++++++++++++++++++++++++++++++++++++++++++++++ -->

    <tr class="border-bottom font-italic height-35">
        
        <td scope="row">
        เกิดอุบัติเหตุระหว่างทำงานที่เกี่ยวกับสินค้า PSI
        </td>

        <td>
        วงเงินไม่เกิน (บาท)
        </td>
        
        <td>
        รายละเอียดเพิ่มเติม
         </td>
         
         </tr>

    <!-- ++++++++++++++++++++++++++++++++++++++++++++++ row content 01 ++++++++++++++++++++++++++++++++++++++++++++++ -->
        
    <tr class="text-muted border-bottom height-35">
        
        <td scope="row">
        ค่ารักษาพยาบาล
        </td>
        
        <td>
        50,000
        </td>
        
        <td>
        จ่ายตามบิลจริง ไม่เกิน 50,000 บาท / ปี
         </td>
         
    </tr> 
 
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++ row content 02 ++++++++++++++++++++++++++++++++++++++++++++++ -->
 
    <tr class="text-muted border-bottom height-35">
        
        <td scope="row">
        มอบเงินช่วยเหลือครอบครัวสำหรับช่างที่เสียชีวิต
        </td>
        
        <td>
        100,000
        </td>
        
        <td>
        โอนเงินเข้าบัญชีที่ผูกกับบัตร ARM
         </td>
         
    </tr> 
        
    </table>
    
    </div>

   
   <!-- ++++++++++++++++++++++++++++++++++++++++++++++ card etc ++++++++++++++++++++++++++++++++++++++++++++++ -->
   
    <div class="my-3 p-3">
    <hr>
    <h6 class="mb-0 text-red-50">**หมายเหตุ</h6>
    
    <div class="media text-muted pt-3">
      <svg class="mr-3 rounded" width="15" height="15" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 20x20"><title>Placeholder</title><rect width="100%" height="100%" fill="#B28080"/><text x="35%" y="50%" fill="#FFFFFF" dy=".3em">1</text></svg>
          <p class="media-body mb-0">
      ทางบริษัทฯ สามารถเปลี่ยนแปลงรายละเอียดเงื่อนไขโดยไม่ต้องแจ้งให้ทราบล่วงหน้า
          </p>
    </div>
      
    <div class="media text-muted pt-3">
      <svg class="mr-3 rounded" width="15" height="15" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 10x20"><title>Placeholder</title><rect width="100%" height="100%" fill="#B28080"/><text x="35%" y="50%" fill="#FFFFFF" dy=".3em">2</text></svg>
          <p class="media-body mb-0">
       ไม่สามารถนำสวัสดิการต่างๆ ไปแลกเปลี่ยนเป็นเงินหรือสินค้าได้
          </p>
    </div>
    
    <div class="media text-muted pt-3">
      <svg class="mr-3 rounded" width="15" height="15" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 10x20"><title>Placeholder</title><rect width="100%" height="100%" fill="#B28080"/><text x="35%" y="50%" fill="#FFFFFF" dy=".3em">3</text></svg>
          <p class="media-body mb-0">
      สำหรับช่างที่ไม่ได้เป็นสมาชิก หรือโดนทางบริษัทฯ แบล็กลิสต์ จะไม่สามารถได้รับเงินช่วยเหลือจาก PSI ในทุกกรณี
           </p>
    </div>  
    
    </div>
    
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++ end content ++++++++++++++++++++++++++++++++++++++++++++++ -->

                </div>
                
    <!-- ++++++++++++++++++++++++++++++++++++++++++++++ end all content ++++++++++++++++++++++++++++++++++++++++++++++ -->            
                
    <div class="mb-0 text-black-50 p-3" style="text-align: center; vertical-align: bottom;">
    
        <footer>
            
       <hr> 
       
     <div>Copyright &copy; All right reserved 2019 PSI CORPORATION</div>
  
        </footer>
        
    </div>
    
        
	</body>
	
</html>