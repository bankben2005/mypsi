<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PSI CARE - ตรวจสอบเงินช่วยเหลือ</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/campaign/checkbeneficiary/vendor/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/backend/vendors/font-awesome/css/font-awesome.min.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-daterangepicker/daterangepicker.css')?>">

</head>

<body>

  <input type="hidden" name="base_url" value="<?php echo base_url()?>">
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-info static-top">
    <div class="container">
      <a class="navbar-brand" href="<?php echo base_url('campaign/'.$this->controller.'/check')?>">PSI CARE - ตรวจสอบเงินช่วยเหลือ</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>


<!--       <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
        </ul>
      </div> -->


    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-10 offset-lg-1">
        	<div class="card mt-2">
        		<div class="card-header bg-white text-center">

        			<div class="row">
        			<div class="col-lg-3">
        				<img src="<?php echo base_url('assets/campaign/checkbeneficiary/images/logo_psicare.png')?>">
        			</div>
        			

        			<div class="col-lg-3">
        				<?php echo form_open('',['name'=>'search-criteria','onsubmit'=>'submitAgentSearch(event)'])?>
        					
        						<div class="form-group text-left">
        							<label><strong>กรอกรหัสช่าง : </strong></label>
        							<?php echo form_input([
        								'name'=>'agent_code',
        								'class'=>'form-control',
        								'required'=>'required',
        								'value'=>@base64_decode($this->input->get('agent_code'))
        							])?>
        						</div>

        						

        					
        				</div>
        				<div class="col-lg-3">
        					<div class="form-group">
        						<label>&nbsp;</label>
        							<?php echo form_button([
        								'type'=>'submit',
        								'class'=>'btn btn-primary btn-block',
        								'content'=>'<i class="fa fa-search"></i> ตรวจสอบข้อมูล'
        							])?>

        							
        						</div>

        				</div>
        				<div class="col-lg-3">
        					<div class="form-group">
        						<label>&nbsp;</label>
        						<?php echo form_button([
        								'type'=>'button',
        								'class'=>'btn btn-danger btn-block',
        								'content'=>'รีเซ็ต',
        								'onclick'=>"resetAgentSearch(this)"
        							])?>
        					</div>
        				</div>	

        				<?php echo form_close()?>
        			</div>

        		</div>
        		<div class="card-body">
        				<?php if(!$has_agent && isset($_GET['agent_code'])){?>
        					<div class="alert alert-warning text-center">
        						<strong>ไม่พบช่างดังกล่าว</strong>
        					</div>
        				<?php }else if(!$has_agent && !isset($_GET['agent_code'])){?>

        				<?php }else{?>
        					<div class="row">
        						<div class="col-lg-6">
        							<p>
        								<strong>รหัสสมาชิก PSI ARM : </strong> <?php echo $agent_data->AgentCode?>
        							</p>

        							<p>
        								<strong>ชื่อ-สกุล : </strong><?php echo $agent_data->AgentName.' '.$agent_data->AgentSurName?>
        							</p>


        						</div>
        						<div class="col-lg-6">
        							<p>
        								<strong>สาขา : </strong> <?php echo $agent_data->BranchData->BranchName?>
        							</p>
        							<p>
        								<strong>ประเภทบัตร : </strong> <?php echo $agent_data->AgentType?>
        							</p>
        						</div>
        					</div>

        					<hr>
        					<div class="row">
        						<div class="col-lg-6">
        							<p>
        								<strong>วันที่ต้องการตรวจสอบ (รับเงินช่วยเหลือ) : </strong>
        								<?php echo form_input([
        									'name'=>'check_date',
        									'class'=>'form-control',
        									'value'=>''
        								])?>
        							</p>
        						</div>
        						<div class="col-lg-6">

                      <?php if($this->input->get('check_type') && $this->input->get('check_type') == 'accountant'){?>
                      <p>
                        <strong>&nbsp;</strong>
                        <a href="javascript:void(0);" class="btn btn-primary btn-block" onclick="openTreatmentForm(this)"><i class="fa fa-plus"></i> เพิ่มประวัติการเบิกค่ารักษาพยาบาล</a>
                      </p> 

                      <?php }?>
        							

        						</div>
        					</div>

        					<div class="row">
        						<div class="col-lg-12">
        							<div id="show-beneficiary" class="alert alert-info">
        								<div class="show-loading col-lg-12 text-center">
        									<i class="fa fa-spin fa-spinner"></i>
        								</div>
        							</div>
        						</div>
        					</div>

        				<?php }?>
        		</div>
        	</div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url('assets/js/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/campaign/checkbeneficiary/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

  <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-daterangepicker/moment.js')?>"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
  <script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



  <script type="text/javascript">
  	
  	function submitAgentSearch(e){
  		e.preventDefault();
  		var search_agent = $('input[name="agent_code"]').val();


  		var current_url = location.protocol + '//' + location.host + location.pathname;
  		var agent_encode = btoa(search_agent); 

      var objQueryString = {'agent_code':agent_encode};


  		//console.log(current_url);

      var queryString = parseQueryString(); 
      // console.log(queryString);

      if(queryString.hasOwnProperty('check_type')){
        // console.log('i am here');
          objQueryString.check_type = queryString.check_type
      }

      // console.log(objQueryString);

      // var test = {
      //   'test':'test1',
      //   'test2':'test'
      // };

      var queryString = new URLSearchParams(objQueryString).toString();

      // console.log('tettstring',teststring);
      // console.log(queryString);

  		window.location.href = current_url+'?'+queryString;
  		

  	}

    function parseQueryString () {

          var parsedParameters = {},

            uriParameters = location.search.substr(1).split('&');



          for (var i = 0; i < uriParameters.length; i++) {

            var parameter = uriParameters[i].split('=');

            parsedParameters[parameter[0]] = decodeURIComponent(parameter[1]);

          }



          return parsedParameters;

    }

  	function resetAgentSearch(){
  		var current_url = location.protocol + '//' + location.host + location.pathname;
  		window.location.href = current_url;
  	}


  	$(document).ready(function(){
  		$('input[name="check_date"]').daterangepicker({
		    singleDatePicker: true,
		    showDropdowns: true,
		    minYear: 1901,
		    maxDate:moment(new Date()),
		    locale: {
            	format: 'DD-MM-YYYY'
        	}
		}, function(start, end, label) {

			//console.log(start.format('DD-MM-YYYY'));
		    // var years = moment().diff(start, 'years');
		    // alert("You are " + years + " years old!");
		    calculateBeneficiary(start.format('DD-MM-YYYY'));
		});

      initialTreatmentDate();
  		calculateBeneficiary();


  	});	

  	function calculateBeneficiary(check_date = null){

  		if(!check_date){
  			check_date = $('input[name="check_date"]').val();
  		}
  		
  		var base_url = $('input[name="base_url"]').val();
  		var post_data = {
  			'check_date':check_date,
  			'agent_code':$('input[name="agent_code"]').val()

  		};


					$.ajax({
                        url: base_url+"campaign/CareBeneficiary/ajaxCalculateBeneficiary",
                        type: "post",
                        data: post_data ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                        	var loading_html = '<div class="show-loading col-lg-12 text-center">';
                        	loading_html += '<i class="fa fa-spin fa-spinner"></i>';
                        	loading_html += '</div>';
                        	$('div#show-beneficiary').html('').html(loading_html);
                        	
                        },	
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            if(response.status){
                            	$('div#show-beneficiary').find('.show-loading').hide();
                            	$('div#show-beneficiary').html(response.view);
                            	
                            }
                            
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });

  		//console.log(post_data);
  	}

  	function printBeneficiary(element){
  		var element = $(element);

  		var data = {
  			'check_date':$('input[name="check_date"]').val(),
  			'agent_code':$('input[name="agent_code"]').val()

  		};

  		var form_html = '<form action="printBeneficiary" method="post">';
  		form_html += '<input type="hidden" name="check_date" value ="'+data.check_date+'">';
  		form_html += '<input type="hidden" name="agent_code" value = "'+data.agent_code+'">';
  		form_html += '</form>';

  		$(form_html).appendTo('body').submit();



  	}

    function openTreatmentForm(element){
      var element = $(element);
      console.log(element);
      $('#treatmentModal').modal('toggle');
    }

    function initialTreatmentDate(){
      $('input[name="treatment_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxDate:moment(new Date()),
        locale: {
              format: 'DD-MM-YYYY'
          }
      }, function(start, end, label) {

        //console.log(start.format('DD-MM-YYYY'));
          // var years = moment().diff(start, 'years');
          // alert("You are " + years + " years old!");
          //calculateBeneficiary(start.format('DD-MM-YYYY'));
      });
    }

    function saveTreatmentForm(e){
      e.preventDefault();
      var form_data = $('form[name="treatment-form"]').serialize();
      var base_url = $('input[name="base_url"]').val();

      form_data += '&agent_code='+$('input[name="agent_code"]').val();

                  $.ajax({
                        url: base_url+"campaign/CareBeneficiary/ajaxSaveTreatmentForm",
                        type: "post",
                        data: form_data ,
                        async:true,
                        dataType:'json',
                        beforeSend: function() {
                          
                          
                        },  
                        success: function (response) {
                            console.log('====== response =======');
                            console.log(response);
                            $('#treatmentModal').modal('toggle');
                            if(response.status){
                              swal({
                                title: "สำเร็จ",
                                text: "เพิ่มประวัติการเบิกค่ารักษาพยาบาลไปยังระบบแล้ว",
                                icon: "success",
                              }).then(function(){
                                  calculateBeneficiary();
                              });
                            }
                            
                            
                        },
                        error: function (request, status, error) {
                                    console.log(request.responseText);
                        }
                        


                    });

    }
  </script>


  <!-- Modal -->
  <div class="modal fade" id="treatmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">เพิ่มประวัติการเบิกค่ารักษาพยาบาล</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php echo form_open('',['name'=>'treatment-form','onsubmit'=>'saveTreatmentForm(event)'])?>
        <div class="modal-body">
          <div class="form-group">
            <label><strong>วันที่เบิกค่ารักษาพยาบาล :</strong></label>
            <?php echo form_input([
              'name'=>'treatment_date',
              'class'=>'form-control'
            ])?>
          </div>

          <div class="form-group">
            <label><strong>จำนวนเงินที่ขอเบิก :</strong></label>
            <div class="input-group mb-3">
              <input type="number" min="1" name="treatment_cost" class="form-control"  aria-label="จำนวนเงินที่ขอเบิก" aria-describedby="basic-addon2" required="required">

              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">บาท</span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label><strong>เพิ่มข้อมูลโดย (ชื่อ-สกุล)</strong></label>
            <?php echo form_input([
              'name'=>'fill_data_by',
              'class'=>'form-control',
              'required'=>'required'
            ])?>
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
          <?php echo form_button([
            'type'=>'submit',
            'class'=>'btn btn-success',
            'content'=>'บันทึกข้อมูล'
          ])?>
        </div>
        <?php echo form_close()?>
      </div>
    </div>
  </div>


</body>

</html>
