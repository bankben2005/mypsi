
<h4><u>ข้อมูลยอดซื้อย้อนหลัง 6 เดือน</u></h4>
<div class="row">
	<div class="col-lg-6">
		<p><strong>ข้อมูลจากวันที่ : </strong> <?php echo $sixMonthData['start_date']?> <strong>ถึงวันที่ </strong> <?php echo $sixMonthData['end_date']?></p>

		<p>
			<strong>ยอดซื้อที่ได้รับเงินช่วยเหลือ : </strong> 180,000 บาท
		</p>

		<p>
			<strong>ยอดซื้อทั้งหมด (ช่าง) : </strong> <?php echo number_format($sixMonthData['sum_netprice'],2)?>
		</p>

		<p>
			<strong>สถานะรับเงินช่วยเหลือ : </strong>
			<?php if($sixMonthData['be_protected']){?>
				<span class="badge badge-success">ได้รับ</span>
			<?php }else{?>
				<span class="badge badge-danger">ไม่ได้รับ</span>
			<?php }?>
		</p>
	</div>
	<div class="col-lg-6">

	</div>
</div>


<h4><u>ข้อมูลยอดซื้อย้อนหลัง 12 เดือน</u></h4>
<div class="row">
	<div class="col-lg-6">
		<p><strong>ข้อมูลจากวันที่ : </strong> <?php echo $oneYearData['start_date']?> <strong>ถึงวันที่ </strong> <?php echo $oneYearData['end_date']?></p>

		<p>
			<strong>ยอดซื้อที่ได้รับเงินช่วยเหลือ : </strong> 360,000 บาท
		</p>

		<p>
			<strong>ยอดซื้อทั้งหมด (ช่าง) : </strong> <?php echo number_format($oneYearData['sum_netprice'],2)?>
		</p>

		<p>
			<strong>สถานะรับเงินช่วยเหลือ : </strong>
			<?php if($oneYearData['be_protected']){?>
				<span class="badge badge-success">ได้รับ</span>
			<?php }else{?>
				<span class="badge badge-danger">ไม่ได้รับ</span>
			<?php }?>
		</p>
	</div>
	<div class="col-lg-6">

	</div>
</div>

<hr>

<div class="row">
	<div class="col-lg-12 text-center"><h4>ประวัติการเบิกค่ารักษาพยาบาล</h4></div>

	<?php if($treatmentData->num_rows() <= 0){?>
		<div class="col-lg-12 text-center alert alert-warning">ไม่พบประวัติการเบิกค่ารักษาพยาบาล</div>
	<?php }else{ ?>

	<table class="table">
		<thead>
			<tr>
				<th>วันที่เบิกค่ารักษาพยาบาล</th>
				<th style="text-align: center;">จำนวนเงินที่เบิกค่ารักษาพยาบาล</th>
			</tr>
		</thead>
		<tbody>
			<?php $total_treatment = 0; 
			foreach($treatmentData->result() as $key => $row){?>
				<tr>
					<td><?php echo date('d/m/Y',strtotime($row->TreatmentDate))?></td>
					<td align="center"><?php echo number_format($row->TreatmentCost,2)?></td>
				</tr>

			<?php $total_treatment += $row->TreatmentCost; }?>
		</tbody>
		<tfoot>
			<tr>
				<td align="right"><strong>รวม : </strong></td>
				<td align="center"><?php echo number_format($total_treatment,2)?> บาท</td>
			</tr>
		</tfoot>
	</table>

	<?php }?>


</div>
<hr>

<?php if($sixMonthData['be_protected'] || $oneYearData['be_protected']){?>
<div class="row">
	<div class="col-lg-12">
		<a href="javascript:void(0)" class="btn btn-success btn-block" onclick="printBeneficiary(this)"><i class="fa fa-print"></i> พิมพ์แบบฟอร์มขอรับความช่วยเหลือ</a> 
	</div>
</div>
<?php }?>

<?php if(ENVIRONMENT == 'development'){?>
<div class="row">
	<div class="col-lg-12">
		<a href="javascript:void(0)" class="btn btn-success btn-block" onclick="printBeneficiary(this)"><i class="fa fa-print"></i> พิมพ์แบบฟอร์มขอรับความช่วยเหลือ</a> 
	</div>
</div>
<?php }?>