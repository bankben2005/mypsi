


<!-- <form class="form-horizontal" id="btu_form" name="btu_form"> -->
    
<!-- start form primary register -->

<div>
  <h4>ใส่จำนวนผลิตภัณฑ์ที่ต้องการสั่งจอง</h4>
  </div>
   <br>

<?php echo form_open('',array('id'=>'form-orderproduct','class'=>'form-horizontal'))?>
<?php echo form_input(array('type'=>'hidden','name'=>'agentcode','value'=>($this->input->get('agentcode'))?$this->input->get('agentcode'):''));?>


<?php foreach($productavailable_list as $key => $row){?>
<!-- product 01 -->

<div class="row">
<div class="col-md-5">
  <img src="<?php echo 'http://apifixit.psisat.com/'.$row['data_product']->ProductImage?>" class="img-responsive">
  <h5><?php echo $row['data_product']->MPNAME.' ('.$row['data_product']->MPDESCRIPTION.')';?>:</h5>

</div>
      
  <div class="col-md-4">
    ราคา (สัมนา): <strong><?php echo number_format($row['data_product']->PriceB,2)?></strong> บาท / <?php echo $row['data_product']->UOM?>
  </div>
  <div class="col-sm-3">
      <input type="number" name="<?php echo $row['data_product']->MPCODE;?>"  onfocus="this.value=''" min="0" placeholder="ใส่จำนวนสินค้า" class="form-control form-control-lg">
  </div>

  <?php if($key < (count($productavailable_list) - 1)){?>
  <div class="clearfix"></div>
  <hr>
  <?php }?>
</div>

  <!-- end product 01 -->
<?php  }?>
<br>
 
    <div class="form-group">
      <div class="col-sm-12">
          <p></p><p>
          </p>
     <h5 class="text-center" style="font-color: red">*หลังจากที่ใส่จำนวนผลิตภัณฑ์เรียบร้อยแล้วให้กดปุ่มสั่งจองผลิตภัณฑ์</h5> 
  </div>
  </div>
  
<br>

  <div class="form-group">
  <div class="col-sm-12">
      <button type="submit" class="btn btn-primary btn-block btn-lg">สั่งจองผลิตภัณฑ์</button>
  </div>
  </div>
 
<!-- end of primary register -->
<?php echo form_close(); ?>


<!-- Modal -->
<div id="modalImage" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

