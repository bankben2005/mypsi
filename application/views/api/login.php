<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>TRAVELIGO FLIGHT API</title>
    
    
    
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>

    <style type="text/css">
        body {
  background: #eee !important;
}

.wrapper {
  margin-top: 80px;
  margin-bottom: 80px;
}

.form-signin {
  max-width: 380px;
  padding: 15px 35px 45px;
  margin: 0 auto;
  background-color: #fff;
  border: 1px solid rgba(0, 0, 0, 0.1);
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 30px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  font-size: 16px;
  height: auto;
  padding: 10px;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="text"] {
  margin-bottom: -1px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 20px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
        
    </style>

    
    
    
  </head>

  <body>

      <div class="wrapper">
    
          <?php echo form_open('',array('class'=>'form-signin'));?>
                <h2 class="form-signin-heading">Please login</h2>
                <?php echo form_input(array('class'=>'form-control','name'=>'username','placeholder'=>'Username','required'=>'required','autofocus'=>'autofocus'));?>
                <?php echo form_password('password', '', 'class="form-control" placeholder="Password" required="required"');?>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>   
          <?php echo form_close();?>
<!--    <form class="form-signin">       
      <h2 class="form-signin-heading">Please login</h2>
      <input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="" />
      <input type="password" class="form-control" name="password" placeholder="Password" required=""/>      
      <label class="checkbox">
        <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe"> Remember me
      </label>
      
    </form>-->
          
          
  </div>
    
    
    
    
    
  </body>
</html>
