<div class="panel panel-default"">
                    <div class="panel-heading">
                        RegisterProduct
                    </div>
                    <div class="panel-body">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Product</a></li>
                        <li class="breadcrumb-item active">Register Product</li>
                        
                      </ol>

                      <div class="row">
                                <div class="col-lg-12 col-md-12">
                                                                                  
                                    <div class="alert alert-warning" role="alert">
                                                                            
                                        <div class="bs-callout bs-callout-warning" id="callout-btn-group-anchor-btn"> 
                                          <h4> <i class="fa fa-hand-o-down"></i> Example RegisterProduct URL</h4> 
                                            <ul class="media-list">
                                                <li class="media">
                                                    <div class="media-left">
                                                                                       
                                                     </div>
                                                     <div class="media-body">
                                                        <h5 class="media-heading"><strong>GET</strong> :  /api/PsiCare/RegisterProduct?request= <label class=label-info>{{url_encode}}</label> <i class="fa fa-arrows-h"></i> <a href='<?php //echo $exampleRequestOneway?>' target="_blank">example</a></h5>
                                                        <!-- <h5 class="media-heading"><strong>GET</strong> : /api/PsiCare/RegisterProduct/format/xml?request=<label class="label-info">{{url_encode}}</label> <i class="fa fa-arrows-h"></i> <a href="<?php //echo $exampleRequestOnewayByFormat;?>" target="_blank">example</a></h5> -->
                                                                                          
                                                        <!-- <p>&nbsp;</p> -->
                                                        <h5 class="media-heading"><strong>POST</strong> : /api/PsiCare/RegisterProduct</h5>
                                                        <!-- <h5 class="media-heading"><strong>POST</strong> : /api/PsiCare/RegisterProduct/format/json</h5> -->
                                                      </div>
                                                   </li>
                                            </ul>
                                                                            
                                            </div>
                                                <div class="alert alert-success">
                                                                               ( <strong>any format in</strong> :  json,csv,xml,html,php,serialize )
                                               </div>
                                                                        
                                           </div>
                                                                                                      
                                    </div>
                                                                    
                            </div>
                            <h4>Example request for POST data</h4>
                                                                    <?php print("<pre>".print_r(array('aa'=>'aa'),true)."</pre>");?>

                    </div>
</div>