<?php //echo $this->router->fetch_class().'<br>'.$this->router->fetch_method();?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PSIFIXIT API</title>

    <?php echo $this->template->stylesheet;?>
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">PSI FIXIT API</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
         
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
               <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">PRODUCT <span class="menu-ico-collapse pull-right"><i class="fa fa-chevron-down"></i></span></a>
                </h4>
              </div>
              <div id="collapseFive" class="panel-collapse collapse in">
                <div class="list-group">
                    <a href="<?php echo base_url();?>" class="list-group-item <?php echo ($this->router->fetch_method() == "exampleRegisterProduct" || ($this->router->fetch_class() == "ApiManual" && $this->router->fetch_method() == "index"))?"active":""?>">
                    <h4 class="list-group-item-heading">RegisterProduct</h4>
                  </a>
                </div>
                <div class="list-group">
                    <a href="<?php echo base_url().$this->router->fetch_class().'/exampleFlightBooking'?>" class="list-group-item">
                    <h4 class="list-group-item-heading">FlightBooking</h4>
                  </a>
                </div>
                
              </div>
            
            </div>

            <div class="col-md-9">
                <?php echo $this->template->content;?>
                <div class="panel panel-default" style="display: none;">
                    <div class="panel-heading">
                        Flight API
                    </div>
                    <div class="panel-body">
                        <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Flight</a></li>
                        <li class="breadcrumb-item active">GetFlightPrice</li>
                        
                      </ol>
                        
                        <div id="exTab2">   
                                        <ul class="nav nav-tabs">
            <li class="active">
                                                        <a  href="#1" data-toggle="tab">Oneway</a>
            </li>
            <li><a href="#2" data-toggle="tab">RoundTrip</a>
            </li>
<!--            <li><a href="#3" data-toggle="tab">Solution</a>
            </li>-->
        </ul>

            <div class="tab-content ">
              <div class="tab-pane active" id="1">
                                                        <p>&nbsp;</p>
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12">
                                                                                  
                                                                        <div class="alert alert-warning" role="alert">
                                                                            
                                                                            <div class="bs-callout bs-callout-warning" id="callout-btn-group-anchor-btn"> 
                                                                                <h4> <i class="fa fa-hand-o-down"></i> Example Oneway URL</h4> 
                                                                                <ul class="media-list">
                                                                                    <li class="media">
                                                                                      <div class="media-left">
                                                                                       
                                                                                      </div>
                                                                                      <div class="media-body">
                                                                                          <h5 class="media-heading"><strong>GET</strong> :  /flight/ThaiSmile/GetFares?request= <label class=label-info>{{url_encode}}</label> <i class="fa fa-arrows-h"></i> <a href='<?php echo $exampleRequestOneway?>' target="_blank">example</a></h5>
                                                                                          <h5 class="media-heading"><strong>GET</strong> : /flight/ThaiSmile/GetFares/format/xml?request=<label class="label-info">{{url_encode}}</label> <i class="fa fa-arrows-h"></i> <a href="<?php echo $exampleRequestOnewayByFormat;?>" target="_blank">example</a></h5>
                                                                                          
                                                                                          <p>&nbsp;</p>
                                                                                          <h5 class="media-heading"><strong>POST</strong> : /flight/ThaiSmile/GetFares</h5>
                                                                                          <h5 class="media-heading"><strong>POST</strong> : /flight/ThaiSmile/GetFares/format/json</h5>
                                                                                      </div>
                                                                                    </li>
                                                                                  </ul>
                                                                            
                                                                            </div>
                                                                            <div class="alert alert-success">
                                                                               ( <strong>any format in</strong> :  json,csv,xml,html,php,serialize )
                                                                            </div>
                                                                        
                                                                        </div>
                                                                                                      
                                                                    </div>
                                                                    
                                                                </div>
                                                        <h4>Example request for POST data</h4>
                                                                    <?php print("<pre>".print_r($requestOneway,true)."</pre>");?>
                                                        
               </div>
                            <div class="tab-pane" id="2">
                                        <p>&nbsp;</p>
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12">
                                                                                  
                                                                        <div class="alert alert-warning" role="alert">
                                                                            
                                                                            <div class="bs-callout bs-callout-warning" id="callout-btn-group-anchor-btn"> 
                                                                                <h4> <i class="fa fa-hand-o-down"></i> Example RoundTrip URL</h4> 
                                                                                <ul class="media-list">
                                                                                    <li class="media">
                                                                                      <div class="media-left">
                                                                                       
                                                                                      </div>
                                                                                      <div class="media-body">
                                                                                          <h5 class="media-heading"><strong>GET</strong> :  /flight/ThaiSmile/GetFlightPrice?request= <label class=label-info>{{url_encode}}</label> <i class="fa fa-arrows-h"></i> <a href='<?php echo $exampleRequestRoundTrip?>' target="_blank">example</a></h5>
                                                                                          <h5 class="media-heading"><strong>GET</strong> : /flight/ThaiSmile/GetFlightPrice/format/php?request=<label class="label-info">{{url_encode}}</label> <i class="fa fa-arrows-h"></i> <a href="<?php echo $exampleRequestRoundTripByFormat;?>" target="_blank">example</a></h5>
                                                                                          
                                                                                          <p>&nbsp;</p>
                                                                                          <h5 class="media-heading"><strong>POST</strong> : /flight/GetFlightPrice</h5>
                                                                                          <h5 class="media-heading"><strong>POST</strong> : /flight/GetFlightPrice/format/json</h5>
                                                                                      </div>
                                                                                    </li>
                                                                                  </ul>
                                                                            
                                                                            </div>
                                                                            <div class="alert alert-success">
                                                                               ( <strong>any format in</strong> :  json,csv,xml,html,php,serialize )
                                                                            </div>
                                                                        
                                                                        </div>
                                                                                                      
                                                                    </div>
                                                                    
                                                                </div>
                                                                    <h4>Example request for POST data</h4>
                                                                     <?php print("<pre>".print_r($requestRoundTrip,true)."</pre>");?>
            </div>
                                                    
            </div>
                        </div>
                        
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script   src="https://code.jquery.com/jquery-3.1.0.min.js"   integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="   crossorigin="anonymous"></script>

    <!-- Bootstrap Core JavaScript -->
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>

<script type="text/javascript">
    <?php if($this->router->fetch_class() == "ApiManaul"){ ?>
                    //$(".flight-getprice").tab('show');
                    $("#collapseFive").collapse('show');
                    
    <?php }?>
    
    </script>