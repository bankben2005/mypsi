
	<div class="panel panel-default">
		<div class="panel-heading text-center"><h4>ข้อมูลลูกค้า</h4></div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-6"> 


					<p>
						<strong>หมายเลขบัตรประชาชน / เบอร์โทรศัพท์ : </strong><?php echo @$ConsumerData->RegisterCode?>
					</p>
					<p>
						<strong>ชื่อ - นามสกุล : </strong><?php echo @$ConsumerData->RegisterName?>
					</p>
					<p>
						<strong>เพศ : </strong><?php echo (@$ConsumerData->Sex == 'M')?'ชาย':'หญิง'?>
					</p>
					<p>
						<strong>ว/ด/ป เกิด : </strong><?php echo @$ConsumerData->BirthDay->format('d/m/Y')?>
					</p>
					<p>
						<strong>ที่อยู่ : </strong><?php echo @$ConsumerData->Addr1?>
					</p>

				</div>
				<div class="col-lg-6"> 
					<p>
						<strong>อำเภอ/เขต : </strong><?php echo @$ConsumerData->District?>
					</p>
					<p>
						<strong>จังหวัด : </strong><?php echo @$ConsumerData->Province?>
					</p>
					<p>
						<strong>รหัสไปรษณีย์ : </strong><?php echo @$ConsumerData->Postalcode?>
					</p>
					<p>
						<strong>โทรศัพท์ : </strong><?php echo @$ConsumerData->Telephone?>
					</p>
					<p>
						<strong>อีเมล์ : </strong><?php echo @$ConsumerData->EmailAddress?>
					</p>

				</div>
			</div>
		</div>


		
		
	</div>

	<div class="panel panel-default">
		<div class="panel-heading text-center"><h4>ข้อมูลการลงทะเบียนสินค้า</h4></div>
		<div class="panel-body">
			<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>รหัสสินค้า</th>
						<th>รายละเอียด</th>
						<th>Serial No.</th>
						<th>วันที่ลงทะเบียน</th>
						<th>วันสิ้นสุดประกัน</th>
						<th>Sat Mail</th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($ProductRegisterList) > 0){ 
						foreach($ProductRegisterList as $key => $row){
					?> 
					<tr>
						<td><?php echo ++$key?></td>
						<td><?php echo $row->MPCode?></td>
						<td><?php echo $row->ModelData->MDDesc?></td>
						<td><?php echo $row->SerialNo?></td>
						<td><?php echo $row->RegisterDate->format('d/m/Y')?></td>
						<td>
							<?php if(strtotime($row->ExpireDate->format('Y-m-d')) >= strtotime(date('Y-m-d'))){?>
								<label class="label label-success"><?php echo $row->ExpireDate->format('d/m/Y')?></label>
							<?php }else{?>
								<label class="label label-danger"><?php echo $row->ExpireDate->format('d/m/Y')?></label>
							<?php }?>
							
								
						</td>
						<td><?php echo ($row->SatMail)?$row->Satmail:'-'?></td>
					</tr>

					<?php } }else{?>
						<tr>
							<td colspan="7"><label class="label label-danger">ไม่พบข้อมูลการลงทะเบียนสินค้า</label></td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
		</div>
	</div>
