<div class="container">
	<div class="row">
	<p>&nbsp;</p>
	<?php echo message_warning($this);?>
	<div class="panel panel-default mt-20">
		<div class="panel-heading">
			<strong>กรอกข้อมูลเพื่อลงทะเบียน O5</strong>
		</div>
		<div class="panel-body">
			<?php echo form_open('',array('id'=>'frm-register'))?>
				<div class="form-group">
					<label>Serial Number (SN) :</label>
					<?php echo form_input(array('name'=>'product_sn','class'=>'form-control','required'=>'required'))?>
				</div>

				<div class="form-group">
					<label>ชื่อ :</label>
					<?php echo form_input(array('name'=>'firstname','class'=>'form-control','required'=>'required'))?>
				</div>

				<div class="form-group">
					<label>สกุล :</label>
					<?php echo form_input(array('name'=>'lastname','class'=>'form-control','required'=>'required'))?>
				</div>

				<div class="form-group">
					<label>เบอร์โทรศัพท์ :</label>
					<?php echo form_input(array('name'=>'telephone','class'=>'form-control','required'=>'required'))?>
				</div>

				<div class="form-group">
					<label><?php echo form_checkbox(array('name'=>'accept_policy'))?>&nbsp;
					ตกลงเงื่อนไขการลงทะเบียน<label> &nbsp;<a href="#" data-toggle="modal" data-target="#policyModal" style="text-decoration: underline;">อ่านเงื่อนไข</a>
				</div>

				<div class="form-group">
					<?php //echo form_submit('_submit','ตกลง','class="btn btn-block btn-primary"')?>
					<button type="submit" class="btn btn-block btn-primary">ตกลง</button>

					<a href="http://psisat.com/arm/knownhow/index.html" class="btn btn-block btn-default">ย้อนกลับ</a>
				</div>


			<?php echo form_close(); ?>
		</div>

	</div>

</div>

</div>

<!-- Modal -->
<div class="modal fade" id="policyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">เงื่อนไขการลงทะเบียนกล่อง O5</h4>
      </div>
      <div class="modal-body">
      	<p>
        - หลังจากลงทะเบียนเสร็จสิ้น ท่านจะได้รับสิทธิรับชมช่องรายการต่างๆผ่าน PSI TV ฟรีทันที ถึงวันที่ 30 ตุลาคม 2561
<br>* หมายเหตุ: บริษัทขอสงวนสิทธิ์ในการเปลี่ยนแปลงช่องรายการโดยมิต้องแจ้งให้ทราบล่วงหน้า
<br>- เนื่องจากกล่อง PSI O5 สามารถเข้าสู่ระบบอินเตอร์เน็ต ดังนั้นทางลูกค้าต้องเป็นผ่ายที่ต้องรับผิดชอบในด้านสิทธิหรือด้านกฏหมาย  
<br>- ในการรับชมรายการต่างๆผ่าน PSI O5 จำเป็นต้องใช้อินเตอร์เน็ตอย่างน้อย 5 Mbps
		</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        
      </div>
    </div>
  </div>
</div>