<style type="text/css">
	.text-center{
		text-align: center;
	}
	#copy-to-clipboard{
		margin-top: 10px;
	}
	button#btn-generate{
			margin-top: 10px;
		}
	.mt-10{
		margin-top: 10px;
	}
	.mt-20{
		margin-top: 20px;
	}
	
</style>
<div class="container">
	<div class="row">
	<p>&nbsp;</p>
	<?php echo message_warning($this);?>

	<div class="col-md-6 col-md-offset-3">
		<div class="row">
		<div class="panel panel-default mt-20">
			<div class="panel-heading text-center">
				<strong>สร้างเลขหลังบัตรสำหรับช่าง</strong>
			</div>
			<div class="panel-body">
				<div class="input-group">
					  <input type="text" name="cardno" class="form-control" placeholder="เลขหลังบัตร" aria-describedby="basic-addon2" id="cardno" readonly="readonly">
					  <span class="input-group-addon" id="basic-addon2"><a href="#" class="clipboard-btn" data-clipboard-action="copy" data-clipboard-target="#cardno" style="cursor: pointer;"><i class="fa fa-clipboard"></i></a></span>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block" id="btn-generate"><i class="fa fa-key"></i> Generate</button>
				</div>

				<!-- <div class="col-md-9 col-sm-9">
					<?php echo form_input(array('name'=>'cardno','class'=>'form-control','readonly'=>'readonly'))?>
				</div>
				<div class="col-md-3 col-sm-3">
					<button type="submit" class="btn btn-primary btn-block" id="btn-generate"><i class="fa fa-key"></i> Generate</button>
				</div>
				<div class="clearfix"></div>

				<div class="col-md-12" id="copy-to-clipboard">
					<button type="button" class="btn btn-block btn-default" disabled="disabled" id="btn-clipboard"><i class="fa fa-clipboard"></i> Copy to clipboard</button>
				</div> -->
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading text-center">
				<strong>แปลงเลขหลังบัตรเป็นวันที่และเวลา</strong>
			</div>
			<div class="panel-body">
					<div class="input-group">
					  <input type="text" name="cardnogenerated" class="form-control" placeholder="เลขหลังบัตร" aria-describedby="basic-addon2" id="cardnogenerated">
					  <span class="input-group-addon" id="basic-addon2"><a href="#" class="unlock-btn" style="cursor: pointer;"><i class="fa fa-unlock"></i></a></span>
					</div>

					<div id="show-datetime">

					</div>
			</div>

		</div>
	</div>
	</div>

	</div>
</div>