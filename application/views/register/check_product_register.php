<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>PSI ตรวจสอบการลงทะเบียนสินค้า</title>
  <meta name="generator" content="Bootply" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="Keywords" content="psi ตรวจสอบการลงทะเบียน,ตรวจสอบลงทะเบียน">
  <meta name="Description" content="PSI ตรวจสอบการลงทะเบียนสินค้า">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css')?>">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <style type="text/css"> 
    .mt-2{
      margin-top: 20px;
    }
    .mt-3{
      margin-top: 30px;
    }
    .text-center{
      text-align: center;
    }

    h1 > small{
      color: white;
    }

    @media(max-width: 425px){
      .h1, h1 {
        font-size: 27px;
      }
    }

  </style>
</head>
<body> 
  <input type="hidden" name="base_url" value="<?php echo base_url()?>">
  <div class="container text-center mt-2">
    <a href="https://psisat.com"><img src="<?php echo base_url('assets/images/logo/logo-psi2018.png')?>" style="max-width: 120px;"></a>
  </div>

  <div class="container mt-2">
    <div class="panel panel-primary">
      <div class="panel-heading text-center"><h1>ตรวจสอบลงทะเบียนสินค้า <br> <small>(ลูกค้าลงทะเบียนผ่าน Call Center)</small></h1></div>
      <div class="panel-body">

        <div class="row">
          <div class="col-lg-5"> 

            <p><strong>กรุณากรอกหมายเลขบัตรประชาชน / เบอร์โทรศัพท์ </strong></p> 

              <?php echo form_open('',['name'=>'search-register-form','onsubmit'=>'submitSearchRegisterForm(event)'])?>
                <div class="form-group">
                  <!-- <label class="text-center"><strong> : </strong></label>  -->
                  <?php echo form_input([
                    'name'=>'register_code',
                    'class'=>'form-control',
                    'required'=>'required',
                    'placeholder'=>'กรอกเลขบัตรประชาชน / เบอร์โทรศัพท์'
                  ])?>
                </div>

                <div class="form-group">
                  <?php echo form_button([
                    'type'=>'submit',
                    'class'=>'btn btn-success btn-block',
                    'content'=>'<i class="fa fa-search"></i>'.' ค้นหา'
                  ])?>
                </div>

              <?php echo form_close()?>

          </div>
          <div class="col-lg-7"> 
            <p class="text-center">
              <strong>ลูกค้าที่ต้องการลงทะเบียนสินค้าใหม่ด้วยตัวเอง กรุณาลงทะเบียนสินค้าด้วยแอพ <a href="http://psi.co.th/help_fixitarm/linkapp/psifixit"><label style="color: #34a7f8;cursor: pointer;text-decoration: underline;">FIXIT</label></a></strong> <br>
                <a href="http://psi.co.th/help_fixitarm/linkapp/psifixit"><img src="<?php echo base_url('assets/images/qr_code/fixit_app.jpg')?>" style="max-width: 200px;"></a>
            </p>



          </div>

        </div>


        




        <hr>

        <div id="show_result_search">

        </div>


      </div>
    </div>
  </div>

  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery/jquery.min.js')?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js')?>"></script> 

  <script type="text/javascript" src="<?php echo base_url('assets/js/register/check_product_register.js?'.time())?>"></script>
</body>
</html>