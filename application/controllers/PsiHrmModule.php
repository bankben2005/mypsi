<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class PsiHrmModule extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }


    public function RequestTokenSignin_post(){
            if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenHrmAPI');
                    
                    $requestAuthenAPI = requestAuthenHrmAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('RequestTokenSignin');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getTokenSignin($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }



    }

    public function RemindEvaluationTask_post(){
            if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenHrmAPI');
                    
                    $requestAuthenAPI = requestAuthenHrmAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('RemindEvaluationTask');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setRemindEvaluationTask($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }


    }

    public function SendLeaveNotification_get(){
           

           if($this->get(null,false)){
                $output = $this->__sendLeaveNotification($this->get());
                echo json_encode($output);
           }else{
                echo json_encode(array('status'=>false,'detail'=>'no send'));
            }
    }

    private function __getTokenSignin($requestCriteria){
        require_once(APPPATH. "libraries/Hrm/HrmApiLibrary.php");
        $output = array();
        $HrmApiLibrary = new HrmApiLibrary();
        $output = $HrmApiLibrary->getTokenSignin($requestCriteria);
        return $output;
    }

    private function __setRemindEvaluationTask($requestCriteria){
        require_once(APPPATH. "libraries/Hrm/HrmApiLibrary.php");
        $output = array();
        $HrmApiLibrary = new HrmApiLibrary();
        $output = $HrmApiLibrary->setRemindEvaluationTask($requestCriteria);
        return $output;

    }

    private function __sendLeaveNotification($requestCriteria){
        require_once(APPPATH. "libraries/Hrm/HrmApiLibrary.php");
        $output = array();
        $HrmApiLibrary = new HrmApiLibrary();
        $output = $HrmApiLibrary->sendLeaveNotification($requestCriteria);
        return $output;
    }

}