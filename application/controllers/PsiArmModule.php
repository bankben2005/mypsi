<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class PsiArmModule extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }


    public function GetArmActivity_post(){
            if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetArmActivity');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getArmActivity($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }



    }

    public function GetCouponAgentList_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetCouponAgentList');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getCouponAgentList($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }


    }

    public function CheckCouponAgentAvailable_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenArmAPI');
                    
                    $requestAuthenAPI = requestAuthenArmAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('CheckCouponAgentAvailable');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__checkCouponAgentAvailable($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function UseCouponAgent_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenArmAPI');
                    
                    $requestAuthenAPI = requestAuthenArmAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('UseCouponAgent');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setCouponAgentUsed($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }
    public function GetRedeemCoupon_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetRedeemCoupon');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getRedeemcoupon($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function SetRedeemPointToCoupon_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetRedeemPointToCoupon');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setRedeemPointToCoupon($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }


    public function GetFlashSale_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetFlashSale');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getFlashSale($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }


    }

    public function SetFlashSaleOrder_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetFlashSaleOrder');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setFlashSaleOrder($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetFlashSaleOrderHistory_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetFlashSaleOrderHistory');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getFlashSaleOrderHistory($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function SetFlashSaleOrderCancel_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetFlashSaleOrderCancel');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setFlashSaleOrderCancel($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function SetBackgroundNotification_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetBackgroundNotification');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setBackgroundNotification($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }



    private function __getArmActivity($requestCriteria){

        require_once(APPPATH. "libraries/Questionnaire/QuestionnaireApiLibrary.php");
        $output = array();
        $QuestionnaireApiLibrary = new QuestionnaireApiLibrary();
        $output = $QuestionnaireApiLibrary->getArmActivity($requestCriteria);
        return $output;

    }

    private function __getCouponAgentList($requestCriteria){
        require_once(APPPATH. "libraries/Coupon/CouponApiLibrary.php");
        $output = array();
        $CouponApiLibrary = new CouponApiLibrary();
        $output = $CouponApiLibrary->getCouponAgentList($requestCriteria);
        return $output;
    }

    private function __checkCouponAgentAvailable($requestCriteria){
        require_once(APPPATH. "libraries/Coupon/CouponApiLibrary.php");
        $output = array();
        $CouponApiLibrary = new CouponApiLibrary();
        $output = $CouponApiLibrary->checkCouponAgentAvailable($requestCriteria);
        return $output;
    }

    private function __setCouponAgentUsed($requestCriteria){
        require_once(APPPATH. "libraries/Coupon/CouponApiLibrary.php");
        $output = array();
        $CouponApiLibrary = new CouponApiLibrary();
        $output = $CouponApiLibrary->setCouponAgentUsed($requestCriteria);
        return $output;
    }

    private function __getRedeemcoupon($requestCriteria){
        require_once(APPPATH. "libraries/Coupon/CouponApiLibrary.php");
        $output = array();
        $CouponApiLibrary = new CouponApiLibrary();
        $output = $CouponApiLibrary->getRedeemCoupon($requestCriteria);
        return $output;
    }

    private function __setRedeemPointToCoupon($requestCriteria){
        require_once(APPPATH. "libraries/Coupon/CouponApiLibrary.php");
        $output = array();
        $CouponApiLibrary = new CouponApiLibrary();
        $output = $CouponApiLibrary->setRedeemPointToCoupon($requestCriteria);
        return $output;
    }

    //======================== for flash sale =============================
    private function __getFlashSale($requestCriteria){
        require_once(APPPATH. "libraries/FlashSale/FlashSaleLibrary.php");
        $output = array();
        $FlashSaleLibrary = new FlashSaleLibrary();
        $output = $FlashSaleLibrary->getFlashSale($requestCriteria);
        return $output;
    }

    private function __setFlashSaleOrder($requestCriteria){
        require_once(APPPATH. "libraries/FlashSale/FlashSaleLibrary.php");
        $output = array();
        $FlashSaleLibrary = new FlashSaleLibrary();
        $output = $FlashSaleLibrary->setFlashSaleOrder($requestCriteria);
        return $output;
    }

    private function __getFlashSaleOrderHistory($requestCriteria){
        require_once(APPPATH. "libraries/FlashSale/FlashSaleLibrary.php");
        $output = array();
        $FlashSaleLibrary = new FlashSaleLibrary();
        $output = $FlashSaleLibrary->getFlashSaleOrderHistory($requestCriteria);
        return $output;
    }

    private function __setFlashSaleOrderCancel($requestCriteria){
        require_once(APPPATH. "libraries/FlashSale/FlashSaleLibrary.php");
        $output = array();
        $FlashSaleLibrary = new FlashSaleLibrary();
        $output = $FlashSaleLibrary->setFlashSaleOrderCancel($requestCriteria);
        return $output;
    }

    private function __setBackgroundNotification($requestCriteria){
        require_once(APPPATH. "libraries/FlashSale/FlashSaleLibrary.php");
        $output = array();
        $FlashSaleLibrary = new FlashSaleLibrary();
        $output = $FlashSaleLibrary->setBackgroundNotification($requestCriteria);
        return $output;
    }
    

}