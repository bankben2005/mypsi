<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_technician extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
                        	$this->load->library(array('pagination'));
    }
    public function index(){
        $this->satellite();
    	
    }
    public function satellite($page = null){
    	$this->page_num = 10;

    	$agent = new M_agent();
    	$agent->where('FixIt01','T');
    	$clone = $agent->get_clone();
		$AgentCount = $clone->count();
		// $Agent->order_by('AgentRating','desc');

		$agent->limit($this->page_num,$page);
		$agent->get();

    	$data = array(
    		'agent' => $agent,
            'provinces' => $this->getAllProvince()
    	);

    	$this->template->content->view('backend/technician/list_satellite',$data);
        $this->template->publish();

    }

    public function create(){
        $this->__create();
    }
    public function edit($agent_code=null){
        if($agent_code){
            /* check agent code */
            $agent = new M_agent();
            $agent->where('AgentCode',$agent_code)->get();

            if($agent->result_count() <= 0){
                redirect('backend/'.$this->controller);
            }else{
                $this->__create($agent_code);
            }
        }else{
             redirect('backend/'.$this->controller);
        }

    }
    private function __create($agent_code=null){


        $this->template->javascript->add(base_url('assets/backend/individual/js/technician/edit.js'));
        $agent = new M_agent();
        if($agent_code){
            $agent->where('AgentCode',$agent_code);
        }

        $agent->get();

        $arProvince = array(""=>__('Select Province','backend/technician/edit'));
        $provinces = new M_provinces();
        $provinces->order_by('province_name','asc');
        foreach ($provinces->get() as $key => $value) {
            # code...
            $arProvince[$value->id] = $value->province_name;
        }

    	
        // print_r($arProvince);exit;
        $data = array(
            'agent' => $agent,
            'provinces' => $arProvince
        );

        //print_r($data);exit;

        //echo $agent->AgentName;
        $this->template->content->view('backend/technician/edit',$data);
        $this->template->publish();


    }

    private function getAllProvince(){
        $arProvince = array(''=>'Select Province');
        $province = new M_province();
        $province->get();

        foreach ($province as $key => $value) {
            # code...
            $arProvince[$value->ProvinceID] = $value->ProvinceName;
        }
        
        return $arProvince;
    }
    public function ajaxGetAmphurByProvince(){

                        $arData = array("0"=>__('Select Province','backend/technician/edit'));
                        $receive_data = array(
                              'province_id' => $this->input->post('province_id')
                        );

                        $amphur = new M_amphurs();
                        $amphur->where('provinces_id',$receive_data['province_id']);

                        foreach ($amphur->get() as $key => $value) {
                              # code...
                              $arData[$value->id] = $value->amphur_name;

                        }
                        echo json_encode(array('status'=>true,'data'=>$arData));

        

    }
    public function ajaxGetDistrictByAmphur(){

                        $arData = array("0" => __('Select Amphur','backend/technician/edit'));
                        $receive_data = array(
                            'amphur_id' => $this->input->post('amphur_id')
                        );

                        $district = new M_districts();
                        $district->where('amphurs_id',$receive_data['amphur_id']);

                        foreach ($district->get() as $key => $value) {
                            # code...
                            $arData[$value->id] = $value->district_name;
                        }

                        echo json_encode(array('status'=>true,'data'=>$arData));



    }
    public function ajaxGetZipcode(){
                        $arData = array();
                        $receive_data = array(
                              'province_id' => $this->input->post('province_id'),
                              'amphur_id' => $this->input->post('amphur_id'),
                              'district_id' => $this->input->post('district_id')
                        );

                        // echo json_encode($receive_data);exit;

                        $zipcode = new M_zipcode();
                        $zipcode->where('provinces_id',$receive_data['province_id'])->where('amphurs_id',$receive_data['amphur_id'])->where('districts_id',$receive_data['district_id']);

                        $arData = array(
                              
                                    'zipcode' => $zipcode->get()->zipcode
                              
                        );
                        echo json_encode(array('status'=>true,'data'=>$arData));
    }

}