<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_staff extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $seminar_conn;
	private $db_config;
    private $seminar_mpcode;

	public function __construct() {
            parent::__construct();
            $this->load->library(array('pagination'));
            $this->load->config('internaldb');
            $this->db_config = $this->config->item('seminar_db');
            $this->connectSeminarDB();
            $this->setSeminarMPCode();
    }
    public function index($page = null){
    		$this->clothdeal();

    }

    public function clothdeal(){

    		$arSeminar = array();
	    	$query = "select * from SeminarInfo where SeminarDate >= '".date('Y-m-d')."' order by SeminarDate";

	    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
	    	// echo ENVIRONMENT;exit;
	    	if(sqlsrv_num_rows($stmt) > 0){
	    		while($result = sqlsrv_fetch_object($stmt)){

	    			array_push($arSeminar, $result);
	    		}
	    	}

	    	$data = array(
	    		'seminar' => $arSeminar
	    	);

	    	$this->template->content->view('backend/staff/seminarclothdeal',$data);
	        $this->template->publish();
    }

    public function s3dealsseminarlist(){

            //echo 'aaaa';exit;
            $arSeminar = array();
            $query = "select * from SeminarInfo where SeminarDate >= '".date('Y-m-d')."' order by SeminarDate";

            $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            // echo ENVIRONMENT;exit;
            if(sqlsrv_num_rows($stmt) > 0){
                while($result = sqlsrv_fetch_object($stmt)){

                    array_push($arSeminar, $result);
                }
            }

            $data = array(
                'seminar' => $arSeminar
            );

            $this->template->content->view('backend/staff/s3dealsseminarlist',$data);
            $this->template->publish();

    }

    public function cloths3list($seminarno = ""){
        $this->dataTableCSSPackages();
        $this->dataTableJSPackages();
        $this->template->javascript->add(base_url('assets/backend/individual/js/staff/clothdeallist.js'));

        $clothdeal_list = array();
        /* check seminar no */
        $seminar_data = $this->getSeminarDataBySeminarNo($seminarno);

        // print_r($seminar_data);

        if(!empty($seminar_data)){

            /* get clothdeal by seminar */
            // $clothdeal_result = $this->getClothDealBySeminarNo($seminarno);
            $s3deal_result = $this->getS3DealBySeminarNo($seminarno);


            // print_r($s3deal_result);
            $data = array(
                's3deal_list' => $s3deal_result,
                'seminar_data' => $seminar_data
            );


            // print_r($data);
            $this->template->content->view('backend/staff/seminars3technicianlist',$data);
            $this->template->publish();



        }else{

            redirect(base_url('backend/admin_staff/clothdeal'));
        }

    }

    public function seminarclothdeallist($seminarno = ""){

        $this->dataTableCSSPackages();
        $this->dataTableJSPackages();
        $this->template->javascript->add(base_url('assets/backend/individual/js/staff/clothdeallist.js'));

    	$clothdeal_list = array();
    	/* check seminar no */
    	$seminar_data = $this->getSeminarDataBySeminarNo($seminarno);

    	// print_r($seminar_data);exit;

    	if(!empty($seminar_data)){

    		/* get clothdeal by seminar */
    		$clothdeal_result = $this->getClothDealBySeminarNo($seminarno);

    		if($clothdeal_result && count($clothdeal_result) > 0){
    				$clothdeal_list = $clothdeal_result;
    		}

    		$data = array(
    			'clothdeal_list' => $clothdeal_list,
    			'seminar_data' => $seminar_data
    		);


            // print_r($data);
    		$this->template->content->view('backend/staff/seminarclothdeallist',$data);
	        $this->template->publish();



    	}else{

    		redirect(base_url('backend/admin_staff/clothdeal'));
    	}



    }

    public function adds3deals(){
        //echo 'aaa';exit;

            $this->template->stylesheet->add(base_url('assets/backend/individual/css/staff/addclothdeal.css'));

            $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');

            $this->template->javascript->add(base_url('assets/backend/individual/jquery-base64-master/jquery.base64.min.js'));

            $this->template->javascript->add(base_url('assets/backend/individual/js/staff/s3scanqr.js'));

            $seminarno  = "";
            $seminarno = $this->input->get('seminarno');

            /* check Seminar no Available*/
            if(!$this->getSeminarDataBySeminarNo($seminarno)){
                redirect(base_url('backend/admin_staff/clothdeal'));
            }


            $data = array(
                'seminarno' => $seminarno,
                'seminar_data' => $this->getSeminarDataBySeminarNo($seminarno)
            );

            // print_r($data);exit;
            $this->template->content->view('backend/staff/addseminars3deal',$data);
            $this->template->publish();





    }

    public function addclothdeal(){

            $this->template->stylesheet->add(base_url('assets/backend/individual/css/staff/addclothdeal.css'));

            $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');

            $this->template->javascript->add(base_url('assets/backend/individual/jquery-base64-master/jquery.base64.min.js'));

            $this->template->javascript->add(base_url('assets/backend/individual/js/staff/scanqr.js'));

            $seminarno  = "";
            $seminarno = $this->input->get('seminarno');

            /* check Seminar no Available*/
            if(!$this->getSeminarDataBySeminarNo($seminarno)){
                redirect(base_url('backend/admin_staff/clothdeal'));
            }

            /* eof check Seminar no*/

            //echo $seminarno;exit;
    		$data = array(
                'seminarno' => $seminarno,
                'seminar_data' => $this->getSeminarDataBySeminarNo($seminarno)
    		);

            // print_r($data);exit;
    		$this->template->content->view('backend/staff/addseminarclothdeal',$data);
	        $this->template->publish();

    }

    public function seminarorder(){
    	/* get all seminar */

    	
    	$arSeminar = array();
    	$query = "select * from SeminarInfo where SeminarDate >= '".date('Y-m-d')."'";

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt) > 0){
    		while($result = sqlsrv_fetch_object($stmt)){

    			array_push($arSeminar, $result);
    		}
    	}

    	$data = array(
    		'seminar' => $arSeminar
    	);

    	$this->template->content->view('backend/staff/seminarorder',$data);
        $this->template->publish();



    }
    public function seminaredit($seminarno = ""){
    	$this->dataTableCSSPackages();
    	$this->dataTableJSPackages();

        $this->template->javascript->add(base_url('assets/backend/individual/js/staff/editproduct.js'));
    	$masterproduct = new M_masterproduct();

    	if($this->input->post(NULL,FALSE)){

    		/* clear order product by seminar */
    		$this->clearProductAvailable($seminarno);
    		/* eof clear order product by seminar */
    		$data_active = array();
    		foreach ($this->input->post('active') as $key => $value) {
    				$data_insert = array(
    					'mpcode' => $value,
    					'amount' => $this->input->post('amount_'.$value)
    				);
    				sqlsrv_query($this->seminar_conn,
    					"insert into SeminarProductAvailable (
    						MPCODE,
    						SeminarNo,
    						Created,
    						Amount

    					)values(
    						'".$data_insert['mpcode']."',
    						'".$seminarno."',
    						'".date('Y-m-d H:i:s')."',
    						'".$data_insert['amount']."'

    					)"
    				);
    				
    				
    		}

    		// print_r($data_active);exit;
    	}


    	/* get seminar product available by semina no*/
    	$seminarproductavailabel = $this->getSeminarProductAvailable($seminarno);

    	$productavailable = array();
    	$productamount = array();
    	if(!empty($seminarproductavailabel)){
	    	foreach ($seminarproductavailabel as $key => $value) {
	    		# code...
	    		array_push($productavailable, trim($value->MPCODE));
	    		$productamount[trim($value->MPCODE)] = $value->Amount;

	    	}

    	}
    	// print_r($productamount);
    	// print_r($productavailable);exit;

    	$data = array(
    		'seminarno' => $seminarno,
    		'product' => $masterproduct->get(),
    		'productavailable' => $productavailable,
    		'productamount' => $productamount
    	);
    	$this->template->content->view('backend/staff/editproduct',$data);
        $this->template->publish();

    } 

    public function getAgentDataBySeminarRegister(){


        $get_data = $this->input->get();
        $arrSeminarRegister = [];

        $query = "select * from SeminarRegister where SeminarNo = '".$get_data['seminar_no']."'"; 

        //echo $query;exit;

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

        //$objData = new StdClass();

        if(sqlsrv_num_rows($stmt) > 0){
                    //$objData = sqlsrv_fetch_object($stmt);
                   // print_r($objData);exit;
                    while($result  = sqlsrv_fetch_object($stmt)) { 

                        $agent_data = $this->getSomeAgentDataByAgentCode([
                            'agent_code'=>$result->AgentCode
                        ]); 

                        $result->{'AgentData'} = $agent_data;
                            array_push($arrSeminarRegister, $result);
                    }
                    
        } 


        foreach ($arrSeminarRegister as $key => $value) {
            # code...
            if(!property_exists($value->AgentData, 'AgentCode')){
                print_r($value);
            }
        }

        // print_r($arrSeminarRegister);


    }

    public function orderproductlist(){
        $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
        $this->template->javascript->add(base_url('assets/backend/individual/js/staff/orderproductlist.js'));
        if($this->input->post(NULL,FALSE)){

                if($this->input->post('record_action') && $this->input->post('choose_action')){

                       switch ($this->input->post('choose_action')) {
                           case 'delete':
                               # code...
                                if($this->deleteOrderProduct($this->input->post('record_action'))){
                                    ///unset($_POST);
                                    $this->msg->add(__('Delete product order success!','backend/staff/orderproductlist'),'success');
                                    redirect('backend/'.$this->controller.'/'.$this->method);
                                }

                            break;
                            case 'cancel':
                                if($this->cancelOrderProduct($this->input->post('record_action'))){
                                    $this->msg->add(__('Cancel product order success!','backend/staff/orderproductlist'),'success');
                                    redirect('backend/'.$this->controller.'/'.$this->method);
                                }

                            break;
                           
                           default:
                               # code...
                               break;
                       }

                }

        }

        
        $this->dataTableCSSPackages();
        $this->dataTableJSPackages();

        $orderproductlist = $this->getOrderProductList();
        


        $data = array(
            'orderproductlist' => $orderproductlist
        );

        $this->template->content->view('backend/staff/orderproductlist',$data);
        $this->template->publish();
    }

    public function orderproductcancel(){
        if($this->input->post(NULL,FALSE)){
                if($this->input->post('record_action') && $this->input->post('choose_action')){

                       switch ($this->input->post('choose_action')) {
                           case 'restore':
                                if($this->restoreOrderProduct($this->input->post('record_action'))){
                                    $this->msg->add(__('Restore product order success!','backend/staff/orderproductcancellist'),'success');
                                    redirect('backend/'.$this->controller.'/'.$this->method);
                                }
                           break;
                        }
                }

        }


        $this->dataTableCSSPackages();
        $this->dataTableJSPackages();
        $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
        $this->template->javascript->add(base_url('assets/backend/individual/js/staff/orderproductcancellist.js'));

        
        

        $orderproductcancellist = $this->getOrderProductCancelList();

        $data = array(
            'orderproductcancellist' => $orderproductcancellist
        );

        $this->template->content->view('backend/staff/orderproductcancellist',$data);
        $this->template->publish();


    }

    public function exportorderproduct(){

        $this->template->javascript->add(base_url('assets/backend/individual/js/staff/exportorderproduct.js'));

        $branchlist_data = array();
        $branchlist_data[''] = '-- เลือกสาขาส่งมอบ --';

        $branchlist = $this->getAllBranchList();

        foreach ($branchlist as $key => $value) {
            # code...
            $branchlist_data[$value->BranchCode] = $value->BranchName;
        }

        $data = array(
            'branchlist' => $branchlist_data
        );

        $this->template->content->view('backend/staff/exportorderproduct',$data);
        $this->template->publish();


    }
    public function exportDataToExcel(){

        // $arData = array('agentdata','orderdata');
        
        $arData['orderdata'] = array();
        $data_received = array(
            'select_year' => $this->input->post('select_year'),
            'select_group' => $this->input->post('select_group'),
            'select_branch' => $this->input->post('select_branch')
        );
        

        /* query */
        $query = "select *,SeminarProductOrder.MPCODE as order_mpcode from SeminarProductOrder 
        join SeminarInfo on SeminarProductOrder.SeminarNo = SeminarInfo.SeminarNo where SeminarInfo.SeminarInfoGroup = '".$data_received['select_group']."' and SeminarProductOrder.Active = '1'";

        $filename = "";

        // echo $query;exit;


        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
        // echo ENVIRONMENT;exit;
        if(sqlsrv_num_rows($stmt) > 0){
            $aragentdata = array();
            $orderdata = array();



            while($result = sqlsrv_fetch_object($stmt)){

                

                //print_r($result);
                $datacheckagent = array('agentcode'=>$result->AgentCode,'select_branch'=>$data_received['select_branch']);

                $agentdata = $this->checkIsAgentInBrachCode($datacheckagent);
                //print_r($agentdata);exit;
                if($agentdata){
                    $agentdatarow = $this->checkIsAgentInBrachCode($datacheckagent);
                    $productrow = $this->getProductByMPCODE($result->order_mpcode);
                    //print_r($agentdatarow);
                    $result->{'AgentData'} = $agentdatarow;
                    $result->{'ProductData'} = $productrow;
                    array_push($arData['orderdata'], $result);
                    //array_push($arData['orderdata']['AgentData'], $agentdatarow);
                    
                }
                
            }


            // print_r($aragentdata);
            // print_r($orderdata);

            // exit;

            // array_push($arData['agentdata'], $aragentdata);
            // array_push($arData['orderdata'], $orderdata);

        }

        //print_r($arData);exit;
        if(empty($arData['orderdata'])){
            $this->msg->add(__('Order product not found','backend/staff/exportorderproduct'),'error');
            redirect(base_url('backend/'.$this->controller.'/exportorderproduct'));

        }else{
            $filename = 'รายการจองสินค้า-'.$arData['orderdata'][0]->SeminarInfoGroup.$data_received['select_year'].'สาขา'.$arData['orderdata'][0]->AgentData->Branch.'.xlsx';

            //echo $filename;exit;
        }

        require_once 'assets/backend/plugin/PHPExcel/Classes/PHPExcel.php';


        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        //exit;

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("PSIFIXIT")
                                     ->setLastModifiedBy("PSIFIXIT Admin")
                                     ->setTitle($filename)
                                     ->setSubject("Excel file for order product")
                                     ->setDescription("Excel file for order product")
                                     ->setKeywords("Order Report")
                                     ->setCategory("Report");

        //print_r($arDataOrder);


        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1')->setCellValue('A1',$filename);
        $objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2','#')
                    ->setCellValue('B2','สัมนา')
                    ->setCellValue('C2','รหัสช่าง')
                    ->setCellValue('D2','สินค้า')
                    ->setCellValue('E2','จำนวน')
                    ->setCellValue('F2','จองเมื่อ')
                    ->setCellValue('G2','ชื่อ-สกุล')
                    ->setCellValue('H2','ที่อยู่')
                    ->setCellValue('I2','อำเภอ')
                    ->setCellValue('J2','จังหวัด')
                    ->setCellValue('K2','เบอร์โทร')
                    ->setCellValue('L2','ราคาสินค้า(สัมนา)')
                    ->setCellValue('M2','สาขา')
                    ->setCellValue('N2','รหัสสาขา');

        $count = 1;
        $table_row = 3;
        foreach ($arData['orderdata'] as $key => $value) {
            # code...
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$table_row.'',@$count)
                        ->setCellValue('B'.$table_row.'',@$value->SeminarDesc)
                        ->setCellValue('C'.$table_row.'',@$value->AgentCode)
                        ->setCellValue('D'.$table_row.'',@$value->ProductData->MPDESCRIPTION)
                        ->setCellValue('E'.$table_row.'',@$value->Amount)
                        ->setCellValue('F'.$table_row.'',@$value->Created->format('d/m/Y H:i:s'))
                        ->setCellValue('G'.$table_row.'',@$value->CustomerName)
                        ->setCellValue('H'.$table_row.'',@$value->CustomerAddress)
                        ->setCellValue('I'.$table_row.'',@$value->CustomerDistrict)
                        ->setCellValue('J'.$table_row.'',@$value->CustomerProvince)
                        ->setCellValue('K'.$table_row.'',@$value->CustomerPhone)
                        ->setCellValue('L'.$table_row.'',@$value->SeminarPrice)
                        ->setCellValue('M'.$table_row.'',@$value->AgentData->Branch)
                        ->setCellValue('N'.$table_row.'',@$value->AgentData->BranchName);
                        $count++;
                        $table_row++;
        }
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('A')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('B')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('C')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('D')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('E')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('F')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('G')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('H')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('I')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('J')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('K')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('L')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('M')
        ->setAutoSize(true);
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('N')
        ->setAutoSize(true);

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('OrderReport');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;





    }

    public function ajaxCheckCardNoExist(){
        $post_data = $this->input->post();

        $query = $this->db->select('Agent.CardNo,Agent.AgentCode,Agent.AgentName,Agent.AgentSurName,Agent.Telephone,Agent.emailaddress,Agent.R_Addr1,Agent.R_District,Agent.R_Province,Agent.BranchCode,Branch.BranchCode,Branch.BranchName')
        ->from('Agent')
        ->join('Branch','Agent.BranchCode = Branch.BranchCode')
        ->where('Agent.CardNo',$post_data['card_no'])
        ->get();

       // echo $this->db->last_query();exit;

        if($query->num_rows() > 0){
            $agent_data = $query->row();

            $agent_view = $this->load->view('backend/staff/ajaxAgentContentSeminarPermit',array(
                'agent_data'=>$agent_data
            ),true); 

            $ticket_history = $this->getBoughtTicketHistoryByAgentCode(array(
                'agent_code'=>$agent_data->AgentCode
            ));

            $ticket_view = $this->load->view('backend/staff/ajaxSeminarTicketContent',array(
                'ticket_data'=>$ticket_history
            ),true);

            //echo $agent_data->Image;exit; 

            $quota_histories = $this->getQuotaHistoryByAgentCode([
                'agent_code'=>$agent_data->AgentCode
            ]); 

            //print_r($quota_histories);exit;

            $quota_view = $this->load->view('backend/staff/ajaxSeminarQuotaContent',[
                'quota_histories'=>$quota_histories['arrQuotaRecord']
            ],true);

            //print_r($quota_view);exit;

            echo json_encode(array(
                'status'=>true,
                'post_data'=>$post_data,
                'agent_data'=>$agent_data,
                'ticket_history'=>$ticket_history,
                'agent_view'=>$agent_view,
                'ticket_view'=>$ticket_view,
                'quota_view'=>$quota_view
            ));
        }else{
            echo json_encode(array(
                'status'=>false,
                'post_data'=>$post_data
            ));
        }

        
    }

    public function ajaxConfirmClothDeals(){
        $post_data = $this->input->post();

        /* insert data to SeminarClothDeals table*/
                    $insertQuery = "insert into SeminarClothDeals(
                        SeminarNo,
                        AgentCode,
                        CustomerName,
                        CustomerAddress,
                        CustomerDistrict,
                        CustomerProvince,
                        CustomerPhone,
                        TotalClothes,
                        Created,
                        Active
                    )values(
                        '".$post_data['SeminarNo']."',
                        '".$post_data['AgentCode']."',
                        '".$post_data['AgentName'].' '.$post_data['AgentSurName']."',
                        '".$post_data['R_Addr1']."',
                        '".$post_data['R_District']."',
                        '".$post_data['R_Province']."',
                        '".$post_data['Telephone']."',
                        '".$post_data['quantity']."',
                        '".date('Y-m-d H:i:s')."',
                        '1'
                    )";

                    

                    $stmt = sqlsrv_query( $this->seminar_conn, $insertQuery,array(),array( "Scrollable" => 'static' ));

                    if($stmt){

                    }





        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data
        ));
    }


    private function connectSeminarDB(){

    	if(ENVIRONMENT == 'development'){


	    	$serverName = $this->db_config['development']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}
		}else if(ENVIRONMENT == 'testing'){
			$serverName = $this->db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}



		}
    }


    private function clearProductAvailable($seminarno = ""){

    	$query = "select * from SeminarProductAvailable where SeminarNo = '".$seminarno."'";

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt) > 0){
    		while($result = sqlsrv_fetch_object($stmt)){
    			sqlsrv_query($this->seminar_conn,"delete from SeminarProductAvailable where id = '".$result->id."'");

    		}
    	}


    }

    private function getSeminarProductAvailable($seminarno = ""){
    	$arReturn = array();
    	$query = "select * from SeminarProductAvailable where SeminarNo = '".$seminarno."'";

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt) > 0){
    		while($result = sqlsrv_fetch_object($stmt)){
    			array_push($arReturn, $result);
    		}
    	}

    	return $arReturn;

    }


    public function ajaxSaveOrderProductAvailable(){
    	// $post_data = $_POST;
// echo json_encode(array('test'=>'test'));exit;
    	if($this->input->post(NULL,FALSE)){
    		
    			/* clear order product by seminar */
    		$this->clearProductAvailable($this->input->post('seminarno'));
    		/* eof clear order product by seminar */
    		$data_active = array();
    		foreach ($this->input->post('active') as $key => $value) {
    				$data_insert = array(
    					'mpcode' => $value,
    					'amount' => $this->input->post('amount_'.$value)
    				);
                    $strInsert = "insert into SeminarProductAvailable (
                            MPCODE,
                            SeminarNo,
                            Created,
                            Amount

                        )values(
                            '".$data_insert['mpcode']."',
                            '".$this->input->post('seminarno')."',
                            '".date('Y-m-d H:i:s')."',
                            '".$data_insert['amount']."'

                        )";
                        // echo $strInsert;exit;
    				sqlsrv_query($this->seminar_conn,$strInsert);
    				
    				
    		}

    		echo json_encode(array(
    			'status' => true
    		));


    	}

    }

    private function getSeminarDataBySeminarNo($seminarno = ""){
    	$query = "select * from SeminarInfo where SeminarNo = '".$seminarno."'";

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

    	if(sqlsrv_num_rows($stmt) > 0){
    		return sqlsrv_fetch_object($stmt);
    	}else{
    		return false;
    	}


    }

    private function getClothDealBySeminarNo($seminarno = ""){
         $arReturn = array();
    	$query = "select * from SeminarClothDeals where SeminarNo = '".$seminarno."'";

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

    	if(sqlsrv_num_rows($stmt) > 0){
    		// return sqlsrv_fetch_object($stmt);
            while ($result = sqlsrv_fetch_object($stmt)) {
                # code...
                // print_r($result);
                array_push($arReturn, $result);
            }
            return $arReturn;
            
    	}else{
    		return false;
    	}


    }

    private function getS3DealBySeminarNo($seminarno = ""){

        $arReturn = array();
        $query = "select * from SeminarS3Deals where SeminarNo = '".$seminarno."'";

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt) > 0){
            // return sqlsrv_fetch_object($stmt);
            while ($result = sqlsrv_fetch_object($stmt)) {
                # code...
                // print_r($result);
                array_push($arReturn, $result);
            }
            return $arReturn;
            
        }else{
            return false;
        }

    }


    public function ajaxGetAgentDataByAgentCode(){
        $mpcode = "";
        $status_ticket = false;
        $already_deal = false;
        $row_ticket = array();
        $seminar_otherplace = array();
        $data_received = array(
            'cardno' => $this->input->post('cardno'),
            'seminar_no' => $this->input->post('seminar_no')
        );

        $query = $this->db->select('AgentCode,CardNo,AgentName,AgentSurName,emailaddress,Telephone,R_Addr1,R_District,R_Province,Image,Branch')->from('Agent')->where('CardNo',$data_received['cardno'])->get();

        if($query->num_rows() > 0){

            $row = $query->row_array();
            $row['Image'] = base64_encode($row['Image']);

            /* get Agent Available for Seminar */
            $resultSeminar = $this->getSeminarDataBySeminarNo($data_received['seminar_no']);

            // print_r($resultSeminar);exit;
            $mpcode = $resultSeminar->MPCODE;


            /* get Agent Already cloth deal */
            $already_deal = $this->checkAgentAlreadyCloathDeal(array('agentcode'=>$row['AgentCode'],'seminarno'=>$data_received['seminar_no']));

            $already_s3deal = ($this->input->post('s3deal'))?$this->checkAgentAlreadyS3Deal(array(
                'agentcode'=>$row['AgentCode'],
                'seminarno'=>$data_received['seminar_no']
            )):false;

            $already_checkin = $this->checkAlreadyCheckin(array('agentcode'=>$row['AgentCode'],'seminarno'=>$data_received['seminar_no'])); 

            $seat_and_room = $this->checkAgentHasSeatAndRoom([
                'agentcode'=>$row['AgentCode'],
                'seminarno'=>$data_received['seminar_no']
            ]);


            if($resultSeminar->SeminarType == '3'){ /* Seminar Quota */
                    // echo 'abcd';exit;
                    
                    /*  check quota available */
                    $seminarRegister = $this->checkAlreadyRegisterQuota(array('seminarno'=>$data_received['seminar_no'],'agentcode'=>$row['AgentCode']));

                    if($seminarRegister){
                            $status_ticket = true;
                            $row_ticket = array(
                                'AgentCode' => $row['AgentCode'],
                                'MPCODE' => '-',
                                'MPDESCRIPTION' => $seminarRegister->SeminarDesc,
                                'MPNAME' => $seminarRegister->SeminarDesc,
                                'POSINETPRICE' => '-',
                                'POSIPRICE' => '0',
                                'POSIQUANTITY' => $seminarRegister->Qty,
                                'POSTDATE' => '-',
                                'POSTSTATUS' => '',
                                'ProductImage' => '',
                                'TMABBNUMBER' => ''
                            );

                    }



            }else if($resultSeminar->SeminarType == '1'){
                $result = $this->getAgentAvailableSeminar(array('mpcode'=>$mpcode,'agent_code'=>$row['AgentCode']));

                if($result->num_rows() > 0){
                    $status_ticket = true;


                    $quantity_amount = 0;
                    $grand_totals_amount = 0;

                    if($result->num_rows() > 1){
                            foreach ($result->result() as $key => $value) {
                                # code...
                                $quantity_amount += $value->POSIQUANTITY;
                                $grand_totals_amount += ($value->POSIPRICE*$value->POSIQUANTITY);
                            }

                    }else{
                            $quantity_amount = $result->row()->POSIQUANTITY;
                            $grand_totals_amount = $result->row()->POSIPRICE;

                    }

                    $row_ticket = $result->row_array();
                    $row_ticket['POSTDATE'] = date('d/m/Y',strtotime($row_ticket['POSTDATE']));
                    $row_ticket['POSIQUANTITY'] = (int)$quantity_amount;
                    // $row_ticket['POSIQUANTITY'] = 15;
                    $row_ticket['POSIPRICE'] = number_format($grand_totals_amount,2);

                    
                }else{

                    $return_seminar_otherplace = $this->checkBuyOtherPlaceSeminarTicket(array(
                        'agentcode'=>$row['AgentCode']
                    ));

                    if($return_seminar_otherplace){
                        $seminar_otherplace = $return_seminar_otherplace;
                    }

                }

            }

            /* eof Agent Available for Seminar*/

            echo json_encode(array(
                'status'=>true,
                'data'=>$row,
                'data_ticket'=>$row_ticket,
                'status_ticket'=>$status_ticket,
                'already_deal'=>$already_deal,
                'already_checkin'=>$already_checkin,
                'already_s3deal'=>$already_s3deal,
                'seminar_otherplace'=>$seminar_otherplace,
                'seat_and_room'=>$seat_and_room
            ));
        }else{
            echo json_encode(array('status'=>false));
        }

       
    }

    public function ajaxSaveClothDeals(){
            $data_received = array(
                    'clothSize' => $this->input->post('clothSize'),
                    'agentcode' => $this->input->post('agentcode'),
                    'seminarno' => $this->input->post('seminarno')
            );

            /* get Agent Data */
            $queryAgent = $this->db->select('AgentCode,CardNo,AgentName,AgentSurName,emailaddress,Telephone,R_Addr1,R_District,R_Province,Image,Branch')->from('Agent')->where('AgentCode',$data_received['agentcode'])->get();

            if($queryAgent->num_rows() > 0){
                    $row = $queryAgent->row();

                    /* insert data to SeminarClothDeals table*/
                    $insertQuery = "insert into SeminarClothDeals(
                        SeminarNo,
                        AgentCode,
                        CustomerName,
                        CustomerAddress,
                        CustomerDistrict,
                        CustomerProvince,
                        CustomerPhone,
                        TotalClothes,
                        Created,
                        Active
                    )values(
                        '".$data_received['seminarno']."',
                        '".$row->AgentCode."',
                        '".$row->AgentName.' '.$row->AgentSurName."',
                        '".$row->R_Addr1."',
                        '".$row->R_District."',
                        '".$row->R_Province."',
                        '".$row->Telephone."',
                        '".count($data_received['clothSize'])."',
                        '".date('Y-m-d H:i:s')."',
                        '1'
                    )";

                    /* Create log file for debug*/
                            $log_file_path = $this->createLogFilePath('CreateClothdeal');
                            $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode(array('strQuery'=>$insertQuery)) . "\n";
                            file_put_contents($log_file_path, $file_content, FILE_APPEND);
                            unset($file_content);

                    $stmt = sqlsrv_query( $this->seminar_conn, $insertQuery,array(),array( "Scrollable" => 'static' ));

                    if($stmt){
                        $insert_id = "";
                        $query_insert_id = "select max(id) as last_insert_id from SeminarClothDeals";

                        $stmt = sqlsrv_query( $this->seminar_conn, $query_insert_id,array(),array( "Scrollable" => 'static' ));
                        // echo ENVIRONMENT;exit;
                        // $result = sqlsrv_fetch_object($stmt);
                        // print_r($result);
                        if(sqlsrv_num_rows($stmt) > 0){
                            $result = sqlsrv_fetch_object($stmt);
                            $insert_id = $result->last_insert_id;
                        }

                        foreach ($data_received['clothSize'] as $key => $value) {
                            # code...
                            $query_insert_item = "insert into SeminarClothDealsItems(
                                SeminarClothDeals_id,
                                Size,
                                Amount,
                                Created
                            )values(
                                '".$insert_id."',
                                '".strtoupper($value['size'])."',
                                '".(int)$value['amount']."',
                                '".date('Y-m-d H:i:s')."'
                            )";

                            $stmt2 = sqlsrv_query( $this->seminar_conn, $query_insert_item,array(),array( "Scrollable" => 'static' ));

                            if($stmt2){

                                
                            }

                        }
                        echo json_encode(array('status'=>true));exit;



                            


                    }else{
                        $txt_error = "";
                        if( ($errors = sqlsrv_errors() ) != null) {
                            foreach( $errors as $error ) {
                                $txt_error .= "SQLSTATE: ".$error[ 'SQLSTATE']."<br />";
                                $txt_error .= "code: ".$error[ 'code']."<br />";
                                $txt_error .= "message: ".$error[ 'message']."<br />";
                            }
                        }

                        echo json_encode(array('status'=>false,'description' =>'cannot add data to seminar clothdeal','txt_error' => $txt_error));exit;

                    }




            }else{
                echo json_encode(array('status'=>false,'description'=>'agent no found'));exit;
            }



            // echo json_encode(array('status'=>false));

    }

    public function seminar_permit(){

        $this->loadSweetAlert();

        $this->template->javascript->add(base_url('assets/backend/individual/js/staff/seminar_permit.js'));

        $data = array(

        );

        $this->template->content->view('backend/staff/seminar_permit',$data);
        $this->template->publish();
    }

    public function ajaxSetTechnicianGotS3Device(){
        $post_data = $this->input->post();

        /* get Agent Data */
            $queryAgent = $this->db->select('AgentCode,CardNo,AgentName,AgentSurName,emailaddress,Telephone,R_Addr1,R_District,R_Province,Image,Branch')->from('Agent')->where('AgentCode',$post_data['agentcode'])->get();

            if($queryAgent->num_rows() > 0){
                    $row = $queryAgent->row();

                    /* insert data to SeminarClothDeals table*/
                    $insertQuery = "insert into SeminarS3Deals(
                        SeminarNo,
                        AgentCode,
                        CustomerName,
                        CustomerAddress,
                        CustomerDistrict,
                        CustomerProvince,
                        CustomerPhone,
                        Totals,
                        Created,
                        Active
                    )values(
                        '".$post_data['seminarno']."',
                        '".$row->AgentCode."',
                        '".$row->AgentName.' '.$row->AgentSurName."',
                        '".$row->R_Addr1."',
                        '".$row->R_District."',
                        '".$row->R_Province."',
                        '".$row->Telephone."',
                        '1',
                        '".date('Y-m-d H:i:s')."',
                        '1'
                    )";

                    $stmt = sqlsrv_query( $this->seminar_conn, $insertQuery,array(),array( "Scrollable" => 'static' ));

                    if($stmt){
                        echo json_encode(array(
                            'status'=>true,
                            'post_data'=>$post_data
                        ));
                        exit;
                    }else{
                        echo json_encode(array(
                            'status'=>false,
                            'post_data'=>$post_data
                        ));
                        exit;
                    }
            }else{

                        echo json_encode(array(
                            'status'=>false,
                            'post_data'=>$post_data
                        ));
                        exit;
            }




        //print_r($post_data);
    }

    public function ajaxCheckinOnline(){
            $data_received = array(
                    'agentcode' => $this->input->post('agentcode'),
                    'cardno' => $this->input->post('cardno')
            );

                            $requestObj = new StdClass();
                            $requestObj->agentcode = $data_received['agentcode'];
                            
                            
                            /* include library */
                            require_once(APPPATH . "libraries/Seminar/SeminarLibrary.php");
                            $output = array();
                            $SeminarLibrary = new SeminarLibrary();
                            $output = $SeminarLibrary->setSeminarCheckin($requestObj);

                            if($output['result_code'] == '000'){

                                    echo json_encode(array('status'=>true,'agentcode'=>$data_received['agentcode'],'cardno'=>$data_received['cardno']));

                            }
                            // print_r($output);

    }

    

    private function getAgentAvailableSeminar($data = array()){

            $query =  $this->db->select('POSTRANSACTION.AgentCode,POSTRANSACTION.TMABBNUMBER,POSTRANSACTION.POSTSTATUS,POSITEM.MPCODE,POSITEM.TMABBNUMBER,POSITEM.POSIQUANTITY,POSITEM.POSIPRICE,POSITEM.POSINETPRICE,POSITEM.POSTDATE,MasterProduct.MPNAME,MasterProduct.MPDESCRIPTION,MasterProduct.ProductImage')
                ->from('POSITEM')
                ->join('POSTRANSACTION','POSTRANSACTION.TMABBNUMBER = POSITEM.TMABBNUMBER')
                ->join('MasterProduct','MasterProduct.MPCODE = POSITEM.MPCODE')
                ->where('POSTRANSACTION.POSTSTATUS','T')
                ->where('POSITEM.MPCODE',$data['mpcode'])
                ->where('POSTRANSACTION.AgentCode',$data['agent_code'])
                ->order_by('POSITEM.POSTDATE','desc')
                ->get();

                // echo $this->db->last_query();exit;

                return $query;

            // echo $this->db->last_query();exit;



    }

    // private function checkAgentAlreadyCloathDeal($data = array()){

    //         $query = "select SeminarNo,AgentCode from SeminarClothDeals where SeminarNo = '".$data['seminarno']."' and AgentCode = '".$data['agentcode']."'";

    //         $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            
    //         if(sqlsrv_num_rows($stmt) > 0){

    //                 return true;
    //         }else{
    //                 return false;

    //         }


    // }

    private function checkAlreadyCheckin($data = array()){


            /* SeminarId Group */
            $queryGroup = "select * from SeminarInfo where SeminarNo = '".$data['seminarno']."'";

            $stmt1 = sqlsrv_query( $this->seminar_conn, $queryGroup,array(),array( "Scrollable" => 'static' ));

            if(sqlsrv_num_rows($stmt1)){
                    $resultObj = sqlsrv_fetch_object($stmt1);
                    $groupObj = $resultObj->SeminarInfoGroup;

                  

                    $query = "select * from SeminarData join SeminarInfo on SeminarData.SeminarNo = SeminarInfo.SeminarNo where SeminarData.AgentCode = '".$data['agentcode']."' order by RegisterDateTime desc";

                    $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

                    if(sqlsrv_num_rows($stmt) > 0){
                        $dataGroup = array();
                        $dataArray = array();
                        $dataReturnData = array();
                        while ($result = sqlsrv_fetch_object($stmt)) {
                            
                            array_push($dataGroup, $result->SeminarInfoGroup);
                            array_push($dataArray, $result);

                        }

                       
                        if(in_array($groupObj, $dataGroup)){

                                $dataReturnData = array(
                                    'SeminarNo' => $dataArray[0]->SeminarNo,
                                    'AgentCode' => $dataArray[0]->AgentCode,
                                    'CheckinDate' => $dataArray[0]->RegisterDateTime->format('Y-m-d'),
                                    'CheckinTime' => $dataArray[0]->RegisterDateTime->format('H:i:s')
                                );
                                return true;

                        }else{
                            return false;

                        }
                        

                        
                    }else{
                        return false;
                    }


            }





            /* old data here */
            // $query = "select SeminarNo,AgentCode from SeminarData where SeminarNo = '".$data['seminarno']."' and AgentCode = '".$data['agentcode']."'";

            // $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            
            // if(sqlsrv_num_rows($stmt) > 0){
            //     return true;
            // }else{
            //     return false;
            // }

    }

    private function checkAlreadyRegisterQuota($data = array()){
            $query = "select * from SeminarRegister join SeminarInfo on SeminarRegister.SeminarNo = SeminarInfo.SeminarNo where SeminarRegister.SeminarNo = '".$data['seminarno']."' and SeminarRegister.AgentCode = '".$data['agentcode']."'";

            // echo $query;exit;

            $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            
            if(sqlsrv_num_rows($stmt) > 0){

                // print_r(sqlsrv_fetch_object($stmt));exit;
                    return sqlsrv_fetch_object($stmt);
            }else{
                    return false;
            }

    }

    public function testnana(){
        $this->getAgentAvailableSeminar(array('mpcode'=>'O1600002','agentcode'=>'5860517'));
    }
    private function createLogFilePath($filename = '') {
        
        //$document_root = $_SERVER['DOCUMENT_ROOT'];

        $log_path = './logs/CreateClothdeal';
        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    public function testbug(){
        $strQuery = "insert into SeminarClothDeals( SeminarNo, AgentCode, CustomerName, CustomerAddress, CustomerDistrict, CustomerProvince, CustomerPhone, TotalClothes, Created, Active )values( 'PSI_TRANSFORM_001', '5830322', 'สมบูรณ์ ปาเบ้า', '17/237 ม.4 ซ.ประชาอุทิศ 19 ถ.ประชาอุทิศ แขวงบางมด', 'ทุ่งครุ', 'กรุงเทพมหานคร', '0870557310', '1', '2018-01-31 12:47:32', '1' )";


            $stmt = sqlsrv_query( $this->seminar_conn, $strQuery,array(),array( "Scrollable" => 'static' ));
            
            if( $stmt === false ) {
                if( ($errors = sqlsrv_errors() ) != null) {
                    foreach( $errors as $error ) {
                        echo "SQLSTATE: ".$error[ 'SQLSTATE']."<br />";
                        echo "code: ".$error[ 'code']."<br />";
                        echo "message: ".$error[ 'message']."<br />";
                    }
                }
            }

    }

    public function deleteSeminarClothDeal($agentcode){

        $query = "delete from SeminarClothDeals where AgentCode = '".$agentcode."' and SeminarNo ='PSI_TRANSFORM_001'";

        sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));





    }

    public function getSeminarClothDealsByAgent($agencode){
        $query = "select * from SeminarClothDeals where AgentCode = '".$agentcode."' and SeminarNo = 'PSI_TRANSFORM_001'";

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            
            if(sqlsrv_num_rows($stmt) > 0){

                print_r(sqlsrv_fetch_object($stmt));exit;
                    //return sqlsrv_fetch_object($stmt);
            }else{
                    return false;
            }
    }

    public function checkAgentAlreadyCloathDeal($data = array()){



        /* get seminar into group */
        $sqlGroup = "select SeminarNo,SeminarInfoGroup from SeminarInfo where SeminarNo = '".$data['seminarno']."'";

        $stmt1 = sqlsrv_query( $this->seminar_conn, $sqlGroup,array(),array( "Scrollable" => 'static' ));
            
            if(sqlsrv_num_rows($stmt1) > 0){
                    $objSeminarGroup = sqlsrv_fetch_object($stmt1);
                    //print_r($objSeminarGroup);exit;

                    $query = "select SeminarClothDeals.SeminarNo,SeminarClothDeals.AgentCode,SeminarInfo.SeminarInfoGroup from SeminarClothDeals join  SeminarInfo on SeminarClothDeals.SeminarNo = SeminarInfo.SeminarNo  where SeminarClothDeals.AgentCode = '".$data['agentcode']."'";
                    
                    $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            
                    if(sqlsrv_num_rows($stmt) > 0){
                        $arClothDealInGroup = array();
                        while($result  = sqlsrv_fetch_object($stmt)) {
                            # code...
                            array_push($arClothDealInGroup,$result->SeminarInfoGroup);
                        }

                        if(in_array($objSeminarGroup->SeminarInfoGroup, $arClothDealInGroup)){
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;

                    }

            }else{
                    


            }

            

            

    }

    public function ajaxGetAgentImageProfileByCardNo(){
        $query = $this->db->select('CardNo,Image')
        ->from('Agent')
        ->where('CardNo',$this->input->post('card_no'))
        ->get();

        //echo $this->input->post();exit;

        if($query->num_rows() > 0){
            echo json_encode(array(
                'status'=>true,
                'image_profile'=>base64_encode($query->row()->Image)
            ));
        }else{
            echo json_encode(array(
                'status'=>false
            ));
        }
    }

    private function getOrderProductList(){

                    
                    $arData = array();
                    $query = "select *,SeminarProductOrder.MPCODE as SeminarProductCode from SeminarProductOrder join SeminarInfo on SeminarProductOrder.SeminarNo = SeminarInfo.SeminarNo where SeminarProductOrder.Active = '1'";

                    $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            
                    if(sqlsrv_num_rows($stmt) > 0){
                        
                        $count = 0;
                        while($result  = sqlsrv_fetch_object($stmt)) {

                            //print_r($result);
                            
                            // $queryAgent = $this->db->select('AgentCode,Branch,CardNo')
                            // ->from('Agent')
                            // ->where('AgentCode',$result->AgentCode)
                            // ->get();
                            // $rowAgent = $queryAgent->row();
                            // $result->{'Branch'} = @$rowAgent->Branch;
                            // $result->{'CardNo'} = @$rowAgent->CardNo;

                            $queryProduct = $this->db->select('MPCODE,MPNAME')
                            ->from('MasterProduct')
                            ->where('MPCODE',$result->SeminarProductCode)
                            ->get();
                            $rowProduct =$queryProduct->row();

                            $result->{'MPNAME'} = @$rowProduct->MPNAME;

                            array_push($arData, $result);

                            $count++;
                        }
                    }
                    return $arData;

    }

    private function getOrderProductCancelList(){
                    $arData = array();
                    $query = "select *,SeminarProductOrder.MPCODE as SeminarProductCode from SeminarProductOrder join SeminarInfo on SeminarProductOrder.SeminarNo = SeminarInfo.SeminarNo where SeminarProductOrder.Active = '0'";

                    $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            
                    if(sqlsrv_num_rows($stmt) > 0){
                        
                        $count = 0;
                        while($result  = sqlsrv_fetch_object($stmt)) {

                            //print_r($result);
                            $queryAgent = $this->db->select('AgentCode,Branch,CardNo')
                            ->from('Agent')
                            ->where('AgentCode',$result->AgentCode)
                            ->get();
                            $rowAgent = $queryAgent->row();
                            $result->{'Branch'} = @$rowAgent->Branch;
                            $result->{'CardNo'} = @$rowAgent->CardNo;

                            $queryProduct = $this->db->select('MPCODE,MPNAME')
                            ->from('MasterProduct')
                            ->where('MPCODE',$result->SeminarProductCode)
                            ->get();
                            $rowProduct =$queryProduct->row();

                            $result->{'MPNAME'} = @$rowProduct->MPNAME;

                            array_push($arData, $result);

                            $count++;
                        }
                    }
                    return $arData;
    }

    public function ajaxGetSeminarGroupByYear(){
            $arSeminarGroupInfo = array();
            $data_received = array(
                'year' => $this->input->post('select_year')
            );

            $querySeminarInfo = "select SeminarDate,SeminarDesc,SeminarInfoGroup from SeminarInfo where year(SeminarDate) = '".$data_received['year']."'";

            $stmt = sqlsrv_query( $this->seminar_conn, $querySeminarInfo,array(),array( "Scrollable" => 'static' ));
            
                    if(sqlsrv_num_rows($stmt) > 0){
                        
                        $count = 0;
                        while($result  = sqlsrv_fetch_object($stmt)) {
                            array_push($arSeminarGroupInfo, $result->SeminarInfoGroup);

                        }
                        $arSeminarGroupInfo = array_unique($arSeminarGroupInfo);

                        echo json_encode(array('status'=>true,'data'=>$arSeminarGroupInfo));
                    }else{
                        echo json_encode(array('status'=>false));


                    }
    }

    public function ajaxSetSeminarSeats(){

        $post_data = $this->input->post(); 



        // check 
        // check seatno already exist 
        $checkSeat = $this->checkSeatNoExist([
            'seminarno'=>$post_data['seminar_no'],
            'agentcode'=>$post_data['agentcode'],
            'seatno'=>$post_data['seat_no']
        ]);

        if($checkSeat['status']){ 
            $seminarSeatData = $checkSeat['seminarSeatData'];

            echo json_encode([
                'status'=>false,
                'result_code'=>'-009',
                'result_desc'=>'เลขที่นั่งซ้ำกับคุณ '.$seminarSeatData->CustomerName.'(รหัสช่าง:'.$seminarSeatData->AgentCode.') กรุณาระบุเลขที่นั่งอื่น'
            ]);exit;

        }
        

            

        // check 
        $queryCheck = "select * from SeminarSeats where AgentCode = '".$post_data['agentcode']."' and SeminarNo = '".$post_data['seminar_no']."'";

        //echo $queryCheck;exit;
        $stmt1 = sqlsrv_query( $this->seminar_conn, $queryCheck,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt1) > 0){
            // update only SeatNo and RoomNo 
            $row = sqlsrv_fetch_object($stmt1); 

            // $query_update = 'update SeminarSeats set SeatNo = "'.$post_data['seat_no'].'",RoomNo = "'.$post_data['room_no'].'",Updated="'.date('Y-m-d H:i:s').'" where id = "'.$row->id.'"';

            $query_update = "update SeminarSeats set";
            $query_update .= " SeatNo = '".$post_data['seat_no']."'";
            $query_update .= ", RoomNo = '".$post_data['room_no']."'";
            $query_update .= " where id = '".$row->id."'";

            //echo $query_update;exit;

            sqlsrv_query($this->seminar_conn,$query_update,[],['Scrollable'=>'static']); 

            echo json_encode([
                'status'=>true,
                'post_data'=>$post_data
            ]);


        }else{
            /* get Agent Data */
            $queryAgent = $this->db->select('AgentCode,CardNo,AgentName,AgentSurName,emailaddress,Telephone,R_Addr1,R_District,R_Province,Image,Branch')->from('Agent')->where('AgentCode',$post_data['agentcode'])->get(); 

            $row = $queryAgent->row();

            $insert_str = "insert into SeminarSeats(
                SeminarNo,
                AgentCode,
                CustomerName,
                CustomerAddress,
                CustomerDistrict,
                CustomerProvince,
                CustomerPhone,
                SeatNo,
                RoomNo,
                Created
            ) values (
                '".$post_data['seminar_no']."',
                '".$post_data['agentcode']."',
                '".$row->AgentName.' '.$row->AgentSurName."',
                '".$row->R_Addr1."',
                '".$row->R_District."',
                '".$row->R_Province."',
                '".$row->Telephone."',
                '".$post_data['seat_no']."',
                '".$post_data['room_no']."',
                '".date('Y-m-d H:i:s')."'
            )"; 

            //echo $insert_str;exit;

            sqlsrv_query($this->seminar_conn,$insert_str,[],['Scrollable'=>'static']); 

            echo json_encode([
                'status'=>true,
                'post_data'=>$post_data
            ]);



        }


        
    }

    private function getAllBranchList(){
        $query = $this->db->select('BranchCode,BranchName')
        ->from('Branch')
        ->order_by('BranchName')
        ->get();
        return $query->result();
    }

    private function deleteOrderProduct($arData = array()){

        if(!empty($arData)){
            foreach ($arData as $key => $value) {
                # code...
                $strQuery = "delete from SeminarProductOrder where id = '".$value."'";

                $stmt = sqlsrv_query( $this->seminar_conn, $strQuery,array(),array( "Scrollable" => 'static' ));
            }
            return true;


        }

    }
    private function cancelOrderProduct($arData = array()){
        if(!empty($arData)){
            foreach ($arData as $key => $value) {
                # code...
                $strQuery = "update SeminarProductOrder set Active = '0',Updated = '".date('Y-m-d H:i:s')."' where id = '".$value."'";

                $stmt = sqlsrv_query( $this->seminar_conn, $strQuery,array(),array( "Scrollable" => 'static' ));
            }
            return true;

        }

    }
    private function restoreOrderProduct($arData = array()){
        //print_r($arData);exit;
        if(!empty($arData)){
            foreach ($arData as $key => $value) {
                # code...
                $strQuery = "update SeminarProductOrder set Active = '1',Updated = '".date('Y-m-d H:i:s')."' where id = '".$value."'";

                // echo $strQuery;exit;

                $stmt = sqlsrv_query( $this->seminar_conn, $strQuery,array(),array( "Scrollable" => 'static' ));
            }
            return true;

        }
    }

    private function checkIsAgentInBrachCode($arData = array()){

        $query = $this->db->select('Agent.AgentCode,Agent.BranchCode,Agent.Branch,Branch.BranchName')
        ->from('Agent')
        ->join('Branch','Agent.BranchCode = Branch.BranchCode')
        ->where('Agent.AgentCode',$arData['agentcode'])
        ->where('Agent.BranchCode',$arData['select_branch'])
        ->get();

        //echo $this->db->last_query()."<br>";exit;

        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }

    }

    private function getProductByMPCODE($mpcode = ""){
        $query = $this->db->select('MPCODE,MPNAME,MPDESCRIPTION')
        ->from('MasterProduct')
        ->where('MPCODE',$mpcode)
        ->get();

        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

    private function checkAgentAlreadyS3Deal($data = array()){


        $queryCheck = "select * from SeminarS3Deals where AgentCode = '".$data['agentcode']."'";

        //echo $queryCheck;exit;
        $stmt1 = sqlsrv_query( $this->seminar_conn, $queryCheck,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt1) > 0){
            return true;
        }else{
            return false;
        }

    }

    private function setSeminarMPCode(){
        $this->load->config('seminar');

        $this->seminar_mpcode = $this->config->item('seminar_mpcode');
        
        
    }

    private function checkBuyOtherPlaceSeminarTicket($data = []){

        $query =  $this->db->select('POSTRANSACTION.AgentCode,POSTRANSACTION.TMABBNUMBER,POSTRANSACTION.POSTSTATUS,POSITEM.MPCODE,POSITEM.TMABBNUMBER,POSITEM.POSIQUANTITY,POSITEM.POSIPRICE,POSITEM.POSINETPRICE,POSITEM.POSTDATE,MasterProduct.MPNAME,MasterProduct.MPDESCRIPTION,MasterProduct.ProductImage')
                ->from('POSITEM')
                ->join('POSTRANSACTION','POSTRANSACTION.TMABBNUMBER = POSITEM.TMABBNUMBER')
                ->join('MasterProduct','MasterProduct.MPCODE = POSITEM.MPCODE')
                ->where('POSTRANSACTION.POSTSTATUS','T')
                ->where_in('POSITEM.MPCODE',$this->seminar_mpcode)
                ->where('POSTRANSACTION.AgentCode',$data['agentcode'])
                ->order_by('POSITEM.POSTDATE','desc')
                ->get();

                //echo $this->db->last_query();exit;
        
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }


    }


    private function getBoughtTicketHistoryByAgentCode($data = array()){ 

        $query =  $this->db->select('POSTRANSACTION.AgentCode,POSTRANSACTION.TMABBNUMBER,POSTRANSACTION.POSTSTATUS,POSITEM.MPCODE,POSITEM.TMABBNUMBER,POSITEM.POSIQUANTITY,POSITEM.POSIPRICE,POSITEM.POSINETPRICE,POSITEM.POSTDATE,MasterProduct.MPNAME,MasterProduct.MPDESCRIPTION,MasterProduct.ProductImage')
                ->from('POSITEM')
                ->join('POSTRANSACTION','POSTRANSACTION.TMABBNUMBER = POSITEM.TMABBNUMBER')
                ->join('MasterProduct','MasterProduct.MPCODE = POSITEM.MPCODE')
                ->where('POSTRANSACTION.POSTSTATUS','T')
                ->like('POSITEM.MPCODE','o','after')
                ->where('POSTRANSACTION.AgentCode',$data['agent_code'])
                ->order_by('POSITEM.POSTDATE','desc')
                ->get();

        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }


    } 

    private function getQuotaHistoryByAgentCode($data = []){ 

        $arrQuotaRecord = [];

        $query = "select * from SeminarRegister join SeminarInfo on SeminarRegister.SeminarNo = SeminarInfo.SeminarNo where SeminarRegister.AgentCode = '".$data['agent_code']."' order by SeminarInfo.SeminarDate desc"; 

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

        $objData = new StdClass();

        if(sqlsrv_num_rows($stmt) > 0){
                    //$objData = sqlsrv_fetch_object($stmt);
                   // print_r($objData);exit;
                    while($result  = sqlsrv_fetch_object($stmt)) {
                            array_push($arrQuotaRecord, $result);
                    }
        }

        return [
            'arrQuotaRecord'=>$arrQuotaRecord
        ];


    }

    private function checkAgentHasSeatAndRoom($data = []){ 

        /* SeminarId Group */
            $query = "select * from SeminarSeats where SeminarNo = '".$data['seminarno']."' and AgentCode = '".$data['agentcode']."'";

            $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

            $objSeat = new StdClass();
            $has_seat_and_room = false;

            if(sqlsrv_num_rows($stmt) > 0){
                    $resultObj = sqlsrv_fetch_object($stmt);
                    $objSeat = $resultObj;
                    $has_seat_and_room = true;
            }

            return [
                'has_seat_and_room'=>$has_seat_and_room,
                'data'=>$objSeat,
                'seminarno'=>$data['seminarno'],
                'agentcode'=>$data['agentcode']
            ];


    }

    private function checkSeatNoExist($data = []){

        $query = "select * from SeminarSeats where SeminarNo = '".$data['seminarno']."' and AgentCode != '".$data['agentcode']."' and SeatNo = '".$data['seatno']."'"; 

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt) > 0){
            return [
                'status'=>true,
                'seminarSeatData'=>sqlsrv_fetch_object($stmt)
            ];
        }else{
            return [
                'status'=>false
            ];
        }


    }

    private function getSomeAgentDataByAgentCode($data = []){

        $query = $this->db->select('AgentCode,AgentName,AgentSurName,CardNo')
        ->from('Agent')
        ->where('AgentCode',$data['agent_code'])
        ->get();

        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return new StdClass();
        }
    }


}