<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_product extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct() {
            parent::__construct();

            //$this->load->library(array('upload'));

    }

    public function export_expired_filter(){
    	//echo 'aaaa';



    	if($this->input->post(NULL,FALSE)){
    		//print_r($this->input->post());exit;

    		$this->ExportExpiredWaterFilter();
    	}
    	

    	$export_expired_filter_data = $this->getExpiredFilterData(array(

    	));

    	//print_r($query->result());

    	$data = array(
    		'product_part_notification'=>$export_expired_filter_data
    	);


    	$this->template->content->view('backend/product/export_expired_filter',$data);
    	$this->template->publish();

    }


    private function getExpiredFilterData($data = array()){
    	$arrResult = array();
    	$query = $this->db->select('*')
    	->from('ProductPartNotification')
    	->join('ProductWarranty','ProductPartNotification.serial_no = ProductWarranty.SerialNo')
    	->join('ProductPart','ProductPart.id = ProductPartNotification.ProductPart_id')
    	->where('ProductPartNotification.product_part_next_notification_date < ',date('Y-m-d'))
    	->where('ProductPartNotification.product_part_next_status_id','4')
    	->get();

    	if($query->num_rows() > 0){
    		foreach ($query->result() as $key => $value) {
    			# code...
    			$customer_data = $this->getCustomerData($value->ConsumerID);
    			$technician_data = $this->getTechnicianDataByProductSerialNumber($value->serial_no);
    			$data_push = array(
    				'serial_number'=>$value->serial_no,
    				'part_name'=>$value->part_name,
    				'customer_contact'=>@$customer_data->CustName,
    				'customer_telephone'=>@$customer_data->Telephone,
    				'customer_address'=>@$customer_data->CustAddress,
    				'warranty_expire_date'=>date('d/m/Y',strtotime($value->warranty_expire_date)),
    				'agent_code'=>@$technician_data->AgentCode,
    				'agent_firstname'=>@$technician_data->AgentName,
    				'agent_lastname'=>@$technician_data->AgentSurName,
    				'agent_address'=>@$technician_data->Addr1,
    				'agent_telephone'=>@$technician_data->Telephone

    			);
    			array_push($arrResult, $data_push);
    		}
    	}

    	//print_r($arrResult);exit;
    	return $arrResult;



    }


    private function getCustomerData($CustomerID = 0){
    	$queryCustomer = $this->db->select('CustName,CustID,CustAddress,Telephone')
    	->from('FixitCustomer')
    	->where('CustID',$CustomerID)
    	->get();

    	if($queryCustomer->num_rows() > 0){
    		return $queryCustomer->row();
    	}else{
    		return new StdClass();
    	}
    }

    private function getTechnicianDataByProductSerialNumber($serial_no = ""){

    	$query = $this->db->select('FixITJobProduct.FixITJob_id,FixITJobProduct.SerialNO,FixITJobProduct.Is_install,FixITJob.JobID,FixITJob.AgentCode')
    	->from('FixITJobProduct')
    	->join('FixITJob','FixITJobProduct.FixITJob_id = FixITJob.JobID')
    	->where('FixITJobProduct.SerialNO',$serial_no)
    	->where('FixITJobProduct.Is_install','T')
    	->get();
    	if($query->num_rows() > 0){
    		$row = $query->row();
    		$agent_code = $row->AgentCode;

    		$queryAgent = $this->db->select('AgentCode,AgentName,AgentSurName,Addr1,Telephone')
    		->from('Agent')
    		->where('AgentCode',$agent_code)
    		->get();
    		return $queryAgent->row();
    	}else{
    		return "";
    	}

    }

    private function ExportExpiredWaterFilter(){

    			$expire_filter_data = $this->getExpiredFilterData(array(

    			));

    			$filename = "ExportExpiredWaterFilter[".date('dmY')."].xlsx";


    			require_once 'assets/backend/plugin/PHPExcel/Classes/PHPExcel.php';


		        // Create new PHPExcel object
		        $objPHPExcel = new PHPExcel();

		        //exit;

		        // Set document properties
		        $objPHPExcel->getProperties()->setCreator("EXPIRED WATERFILTER")
		                                     ->setLastModifiedBy("PSIFIXIT Admin")
		                                     ->setTitle($filename)
		                                     ->setSubject("Excel file for expired water filter")
		                                     ->setDescription("Excel file for expire water filter")
		                                     ->setKeywords("Waterfilter")
		                                     ->setCategory("Report");

		        //print_r($arDataOrder);


		        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:K1')->setCellValue('A1',$filename);
		        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		        $objPHPExcel->setActiveSheetIndex(0)
		                    ->setCellValue('A2','#')
		                    ->setCellValue('B2','Serial Number')
		                    ->setCellValue('C2','Part Name')
		                    ->setCellValue('D2','Customer Contact')
		                    ->setCellValue('E2','Customer Telephone')
		                    ->setCellValue('F2','Customer Address')
		                    ->setCellValue('G2','Warranty Expire Date')
		                    ->setCellValue('H2','Agent Code')
		                    ->setCellValue('I2','Agent Firstname&Lastname')
		                    ->setCellValue('J2','Agent Address')
		                    ->setCellValue('K2','Agent Telephone');


		        $count = 1;
		        $table_row = 3;
		        foreach ($expire_filter_data as $key => $value) {
		            # code...
		                        $objPHPExcel->setActiveSheetIndex(0)
		                        ->setCellValue('A'.$table_row.'',@$count)
		                        ->setCellValue('B'.$table_row.'',@$value['serial_number'])
		                        ->setCellValue('C'.$table_row.'',@$value['part_name'])
		                        ->setCellValue('D'.$table_row.'',@$value['customer_contact'])
		                        ->setCellValue('E'.$table_row.'',@$value['customer_telephone'])
		                        ->setCellValue('F'.$table_row.'',@$value['customer_address'])
		                        ->setCellValue('G'.$table_row.'',@$value['warranty_expire_date'])
		                        ->setCellValue('H'.$table_row.'',@$value['agent_code'])
		                        ->setCellValue('I'.$table_row.'',@$value['agent_firstname'].' '.$value['agent_lastname'])
		                        ->setCellValue('J'.$table_row.'',@$value['agent_address'])
		                        ->setCellValue('K'.$table_row.'',@$value['agent_telephone']);
		                        $count++;
		                        $table_row++;
		        }

		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('A')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('B')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('C')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('D')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('E')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('F')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('G')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('H')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('I')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('J')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('K')
		        ->setAutoSize(true);

		        $objPHPExcel->getActiveSheet()->setTitle('ExpiredWaterFilterReport');


		        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
		        $objPHPExcel->setActiveSheetIndex(0);


		        // Redirect output to a client’s web browser (Excel2007)
		        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		        header('Content-Disposition: attachment;filename="'.$filename.'"');
		        header('Cache-Control: max-age=0');
		        // If you're serving to IE 9, then the following may be needed
		        header('Cache-Control: max-age=1');

		        // If you're serving to IE over SSL, then the following may be needed
		        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		        header ('Pragma: public'); // HTTP/1.0

		        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		        $objWriter->save('php://output');
		        exit;
    }


}