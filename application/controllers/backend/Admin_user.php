<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_user extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	private $conn_mysql;
	public function __construct() {
            parent::__construct();

            $this->connect_mysql();
            $this->load->helper(array('backend'));

    }
    public function userlist(){
    		$this->dataTableCSSPackages();
    		$this->dataTableJSPackages();
    		$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/enabledatatable.js'));


    		$dataUser = $this->getAllUser();


    		$data = array(
    			'userlist' => $dataUser
    		);


    		//print_r($data);exit;
	    	$this->template->content->view('backend/user/userlist',$data);
	        $this->template->publish();




    }
    /* Action User*/
    public function createUser(){
    		$this->__createUser();
	}
	public function editUser($id){
			/* check user */
			$sql = "select id from members where id ='".$id."'";

			$result = $this->conn_mysql->query($sql);

			if($result->num_rows > 0){
				$this->__createUser($id);
			}else{
				redirect(base_url('backend/Admin_user/userlist'));
			}


	}
	public function deleteUser($id=0){
				
				$member_data =$this->getMemberDataById($id);
				// print_r($this->getMemberDataById($id));exit;
				if(empty($member_data)){
					redirect(base_url('backend/'.$this->controller.'/userlist'));
				}

				//print_r($member_data);exit;

				/* delete file and folder from uploaded folder */
				if(file_exists('./uploaded/member/'.$member_data['id'].'/'.$member_data['cover'])){
					
					// unlink('./uploaded/member/'.$member_data['id'].'/'.$member_data['cover']);
					// rmdir('./uploaded/member/'.$member_data['id']);
				}

				/* delete from table member_permissions */
				$selectmbpermission = "select * from member_permissions where members_id = '".$member_data['id']."'";

				$result = $this->conn_mysql->query($selectmbpermission);

				//print_r($result->num_rows);exit;
				if($result->num_rows > 0){
					$this->conn_mysql->query("delete from member_permissions where members_id = '".$member_data['id']."'");
				}
				//exit;

				/* eof delete from table member_permissions */

				$sqlDelete = "delete from members where id = '".$member_data['id']."'";

				if($this->conn_mysql->query($sqlDelete)){
						$this->msg->add(__('Delete user success','backend/user/userlist'),'success');
						redirect(base_url('backend/admin_user/userlist'));

				}





	}
	private function __createUser($id=null){

				$this->template->stylesheet->add(base_url('assets/backend/individual/css/imghover/img_hover.css'));
				// print_r($this->account_data);
				$this->dataTableCSSPackages();
	    		$this->dataTableJSPackages();



	    		$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
	    		$this->template->javascript->add(base_url('assets/backend/individual/js/jqueryvalidate/jquery.validate.min.js'));
	    		$this->template->javascript->add(base_url('assets/backend/individual/js/user/createuser.js'));
				$memberData = $this->getMemberDataById($id);

				// print_r($memberData);

				if($this->input->post(NULL,FALSE)){

						if($id){
							$memberData = $this->getMemberDataById($id);

							$post_data = $this->input->post();
							$post_data['id'] = $memberData['id'];
							
							if($this->updateMemberData($post_data)){

								/* update member cover */
								if($_FILES['member_cover']['error'] == 0){
									$this->uploadMemberCover($memberData['id'],'update');
								}

								$this->msg->add(__('Update member data success','backend/user/createuser'),'success');
								redirect(base_url('backend/'.$this->controller.'/editUser/'.$id));
							}else{

								$this->msg->add(__('Can not update member data success','backend/user/createuser'),'error');
								redirect(base_url('backend/'.$this->controller.'/editUser/'.$id));
							}

						}else{

							$post_data = $this->input->post();

							$insert_id = $this->insertMemberData($post_data);

							if($insert_id){
								/* update member cover */
								if($_FILES['member_cover']['error'] == 0){
									$this->uploadMemberCover($insert_id,'create');
								}

								$this->msg->add(__('Create member success','backend/user/createuser'),'success');
								redirect(base_url('backend/'.$this->controller.'/editUser/'.$id));
							}else{

								$this->msg->add(__('Can not create member data','backend/user/createuser'),'error');
								redirect(base_url('backend/'.$this->controller.'/createUser'));
							}



						}
				}


				$data = array(
					'member' => $memberData,
					'member_access_types' => $this->getMemberAccessTypes(),
					'member_menus' => $this->getMemberMenu(),
					'member_permissions' => $this->getMemberPermissions($id)

				);

				// print_r($data);
				$this->template->content->view('backend/user/createuser',$data);
				$this->template->publish();

	}

    public function membermenulist(){
        //echo 'aaaa';
            $this->dataTableCSSPackages();
            $this->dataTableJSPackages();
            $this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/enabledatatable.js'));


            $dataMemberMenu = $this->getAllMemberMenuList();


            $data = array(
                'membermenulist' => $dataMemberMenu
            );


            //print_r($data);exit;
            $this->template->content->view('backend/user/membermenulist',$data);
            $this->template->publish();



    }

    public function createMemberMenu(){

        $this->__createMemberMenu();
    }
    public function editMemberMenu($id){
            /* check member menus */
            $sql = "select id from member_menus where id ='".$id."'";

            $result = $this->conn_mysql->query($sql);

            if($result->num_rows > 0){
                $this->__createMemberMenu($id);
            }else{
                redirect(base_url('backend/Admin_user/membermenulist'));
            }

    }

    public function deleteMemberMenu($id=0){

                $sql = "select * from member_menus where id = '".$id."'";


                $result = $this->conn_mysql->query($sql);

                //print_r($result->num_rows);exit;
                if($result->num_rows > 0){
                    $this->conn_mysql->query("delete from member_menus where id = '".$id."'");
                    $this->msg->add(__('Delete member menus success','backend/user/membermenulist'),'success');
                        redirect(base_url('backend/admin_user/membermenulist'));
                }
                //exit;

    }

    private function __createMemberMenu($id=null){


                $membermenudata = $this->getMemberMenuById($id);

                if($this->input->post(NULL,FALSE)){
                        //print_r($this->input->post());exit;
                        $post_data = $this->input->post();
                        if($id){
                            print_r($post_data);exit;

                        }else{
                            print_r($post_data);exit;

                        }

                }



                $data = array(
                    'membermenu' => $membermenudata
                );


                // print_r($data);
                $this->template->content->view('backend/user/createmembermenu',$data);
                $this->template->publish();


    }




    private function getAllUser(){

    		$dataReturn =array();

    		$sql = "select *,members.id as member_id,members.name as member_name,members.created as member_created from members join member_access_types on members.member_access_types_id = member_access_types.id";

    		$result = $this->conn_mysql->query($sql);

    		while($row = $result->fetch_assoc()){
    			array_push($dataReturn, $row);
    		}

    		return $dataReturn;


    }

    private function getAllMemberMenuList(){

            $dataReturn = array();

            $sql = "select * from member_menus order by id";

            $result = $this->conn_mysql->query($sql);

            while ($row=$result->fetch_assoc()) {
                # code...
                array_push($dataReturn, $row);
            }
            return $dataReturn;

    }

    private function getMemberDataById($id){
    	$sql = "select * from members where id ='".$id."'";

    	$result = $this->conn_mysql->query($sql);

    	return $result->fetch_assoc();
    }
    private function getMemberMenuById($id){
        $sql = "select * from member_menus where id = '".$id."'";

        $result = $this->conn_mysql->query($sql);

        return $result->fetch_assoc();
    }

    private function getMemberAccessTypes(){
    	$dataReturn = array();
    	$sql = "select * from member_access_types";

    	$result = $this->conn_mysql->query($sql);

    	while ($row = $result->fetch_assoc()) {
    		# code...
    		// array_push($dataReturn, $row);
    		$dataReturn[$row['id']] = $row['name'];
    	}

    	return $dataReturn;

    	// print_r($dataReturn);exit;
    }

    private function getMemberMenu(){
    	$dataReturn = array();
    	$sql = "select * from member_menus";
    	$result = $this->conn_mysql->query($sql);

    	while ($row = $result->fetch_assoc()) {
    		# code...
    		array_push($dataReturn, $row);
    	}
    	return $dataReturn;


    }

    private function getMemberPermissions($member_id){
    	$dataReturn = array();

    	$sql = "select * from member_permissions where members_id = '".$member_id."'";
    	$result = $this->conn_mysql->query($sql);

    	while ($row = $result->fetch_assoc()) {
    		# code...
    		array_push($dataReturn, $row);
    	}
    	return $dataReturn;
    	// print_r($dataReturn);exit;

    }
    private function connect_mysql(){

        if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "psifixit";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psifixit";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psisat@2020";
            $dbname = "psifixit";

        }

        // Create connection
        $this->conn_mysql = new mysqli($servername, $username, $password, $dbname);
        $this->conn_mysql->set_charset("utf8");
        // Check connection
        if ($this->conn_mysql->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }

    private function updateMemberData($data = array()){
    	$sqlUpdate = "update members set 
    	name='".$data['name']."',
    	lastname='".$data['lastname']."',
    	email='".$data['email']."',
    	member_access_types_id='".$data['member_access_types_id']."',
    	updated='".date('Y-m-d H:i:s')."',
    	active='".$data['active']."' where id = '".$data['id']."'
    	";

    	return $this->conn_mysql->query($sqlUpdate);

    }
    private function insertMemberData($data = array()){
    	$sqlInsert = "insert into members(
    		name,
    		lastname,
    		email,
    		password,
    		member_access_types_id,
    		created,
    		active
    	)values(
    		'".$data['name']."',
    		'".$data['lastname']."',
    		'".$data['email']."',
    		'".sha1($data['member_password'])."',
    		'".$data['member_access_types_id']."',
    		'".date('Y-m-d H:i:s')."',
    		'".$data['active']."'
    	)";

    	//echo $sqlInsert;exit;
    	$result = $this->conn_mysql->query($sqlInsert);

    	// print_r($result->insert_id);exit;
    	if($result){
    		// echo mysqli_insert_id($this->conn_mysql);exit;
    		return mysqli_insert_id($this->conn_mysql);
    	}else{
    		return false;
    	}
    	

    }

    public function ajaxMemberMenuPermissions(){
    	$data_received = array(
    		'status' => $this->input->post('data_active'),
    		'members_id' => $this->input->post('data_member'),
    		'member_menus_id' => $this->input->post('data_menu')
    	);

    	if($data_received['status'] == 'active'){
    		/* if data equal true find record and remove it */
    		$queryRemoveSelect = "select * from member_permissions where members_id = '".$data_received['members_id']."' and member_menus_id = '".$data_received['member_menus_id']."'";

    		$resultGet = $this->conn_mysql->query($queryRemoveSelect);

    		if($resultGet->num_rows > 0){
    			$queryRemove = "delete from member_permissions where members_id = '".$data_received['members_id']."' and member_menus_id = '".$data_received['member_menus_id']."'";

    			if($this->conn_mysql->query($queryRemove)){
    				echo json_encode(array(
    					'status' => true,
    					'action' => 'set_unactive'
    				));
    				exit;

    			}

    		}

    	}else{
    		/* if data equal false insert new data to table */
    		$default_ability = array(
    			'view'=>true,
    			'edit'=>false,
    			'delete' => false
    		);
    		$queryInsert = "insert into member_permissions(
    			members_id,
    			member_menus_id,
    			event_ability
    		)values(	
    			'".$data_received['members_id']."',
    			'".$data_received['member_menus_id']."',
    			'".json_encode($default_ability)."'
    		)";

    		if($this->conn_mysql->query($queryInsert)){
    			echo json_encode(array(
    				'status' => true,
    				'action' => 'set_active'
    			));
    			exit;

    		}

    	}
    }

    public function ajaxMemberEventPermissions(){
    	$data_received = array(
    		'data_check' => $this->input->post('data_check'),
    		'data_event'=>$this->input->post('data_event'),
    		'members_id' => $this->input->post('data_member'),
    		'member_menus_id' => $this->input->post('data_menu')
    	);
    	//print_r($data_received);exit;

    	/* query to find record */
    	$selectQuery = "select * from member_permissions where members_id = '".$data_received['members_id']."' and member_menus_id = '".$data_received['member_menus_id']."'";

    	$resultSelect = $this->conn_mysql->query($selectQuery);

    	if($resultSelect->num_rows > 0){
    		$rowResult = $resultSelect->fetch_assoc();
    		$dataEvent = json_decode($rowResult['event_ability'],true);

    		if($data_received['data_check'] == 'checked'){
    			$dataEvent[$data_received['data_event']] = true;
    		}else{
    			$dataEvent[$data_received['data_event']] = false;
    		}

    		/* update record after check or unchec */
    		$sqlUpdate = "update member_permissions set event_ability = '".json_encode($dataEvent)."' where members_id = '".$data_received['members_id']."' and member_menus_id = '".$data_received['member_menus_id']."'";
    		if($this->conn_mysql->query($sqlUpdate)){
    			echo json_encode(array(
    				'status'=>true,
    				'event'=>$data_received['data_event'],
    				'action'=>$data_received['data_check']
    			));
    		}else{
    			echo json_encode(array(
    				'status' => false
    			));
    		}

    	}else{
    		echo json_encode(array(
    			'status' => false
    		));
    	}


    }

    public function ajaxMemberResetPassword(){
    	$data_received = array(
    		'new_password' => $this->input->post('new_password'),
    		'members_id'=>$this->input->post('members_id')
    	);

    	$sqlUpdate = "update members set password = '".sha1($data_received['new_password'])."' where id = '".$data_received['members_id']."'";

    	if($this->conn_mysql->query($sqlUpdate)){
    		echo json_encode(array(
    			'status' => true
    		));
    	}else{
    		echo json_encode(array(
    			'status' => false
    		));
    	}

    }

    private function uploadMemberCover($member_id=0,$status=""){
    						$this->load->library(array('upload'));

    						$config = array();
							if(!is_dir('uploaded/member/'.$member_id.'')){
                                mkdir('uploaded/member/'.$member_id.'',0777,true);
                            }
                            clearDirectory('uploaded/member/'.$member_id.'/');
                                                     
                            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                            $config['upload_path'] = 'uploaded/member/'.$member_id.'/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '2000';

                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload('member_cover')){
                                $error = array('error' => $this->upload->display_errors());
                                $this->msg->add(__($error['error'],'default'), 'error');
                                if(file_exists('uploaded/member/'.$member_id)){
                                        rmdir('uploaded/member/'.$member_id);
                                }
                                if($status == "create"){
	                                if($this->conn_mysql->query("delete from members where id = '".$member_id."'")){
	                                	redirect(base_url('backend/admin_user/userlist'));
	                                }
                            	}
                                
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());

                                if($this->conn_mysql->query("update members set cover = '".$data_upload['upload_data']['file_name']."' where id='".$member_id."'")){
                                	return true;
                                }
                                
                            }
                            return false;
    }

    private function insertMemberMenuData($post_data){

            $sql = "insert into member_menus(
                name,
                controller_name,
                method_name,
                created,
                active
            )values(
                '".$post_data['name']."',
                '".$post_data['controller_name']."',
                '".$post_data['method_name']."',
                '".date('Y-m-d H:i:s')."',
                '".$post_data['active']."'
            )";

            return $this->conn_mysql->query($sql);

    }

    private function updateMemberMenuDate($post_data){
            $sql = "update member_menus set name='".$post_data['name']."',
            controller_name = '".$post_data['controller_name']."',
            method_name = '".$post_data['method_name']."',
            updated = '".date('Y-m-d H:i:s')."',
            active = '".$post_data['active']."' 
            ";

            return $this->conn_mysql->query($sql);

    }


}