<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_customer extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
            parent::__construct();
            $this->load->library(array('pagination'));
    }
    public function index($page = null){
    	$data = array();
        $this->page_num = 10;
        $this->template->javascript->add(base_url('assets/backend/individual/js/customer/list_customer.js'));


        
        if($this->input->get(NULL,FALSE)){
        	
            $customer = new M_fixitcustomer();

            if($this->input->get('customer_id')){
                $customer->like('CustID',$this->input->get('customer_id'),'both');

            }else if($this->input->get('customer_name')){
                $customer->like('CustName',$this->input->get('customer_name'),'both');

            }else if($this->input->get('customer_email')){
                $customer->where('Email',$this->input->get('customer_email'));

            }

            $clone = $customer->get_clone();
            $CustomerCount = $clone->count();
            $customer->limit($this->page_num,$page);
            $customer->get();

            

        }else{

            $customer = new M_fixitcustomer();
            $clone = $customer->get_clone();
            $CustomerCount = $clone->count();
            $customer->limit($this->page_num,$page);
            $customer->get();

        }
        


        $data['customer'] = $customer;
        $this->config_page($CustomerCount,$page);

        $config_page = array();
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url('backend/Admin_customer').'/';
        $this->pagination->initialize($config_page);
        $data['pages'] = $this->pagination->create_links();



    	$this->template->content->view('backend/customer/list_customer',$data);
        $this->template->publish();

    	
    }

    public function create(){



    }
    private function __create($id = null){
    	$data = array();
    	$data['count_customer_product'] = 0;
    	$data['customer_product'] = "";

    	$customer = new M_fixitcustomer();

    	if($id){
    		$customer->where('CustID',$id)->get();
    		$customer_product = new M_productwarranty();
    		$customer_product->where('ConsumerID',$customer->CustID)->get();
    		if($customer_product->result_count() > 0){
    			$data['count_customer_product'] = $customer_product->result_count();
    			$data['customer_product'] = $customer_product;
    		}

    	}else{
    		$customer = $customer->get();
    	}
    	
    	if($this->input->post(NULL,FALSE)){


    	}
    	
    	$data['customer'] = $customer;


    	// echo timeAgo(date('Y-m-d H:i:s'));exit;
    	$this->template->content->view('backend/customer/create',$data);
        $this->template->publish();



    }

    public function edit($id = null){

    	$customer = new M_fixitcustomer();
    	$customer->where('CustID',$id)->get();

    	if($customer->result_count() > 0){
    		$this->__create($id);
    	}else{
    		redirect($this->controller);
    	}
    				
    }

    public function view($id = null){
    	$this->edit($id);
    }



}