<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_additional extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	private $conn_mysql;
	public function __construct() {
            parent::__construct();

            $this->connect_mysql();

    }
    public function registerofivelist(){
    		$dataRegister = array();
    		$this->dataTableCSSPackages();
    		$this->dataTableJSPackages();
    		$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/enabledatatable.js'));

    		$query = "select * from insertsn_logs order by created desc";

    		$result = $this->conn_mysql->query($query);

	        if($result->num_rows > 0){
	          while($row = $result->fetch_assoc()){
	          		array_push($dataRegister, array(
	          			'id' => $row['id'],
	          			'firstname' => $row['firstname'],
	          			'lastname' => $row['lastname'],
	          			'telephone' => $row['telephone'],
	          			'logs' => json_decode($row['logs']),
	          			'created' => $row['created']
	          		));
	          }

	          

	      	}

    		
    		$data = array(
    			'registerdata' => $dataRegister
    		);

	    	$this->template->content->view('backend/additional/registerofivelist',$data);
	        $this->template->publish();




    }

    private function connect_mysql(){

        if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "registerofive";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "registerofive";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "registerofive";

        }

        // Create connection
        $this->conn_mysql = new mysqli($servername, $username, $password, $dbname);
        $this->conn_mysql->set_charset("utf8");
        // Check connection
        if ($this->conn_mysql->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }

}