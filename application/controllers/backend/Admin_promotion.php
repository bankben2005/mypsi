<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_promotion extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $seminar_conn;
	private $db_config;
	public function __construct() {
            parent::__construct();

            $this->load->library(array('upload'));
    }

    public function index(){
    		$this->couponlist();

    }

    public function couponlist(){
    		$this->dataTableCSSPackages();
    		$this->dataTableJSPackages();
    		$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/enabledatatable.js'));

    		$coupon = new M_coupon();


    		$data = array(
	    		'coupon' => $coupon,
	    		
	    	);

	    	$this->template->content->view('backend/promotion/couponlist',$data);
	        $this->template->publish();

    }

    /* Action Questionnaire Set*/
    public function createCoupon(){
    		$this->__createCoupon();
	}
	public function editCoupon($id){
					  $coupon = new M_coupon($id);
                      
                      if($coupon->getId()){
                          $this->__createCoupon($id);
                      }else{
                          redirect(base_url('backend/'.$this->controller.'/couponlist'));
                      }

	}
	public function deleteCoupon($id=0){
					$coupon = new M_coupon($id);
					if($coupon ->id){

						$files1 = glob('uploaded/coupon/coupon_image/'.$coupon->id.'/*');
						$files2 = glob('uploaded/coupon/coupon_imageused/'.$coupon->id.'/*');
						$files3 = glob('uploaded/coupon/coupon_imageexpired/'.$coupon->id.'/*');
						 // get all file names
						foreach($files1 as $file){ // iterate files
							if(is_file($file))
							unlink($file); // delete file
						}
						foreach($files2 as $file){ // iterate files
							if(is_file($file))
							unlink($file); // delete file
						}
						foreach($files3 as $file){ // iterate files
							if(is_file($file))
							unlink($file); // delete file
						}

						if(is_dir('uploaded/coupon/coupon_image/'.$coupon->id)){
						  	rmdir('uploaded/coupon/coupon_image/'.$coupon->id);
					  	}
					  	if(is_dir('uploaded/coupon/coupon_imageused/'.$coupon->id)){
						  	rmdir('uploaded/coupon/coupon_imageused/'.$coupon->id);
					  	}
					  	if(is_dir('uploaded/coupon/coupon_imageexpired/'.$coupon->id)){
						  	rmdir('uploaded/coupon/coupon_imageexpired/'.$coupon->id);
					  	}

						if($coupon ->delete()){
							$this->msg->add(__('Delete coupon success','backend/promotion/couponlist'),'success');

							redirect(base_url('backend/'.$this->controller.'/couponlist'));
						}
					}else{
						redirect(base_url('backend/'.$this->controller.'/couponlist'));
					}
	}
	private function __createCoupon($id=null){

			$this->template->stylesheet->add(base_url('assets/backend/individual/css/bootstrap-daterangepicker/daterangepicker.css'));
			$this->template->stylesheet->add(base_url('assets/backend/individual/css/imghover/img_common.css'));
			$this->template->stylesheet->add(base_url('assets/backend/individual/css/imghover/img_hover.css'));


			$this->template->javascript->add(base_url('assets/backend/individual/js/bootstrap-daterangepicker/moment.min.js'));
			$this->template->javascript->add(base_url('assets/backend/individual/js/bootstrap-daterangepicker/daterangepicker.js'));

			$this->template->javascript->add(base_url('assets/backend/individual/js/jqueryvalidate/jquery.validate.min.js'));
			$this->template->javascript->add(base_url('assets/backend/individual/js/coupon/createcoupon.js'));

			$coupon = new M_coupon($id);


			if($this->input->post(NULL,FALSE)){


				

				// print_r($this->input->post());exit;
				$coupon->Code = $this->input->post('Code');
				$coupon->Name = $this->input->post('Name');
				$coupon->Description = $this->input->post('Description');
				$coupon->PointUse = ($this->input->post('PointUse'))?$this->input->post('PointUse'):0;
				$coupon->DiscountRate = $this->input->post('DiscountRate');
				$coupon->DiscountType = $this->input->post('DiscountType');
				$coupon->DiscountCurrency = 'THB';
				$coupon->CouponType_id=  $this->input->post('CouponType_id');

				/* Duration Data */
				$startDate = "";
				$endDate = "";
				$exDuration = explode(' - ', $this->input->post('Duration'));
				$startDate = new DateTime($exDuration[0]);
				$endDate = new DateTime($exDuration[1]);
				/* Eof Duration Data */



				$coupon->StartDate = $startDate->format('Y-m-d H:i:s');
				$coupon->EndDate =$endDate->format('Y-m-d H:i:s');

				if($id){
					$coupon->Updated = date('Y-m-d H:i:s');
				}else{
					$coupon->Created = date('Y-m-d H:i:s');
				}

				if($coupon->save()){



						/* save coupon image */
						if($_FILES['coupon_image']['error'] == 0){
							$this->uploadCouponImage($coupon);
						}
						if($_FILES['coupon_imageused']['error'] == 0){
							$this->uploadCouponImageUsed($coupon);
						}
						if($_FILES['coupon_imageexpired']['error'] == 0){
							$this->uploadCouponImageExpired($coupon);
						}
						/* eof save coupon image */


						$txt_title = ($id)?__('Edit','backend/default'):__('Create','backend/default');
                        $this->msg->add($txt_title.__('Complete','backend/default'),'success');
                        redirect($this->uri->uri_string());



				}


				





			}

			$str_date = "";
			$end_date = "";

			if($coupon->id){
    			$str_date = new DateTime($coupon->StartDate);
    			$end_date = new DateTime($coupon->EndDate);

    		}

			$data = array(
				'coupon' => $coupon,
				'coupontype' => $this->getAllCouponType(),
				'duration' => ($str_date && $end_date)?$str_date->format('d-m-Y').' - '.$end_date->format('d-m-Y'):""
			);


			$this->template->content->view('backend/promotion/createcoupon',$data);
			$this->template->publish();




	}


	private function getAllCouponType(){
		$arReturn = array();

		$coupontype = new M_coupontype();
		foreach ($coupontype->get() as $key => $value) {
			# code...
			$arReturn[$value->id] = $value->Name;
		}
		return $arReturn;
	}

	private function uploadCouponImage($coupon){
							$config = array();
							if(!is_dir('uploaded/coupon/coupon_image/'.$coupon->id.'')){
                                mkdir('uploaded/coupon/coupon_image/'.$coupon->id.'',0777,true);
                            }
                            clearDirectory('uploaded/coupon/coupon_image/'.$coupon->id.'/');
                                                     
                            $config['file_name'] = 'coupon'.md5(strtotime(date('Y-m-d H:i:s')));
                            $config['upload_path'] = 'uploaded/coupon/coupon_image/'.$coupon->id.'/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '2000';

                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload('coupon_image')){
                                $error = array('error' => $this->upload->display_errors());
                                $this->msg->add(__($error['error'],'default'), 'error');
                                if(file_exists('uploaded/coupon/coupon_image/'.$coupon->id)){
                                        rmdir('uploaded/coupon/coupon_image/'.$coupon->id);
                                }
                                if(!$coupon->id){$coupon->delete();}
                                if($coupon->id){
                                    redirect(base_url('backend/').$this->controller.'/editCoupon/'.$id);   
                                }else{
                                    redirect(base_url('bakcend/').$this->controller.'/createCoupon');
                                }
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());
                                $coupon_update = new M_coupon($coupon->id);
                                $coupon_update->Image = $data_upload['upload_data']['file_name'];
                                if($coupon_update->save()){
                                	return true;
                                }
                            }
                            return false;

	}
	private function uploadCouponImageUsed($coupon){
							$config = array();
							if(!is_dir('uploaded/coupon/coupon_imageused/'.$coupon->id.'')){
                                mkdir('uploaded/coupon/coupon_imageused/'.$coupon->id.'',0777,true);
                            }
                            clearDirectory('uploaded/coupon/coupon_imageused/'.$coupon->id.'/');
                                                     
                            $config['file_name'] = 'couponused'.md5(strtotime(date('Y-m-d H:i:s')));
                            $config['upload_path'] = 'uploaded/coupon/coupon_imageused/'.$coupon->id.'/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '2000';
                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload('coupon_imageused')){
                                $error = array('error' => $this->upload->display_errors());
                                $this->msg->add(__($error['error'],'default'), 'error');
                                if(file_exists('uploaded/coupon/coupon_imageused/'.$coupon->id)){
                                        rmdir('uploaded/coupon/coupon_imageused/'.$coupon->id);
                                }
                                if(!$coupon->id){$coupon->delete();}
                                if($coupon->id){
                                    redirect(base_url('backend/').$this->controller.'/editCoupon/'.$id);   
                                }else{
                                    redirect(base_url('backend/').$this->controller.'/createCoupon');
                                }
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());
                                $coupon_update = new M_coupon($coupon->id);
                                $coupon_update->ImageUsed = $data_upload['upload_data']['file_name'];
                                if($coupon_update->save()){
                                	return true;
                                }
                            }
                            return false;

	}
	private function uploadCouponImageExpired($coupon){
							$config = array();
							if(!is_dir('uploaded/coupon/coupon_imageexpired/'.$coupon->id.'')){
                                mkdir('uploaded/coupon/coupon_imageexpired/'.$coupon->id.'',0777,true);
                            }
                            clearDirectory('uploaded/coupon/coupon_imageexpired/'.$coupon->id.'/');
                                                     
                            $config['file_name'] = 'couponexpired'.md5(strtotime(date('Y-m-d H:i:s')));
                            $config['upload_path'] = 'uploaded/coupon/coupon_imageexpired/'.$coupon->id.'/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '2000';
                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload('coupon_imageexpired')){
                                $error = array('error' => $this->upload->display_errors());
                                $this->msg->add(__($error['error'],'default'), 'error');
                                if(file_exists('uploaded/coupon/coupon_imageexpired/'.$coupon->id)){
                                        rmdir('uploaded/coupon/coupon_imageexpired/'.$coupon->id);
                                }
                                if(!$coupon->id){$coupon->delete();}
                                if($coupon->id){
                                    redirect(base_url('backend/').$this->controller.'/editCoupon/'.$id);   
                                }else{
                                    redirect(base_url('backend/').$this->controller.'/createCoupon');
                                }
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());
                                $coupon_update = new M_coupon($coupon->id);
                                $coupon_update->ImageExpired = $data_upload['upload_data']['file_name'];
                                if($coupon_update->save()){
                                	return true;
                                }
                            }
                            return false;

	}

	public function delete_image($coupon_id,$coupon_status){

			/* get all coupon image */
			$allcouponimage = $this->getAllCouponImage($coupon_id);

			if(!$allcouponimage){
				$this->msg->add(__('Coupon not found','backend/promotion/createcoupon'),'error');
				redirect(base_url('backend/admin_promotion/editCoupon/'.$coupon_id));
			}

			switch ($coupon_status) {
				case 'image':
					if(file_exists('./uploaded/coupon/coupon_image/'.$coupon_id.'/'.$allcouponimage->Image)){
						if(unlink('./uploaded/coupon/coupon_image/'.$coupon_id.'/'.$allcouponimage->Image)){
							$update_coupon = $this->db->update('Coupon',array('Image'=>''),array('id'=>$coupon_id));
							$this->msg->add(__('Delete coupon image success','backend/promotion/createcoupon'),'success');
							redirect(base_url('backend/admin_promotion/editCoupon/'.$coupon_id));
						}
					}
				break;
				case 'used':
					if(file_exists('./uploaded/coupon/coupon_imageused/'.$coupon_id.'/'.$allcouponimage->ImageUsed)){
						if(unlink('./uploaded/coupon/coupon_imageused/'.$coupon_id.'/'.$allcouponimage->ImageUsed)){
							$update_coupon = $this->db->update('Coupon',array('ImageUsed'=>''),array('id'=>$coupon_id));
							$this->msg->add(__('Delete coupon image success','backend/promotion/createcoupon'),'success');
							redirect(base_url('backend/admin_promotion/editCoupon/'.$coupon_id));
						}
					}
				break;
				case 'expired':
					if(file_exists('./uploaded/coupon/coupon_imageexpired/'.$coupon_id.'/'.$allcouponimage->ImageExpired)){
						if(unlink('./uploaded/coupon/coupon_imageexpired/'.$coupon_id.'/'.$allcouponimage->ImageExpired)){
							$update_coupon = $this->db->update('Coupon',array('ImageExpired'=>''),array('id'=>$coupon_id));
							$this->msg->add(__('Delete coupon image success','backend/promotion/createcoupon'),'success');
							redirect(base_url('backend/admin_promotion/editCoupon/'.$coupon_id));
						}
					}
				break;
				
				default:
					if(file_exists('./uploaded/coupon/coupon_image/'.$coupon_id.'/'.$allcouponimage->Image)){
						if(unlink('./uploaded/coupon/coupon_image/'.$coupon_id.'/'.$allcouponimage->Image)){
							$update_coupon = $this->db->update('Coupon',array('Image'=>''),array('id'=>$coupon_id));
							$this->msg->add(__('Delete coupon image success','backend/promotion/createcoupon'),'success');
							redirect(base_url('backend/admin_promotion/editCoupon/'.$coupon_id));
						}
					}
				break;
			}

	}

	private function getAllCouponImage($coupon_id = 0){
		$query = $this->db->select('id,Image,ImageUsed,ImageExpired')
		->from('Coupon')
		->where('id',$coupon_id)
		->get();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function ajaxGetCouponGenerateStatus(){

		$data_received = array(
			'coupontype_id' => $this->input->post('coupontype_id')
		);

		$query = $this->db->select('*')
		->from('CouponType')
		->where('id',$data_received['coupontype_id'])
		->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			echo json_encode(array(
				'status' => true,
				'isgenerate' => ($row->IsGenerateCode)?true:false
			));
		}else{
			echo json_encode(array(
				'status' => false
			));

		}

		
	}




}