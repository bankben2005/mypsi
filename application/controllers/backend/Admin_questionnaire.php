<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_questionnaire extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $seminar_conn;
	private $db_config;
	public function __construct() {
            parent::__construct();

    }

    public function index(){
    		$this->dataTableCSSPackages();
    		$this->dataTableJSPackages();
    		$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/enabledatatable.js'));

    		$questionnaire = new M_questionnaire();


    		$data = array(
	    		'questionnaire' => $questionnaire,
	    		
	    	);

	    	$this->template->content->view('backend/questionnaire/questionnairelist',$data);
	        $this->template->publish();


    }

    public function questionnaireSetList(){
    		$this->dataTableCSSPackages();
    		$this->dataTableJSPackages();
    		$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/enabledatatable.js'));

    		$questionnaireset = new M_questionnaireset();



    		$data = array(
    			'questionnaireset' => $questionnaireset
    		);

    		$this->template->content->view('backend/questionnaire/questionnairesetlist',$data);

    		$this->template->publish();


    }
    public function questionnaireCategory(){
    		$this->dataTableCSSPackages();
    		$this->dataTableJSPackages();
    		$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/enabledatatable.js'));

    		$questionnairecategory = new M_questionnairecategory();

    		$data = array(
    			'questionnairecategory' => $questionnairecategory
    		);

    		$this->template->content->view('backend/questionnaire/questionnairecategorylist',$data);

    		$this->template->publish();
    }

    public function questionnaireAnswerList(){
    	$this->dataTableCSSPackages();
    	$this->dataTableJSPackages();
    	$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/enabledatatable.js'));

    	$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/questionnaireanswerlist.js'));

    	$setid = $this->getQuestionnaireSetAvailable();
    	$questionnaireanswer = new StdClass();

    	$query = $this->db->select('*,QuestionnaireAnswer.id as answer_id,QuestionnaireAnswer.created as answer_created')
    	->from('QuestionnaireAnswer')
    	->join('QuestionnaireSet','QuestionnaireAnswer.QuestionnaireSet_id = QuestionnaireSet.id')
    	->where('QuestionnaireAnswer.QuestionnaireSet_id',($this->input->get('questionnaireset'))?$this->input->get('questionnaireset'):$setid)->get();


    	$questionnaireanswer = ($query->num_rows() > 0)?$query->result():$questionnaireanswer;

    	if(count($questionnaireanswer) > 0){
    			foreach ($questionnaireanswer as $key => $value) {
    				# code...
    				$questionnaireanswer[$key]->{'Assessor'} = $this->getAgentAnswerData($value->AgentCode);
    				$questionnaireanswer[$key]->{'Assessment'} = $this->getAgentAnswerData($value->SUNAME);
    				$questionnaireanswer[$key]->{'summary_percent'} = $this->getQuestionnaireSummaryPerCent(array('answer_id'=>$value->answer_id));
    			}

    	}
    	
    	$data = array(
    		'questionnaireanswer' => $questionnaireanswer,
    		'questionnaireset'=>$this->getAllQuestionnaireSet()
    	);

    	//print_r($data);

    	$this->template->content->view('backend/questionnaire/questionnaireanswerlist',$data);
    	$this->template->publish();


    }

    public function viewQuestionnaireAnswerDetail($answer_id=""){

    	$answerdata = new StdClass();
    	$agentrow = new StdClass();
    	$sunamedata = new StdClass();

    	$query = $this->db->select('*,QuestionnaireSet.Name as setname,QuestionnaireRate.Name as rate_name')
    	->from('QuestionnaireAnswer')
    	->join('QuestionnaireAnswerList','QuestionnaireAnswer.id = QuestionnaireAnswerList.QuestionnaireAnswer_id')
    	->join('Questionnaire','QuestionnaireAnswerList.Questionnaire_id = Questionnaire.id')
    	->join('QuestionnaireSet','QuestionnaireAnswer.QuestionnaireSet_id = QuestionnaireSet.id')
    	->join('QuestionnaireRate','QuestionnaireAnswerList.QuestionnaireRate_id = QuestionnaireRate.id')
    	->join('QuestionnaireCategory','QuestionnaireCategory.id = Questionnaire.QuestionnaireCategory_id')
    	->where('QuestionnaireAnswer.id',$answer_id)
    	->get();

    	//echo $this->db->last_query();exit;

    	if($query->num_rows() <= 0){
    		redirect(base_url('backend/'.$this->controller.'/questionnaireAnswerList'));

    	}else{
    		$answerdata = $query->result();
    		/* get agent data  */
    		$agentrow = $this->getAgentAnswerData($answerdata[0]->AgentCode);

    		$sunamedata = $this->getAgentAnswerData($answerdata[0]->SUNAME);
    		//print_r($agentrow);

    	}

    	//print_r($query->result());


    	$data = array(
    		'answerdata' => $answerdata,
    		'agentdata' => $agentrow,
    		'sunamedata' => $sunamedata
    	);

    	// print_r($data);
    	$this->template->content->view('backend/questionnaire/questionnaireanswerdetail',$data);
    	$this->template->publish();


    }

    public function deleteQuestionnaireAnswer($questionnaireanswer_id = ""){

    		$query = $this->db->select('*')
    		->from('QuestionnaireAnswer')
    		->where('id',$questionnaireanswer_id)
    		->get();

    		if($query->num_rows() > 0){
    			$rowData =$query->row();
    			/* delete coupon agent if found*/
    			$coupon = $this->getCouponAvailable();
    			if($coupon){
    					$this->db->delete('CouponAgent',array('AgentCode'=>$rowData->AgentCode,'Coupon_id'=>$coupon->id));
    			}

    			/* delete anwserlist */
    					$this->db->delete('QuestionnaireAnswerList',array('QuestionnaireAnswer_id'=>$rowData->id));

    					if($this->db->delete('QuestionnaireAnswer',array('id'=>$rowData->id))){


			                $this->msg->add(__('Delete questionnaire answer success!','backend/questionnaire/questionnaireanswerlist'),'success');
			                redirect(base_url('backend/'.$this->controller.'/questionnaireAnswerList'));
    					}

    		}else{

    			redirect(base_url('backend/'.$this->controller.'/questionnaireAnswerList'));

    		}


    }


    /* Action Questionnaire Set*/
    public function createQuestionnaireSet(){
    		$this->__createQuestionnaireSet();
	}
	public function editQuestionnaireSet($id){
					  $questionnaireset = new M_questionnaireset($id);
                      
                      if($questionnaireset->getId()){
                          $this->__createQuestionnaireSet($id);
                      }else{
                          redirect(base_url('backend/'.$this->controller.'/questionnaireSetList'));
                      }

	}
	public function deleteQuestionnaireSet($id=0){
					$questionnaireset = new M_questionnaireset($id);
					if($questionnaireset->id){
						if($questionnaireset->delete()){
							$this->msg->add(__('Delete questionnaire set success','backend/questionnaire/questionnairesetlist'),'success');

							redirect(base_url('backend/'.$this->controller.'/questionnaireSetList'));
						}
					}else{
						redirect(base_url('backend/'.$this->controller.'/questionnaireSetList'));
					}
	}

	private function __createQuestionnaireSet($id=null){

			$this->template->stylesheet->add(base_url('assets/backend/individual/css/bootstrap-daterangepicker/daterangepicker.css'));

			$this->template->javascript->add(base_url('assets/backend/individual/js/bootstrap-daterangepicker/moment.min.js'));
			$this->template->javascript->add(base_url('assets/backend/individual/js/bootstrap-daterangepicker/daterangepicker.js'));
			$this->template->javascript->add(base_url('assets/backend/individual/js/questionnaire/createquestionnaireset.js'));


			$questionnaireset = new M_questionnaireset($id);

			if($this->input->post(NULL,FALSE)){

				/* Duration Data */
				$startDate = "";
				$endDate = "";
				$exDuration = explode(' - ', $this->input->post('Duration'));
				$startDate = new DateTime($exDuration[0]);
				$endDate = new DateTime($exDuration[1]);
				/* Eof Duration Data */


				$questionnaireset->Name = $this->input->post('Name');
				$questionnaireset->Description = $this->input->post('Description');
				$questionnaireset->StartDate = $startDate->format('Y-m-d H:i:s');
				$questionnaireset->EndDate = $endDate->format('Y-m-d H:i:s');
				$questionnaireset->Active = $this->input->post('Active');

				if($questionnaireset->id){
					$questionnaireset->Updated = date('Y-m-d H:i:s');
				}else{
					$questionnaireset->Created = date('Y-m-d H:i:s');
				}


				if($questionnaireset->save()){
					$txt_title = ($id)?__('Edit','backend/questionnaire/default'):__('Create','backend/questionnaire/default');
	                $this->msg->add($txt_title.__('Complete','backend/questionnaire/backend'),'success');
	                redirect($this->uri->uri_string());

            	}



				// print_r($exDuration);
    // 			print_r($this->input->post());exit;




    		}

    		$Duration = "";
    		$StartDate = "";
    		$EndDate = "";


    		if($questionnaireset->id){
    			$StartDate = new DateTime($questionnaireset->StartDate);
    			$EndDate = new DateTime($questionnaireset->EndDate);

    		}
			$data = array(
				'questionnaireset' => $questionnaireset,
				'duration' => ($StartDate && $EndDate)?$StartDate->format('d-m-Y').' - '.$EndDate->format('d-m-Y'):""
			);

			$this->template->content->view('backend/questionnaire/createquestionnaireset',$data);
			$this->template->publish();

	}


	//========================== Eof questionnaire set ============================



	//========================== Start Questionnaire Category =====================

	public function createQuestionnaireCategory(){
    		$this->__createQuestionnaireCategory();
	}
	public function editQuestionnaireCategory($id){
					  $questionnairecategory = new M_questionnairecategory($id);
                      
                      if($questionnairecategory->getId()){
                          $this->__createQuestionnaireCategory($id);
                      }else{
                          redirect(base_url('backend/'.$this->controller.'/questionnaireCategory'));
                      }

	}
	public function deleteQuestionnaireCategory($id=0){
					$questionnairecategory = new M_questionnairecategory($id);
					if($questionnairecategory->id){
						if($questionnairecategory->delete()){
							$this->msg->add(__('Delete questionnaire category success','backend/questionnaire/questionnairecategorylist'),'success');

							redirect(base_url('backend/'.$this->controller.'/questionnaireCategory'));
						}
					}else{
						redirect(base_url('backend/'.$this->controller.'/questionnaireCategory'));
					}


	}
	private function __createQuestionnaireCategory($id=null){
			$questionnairecategory = new M_questionnairecategory($id);


			if($this->input->post(NULL,FALSE)){
				// print_r($this->input->post());exit;
				$questionnairecategory->Name = $this->input->post('Name');
				$questionnairecategory->Active = $this->input->post('Active');

				if($questionnairecategory->id){
					$questionnairecategory->Updated = date('Y-m-d H:i:s');
				}else{
					$questionnairecategory->Created = date('Y-m-d H:i:s');
				}

				if($questionnairecategory->save()){

					$txt_title = ($id)?__('Edit','backend/questionnaire/default'):__('Create','backend/questionnaire/default');
	                $this->msg->add($txt_title.__('Complete','backend/questionnaire/createquestionnairecategory'),'success');
	                redirect($this->uri->uri_string());

				}


			}



			$data = array(
				'questionnairecategory' => $questionnairecategory
			);


			$this->template->content->view('backend/questionnaire/createquestionnairecategory',$data);
			$this->template->publish();


	}



	//========================== Eof Questionnaire Category =======================




	//========================== Action for Questionnaire =========================
	public function createQuestionnaire(){
    		$this->__createQuestionnaire();
	}
	public function editQuestionnaire($id){
					  $questionnaire = new M_questionnaire($id);
                      
                      if($questionnaire->getId()){
                          $this->__createQuestionnaire($id);
                      }else{
                          redirect(base_url('backend/'.$this->controller.'/index'));
                      }

	}
	public function deleteQuestionnaire($id=0){
					$questionnaire = new M_questionnaire($id);
					if($questionnaire->id){
						if($questionnaire->delete()){
							$this->msg->add(__('Delete questionnaire success','backend/questionnaire/index'),'success');

							redirect(base_url('backend/'.$this->controller.'/index'));
						}
					}else{
						redirect(base_url('backend/'.$this->controller.'/index'));
					}


	}
	private function __createQuestionnaire($id=null){
			$questionnaire = new M_questionnaire($id);


			if($this->input->post(NULL,FALSE)){

				//print_r($this->input->post());exit;
				$questionnaire->Question = $this->input->post('Question');
				$questionnaire->QuestionnaireSet_id = $this->input->post('QuestionnaireSet_id');
				$questionnaire->QuestionnaireCategory_id = $this->input->post('QuestionnaireCategory_id');
				$questionnaire->Active = $this->input->post('Active');
				$questionnaire->Type = $this->input->post('Type');

				if($questionnaire->id){
					$questionnaire->Updated = date('Y-m-d H:i:s');
				}else{
					$questionnaire->Created = date('Y-m-d H:i:s');
				}

				if($questionnaire->save()){
						$txt_title = ($id)?__('Edit','backend/questionnaire/default'):__('Create','backend/questionnaire/default');
		                $this->msg->add($txt_title.__('Complete','backend/questionnaire/createquestionnaire'),'success');
		                redirect($this->uri->uri_string());

				}

			}

			//echo $questionnaire->Type;exit;

			$data = array(
				'questionnaire' => $questionnaire,
				'questionnaireset' => $this->getAllSetAvailable(),
	    		'questionnairecategory' => $this->getAllCategoryAvailable()
			);

			// echo $questionnaire->Question;exit;

			//print_r($data);exit;

			$this->template->content->view('backend/questionnaire/createquestionnaire',$data);
			$this->template->publish();



	}

	//========================== Eof action for questionnaire =====================







	private function getAllSetAvailable(){
		$arData = array();
		$questionnaireset = new M_questionnaireset();
		$questionnaireset->where('Active',1)->get();

		foreach ($questionnaireset as $key => $value) {
			//print_r($value);
			$arData[$value->id] = $value->Name;
		}
		//exit;
		return $arData;
	}

	private function getAllCategoryAvailable(){
		$arData = array();
		$questionnairecategory = new M_questionnairecategory();
		$questionnairecategory->where('Active',1)->get();

		foreach ($questionnairecategory as $key => $value) {
			# code...
			$arData[$value->id] = $value->Name;
		}
		return $arData;
		// return $questionnairecategory;
	}

	private function getQuestionnaireSetAvailable(){
		$query = $this->db->select('*')
		->from('QuestionnaireSet')
		->where('EndDate >=',date('Y-m-d'))
		->get();
		if($query->num_rows() > 0){
			return $query->row()->id;
		}else{
			return 0;
		}
	}

	private function getAgentAnswerData($agentcode){

		$query = $this->db->select('AgentCode,AgentName,AgentSurName,AgentType,TradeName,Province,Telephone')
		->from('Agent')
		->where('AgentCode',$agentcode)
		->get();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}



	}
	private function getQuestionnaireSummaryPerCent($arData){

		$query = $this->db->select('SUM(QuestionnaireRate.Rate) as sumrate')
		->from('QuestionnaireAnswerList')
		->join('QuestionnaireRate','QuestionnaireAnswerList.QuestionnaireRate_id = QuestionnaireRate.id')
		->where('QuestionnaireAnswerList.QuestionnaireAnswer_id',$arData['answer_id'])
		->get();
		$row = $query->row();

		$queryCountQuestionnaire = $this->db->select('COUNT(Questionnaire.id) as sum_choice')
		->from('Questionnaire')
		->join('QuestionnaireSet','QuestionnaireSet.id = Questionnaire.QuestionnaireSet_id')
		->join('QuestionnaireAnswer','QuestionnaireAnswer.QuestionnaireSet_id = QuestionnaireSet.id')
		->where('QuestionnaireAnswer.id',$arData['answer_id'])->get();
		//echo $this->db->last_query();exit;
		$sum_choice = $queryCountQuestionnaire->row()->sum_choice;
		//print_r($queryCountQuestionnaire->row()->sum_choice);

		return ($row->sumrate/($sum_choice*$sum_choice))*100;
		//print_r($row);exit;
	}

	private function getCouponAvailable(){
		$query = $this->db->select('*')
		->from('Coupon')
		->where('Active',1)
		->where('EndDate >=',date('Y-m-d'))
		->get();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	private function getAllQuestionnaireSet(){
		$arSelect = array();
		$arSelect['0'] = __('Select Questionnaire Set','backend/questionnaire/questionnaireanswerlist');
		$query = $this->db->select('id,name,description,active')
		->from('QuestionnaireSet')
		->where('active',1)
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$arSelect[$value->id] = $value->name;
			}
		}
		return $arSelect;
	}

	public function exportQuestionnaire(){
		if($this->input->post(NULL,FALSE)){

				$query = $this->db->select('*,QuestionnaireAnswer.id as answer_id,QuestionnaireAnswer.created as answer_created')
		    	->from('QuestionnaireAnswer')
		    	->join('QuestionnaireSet','QuestionnaireAnswer.QuestionnaireSet_id = QuestionnaireSet.id')
		    	->where('QuestionnaireAnswer.QuestionnaireSet_id',$this->input->post('export_questionnaireset'))
		    	->order_by('QuestionnaireAnswer.SUNAME','desc')
		    	->get();


		    	$questionnaireanswer = ($query->num_rows() > 0)?$query->result():$questionnaireanswer;

		    	if(count($questionnaireanswer) > 0){
		    			foreach ($questionnaireanswer as $key => $value) {
		    				# code...
		    				$questionnaireanswer[$key]->{'Assessor'} = $this->getAgentAnswerData($value->AgentCode);

		    				$questionnaireanswer[$key]->{'Assessment'} = $this->getAgentAnswerData($value->SUNAME);

		    				$questionnaireanswer[$key]->{'answerlist'} = $this->getQuestionnaireAnswerListByAnswerId($value->answer_id);

		    				$questionnaireanswer[$key]->{'summary_percent'} = $this->getQuestionnaireSummaryPerCent(array('answer_id'=>$value->answer_id));
		    			}

		    	}

		    	// print_r($questionnaireanswer);exit;
		    	$filename = "ผลการประเมิน-".$questionnaireanswer[0]->Name.'.xlsx';

		    	require_once 'assets/backend/plugin/PHPExcel/Classes/PHPExcel.php';


		        // Create new PHPExcel object
		        $objPHPExcel = new PHPExcel();

		        //exit;

		        // Set document properties
		        $objPHPExcel->getProperties()->setCreator("PSIFIXIT")
		                                     ->setLastModifiedBy("PSIFIXIT Admin")
		                                     ->setTitle($filename)
		                                     ->setSubject("Excel file for questionnaire")
		                                     ->setDescription("Excel file questionnaire")
		                                     ->setKeywords("Questionnaire Report")
		                                     ->setCategory("Report");

		        //print_r($arDataOrder);


		        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1')->setCellValue('A1',$filename);
		        $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		        $objPHPExcel->setActiveSheetIndex(0)
		                    ->setCellValue('A2','#')
		                    ->setCellValue('B2','รหัสผู้ประเมิน')
		                    ->setCellValue('C2','ชื่อสกุล')
		                    ->setCellValue('D2','รหัสผู้ถูกประเมิน')
		                    ->setCellValue('E2','ชื่อร้านหรือชื่อตัวแทน')
		                    ->setCellValue('F2','มีความรวดเร็ว และถูกต้องในการจัดส่งสินค้า')
		                    ->setCellValue('G2','การให้ความรู้ /ข้อมูล ผลิตภัณฑ์')
		                    ->setCellValue('H2','การบริการหลังการขาย')
		                    ->setCellValue('I2','ความรวดเร็ว แม่นยำในการให้คำปรึกษา และแก้ไขปัญหา')
		                    ->setCellValue('J2','การแจ้งข้อมูลข่าวสาร ด้านการตลาดและส่งเสริมการขาย')
		                    ->setCellValue('K2','ผลรวม(เปอร์เซน)')
		                    ->setCellValue('L2','ข้อเสนอแนะ');

		        $count = 1;
		        $table_row = 3;
		        foreach ($questionnaireanswer as $key => $value) {
		            # code...
		            // print_r($value);exit;
		            $assessment_name = "";
		            if($value->Assessment->AgentType == 'AD'){
		            	$assessment_name = $value->Assessment->TradeName.' '.$value->Assessment->AgentName;
		            }else{
		            	$assessment_name = $value->Assessment->AgentName.' '.$value->Assessment->AgentSurName;
		            }
		            //print_r($value);exit;
		                        $objPHPExcel->setActiveSheetIndex(0)
		                        ->setCellValue('A'.$table_row.'',@$count)
		                        ->setCellValue('B'.$table_row.'',@$value->Assessor->AgentCode)
		                        ->setCellValue('C'.$table_row.'',@$value->Assessor->AgentName.' '.$value->Assessor->AgentSurName)
		                        ->setCellValue('D'.$table_row.'',@$value->Assessment->AgentCode)
		                        ->setCellValue('E'.$table_row.'',@$assessment_name)
		                        ->setCellValue('F'.$table_row.'',@$value->answerlist[0]['rate_name'])
		                        ->setCellValue('G'.$table_row.'',@$value->answerlist[1]['rate_name'])
		                        ->setCellValue('H'.$table_row.'',@$value->answerlist[2]['rate_name'])
		                        ->setCellValue('I'.$table_row.'',@$value->answerlist[3]['rate_name'])
		                        ->setCellValue('J'.$table_row.'',@$value->answerlist[4]['rate_name'])
		                        ->setCellValue('K'.$table_row.'',@$value->summary_percent)
		                        ->setCellValue('L'.$table_row.'',@$value->Recommend);
		                        $count++;
		                        $table_row++;
		        }


				        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('A')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('B')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('C')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('D')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('E')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('F')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('G')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('H')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('I')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('J')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('K')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('L')
		        ->setAutoSize(true);


		        // Rename worksheet
		        $objPHPExcel->getActiveSheet()->setTitle('OrderReport');


		        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
		        $objPHPExcel->setActiveSheetIndex(0);


		        // Redirect output to a client’s web browser (Excel2007)
		        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		        header('Content-Disposition: attachment;filename="'.$filename.'"');
		        header('Cache-Control: max-age=0');
		        // If you're serving to IE 9, then the following may be needed
		        header('Cache-Control: max-age=1');

		        // If you're serving to IE over SSL, then the following may be needed
		        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		        header ('Pragma: public'); // HTTP/1.0

		        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		        $objWriter->save('php://output');
		        exit;


		}
	}

	private function getQuestionnaireAnswerListByAnswerId($answer_id){

		$query = $this->db->select('*,QuestionnaireSet.Name as setname,QuestionnaireRate.Name as rate_name')
    	->from('QuestionnaireAnswer')
    	->join('QuestionnaireAnswerList','QuestionnaireAnswer.id = QuestionnaireAnswerList.QuestionnaireAnswer_id')
    	->join('Questionnaire','QuestionnaireAnswerList.Questionnaire_id = Questionnaire.id')
    	->join('QuestionnaireSet','QuestionnaireAnswer.QuestionnaireSet_id = QuestionnaireSet.id')
    	->join('QuestionnaireRate','QuestionnaireAnswerList.QuestionnaireRate_id = QuestionnaireRate.id')
    	->join('QuestionnaireCategory','QuestionnaireCategory.id = Questionnaire.QuestionnaireCategory_id')
    	->where('QuestionnaireAnswer.id',$answer_id)
    	->get();

    	return $query->result_array();

    	//echo $this->db->last_query();exit;
	}




}