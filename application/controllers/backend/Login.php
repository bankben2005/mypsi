<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	private $authentication;
  private $conn;
  private $seminar_conn;
  private $db_config;
	public function __construct() {
                       parent::__construct();
                       $this->load->library(array('msg','Lang_controller'));
                       $this->load->helper(array('our','lang'));

                       /* connect mysql */

                       $this->connect_mysql();
                       /* eof connect mysql */


                       // $this->load->config('authentication');
                        $this->load->config('internaldb');
                        $this->db_config = $this->config->item('seminar_db');
                        $this->connectSeminarDB();

                       // $this->authentication = $this->config->item('authentication');
                       // $this->authentication = $this->authentication['administrator'];
    }
    public function index(){

    	if($this->session->userdata('user_id')){
    		redirect(base_url('backend/admin_dashboard'));
    	}

    	if($this->input->post(NULL,FALSE)){

        $sql = "SELECT * FROM members WHERE email = '".$this->input->post('email')."' and password = '".sha1($this->input->post('password'))."'";
        $result = $this->conn->query($sql);

        if($result->num_rows > 0){
          $row = $result->fetch_assoc();
          
          if($row['active']){
            /* get user permission menu */
            $strQuery = "select * from member_permissions join member_menus on member_permissions.member_menus_id = member_menus.id where member_permissions.members_id = '".$row['id']."'";
            //echo $strQuery;exit;
            $resultPermission = $this->conn->query($strQuery);

            //print_r($resultPermission);exit;
            $controller_ability = array();
            $member_permissions = array();
            if($resultPermission->num_rows > 0){
                while ($row_permission = $resultPermission->fetch_assoc()) {
                  # code...
                  array_push($controller_ability, $row_permission['controller_name']);
                  array_push($member_permissions, $row_permission);
                }
                $controller_ability = array_unique($controller_ability);

                ($row['member_access_types_id'] == '1')?array_push($controller_ability, 'SuperAdmin'):"";

                ($row['member_access_types_id'] == '2')?array_push($controller_ability, "Administrator"):"";
                /* eof get user permission menu */
                $member_data = array(
                  'data' => $row,
                  'member_permissions' => $member_permissions,
                  'member_controller_available' => $controller_ability
                );

                $this->session->set_userdata('user_id',$row['id']);
                $this->session->set_userdata('member_data',$member_data);

                redirect(base_url('backend/admin_dashboard'));
            
            }else{
                $this->msg->add(__('Please set permission for this user,before access to backend','backend/login'),'error');
                redirect($this->uri->uri_string());
            }
            
          }else{
            $this->msg->add(__('Account has been blocked,Please contact administrator.','default'),'error');
            redirect($this->uri->uri_string());
          }
        }else{

            /* check again with db 192.168.100.160 */
            $query = "select * from SystemUser where UserName = '".$this->input->post('email')."' and Password = '".$this->input->post('password')."'";

            //echo $query;exit;

            $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            //echo sqlsrv_num_rows($stmt);exit;
            if(sqlsrv_num_rows($stmt) > 0){
                $result = sqlsrv_fetch_object($stmt);

                //print_r($result);exit;

                $members_data = $this->insertNewUser($result);
                if($members_data){
                    
                      /* get user permission menu */
                $strQuery = "select * from member_permissions join member_menus on member_permissions.member_menus_id = member_menus.id where member_permissions.members_id = '".$members_data['id']."'";
                //echo $strQuery;exit;
                $resultPermission = $this->conn->query($strQuery);

                //print_r($resultPermission);exit;
                $controller_ability = array();
                $member_permissions = array();
                if($resultPermission->num_rows > 0){
                    while ($row_permission = $resultPermission->fetch_assoc()) {
                      # code...
                      array_push($controller_ability, $row_permission['controller_name']);
                      array_push($member_permissions, $row_permission);
                    }
                
                }

                $controller_ability = array_unique($controller_ability);

                ($members_data['member_access_types_id'] == '1')?array_push($controller_ability, 'SuperAdmin'):"";

                ($members_data['member_access_types_id'] == '2')?array_push($controller_ability, "Administrator"):"";
                /* eof get user permission menu */
                $member_data = array(
                  'data' => $members_data,
                  'member_permissions' => $member_permissions,
                  'member_controller_available' => $controller_ability
                );

                $this->session->set_userdata('user_id',$members_data['id']);
                $this->session->set_userdata('member_data',$member_data);

                redirect(base_url('backend/admin_dashboard'));


                }

            }




            $this->msg->add('Authentication fail,Please try again.','error');
            redirect($this->uri->uri_string());
        }
    	}


    	$this->load->view('backend/login');
    }
    public function logout(){
    	$this->session->unset_userdata('user_id');
      $this->session->unset_userdata('member_data');
      redirect(base_url('backend/admin_dashboard'));
    }
    private function connect_mysql(){

        if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "psifixit";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psifixit";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psisat@2020";
            $dbname = "psifixit";

        }

        // Create connection
        $this->conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        $this->conn->set_charset("utf8");
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }
    private function close_mysql(){
        $this->conn->close();
    }

    private function connectSeminarDB(){

      if(ENVIRONMENT == 'development'){


        $serverName = $this->db_config['development']['servername']; //serverName\instanceName
        $connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
        $this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

        
        if(!$this->seminar_conn){
             echo "Connection could not be established.<br />";
             die( print_r( sqlsrv_errors(), true));
        }
      }else if(ENVIRONMENT == 'testing'){
        $serverName = $this->db_config['testing']['servername']; //serverName\instanceName
        $connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

        $this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

        
        if(!$this->seminar_conn){
             echo "Connection could not be established.<br />";
             die( print_r( sqlsrv_errors(), true));
        }

      }else if(ENVIRONMENT == 'production'){

        $serverName = $this->db_config['production']['servername']; //serverName\instanceName
        $connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
        $this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

        
        if(!$this->seminar_conn){
             echo "Connection could not be established.<br />";
             die( print_r( sqlsrv_errors(), true));
        }
      }
    }

    private function insertNewUser($dataObj){

          /* Create member */
          $strQuery = "insert into members(name,
            lastname,
            email,
            password,
            cover,
            member_access_types_id,
            created,
            updated,
            active)values(
            '',
            '',
            '".$dataObj->UserName."',
            '".sha1($dataObj->Password)."',
            '',
            '4',
            '".date('Y-m-d H:i:s')."',
            '".date('Y-m-d H:i:s')."',
            '1'
            )";
            // echo $strQuery;exit;


          if($this->conn->query($strQuery)){
              $last_id = $this->conn->insert_id;
              $data_event_ability = '{"view":true,"edit":false,"delete":false}';
              /* Insert member_permissions */
              $queryPermission = "insert into member_permissions(
                members_id,
                member_menus_id,
                event_ability
              )values(
                '".$last_id."',
                '8',
                '".$data_event_ability."'
              )";

              if($this->conn->query($queryPermission)){
                $memberDataQuery = "select * from members where id = '".$last_id."'";
                $result = $this->conn->query($memberDataQuery);
                return $result->fetch_assoc();
              }else{
                return false;
              }

          }else{
            return false;
          }


          /* eof create member */


    }

   
}