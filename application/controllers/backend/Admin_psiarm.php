<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_psiarm extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $onesignalConfig;
	private $conn;
	public function __construct() {
            parent::__construct();

            $this->load->library(array('upload'));

    }

    public function activitylist(){

    		$data = array(
	    		'activitylist' => $this->getAllActivity()
	    		
	    	);

	    	$this->template->content->view('backend/psiarm/activitylist',$data);
	        $this->template->publish();

    }

    private function getAllActivity(){
    	$query = $this->db->select('*,ArmActivity.id as activity_id,ArmActivity.Name as activity_name,ArmActivity.Created as activity_created,ArmActivity.Updated as activity_updated')
    	->from('ArmActivity')
    	->join('ArmActivityType','ArmActivity.ArmActivityType_id = ArmActivityType.id')
    	->get();

    	if($query->num_rows() > 0){
    		//print_r($query->result());exit;
    		return $query->result();
    	}else{
    		return false;
    	}

    }
    /* Action Activity */
    public function createActivity(){
    		$this->__createActivity();
	}
	public function editActivity($id){

					  $armactivity = new M_armactivity($id);
                      
                      if($armactivity->getId()){
                          $this->__createActivity($id);
                      }else{
                          redirect(base_url('backend/'.$this->controller.'/activitylist'));
                      }
			
	}
	public function deleteActivity($id=0){
					  $armactivity = new M_armactivity($id);
                      
                      if($armactivity->getId()){
                         /* delete file upload and root folder */
                         if(file_exists('./uploaded/activity/'.$armactivity->getId().'/'.$armactivity->Cover.'')){

                         	 unlink('./uploaded/activity/'.$armactivity->getId().'/'.$armactivity->Cover.'');

                         	 rmdir('./uploaded/activity/'.$armactivity->getId().'');
                         }
                         $armactivity->delete();

                         $this->msg->add(__('Delete activity complete','backend/psiarm/activitylist'),'success');
                         redirect(base_url('backend/'.$this->controller.'/activitylist'));

                         /* eof delete file upload */
                      }else{
                          redirect(base_url('backend/'.$this->controller.'/activitylist'));
                      }


	}


	public function notification(){

		$arrData = array();

		$this->connect_mysql();
        /* eof connect mysql */

        $strQuery = "select *,psiarm_notification.created as noti_created from psiarm_notification join members on psiarm_notification.members_id = members.id";
            //echo $strQuery;exit;
        $resultNotification = $this->conn->query($strQuery);

        if (!$resultNotification) {
		   printf("Errormessage: %s\n", $this->conn->error);
		}

        if($resultNotification->num_rows > 0){
        	while ($row = $resultNotification->fetch_assoc()) {
        		# code...
        		array_push($arrData, $row);
        	}
        }




		$data = array(
			'notification'=>$arrData
		);

		//print_r($data);


		$this->template->content->view('backend/psiarm/notification_list',$data);
		$this->template->publish();
	}


	public function flashsale(){

		$this->template->javascript->add(base_url('assets/backend/individual/js/jquerycountdown/jquery.countdown.min.js'));
		
		$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');

		$this->template->javascript->add(base_url('assets/backend/individual/js/psiarm/flashsale_list.js'));

		$flashsaleset = new M_flashsaleset();

		$data = array(	
			'flashsaleset'=>$flashsaleset
		);

		$this->template->content->view('backend/psiarm/flashsale_list',$data);
		$this->template->publish();

	}

	public function view_flashsale($id){

		$this->template->stylesheet->add(base_url('assets/backend/individual/css/imghover/img_common.css'));
		$this->template->stylesheet->add(base_url('assets/backend/individual/css/imghover/img_hover.css'));
		$this->template->stylesheet->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.css');

		$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
		$this->template->javascript->add(base_url('assets/backend/individual/js/jquerycountdown/jquery.countdown.min.js'));

		$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/moment.min.js');

		$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js');
		$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js');
		$this->template->javascript->add(base_url('assets/backend/individual/js/psiarm/view_flashsale.js'));

		$this->load->helper(array(
			'backend'
		));

		$flashsaleset = new M_flashsaleset($id);



		if(!$flashsaleset->id){
			redirect(base_url('backend/'.$this->controller.'/flashsale'));
		}



		if($this->input->post(NULL,FALSE)){

			$flashsalesetproduct = new M_flashsalesetproduct($this->input->post('hide_setproduct_id'));

			$flashsalesetproduct->name = $this->input->post('name');
			$flashsalesetproduct->description = $this->input->post('description');
			$flashsalesetproduct->total_available = $this->input->post('total_available');

			$flashsalesetproduct->save();
			if(count($_FILES) > 0){
				// print_r($_FILES);exit;
				if($_FILES['landscape_image']['error'] == '0'){
					// upload landscape_image
					$this->uploadLandScapeImage($flashsalesetproduct);
				}

				if($_FILES['portait_image']['error'] == '0'){
					// upload portait image 
					$this->uploadPortaitImage($flashsalesetproduct);
				} 
			}

			$this->msg->add(__('Edit flashsale set product complete','backend/psiarm/view_flashsale'),'success');
			redirect($this->uri->uri_string());
			//print_r($this->input->post());exit;
		}

		// if(ENVIRONMENT == 'production'){
		// 	echo '<center><h1>Coming Soon!!!</h1></center>';exit;
		// }

		

		/* calcualte remaining second */
		$dateend = strtotime($flashsaleset->end_datetime);
		$remaining_seconds = $dateend-time();




		$data = array(
			'flashsaleset'=>$flashsaleset,
			'remaining_seconds'=>$remaining_seconds,
			'select_branch'=>$this->getSelectAllBranch()
		);

		$this->template->content->view('backend/psiarm/view_flashsale',$data);
		$this->template->publish();


	}

	public function delete_flashsale($id){

	}

	public function createFlashSale(){
		// $this->template->stylesheet->add(base_url('assets/backend/individual/css/bootstrap-daterangepicker/daterangepicker.css'));
		if($this->checkFlashSaleAvailableNow()){
			redirect('backend/'.$this->controller.'/flashsale');
		}

		$this->loadValidator();

		$this->template->stylesheet->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.css');

		$this->template->stylesheet->add(base_url('assets/backend/individual/css/bootstrap-stepprogress/bootstrap-stepprogress.css'));



		$this->template->javascript->add(base_url('assets/backend/individual/js/jqueryvalidate/jquery.validate.min.js'));

		// $this->template->javascript->add(base_url('assets/backend/individual/js/bootstrap-daterangepicker/moment.min.js'));
		
		// $this->template->javascript->add(base_url('assets/backend/individual/js/bootstrap-daterangepicker/daterangepicker.js'));
		$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/moment.min.js');

		$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js');

		$this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js');

		
		$this->template->javascript->add(base_url('assets/backend/individual/js/psiarm/create_flashsale_stepone.js'));

		// $this->template->content->view(base_url('assets/backend/individual/js/psiarm'))


		
		$flashsaleset = new M_flashsaleset();

		if($this->input->post(NULL,FALSE)){
			// print_r($this->input->post());exit;
			$this->session->set_userdata('stepone',$this->input->post());
			redirect('backend/'.$this->controller.'/createFlashSale_steptwo');

		}

		$data = array(
			'flashsaleset'=>$flashsaleset
		);

		$this->template->content->view('backend/psiarm/create_flashsale',$data);
		$this->template->publish();

	}

	public function createFlashSale_steptwo(){

		$this->template->stylesheet->add(base_url('assets/backend/individual/css/bootstrap-stepprogress/bootstrap-stepprogress.css'));

		$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');

		$this->template->javascript->add(base_url('assets/backend/individual/js/psiarm/create_flashsale_steptwo.js'));
		//print_r($this->session->userdata());exit;
		if(!$this->session->userdata('stepone')){
			redirect('backend/'.$this->controller.'/createFlashSale');
		}

		if($this->session->userdata('steptwo')){
			$this->session->unset_userdata('steptwo');
		}




		$data = array(

		);

		$this->template->content->view('backend/psiarm/create_flashsale_steptwo',$data);
		$this->template->publish();
	}

	public function createFlashSale_stepthree(){
		$this->template->stylesheet->add(base_url('assets/backend/individual/css/bootstrap-stepprogress/bootstrap-stepprogress.css'));

		$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');

		$this->template->javascript->add(base_url('assets/backend/individual/js/psiarm/create_flashsale_stepthree.js'));


		if(!$this->session->userdata('steptwo')){
			redirect('backend/'.$this->controller.'/createFlashSale_steptwo');
		}


		$data = array(
			'stepone_data'=>$this->session->userdata('stepone'),
			'steptwo_data'=>$this->getFullStepTwoData()
		);


		$this->template->content->view('backend/psiarm/create_flashsale_stepthree',$data);
		$this->template->publish();


	}

	public function editFlashSale($id){

		/* check flash sale first*/

		/* eof check flash sale */
	}

	public function deleteFlashSale($id){

	}



	public function edit_setproduct($setproduct_id){
		/* check set product id*/
		$flashsalesetproduct = new M_flashsalesetproduct($setproduct_id);

		if(!$flashsalesetproduct->id){
			redirect(base_url('backend/'.$this->controller.'/flashsale'));
		}
		/* eof check set product id*/

		$data = array(
			'flashsalesetproduct'=>$flashsalesetproduct
		);

		$this->template->content->view('backend/psiarm/edit_setproduct',$data);
		$this->template->publish();


	}

	public function ajaxGetMPCODEData(){
		$post_data = $this->input->post();
		$product = new M_masterproduct();
		$product->where('MPCODE',$post_data['mpcode'])->get();

		//echo $product->check_last_query();exit;

		if($product->MPCODE){
			echo json_encode(array(
				'status'=>true,
				'post_data'=>$post_data,
				'data'=>$product->to_array()
			));
		}else{
			echo json_encode(array(
				'status'=>false
			));
		}

		
	}

	public function ajaxGetMPNAMEData(){
		$arrData = array();
		$post_data = $this->input->post();

		$product = new M_masterproduct();
		$product->like('MPNAME',$post_data['mpname'],'both')->get();

		if($product->result_count() > 0){
			foreach ($product as $key => $value) {
				# code...
				array_push($arrData, $value->to_array());
			}

			echo json_encode(array(
				'status'=>true,
				'post_data'=>$post_data,
				'data'=>$arrData
			));

		}else{
			echo json_encode(array(
				'status'=>false
			));
		}


	}

	public function ajaxSetFlashSaleStatus(){
		$post_data = $this->input->post();

		$flashsaleset = new M_flashsaleset($post_data['flashsaleset_id']);
		$flashsaleset->active = $post_data['changed_status'];
		$flashsaleset->save();



		echo json_encode(array(
			'status'=>true,
			'post_data'=>$post_data
		));
	}

	public function ajaxUpdateDuration(){
		$post_data = $this->input->post();

		$start_datetime = new DateTime($post_data['start_datetime']);
		$end_datetime = new DateTime($post_data['end_datetime']);
		$flashsaleset_id = $post_data['flashsaleset_id'];
		//print_r($start_datetime);
		//print_r($end_datetime);
		//print_r($flashsaleset_id);
		$flashsaleset = new M_flashsaleset($flashsaleset_id);

		$flashsaleset->start_datetime = $start_datetime->format('Y-m-d H:i:s');
		$flashsaleset->end_datetime = $end_datetime->format('Y-m-d H:i:s');
		$flashsaleset->save();

		echo json_encode(array(
			'status'=>true,
			'post_data'=>$post_data
		));
	}

	public function ajaxConfirmStepTwo(){
		$post_data = $this->input->post();

		$this->session->set_userdata('steptwo',$post_data['steptwo_data']);
		echo json_encode(array(
			'status'=>true,
			'post_data'=>$post_data
		));
	}

	public function ajaxConfirmStepThree(){
		$stepone_data = $this->session->userdata('stepone');
		$steptwo_data = $this->getFullStepTwoData();

		if(!$stepone_data || !$steptwo_data){
			echo json_encode(array(
				'status'=>false
			));
		}else{ /* if set session already */

			/* first insert into flashsale set */
			$ex_duration = explode(' - ', $stepone_data['startend_datetime']);
			$start_datetime = new DateTime($ex_duration[0]);
			$end_datetime = new DateTime($ex_duration[1]);

			$flashsaleset = new M_flashsaleset();
			$flashsaleset->name = $stepone_data['name'];
			$flashsaleset->description = $stepone_data['description'];
			$flashsaleset->active = $stepone_data['active'];
			$flashsaleset->start_datetime = $start_datetime->format('Y-m-d H:i:s');
			$flashsaleset->end_datetime =$end_datetime->format('Y-m-d H:i:s');

			if($flashsaleset->save()){
				/* insert into flashsalesetproduct */
				foreach ($steptwo_data as $key => $value) {
					# code...
					$flashsalesetproduct = new M_flashsalesetproduct();
					$flashsalesetproduct->FlashSaleSet_id = $flashsaleset->id;
					$flashsalesetproduct->MPCODE = $value['MPCODE'];
					$flashsalesetproduct->name = $value['MPNAME'];
					$flashsalesetproduct->description = $value['MPDESCRIPTION'];
					$flashsalesetproduct->total_available = $value['quantity'];
					$flashsalesetproduct->save();
				}
				/* eof insert into flashsalesetproduct */
			}

			$this->session->unset_userdata('stepone');
			$this->session->unset_userdata('steptwo');

			echo json_encode(array(
				'status'=>true
			));
			//print_r($duration);

		}
	}

	public function ajaxCheckCurrentFlashSaleExist(){
		$post_data = $this->input->post();

		if($this->checkFlashSaleAvailableNow()){
			echo json_encode(array(
				'status'=>false
			));
		}else{
			echo json_encode(array(
				'status'=>true
			));
		}

		
	}

	public function createNotification(){

		$this->template->javascript->add(base_url('assets/backend/individual/js/psiarm/create_notification.js'));

		$this->load->config('api');

		$this->onesignalConfig = $this->config->item('onesignal_psiarm');

		//print_r($this->onesignalConfig);

		if($this->input->post(NULL,FALSE)){
			//print_r($this->input->post());exit;
							$txt_notification = $this->input->post('text_message');

                            $content = array(
                                'en' => $txt_notification
                            );

                            $fields = array(
                                'app_id' => $this->onesignalConfig['app_id'],
                                // 'include_player_ids' => array('13a5fe60-fb8a-45ad-a927-58abd2b7c825'),
                                'data' => array("jobstatus" => "PsiArmNotification"),
                                'contents' => $content,
                                'ios_badgeType'=>'Increase',
                        		'ios_badgeCount'=>1,
                            );

                            if($this->input->post('send_type') == 'device_token'){
                            	$fields['include_player_ids'] = array($this->input->post('device_token'));
                            }else{
                            	//"included_segments" : ["Inactive Users","Active Users"],
                            	$fields['included_segments'] = array('Inactive Users','Active Users');
                            }

                            // $txt_message = $this->input->post('text_message');

                            // if($this->checkEmoji($txt_message)){
                            // 	echo 'has emoji';
                            // }else{
                            // 	echo 'no has emoji';
                            // }
                            // exit;
                            // echo $this->input->post('text_message');exit;


                            
                            $post_data = json_encode($fields);
                            
                            $response = json_decode($this->onesignalCurlNotification($post_data));

                            if($response->recipients > 0){

                            	$this->connect_mysql();

                            		//echo $response->recipients;exit;
                            	$query_insert = "insert into psiarm_notification(
                            		members_id,
                            		text_message,
                            		total_recipients,
                            		created
                            	)values(
                            		'".$this->session->userdata('member_data')['data']['id']."',
                            		'".$this->input->post('text_message')."',
                            		'".$response->recipients."',
                            		'".date('Y-m-d H:i:s')."'
                            	)";

                            	$query_result = $this->conn->query($query_insert);
                            	if(!$query_result){
                            		printf("Errormessage: %s\n", $this->conn->error);exit;
                            	}

                            	redirect($this->uri->uri_string());



                            }

		}

		$data = array(

		);

		$this->template->content->view('backend/psiarm/create_notification',$data);
		$this->template->publish();
	}


	public function testConnectMySql(){
		echo ENVIRONMENT;
		if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "psifixit";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psifixit";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psifixit";

        }

        // Create connection
        $this->conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        $this->conn->set_charset("utf8");
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }else{
        	echo 'connected';exit;
        } 

	}

	public function exportOrderToExcel(){
		if($this->input->post(NULL,FALSE)){
			if($this->input->post('branch_code')){
				$branch_code = $this->input->post('branch_code');

				$branch_data = $this->getBranchDataByBranchCode($branch_code);
				$flashsale_data = $this->getFlashSaleSetDataById($this->input->post('flashsale_set_id'));

				$flashsale_duration = date('d-m-Y_H:i',strtotime($flashsale_data->start_datetime)).' - '.date('d-m-Y_H:i',strtotime($flashsale_data->end_datetime));

				$filename = "FlashSale[".$branch_data->BranchName."][".$flashsale_duration."].xlsx";

				$queryFlashSaleTransaction = $this->db->select('FlashSaleTransaction.id as transaction_id,Agent.AgentCode,Agent.AgentName,Agent.AgentSurName,')
				->from('FlashSaleTransaction')
				->join('Agent','FlashSaleTransaction.AgentCode = Agent.AgentCode')
				->join('Branch','Agent.BranchCode = Branch.BranchCode')
				->where('Branch.BranchCode',$branch_data->BranchCode)
				->where('FlashSaleTransaction.FlashSaleSet_id',$flashsale_data->id)
				->get(); 

				//echo $this->db->last_query();exit;
				//print_r($queryFlashSaleTransaction);exit;

				if($queryFlashSaleTransaction->num_rows() <= 0){
					$this->msg->add(__('Not found order for this branch','backend/psiarm/view_flashsale'),'error');
					redirect('backend/'.$this->controller.'/view_flashsale/'.$flashsale_data->id);
				}else{

					$arrProductData = array();
					/* if found record */
					$result = $queryFlashSaleTransaction->result();


					foreach ($result as $key => $value) {
						# code...
						//$setproduct_data = $this->getFlashSaleSetProductDataById($value->)
						$transaction_product_query = $this->db->select('*,FlashSaleTransactionProduct.created as product_order_created')
						->from('FlashSaleTransactionProduct')
						->join('FlashSaleSetProduct','FlashSaleTransactionProduct.FlashSaleSetProduct_id = FlashSaleSetProduct.id')
						->where('FlashSaleTransactionProduct.FlashSaleTransaction_id',$value->transaction_id)
						->get();

						if($transaction_product_query->num_rows() > 0){
							foreach ($transaction_product_query->result() as $k => $v) {
								# code...
								$data_push = array(
									'FlashSale_name'=>$flashsale_data->name,
									'AgentCode'=>$value->AgentCode,
									'FirstnameLastname'=>$value->AgentName.' '.$value->AgentSurName,
									'ProductCode'=>$v->MPCODE,
									'ProductName'=>$v->name,
									'Amount'=>$v->amount,
									'Created'=>$v->product_order_created,
									'Branch'=>$branch_data->BranchName
								);
								array_push($arrProductData, $data_push);
							}
						}
						
						// print_r($value->transaction_id);
					}

					//print_r($arrData);

					//exit;

					//print_r($result);exit;

				}

				require_once 'assets/backend/plugin/PHPExcel/Classes/PHPExcel.php';


		        // Create new PHPExcel object
		        $objPHPExcel = new PHPExcel();

		        //exit;

		        // Set document properties
		        $objPHPExcel->getProperties()->setCreator("PSI FLASHSALE")
		                                     ->setLastModifiedBy("PSIARM Admin")
		                                     ->setTitle($filename)
		                                     ->setSubject("Excel file for order product by flashsale")
		                                     ->setDescription("Excel file for order product by flashsale")
		                                     ->setKeywords("FlashSale Order")
		                                     ->setCategory("Report");

		        //print_r($arDataOrder);


		        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1')->setCellValue('A1',$filename);
		        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		        $objPHPExcel->setActiveSheetIndex(0)
		                    ->setCellValue('A2','#')
		                    ->setCellValue('B2','FlashSale')
		                    ->setCellValue('C2','รหัสช่าง')
		                    ->setCellValue('D2','ชื่อ-สกุล')
		                    ->setCellValue('E2','รหัสสินค้า')
		                    ->setCellValue('F2','ชื่อสินค้า')
		                    ->setCellValue('G2','จำนวนสั่งจอง')
		                    ->setCellValue('H2','เวลาที่สั่งจอง')
		                    ->setCellValue('I2','สาขาที่ดูแล');


		        $count = 1;
		        $table_row = 3;
		        foreach ($arrProductData as $key => $value) {
		            # code...
		                        $objPHPExcel->setActiveSheetIndex(0)
		                        ->setCellValue('A'.$table_row.'',@$count)
		                        ->setCellValue('B'.$table_row.'',@$value['FlashSale_name'])
		                        ->setCellValue('C'.$table_row.'',@$value['AgentCode'])
		                        ->setCellValue('D'.$table_row.'',@$value['FirstnameLastname'])
		                        ->setCellValue('E'.$table_row.'',@$value['ProductCode'])
		                        ->setCellValue('F'.$table_row.'',@$value['ProductName'])
		                        ->setCellValue('G'.$table_row.'',@number_format($value['Amount'],2))
		                        ->setCellValue('H'.$table_row.'',@$value['Created'])
		                        ->setCellValue('I'.$table_row.'',@$value['Branch']);
		                        $count++;
		                        $table_row++;
		        }

		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('A')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('B')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('C')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('D')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('E')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('F')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('G')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('H')
		        ->setAutoSize(true);
		        $objPHPExcel->getActiveSheet()
		        ->getColumnDimension('I')
		        ->setAutoSize(true);

		        $objPHPExcel->getActiveSheet()->setTitle('FlashSaleOrderReport');


		        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
		        $objPHPExcel->setActiveSheetIndex(0);


		        // Redirect output to a client’s web browser (Excel2007)
		        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		        header('Content-Disposition: attachment;filename="'.$filename.'"');
		        header('Cache-Control: max-age=0');
		        // If you're serving to IE 9, then the following may be needed
		        header('Cache-Control: max-age=1');

		        // If you're serving to IE over SSL, then the following may be needed
		        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		        header ('Pragma: public'); // HTTP/1.0

		        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		        $objWriter->save('php://output');
		        exit;









				//echo $file_name;exit;
				//print_r($branch_data);
				//print_r($this->input->post('branch_code'));
			}
		}
	}


	private function __createActivity($id=NULL){
			$this->template->stylesheet->add(base_url('assets/backend/individual/css/bootstrap-daterangepicker/daterangepicker.css'));
			$this->template->stylesheet->add(base_url('assets/backend/individual/css/imghover/img_common.css'));
			$this->template->stylesheet->add(base_url('assets/backend/individual/css/imghover/img_hover.css'));


			$this->template->javascript->add(base_url('assets/backend/individual/js/bootstrap-daterangepicker/moment.min.js'));
			$this->template->javascript->add(base_url('assets/backend/individual/js/bootstrap-daterangepicker/daterangepicker.js'));

			$this->template->javascript->add(base_url('assets/backend/individual/js/jqueryvalidate/jquery.validate.min.js'));
			$this->template->javascript->add(base_url('assets/backend/individual/js/psiarm/createactivity.js'));

			$activity = new M_armactivity($id);


			if($this->input->post(NULL,FALSE)){

				$activity->Name = $this->input->post('Name');
				$activity->UriString = $this->input->post('UriString');
				$activity->Active = $this->input->post('Active');
				$activity->ArmActivityType_id = $this->input->post('ArmActivityType_id');


				/* Duration Data */
				$startDate = "";
				$endDate = "";
				$exDuration = explode(' - ', $this->input->post('Duration'));
				$startDate = new DateTime($exDuration[0]);
				$endDate = new DateTime($exDuration[1]);
				/* Eof Duration Data */



				$activity->StartDate = $startDate->format('Y-m-d H:i:s');
				$activity->EndDate =$endDate->format('Y-m-d H:i:s');

				if($id){
					$activity->Updated = date('Y-m-d H:i:s');
				}else{
					$activity->Created = date('Y-m-d H:i:s');
				}

				if($activity->save()){

						/* save coupon image */
						if($_FILES['activity_image']['error'] == 0){
							$this->uploadActivityImage($activity);
						}

						$txt_title = ($id)?__('Edit','backend/default'):__('Create','backend/default');
                        $this->msg->add($txt_title.__('Complete','backend/default'),'success');
                        redirect($this->uri->uri_string());



				}


			}

			$str_date = "";
			$end_date = "";

			if($activity->id){
    			$str_date = new DateTime($activity->StartDate);
    			$end_date = new DateTime($activity->EndDate);

    		}

			$data = array(
				'activity' => $activity,
				'activity_type' => $this->getAllActivityType(),
				'duration' => ($str_date && $end_date)?$str_date->format('d-m-Y').' - '.$end_date->format('d-m-Y'):""
			);

			//print_r($data);


			$this->template->content->view('backend/psiarm/createactivity',$data);
			$this->template->publish();


	}

	private function getAllActivityType(){
		$arReturn = array();

		$query = $this->db->select('*')
		->from('ArmActivityType')
		->where('Active',1)
		->get();

		if($query->num_rows() > 0){

			foreach ($query->result() as $key => $value) {
				# code...
				$arReturn[$value->id] = $value->Name;
			}

		}
		return $arReturn;

	}

	private function uploadActivityImage($activity){
							$config = array();
							if(!is_dir('uploaded/activity/'.$activity->id.'')){
                                mkdir('uploaded/activity/'.$activity->id.'',0777,true);
                            }
                            clearDirectory('uploaded/activity/'.$activity->id.'/');
                                                     
                            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                            $config['upload_path'] = 'uploaded/activity/'.$activity->id.'/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '2000';

                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload('activity_image')){
                                $error = array('error' => $this->upload->display_errors());
                                $this->msg->add(__($error['error'],'default'), 'error');
                                if(file_exists('uploaded/activity/'.$activity->id)){
                                        rmdir('uploaded/activity/'.$activity->id);
                                }
                                if(!$activity->id){$activity->delete();}
                                if($activity->id){
                                    redirect(base_url('backend/').$this->controller.'/editActivity/'.$id);   
                                }else{
                                    redirect(base_url('bakcend/').$this->controller.'/createActivity');
                                }
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());
                                $activity_update = new M_armactivity($activity->id);
                                $activity_update->Cover = $data_upload['upload_data']['file_name'];
                                if($activity_update->save()){
                                	return true;
                                }
                            }
                            return false;
	}

	private function uploadLandScapeImage($flashsalesetproduct){
							$config = array();
							if(!is_dir('uploaded/flashsalesetproduct/landscape/'.$flashsalesetproduct->id.'')){
                                mkdir('uploaded/flashsalesetproduct/landscape/'.$flashsalesetproduct->id.'',0777,true);
                            }
                            clearDirectory('uploaded/flashsalesetproduct/landscape/'.$flashsalesetproduct->id.'/');
                                                     
                            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                            $config['upload_path'] = 'uploaded/flashsalesetproduct/landscape/'.$flashsalesetproduct->id.'/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '2000';

                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload('landscape_image')){
                                $error = array('error' => $this->upload->display_errors());
                                $this->msg->add(__($error['error'],'default'), 'error');
                               
                                redirect(base_url('backend/'.$this->controller.'/view_flashsale/'.$flashsalesetproduct->flashsaleset->get()->id));
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());

                                $setproduct_update = new M_flashsalesetproduct($flashsalesetproduct->id);
                                $setproduct_update->image_landscape = $data_upload['upload_data']['file_name'];
                                $setproduct_update->save();

                                return true;
                            }
                            return false;
	}

	private function uploadPortaitImage($flashsalesetproduct){
							$config = array();
							if(!is_dir('uploaded/flashsalesetproduct/portait/'.$flashsalesetproduct->id.'')){
                                mkdir('uploaded/flashsalesetproduct/portait/'.$flashsalesetproduct->id.'',0777,true);
                            }
                            clearDirectory('uploaded/flashsalesetproduct/portait/'.$flashsalesetproduct->id.'/');
                                                     
                            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                            $config['upload_path'] = 'uploaded/flashsalesetproduct/portait/'.$flashsalesetproduct->id.'/';
                            $config['allowed_types'] = 'gif|jpg|png';
                            $config['max_size'] = '2000';

                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload('portait_image')){
                                $error = array('error' => $this->upload->display_errors());
                                $this->msg->add(__($error['error'],'default'), 'error');
                               
                                redirect(base_url('backend/'.$this->controller.'/view_flashsale/'.$flashsalesetproduct->flashsaleset->get()->id));
                            }else{
                                $data_upload = array('upload_data' => $this->upload->data());

                                $setproduct_update = new M_flashsalesetproduct($flashsalesetproduct->id);
                                $setproduct_update->image_portait = $data_upload['upload_data']['file_name'];
                                $setproduct_update->save();

                                return true;
                            }
                            return false;
	}

	private function getFullStepTwoData(){
		$arrReturn = array();
		$steptwo = $this->session->userdata('steptwo');
		// print_r($steptwo);exit;
		foreach ($steptwo as $key => $value) {
			# code...
			$product = new M_masterproduct();
			$product->where('MPCODE',$value['mpcode'])->get();

			array_push($arrReturn, $product->to_array());
			$arrReturn[$key]['quantity'] = $value['quantity'];

		}
		return $arrReturn;

		// print_r($arrReturn);exit;
	}

	private function checkFlashSaleAvailableNow(){
		$flashsaleset = new M_flashsaleset();
		$flashsaleset->where('end_datetime >= ',date('Y-m-d H:i:s'))->where('active',1)->get();

		//print_r($flashsaleset->to_array());
		if($flashsaleset->result_count() > 0){
			return true;
		}else{
			return false;
		}
	}

	private function connect_mysql(){

        if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "psifixit";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psifixit";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "psifixit";

        }

        // Create connection
        $this->conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        $this->conn->set_charset("utf8mb4");
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }

    private function onesignalCurlNotification($post_data){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->onesignalConfig['url']);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->onesignalConfig['header']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                //echo $response;exit;

               
                return $response;

    }

    private function checkEmoji($string){
	    preg_match( '/[\x{1F600}-\x{1F64F}]/u', $string, $matches_emo );

    	return !empty( $matches_emo[0] ) ? true : false;
	}

	private function getSelectAllBranch(){
		$arrReturn = array();
		$query = $this->db->select('*')
		->from('Branch')
		->where('Flags_Select','T')
		->order_by('BranchName','asc')
		->get();

		foreach ($query->result() as $key => $value) {
			# code...
			$arrReturn[$value->BranchCode] = $value->BranchName;
		}
		//print_r($arrReturn);exit;
		return $arrReturn;
	}

	private function getBranchDataByBranchCode($branch_code = ""){
		$query = $this->db->select('*')
		->from('Branch')
		->where('BranchCode',$branch_code)
		->get();

		return $query->row();
	}

	private function getFlashSaleSetDataById($flashsale_set_id){
		$query = $this->db->select('*')
		->from('FlashSaleSet')
		->where('id',$flashsale_set_id)
		->get();

		return $query->row();
	}



}