<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class PsiMyPSIModule extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
    //backup route api
    public function SyncWPuser_post(){
        
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SyncWPuser');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__SyncWPuser($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

            
        }

    }

    public function SyncFixITData_post(){
        
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SyncFixITData');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__SyncFixITData($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

            
        }

    }
    private function __SyncFixITData($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->SyncFixITData($requestCriteria);
        return $output;
    }


    private function __SyncWPuser($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->SyncWPuser($requestCriteria);
        return $output;
    }




}
