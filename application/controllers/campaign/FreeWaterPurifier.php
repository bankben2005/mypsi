<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include_once APPPATH . 'libraries/Campaign/FreeWaterPurifierLibrary.php';
class FreeWaterPurifier extends FreeWaterPurifierLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    // private $onesignalConfig = array();


	public function __construct() {
                        parent::__construct();
                        $this->load->helper([
                            'campaign'
                        ]);

    }

    public function index(){ 


        $this->loadSweetAlert();
    	$this->loadValidator();


    	$this->template->javascript->add(base_url('assets/campaign/freepurifier/js/register.js?'.time())); 


        


    	$data = [
    		'select_province'=>$this->getSelectProvince()
    	];

    	$this->template->content->view('campaign/freepurifier/register',$data);
    	$this->template->publish();


    	// echo 'aaa';
    }

    public function RegisterList(){

        $realm = 'Restricted area';
        $users = array('admin' => 'mypass', 'guest' => 'guest');


        if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Digest realm="'.$realm.
                   '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

            die('Text to send if user hits Cancel button');
        }


        // analyze the PHP_AUTH_DIGEST variable
        if (!($data = $this->http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
            !isset($users[$data['username']])){
            $this->digestLogout();
            die('Wrong Credentials!');
        }

        // generate the valid response
        $A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
        $A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
        $valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

        if ($data['response'] != $valid_response){
            $this->digestLogout();
            die('Wrong Credentials!');
        }

        //print_r($data['username']);



        $this->loadSweetAlert();
        $this->template->javascript->add(base_url('assets/campaign/freepurifier/js/register_list.js')); 


        
        $purifier_register = new M_campaignpurifierregister();
        $purifier_register->order_by('created','desc')->get();


        $data = [
            'purifier_register'=>$purifier_register,
            'digest_username'=>$data['username']

        ];


        $this->template->content->view('campaign/freepurifier/register_list',$data);
        $this->template->publish();


    }

    public function deletePurifierRegister($register_id){



        $purifier_register = new M_campaignpurifierregister();
        $purifier_register->where('id',$register_id)
        ->get();

        if($purifier_register->id){
            // write log file for delete 
            /* Create log temporary delete */
            $log_file_path = $this->createLogFilePath('DeletePurifierRegister');
            $file_content = date("Y-m-d H:i:s") . ' value : ' . json_encode($purifier_register->to_array()) . "\n";
            file_put_contents($log_file_path, $file_content, FILE_APPEND);
            unset($file_content);

            $this->db->delete('CampaignPurifierRegister',[
                'id'=>$register_id
            ]);
            redirect(base_url('campaign/'.$this->controller.'/RegisterList'));

            // if($purifier_register->delete()){
                
            // }


        }

    }

    public function exportToExcel(){

        ini_set('memory_limit','512M'); // This also needs to be increased in some cases. Can be changed to a higher value as per need)
        ini_set('sqlsrv.ClientBufferMaxKBSize','524288'); // Setting to 512M
        ini_set('pdo_sqlsrv.client_buffer_max_kb_size','524288'); // Setting to 512M - for pdo_sqlsrv
        //$q = $this->input->get('q');

        $purifier_register = new M_campaignpurifierregister();
        $purifier_register->where('active',1)
        ->order_by('created','desc')
        ->get();

        if($purifier_register->result_count() <=0){

            echo ("<script LANGUAGE='JavaScript'>
    window.alert('Not found data for export');
    window.location.href='".base_url('campaign/'.$this->controller.'/RegisterList')."';exit;
    </script>");exit;


            //echo '<script>alert("Not found data to export");window.location.href="'..'";</script>';
        }



        $filename = 'PurifierRegister'.date('d-m-Y H:i').'.xlsx';

        require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel.php';



        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Purifier register report")
                    ->setLastModifiedBy("PR ADMIN")
                    ->setTitle($filename)
                    ->setSubject("Excel file for purifier register record")
                    ->setDescription("Excel file for purifier register record")
                    ->setKeywords("Purifier register Report")
                    ->setCategory("Purifier Report");



        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1')->setCellValue('A1',$filename);
        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A2','#')
                            ->setCellValue('B2','ผู้ยื่นเรื่อง')
                            ->setCellValue('C2','เบอร์โทรศัพท์ (ผู้ยื่นเรื่อง)')
                            ->setCellValue('D2','อีเมล์ (ผู้ยื่นเรื่อง)')
                            ->setCellValue('E2','ที่อยู่ (ผู้ยื่นเรื่อง)')
                            ->setCellValue('F2','สถานที่ติดตั้ง')
                            ->setCellValue('G2','เบอร์โทรศัพท์ (สถานที่ติดตั้ง)')
                            ->setCellValue('H2','อีเมล์ (สถานที่ติดตั้ง)')
                            ->setCellValue('I2','ที่อยู่ (สถานที่ติดตั้ง)')
                            ->setCellValue('J2','ผู้ประสานงาน (สถานที่ติดตั้ง)')
                            ->setCellValue('K2','แหล่งที่มาข้อมูลการสมัคร')
                            ->setCellValue('L2','Remark')
                            ->setCellValue('M2','วัน/เดือน/ปี ที่สมัคร');


        $count = 1;
        $table_row = 3;

        foreach ($purifier_register as $key => $value) {
            # code...
            $place_address  = $value->place_address;
            $place_address .= ' ต.'.$value->districts->get()->district_name;
            $place_address .= ' อ.'.$value->amphurs->get()->amphur_name;
            $place_address .= ' จ.'.$value->provinces->get()->province_name;
            $place_address .= ' '.$value->zipcode;


            $campaignFromTextArray = $this->getCampaignFromTextArray($value->get_campaign_from,$value->get_campaign_from_other_remark);




            $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$table_row.'',@$count)
                                ->setCellValue('B'.$table_row.'',@$value->firstname.' '.$value->lastname)
                                ->setCellValue('C'.$table_row.'',@$value->telephone)
                                ->setCellValue('D'.$table_row.'',@$value->email)
                                ->setCellValue('E'.$table_row.'',@$value->address)
                                ->setCellValue('F'.$table_row.'',@$value->place_name)
                                ->setCellValue('G'.$table_row.'',@$value->place_telephone)
                                ->setCellValue('H'.$table_row.'',@$value->place_email)
                                ->setCellValue('I'.$table_row.'',@$place_address)
                                ->setCellValue('J'.$table_row.'',@$value->coordinator.' Tel:'.@$value->coordinator_telephone)
                                ->setCellValue('K'.$table_row.'',@implode(',', $campaignFromTextArray))
                                ->setCellValue('L'.$table_row.'',@$value->remark)
                                ->setCellValue('M'.$table_row.'',@date('d/m/Y H:i',strtotime($value->created)));

            $count++;
                    $table_row++;

        }

        $objPHPExcel->getActiveSheet()
                ->getColumnDimension('A')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('B')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('C')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('D')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('E')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('F')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('G')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('H')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('I')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('J')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('K')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('L')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('M')
                ->setAutoSize(true);

            // Rename worksheet
                $objPHPExcel->getActiveSheet()->setTitle('PurifierReport');


                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);


                // Redirect output to a client’s web browser (Excel2007)
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$filename.'"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');

                // If you're serving to IE over SSL, then the following may be needed
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save('php://output');

                exit;





    }

    public function ajaxSaveRegisterPurifier(){
        $post_data = $this->input->post();

        

            $purifier_register = new M_campaignpurifierregister();

            $purifier_register->firstname = $post_data['firstname'];
            $purifier_register->lastname = $post_data['lastname'];
            $purifier_register->telephone = $post_data['telephone'];
            $purifier_register->email = $post_data['email'];
            $purifier_register->address = $post_data['address'];


            // for place information 
            $purifier_register->place_name = $post_data['place_name'];
            $purifier_register->place_telephone = $post_data['place_telephone'];
            // $purifier_register->place_email = $post_data['place_email'];
            $purifier_register->place_address = $post_data['place_address'];

            $purifier_register->Provinces_id = $post_data['select_province'];
            $purifier_register->Amphurs_id = $post_data['select_amphur'];
            $purifier_register->Districts_id = $post_data['select_district'];
            $purifier_register->zipcode = $post_data['zipcode'];
            $purifier_register->coordinator = @$post_data['place_coordinator'];
            $purifier_register->coordinator_telephone = @$post_data['place_coordinator_telephone'];
            $purifier_register->advisor_customer = @$post_data['advisor_customer'];
            $purifier_register->get_campaign_from = (isset($post_data['get_campaign_from']) && count($post_data['get_campaign_from']) > 0)?json_encode($post_data['get_campaign_from']):'[]';
            $purifier_register->get_campaign_from_other_remark = $post_data['other_description'];
            // eof place information

            $purifier_register->save();

            echo json_encode([
                    'status'=>true,
                    'post_data'=>$post_data
                ]); exit;

            

    }


    public function getAmphurByProvinceId(){


    	$arReturn = array();
        $arReturn["0"] = __('Select Amphur','campaign/purifier_template');
        $post_data = $this->input->post();

        $amphur = new M_amphurs();
        $amphur->where('provinces_id',$post_data['province_id'])
        ->order_by('amphur_name','asc')
        ->get();

        foreach ($amphur as $key => $value) {
            # code...
            $arReturn[$value->id] = $value->amphur_name;
        }
        


        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data,
            'data'=>$arReturn
        ));


    }

    public function getDistrictByAmphurId(){
    	$arReturn = array();
        $arReturn["0"] = __('Select District','campaign/purifier_template');
        $post_data = $this->input->post();

        $district = new M_districts();
        $district->where('provinces_id',$post_data['province_id'])
        ->where('amphurs_id',$post_data['amphur_id'])
        ->order_by('district_name','asc')
        ->get();

        foreach ($district as $key => $value) {
            $arReturn[$value->id] = $value->district_name;
        }
        
        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data,
            'data'=>$arReturn
        ));
    }

    public function getZipcodeByDistrictId(){
    	$arReturn = array();

        $post_data = $this->input->post();
        $zipcode = new M_zipcode();
        $zipcode->where('provinces_id',$post_data['province_id'])
        ->where('amphurs_id',$post_data['amphur_id'])
        ->where('districts_id',$post_data['district_id'])
        ->get();

        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data,
            'zipcode'=>$zipcode->zipcode
        ));
    }

    public function ajaxSaveRemark(){
        $post_data = $this->input->post();

        $purifier_register = new M_campaignpurifierregister($post_data['register_id']);

        $purifier_register->remark = $post_data['remark'];
        $purifier_register->save();



        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]); 
    }


    private function getSelectProvince(){
    	$arrProvince = [];
    	$arrProvince[''] = __('Select Province','campaign/purifier_template');

    	$provinces = new M_provinces();
    	$provinces->order_by('province_name','asc')
    	->get();

    	foreach ($provinces as $key => $value) {
    		# code...
    		$arrProvince[$value->id] = $value->province_name;
    	}

    	return $arrProvince;
    }
    private function http_digest_parse($txt){
        // protect against missing data
        $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
        $data = array();
        $keys = implode('|', array_keys($needed_parts));

        preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

        foreach ($matches as $m) {
            $data[$m[1]] = $m[3] ? $m[3] : $m[4];
            unset($needed_parts[$m[1]]);
        }

        return $needed_parts ? false : $data;
    }

    public function digestLogout(){
        header('HTTP/1.1 401 Unauthorized');
        return true;

    }

    private function createLogFilePath($filename = '') {
        $log_path = './logs/purifier_register';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }
        

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function getCampaignFromTextArray($encode_data,$other_description = ''){
        $arReturn = [];
        $decode_data = json_decode($encode_data);

        if(count($decode_data) > 0){
            foreach ($decode_data as $key => $value) {
                # code...
                $txtValue = $value;
                if($txtValue == 'other'){
                    array_push($arReturn, textGetCampaignFrom($value).' : '.$other_description);
                }else{
                    array_push($arReturn, textGetCampaignFrom($value));
                }
                

            }
        }

        return $arReturn;

    }
}