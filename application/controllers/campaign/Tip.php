<?php
defined('BASEPATH') OR exit('No direct script access allowed');


include_once APPPATH . 'libraries/Campaign/CampaignLibrary.php';
class Tip extends CampaignLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    private $onesignalConfig = array();
	public function __construct() {
                        parent::__construct();
                        $this->load->config('api');
                        $this->onesignalConfig = $this->config->item('onesignal');

    }
    public function index(){
    	
        // $customer_token = 'ca1cfe6b-60d3-4ff5-b1ba-163d6172eaf6';
        // $this->sendNotificationToCustomer($customer_token);

        // exit;
    	$this->template->stylesheet->add(base_url('assets/css/campaign/tip/insurance_register.css'));
        $this->loadDateRangePickerStyle();

    	$this->loadValidator();
        $this->loadDateRangePickerScript();
    	
        $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
        $this->template->javascript->add(base_url('assets/js/campaign/tip/insurance_register.min.js?'.strtotime(date('Y-m-d H:i:s'))));
        
        // $this->template->javascript->add(base_url('assets/js/campaign/tip/insurance_register.js?'.strtotime(date('Y-m-d H:i:s'))));


        if($this->input->post(NULL,FALSE)){
                //print_r($this->input->post());exit;
                $dob = $this->input->post('dob');
                $dob_obj = new DateTime($dob);


                $register = new M_campaigninsuranceregister();
                $register->ConsumerID = $this->input->post('ConsumerID');
                $register->broker = 'Tip';
                $register->home_type = $this->input->post('home_type');
                $register->title = $this->input->post('title');
                $register->firstname = $this->input->post('firstname');
                $register->lastname = $this->input->post('lastname');
                $register->telephone = $this->input->post('telephone');
                $register->email = $this->input->post('email');
                //$register->id_card = $this->input->post('id_card');
                //$register->dob = $dob_obj->format('Y-m-d');
                $register->address_house_no = $this->input->post('address_house_no');
                //$register->address_moo = $this->input->post('address_moo');
                //$register->address_soi = $this->input->post('address_soi');
                //$register->address_road = $this->input->post('address_road');
                $register->Provinces_id = $this->input->post('province_id');
                $register->Amphurs_id = $this->input->post('amphur_id');
                $register->Districts_id = $this->input->post('district_id');
                $register->zipcode = $this->input->post('zipcode');
                $register->enewsletter_status = ($this->input->post('accept_enewsletter'))?1:0;

                if($register->save()){

                        /* update reference number */
                        $ref_code = 'ins-'.str_pad($register->id, 8, '0', STR_PAD_LEFT);

                        $update_data = new M_campaigninsuranceregister($register->id);
                        $update_data->ref_code = $ref_code;
                        $update_data->save();


                        /* send notification to customer if register success */
                        $fixitcustomer = new M_fixitcustomer();
                        $fixitcustomer->where('CustID',$register->ConsumerID)->get();



                        if($fixitcustomer->result_count() > 0){
                            //echo $fixitcustomer->Device_Token;exit;
                            $this->sendNotificationToCustomer($fixitcustomer->Device_Token);
                        }

                        /* eof send notification */


                        redirect(base_url('campaign/tip/registerSuccess/'.$register->id));
                }


        }



    	$data = array(
    		'seo_title'=>'ลงทะเบียนสินค้ากับ PSI รับฟรีประกันอัคคีภัยสำหรับที่อยู่อาศัย',
    		'select_province'=>$this->getSelectProvince()
    	);

    	//$this->load->view('campaign/tip/insurance_register',$data);
    	$this->template->content->view('campaign/tip/insurance_register',$data);
    	$this->template->publish();
    }

    public function registerSuccess($id){

            $register_data = new M_campaigninsuranceregister();
            $register_data->where('id',$id)->get();

            $data = array(
                'seo_title'=>'ลงทะเบียนรับความคุ้มครองสำเร็จ',
                'register_data'=>$register_data
            );
            $this->template->content->view('campaign/tip/register_success',$data);
            $this->template->publish();
    }

    public function already_register(){
            $data = array(
                'seo_title'=>'คุณได้ทำการลงทะเบียน'
            );

            $this->template->content->view('campaign/tip/already_register',$data);
            $this->template->publish();
    }

    public function ajaxGetAmphurByProvince(){
        $arAmphur = array();
        $arAmphur["0"] = 'เลือกอำเภอ';
        $post_data = $this->input->post();


        $amphur = new M_amphurs();
        $amphur->where('provinces_id',$post_data['province_id'])->get();

        foreach ($amphur as $key => $value) {
            # code...
            $arAmphur[$value->id] = $value->amphur_name;
        }
        //print_r($arAmphur);exit;
        //ksort($arAmphur);

        echo json_encode(array(
            'status'=>true,
            'data'=>$arAmphur,
            'post_data'=>$post_data
        ));
    }

    public function ajaxGetDristrictByAmphur(){
        $arDistrict = array();
        $arDistrict['0'] = 'เลือกตำบล';
        $post_data = $this->input->post();

        $district = new M_districts();
        $district->where('amphurs_id',$post_data['amphur_id'])->get();

        foreach ($district as $key => $value) {
            # code...
            $arDistrict[$value->id] = $value->district_name;
        }
        //ksort($arDistrict);

        echo json_encode(array(
                'status'=>true,
                'data'=>$arDistrict,
                'post_data'=>$post_data
        ));
    }

    public function ajaxGetZipcodeByDistrict(){
        $post_data = $this->input->post();

        $zipcode_text = "";
        $zipcode = new M_zipcode();
        $zipcode->where('provinces_id',$post_data['province_id'])->where('amphurs_id',$post_data['amphur_id'])->where('districts_id',$post_data['district_id'])->get();

        if($zipcode->getId()){
            $zipcode_text = $zipcode->zipcode;
        }

        echo json_encode(array(
                'status'=>true,
                'zipcode'=>$zipcode_text,
                'post_data'=>$post_data
        ));

    }

    public function ajaxGetConsumerDataByConsumerID(){
        $post_data = $this->input->post();
        $already_register = false;

        $fixitcustomer = new M_fixitcustomer();
        $fixitcustomer->where('CustID',$post_data['ConsumerID'])->get();

        $exFirstnameLastname = explode(' ', $fixitcustomer->CustName);

        /* check alredy register */
        $campaign = new M_campaigninsuranceregister();
        $campaign->where('ConsumerID',$post_data['ConsumerID'])->where('broker','Tip')->where('active',1)->get();

        if($campaign->result_count() > 0){
            $already_register = true;
        }
        /* eof check already register */

        echo json_encode(array(
            'status'=>true,
            'firstname'=>@$exFirstnameLastname[0],
            'lastname'=>@$exFirstnameLastname[1],
            'telephone'=>@$fixitcustomer->Telephone,
            'email'=>@$fixitcustomer->Email,
            'already_register'=>$already_register

        ));

    }

    public function ajaxGetConsumerDataForCheck(){
        $post_data = $this->input->post();

        $fixitcustomer = new M_fixitcustomer();
        $fixitcustomer->where('CustID',$post_data['ConsumerID'])->get();

        $customer_data = $fixitcustomer->to_array();


        echo json_encode(array(
            'status'=>true,
            'customer_data'=>$customer_data,
            'post_data'=>$post_data
        ));
    }   

    public function ajaxGetConsumerDataByUsername(){
        $post_data = $this->input->post();

        $already_register = false;

        $fixitcustomer = new M_fixitcustomer();
        $fixitcustomer->where('UserName',$post_data['customer_username'])->get();

        //print_r($fixitcustomer->to_array());
        if(!$fixitcustomer->UserName){
            echo json_encode(array(
                'status'=>false,
                'flag'=>'not_found_customer',
                'message'=>'Not found customer'
            ));exit;
        }


        /* check customer register product after 16/05/18 */
        $checkregister = new M_productwarranty();
        $checkregister->where('ConsumerID',$fixitcustomer->CustID)
        ->where('Start_WarrantyDate >=','2018-05-16')
        ->get();

        //echo $checkregister->check_last_query();
        if($checkregister->result_count() <= 0){
            echo json_encode(array(
                'status'=>false,
                'flag'=>'not_found_registerproduct',
                'message' => 'Not found register product'
            ));exit;
        }

        /* */


        $exFirstnameLastname = explode(' ', $fixitcustomer->CustName);

        /* check alredy register */
        $campaign = new M_campaigninsuranceregister();
        $campaign->where('ConsumerID',$fixitcustomer->CustID)->where('broker','Tip')->where('active',1)->get();

        if($campaign->result_count() > 0){
            $already_register = true;
        }
        /* eof check already register */

        echo json_encode(array(
            'status'=>true,
            'firstname'=>@$exFirstnameLastname[0],
            'lastname'=>@$exFirstnameLastname[1],
            'telephone'=>@$fixitcustomer->Telephone,
            'email'=>@$fixitcustomer->Email,
            'already_register'=>$already_register,
            'ConsumerID'=>$fixitcustomer->CustID,
            'post_data'=>$post_data

        ));

    }




    private function getSelectProvince(){
    	$arReturn = array();
        $arReturn['0'] = 'เลือกจังหวัด';
    	$provinces = new M_provinces();
    	foreach ($provinces->get() as $key => $value) {
    		# code...
    		$arReturn[$value->id] = $value->province_name;
    	}
        //ksort($arReturn);
    	return $arReturn;
    }

    private function generateRefCode($length=5){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }

    private function onesignalRegisterInsuranceSuccess($post_data){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->onesignalConfig['url']);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->onesignalConfig['header']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

               
                return $response;
    }

    private function sendNotificationToCustomer($customer_token){
                            
                            $txt_notification = "ท่านได้รับสิทธิ์ตามที่ลงทะเบียนแคมเปญ PSI-TIP เรียบร้อยแล้ว \r\n";
                            $txt_notification .= "ใบสรุปความคุ้มครองจะจัดส่งให้ตามอีเมลที่ลงทะเบียนไว้อีกครั้ง";

                            $content = array(
                                'en' => $txt_notification
                            );

                            $fields = array(
                                'app_id' => $this->onesignalConfig['app_id'],
                                'include_player_ids' => array($customer_token),
                                'data' => array("jobstatus" => "REGISTER"),
                                'contents' => $content
                            );

                            
                            $post_data = json_encode($fields);
                            
                            $response = json_decode($this->onesignalRegisterInsuranceSuccess($post_data));

                            if($response->recipients > 0){

                            }
       

    }



}