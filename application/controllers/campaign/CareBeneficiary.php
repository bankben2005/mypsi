<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


include_once APPPATH . 'libraries/Campaign/CampaignLibrary.php';
class CareBeneficiary extends CampaignLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    // private $onesignalConfig = array();


	public function __construct() {
                        parent::__construct();

    }

    public function index(){

    	if(!$this->input->get('agentcode')){
    		echo '<h1>Not found agent code</h1>';exit;
    	}

    	$beneficiaryData = $this->getBeneficiaryData([
    		'AgentCode'=>@$this->input->get('agentcode')
    	]);


    	if($this->input->post(NULL,FALSE)){

    		//print_r($this->input->post());exit;

    		$this->db->insert('AgentInsuranceBeneficiary',[
    			'AgentCode'=>$this->input->get('agentcode'),
    			'beneficiary_firstname'=>$this->input->post('firstname'),
    			'beneficiary_lastname'=>$this->input->post('lastname'),
    			'beneficiary_relationship'=>$this->input->post('relationship'),
    			'created'=>date('Y-m-d H:i:s')
    		]);

    		//echo $this->db->last_query();exit;

    		redirect($this->uri->uri_string().'?agentcode='.$this->input->get('agentcode'));
    	}

    	$data = [
    		'beneficiaryData'=>$beneficiaryData
    	];

    	$this->load->view('campaign/carebeneficiary/carebeneficiary',$data);

    }

    public function check(){

        $has_agent = false;

        if(isset($_GET['agent_code'])){
            $agent_code = base64_decode($this->input->get('agent_code'));

            $agent = $this->db->select('AgentCode,AgentName,AgentSurName,AgentType,BranchCode')
            ->from('Agent')
            ->where('AgentCode',$agent_code)
            ->get();

            if($agent->num_rows() > 0){
                $has_agent = true;
                $rowAgent = $agent->row();

                $branch_data = $this->getBranchDataByBranchCode([
                    'branch_code'=>$rowAgent->BranchCode
                ]);

                $rowAgent->{'BranchData'} = $branch_data;


                //print_r($rowAgent);
            }




        }

        $data = [
            'has_agent'=>$has_agent,
            'agent_data'=>@$rowAgent
        ];


        $this->load->view('campaign/carebeneficiary/check',$data);

    }

    public function printBeneficiary(){
        $this->load->helper([
            'our'
        ]);
        if($this->input->post(NULL,FALSE)){

            $agent = $this->db->select('AgentCode,AgentName,AgentSurName,AgentType,BranchCode')
            ->from('Agent')
            ->where('AgentCode',$this->input->post('agent_code'))
            ->get();

            $rowAgent = $agent->row();
            $rowAgent->{'BranchData'} = $this->getBranchDataByBranchCode([
                'branch_code'=>$rowAgent->BranchCode
            ]);

            $post_data = $this->input->post();

            $sixMonthData = $this->getCareBeneficiarySixMonth([
                'check_date'=>$post_data['check_date'],
                'agent_code'=>$post_data['agent_code']
            ]);

            $oneYearData = $this->getCareBeneficiaryOneYear([
                'check_date'=>$post_data['check_date'],
                'agent_code'=>$post_data['agent_code']
            ]);

            $be_protected_type = ($sixMonthData['be_protected'])?'six_month':'one_year';

            $data = [
                'agent_data'=>$rowAgent,
                'sixMonthData'=>$sixMonthData,
                'oneYearData'=>$oneYearData,
                'be_protected_type'=>$be_protected_type,
                'treatmentData'=>$this->getTreatmentHistoryData([
                    'agent_code'=>$post_data['agent_code']
                ])
            ];

            // print_r($data);exit;


            $view_pdf = $this->load->view('campaign/carebeneficiary/printBeneficiaryPDF',$data,true);


            $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            // print_r($fontDirs);
            // print_r($fontData);exit;

            $mpdf = new \Mpdf\Mpdf(
                [
                'mode' => 'utf-8',
                'format' => 'A4',
                'margin_left' => 10,
                'margin_right' => 10,
                'margin_top' => 18,
                'margin_bottom' => 10,
                'margin_header' => 10,
                'margin_footer' => 10,
                'fontDir' => array_merge($fontDirs, [
                    __DIR__ . '/fonts',
                ]),
                'fontdata' => $fontData + [
                    'thsarabun' => [
                        'R' => 'THSarabunNew.ttf',
                        //'I' => 'THSarabunNew Italic.ttf',
                        //'B' => 'THSarabunNew Bold.ttf',
                    ]
                ],
                'default_font' => 'thsarabun'
                ]

            );
            $html = $view_pdf;



            $filename = 'PSICARE - '.$this->input->post('agent_code').'_'.$this->input->post('check_date');

            // $headerHtml = '<div style="width:50%;float:left;text-align:left;">';
            // $headerHtml .= ' ';
            // $headerHtml .= '</div>';

            $headerHtml = '<div style="width:100%;text-align:right;">';
            $headerHtml .= '{PAGENO} / {nb}';
            $headerHtml .= '</div>';

            $mpdf->setHeader($headerHtml);

            $mpdf->WriteHTML($html);
            $mpdf->Output($filename.'.pdf','I'); // opens in browser
        }
    }

    public function ajaxCalculateBeneficiary(){
        $post_data = $this->input->post();


        $sixMonthData = $this->getCareBeneficiarySixMonth([
            'check_date'=>$post_data['check_date'],
            'agent_code'=>$post_data['agent_code']
        ]);

        $oneYearData = $this->getCareBeneficiaryOneYear([
            'check_date'=>$post_data['check_date'],
            'agent_code'=>$post_data['agent_code']
        ]);

        
        $view = $this->load->view('campaign/carebeneficiary/ajaxCheckAgentBeneficiary',[
            'sixMonthData'=>$sixMonthData,
            'oneYearData'=>$oneYearData,
            'treatmentData'=>$this->getTreatmentHistoryDataByAgentCode([
                'agent_code'=>$post_data['agent_code']
            ])
        ],true);



        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data,
            'view'=>$view
        ]);
    }

    public function ajaxSaveTreatmentForm(){
        $post_data = $this->input->post();

        $treatment_date = new DateTime($post_data['treatment_date']);

        $this->db->insert('AgentInsuranceTreatmentHistory',[
            'AgentCode'=>$post_data['agent_code'],
            'TreatmentCost'=>(float)$post_data['treatment_cost'],
            'CreatedBy'=>$post_data['fill_data_by'],
            'TreatmentDate'=>$treatment_date->format('Y-m-d'),
            'created'=>date('Y-m-d H:i:s')
        ]);

        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]); 
    }

    private function getBeneficiaryData($data = []){

    	$query = $this->db->select('*')
    	->from('AgentInsuranceBeneficiary')
    	->where('AgentCode',$data['AgentCode'])
    	->get();

    	if($query->num_rows() > 0){
    		return $query->row();
    	}else{
    		return false;
    	}



    }

    private function getBranchDataByBranchCode($data = []){
        $query = $this->db->select('*')
        ->from('Branch')
        ->where('BranchCode',$data['branch_code'])
        ->get();

        return $query->row();
    }

    private function getCareBeneficiarySixMonth($data = []){

        $start_checkdate = new DateTime($data['check_date']);
        $start_checkdate->sub(new DateInterval('P6M'));

        $check_date = new DateTime($data['check_date']);

        $query = $this->db->select('SUM(POSTRANSACTION.POSTNETPRICE) as sum_netprice')
        ->from('POSTRANSACTION')
        ->where('AgentCode',$data['agent_code'])
        ->where('CONVERT(char(10), POSTRANSACTION.POSTDATE,126) >= ',$start_checkdate->format('Y-m-d'))
        ->where('CONVERT(char(10), POSTRANSACTION.POSTDATE,126) <= ',$check_date->format('Y-m-d'))
        ->get();

        $sum_netprice = ($query->row()->sum_netprice)?$query->row()->sum_netprice:0;

        $be_protected = ($sum_netprice >= 180000)?true:false;


        return [
            'start_date'=>$start_checkdate->format('d-m-Y'),
            'end_date'=>$check_date->format('d-m-Y'),
            'sum_netprice'=>$sum_netprice,
            'be_protected'=>$be_protected
        ];



    }

    private function getCareBeneficiaryOneYear($data = []){
        $start_checkdate = new DateTime($data['check_date']);
        $start_checkdate->sub(new DateInterval('P12M'));

        $check_date = new DateTime($data['check_date']);

        
        $query = $this->db->select('SUM(POSTRANSACTION.POSTNETPRICE) as sum_netprice')
        ->from('POSTRANSACTION')
        ->where('AgentCode',$data['agent_code'])
        ->where('CONVERT(char(10), POSTRANSACTION.POSTDATE,126) >= ',$start_checkdate->format('Y-m-d'))
        ->where('CONVERT(char(10), POSTRANSACTION.POSTDATE,126) <= ',$check_date->format('Y-m-d'))
        ->get();

        $sum_netprice = ($query->row()->sum_netprice)?$query->row()->sum_netprice:0;

        $be_protected = ($sum_netprice >= 360000)?true:false;

        return [
            'start_date'=>$start_checkdate->format('d-m-Y'),
            'end_date'=>$check_date->format('d-m-Y'),
            'sum_netprice'=>$sum_netprice,
            'be_protected'=>$be_protected
        ];



    }

    private function getTreatmentHistoryData($data = []){

        $arrReturn = [
            'sum_treatment_cost'=>0,
            'count_treatment'=>0
        ];


        $minus_12_month = new DateTime();
        $minus_12_month->sub(new DateInterval('P12M'));

        //print_r($minus_12_month);exit;

        $query = $this->db->select('*')
        ->from('AgentInsuranceTreatmentHistory')
        ->where('AgentCode',$data['agent_code'])
        ->where('TreatmentDate >=',$minus_12_month->format('Y-m-d'))
        ->get();

        if($query->num_rows() > 0){
                $sum_treatment_cost = 0;
                foreach ($query->result() as $key => $value) {
                    # code...
                    $sum_treatment_cost += $value->TreatmentCost;

                }

                $arrReturn['sum_treatment_cost'] = $sum_treatment_cost;
                $arrReturn['count_treatment'] = $query->num_rows();
        }

        return $arrReturn;

    }

    private function getTreatmentHistoryDataByAgentCode($data = []){

        $minus_12_month = new DateTime();
        $minus_12_month->sub(new DateInterval('P12M'));


        $query = $this->db->select('*')
        ->from('AgentInsuranceTreatmentHistory')
        ->where('AgentCode',$data['agent_code'])
        ->where('TreatmentDate >=',$minus_12_month->format('Y-m-d'))
        ->get();
        return $query;
    }

}