<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class PsiFixitModule extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function CheckProductSN_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('CheckProductSN');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__checkProductSN($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

            
        }

    }

    public function CreateJobManual_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('CreateJobManual');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__createJobManual($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function GetCustomerByAgent_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);


                    $requestAuthenAPI->set_requireMethod('GetCustomerByAgent');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{
                        $output = $this->__getCustomerByAgent($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            
        }
    }

    public function GetProductByCustomer_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);


                    $requestAuthenAPI->set_requireMethod('GetProductByCustomer');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{
                        $output = $this->__getProductByCustomer($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            
        }
    }

    public function GetFixitJobType_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);


                    $requestAuthenAPI->set_requireMethod('GetFixitJobType');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{
                        $output = $this->__getFixitJobType($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            
        }
    }
    public function LinkJobCustomer_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);


                    $requestAuthenAPI->set_requireMethod('LinkJobCustomer');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{
                        $output = $this->__setLinkJobCustomer($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            
        }


    }

    public function RemindAircondition_post(){
            if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);


                    $requestAuthenAPI->set_requireMethod('RemindAircondition');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{
                        $output = $this->__remindAircondition($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            
        }  

    }

    public function GetAgentDataByAgentCode_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);


                    $requestAuthenAPI->set_requireMethod('GetAgentDataByAgentCode');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{
                        $output = $this->__getAgentDataByAgentCode($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            
        }  
    }

    public function GetAgentDeviceToken_get(){

        if(isset($_GET['agent_code']) && $_GET['agent_code'] != ""){
            $agent_code = $_GET['agent_code'];
            //echo $agent_code; 
            $requestCriteria = new stdClass();
            $requestCriteria->agent_code = $agent_code;
            $output = $this->__getAgentDeviceToken($requestCriteria);
            echo json_encode($output);exit;
        }else{
            $arr_return = array(
                'status'=>false,
                'message'=>'not-found-agentcode'
            );
            echo json_encode($arr_return);exit;
        }

    }

    public function GetAgentImageProfileByAgentCode_get(){
        if(isset($_GET['agent_code']) && $_GET['agent_code'] != ""){
            $agent_code = $_GET['agent_code'];
            $query = $this->db->select('AgentCode,Image')
            ->from('Agent')
            ->where('AgentCode',$agent_code)->get();

            if($query->num_rows() > 0){
                $arr_return = array(
                    'status'=>true,
                    'image'=>base64_encode($query->row()->Image)
                );
                //print_r($arr_return);
                //$this->set_response($arr_return,  REST_Controller::HTTP_OK);
                //echo $query->row()->Image;
                echo json_encode($arr_return);exit;
            }else{
                $arr_return = array(
                    'status'=>false,
                    'message'=>'not-found-agent-by-agentcode'
                );
                echo json_encode($arr_return);exit;

            }


        }else{
            $arr_return = array(
                'status'=>false,
                'message'=>'not-found-agentcode'
            );
            echo json_encode($arr_return);exit;
        }

    }

    private function __checkProductSN($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->checkProductSN($requestCriteria);
        return $output;
    }

    private function __createJobManual($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->createJobManual($requestCriteria);
        return $output;
    }
    private function __getCustomerByAgent($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->getCustomerByAgent($requestCriteria);
        return $output;
    }
    private function __getProductByCustomer($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->getProductByCustomer($requestCriteria);
        return $output;
    }
    private function __getFixitJobType($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->getFixitJobType($requestCriteria);
        return $output;
    }
    private function __setLinkJobCustomer($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->setLinkJobCustomer($requestCriteria);
        return $output;

    }

    private function __remindAircondition($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->remindAircondition($requestCriteria);
        return $output;

    }

    private function __getAgentDataByAgentCode($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->getAgentDataByAgentCode($requestCriteria);

        //print_r($output);exit;
        return $output;
    }

    private function __getAgentDeviceToken($requestCriteria){
        require_once(APPPATH. "libraries/Agent/AgentLibrary.php");
        $output = array();
        $AgentLibrary = new AgentLibrary();
        $output = $AgentLibrary->getAgentDeviceToken($requestCriteria);

        //print_r($output);exit;
        return $output;
    }




}
