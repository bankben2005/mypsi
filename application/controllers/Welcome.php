<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Frontend_controller.php';
class Welcome extends Frontend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	private $detect;
	public function __construct() {
                        parent::__construct();
                        $this->load->library(array('pagination'));


                        require_once APPPATH."/libraries/Mobile_Detect.php";

                        $this->detect = new Mobile_Detect();

    }
	public function index($page = NULL)
	{
		//$page = (!$page)?1:$page;
		if(ENVIRONMENT == 'production'){
			echo "<h1>Welcome</h1>";exit;
		}
		$this->page_num = 10;

		$view_file = '';

     
		


		
        if($this->detect->isMobile()){
        	$view_file = 'frontend/welcome/welcome_mobile_tablet';
        }else if($this->detect->isTablet()){
        	$view_file = 'frontend/welcome/welcome_mobile_tablet';
        }else{
        	$view_file = 'frontend/welcome/welcome_desktop';
        	$this->page_num = 20;
        }


		$data = array();
		$this->template->stylesheet->add(base_url('assets/css/bootstrap-select/bootstrap-select.min.css'));

		// $this->template->stylesheet->add(base_url('assets/css/psi.css'));
		$this->template->stylesheet->add(base_url('assets/css/psi.min.css?'.strtotime(date('Y-m-d H:i:s'))));
		// $this->template->stylesheet->add('https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css');
		// $this->template->javascript->add('https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js');
		$this->template->javascript->add(base_url('assets/js/typeahead/bootstrap3-typeahead.min.js'));


		
		$this->template->javascript->add(base_url('assets/js/bootstrap-select/bootstrap-select.min.js'));
		$this->template->javascript->add(base_url('assets/js/frontend/welcome.min.js?'.strtotime(date('Y-m-d H:i:s'))));
		// $this->template->javascript->add(base_url('assets/js/frontend/welcome.js'));

		

		// add meta tag
		$this->template->meta->add('keywords','ช่าง PSI,ช่างติดจาน,ช่างกล้อง OCS,ช่างแอร์ PSI,ช่างไฟฟ้า PSI,ช่างเครื่องกรองน้ำ PSI');
		$this->template->meta->add('description','ค้นหาช่าง PSI จานดาวเทียม,กล้อง OCS,แอร์,ไฟฟ้า,เครื่องกรองน้ำ');


		$this->setData('seo_title','ค้นหาช่าง PSI ตัวแทนติดตั้งมาตรฐาน');
		$this->setData('seo_keywords','keywords','ช่าง PSI,ช่างติดจาน,ช่างกล้อง OCS,ช่างแอร์ PSI,ช่างไฟฟ้า PSI,ช่างเครื่องกรองน้ำ PSI');
		$this->setData('seo_description','ค้นหาช่าง PSI จานดาวเทียม,กล้อง OCS,แอร์,ไฟฟ้า,เครื่องกรองน้ำ');
		// facebook meta 
		//$this->template->meta->add('')

		// eof facebook meta

		// eof add meta tag
		

		// $query = $this->db->select('*')->from('Agent')->get();

		// print_r($query->result());exit;
		
		
		// $Agent  = new M_agent();
		if($this->input->get(NULL,FALSE)){
			$Agent  = new M_agent();
			if($this->input->get('search_code')){
				$Agent->where('AgentCode',$this->input->get('search_code'));

			}else if($this->input->get('search_province') && $this->input->get('search_amphur')){

				$search_amphur = $this->input->get('search_amphur');
				if (strpos($this->input->get('search_amphur'), 'เมือง') !== false) {
				    $search_amphur = 'เมือง';
				}
				$Agent->where('R_Province',$this->input->get('search_province'));
				$Agent->like('R_District',$search_amphur,'both');
			// }else if($this->input->get('search_province')){
			// 	$Agent->like('R_Province',$this->input->get('search_province'),'both');
			// }else if($this->input->get('search_amphur')){
			// 	$Agent->like('R_District',$this->input->get('search_amphur'),'both');

			}else if($this->input->get('search_shopname')){
				$Agent->like('TradeName',$this->input->get('search_shopname'),'both');
			}else if($this->input->get('search_name')){
				$Agent->like('AgentName',$this->input->get('search_name'),'both');
			}else if($this->input->get('search_surname')){
				$Agent->like('AgentSurName',$this->input->get('search_surname'),'both');
			}else if($this->input->get('search_address')){
				$Agent->where('R_Addr1',$this->input->get('search_address'));
			}else if($this->input->get('search_telephone')){
				$Agent->where('Telephone',$this->input->get('search_telephone'));
			}

			if($this->input->get('FixIt01') == 'true'){
				$Agent->where('FixIt01','T');
			}
			if($this->input->get('FixIt02') == 'true'){
				$Agent->where('FixIt02','T');
			}
			if($this->input->get('FixIt03') == 'true'){
				$Agent->where('FixIt03','T');
			}
			if($this->input->get('FixIt04') == 'true'){
				// echo 'eieieii';
				$Agent->where('FixIt04','T');
			}
			if($this->input->get('FixIt05') == 'true'){
				$Agent->where('FixIt05','T');
			}

			// echo $this->input->get('FixIt04');exit;
			// $Agent->or_where('FixIt01 !=','F');
			// $Agent->or_where('FixIt02 !=','F');
			// $Agent->or_where('FixIt03 !=','F');
			// $Agent->or_where('FixIt04 !=','F');
			// $Agent->or_where('FixIt05 !=','F');
			

			$Agent->not_like('AgentName','TEST','both');

			$Agent->where("(FixIt01 = 'T' or FixIt02 = 'T' or FixIt03 = 'T' or FixIt04 = 'T' or FixIt05 = 'T')");
			/* where not in array */
			$arWhereNotIn = array('AP','GN',"");
			/* eof where not in array */
			$Agent->where_not_in('AgentType',$arWhereNotIn);
			$Agent->where('status','O');

			
			$clone = $Agent->get_clone();
			$AgentCount = $clone->count();
			$Agent->order_by('BranchCode','asc');
			$Agent->order_by('AgentCode','asc');
			// $Agent->order_by('AgentRating','desc');
			$Agent->limit($this->page_num,$page);
        	$Agent->get();

        	if(isset($_GET['test']) && $_GET['test'] == 'test'){
        	 echo $Agent->check_last_query();
        	}
        	
        	
		}else{
			$Agent  = new M_agent();
			/* where not in array */
			$arWhereNotIn = array('AD','GN',"");
			/* eof where not in array */
			$Agent->where('status','O');
			$Agent->where_not_in('AgentType',$arWhereNotIn);
			$Agent->not_like('AgentName','TEST','both');

			$Agent->where("(FixIt01 = 'T' or FixIt02 = 'T' or FixIt03 = 'T' or FixIt04 = 'T' or FixIt05 = 'T')");

			// $Agent->or_where('FixIt01 !=','F');
			// $Agent->or_where('FixIt01 !=','F');
			// $Agent->or_where('FixIt02 !=','F');
			// $Agent->or_where('FixIt03 !=','F');
			// $Agent->or_where('FixIt04 !=','F');
			// $Agent->or_where('FixIt05 !=','F');

			

			$clone = $Agent->get_clone();
			$AgentCount = $clone->count();
			// $Agent->order_by('AgentRating','desc');
			$Agent->order_by('BranchCode','asc');
			$Agent->order_by('AgentCode','asc');

			$Agent->limit($this->page_num,$page);
			$Agent->get();
			//echo $Agent->check_last_query();
			
        }

        if(isset($_GET['test']) && $_GET['test'] == 'test'){ 

        	echo $AgentCount;
        	foreach ($Agent as $key => $value) {
        		# code...
        		echo '<br>';
        		echo ++$key.' - '.$value->AgentCode.' '.$value->AgentName.' '.$value->AgentSurName.'<br>';
        	}

        	exit;
        }

        //$clone->check_last_query();
        ob_start();
        $clone->check_last_query();

        $textQuery = ob_get_clean();
        /* Create log file for debug*/
        $log_file_path = $this->createLogFilePath('QueryDBLogs');
        $file_content = date("Y-m-d H:i:s") . ' query string : ' . $textQuery . "\n";
        file_put_contents($log_file_path, $file_content, FILE_APPEND);
        unset($file_content);




        $province = new M_province();
        $arProvince = array();
        $arProvince['0'] = "เลือกจังหวัด";
        foreach ($province->get() as $key => $value) {
        	# code...
        	$arProvince[$value->ProvinceID."|".$value->ProvinceName] = $value->ProvinceName;
        }

        $this->setData('agent',$Agent);
        $this->config_page($AgentCount,$page);

        $config_page = array();
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url().'Welcome/';
        $config_page['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config_page);
        $this->setData('pages', $this->pagination->create_links());
        $this->setData('result_count',$AgentCount);
        $this->setData('Province',$arProvince);
        // print_r($arProvince);exit;
		

        $data = $this->getData();
        // print_r($this->getData());exit; 




        
        
        // exit;
        //echo $view_file;exit;

		$this->template->content->view($view_file,$data);
        $this->template->publish();
	}

	public function getAllProvince(){
		$arData = array();
		$province = new M_province();


		foreach ($province->get() as $key => $value) {
			# code...
			// $arData['ProvinceName'][$key] = $value->ProvinceName;
			// $arData['ProvinceID'][$key] = $value->ProvinceID;
			$arData[$key] = array(
				'id' => $value->ProvinceID,
				'name' => $value->ProvinceName
			);
		}

		echo json_encode($arData);
		// print_r($arData);


	}

	public function testFixitSendNotification(){
		$device_token = 'ca1cfe6b-60d3-4ff5-b1ba-163d6172eaf6';

		$this->load->config('api');

		$api_config = $this->config->item('onesignal'); 

					$txt_notification = 'test notification';

                	// echo $txt_notification.'<br>';exit;

                            $content = array(
                                'en' => $txt_notification
                            );

                            $fields = array(
                                'app_id' => $this->api_config['app_id'],
                                'include_player_ids' => array($device_token),
                                'data' => array("jobstatus" => "REGISTER"),
                                'contents' => $content
                            );

                            
                            $post_data = json_encode($fields);
                            
                            $response = json_decode($this->onesignalCurlNotification($post_data)); 

                            print_r($response);
	}

	public function getFilterWarranty(){ 



		$productpartnotification = new M_productpartnotification(); 
		$productpartnotification->where('serial_no',@$this->input->get('serial_no'))
		->get(); 

		//echo $productpartnotification->check_last_query();exit;

		if($productpartnotification->result_count()){
			$data = [];
			foreach ($productpartnotification as $key => $value) {
				# code...
				
				array_push($data, $value->to_array()); 
			}


			foreach ($data as $key => $value) {
				# code...
				$data[$key]['reset_logs'] = [];
				$productpartnotificationlogs = new M_productpartnotificationresetlogs();
				$productpartnotificationlogs->where('ProductPartNotification_id',$value['id'])
				->get(); 

				if($productpartnotificationlogs->result_count() > 0){
					foreach ($productpartnotificationlogs as $k => $v) {
						# code...
						$arr_reset_log = []; 
						$arr_reset_log = $v->to_array();
						$arr_reset_log['MPCODE'] = $this->getMPCODEBySerialNumber($v->barcode);

						if(isset($_GET['test']) && $_GET['test'] == 'test'){
							print_r($arr_reset_log);exit;
						}
						array_push($data[$key]['reset_logs'], $arr_reset_log);

					}
				}


			}

			echo json_encode([
				'status'=>true,
				'data'=>$data,
				'desc'=>'Success'
			]);
		}else{
			echo json_encode([
				'status'=>false,
				'desc'=>'No found'
			]);
		}


	}

	public function getAllAmphur(){
		$arData = array();
		$amphur = new M_amphurs();


		foreach ($amphur->get() as $key => $value) {
			# code...
			$arData[$key] = $value->amphur_name;
		}

		echo json_encode($arData);
		// print_r($arData);
	}
	public function getAmphurByProvince(){

		$data_receive = array(
			'ProvinceID' => $this->input->get('ProvinceID')
		);
		$arData = array();
		$amphur = new M_district();
		$amphur->where('ProvinceID',$data_receive['ProvinceID']);

		foreach ($amphur->get() as $key => $value) {
			# code...
			$arData[$key] = $value->District_Name_TH;
		}

		echo json_encode($arData);

	}

	public function ajaxGetAmphurByProvince(){
		$data_receive = array(
			'province_id' => $this->input->post('province_id')
		);

		$arData = array();
		$arDataAmphur = array();
		$amphur = new M_district();
		$amphur->where('ProvinceID',$data_receive['province_id']);

		foreach ($amphur->get() as $key => $value) {
			# code...
			$arDataAmphur[$value->District_Name_TH] = $value->District_Name_TH;
		}

		$arData = array(
			'status' => true,
			'data' => $arDataAmphur
		);

		echo json_encode($arData);
	}

	public function testGetStreaming(){
		$link_name = ' http://96.30.124.98:1935/redirect/psilive2a/THAICCTV.stream_360p?type=m3u8';
		$link = 'http://96.30.124.109:4040/api/Product?linkname='.$link_name.'&boxuser=psifixit';

		echo file_get_contents($link);
		echo $link;exit;
	}

	public function testSendAirconNotification(){

		$device_token = @$this->input->get('device_token');
		$this->load->config('api');

		$api_config = $this->config->item('onesignal_fixitarm');



		$txt_notification = "";
		$txt_notification .= "แจ้งเตือนถึงกำหนดล้างแอร์ให้ลูกค้าที่ไปติดตั้งเมื่อวันที่วันที่ .... [เพื่อทดสอบเท่านั้น]";

				$content = array(
                                'en' => $txt_notification
                );

                $fields = array(
                        'app_id' => $api_config['app_id'],
                        'include_player_ids' => array($device_token),
                        'data' => array("jobstatus" => "REMIND"),
                        'contents' => $content
                );

                $post_data = json_encode($fields);
                        
                        //$response = new StdClass();
                $response = json_decode($this->onesignalFixitArmCurlNotification($post_data));

                print_r($response);exit;

	}

	public function testSendAirconNotificationToFixitCustomer(){
		$device_token = @$this->input->get('device_token');
		$this->load->config('api');

		$api_config = $this->config->item('onesignal');



		$txt_notification = "";
		$txt_notification .= "แจ้งเตือนถึงกำหนดล้างแอร์ .... [เพื่อทดสอบเท่านั้น]";

				$content = array(
                                'en' => $txt_notification
                );

                $fields = array(
                        'app_id' => $api_config['app_id'],
                        'include_player_ids' => array($device_token),
                        'data' => array("jobstatus" => "REMIND"),
                        'contents' => $content
                );

                $post_data = json_encode($fields);
                        
                        //$response = new StdClass();
                $response = json_decode($this->onesignalFixitCurlNotification($post_data));

                print_r($response);exit;

	}

	public function syncUpdateAppChaangStatus(){

		$all_technician = $this->curlGetTechnicialFromChaangApplication();
		$all_technician = json_decode($all_technician);

		print_r($all_technician);
		foreach ($all_technician as $key => $value) {
			# code...

			//print_r($value);
			$this->db->update('Agent',[
				'AppChaangStatus'=>1
			],['AgentCode'=>$value->code]);
		}



	}


	public function getAllProductCode(){

		$strQuery = 'select MPCODE,MPNAME,MPDESCRIPTION,WarrantyMonths from MasterProduct where MPCODE in (
			select distinct MPCODE from MasterProductSN
		) order by MPNAME asc';

		$query = $this->db->query($strQuery);

		echo json_encode([
			'status'=>true,
			'ProductList'=>$query->result_array(),
			'result_code'=>'000',
			'result_desc'=>'Success'
		]);exit;

		// print_r($query->result());
	}

	public function getMasterProductNameAndDescription(){
		$get_data = $this->input->get();

		$query = $this->db->select('MPCODE,MPNAME,MPDESCRIPTION')
		->from('MasterProduct')
		->where('MPCODE',$get_data['MPCODE'])
		->get();

		if($query->num_rows() > 0){
			echo json_encode([
				'status'=>true,
				'ProductData'=>$query->row(),
				'result_code'=>'000',
				'result_desc'=>'Success'
			]);
		}else{
			echo json_encode([
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found'
			]);
		}




	}

	private function testDateTimeOnline(){
		$today_is = new Datetime('2018-01-03 14:25:09.000');
		print_r($today_is);
		$warranty_days = 365;
		$next_date = $today_is->add(new DateInterval('P'.(int)$warranty_days.'D'));

		
		print_r($next_date);

	}
	private function createLogFilePath($filename = '') {
        
        //$document_root = $_SERVER['DOCUMENT_ROOT'];

        $log_path = './logs/QueryDBLogs';
        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function onesignalFixitArmCurlNotification($post_data){

    	$this->load->config('api');
		$api_config = $this->config->item('onesignal_fixitarm');


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $api_config['url']);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $api_config['header']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

               
                return $response;

    }

    private function onesignalFixitCurlNotification($post_data){

    	$this->load->config('api');
		$api_config = $this->config->item('onesignal');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $api_config['url']);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $api_config['header']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);               
            return $response;

    }

    private function curlGetTechnicialFromChaangApplication($post_data = []){
	    	$this->load->config('api');
			$api_config = $this->config->item('request_technician_from_app_chaang');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $api_config['request_url']);
            //curl_setopt($ch, CURLOPT_HTTPHEADER, $api_config['header']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);               
            return $response;


    }

    private function getMPCODEBySerialNumber($serial_no){
    	$master_sn = new M_masterproductsn();
    	$master_sn->where('SerialNo',$serial_no)
    	->get();

    	if($master_sn->result_count() > 0){
    		return $master_sn->MPCODE;
    	}else{
    		return '';
    	}
    }

   

}
