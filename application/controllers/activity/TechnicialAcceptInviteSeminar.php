<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TechnicialAcceptInviteSeminar extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	//private $product_register_conn;
	private $seminar_conn;
	private $seminar_available;

	public function __construct() {
		parent::__construct(); 

		$this->load->config('internaldb');
		$this->db_config = $this->config->item('seminar_db');
		$this->connectSeminarDB();
	}

	public function index(){  


		// $seminar_available_data = $this->seminar_available;
		// $seminar_available_data->{'SeminarTickerCover'} = $this->getApiUrlTicket();



		// print_r($seminar_available_data);exit;

		$data = [
			// 'seminar_data'=>$seminar_available_data
		];


		$this->load->view('activity/technicial_accept_invite_seminar',$data);



	}

	public function ajaxGetSeminarAndAccept(){
		$post_data = $this->input->post();

		$this->seminar_available = $this->getSeminarAvailable([
			'agentcode'=>$post_data['agentcode']
		]);


		$seminar_available_data = $this->seminar_available;
		$seminar_available_data->{'SeminarTickerCover'} = ($seminar_available_data->TicketUrl)?$seminar_available_data->TicketUrl:$this->getApiUrlTicket();



		// print_r($seminar_available_data);exit;

		$technician_data = $this->getTechnicianDataByAgentCode($post_data['agentcode']);

		$checkAlreadyAcceptJoin = $this->checkTechnicialAreadyAcceptJoin([
			'agentcode'=>$post_data['agentcode'],
			'seminarno'=>$this->seminar_available->SeminarNo
		]);


		// print_r($checkAlreadyAcceptJoin);exit;



		$data = [
			'technician_data'=>$technician_data,
			'seminar_data'=>$seminar_available_data,
			'seminar_register'=>@$checkAlreadyAcceptJoin['SeminarRegister']
		];

		// print_r($data);exit;
		$view = "";

		if(!$checkAlreadyAcceptJoin['status']){
			$view = $this->load->view('activity/ajax_not_found_register_information',[],true);

		}else{
			$view = $this->load->view('activity/ajax_render_accept_invite_seminar',$data,true);
		}


		

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data,
			'view'=>$view
		]);
	}

	public function ajaxSetAcceptJoinSeminar(){
		$post_data = $this->input->post(); 


		$queryUpdate = "update SeminarRegister set AcceptJoin = 1,AcceptJoinDatetime = '".date('Y-m-d H:i:s')."' where AgentCode = '".$post_data['agentcode']."' and SeminarNo = '".$post_data['seminarno']."'";

		$stmt = sqlsrv_query( $this->seminar_conn, $queryUpdate,array(),array( "Scrollable" => 'static' )); 



		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	}
	private function connectSeminarDB(){

		if(ENVIRONMENT == 'development'){


	    	$serverName = $this->db_config['development']['servername']; //serverName\instanceName
	    	$connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
	    	$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);


	    	if(!$this->seminar_conn){
	    		echo "Connection could not be established.<br />";
	    		die( print_r( sqlsrv_errors(), true));
	    	}
	    }else if(ENVIRONMENT == 'testing'){
			$serverName = $this->db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
				echo "Connection could not be established.<br />";
				die( print_r( sqlsrv_errors(), true));
			}



		}
	}

	private function getSeminarAvailable($data = []){
		$query = "select * from SeminarInfo  
		join SeminarInfoGroup on SeminarInfo.SeminarInfoGroup = SeminarInfoGroup.SeminarInfoGroup";
		$query .= " join SeminarRegister on SeminarRegister.SeminarNo = SeminarInfo.SeminarNo";
		$query .= " where SeminarInfo.SeminarDate >= '".date('Y-m-d')."'";
		$query .= " and SeminarRegister.AgentCode = '".$data['agentcode']."'";
		$query .= " order by SeminarInfo.SeminarDate asc"; 

    	// echo $query;exit;

		$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' )); 

    	// print_r(sqlsrv_num_rows($stmt));exit;

		if(sqlsrv_num_rows($stmt) > 0){
			$result = sqlsrv_fetch_object($stmt); 

    		// print_r($result);exit;
			return $result;
		}

	}

	private function getApiUrlTicket(){
		switch (ENVIRONMENT) {
			case 'development':
				return 'http://apiservice.psisat.com/uploaded/seminar/ticket/seminar_card.png';
			break;
			case 'testing':

			break;

			case 'production':
				return 'http://apiservice.psisat.com/uploaded/seminar/ticket/seminar_card.png';
			break;
			default:
				# code...
			break;
		}
	}

	private function getTechnicianDataByAgentCode($agent_code = ""){

		$query = $this->db->select('AgentCode,AgentName,AgentSurName')
		->from('Agent')
		->where('AgentCode',$agent_code)
		->get();

		return $query->row();

	}

	private function checkTechnicialAreadyAcceptJoin($data = []){

		$qureyCheck = "select * from SeminarRegister where AgentCode = '".$data['agentcode']."' and SeminarNo = '".$data['seminarno']."'"; 

		//echo $qureyCheck;exit;

		$stmt = sqlsrv_query( $this->seminar_conn, $qureyCheck,array(),array( "Scrollable" => 'static' )); 

    	// print_r(sqlsrv_num_rows($stmt));exit;

		if(sqlsrv_num_rows($stmt) > 0){
			$row = sqlsrv_fetch_object($stmt); 

			return [
				'status'=>true,
				'SeminarRegister'=>$row
			];
		}else{
			return [
				'status'=>false
			];
		}

	}

}