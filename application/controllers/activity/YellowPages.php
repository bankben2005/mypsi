<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class YellowPages extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	//private $product_register_conn;
	//private $seminar_conn;

	public function __construct() {
            parent::__construct(); 

            

    }

    public function index(){
    		$html_redirect = 'https://www.yellowpages.co.th/form/psi-dealer2020';

   //  		header("Location: https://www.yellowpages.co.th/campaign/psi-dealer2020");
			// exit();

			$this->load->view('activity/yellowpages',[
				'redirect_url'=>$html_redirect
			]);
    }

}