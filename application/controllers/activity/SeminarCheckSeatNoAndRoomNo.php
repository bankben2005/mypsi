<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SeminarCheckSeatNoAndRoomNo extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	//private $product_register_conn;
	private $seminar_conn;

	public function __construct() {
            parent::__construct(); 

            $this->load->config('internaldb');
            $this->db_config = $this->config->item('seminar_db');
            $this->connectSeminarDB();

            $this->seminar_available = $this->getSeminarAvailable();
    }

    public function index(){  

    	//print_r($this->seminar_available);



    	$data = [

    	];


    	$this->load->view('activity/seminar_check_seat_and_room',$data);



    }

    public function ajaxGetSeatAndRoomInfo(){
    	$post_data = $this->input->post(); 


    	// check agent available for this seminar





    	if($this->seminar_available->SeminarType == '3'){ 
    		$queryCheckAvailable = "select * from SeminarRegister where SeminarNo = '".$this->seminar_available->SeminarNo."' and AgentCode = '".$post_data['agentcode']."'"; 

    		$stmt = sqlsrv_query( $this->seminar_conn, $queryCheckAvailable,array(),array( "Scrollable" => 'static' ));
		    	// echo ENVIRONMENT;exit;
		   	if(sqlsrv_num_rows($stmt) <= 0){ 

		   		$view = $this->load->view('activity/ajax_not_found_register_information',[],true);

		   		echo json_encode([
		   			'status'=>true,
		   			'query'=>$queryCheckAvailable,
		   			'post_data'=>$post_data,
		   			'view'=>$view
		   		]);exit;

		   	}

    	}else{

    		// check by purchase seminar card



    	}
    	


    	$dataView = [];


    	$query1 = "select * from SeminarSeats where AgentCode = '".$post_data['agentcode']."' and SeminarNo = '".$this->seminar_available->SeminarNo."'"; 

    	// echo $query; 

    	$stmt1 = sqlsrv_query( $this->seminar_conn, $query1,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt1) > 0){
    		$row = sqlsrv_fetch_object($stmt1);
    		$dataView['SeminarSeats'] = $row;

    	}

    	$view = $this->load->view('activity/ajax_render_seat_and_room',$dataView,true);


    	echo json_encode([
    		'status'=>true,
    		'post_data'=>$post_data,
    		'view'=>$view,
    		'seminar_available'=>$this->seminar_available
    	]);
    }
    private function getSeminarAvailable(){
    	$query = "select TOP 1 * from SeminarInfo where SeminarDate >= '".date('Y-m-d')."' order by SeminarDate asc";

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt) > 0){
    		$result = sqlsrv_fetch_object($stmt);
    		return $result;
    	}
    		
    }


    private function connectSeminarDB(){

    	if(ENVIRONMENT == 'development'){


	    	$serverName = $this->db_config['development']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}
		}else if(ENVIRONMENT == 'testing'){
			$serverName = $this->db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}



		}
    }

    


}