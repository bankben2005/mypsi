<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class PsiCareModule extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }


    public function RegisterProduct_get(){
        
        if(!empty($this->get()) || file_get_contents('php://input')){

            $request = array(
            'consumerid' => @$this->get('consumerid'),
            'productid' => @$this->get('productid'),
            'serialno' => @$this->get('serialno'),
            'ppm' => @$this->get('ppm')
            );

             $this->load->library('requestAuthenAPI');
             $requestAuthenAPI = RequestAuthenAPI::get_instance();
             $requestCriteria = new stdClass();
             $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

             // print_r($requestCriteria);exit;

             
             $output = $this->__processRegisterProduct($requestCriteria);
             echo json_encode($output);exit;

             $this->set_response($output,  REST_Controller::HTTP_OK);


        }

    }
    public function RegisterProduct_post(){
        
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('RegisterProduct');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__processRegisterProduct($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

            
        }


                    
        
        
    }

    public function GetCustomerProductList_post(){

            // exit;
            if($this->post('request') || file_get_contents('php://input')){

                        $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                        // print_r($request);exit;
                        
                        $this->load->library('requestAuthenAPI');
                        
                        $requestAuthenAPI = RequestAuthenAPI::get_instance();
                        $requestCriteria = new stdClass();
                        $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                        // print_r($requestCriteria);exit;

                        $requestAuthenAPI->set_requireMethod('GetCustomerProductList');
                        if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                            $error = array();
                            $error['status'] = false;
                            $error['result_code'] = "-001";
                             $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                             echo json_encode($error);exit;
                            // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                            //exit;
                        }else{

                            $output = $this->__processProductInfo($requestCriteria);
                            // echo json_encode($output);exit;
                            echo json_encode($output);exit;

                            // $this->set_response($output,  REST_Controller::HTTP_OK);

                        }
            }else{
                $arReturn = array(
                    'status' => false,
                    'result_code'=>'-005',
                    'result_desc'=>'RequestException : Request not found.'
                );
                echo json_encode($arReturn);exit;

                // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

                
            }

    }

    public function GetCustomerProductDetail_post(){

            if($this->post('request') || file_get_contents('php://input')){

                        $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                        // print_r($request);exit;
                        
                        $this->load->library('requestAuthenAPI');
                        
                        $requestAuthenAPI = RequestAuthenAPI::get_instance();
                        $requestCriteria = new stdClass();
                        $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                        // print_r($requestCriteria);exit;

                        $requestAuthenAPI->set_requireMethod('GetCustomerProductDetail');
                        if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                            $error = array();
                            $error['status'] = false;
                            // $error['error']['title'] = "Authentication Exception";
                            // $error['error']['message']  = "Can't authentication,please try again.";
                            $error['result_code'] = "-001";
                             $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                             echo json_encode($error);exit;
                            // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                            //exit;
                        }else{
                            // print_r($requestCriteria);exit;

                            $output = $this->__processProductDetail($requestCriteria);
                            // echo json_encode($output);exit;
                            echo json_encode($output);exit;

                            // $this->set_response($output,  REST_Controller::HTTP_OK);

                        }
            }else{
                $arReturn = array(
                    'status' => false,
                    'result_code'=>'-005',
                    'result_desc'=>'RequestException : Request not found.'
                );
                echo json_encode($arReturn);exit;

                // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

                
            }
    }

    public function GetCustomerProductPartDetail_post(){

            if($this->post('request') || file_get_contents('php://input')){

                        $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                        // print_r($request);exit;
                        
                        $this->load->library('requestAuthenAPI');
                        
                        $requestAuthenAPI = RequestAuthenAPI::get_instance();
                        $requestCriteria = new stdClass();
                        $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                        // print_r($requestCriteria);exit;

                        $requestAuthenAPI->set_requireMethod('GetCustomerProductPartDetail');
                        if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                            $error = array();
                            $error['status'] = false;
                            // $error['error']['title'] = "Authentication Exception";
                            // $error['error']['message']  = "Can't authentication,please try again.";
                            $error['result_code'] = "-001";
                             $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                             echo json_encode($error);exit;
                            // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                            //exit;
                        }else{
                            // print_r($requestCriteria);exit;

                            $output = $this->__processProductPartDetail($requestCriteria);
                            // echo json_encode($output);exit;
                            echo json_encode($output);exit;

                            // $this->set_response($output,  REST_Controller::HTTP_OK);

                        }
            }else{
                $arReturn = array(
                    'status' => false,
                    'result_code'=>'-005',
                    'result_desc'=>'RequestException : Request not found.'
                );
                echo json_encode($arReturn);exit;

                // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

                
            }

    }

    public function SetProductPartNotification_post(){

            if($this->post('request') || file_get_contents('php://input')){

                        $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                        // print_r($request);exit;
                        
                        $this->load->library('requestAuthenAPI');
                        
                        $requestAuthenAPI = RequestAuthenAPI::get_instance();
                        $requestCriteria = new stdClass();
                        $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                        // print_r($requestCriteria);exit;

                        $requestAuthenAPI->set_requireMethod('SetProductPartNotification');
                        if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                            $error = array();
                            $error['status'] = false;
                            // $error['error']['title'] = "Authentication Exception";
                            // $error['error']['message']  = "Can't authentication,please try again.";
                            $error['result_code'] = "-001";
                             $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                             echo json_encode($error);exit;
                            // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                            //exit;
                        }else{
                            //print_r($requestCriteria);exit;

                            $output = $this->__processSetProductPartNotification($requestCriteria);
                            echo json_encode($output);exit;
                            // echo json_encode($output);exit;

                            // $this->set_response($output,  REST_Controller::HTTP_OK);

                        }
            }else{
                $arReturn = array(
                    'status' => false,
                    'result_code'=>'-005',
                    'result_desc'=>'RequestException : Request not found.'
                );
                echo json_encode($arReturn);exit;

                // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

                
            }

    }

    public function ResetProductPartWarranty_post(){

            if($this->post('request') || file_get_contents('php://input')){

                        $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                        // print_r($request);exit;
                        
                        $this->load->library('requestAuthenAPI');
                        
                        $requestAuthenAPI = RequestAuthenAPI::get_instance();
                        $requestCriteria = new stdClass();
                        $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                        // print_r($requestCriteria);exit;

                        $requestAuthenAPI->set_requireMethod('ResetProductPartWarranty');
                        if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                            $error = array();
                            $error['status'] = false;
                            // $error['error']['title'] = "Authentication Exception";
                            // $error['error']['message']  = "Can't authentication,please try again.";
                            $error['result_code'] = "-001";
                             $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                             echo json_encode($error);exit;
                            // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                            //exit;
                        }else{
                            // echo json_encode($output);exit;
                            $output = $this->__processResetProductPartWarranty($requestCriteria);
                            echo json_encode($output);exit;
                            // $this->set_response($output,  REST_Controller::HTTP_OK);

                        }
            }else{
                $arReturn = array(
                    'status' => false,
                    'result_code'=>'-005',
                    'result_desc'=>'RequestException : Request not found.'
                );
                echo json_encode($arReturn);exit;

                // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

                
            }

    }
    public function CheckRegisterProductSN_post(){
            if($this->post('request') || file_get_contents('php://input')){

                        $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                        // print_r($request);exit;
                        
                        $this->load->library('requestAuthenAPI');
                        
                        $requestAuthenAPI = RequestAuthenAPI::get_instance();
                        $requestCriteria = new stdClass();
                        $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                        // print_r($requestCriteria);exit;

                        $requestAuthenAPI->set_requireMethod('CheckRegisterProductSN');
                        if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                            $error = array();
                            $error['status'] = false;
                            // $error['error']['title'] = "Authentication Exception";
                            // $error['error']['message']  = "Can't authentication,please try again.";
                            $error['result_code'] = "-001";
                             $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                             echo json_encode($error);exit;
                            // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                            //exit;
                        }else{
                            // echo json_encode($output);exit;
                            $output = $this->__processCheckRegisterProductSN($requestCriteria);
                            echo json_encode($output);exit;
                            // $this->set_response($output,  REST_Controller::HTTP_OK);

                        }
            }else{
                $arReturn = array(
                    'status' => false,
                    'result_code'=>'-005',
                    'result_desc'=>'RequestException : Request not found.'
                );
                echo json_encode($arReturn);exit;

                // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

                
            }
    }

    public function CampaignCronSendInsuranceToTip_post(){

            if($this->post('request') || file_get_contents('php://input')){

                        $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                        // print_r($request);exit;
                        
                        $this->load->library('requestAuthenAPI');
                        
                        $requestAuthenAPI = RequestAuthenAPI::get_instance();
                        $requestCriteria = new stdClass();
                        $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                        // print_r($requestCriteria);exit;

                        $requestAuthenAPI->set_requireMethod('CampaignCronSendInsuranceToTip');
                        if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                            $error = array();
                            $error['status'] = false;
                            // $error['error']['title'] = "Authentication Exception";
                            // $error['error']['message']  = "Can't authentication,please try again.";
                            $error['result_code'] = "-001";
                             $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                             echo json_encode($error);exit;
                            // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                            //exit;
                        }else{
                            // echo json_encode($output);exit;
                            $output = $this->__campaignCronSendInsuranceToTip($requestCriteria);
                            echo json_encode($output);exit;
                            // $this->set_response($output,  REST_Controller::HTTP_OK);

                        }
            }else{
                $arReturn = array(
                    'status' => false,
                    'result_code'=>'-005',
                    'result_desc'=>'RequestException : Request not found.'
                );
                echo json_encode($arReturn);exit;

                // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);

                
            }


    }

    public function testSendExpiredWaterFilter_get(){

        $requestCriteria = new stdClass();

        //print_r($requestCriteria);exit;

        require_once(APPPATH. "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->testSendExpiredWaterFilter_get($requestCriteria);
        return $output;

    }



    private function __processRegisterProduct($requestCriteria){
        require_once(APPPATH . "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->registerProduct($requestCriteria);
        return $output;
    }

    private function __processProductInfo($requestCriteria){
        require_once(APPPATH. "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->getProductInfo($requestCriteria);
        return $output;

    }

    private function __processProductDetail($requestCriteria){
        require_once(APPPATH. "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->getProductDetail($requestCriteria);
        return $output;
    }

    private function __processProductPartDetail($requestCriteria){
        require_once(APPPATH. "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->getProductPartDetail($requestCriteria);
        return $output;
    }

    private function __processSetProductPartNotification($requestCriteria){
        require_once(APPPATH. "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->setProductNotification($requestCriteria);
        return $output;
    }

    private function __processResetProductPartWarranty($requestCriteria){
        require_once(APPPATH. "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->resetProductPartWarranty($requestCriteria);
        return $output;
    }
    private function __processCheckRegisterProductSN($requestCriteria){
        require_once(APPPATH. "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->checkRegisterProductSN($requestCriteria);
        return $output;
    }

    private function __campaignCronSendInsuranceToTip($requestCriteria){
        require_once(APPPATH. "libraries/Product/ProductLibrary.php");
        $output = array();
        $ProductLibrary = new ProductLibrary();
        $output = $ProductLibrary->campaignCronSendInsuranceToTip($requestCriteria);
        return $output;
    }
    

}
