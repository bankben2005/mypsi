<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Questionnaire/QuestionnaireLibrary.php';
class Questionnaire extends QuestionnaireLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct() {
		parent::__construct();

	}
	public function index(){
		
		$this->template->stylesheet->add('https://fonts.googleapis.com/css?family=Kanit');
		$this->template->stylesheet->add(base_url('assets/questionnaire/css/questionnairebranchlist.css?'.strtotime(date('Y-m-d H:i:s'))));

		$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
		$this->template->javascript->add(base_url('assets/questionnaire/js/questionnairebranchlist.js?'.strtotime(date('Y-m-d H:i:s'))));

		$this->template->meta->add('keywords','');
		$this->template->meta->add('description','');


		$this->setData('seo_title','');
		$this->setData('seo_keywords','');
		$this->setData('seo_description','');

		$data = $this->getData();

		$this->template->content->view('questionnaire/questionnairebranchlist',$data);
		$this->template->publish();


	}

	public function DoQuestionnaire(){
		$data_receive = array(
			'agencode' => $this->input->get('agentcode'),
			'suname' => $this->input->get('suname')
		);


		$this->template->stylesheet->add('https://fonts.googleapis.com/css?family=Kanit');
		$this->template->stylesheet->add(base_url('assets/css/star-rating/star-rating.min.css'));
		$this->template->stylesheet->add(base_url('assets/questionnaire/css/questionnairebranchlist.css?'.strtotime(date('Y-m-d H:i:s'))));

		$this->template->javascript->add(base_url('assets/js/star-rating/star-rating.min.js'));
		$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
		$this->template->javascript->add(base_url('assets/questionnaire/js/doquestionnaire.js?'.strtotime(date('Y-m-d H:i:s'))));


		$this->template->meta->add('keywords','');
		$this->template->meta->add('description','');


		$this->setData('seo_title','');
		$this->setData('seo_keywords','');
		$this->setData('seo_description','');
		$this->setData('questionnaire',$this->getQuestionnaire());
		$this->setData('sunamedata',$this->getSUNAMEData($data_receive['suname']));



		$data = $this->getData();

		//print_r($data);exit;

		$this->template->content->view('questionnaire/doquestionnaire',$data);
		$this->template->publish();


		//print_r($data_receive);

	}

	public function createQuestionnaireSet(){


	}
	public function editQuestionnaireSet($id){
					 $questionnaireset = new M_quesionnaireset($id);
                      
                      if($article->getId()){
                          $this->__createQuestionnaireSet($id);
                      }else{
                          redirect($this->controller);
                      }

	}

	private function __createQuestionnaireSet($id=null){


	}

	public function manualInsertGroup(){
		$category = new M_questionnairecategory();
		$category->Name = 'สินค้า';
		$category->Created = date('Y-m-d H:i:s');
		$category->save();

	}

	public function manualInsertRate(){
		$rate = new M_questionnairerate();
		$rate->Name = 'ดีมาก';
		$rate->Rate = 5;
		$rate->Created = date('Y-m-d H:i:s');
		$rate->save();
	}

	public function getAgentBuyProduct(){



		$query = $this->db->select('POSTRANSACTION.AgentCode,POSTRANSACTION.SUNAME,Agent.AgentCode')
		->from('POSTRANSACTION')
		->join('Agent','Agent.AgentCode = POSTRANSACTION.AgentCode')
		->where('POSTRANSACTION.AgentCode','5820966')
		->get();
		//echo $this->db->last_query();exit;

		if($query->num_rows() > 0){

			$arrSuname = array();
			$rowData = $query->result();
			//print_r($rowData);


			foreach ($rowData as $key => $value) {
				# code...
				array_push($arrSuname, $value->SUNAME);
			}

			$arrSuname = array_unique($arrSuname);
			//print_r($arrSuname);
		}



	}

	public function setDefaultActivity(){
		$query = $this->db->insert('ArmActivity',
			array('Name'=>'ประเมินสาขาและ PT',
				'ArmActivityType_id' => 1,
				'StartDate' => '2018-02-08',
				'EndDate' => '2018-03-01'

			));
	}

	public function ajaxGetSUNAMEByAgentCode(){
		$dataReturn = array();
		$data_receive = array(
			'agentcode' => $this->input->post('agentcode')
		);

		// if($data_receive['agentcode'] == '5990099'){
		// 	$data_receive['agentcode'] = '5860517';
		// }

		$query = $this->db->select('POSTRANSACTION.AgentCode,POSTRANSACTION.SUNAME,Agent.AgentCode')
		->from('POSTRANSACTION')
		->join('Agent','Agent.AgentCode = POSTRANSACTION.AgentCode')
		->where('POSTRANSACTION.AgentCode',$data_receive['agentcode'])
		->get();
		//echo $this->db->last_query();exit;

		$arrSuname = array();
		if($query->num_rows() > 0){
			$rowData = $query->result();
			//print_r($rowData);


			foreach ($rowData as $key => $value) {
				# code...
				array_push($arrSuname, $value->SUNAME);
			}

			$arrSuname = array_unique($arrSuname);
			//print_r($arrSuname);exit;

		}

		if(count($arrSuname) > 0){
			foreach ($arrSuname as $key => $value) {
				# code...
				$branchQuery = $this->db->select('AgentCode,AgentName,AgentSurName,Province,AgentType,TradeName')
				->from('Agent')
				->where('AgentCode',$value)
				->get();

				$row =$branchQuery->row();
				$row->{'is_answer'} = $this->checkIsAnswer(array('suname'=>$value,'agentcode'=>$data_receive['agentcode']));

				array_push($dataReturn, $row);
			}

			$view = $this->load->view('questionnaire/questionnairebranchrow',array('branchrow'=>$dataReturn),true);


			echo json_encode(array('status'=>true,'data'=>$dataReturn,'view'=>$view));exit;
		}else{
			echo json_encode(array('status'=>false));exit;

		}

	}

	private function getQuestionnaire(){
		$query = $this->db->select('*,Questionnaire.id as questionnaire_id')
		->from('Questionnaire')
		->join('QuestionnaireSet','Questionnaire.QuestionnaireSet_id = QuestionnaireSet.id')
		->where('QuestionnaireSet.StartDate <=',date('Y-m-d'))
		->where('QuestionnaireSet.EndDate >=',date('Y-m-d'))
		->where('Questionnaire.Active',1)
		->get();

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}

	}

	private function getSUNAMEData($suname = ""){
		$query = $this->db->select('AgentCode,AgentName,AgentSurName,Province,AgentType,TradeName')
		->from('Agent')
		->where('AgentCode',$suname)
		->get();

		if($query->num_rows() > 0){
				return $query->row();
		}else{
				return false;
		}

	}

	private function getQuestionnaireSetAvailable(){
		$query = $this->db->select('id,StartDate,EndDate')
		->from('QuestionnaireSet')
		->where('EndDate >=',date('Y-m-d'))
		->get();
		if($query->num_rows() > 0){
			return $query->row()->id;
		}else{
			return 0;
		}
	}

	private function checkIsAnswer($data = array()){
			$setid = $this->getQuestionnaireSetAvailable();


			$query = $this->db->select('AgentCode,SUNAME,QuestionnaireSet_id')
			->from('QuestionnaireAnswer')
			->where('AgentCode',$data['agentcode'])
			->where('SUNAME',$data['suname'])
			->where('QuestionnaireSet_id',$setid)
			->get();

			if($query->num_rows() > 0){
					return true;
			}else{
					return false;
			}


	}

	private function getSUNAMEByAgentCode($data = array()){
		$query = $this->db->select('POSTRANSACTION.AgentCode,POSTRANSACTION.SUNAME,Agent.AgentCode')
		->from('POSTRANSACTION')
		->join('Agent','Agent.AgentCode = POSTRANSACTION.AgentCode')
		->where('POSTRANSACTION.AgentCode',$data['agentcode'])
		->get();
		//echo $this->db->last_query();exit;

		$arrSuname = array();
		if($query->num_rows() > 0){
			$rowData = $query->result();
			//print_r($rowData);


			foreach ($rowData as $key => $value) {
				# code...
				array_push($arrSuname, $value->SUNAME);
			}

			$arrSuname = array_unique($arrSuname);
			//print_r($arrSuname);exit;

		}

		return $arrSuname;



	}

	private function getQuestionnaireAnswerByAgentCode($data = array()){
		$setid = $this->getQuestionnaireSetAvailable();

		$query = $this->db->select('*')
		->from('QuestionnaireAnswer')
		->where('AgentCode',$data['agentcode'])
		->where('QuestionnaireSet_id',$setid)
		->get();

		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return true;
		}


	}

	public function ajaxSubmitQuestionnaire(){
		$questionnaire = $this->getQuestionnaire();

		/* insert questionnaire answer */
		$insert_questionnaireanswer = array(
			'QuestionnaireSet_id' => $this->getQuestionnaireSetAvailable(),
			'AgentCode' => $this->input->post('agentcode'),
			'SUNAME' => $this->input->post('suname'),
			'Created' => date('Y-m-d H:i:s'),
			'Recommend' => $this->input->post('Recommend')
		);

		// print_r($insert_questionnaireanswer);exit;

		if($this->db->insert('QuestionnaireAnswer',$insert_questionnaireanswer)){
				/* insert questionnaire answer list */
				$insert_id = $this->db->insert_id();

				foreach ($questionnaire as $key => $value) {
					# code...
					$insert_questionnaireanswerlist = array(
						'QuestionnaireAnswer_id' => $insert_id,
						'Questionnaire_id'=>$value->questionnaire_id,
						'QuestionnaireRate_id'=>$this->input->post('rating_'.$value->questionnaire_id.''),
						'Created' => date('Y-m-d H:i:s')
					);

					$this->db->insert('QuestionnaireAnswerList',$insert_questionnaireanswerlist);
				}

				/* get QuestionnaireCoupon Available*/
				$has_coupon = false;
				$get_coupon = false;
				$QuestionnaireCoupon = "";
				$QuestionnaireCoupon = $this->getQuestionnaireCouponAvailable();

				if($QuestionnaireCoupon){
					$has_coupon = true;
					/* count for get promotion */
					$dataAllSuName = $this->getSUNAMEByAgentCode(array('agentcode'=>$this->input->post('agentcode')));

					$dataQuestionnaireAnswer = $this->getQuestionnaireAnswerByAgentCode(array('agentcode'=>$this->input->post('agentcode')));

					if(count($dataAllSuName) == count($dataQuestionnaireAnswer)){
						/* if assessment all*/
						/* insert data to couponagent table */
						$insertCouponAgent = array(
							'AgentCode' => $this->input->post('agentcode'),
							'Code' => $QuestionnaireCoupon->Code,
							'Coupon_id' => $QuestionnaireCoupon->coupon_id,
							'Created' => date('Y-m-d H:i:s')
						);

						if($this->db->insert('CouponAgent',$insertCouponAgent)){
							$get_coupon = true;
						}
					}

				}



				/* eof count for get promotion */

				echo json_encode(array('status'=>true,'has_coupon'=>$has_coupon,'get_coupon'=>$get_coupon,'coupon_data'=>$QuestionnaireCoupon,'agentcode'=>$this->input->post('agentcode')));exit;



		}else{
				echo json_encode(array('status'=>false));exit;
		}


		/* eof insert questionnaire answer */
	}

	private function getQuestionnaireCouponAvailable(){
		$query = $this->db->select('*,Coupon.id as coupon_id')
		->from('Coupon')
		->join('CouponType','Coupon.CouponType_id = CouponType.id')
		->where('CouponType.Name','Questionnaire')
		->where('Coupon.StartDate <=',date('Y-m-d'))
		->where('Coupon.EndDate >=',date('Y-m-d'))
		->get();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}


	}

	public function setDefaultCoupon(){
		$data = array(
			'Code' => 'QBR18',
			'Name' => 'รับส่วนลดจากการประเมิน',
			'DiscountRate' => 100,
			'DiscountType' => 'amount',
			'StartDate' => date('Y-m-d'),
			'EndDate' => '2018-03-01',
			'CouponType_id' => 1,
			'Created' => date('Y-m-d H:i:s'),
			'Active' => 1
		);

		$this->db->insert('Coupon',$data);
		echo $this->db->last_query();exit;

	}

	public function testcouponavailable(){
		$row = $this->getQuestionnaireCouponAvailable();
		print_r($row);
	}



}