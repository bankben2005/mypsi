<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class PsiSeminarModule extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }


    public function GetTicketDetail_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetTicketDetail');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getTicketDetail($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }


    }

    public function SeminarCheckin_post(){
        //echo 'aaaa';
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenSeminarAPI');
                    
                    $requestAuthenAPI = requestAuthenSeminarAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SeminarCheckin');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setSeminarCheckin($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }

    public function TestTechnicianCheckin_get(){
            $this->__testTechnicianCheckin();
    }

    public function DeleteDemoRegister_get(){
        
        require_once(APPPATH. "libraries/Seminar/SeminarLibrary.php");
        $output = array();
        $SeminarLibrary = new SeminarLibrary();
        $output = $SeminarLibrary->deleteDemoRegister();
        $SeminarLibrary->closeConnectSeminarDB();
        return $output;

    }
    public function __testTechnicianCheckin(){

        require_once(APPPATH. "libraries/Seminar/SeminarLibrary.php");
        $output = array();
        $SeminarLibrary = new SeminarLibrary();
        $output = $SeminarLibrary->testTechnicianCheckin();
        $SeminarLibrary->closeConnectSeminarDB();
        return $output;
    }

    private function __setSeminarCheckin($requestCriteria){
        //print_r($requestCriteria);exit;
        require_once(APPPATH. "libraries/Seminar/SeminarLibrary.php");
        $output = array();
        $SeminarLibrary = new SeminarLibrary();
        $output = $SeminarLibrary->setSeminarCheckin($requestCriteria);
        $SeminarLibrary->closeConnectSeminarDB();
        return $output;
    }
    private function __getTicketDetail($requestCriteria){

        require_once(APPPATH. "libraries/Seminar/SeminarLibrary.php");
        $output = array();
        $SeminarLibrary = new SeminarLibrary();
        $output = $SeminarLibrary->getTicketDetail($requestCriteria);
        $SeminarLibrary->closeConnectSeminarDB();
        return $output;

    }

}