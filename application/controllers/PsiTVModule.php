<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class PsiTVModule extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function GetAllChannelList_post(){
        
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllChannelList');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__getAllChannelList($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function GetAllChannelListUnAuth_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $output = $this->__getAllChannelList($requestCriteria);
                    $this->set_response($output,  REST_Controller::HTTP_OK);


        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }

    }


    public function GetChannelDetail_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetChannelDetail');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__getChannelDetail($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function GetChannelDetailUnAuth_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;
                    $output = $this->__getChannelDetail($requestCriteria);
                    $this->set_response($output,  REST_Controller::HTTP_OK);

        }else{

            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }


    }

    public function TogglesFavouriteChannel_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('TogglesFavouriteChannel');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__togglesFavouriteChannel($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function GetFavouriteChannel_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetFavouriteChannel');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__getFavouriteChannel($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function GetRecentChannelView_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetRecentChannelView');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__getRecentChannelView($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }


    }

    public function InsertChannelList_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('InsertChannelList');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__insertChannel($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function UpdateChannelList_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('UpdateChannelList');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__updateChannelList($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }



    }

    public function GetChannelCategories_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetChannelCategories');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__getChannelCategories($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function GetChannelByCategory_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetChannelByCategory');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__getChannelByCategory($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function SetTVBannerClick_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetTVBannerClick');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__setTVBannerClick($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function GetS3TVChannelList_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('requestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetS3TVChannelList');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                        // $this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
                        //exit;
                    }else{
                        // echo 'eeeee';exit;

                        $output = $this->__getS3TVChannelList($requestCriteria);
                        echo json_encode($output);exit;


                        // $this->set_response($output,  REST_Controller::HTTP_OK);

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);

            // $this->set_response($arReturn,  REST_Controller::HTTP_BAD_REQUEST);
        }

    }




    private function __getAllChannelList($requestCriteria){
        //$this->insertChannel();
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->getAllChannelList($requestCriteria);
        return $output;
    }

    private function __getChannelDetail($requestCriteria){
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->getChannelDetail($requestCriteria);
        return $output;
    }
    private function __togglesFavouriteChannel($requestCriteria){
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->togglesFavouriteChannel($requestCriteria);
        return $output;
    }

    private function __getFavouriteChannel($requestCriteria){
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->getFavouriteChannel($requestCriteria);
        return $output;
    }

    private function __getRecentChannelView($requestCriteria){
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->getRecentChannelView($requestCriteria);
        return $output;
    }

    private function __updateChannelList($requestCriteria){
        // $this->insertChanel();
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->updateChannelList($requestCriteria);
        return $output;
    }
    private function __getChannelCategories($requestCriteria){
        // $this->insertChanel();
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->getChannelCategories($requestCriteria);
        return $output;
    }

    private function __getChannelByCategory($requestCriteria){
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->getChannelByCategory($requestCriteria);
        return $output;
        
    }

    private function __setTVBannerClick($requestCriteria){
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->setTVBannerClick($requestCriteria);
        return $output;
    }
    private function __insertChannel($requestCriteria){
        //print_r($requestCriteria);exit;
                $apiConfig = "";
                $get_channel_url = "";

                $this->load->config('api');
                $apiConfig = $this->config->item('tvapis');

                $get_channel_url = $apiConfig['get_channel'];

                //print_r($get_channel_url);exit;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $get_channel_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                $response = json_decode($response);

                //print_r($response);exit;
                foreach ($response->result->channels as $key => $value) {
                    //print_r($value);exit;
                    $dataInsert = array(
                        'api_id'=>$value->id,
                        'channel_number'=>$value->channel_number[0],
                        'ordinal'=>$value->ordinal,
                        'streamer'=>$value->streamer,
                        'nodes_host'=>$value->nodes[0]->host,
                        'nodes_folder'=>$value->nodes[0]->folder,
                        'name'=>$value->name,
                        'description'=>$value->description,
                        'logo'=>$value->logo,
                        'country_iso2'=>$value->country,
                        'created'=>$value->created_date,
                        'updated'=>$value->updated_date
                    );
                    //print_r($dataInsert);exit;
                    $this->db->insert('TVChannels',$dataInsert);

                    /* insert default to tvchannelcategorylist */
                    $insertCategoryList =array(
                        'TVChannels_id'=>$this->db->insert_id(),
                        'TVChannelCategories_id'=>'1',
                        'created'=> date('Y-m-d H:i:s')
                    );
                    $this->db->insert('TVChannelCategoryLists',$insertCategoryList);

                    /* eof insert default tvchannelcategorylist */

                    //echo $this->db->last_query();exit;
                }

                return array(
                    'status' => true
                );



    }

    private function __getS3TVChannelList($requestCriteria){
        require_once(APPPATH. "libraries/TV/TVApiLibrary.php");
        $output = array();
        $TVApiLibrary = new TVApiLibrary();
        $output = $TVApiLibrary->getS3TVChannelList($requestCriteria);
        return $output;
    }




}