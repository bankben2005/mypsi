<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class PsiActivityModule extends REST_Controller {

    public $sms_config = array();
    function __construct(){
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function SendSms_get(){
        $output = array();

        if(!$this->get('telephone_number') || !$this->get('message')){
            $this->set_response(array(
                'status'=>false,
                'result_code'=>'-002',
                'result_desc'=>'ไม่พบ เบอร์โทรหรือข้อความ'
            ),  REST_Controller::HTTP_OK);
            exit;
        }

        $this->load->config('sms');
        $this->sms_config = $this->config->item('ANTS');

        $telephone_number = $this->get('telephone_number');

        if(substr($telephone_number, 0,1) == '0'){
          $telephone_number = '66'.substr($telephone_number, 1,9);
        }

        $data = array(
            'from'=>$this->sms_config['Sender'],
            'to'=>$telephone_number,
            'text'=>$this->get('message').' '.$this->get('otp')
        );


        $sendSms = $this->curlSendSMSAnts($data);
        if($sendSms['status']){
            $output =  array(
              'status'=>true,
              'result_code'=>'success',
              'result_desc'=>'Send sms success'
            );
            $this->set_response($output,  REST_Controller::HTTP_OK);

        }else{
            $output =  array(
              'status'=>false,
              'result_code'=>'-002',
              'result_desc'=>$sendSms['name'].' - '.$sendSms['description']
            );
            $this->set_response($output,  REST_Controller::HTTP_OK);

        }
        // print_r($data);
        // print_r($this->sms_config);
        // print_r($this->get());exit;
    }

    public function UpdateChannelDirectLink_post(){
        $output = array();

        $post_data = $this->post();

        /* Create log file for debug*/
        $log_file_path = $this->createLogFilePath('updateChannelDirect');
        $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($post_data) . "\n";
        file_put_contents($log_file_path, $file_content, FILE_APPEND);
        unset($file_content);
        //exit;

        // print_r($post_data);exit;

        
        if(count($post_data) > 0){
            /* check channel id */
            $checkChannel = $this->db->select('id')
            ->from('TVChannels')->where('ofive_map_channel',$post_data['channel_id'])->get();

            if($checkChannel->num_rows() <= 0){

                // check for create channel
                if(isset($post_data['create_channel']) && $post_data['create_channel'] == 'true'){

                    // get max channel number and plus one for new channel
                    $max_channel_number = $this->getMaxChannelNumber();

                    $this->db->insert('TVChannels',[
                        'name'=>@$post_data['name'],
                        'description'=>@$post_data['description'],
                        'direct_streaming_link'=>@$post_data['direct_streaming_link'],
                        'direct_streaming_status'=>@$post_data['direct_streaming_status'],
                        'mv_status'=>@$post_data['mv_status'],
                        'active'=>@$post_data['active'],
                        'mv_request_streaming'=>@$post_data['mv_request_streaming'],
                        'ofive_map_channel'=>$post_data['channel_id'],
                        'channel_number'=>$max_channel_number
                    ]);

                    /* check upload profile image  */
                    if($_FILES['channel_logo']['error'] == 0){
                        $this->uploadChannelLogo(array(
                                'tvchannels_id'=>$this->db->insert_id()
                        ));
                    }

                    echo json_encode(array(
                        'status'=>true
                    ));exit;
                    
                }else{

                        // eof check for create channel
                        echo json_encode(array(
                            'status'=>false,
                            'message'=>'not-found-channel'
                        ));exit;
                }
            }else{

                $this->db->update('TVChannels',array(
                    'name'=>@$post_data['name'],
                    'description'=>@$post_data['description'],
                    'direct_streaming_link'=>@$post_data['direct_streaming_link'],
                    'direct_streaming_status'=>@$post_data['direct_streaming_status'],
                    'mv_status'=>@$post_data['mv_status'],
                    'active'=>@$post_data['active'],
                    'mv_request_streaming'=>@$post_data['mv_request_streaming']
                ),array('id'=>@$checkChannel->row()->id));

                


                /* check upload profile image  */
                if($_FILES['channel_logo']['error'] == 0){
                    $this->uploadChannelLogo(array(
                            'tvchannels_id'=>$checkChannel->row()->id
                    ));
                }


                echo json_encode(array(
                    'status'=>true
                ));exit;
            }

        }else{
            echo json_encode(array(
                'status'=>false,
                'message'=>'not-found-request'
            ));exit;

        }


        
    }

    public function UpdateTVChannelViewsDaily_get(){

        $date_for_export = "";
        if(isset($_GET['date']) && $_GET['date'] != ''){
            $date_for_export = $_GET['date'];
        }
        //$this->__updateTVChannelViewsDaily($date_for_export);

        //echo $date_for_export;exit;

        $to_day = new DateTime();
        $dateto_calculate = ($date_for_export)?$date_for_export:$to_day->format('Y-m-d');
        


        if(!$this->checkAlreadyTVChannelviewsDaily($dateto_calculate)){

           

            /* */
            $query = $this->db->select('count(TVChannelViews.id) as count_views,TVChannelViews.TVChannels_id as channel_id')
            ->from('TVChannelViews')
            //->join('TVChannels','TVChannelViews.TVChannels_id = TVChannels.id')
            ->where('CONVERT(char(10), TVChannelViews.created,126) = ',$dateto_calculate)
            ->group_by('TVChannelViews.TVChannels_id')
            ->order_by('count_views','desc')
            ->get();

            

            if($query->num_rows() > 0){


                    foreach($query->result() as $key => $row){
                        /* insert into tvchannelviews_daily */
                        $this->db->insert('TVChannelViewsDaily',array(
                            'tvchannels_id'=>$row->channel_id,
                            'date'=>$dateto_calculate,
                            'sum_views'=>$row->count_views,
                            'created'=>date('Y-m-d H:i:s')
                        ));
                        /* eof insert into tvchannelviews daily */
                    }

                    $this->db->delete('TVChannelViews',array(
                        'CONVERT(char(10), TVChannelViews.created,126) = '=>$dateto_calculate
                    ));

            }

        }

    }

    private function curlSendSMSAnts($data = array()){

        $authorize = "";
        $authorize = "Basic ".base64_encode($this->sms_config['Username'].':'.$this->sms_config['Password']);

        $ch = curl_init();   
        curl_setopt($ch,CURLOPT_URL,$this->sms_config['Url']);

        // if(ENVIRONMENT == 'production'){
        //   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type:application/json',
          'Authorization:'.$authorize
        ));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch,CURLOPT_POST,1); 
        curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));
        
        $Result = curl_exec($ch);
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        curl_close($ch);

        //print_r($Result);exit;
        $decode_result = json_decode($Result);

        if($decode_result->messages[0]->status->groupId == '5'){
            return array(
                'status'=>false,
                'name'=>$decode_result->messages[0]->status->name,
                'description'=>$decode_result->messages[0]->status->description
            );

        }else{
            return array(
                'status'=>true
            );
        }


        // return true;
        //print_r($decode_result);exit;
        // if($decode_result->messages[0]->status->groupId == '1'){
        //   return true;
        // }else{
        //   return false;
        // }

        // if($this->checkAntsMessageLogs($decode_result)){
        //   return true;
        // }else{
        //   return false;
        // }



    }

    private function checkAlreadyTVChannelviewsDaily($dateto_calculate){
        $query = $this->db->select('id,date')
        ->from('TVChannelViewsDaily')
        ->where('date',$dateto_calculate)
        ->limit(1)->get();

        if($query->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    private function uploadChannelLogo($data = []){

        $tvchannels_id = $data['tvchannels_id'];


        $this->load->library(array('upload'));
            //print_r($_FILES);
        if(!is_dir('./uploaded/tv/logo/')){
            mkdir('./uploaded/tv/logo',0777,true);
        }
        

        $config = array();                                         
        $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
        $config['upload_path'] = './uploaded/tv/logo/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '10000';
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('channel_logo')){
            $error = array('error' => $this->upload->display_errors());
            $this->msg->add($error['error'], 'error');

                // if(file_exists('uploaded/package_image/available/'.$package_id)){
                //         rmdir('uploaded/package_image/available/'.$package_id);
                // }

        }else{
            $data_upload = array('upload_data' => $this->upload->data());

                //$promotion_update = new M_promotion($promotion_id);
            $upload_file_name = $data_upload['upload_data']['file_name'];

                // $package = new M_package($package_id);
                // $package->img_package_available = $upload_file_name;
                // $package->img_package_available_size = $_FILES['img_package_available']['size'];
                // $package->save();
            $this->db->update('TVChannels',[
                'logo'=>$upload_file_name,
                'updated'=>date('Y-m-d H:i:s')
            ],['id'=>$tvchannels_id]);
            return true;
        }


    }

    private function createLogFilePath($filename = '') {
        
        //$document_root = $_SERVER['DOCUMENT_ROOT'];

        $log_path = APPPATH.'/logs/updateChannel';
        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function getMaxChannelNumber(){
        $query = $this->db->select('MAX(channel_number) as max_channel_number')
        ->from('TVChannels')
        ->get();

        return $query->row()->max_channel_number;
    }


    

}