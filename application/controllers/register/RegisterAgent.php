<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterAgent extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $data = array();
    public $page_num = 10;
    public $controller = '';
    public $method = '';

    public $db_config = array();
    public $ptm_conn;
    private $conn_mysql;


	public function __construct() {
            parent::__construct();


            		$this->setController($this->router->class);
                    $this->setMethod($this->router->method);

                    //$CI->load->helper(array('frontend','lang','our'));
                    //$CI->load->library(array('Msg','Lang_controller'));
                    $this->load->library(array('Lang_controller','Msg'));
                    $this->load->helper(array('lang', 'our','url'));
                    $this->load->config('internaldb');
                    $this->db_config = $this->config->item('ptm_db');
                    
                    $this->template->set_template('register/templateofive');


                    
                    $this->_load_js();
                    $this->_load_css();


    }

    public function index(){
            

    }
    public function generateCardNo(){

        $this->template->javascript->add(base_url('assets/js/clipboard/clipboard.min.js'));
        $this->template->javascript->add(base_url('assets/register/generatecardno.js'));


        $this->template->meta->add('keywords','');
        $this->template->meta->add('description','');


        $this->setData('seo_title','');
        $this->setData('seo_keywords','');
        $this->setData('seo_description','');

        $data = $this->getData();
        //print_r($data);

        $this->template->content->view('register/registeragent/generatecardnumber',$data);
        $this->template->publish();
    }

    /* load javascript
     */
    private function _load_js() {
        $this->template->javascript->add(base_url('assets/js/jquery/jquery.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap/bootstrap.min.js'));
        //$this->template->javascript->add(base_url('assets/js/bootstrap/bootstrap.bundle.min.js'));
        $this->template->javascript->add(base_url('assets/js/frontend/instance.js'));

    }
    /**
     * load style sheet
     */
    private function _load_css() {

         $this->template->stylesheet->add(base_url('assets/css/bootstrap/bootstrap.min.css'));
         $this->template->stylesheet->add('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

    }

    public function getController() {
        return $this->controller;
    }

    public function setController($controller) {
        $this->controller = $controller;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        $this->method = $method;
    }
    public function getData($key = null) {
        if (is_null($key) || empty($key))
            return $this->data;
        else
            return $this->data[$key];
    }

    public function setData($key, $data) {
        $this->data[$key] = $data;
    }

    public function ajaxGenerateAgentCardNumber(){
            $codegenerate = "";

            $strtotime = strtotime(date('Y-m-d H:i:s'));

            $strtotime = substr($strtotime, 1);
            $codegenerate = "0".$strtotime;

            echo json_encode(array('status'=>true,'code'=>$codegenerate));

    }
    public function ajaxGetCardnoDateTime(){
        $datetime = "";
        $data_received = array(
            'cardno' => $this->input->post('cardno')
        );

        $cardno_time = substr($data_received['cardno'], 1);
        $cardno_time = '1'.$cardno_time;

        $datetime = date('d/m/Y H:i:s',$cardno_time);


        echo json_encode(array('status'=>true,'datetime'=>$datetime));
    }

}