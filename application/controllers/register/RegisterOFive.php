<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterOFive extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $data = array();
    public $page_num = 10;
    public $controller = '';
    public $method = '';

    public $db_config = array();
    public $ptm_conn;
    private $conn_mysql;


	public function __construct() {
            parent::__construct();


            		$this->setController($this->router->class);
                    $this->setMethod($this->router->method);

                    //$CI->load->helper(array('frontend','lang','our'));
                    //$CI->load->library(array('Msg','Lang_controller'));
                    $this->load->library(array('Lang_controller','Msg'));
                    $this->load->helper(array('lang', 'our','url'));
                    $this->load->config('internaldb');
                    $this->db_config = $this->config->item('ptm_db');
                    
                    $this->template->set_template('register/templateofive');


                    $this->connectPtmDB();
                   // $this->connect_mysql();
                    $this->_load_js();
                    $this->_load_css();


    }

    public function index(){
            $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
            $this->template->javascript->add(base_url('assets/register/registerofive.js'));
            if($this->input->post(NULL,FALSE)){

                /* check exist serial number */
                $query = "select * from ptm_memory where sn = '".$this->input->post('product_sn')."'";

                
                $stmt = sqlsrv_query( $this->ptm_conn, $query,array(),array( "Scrollable" => 'static' ));

                if(sqlsrv_num_rows($stmt) > 0){
                  $obj = sqlsrv_fetch_object($stmt);

                  /* check already register */
                  if($obj->sta == 'success'){
                        $this->msg->add('อุปกรณ์ได้ถูกทำการลงทะเบียนแล้ว','error');
                        redirect($this->uri->uri_string());

                  }else{


                  /* eof check already register */


                      /* only update status and update contact*/
                      
                      $onlyupdate = "update ptm_memory set sta = 'success',fname= '".$this->input->post('firstname')."',lname='".$this->input->post('lastname')."',phone='".$this->input->post('telephone')."' where c_sn = '".$obj->c_sn."'";

                      if(sqlsrv_query($this->ptm_conn,$onlyupdate,array(),array( "Scrollable" => 'static' ))){
                        $this->msg->add('ทำการลงทะเบียนเรียบร้อย','success');
                        redirect($this->uri->uri_string());
                        
                      }else{
                        $this->msg->add('เกิดข้อผิดพลาดในการลงทะเบียน','error');
                        redirect($this->uri->uri_string());
                      }
                  }
                  

                }else{

                        $insertdata = "insert into ptm_memory(
                            sn,
                            sta,
                            fname,
                            lname,
                            phone
                        )values(
                            '".$this->input->post('product_sn')."',
                            'success',
                            '".$this->input->post('firstname')."',
                            '".$this->input->post('lastname')."',
                            '".$this->input->post('telephone')."'

                        )";

                        if(sqlsrv_query($this->ptm_conn,$insertdata,array(),array( "Scrollable" => 'static' ))){

                            /* Sendemail to Admin */
                            //$this->sendmailToAdmin($this->input->post());
                            /* Eof Sendemail to Admin*/


                            /* Insert logs */
                            //$this->insertlogs($this->input->post());
                            /* Eof Insert logs*/



                            $this->msg->add('ทำการลงทะเบียนเรียบร้อย','success');
                            redirect($this->uri->uri_string());
                          }else{
                            $this->msg->add('เกิดข้อผิดพลาดในการลงทะเบียน','error');
                            redirect($this->uri->uri_string());
                          }
                }



                
            }


    		$this->template->meta->add('keywords','');
			$this->template->meta->add('description','');


			$this->setData('seo_title','');
			$this->setData('seo_keywords','');
			$this->setData('seo_description','');

			$data = $this->getData();


    		$this->template->content->view('register/registerofive/registerpage',$data);
    		$this->template->publish();


    }

    /* load javascript
     */
    private function _load_js() {
        $this->template->javascript->add(base_url('assets/js/jquery/jquery.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap/bootstrap.min.js'));
        //$this->template->javascript->add(base_url('assets/js/bootstrap/bootstrap.bundle.min.js'));
        $this->template->javascript->add(base_url('assets/js/frontend/instance.js'));

    }
    /**
     * load style sheet
     */
    private function _load_css() {

         $this->template->stylesheet->add(base_url('assets/css/bootstrap/bootstrap.min.css'));
         $this->template->stylesheet->add('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

    }

    public function getController() {
        return $this->controller;
    }

    public function setController($controller) {
        $this->controller = $controller;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        $this->method = $method;
    }
    public function getData($key = null) {
        if (is_null($key) || empty($key))
            return $this->data;
        else
            return $this->data[$key];
    }

    public function setData($key, $data) {
        $this->data[$key] = $data;
    }

    private function connectPtmDB(){


        //print_r($this->db_config);exit;
        if(ENVIRONMENT == 'development'){


            $serverName = $this->db_config['development']['servername']; //serverName\instanceName
            $connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
            $this->ptm_conn = sqlsrv_connect( $serverName, $connectionInfo);

            
            if(!$this->ptm_conn){
                 echo "Connection could not be established.<br />";
                 die( print_r( sqlsrv_errors(), true));
            }
        }else if(ENVIRONMENT == 'testing'){
            $serverName = $this->db_config['testing']['servername']; //serverName\instanceName
            $connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

            $this->ptm_conn = sqlsrv_connect( $serverName, $connectionInfo);

            
            if(!$this->ptm_conn){
                 echo "Connection could not be established.<br />";
                 die( print_r( sqlsrv_errors(), true));
            }

        }else if(ENVIRONMENT == 'production'){
            //echo 'aaaa';exit;
            $serverName = $this->db_config['production']['servername']; //serverName\instanceName
            $connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['production']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
            $this->ptm_conn = sqlsrv_connect( $serverName, $connectionInfo);

            
            if(!$this->ptm_conn){
                 echo "Connection could not be established.<br />";
                 die( print_r( sqlsrv_errors(), true));
            }



        }
    }

    public function testSendEmail(){

        $data = array(
            'product_sn' => 'BEA68A1A33447C9A',
            'firstname' => 'kridsada',
            'lastname' => 'phudpetkaew',
            'telephone' => '0802295577'
        );

        $this->load->helper(array('sendmail'));

        $subject = "มีการลงทะเบียนกล่อง O5";
        $message = "พบการลงทะเบียนกล่อง O5 เข้ามาในระบบโดยเพิ่ม Serial Number เข้ามาใหม่ โดยมีรายละเอียดดังนี้ <br>";
        $message .= "SN : ".$data['product_sn'].'<br>';
        $message .= "ชื่อ : ".$data['firstname'].'<br>';
        $message .= "นามสกุล : ".$data['lastname'].'<br>';
        $message .= "เบอร์โทรศัพท์ : ".$data['telephone'].'<br>';

        $to = "jquery4me@gmail.com";


        psi_sendmail($subject, $message, $to, $from='kridsada@psisat.com', $from_name='kridsada@psisat.com', $bcc=false);

    }

    private function sendmailToAdmin($data){
        $this->load->helper(array('sendmail'));

        $subject = "มีการลงทะเบียนกล่อง O5";
        $message = "พบการลงทะเบียนกล่อง O5 เข้ามาในระบบโดยเพิ่ม Serial Number เข้ามาใหม่ โดยมีรายละเอียดดังนี้ <br>";
        $message .= "SN : ".$data['product_sn'].'<br>';
        $message .= "ชื่อ : ".$data['firstname'].'<br>';
        $message .= "นามสกุล : ".$data['lastname'].'<br>';
        $message .= "เบอร์โทรศัพท์ : ".$data['telephone'].'<br>';

        $to = "rattana@psisat.com";


        psi_sendmail($subject, $message, $to, $from='kridsada@psisat.com', $from_name='kridsada@psisat.com', $bcc=false);

    }
    private function insertlogs($data){
        $strInsert = "insert into insertsn_logs(
            logs,
            firstname,
            lastname,
            telephone,
            created,
            updated
        )values(
            '".json_encode($data)."',
            '".$data['firstname']."',
            '".$data['lastname']."',
            '".$data['telephone']."',
            '".date('Y-m-d H:i:s')."',
            '".date('Y-m-d H:i:s')."'
        )";
        return $this->conn_mysql->query($strInsert);


    }

    private function connect_mysql(){

        if(ENVIRONMENT == "development"){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "registerofive";

        }else if(ENVIRONMENT == "testing"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "registerofive";

        }else if(ENVIRONMENT == "production"){
            $servername = "localhost";
            $username = "root";
            $password = "psifixit";
            $dbname = "registerofive";

        }

        // Create connection
        $this->conn_mysql = new mysqli($servername, $username, $password, $dbname);
        $this->conn_mysql->set_charset("utf8");
        // Check connection
        if ($this->conn_mysql->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } 

    }



}