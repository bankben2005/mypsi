<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CheckProductRegister extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	private $product_register_conn;

	public function __construct() {
            parent::__construct(); 

            $this->connectDB();

    }

    public function index(){


    	$data = [

    	];

    	$this->load->view('register/check_product_register',$data);

    }

    public function ajaxSearchProductRegister(){
    	$post_data = $this->input->post(); 

    	// check RegisterCode exist 
    	$queryRegisterCodeData = "select * from Consumer where RegisterCode = '".$post_data['register_code']."'"; 

    	$qRegisterCode = sqlsrv_query( $this->product_register_conn, $queryRegisterCodeData,array(),array( "Scrollable" => 'static' )); 



    	if(sqlsrv_num_rows($qRegisterCode) > 0){
    		$row = sqlsrv_fetch_object($qRegisterCode); 

    		$arrData = [
    			'ConsumerData'=>$row,
    			'ProductRegisterList'=>$this->getProductRegisterListByConsumerCode([
    				'register_code'=>$row->RegisterCode
    			])
    		]; 

    		$view = $this->load->view('register/ajax_render_consumer_data',$arrData,true);

    		//print_r($arrData);
    		unset($_POST);
    		echo json_encode([
    			'status'=>true,
    			'view'=>$view,
    			'arrData'=>$arrData,
    			'result_code'=>'000',
    			'result_desc'=>'Success'
    		]);

    	}else{

    		$view = $this->load->view('register/ajax_notfound_consumer',[],true);

    		unset($_POST);
    		echo json_encode([
    			'status'=>false,
    			'view'=>$view,
    			'result_code'=>'-002',
    			'result_desc'=>'Not found register code'
    		]);

    	}



    	





    	
    }

    private function connectDB(){ 

    	$product_register_config = [
    		'servername'=>'192.168.100.160',
    		'db_name'=>'PsiProductRegister',
    		'username'=>'Dev',
    		'password'=>'Psi@dev'
    	];

            //echo 'aaaa';exit;
            $serverName = $product_register_config['servername']; //serverName\instanceName
            $connectionInfo = array( "Database"=>$product_register_config['db_name'], "UID"=>$product_register_config['username'], "PWD"=>$product_register_config['password'],"CharacterSet" => "UTF-8");
            $this->product_register_conn = sqlsrv_connect( $serverName, $connectionInfo);

            
            if(!$this->product_register_conn){
                 echo "Connection could not be established.<br />";
                 die( print_r( sqlsrv_errors(), true));
            }


    } 

    private function getProductRegisterListByConsumerCode($data = []){ 

    	$arrReturn = [];

    	$query = "select * from ProductRegister where RegisterCode = '".$data['register_code']."' order by ExpireDate desc";

                
        $stmt = sqlsrv_query( $this->product_register_conn, $query,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt) > 0){
        		$key = 0;
                while($result = sqlsrv_fetch_object($stmt)){

                    array_push($arrReturn, $result); 

                    $arrReturn[$key]->{'ModelData'} = $this->getProductModelByProductRegisterData([
                    	'GPCode'=>$result->GPCode,
                    	'MDCode'=>$result->MPCode
                    ]);

                    $key++;
                }
        }

        return $arrReturn;

    }

    private function getProductModelByProductRegisterData($data = []){
    	$arrReturn = [];

    	$query = "select * from ProductModel where GPCode = '".$data['GPCode']."' and MDCode = '".$data['MDCode']."'";

                
        $stmt = sqlsrv_query( $this->product_register_conn, $query,array(),array( "Scrollable" => 'static' ));

        $row = new StdClass();
        if(sqlsrv_num_rows($stmt) > 0){
        	$row = sqlsrv_fetch_object($stmt);               
        }
        return $row;
    }
}