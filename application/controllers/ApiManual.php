<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiManual extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $authentication = array();
	public function __construct() {
                        parent::__construct();
                        $this->load->library(array('session'));
                        $this->template->set_template('api/template');
                        $this->setTemplate();

                        $this->authentication = array(
                        	'username' => 'psi',
                        	'password' => 'psi'
                        );
    }
	public function index(){

			if($this->session->userdata('api_manual_id')){
				$this->homepage();
			}else{
				$this->login();
			}

	}

	private function login(){

		if($this->input->post(NULL,FALSE)){

			if($this->input->post('username') == $this->authentication['username'] && $this->input->post('password') == $this->authentication['password']){
				$this->session->set_userdata('api_manual_id','999');
				

				redirect($this->router->class);
			}
		}

		$this->load->view('api/login');

	}
	private function homepage(){

		

		$data = array(

		);

		$this->template->content->view('api/function/register_product',$data);
        $this->template->publish();

	}

	private function setTemplate(){
					$this->template->title = 'PSIFIXIT API!';
                    $this->template->stylesheet->add(base_url().'assets/css/bootstrap/bootstrap.min.css');
                    $this->template->stylesheet->add(base_url().'assets/css/api/api.css');
                    $this->template->stylesheet->add('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
	}

	public function signout(){
		$this->session->unset_userdata('api_manual_id');
		redirect($this->router->class);
	}

}