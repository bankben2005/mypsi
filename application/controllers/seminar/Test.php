<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Seminar/OrderProductLibrary.php';
class Test extends OrderProductLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $seminar_conn;
	private $db_config;
	private $seminar_available;
	public function __construct() {
            parent::__construct();
            
            $this->load->config('internaldb');
            $this->db_config = $this->config->item('seminar_db');
            $this->connectSeminarDB();

    }

    public function index(){

    	$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
        $this->template->javascript->add(base_url('assets/seminar/js/test.js'));
        $data = array();
        $this->template->meta->add('keywords','');
        $this->template->meta->add('description','');


        $this->setData('seo_title','สั่งจองสินค้าจากงานสัมนา');
        $this->setData('seo_keywords','');
        $this->setData('seo_description','');

        $data = $this->getData();

        $this->template->content->view('seminar/test/test',$data);
        $this->template->publish();

    }

    public function copySeminarQuota(){
    	
    	$data_receive = array(
    		'older_seminar_code'=>$this->input->post('older_seminar_code'),
    		'newer_seminar_code'=>$this->input->post('newer_seminar_code')

    	);

    	$query = "select * from SeminarRegister where SeminarNo = '".$data_receive['older_seminar_code']."'";

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	$arData = array();
    	if(sqlsrv_num_rows($stmt) > 0){
    		while($result = sqlsrv_fetch_object($stmt)){
    			array_push($arData, $result);
    		}

    	}

    	// print_r($arData);exit;

    	foreach ($arData as $key => $value) {
    		# code...
    			$strInsert = "insert into SeminarRegister(
    				SeminarNo,
    				AgentCode,
    				Qty
    			)values(
    				?,
    				?,
    				?
    			)";

    			$params = array($data_receive['newer_seminar_code'],$value->AgentCode,$value->Qty);

    			sqlsrv_query( $this->seminar_conn, $strInsert, $params);
    	}


    }
    private function connectSeminarDB(){

    	if(ENVIRONMENT == 'development'){


	    	$serverName = $this->db_config['development']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}
		}else if(ENVIRONMENT == 'testing'){
			$serverName = $this->db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}



		}
    }

}