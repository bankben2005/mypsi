<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Seminar/OrderProductLibrary.php';
class OrderProduct extends OrderProductLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $seminar_conn;
	private $db_config;
	private $seminar_available;
	public function __construct() {
            parent::__construct();
            $this->load->config('internaldb');
            $this->db_config = $this->config->item('seminar_db');
            $this->connectSeminarDB();

            $this->seminar_available = $this->getSeminarAvailable();

            // print_r($this->seminar_available);exit;
    }
	public function index(){


        

        $datetimenow = strtotime(date('Y-m-d H:i:s'));
		$this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
		$this->template->javascript->add(base_url('assets/seminar/js/orderproduct.js?'.$datetimenow));

		if($this->input->post(NULL,FALSE)){
			$this->ordersummary($this->input->post());
		}else{

		$productavailable_list =$this->getSeminarProductAvailable();

        // print_r($productavailable_list);exit;
		
		$this->template->meta->add('keywords','');
		$this->template->meta->add('description','');


		$this->setData('seo_title','สั่งจองสินค้าจากงานสัมนา');
		$this->setData('seo_keywords','');
		$this->setData('seo_description','');
		$this->setData('productavailable_list',$productavailable_list);

		$data = $this->getData();

		$this->template->content->view('seminar/orderproduct/orderproduct',$data);
        $this->template->publish();
    	}

	}

    public function listorderproduct(){

        $agentcode = $this->input->get('agentcode');

        if(!$agentcode){
            redirect(base_url('seminar/'.$this->controller));
        }

        $listorderproduct = $this->getOrderProductListByAgentCode($agentcode);

        


        $this->template->meta->add('keywords','');
        $this->template->meta->add('description','');


        $this->setData('seo_title','สั่งจองสินค้าจากงานสัมนา');
        $this->setData('seo_keywords','');
        $this->setData('seo_description','');
        $this->setData('listorderproduct',$listorderproduct);

        $data = $this->getData();

        $this->template->content->view('seminar/orderproduct/listorderproduct',$data);
        $this->template->publish();

    }

	public function ordersummary($post_data = array()){

		$data_order = array();
		$data_order['agentcode'] = "";
		$data_order['product'] = array();

		foreach ($post_data as $key => $value) {
			# code...
			if($value){
				if($key == 'agentcode'){
					$data_order['agentcode'] = $value;
				}else{

					$rowProduct = $this->getDataProductByMpCode(trim($key));
					array_push($data_order['product'], array(
						'MPNAME' => $rowProduct->MPNAME,
						'MPDESCRIPTION' => $rowProduct->MPDESCRIPTION,
						'MPCODE' => $key,
						'Amount' => $value,
                        'PriceB' => $rowProduct->PriceB,
                        'ProductImage' => $rowProduct->ProductImage,
                        'UOM' => $rowProduct->UOM
					));
				}
			}
		}

		

		$this->template->meta->add('keywords','');
		$this->template->meta->add('description','');


		$this->setData('seo_title','');
		$this->setData('seo_keywords','');
		$this->setData('seo_description','');

		$this->setData('summary_data',$data_order);

		$data = $this->getData();

		$this->template->content->view('seminar/orderproduct/orderproductsummary',$data);
        $this->template->publish();


	}

	private function getSeminarProductAvailable(){
		/* Seminar */
		
		$returnData = array();
		// print_r($this->seminar_available);exit;

		// $query = "select * from SeminarProductAvailable where SeminarNo = '".$this->seminar_available->SeminarNo."' order by Amount asc";

        $query = "select * from SeminarProductAvailable where SeminarNo = '".$this->seminar_available->SeminarNo."' order by Amount asc";

        // $query = "select * from SeminarProductAvailable left join SeminarInfo on SeminarProductAvailable"

		//echo $query;exit;


		$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt) > 0){
    		while($result = sqlsrv_fetch_object($stmt)){
    			$gProduct = $this->db->select('MPCODE,MPNAME,MPDESCRIPTION,PriceB,ProductImage,UOM')
    			->from('MasterProduct')
    			->where('MPCODE',$result->MPCODE)
    			->get();
    			$rowProduct = $gProduct->row();


    			$data = array(
    				'data_available' => $result,
    				'data_product' => $rowProduct

    			);
    			array_push($returnData, $data);
    		}
    		return $returnData;

    	}else{
    		return new StdClass();
    	}




	}

	    private function connectSeminarDB(){

    	if(ENVIRONMENT == 'development'){


	    	$serverName = $this->db_config['development']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}
		}else if(ENVIRONMENT == 'testing'){
			$serverName = $this->db_config['testing']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");

			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}

		}else if(ENVIRONMENT == 'production'){

			$serverName = $this->db_config['production']['servername']; //serverName\instanceName
			$connectionInfo = array( "Database"=>$this->db_config['db_name'], "UID"=>$this->db_config['testing']['username'], "PWD"=>$this->db_config['testing']['password'],"CharacterSet" => "UTF-8");
			$this->seminar_conn = sqlsrv_connect( $serverName, $connectionInfo);

			
			if(!$this->seminar_conn){
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}



		}
    }

    private function getSeminarAvailable(){
    	$query = "select TOP 1 * from SeminarInfo where SeminarDate >= '".date('Y-m-d')."' order by SeminarDate asc";

    	$stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    	// echo ENVIRONMENT;exit;
    	if(sqlsrv_num_rows($stmt) > 0){
    		$result = sqlsrv_fetch_object($stmt);
    		return $result;
    	}
    		
    }

    private function getDataProductByMpCode($mpcode = ""){
    			$gProduct = $this->db->select('MPCODE,MPNAME,MPDESCRIPTION,PriceB,ProductImage,UOM')
    			->from('MasterProduct')
    			->where('MPCODE',$mpcode)
    			->get();
    			$rowProduct = $gProduct->row();
    			return $rowProduct;
    }

    public function ajaxSaveProductOrder(){
    	$data_insert = array();
    	$seminar_available = $this->getSeminarAvailable();
    	$agent_data = $this->getAgentData($this->input->post('agentcode'));
    	$data_receive = $this->input->post();

    	foreach ($data_receive as $key => $value) {
    		# code...
    		if($key!="agentcode"){
    			/* insert record for order */
    			$data = array(
    				'AgentCode' => $this->input->post('agentcode'),
    				'SeminarNo' => $seminar_available->SeminarNo,
    				'MPCODE' => trim($key),
    				'Amount' => $value,
    				'Created' => date('Y-m-d H:i:s'),
    				'CustomerName' => $agent_data['CustomerName'],
    				'CustomerAddress' => $agent_data['CustomerAddress'],
    				'CustomerDistrict' => $agent_data['CustomerDistrict'],
    				'CustomerProvince' => $agent_data['CustomerProvince'],
    				'CustomerPhone' => $agent_data['CustomerPhone'],
                    'SeminarPrice' => $this->getSeminarPriceByMPCODE(trim($key))
    			);

    			array_push($data_insert, $data);
    		}
    	}
    	// print_r($data_insert);exit;
        $this->setSeminarProductOrder($data_insert);

    	
    		echo json_encode(array('status'=>true,'insert'=>$data_insert));
    	


    		



    }

    private function getAgentData($agentcode = ""){
    	$query = $this->db->select('AgentCode,AgentName,AgentSurName,R_Addr1,R_District,R_Province,Telephone')
    	->from('Agent')
    	->where('AgentCode',$agentcode)
    	->get();
    	$row = $query->row();

    	return array(
    		'CustomerName' => @$row->AgentName.' '.@$row->AgentSurName,
    		'CustomerAddress' => @$row->R_Addr1,
    		'CustomerDistrict' => @$row->R_District,
    		'CustomerProvince' => @$row->R_Province,
    		'CustomerPhone' => @$row->Telephone
    	);

    }

    private function setSeminarProductOrder($data = array()){
    	foreach ($data as $key => $value) {
    		# code...
    		$strQuery = "insert into SeminarProductOrder(
    			SeminarNo,
    			AgentCode,
    			MPCODE,
    			Amount,
    			Created,
    			CustomerName,
    			CustomerAddress,
    			CustomerDistrict,
    			CustomerProvince,
    			CustomerPhone,
                SeminarPrice
    		)values(
    			'".$value['SeminarNo']."',
    			'".$value['AgentCode']."',
    			'".$value['MPCODE']."',
    			'".$value['Amount']."',
    			'".$value['Created']."',
    			'".$value['CustomerName']."',
    			'".$value['CustomerAddress']."',
    			'".$value['CustomerDistrict']."',
    			'".$value['CustomerProvince']."',
    			'".$value['CustomerPhone']."',
                '".$value['SeminarPrice']."'
    		)";
    		// echo $strQuery;exit;

    		sqlsrv_query( $this->seminar_conn, $strQuery,array(),array( "Scrollable" => 'static' ));


    	}

    	return true;
    }

    public function test(){
        $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
        $this->template->javascript->add(base_url('assets/seminar/js/test.js'));
        $data = array();
        $this->template->meta->add('keywords','');
        $this->template->meta->add('description','');


        $this->setData('seo_title','สั่งจองสินค้าจากงานสัมนา');
        $this->setData('seo_keywords','');
        $this->setData('seo_description','');

        $data = $this->getData();

        $this->template->content->view('seminar/test/test',$data);
        $this->template->publish();

    }

    // private function 

    private function getSeminarPriceByMPCODE($mpcode){

        $query = $this->db->select('MPCODE,PriceB')
        ->from('MasterProduct')
        ->where('MPCODE',$mpcode)
        ->get();

        if($query->num_rows() > 0){
            return $query->row()->PriceB;
        }else{
            return '';
        }
    }

    public function testSeminarAvailable(){
        $query = "select TOP 1 * from SeminarInfo where SeminarDate >= '".date('Y-m-d')."' order by SeminarDate asc";

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
        // echo ENVIRONMENT;exit;
        if(sqlsrv_num_rows($stmt) > 0){
            $result = sqlsrv_fetch_object($stmt);
            print_r($result);exit;
        }
    }

    public function ajaxCheckAlreadyOrderProduct(){
        //$seminar_data = $this->getSeminarAvailable();
        $data_receive =array(
            'agentcode' => $this->input->post('agentcode')
        );

        $query = "select * from SeminarProductOrder where AgentCode = '".$data_receive['agentcode']."' and SeminarNo = '".$this->seminar_available->SeminarNo."' and Active = '1'";

        $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
        // echo ENVIRONMENT;exit;
        


        if(sqlsrv_num_rows($stmt) > 0){
            echo json_encode(array('status'=>true,'agentcode'=>$data_receive['agentcode']));exit;
        }else{
            echo json_encode(array('status'=>false,'agentcode'=>$data_receive['agentcode']));exit;
        }



    }


    public function getOrderProductListByAgentCode($agentcode){

            $arReturn = array();
            $query = "select * from SeminarProductOrder where AgentCode = '".$agentcode."' and Active = '1'";

            $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
            // echo ENVIRONMENT;exit;
            if(sqlsrv_num_rows($stmt) > 0){

                while ($result = sqlsrv_fetch_object($stmt)) {
                    $masterproduct_data = $this->db->select('MPCODE,MPNAME,MPDESCRIPTION,ProductImage,UOM')
                    ->from('MasterProduct')
                    ->where('MPCODE',$result->MPCODE)
                    ->get();
                    $row = $masterproduct_data->row();
                    # code...
                    $result->{'ProductImage'} = 'http://apifixit.psisat.com/'.$row->ProductImage;
                    $result->{'MPNAME'} = $row->MPNAME;
                    $result->{'MPDESCRIPTION'} = $row->MPDESCRIPTION;
                    $result->{'UOM'} = $row->UOM;
                    array_push($arReturn, $result);
                }
                
                //print_r($arReturn);exit;

            }
            return $arReturn;





    }

    // public function showAllOrderProduct(){
    //     $query = "select * from SeminarProductOrder";

    //     $stmt = sqlsrv_query( $this->seminar_conn, $query,array(),array( "Scrollable" => 'static' ));
    //     // echo ENVIRONMENT;exit;
    //     if(sqlsrv_num_rows($stmt) > 0){
    //             foreach($result = sqlsrv_fetch_object($stmt)){
    //                 print_r($result);
    //             }
    //     }

    // }


}